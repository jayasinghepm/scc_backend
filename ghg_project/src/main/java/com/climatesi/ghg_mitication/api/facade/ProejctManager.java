package com.climatesi.ghg_mitication.api.facade;

import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface ProejctManager extends DataEntryManager<Project, Integer> {

    int deleteProject(int id);
}
