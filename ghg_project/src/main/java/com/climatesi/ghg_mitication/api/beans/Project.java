package com.climatesi.ghg_mitication.api.beans;

import java.util.Date;

public interface Project {

    public int getId();

    public void setId(int id);

    public int getCompanyId();

    public void setCompanyId(int companyId);

    public Date getEndDate();

    public void setEndDate(Date endDate);

    public boolean isExtended();

    public void setExtended(boolean extended);

    public int getSignedWith();

    public void setSignedWith(int signedWith);

    public int getStatus();

    public void setStatus(int status);

    public int getNumberOfPayments();

    public void setNumberOfPayments(int numberOfPayments);

    public String getName();

    public void setName(String name);

    public Date getIntiatedDate();

    public void setIntiatedDate(Date intiatedDate);

    public Date getSignedDate();

    public void setSignedDate(Date signedDate);

    public Date getFirstPayDate();

    public void setFirstPayDate(Date firstPayDate);

    public Date getSecondPayDate();

    public void setSecondPayDate(Date secondPayDate);

    public Date getFinalPayDate();

    public void setFinalPayDate(Date finalPayDate);

    public Date getExtendedDate();

    public void setExtendedDate(Date extendedDate);

    public int isDeleted();

    public void setDeleted(int deleted);

    public String getCompany();

    public void setCompany(String company) ;

    public String getStatusStr();

    public void setStatusStr(String statusStr);

    public boolean isDataEntryExpired();

    public void setDataEntryExpired(boolean dataEntryExpired);

    public int getDisableFormula();

    public void setDisableFormula(int disableFormula);
}
