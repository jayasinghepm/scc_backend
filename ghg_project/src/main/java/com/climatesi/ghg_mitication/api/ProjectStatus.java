package com.climatesi.ghg_mitication.api;

public class ProjectStatus {

    public static final int NEW = 1;
    public static final int SIGNED = 2;
    public static final int APPROVED = 3;
    public static final int DATA_ENTRY = 4;
    public static final int COMPLETENESS_CHECK = 5;
    public static final int FIRST_DRAFT_SUBMISSION = 6;
    public static final int FINAL_SUBMISSION = 7;
    public static final int CLOSED = 8;

}
