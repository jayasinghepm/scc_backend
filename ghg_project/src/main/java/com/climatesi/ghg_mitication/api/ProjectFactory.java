package com.climatesi.ghg_mitication.api;

import com.climatesi.ghg_mitication.api.facade.ProejctManager;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import implGeneral.facade.ProjectManagerFacade;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class ProjectFactory {
    private static Logger logger = Logger.getLogger(ProjectFactory.class);

    private static ProjectFactory factory;

    private ProejctManager proejctManager;

    private ProjectFactory() {
    }

    public static ProjectFactory getInstance() throws GHGException {
        try {
            if (factory == null) {
                factory = new ProjectFactory();
            }
            return factory;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new GHGException("Error in get instance of QuestionFactory", ex);
        }
    }

    public ProejctManager getProejctManager(EntityManager em) {
        if (proejctManager == null) {
            proejctManager = new ProjectManagerFacade();
            proejctManager.injectEntityManager(em);
        }
        return proejctManager;
    }
}
