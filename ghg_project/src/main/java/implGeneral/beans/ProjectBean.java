package implGeneral.beans;

import com.climatesi.ghg_mitication.api.beans.Project;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "GHG_PROJECT")
@NamedQueries({
        @NamedQuery(name = "ProjectBean.findById",
                query = "SELECT i FROM ProjectBean i WHERE i.id = :id"),
})
public class ProjectBean implements Project {
    @Id
    @GeneratedValue(generator = "projectIdSeq")
    @SequenceGenerator(name = "projectIdSeq", sequenceName = "PROJECT_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "ID")
    private int id;

    @Column(name = "COM_ID")
    private int companyId;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "IS_EXTENDED")
    private boolean isExtended;

    @Column(name = "SIGNED_WITH")
    private int signedWith;

    @Column(name = "STATUS")
    private int status;

    @Column(name = "PAYMENT_TERMS")
    private int numberOfPayments;

    @Column(name = "NAME")
    private String name;

    @Column(name = "INITIATED_DATE")
    private Date intiatedDate;

    @Column(name = "SINGED_DATE")
    private Date signedDate;

    @Column(name = "FIRST_PAY_DATE")
    private Date firstPayDate;

    @Column(name = "SECOND_PAY_DATE")
    private Date secondPayDate;

    @Column(name = "FINAL_PAY_DATE")
    private Date finalPayDate;

    @Column(name = "EXTENDED_DATE")
    private Date extendedDate;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private int disableFormula;

    private String company;

    private String statusStr;

    private boolean dataEntryExpired;

    public int getDisableFormula() {
        return disableFormula;
    }

    public void setDisableFormula(int disableFormula) {
        this.disableFormula = disableFormula;
    }

    public boolean isDataEntryExpired() {
        return dataEntryExpired;
    }

    public void setDataEntryExpired(boolean dataEntryExpired) {
        this.dataEntryExpired = dataEntryExpired;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isExtended() {
        return isExtended;
    }

    public void setExtended(boolean extended) {
        isExtended = extended;
    }

    public int getSignedWith() {
        return signedWith;
    }

    public void setSignedWith(int signedWith) {
        this.signedWith = signedWith;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNumberOfPayments() {
        return numberOfPayments;
    }

    public void setNumberOfPayments(int numberOfPayments) {
        this.numberOfPayments = numberOfPayments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getIntiatedDate() {
        return intiatedDate;
    }

    public void setIntiatedDate(Date intiatedDate) {
        this.intiatedDate = intiatedDate;
    }

    public Date getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(Date signedDate) {
        this.signedDate = signedDate;
    }

    public Date getFirstPayDate() {
        return firstPayDate;
    }

    public void setFirstPayDate(Date firstPayDate) {
        this.firstPayDate = firstPayDate;
    }

    public Date getSecondPayDate() {
        return secondPayDate;
    }

    public void setSecondPayDate(Date secondPayDate) {
        this.secondPayDate = secondPayDate;
    }

    public Date getFinalPayDate() {
        return finalPayDate;
    }

    public void setFinalPayDate(Date finalPayDate) {
        this.finalPayDate = finalPayDate;
    }

    public Date getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(Date extendedDate) {
        this.extendedDate = extendedDate;
    }
}
