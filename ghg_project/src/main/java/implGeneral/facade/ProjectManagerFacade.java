package implGeneral.facade;

import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg_mitication.api.facade.ProejctManager;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import implGeneral.beans.ProjectBean;
import implGeneral.persistence.ProjectDAO;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class ProjectManagerFacade implements ProejctManager {
    private static Logger logger = Logger.getLogger(ProjectManagerFacade.class);
    private ProjectDAO dao;

    @Override
    public Object addEntity(Project entity) {
        try {
            return dao.update((ProjectBean) entity);
        } catch (Exception e) {
            logger.error("Error occured  while adding project ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(Project entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Project entity) {
        return null;
    }

    @Override
    public Object updateEntity(Project entity, int userID) {
        try {
            return dao.update((ProjectBean) entity);
        } catch (Exception e) {
            logger.error("Error occured  while updating Project ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public boolean setStatusOfEntity(Project entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Project entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Project entity) {
        return null;
    }

    @Override
    public Project getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Project> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Project> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Project getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<Project> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Project> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new ProjectDAO(entityManager);
    }

    @Override
    public int deleteProject(int id) {
        try {
            return dao.deleteProject(id);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
