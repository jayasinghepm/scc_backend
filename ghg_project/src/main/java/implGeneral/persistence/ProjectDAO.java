package implGeneral.persistence;

import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import implGeneral.beans.ProjectBean;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class ProjectDAO extends AbstractDAO<ProjectBean, Project> {

    private static Logger logger = Logger.getLogger(ProjectDAO.class.getName());
    private EntityManager em;

    public ProjectDAO(EntityManager em) {
        super(em, ProjectBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("GHG_PROJECT", "ID", "ProjectBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }


    public int deleteProject(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from ProjectBean l where l.id = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }


    @Override
    public Map<String, Object> getAddSpParams(ProjectBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(ProjectBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(ProjectBean entityImpl) throws GHGException {
        return null;
    }
}
