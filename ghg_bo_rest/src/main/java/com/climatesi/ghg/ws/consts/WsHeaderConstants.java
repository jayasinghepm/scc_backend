package com.climatesi.ghg.ws.consts;

public class WsHeaderConstants {

    //Send
    public static final int SEND_WEB_NOTIFICATION = 1;
    public static final int SEND_AIRPORT_RESULTS = 2;
//    public static final int SEND_COUNTRIES_RESULTS = 2;


    // read
    public static final int READ_WEB_NOTIFICATION = 1;
    public static final int READ_LOAD_AIRPORTS = 2;
}
