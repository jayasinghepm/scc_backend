package com.climatesi.ghg.ws.consts;

public class WsGroupConstants {

    public static final int GROUP_AUTH = 1;
    public static final int GROUP_WEB_NOTIFICATION = 2;
    public static final int GROUP_DB_LOAD = 3;
    public static final int GROUP_MESSAGING = 4;

}
