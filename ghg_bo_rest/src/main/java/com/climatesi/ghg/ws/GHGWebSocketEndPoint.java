package com.climatesi.ghg.ws;

import com.climatesi.ghg.api.beans.WebNotification;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.notification.NotificationController;
import com.climatesi.ghg.bo_controller.populators.NotificationPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.ws_message.WebNotificationDTO;
import com.climatesi.ghg.implGeneral.beans.WebNotificationBean;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.Gson;
import org.jboss.logging.Logger;

import javax.enterprise.event.Observes;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;

@ServerEndpoint(
        value = "/ws/{user}"
)
public class GHGWebSocketEndPoint {

    private static final Logger logger = Logger.getLogger(GHGWebSocketEndPoint.class);
    private static Queue<Session> queue = new ConcurrentLinkedQueue<>();
    private static Map<Integer, Session> sessionMap  = new ConcurrentSkipListMap();
    private static Gson gson = new Gson();
    private static NotificationController notificationController;


    /* PriceVolumeBean calls this method to send updates */
    public static void send(double price, int volume) {
//        String msg = String.format("%.2f / %d", price, volume);
//        try {
//            /* Send updates to all open WebSocket sessions */
//            for (Session session : queue) {
//                session.getBasicRemote().sendText(msg);
//                logger.log(Level.INFO, "Sent: {0}", msg);
//            }
//        } catch (IOException e) {
//            logger.log(Level.INFO, e.toString());
//        }
    }

    public static void sendNotificationAndPersist(@Observes WebNotificationDTO dto) {
        logger.info("Notification: " + dto.getMessage());
        //change date to ist
        DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:SS z");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        String IST = df.format(new Date());
        dto.setAddedTime(IST);
        int userId = dto.getUserId();

       dto.setUserId(userId);
        WebNotification res;
        try {
            try {
                notificationController = GHGControllerFactory.getInstance().getNotificationController();
                res = notificationController.editNotification(dto);
                if (sessionMap.containsKey(userId)) {
                    Session userSession = sessionMap.get(userId);
                    if (res != null) {
                        dto.setId(res.getId());
                        try {
                            userSession.getBasicRemote().sendText(gson.toJson(dto));
                        } catch (Exception e) {
                            logger.error("Error in sending notification", e);
                        }
                    }

                }
            } catch (GHGException e) {
                logger.error(e);
            }

        } catch (Exception e) {
            logger.error(e);
        }
    }

    public static void initNotifications(int userId) {
        if (sessionMap.containsKey(userId)) {
            Session userSession = sessionMap.get(userId);
            try {
                notificationController = GHGControllerFactory.getInstance().getNotificationController();
//                res = notificationController.editNotification(dto);
            } catch (GHGException e) {
                logger.error(e);
            }
//            WebNotificationDTO msg = new WebNotificationDTO();
//            msg.setUserId(userId);
//            msg.setMessage("Dummay tex");
//            try {
//                userSession.getBasicRemote().sendText(gson.toJson(msg));
//            } catch (IOException e) {
//               logger.error(e);
//            }
            List<WebNotification> list = notificationController.getNotifications(userId);
            list.forEach( n -> {
                WebNotificationDTO dto = NotificationPopulator.getInstance().populateDTO((WebNotificationBean)n);
                try {
                    userSession.getBasicRemote().sendText(gson.toJson(dto));
                } catch (IOException e) {
                    logger.error(e);
                }
            });
            if (list.size() == 0) {
                try {
                    userSession.getBasicRemote().sendText("Empty Notifications");
                } catch (IOException e) {
                  logger.error(e);
                }
            }
        }
    }

    @OnOpen
    public void openConnection(@PathParam("user") int user, Session session) {
        /* Register this connection in the queue */
        logger.info( "Connection opened.");
        logger.info(session);
        logger.info("user id: " + user);
        if (sessionMap.containsKey(user )) {
            // replace new session
            sessionMap.replace(user, session);
        } else {
            sessionMap.put(user , session);
        }

        queue.add(session);
        initNotifications(user);

    }

    @OnClose
    public void closedConnection(Session session) {
        /* Remove this connection from the queue */
        queue.remove(session);
        if (sessionMap.containsValue(session)) {
            logger.info("session map contains session");
            Integer userId = -1;
            for (Map.Entry<Integer, Session> entry : sessionMap.entrySet()) {
               if (entry.getValue().equals(session)) {
                   userId = entry.getKey();
                   break;
               }
            }
            if (userId != -1) {
                sessionMap.remove(userId);
            }
        }
        logger.info("Connection closed.");
    }

    @OnError
    public void error(Session session, Throwable t) {
        /* Remove this connection from the queue */
        queue.remove(session);
        if (sessionMap.containsValue(session)) {
            logger.info("session map contains session");
            int  userId =-1;
            for (Map.Entry<Integer, Session> entry : sessionMap.entrySet()) {
                if (entry.getValue().equals(session)) {
                    userId = entry.getKey();
                    break;
                }
            }
            if (userId != -1) {
                sessionMap.remove(userId);
            }

        }
        logger.info (t.toString());
        logger.info( "Connection error.");
    }

    @OnMessage
    public void onMessage( String msg) {
        logger.info("Ws Message recieved : " + msg);
        try {
            WebNotificationDTO dto = new Gson().fromJson(msg, WebNotificationDTO.class);
            if (dto != null) {
                notificationController.editNotification(dto);

            }
        }catch (Exception e) {
            logger.error("Error ocurred parsing ws message", e);
        }

    }
}
