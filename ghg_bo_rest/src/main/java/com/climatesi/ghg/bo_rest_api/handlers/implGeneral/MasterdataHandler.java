package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.MasterDataHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.request.AirportListReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.request.CountryListReqMsg;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class MasterdataHandler implements MessageHandler {

    private static Logger logger = Logger.getLogger(MasterdataHandler.class);
    private static MasterdataHandler instance;
    private MasterDataHelper helper;

    private MasterdataHandler() {
        try {
            helper = new MasterDataHelper();
        } catch (Exception e) {
            logger.error("Error in intialing helper", e);
        }
    }

    public static MasterdataHandler getInstance() {
        if (instance == null) {
            instance = new MasterdataHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch (header.getMessageType()) {

            case HeaderConstants.REQUEST_TYPE_LIST_COUNTRIES: {
                logger.info("");
                responseMessage = helper.getCountries((CountryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_AIRPORTS: {
                logger.info("");
                responseMessage = helper.getAirports((AirportListReqMsg) message, user);
                break;
            }

        }
        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;

    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
