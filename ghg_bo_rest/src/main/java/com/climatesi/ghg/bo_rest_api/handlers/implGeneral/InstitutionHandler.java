package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.InsititutionHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request.*;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class InstitutionHandler implements MessageHandler {

    private static Logger logger = Logger.getLogger(InstitutionHandler.class);
    private static InstitutionHandler instance;
    private InsititutionHelper insititutionHelper;

    private InstitutionHandler() {
        try {
            insititutionHelper = new InsititutionHelper();
        } catch (Exception e) {
            logger.error("Error in intializing", e);
        }
    }


    public static InstitutionHandler getInstance() {
        if (instance == null) {
            instance = new InstitutionHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        logger.info("Process Instittution group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch (header.getMessageType()) {
            case HeaderConstants.REQUEST_TYPE_LIST_BRANCH: {
                logger.info("");
                responseMessage = insititutionHelper.getBranchList((BranchListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_COMPANY: {
                logger.info("");
                responseMessage = insititutionHelper.getCompanyList((CompanyListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_BRANCH: {
                logger.info("");
                responseMessage = insititutionHelper.manageBranch((BranchManagedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_COMPANY: {
                logger.info("");
                responseMessage = insititutionHelper.manageCompany((CompanyManagedReqeustMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_COMPANY_DATA_ENTRY: {
                logger.info("");
                responseMessage = insititutionHelper.getCompanyListDataEntry((CompanyListRequestMsg) message, user);
                break;
            } case HeaderConstants.REQUEST_TYPE_LIST_COMPANY_SUMMARY_INPUTS: {
                logger.info("");
                responseMessage = insititutionHelper.getCompanyListSummaryInputs((CompanyListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGE_MENU_: {
                logger.info("");
                responseMessage = insititutionHelper.manageMenuCompany((ManageMenuCompanyReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_UPDATE_CATEGORY : {
                responseMessage = insititutionHelper.manageEmissionCategory((ManageEmissionCategoryReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_UPDATE_EXCLUDED_REASONS_AND_SUGGESTIONS : {
                responseMessage = insititutionHelper.updateSuggestionsAndExcludedEmissionSources((UpdateCompanyExcludedReasonAndSuggestionReqMsg) message, user);
                break;
            }

        }

        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
