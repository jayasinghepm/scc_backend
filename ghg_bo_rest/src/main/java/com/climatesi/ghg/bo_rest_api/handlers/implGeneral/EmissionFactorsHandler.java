package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.EmissionFactorHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.request.*;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class EmissionFactorsHandler implements MessageHandler {

    private static Logger logger = Logger.getLogger(EmissionFactorsHandler.class);
    private static EmissionFactorsHandler instance;
    private EmissionFactorHelper emissionFactorHelper;

    private EmissionFactorsHandler() {
        try {
            emissionFactorHelper = new EmissionFactorHelper();
        } catch (Exception e) {
            logger.error("Error in intializing", e);
        }
    }


    public static EmissionFactorsHandler getInstance() {
        if (instance == null) {
            instance = new EmissionFactorsHandler();
        }
        return instance;
    }


    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        logger.info("Process Instittution group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch (header.getMessageType()) {
            case HeaderConstants.REQUEST_TYPE_MANAGED_EM_FACTOR: {
                responseMessage = emissionFactorHelper.manageEmFactor((ManageEmFactorReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_PUB_TRANS_FACTOR: {
                responseMessage = emissionFactorHelper.managePubTransFactors((ManagePubTransFactorReqMsg) message, user);
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_WASTE_DIS_FACTOR: {
                responseMessage = emissionFactorHelper.manageWasteDisFactors((ManageWasteDisFactorReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_EM_FACTOR: {
                responseMessage = emissionFactorHelper.getEmissionFactors((ListEmFactorReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_PUB_TRANS_FACTOR: {
                responseMessage = emissionFactorHelper.getPublicTransFactors((ListPubTransFactorReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_WASTE_DIS_FACTOR: {
                responseMessage = emissionFactorHelper.getWasteDisFactors((ListWasteDisFactorReqMsg) message, user);
                break;
            }
        }
        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
