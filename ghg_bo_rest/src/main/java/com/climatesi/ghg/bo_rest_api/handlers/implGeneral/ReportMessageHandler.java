package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.ReportHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.req.*;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class ReportMessageHandler implements MessageHandler {

    private static Logger logger = Logger.getLogger(ReportMessageHandler.class);
    private static ReportMessageHandler instance;
    private ReportHelper reportHelper;

    private ReportMessageHandler() {
        try {
            reportHelper = new ReportHelper();
        }catch (Exception e) {
            logger.error("Error in handler",e);
        }
    }

    public static ReportMessageHandler getInstance() {
        if (instance == null) {
            instance = new ReportMessageHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        logger.info("Process Emission group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch(header.getMessageType()){
            case HeaderConstants.REQUEST_TYPE_MANAGE_GHG_REPORT: {
                responseMessage = reportHelper.manageGHGReport((ManageGHGReportReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_GHG_REPORT: {
                responseMessage = reportHelper.getReports((ListGHGReportReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGE_REPORT_META_DATA: {
                responseMessage = reportHelper.manageReportMetaData((ManageReportMetaDataReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_REPORT_META_DATA: {
                responseMessage = reportHelper.listReportMetaData((ListReportMetaReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_GET_REPORT_ZIP: {
                responseMessage = reportHelper.getReportZip((GetReportZipReqMsg)message, user);
                break;
            }
        }
        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }
        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);
        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
