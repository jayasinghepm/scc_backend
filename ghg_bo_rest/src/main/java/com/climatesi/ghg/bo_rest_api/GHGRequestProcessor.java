package com.climatesi.ghg.bo_rest_api;

import com.climatesi.ghg.bo_messaging.api.MessageProtocolFactory;
import com.climatesi.ghg.bo_messaging.api.MessageProtocolManager;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.GroupConstants;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_rest_api.handlers.implGeneral.*;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.json.JsonObject;

public class GHGRequestProcessor {

    private static Logger logger = Logger.getLogger(GHGRequestProcessor.class.getName());
    private MessageProtocolManager messageProtocolManager;
    private SessionManager sessionManager;

    public GHGRequestProcessor() {
        try {
            messageProtocolManager = MessageProtocolFactory.getInstance().getMessageProtocolManager();
            sessionManager = SessionManager.getInstance();
        } catch (GHGException e) {
            logger.error("Error in getting at GHGRequestProcessor constructor", e);
        }
    }

    public String processWebRequest(String request) {
        logger.info("Started to process client request .................  " +  request);
        Envelope responseEnv = null;
        boolean doSessionValidation = true;
        boolean doAuthorization = false;
        int user = 0;
        Header header = null;

        try {
            header = messageProtocolManager.getHeaderFromEnvelopeMessage(request);

            if (header.getMessageGroup() == GroupConstants.GROUP_AUTH &&
                header.getMessageType() == HeaderConstants.REQUEST_TYPE_LOGIN_NORMAL
            ) {
                doSessionValidation = false;
            }

            if (doSessionValidation) {
                logger.info("Session Validation started.");
                user = sessionManager.validateUserSession(header.getSessionId());

                if (user == 0) {
                    logger.info("Session validation failed");
                    responseEnv = ResponseGenerator.getInstance().
                            getErrorResponse(header, HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_SESSION, "Invalid session ID");
                    return messageProtocolManager.getJSonString(responseEnv);
                }
                logger.info("Session validated for User Id >> " + user);
            }

            doAuthorization = header.getMessageGroup() != GroupConstants.GROUP_AUTH;
            if (doAuthorization) {
                // do authorization other message grops
            }

            Message message = null;
            JsonObject jsonObject = null;


            message = messageProtocolManager.getMessageFromEnvelopeMessage(header, request);
            logger.info("Message Group  " + header.getMessageGroup());
            logger.info("Message" + message);
            switch (header.getMessageGroup()) {
                case GroupConstants.GROUP_AUTH: {
                    responseEnv = AuthMessageHandler.getAuthMessageHandler().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_EMISSION: {
                    responseEnv = EmissionMessageHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_BUSINESS_USERS: {
                    responseEnv = BusinessUserHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_INSTITUTION: {
                    responseEnv = InstitutionHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_LOGIN_USER: {
                    responseEnv = UserLoginHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_MASTER_DATA: {
                    responseEnv = MasterdataHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_EM_FACTORS: {
                    responseEnv = EmissionFactorsHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_GHG_PROJECT: {
                    responseEnv = ProjectHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_SUMMARY_INPUTS: {
                    responseEnv = SummaryInputsMessageHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_REPORT: {
                    responseEnv = ReportMessageHandler.getInstance().processMessage(header, message, user);
                    break;
                }
                case GroupConstants.GROUP_MITIGATION: {
                    responseEnv = MitigationHandler.getInstance().processMessage(header, message,user);
                    break;
                }
                default:
                    logger.error("Unknown Message group");
                    responseEnv = ResponseGenerator.getInstance().getErrorResponse(header, HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_UNKNOWN_MSG, "Unknown message group");
                    break;
            }

        } catch (Exception e) {
            logger.error("Error in processiong request : " + e.getMessage(), e);
            responseEnv = ResponseGenerator.getInstance().getErrorResponse(header, HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_UNKNOWN_MSG, e.getMessage());

        }



        return messageProtocolManager.getJSonString(responseEnv);
    }
}
