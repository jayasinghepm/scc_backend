package com.climatesi.ghg.bo_rest_api;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("services")
public class ApplicationWSActivator extends Application {
}
