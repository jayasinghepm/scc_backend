package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.BusinessUserHelper;
import com.climatesi.ghg.bo_controller.helpers.MitigationHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust.AdminListRequestMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust.EmployeeListRequestMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.GetMitigationActDataReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.GetMitigationReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.ManageMitigationActDataReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.ManageMitigationProjectReqMsg;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class MitigationHandler implements MessageHandler {

    private static final Logger logger = Logger.getLogger(MitigationHandler.class);
    private static MitigationHandler instance;
    private MitigationHelper mitigationHelper;


    private MitigationHandler() {
        try {
            mitigationHelper = new MitigationHelper();
        } catch (Exception e) {
            logger.error("Error in handler", e);
        }
    }

    public static MitigationHandler getInstance() {
        if (instance == null) {
            instance = new MitigationHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        logger.info("Process Emission group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch (header.getMessageType()) {
            case HeaderConstants.REQUEST_TYPE_MANAGE_MITIGATION_PROJECT: {
                logger.info("");
                responseMessage = mitigationHelper.manageMitigationProject((ManageMitigationProjectReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_GET_MITIGATION_PROJECTS: {
                logger.info("");
                responseMessage = mitigationHelper.getMitigationProject((GetMitigationReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGE_MITIGATION_PROJECT_MONTHLY_ACTIVITY: {
                logger.info("");
                responseMessage = mitigationHelper.manageMitigationProjectActData((ManageMitigationActDataReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_GET_MITIGATION_PROJECT_MONTHLY_ACTIVITY: {
                logger.info("");
                responseMessage = mitigationHelper.getMitigationProjectActData((GetMitigationActDataReqMsg)message, user);
                break;
            }


        }
        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
