package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.helpers.UserLoginHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.CreateLoginProfileBranchReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.FindLoginUserReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.ListUsrActLogsReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.ManageLoginUserReqMsg;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class UserLoginHandler implements MessageHandler {

    private static Logger logger = Logger.getLogger(UserLoginController.class);
    private static UserLoginHandler instance;
    private UserLoginHelper helper;

    private UserLoginHandler() {
        try {
            helper = new UserLoginHelper();
        } catch (Exception e) {
            logger.error("Error in intializing", e);
        }
    }

    public static UserLoginHandler getInstance() {
        if (instance == null) {
            instance = new UserLoginHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch (header.getMessageType()) {

            case HeaderConstants.REQUEST_TYPE_MANAGE_LOGIN_PROFILE: {
                logger.info("");
                responseMessage = helper.manageLoginUser((ManageLoginUserReqMsg) message);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_FIND_LOGIN_PROFILE: {
                responseMessage = helper.findUserLogin((FindLoginUserReqMsg)message);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_USR_ACT_LOGS : {
                responseMessage = helper.getFilterLogs((ListUsrActLogsReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_CREATE_LOGIN_PROFILE_FOR_BRANCHES: {
                responseMessage = helper.createBranchLoginProfile((CreateLoginProfileBranchReqMsg)message, user);
                break;
            }

        }
        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;

    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
