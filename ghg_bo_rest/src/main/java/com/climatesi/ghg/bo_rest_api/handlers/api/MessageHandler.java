package com.climatesi.ghg.bo_rest_api.handlers.api;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;


public interface MessageHandler {

    /**
     * Process the json message from the client and send responses to the client/s
     *
     * @param header  contains request/response meta data
     * @param message contains request/response specific details
     * @param user    is used to authorize and audit the action. Pass null if not required.
     * @return envelope object which contains response for request
     * @throws GHGException the mubasher exception
     */
    // TODO:  add User param
    Envelope processMessage(Header header, Message message, int user) throws GHGException;

    /**
     * Process message envelope.
     *
     * @param header  the header
     * @param message the message
     * @param user    the user
     * @return the envelope
     * @throws GHGException the mubasher exception
     */
//    Todo: add User Param
    Envelope processMessage(Header header, JsonObject message, int user) throws GHGException;
}

