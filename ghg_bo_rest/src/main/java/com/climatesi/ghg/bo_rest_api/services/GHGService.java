package com.climatesi.ghg.bo_rest_api.services;

import com.climatesi.ghg.bo_rest_api.GHGRequestProcessor;
import org.jboss.logging.Logger;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/ghg_services")
public class GHGService {

    private static Logger logger = Logger.getLogger(GHGService.class.getName());
    private GHGRequestProcessor processor = new GHGRequestProcessor();


    @OPTIONS
    @Path("/")
    @PermitAll
    public Response handle() {
        logger.info("Request for OPTIONS");

        return Response.status(Response.Status.NO_CONTENT)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .allow("OPTIONS").build();
    }


    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGHGService(String request) {
        logger.info("request  >> " + request);
        String response = processor.processWebRequest(request);
        logger.info("Resoponse  >> " + response);


        return Response.status(Response.Status.OK)
                .entity(response)

                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .build();
    }


}
