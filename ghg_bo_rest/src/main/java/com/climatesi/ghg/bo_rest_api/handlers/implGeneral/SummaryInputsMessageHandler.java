package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.SummaryInputHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.GetProjectFullSummaryReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.ManageProjectDataEntryStatusReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.ProjectDataEntryStatusReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.SummaryInputsRequestMessage;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;


public class SummaryInputsMessageHandler implements MessageHandler {

    private static final Logger logger = Logger.getLogger(SummaryInputsMessageHandler.class);
    private static SummaryInputsMessageHandler instance;
    private SummaryInputHelper summaryInputHelper;

    private SummaryInputsMessageHandler() {
        try {
            summaryInputHelper = new SummaryInputHelper();
        }catch (Exception e) {
            logger.error("Error in handler", e);
        }
    }

    public static SummaryInputsMessageHandler getInstance() {
        if (instance == null) {
            instance = new SummaryInputsMessageHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        logger.info("Process Emission group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;

        switch(header.getMessageType()) {
            case HeaderConstants.REQUEST_TYPE_ELEC_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getElecSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MUN_WATER_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getMunWaterSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_REFRI_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getRefriSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_FIRE_EXT_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getFireExtSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_WASTE_TRANS_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getWasteTransSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_COMPANY_OWNED_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getCompanyOwnedSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_EMP_COMM_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getEmpCommutingSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_BUS_AIRTRAVEL_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getAirtravelSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_RENTED_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getRentedSummaray((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_HIRED_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getHiredSummaray((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_GENERATORS_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getGeneratorsSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_WASTE_DIS_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getWasteDisSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_TRANS_LOC_PUR_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getTransLocPurchSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_DATA_ENTRY_STATUS: {
                responseMessage = summaryInputHelper.getDataEntryStatus((ProjectDataEntryStatusReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGE_DATA_ENTRY_STATUS: {
                responseMessage = summaryInputHelper.manageDataEntryStatus((ManageProjectDataEntryStatusReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_SEA_AIR_FREIGHT_SUMM_INPUTS:{
                responseMessage = summaryInputHelper.getSeaAirFreightSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_BIO_MASS_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getBioMassSummary((SummaryInputsRequestMessage) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LPG_ENTRY_SUMM_INPUTS: {
                responseMessage = summaryInputHelper.getLPGEntry((SummaryInputsRequestMessage) message, user);

                break;
            }

            case HeaderConstants.REQUEST_TYPE_GET_PROJECT_FULL_SUMMARY: {
                logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2");
                logger.info(message);
                logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                responseMessage =  summaryInputHelper.getProjectFullSummary((GetProjectFullSummaryReqMsg) message, user);
                break;
            }
        }



        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
       return null;
    }
}
