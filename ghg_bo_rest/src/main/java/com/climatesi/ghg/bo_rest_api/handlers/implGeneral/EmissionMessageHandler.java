package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.EmissionHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request.*;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class EmissionMessageHandler implements MessageHandler {

    private static EmissionMessageHandler instance = null;
    private static Logger logger = Logger.getLogger(EmissionMessageHandler.class);
    private EmissionHelper emissionHelper;


    private EmissionMessageHandler() {
        emissionHelper = new EmissionHelper();
    }


    public static EmissionMessageHandler getInstance() {
        if (instance == null) {
            instance = new EmissionMessageHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        logger.info("Process Emission group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch (header.getMessageType()) {
            case HeaderConstants.REQUEST_TYPE_LIST_AIRTRAVEL: {
                logger.info("REQUEST_TYPE_LIST_AIRTRAVEL");
                responseMessage = emissionHelper.getAirTravelEntryList((BusinessAirTravelEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_ELECTRICTY: {
                logger.info("REQUEST_TYPE_LIST_ELECTRICTY");
                responseMessage = emissionHelper.getElectricityEntryList((ElectricityEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_FIRE_EXT: {
                logger.info("REQUEST_TYPE_LIST_FIRE_EXT");
                responseMessage = emissionHelper.getFireExtEntryList((FireExtingEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_GENERATORS: {
                logger.info("REQUEST_TYPE_LIST_GENERATORS");
                responseMessage = emissionHelper.getGeneratorsEntryList((GeneratorsEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_MUN_WATER: {
                logger.info("REQUEST_TYPE_LIST_MUN_WATER");
                responseMessage = emissionHelper.getMunWaterEntryList((MunicipalWaterEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_REFRI: {
                logger.info("REQUEST_TYPE_LIST_REFRI");
                responseMessage = emissionHelper.getRefriEntryList((RefrigerantsEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_TRANS_LOCAL: {
                logger.info("REQUEST_TYPE_LIST_TRANS_LOCAL");
                responseMessage = emissionHelper.getTransLocalEntryList((TransportLocalEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_VEHICLE: {
                logger.info("REQUEST_TYPE_LIST_VEHICLE");
                responseMessage = emissionHelper.getVehicleEntryList((VehicleEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_WASTE_DIS: {
                logger.info("REQUEST_TYPE_LIST_WASTE_DIS");
                responseMessage = emissionHelper.getWasteDisposalEntryList((WasteDisposalEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_WASTE_TRANS: {
                logger.info("REQUEST_TYPE_LIST_WASTE_TRANS");
                responseMessage = emissionHelper.getWasteTransportEntryList((WasteTransportEntryListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_AIRTRAVEL: {
                logger.info("REQUEST_TYPE_MANAGED_AIRTRAVEL");
                responseMessage = emissionHelper.manageAirTravelEntry((BusinessAirTravelEntryManagedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_ELECTRICTY: {
                logger.info("REQUEST_TYPE_MANAGED_ELECTRICTY");
                responseMessage = emissionHelper.manageElectricityEntry((ElectricityEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_FIRE_EXT: {
                logger.info("REQUEST_TYPE_MANAGED_FIRE_EXT");
                responseMessage = emissionHelper.manageFireExtEntry((FireExtingEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_GENERATORS: {
                logger.info("REQUEST_TYPE_MANAGED_GENERATORS");
                responseMessage = emissionHelper.manageGeneratorsEntry((GeneratorsEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_MUN_WATER: {
                logger.info("REQUEST_TYPE_MANAGED_MUN_WATER");
                responseMessage = emissionHelper.manageMunWaterEntry((MunicipalWaterEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_REFRI: {
                logger.info("REQUEST_TYPE_MANAGED_REFRI");
                responseMessage = emissionHelper.manageRefriEntry((RefrigerantsEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_TRANS_LOCAL: {
                logger.info("REQUEST_TYPE_MANAGED_TRANS_LOCAL");
                responseMessage = emissionHelper.manageTransLocalEntry((TransportLocalEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_VEHICLE: {
                logger.info("REQUEST_TYPE_MANAGED_VEHICLE");
                responseMessage = emissionHelper.manageVehicleEntry((VehicleEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_WASTE_DIS: {
                logger.info("REQUEST_TYPE_MANAGED_WASTE_DIS");
                responseMessage = emissionHelper.manageWasteDisEntry((WasteDisposalEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_WASTE_TRANS: {
                logger.info("REQUEST_TYPE_MANAGED_WASTE_TRANS");
                responseMessage = emissionHelper.manageWasteTransEntry((WasteTransportEntryMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_EMP_COMMUTING: {
                logger.info("REQUEST_TYPE_LIST_EMP_COMMUTING");
                responseMessage = emissionHelper.getEmpCommutingList((EmpCommutingListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGE_EMP_COMMUTING: {
                logger.info("REQUEST_TYPE_MANAGE_EMP_COMMUTING");
                responseMessage = emissionHelper.manageEmpCommuting((EmpCommutingManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_GHG_EMISSION_SUMMARY: {

                logger.info(message);
                responseMessage = emissionHelper.getGHGEmissionSummary((GHGEmissionSummaryReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_GHG_EMISSION_BRANCHWISE: {
                responseMessage = emissionHelper.getGHGEmissionSummaryBranchWise((GHGEmissionSummaryReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_GHG_EMISSION_SUMMARY_EXCLUDED:{
                responseMessage = emissionHelper.getGHGEmissionSummary_excluded((GHHEmissionSummaryExcluedReqMsg)message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGE_GHG_EMISSION_SUMMARY: {
                responseMessage = emissionHelper.manageGHGEmissionSummary((ManageGHGEmissionSummaryReqMsg) message,user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_EXPORT_GHG_EMISSION_SUMMARY: {
                responseMessage = emissionHelper.exportGHGEmissionSummary((ExportGHGEmissionSummaryReqMsg)message);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_SEA_AIR_FRIGHT: {
                responseMessage = emissionHelper.getSeaAirFreightEntryList((SeaAirFreightListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_SEA_AIR_FREIGHT: {
                responseMessage = emissionHelper.manageSeaAirFreightEntry((SeaAirFreightMangedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_BIO_MASS: {
                responseMessage = emissionHelper.manageBioMassEntry( (BioMassEntryManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_BIO_MASS: {
                responseMessage = emissionHelper.getBioMassEntryList((BioMassEntryListReqMsg) message,user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_LPG_ENTRY: {
                responseMessage = emissionHelper.getLPGEntryList((LPGEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_LPG_ENTRY:{
                responseMessage = emissionHelper.manageLPGEntry((LPGEntryManagedReqMsg) message, user);
                break;
            }
            //----
            case HeaderConstants.REQUEST_TYPE_LIST_BGAS_ENTRY: {
                responseMessage = emissionHelper.getBGASEntryList((BGASEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_BGAS_ENTRY:{
                responseMessage = emissionHelper.manageBGASEntry((BGASEntryManagedReqMsg) message, user);
                break;
            }


            case HeaderConstants.REQUEST_TYPE_MANAGED_ASH_TRANSPORTATION:{
                responseMessage = emissionHelper.manageAshTransportationEntry((AshTransportationManagedReqMsg) message,user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_ASH_TRANSPORTATION:{
                responseMessage = emissionHelper.getAshTransportationEntryList((AshTransportationEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_FORKLIFTS: {
                responseMessage = emissionHelper.manageForkliftsEntry((ForkliftsManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_FORKLIFTS: {
                responseMessage = emissionHelper.getForkliftsEntryList((ForkliftEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_FURNACE_OIL: {
                responseMessage = emissionHelper.manageFurnaceEntry((FurnaceManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_FURNACE_OIL: {
                responseMessage = emissionHelper.getFurnaceOilEntryList((FurnaceEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_LORRY_TRANSPORTATION: {
                responseMessage = emissionHelper.manageLorryTransportationEntry((LorryTransportationManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_LORRY_TRANSPORTATION: {
                responseMessage = emissionHelper.getLorryTransportationEntryList((LorryTransportationEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_OILGAS_TRANSPORTATION: {
                responseMessage = emissionHelper.manageOilAndGasOiEntry((OilAndGasTransportationManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_OILGAS_TRANSPORTATION: {
                responseMessage = emissionHelper.getOilAndGasTransportationEntryList((OilAndGasEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_PAPER_WASTE: {
                responseMessage = emissionHelper.managePaperWasteEntry((PaperWasteManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_PAPER_WASTE: {
                responseMessage = emissionHelper.getPaperWasteEntryList((PaperWasteEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_PAID_MANAGER_VEHICLE: {
                responseMessage = emissionHelper.managePaidManagerVehicleEntry((PaidManagerVehicleManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_PAID_MANAGER_VEHICLE: {
                responseMessage = emissionHelper.getPaidManagerVehicleEntryList((PaidManagerVehicleEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_RAW_MATERIAL_TRANSPORTATION: {
                responseMessage = emissionHelper.manageRawMaterialTransportationEntry((RawMaterialTransportationManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_RAW_MATERIAL_TRANSPORTATION: {
                responseMessage = emissionHelper.getRawMaterialTransportationEntryList((RawMaterialTransportationEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_SAW_DUST_TRANSPORTATION: {
                responseMessage = emissionHelper.manageSawDustTransportationEntry((SawDustTransportationManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_SAW_DUST_TRANSPORTATION: {
                responseMessage = emissionHelper.getSawDustTransportationEntryList((SawDustTransportationEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_SLUDGE_TRANSPORTATION: {
                responseMessage = emissionHelper.manageSludgeTransportationEntry((SludgeTransportationManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_SLUDGE_TRANSPORTATION: {
                responseMessage = emissionHelper.getSludgeTransportationEntryList((SludgeTransportationEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_STAFF_TRANSPORTATION: {
                responseMessage = emissionHelper.manageStaffTransportationEntry((StaffTransportationManagedReqMsg) message, user);

                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_STAFF_TRANSPORTATION: {
                responseMessage = emissionHelper.getStaffTransportationEntryList((StaffTransportationEntryListReqMsg) message, user);

                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_VEHICLE_OTHERS_TRANSPORTATION: {
                responseMessage = emissionHelper.getVehicleOthersEntryList((VehicleOthersEntryListReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_VEHICLE_OTHERS_TRANSPORTATION: {
                responseMessage = emissionHelper.manageVehicleOthersEntry((VehicleOthersManagedReqMsg) message,user);
                break;
            }

            default:
                logger.error("Unknown Message Type");
                responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_UNKNOWN_MSG;
                responseNarration = "Unknown Message Type";
                break;
        }
        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
