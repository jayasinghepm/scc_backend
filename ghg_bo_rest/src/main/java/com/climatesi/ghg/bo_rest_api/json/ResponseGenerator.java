package com.climatesi.ghg.bo_rest_api.json;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.constants.GroupConstants;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import org.jboss.logging.Logger;

public class ResponseGenerator {
    private static ResponseGenerator instance = null;
    private static Logger logger = Logger.getLogger(ResponseGenerator.class.getName());

    private ResponseGenerator() {

    }
    public static ResponseGenerator getInstance() {
        if (instance == null) {
            instance = new ResponseGenerator();
        }

        return instance;
    }

    public Envelope getErrorResponse(Header reqHeader, int responseStatus, String errorMessage ) {
        logger.info("Start generate error response. Error message >> " + errorMessage + ", request header >> " + reqHeader);

        Header responseHeader = getDefaultResponseHeader(reqHeader);
        responseHeader.setResponseStatus(responseStatus);
        responseHeader.setResponseNarration(errorMessage);
        logger.info("Response header generated. >> " + responseHeader);

        Envelope response = new EnvelopeBean((HeaderBean) responseHeader, null);
        logger.info("End of generate error response >> " + response);
        return response;
    }

    public Header getDefaultResponseHeader(Header requestHeader) {
        Header responseHeader = new HeaderBean();
        responseHeader.setProtocolVersion(requestHeader.getProtocolVersion());
        responseHeader.setSessionId(requestHeader.getSessionId());
        responseHeader.setMessageGroup(requestHeader.getMessageGroup());
        responseHeader.setMessageType(getResponseMessageType(requestHeader.getMessageGroup(), requestHeader.getMessageType()));
        responseHeader.setChannelId(requestHeader.getChannelId());
        responseHeader.setClientIp(requestHeader.getClientIp());
        responseHeader.setClientVersion(requestHeader.getClientVersion());
        responseHeader.setClientReqID(requestHeader.getClientReqID());
        responseHeader.setInstitutionCode(requestHeader.getInstitutionCode());
        responseHeader.setInstitutionId(requestHeader.getInstitutionId());
        return responseHeader;
    }

    private int getResponseMessageType(int requestMsgGroup, int requestMsgType) {
        int responseMessageType = requestMsgType + 1000;

        switch (requestMsgGroup) {
            case GroupConstants.GROUP_AUTH: {
                switch(requestMsgType) {
                    case HeaderConstants.REQUEST_TYPE_LOGIN_NORMAL: {
                        responseMessageType = HeaderConstants.RESPONSE_TYPE_LOGIN_NORMAL;
                        break;
                    }
                    case HeaderConstants.REQUEST_TYPE_LOGOUT_NORMAL: {
                        responseMessageType = HeaderConstants.RESPONSE_TYPE_LOGOUT_NORMAL;
                        break;
                    }
                }
                break;
            }
        }
        return responseMessageType;
    }

}
