package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.bo_controller.helpers.BusinessUserHelper;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust.*;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class BusinessUserHandler implements MessageHandler {

    private static final Logger logger = Logger.getLogger(BusinessUserHandler.class);
    private static BusinessUserHandler instance;
    private BusinessUserHelper businessUserHelper;


    private BusinessUserHandler() {
        try {
            businessUserHelper = new BusinessUserHelper();
        } catch (Exception e) {
            logger.error("Error in handler", e);
        }
    }

    public static BusinessUserHandler getInstance() {
        if (instance == null) {
            instance = new BusinessUserHandler();
        }
        return instance;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws GHGException {
        logger.info("Process Emission group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;
        switch (header.getMessageType()) {
            case HeaderConstants.REQUEST_TYPE_LIST_EMPLOYEE: {
                logger.info("");
                responseMessage = businessUserHelper.getEmployees((EmployeeListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_ADMIN: {
                logger.info("");
                responseMessage = businessUserHelper.getAdmins((AdminListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_COMADMIN: {
                logger.info("");
                responseMessage = businessUserHelper.getCadmins((CadminListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_CLIENT: {
                logger.info("");
                responseMessage = businessUserHelper.getClients((ClientListRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_EMPLOYEE: {
                logger.info("");
                responseMessage = businessUserHelper.manageEmployee((EmployeeMangedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_ADMIN: {
                logger.info("");
                responseMessage = businessUserHelper.manageAdmin((AdminManagedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_COMADMIN: {
                logger.info("");
                responseMessage = businessUserHelper.manageCadmin((CadminManagedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_CLIENT: {
                logger.info("");
                responseMessage = businessUserHelper.manageClient((ClientManagedRequestMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_MANAGED_DATA_ENTRY_USER: {
                responseMessage = businessUserHelper.manageDataEntryUser((DataEntryUserManagedReqMsg) message, user);
                break;
            }
            case HeaderConstants.REQUEST_TYPE_LIST_DATA_ENTRY_USER: {
                responseMessage = businessUserHelper.getDataEntryUsers((DataEntryUserListReqMsg) message, user);
                break;
            }
            default:
                logger.error("Unknown Message Type");
                responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_UNKNOWN_MSG;
                responseNarration = "Unknown Message Type";
                break;
        }
        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }
}
