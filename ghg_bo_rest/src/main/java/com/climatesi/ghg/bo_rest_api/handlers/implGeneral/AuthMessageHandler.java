package com.climatesi.ghg.bo_rest_api.handlers.implGeneral;

import com.climatesi.ghg.api.LoginResponseStatus;
import com.climatesi.ghg.api.beans.AuthReply;
import com.climatesi.ghg.bo_controller.helpers.AuthMessageHelper;
import com.climatesi.ghg.bo_controller.populators.AuthMessagePopulator;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.authentication.AuthenticateRequestBean;
import com.climatesi.ghg.bo_rest_api.SessionManager;
import com.climatesi.ghg.bo_rest_api.handlers.api.MessageHandler;
import com.climatesi.ghg.bo_rest_api.json.ResponseGenerator;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.climatesi.ghg.utility.api.exceptions.OperationNotSupportException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.List;

public class AuthMessageHandler implements MessageHandler {
    private static AuthMessageHandler authMessageHandler = null;
    private static Logger logger = Logger.getLogger(AuthMessageHandler.class);
    private AuthMessageHelper authMessageHelper;

    private AuthMessageHandler() {
        authMessageHelper = new AuthMessageHelper();
    }

    public static AuthMessageHandler getAuthMessageHandler() {
        if (authMessageHandler == null) {
            authMessageHandler = new AuthMessageHandler();
        }

        return authMessageHandler;
    }

    @Override
    public Envelope processMessage(Header header, Message message, int user) throws OperationNotSupportException {
        logger.info("Process authentication group request. Message Type >> " + header.getMessageType());

        Header responseHeader = ResponseGenerator.getInstance().getDefaultResponseHeader(header);
        Message responseMessage = null;
        Envelope response;
        int responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL;
        String responseNarration = null;

        switch (header.getMessageType()) {
            case HeaderConstants.REQUEST_TYPE_LOGIN_NORMAL:
                logger.info("REQUEST_TYPE_LOGIN_NORMAL");
                AuthReply authReply = authMessageHelper.doLogin((AuthenticateRequestBean) message, header, user);
                //Add to session cache
                if (authReply.getLoginStatus().equals(LoginResponseStatus.SUCCESS.getCode())) {
                    //Remove invalidated sessions
                    if (authReply.getRemovedSessions() != null) {
                        removeFromCache(authReply.getRemovedSessions());
                    }
                    //Add to cache
                    SessionManager.getInstance().addToSessions(authReply.getUserId(), authReply.getSessionId());
                }
                //Create response message
                AuthMessagePopulator populator = new AuthMessagePopulator();
                responseMessage = populator.populateAuthenticationResponse(authReply);
                break;
            case HeaderConstants.REQUEST_TYPE_LOGOUT_NORMAL:
                logger.info("REQUEST_TYPE_LOGOUT_NORMAL");
                int userId = SessionManager.getInstance().validateUserSession(header.getSessionId());
                if (userId != 0) {
                    ListResult<String> delSessions = authMessageHelper.doLogoutAT(userId); // Todo : User loginId Implementation
                    if (delSessions != null) {
                        removeFromCache(delSessions.getList());
                    }
                }else{
                    logger.info("Valid session not found for the user ");
                }
                break;
            default:
                logger.error("Unknown Message Type");
                responseStatus = HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_UNKNOWN_MSG;
                responseNarration = "Unknown Message Type";
                break;
        }

        responseHeader.setResponseStatus(responseStatus);
        if (responseNarration != null) {
            responseHeader.setResponseNarration(responseNarration);
        }

        response = new EnvelopeBean((HeaderBean) responseHeader, responseMessage);

        return response;
    }

    @Override
    public Envelope processMessage(Header header, JsonObject message, int user) throws GHGException {
        return null;
    }

    private void removeFromCache(List<String> sessionsList) {
        for (String delSession : sessionsList) {
            logger.info("Deleted Session Id >> " + delSession);
            if (SessionManager.getInstance().validateUserSession(delSession) != 0) {
                logger.info("Removing session from cache >> " + delSession);
                SessionManager.getInstance().removeFromCache(delSession);
            }
        }
    }
}
