package com.climatesi.ghg.bo_rest_api;

import org.jboss.logging.Logger;

import java.util.HashMap;
import java.util.Map;

public class SessionManager {

    private static Logger logger = Logger.getLogger(SessionManager.class.getName());
    private static SessionManager instance = null;
    private static Map<String, Integer> userSessions = new HashMap<>();

    private SessionManager() {
        //Left empty intentionally
    }

    public static SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }

        return instance;
    }

    public boolean addToSessions(int userId, String sessionID) {
        logger.info("Added User into sessions, sessionId => " + sessionID);
        if (userId == 0) {
            logger.error("User is null");
            return false;
        }

        if (userSessions.containsKey(sessionID)) {
            logger.info("SessionId is already exists , removing sessionId");
            userSessions.remove(sessionID);
        }
        userSessions.put(sessionID, userId);
        return true;
    }

    public boolean removeFromCache(String sessionId) {
        if (userSessions.containsKey(sessionId)) {
            userSessions.remove(sessionId);
            return true;
        }

        return false;
    }

    public int validateUserSession(String sessionId) {
        int userId = 0;
        logger.info("Session validate for session id >> " + sessionId);

        if (userSessions.containsKey(sessionId)) {
            userId = userSessions.get(sessionId);
            logger.info("User is in active sessions. Employee Id >> " + userId);
        }

        return userId;
    }
}
