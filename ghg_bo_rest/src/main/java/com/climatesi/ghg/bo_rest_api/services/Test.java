package com.climatesi.ghg.bo_rest_api.services;


import org.jboss.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("test")
public class Test {

    private static Logger logger = Logger.getLogger(Test.class.getName());


    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTestResult() {
        logger.info("Requested Test Service");
//        try {
//            boolean v = GHGControllerFactory.getInstance().getTestController().isEntityMangerNull();
//            return Response.ok(v).build();
//        } catch(Exception e) {
//            return Response.ok(e.getMessage()).build();
//        }
        try {
            Object result = "";
//          Object result = GHGControllerFactory.getInstance().getUserLoginController().addUserLoginProfile();
          return  Response.status(Response.Status.BAD_REQUEST)
                  .entity(result).build();
        } catch (Exception e) {
            return Response.ok("error", e.getMessage()).build();
        }

    }

}
