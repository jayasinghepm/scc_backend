# Build
mvn clean package && docker build -t com.climatesi.ghg/ghg_bo_rest .

# RUN

docker rm -f ghg_bo_rest || true && docker run -d -p 8080:8080 -p 4848:4848 --name ghg_bo_rest com.climatesi.ghg/ghg_bo_rest 