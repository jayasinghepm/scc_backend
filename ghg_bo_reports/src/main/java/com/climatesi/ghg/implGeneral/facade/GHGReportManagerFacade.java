package com.climatesi.ghg.implGeneral.facade;

import com.climatesi.ghg.api.beans.GHGReport;
import com.climatesi.ghg.api.facade.GHGReportManager;
import com.climatesi.ghg.implGeneral.beans.GHGReportBean;
import com.climatesi.ghg.implGeneral.persistence.GHGReportDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class GHGReportManagerFacade implements GHGReportManager {

    private Logger logger = Logger.getLogger(GHGReportManagerFacade.class);
    private GHGReportDAO dao;

    @Override
    public Object addEntity(GHGReport entity) {
        try {
            return dao.insert((GHGReportBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding GHGReportBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(GHGReport entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(GHGReport entity) {
        try {
            return dao.insert((GHGReportBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding GHGReportBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(GHGReport entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(GHGReport entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(GHGReport entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(GHGReport entity) {
        return null;
    }

    @Override
    public GHGReport getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<GHGReport> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<GHGReport> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public GHGReport getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<GHGReport> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<GHGReport> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new GHGReportDAO(entityManager);
    }

    @Override
    public int getLastSortedProjectId() throws Exception {
        try {
            int pId = dao.getLastSortedProjectId();
            return pId;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public int deleteReport(int id) {
        try {
            return this.dao.deleteReport(id);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
