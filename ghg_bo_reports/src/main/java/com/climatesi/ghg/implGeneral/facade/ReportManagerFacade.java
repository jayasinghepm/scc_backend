package com.climatesi.ghg.implGeneral.facade;

import com.climatesi.ghg.api.beans.ReportMetaData;
import com.climatesi.ghg.api.facade.ReportMetaDataManager;
import com.climatesi.ghg.implGeneral.beans.ReportMetaDataBean;
import com.climatesi.ghg.implGeneral.persistence.GHGReportDAO;
import com.climatesi.ghg.implGeneral.persistence.ReportMetaDataDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class ReportManagerFacade implements ReportMetaDataManager {

    private Logger logger = Logger.getLogger(ReportManagerFacade.class);
    private ReportMetaDataDAO dao;

    @Override
    public Object addEntity(ReportMetaData entity) {
        try {
            return dao.insert((ReportMetaDataBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding report meta data Bean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }



    @Override
    public Object addEntity(ReportMetaData entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(ReportMetaData entity) {
        try {
            return dao.update((ReportMetaDataBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while updating ReportMetaDataBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(ReportMetaData entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(ReportMetaData entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(ReportMetaData entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(ReportMetaData entity) {
        return null;
    }

    @Override
    public ReportMetaData getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<ReportMetaData> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<ReportMetaData> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public ReportMetaData getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<ReportMetaData> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<ReportMetaData> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);

    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new ReportMetaDataDAO(entityManager);
    }
}
