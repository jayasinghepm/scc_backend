package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.GHGReport;
import com.climatesi.ghg.implGeneral.beans.GHGReportBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

public class GHGReportDAO extends AbstractDAO<GHGReportBean, GHGReport> {
    private EntityManager em;
    private static Logger logger = Logger.getLogger(GHGReportDAO.class);


    public GHGReportDAO(EntityManager em) {
        super(em, GHGReportBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("GHG_REPORT", "projectId", "GHGReportBean");
        meta.setKeyPropertyName("projectId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public int getLastSortedProjectId() throws Exception {
        Integer pId = 0;
        try {
            Query query = em.createQuery("Select r.projectId from GHGReportBean r ORDER BY r.projectId asc");
            query.setMaxResults(1);
            pId = (Integer) query.getSingleResult();
//            return pId;
        }catch (Exception e) {
            logger.error(e);
//            throw  new Exception("Error occured in getting last project id.");
        }
        return pId;
    }

    public int deleteReport(int id) {
        try {
//        em.getTransaction().begin();
        Query query = em.createQuery("Delete  from GHGReportBean l where l.projectId = :id");
        query.setParameter("id", id);
        int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
        logger.error(e.getMessage());
    }
        return id;
    }

    @Override
    public Map<String, Object> getAddSpParams(GHGReportBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(GHGReportBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(GHGReportBean entityImpl) throws GHGException {
        return null;
    }
}
