package com.climatesi.ghg.implGeneral.beans;

import com.climatesi.ghg.api.beans.ReportMetaData;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapStrngListStringConverter;
import com.climatesi.ghg.utility.implGeneral.persistence.JsonObjectListConverter;
import com.climatesi.ghg.utility.implGeneral.persistence.StringListConverter;
import com.google.gson.JsonObject;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;

@Entity
@Table(name = "REPORT_META_DATA")
@NamedQueries({
        @NamedQuery(name = "ReportMetaDataBean.findById",
                query = "SELECT i FROM ReportMetaDataBean i WHERE i.id = :id"),
})
public class ReportMetaDataBean implements ReportMetaData {

    @Id
    @GeneratedValue(generator = "reportMetaDataIdSeq")
    @SequenceGenerator(name = "reportMetaDataIdSeq", sequenceName = "REPORT_META_DATA_SEQ", initialValue = 1, allocationSize = 1)
    private int id;

    private int projectId;

    @Lob
    private String comName_header;

    @Lob
    private String comName_address;

    @Lob
    private String addr1;

    @Lob
    private String addr2;

    @Lob
    private String district;

    @Lob
    private String comRegNo;


    private String email;

    private String telphone;

    private String fax;

    private String climateSi_Name;

    @Lob
    private String climateSi_addr1;

    @Lob
    private String climateSi_addr2;


    private String climateSi_district;

    private String climateSi_email;

    private String climaeSi_telephone;

    private String climatesi_fax;

    private String year;

    private String yearStartDate;

    private String yearEndDate;

    private int attempt;

    @Lob
    @Convert(converter = HashMapStrngListStringConverter.class)
    private HashMap<String, List<String>> excludedSrcAndReasons;

    private String proposedDate;

    @Lob
    private String desCompany;

    @Lob
    private String desResUnit;

    @Lob
    private String desEnvironmentalMgmntFramework;

    private String isoStandard;


    private String previousYearISOStandard;

    @Lob
    @Convert(converter = StringListConverter.class)
    private List<String> suggestions;

    @Lob
    private String desGHGMitigationActions;

    @Lob
    @Convert(converter = StringListConverter.class)
    private List<String> nextSteps;

    @Lob
    @Convert(converter = StringListConverter.class)
    private List<String> products;

    private int isDeleted;
    @Lob
    private String figEnvMgmtFramework;
    @Lob
    private String figResUnit;
    @Lob
    private String srcEnvMgmtFramework;
    @Lob
    private String srcResUnit;

    @Lob
    @Convert(converter = JsonObjectListConverter.class)
    private JsonObject previousYearEmission;

    @Lob
    @Convert(converter = JsonObjectListConverter.class)
    private JsonObject previousYearEfs;

    @Lob
    private String gr_dir_v_indir;
    @Lob
    private String gr_result_by_src;
    @Lob
    private String gr_pie_direct;
    @Lob
    private String gr_pie_indirect;
    @Lob
    private String gr_com_ghg;
    @Lob
    private String gr_com_direct_by_src;
    @Lob
    private String gr_com_indirect_by_src;
    @Lob
    private String gr_percaptia;
    @Lob
    private String gr_intensity;
    @Lob
    private String gr_per_production;
    @Lob
    @Convert(converter = StringListConverter.class)
    private JsonObject previousYearStats;





    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getComName_header() {
        return comName_header;
    }

    public void setComName_header(String comName_header) {
        this.comName_header = comName_header;
    }

    public String getComName_address() {
        return comName_address;
    }

    public void setComName_address(String comName_address) {
        this.comName_address = comName_address;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getComRegNo() {
        return comRegNo;
    }

    public void setComRegNo(String comRegNo) {
        this.comRegNo = comRegNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getClimateSi_Name() {
        return climateSi_Name;
    }

    public void setClimateSi_Name(String climateSi_Name) {
        this.climateSi_Name = climateSi_Name;
    }

    public String getClimateSi_addr1() {
        return climateSi_addr1;
    }

    public void setClimateSi_addr1(String climateSi_addr1) {
        this.climateSi_addr1 = climateSi_addr1;
    }

    public String getClimateSi_addr2() {
        return climateSi_addr2;
    }

    public void setClimateSi_addr2(String climateSi_addr2) {
        this.climateSi_addr2 = climateSi_addr2;
    }

    public String getClimateSi_district() {
        return climateSi_district;
    }

    public void setClimateSi_district(String climateSi_district) {
        this.climateSi_district = climateSi_district;
    }

    public String getClimateSi_email() {
        return climateSi_email;
    }

    public void setClimateSi_email(String climateSi_email) {
        this.climateSi_email = climateSi_email;
    }

    public String getClimaeSi_telephone() {
        return climaeSi_telephone;
    }

    public void setClimaeSi_telephone(String climaeSi_telephone) {
        this.climaeSi_telephone = climaeSi_telephone;
    }

    public String getClimatesi_fax() {
        return climatesi_fax;
    }

    public void setClimatesi_fax(String climatesi_fax) {
        this.climatesi_fax = climatesi_fax;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYearStartDate() {
        return yearStartDate;
    }

    public void setYearStartDate(String yearStartDate) {
        this.yearStartDate = yearStartDate;
    }

    public String getYearEndDate() {
        return yearEndDate;
    }

    public void setYearEndDate(String yearEndDate) {
        this.yearEndDate = yearEndDate;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public HashMap<String, List<String>> getExcludedSrcAndReasons() {
        return excludedSrcAndReasons;
    }

    public void setExcludedSrcAndReasons(HashMap<String, List<String>> excludedSrcAndReasons) {
        this.excludedSrcAndReasons = excludedSrcAndReasons;
    }

    public String getProposedDate() {
        return proposedDate;
    }

    public void setProposedDate(String proposedDate) {
        this.proposedDate = proposedDate;
    }

    public String getDesCompany() {
        return desCompany;
    }

    public void setDesCompany(String desCompany) {
        this.desCompany = desCompany;
    }

    public String getDesResUnit() {
        return desResUnit;
    }

    public void setDesResUnit(String desResUnit) {
        this.desResUnit = desResUnit;
    }

    public String getDesEnvironmentalMgmntFramework() {
        return desEnvironmentalMgmntFramework;
    }

    public void setDesEnvironmentalMgmntFramework(String desEnvironmentalMgmntFramework) {
        this.desEnvironmentalMgmntFramework = desEnvironmentalMgmntFramework;
    }

    public String getIsoStandard() {
        return isoStandard;
    }

    public void setIsoStandard(String isoStandard) {
        this.isoStandard = isoStandard;
    }

    public String getPreviousYearISOStandard() {
        return previousYearISOStandard;
    }

    public void setPreviousYearISOStandard(String previousYearISOStandard) {
        this.previousYearISOStandard = previousYearISOStandard;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    public String getDesGHGMitigationActions() {
        return desGHGMitigationActions;
    }

    public void setDesGHGMitigationActions(String desGHGMitigationActions) {
        this.desGHGMitigationActions = desGHGMitigationActions;
    }

    public List<String> getNextSteps() {
        return nextSteps;
    }

    public void setNextSteps(List<String> nextSteps) {
        this.nextSteps = nextSteps;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public String getFigEnvMgmtFramework() {
        return figEnvMgmtFramework;
    }

    public void setFigEnvMgmtFramework(String figEnvMgmtFramework) {
        this.figEnvMgmtFramework = figEnvMgmtFramework;
    }

    public String getFigResUnit() {
        return figResUnit;
    }

    public void setFigResUnit(String figResUnit) {
        this.figResUnit = figResUnit;
    }

    public String getSrcEnvMgmtFramework() {
        return srcEnvMgmtFramework;
    }

    public void setSrcEnvMgmtFramework(String srcEnvMgmtFramework) {
        this.srcEnvMgmtFramework = srcEnvMgmtFramework;
    }

    public String getSrcResUnit() {
        return srcResUnit;
    }

    public void setSrcResUnit(String srcResUnit) {
        this.srcResUnit = srcResUnit;
    }

    public JsonObject getPreviousYearEmission() {
        return previousYearEmission;
    }

    public void setPreviousYearEmission(JsonObject previousYearEmission) {
        this.previousYearEmission = previousYearEmission;
    }

    public JsonObject getPreviousYearEfs() {
        return previousYearEfs;
    }

    public void setPreviousYearEfs(JsonObject previousYearEfs) {
        this.previousYearEfs = previousYearEfs;
    }


    public String getGr_dir_v_indir() {
        return gr_dir_v_indir;
    }

    public void setGr_dir_v_indir(String gr_dir_v_indir) {
        this.gr_dir_v_indir = gr_dir_v_indir;
    }

    public String getGr_result_by_src() {
        return gr_result_by_src;
    }

    public void setGr_result_by_src(String gr_result_by_src) {
        this.gr_result_by_src = gr_result_by_src;
    }

    public String getGr_pie_direct() {
        return gr_pie_direct;
    }

    public void setGr_pie_direct(String gr_pie_direct) {
        this.gr_pie_direct = gr_pie_direct;
    }

    public String getGr_pie_indirect() {
        return gr_pie_indirect;
    }

    public void setGr_pie_indirect(String gr_pie_indirect) {
        this.gr_pie_indirect = gr_pie_indirect;
    }

    public String getGr_com_ghg() {
        return gr_com_ghg;
    }

    public void setGr_com_ghg(String gr_com_ghg) {
        this.gr_com_ghg = gr_com_ghg;
    }

    public String getGr_com_direct_by_src() {
        return gr_com_direct_by_src;
    }

    public void setGr_com_direct_by_src(String gr_com_direct_by_src) {
        this.gr_com_direct_by_src = gr_com_direct_by_src;
    }

    public String getGr_com_indirect_by_src() {
        return gr_com_indirect_by_src;
    }

    public void setGr_com_indirect_by_src(String gr_com_indirect_by_src) {
        this.gr_com_indirect_by_src = gr_com_indirect_by_src;
    }

    public String getGr_percaptia() {
        return gr_percaptia;
    }

    public void setGr_percaptia(String gr_percaptia) {
        this.gr_percaptia = gr_percaptia;
    }

    public String getGr_intensity() {
        return gr_intensity;
    }

    public void setGr_intensity(String gr_intensity) {
        this.gr_intensity = gr_intensity;
    }

    public String getGr_per_production() {
        return gr_per_production;
    }

    public void setGr_per_production(String gr_per_production) {
        this.gr_per_production = gr_per_production;
    }

    public JsonObject getPreviousYearStats() {
        return previousYearStats;
    }

    public void setPreviousYearStats(JsonObject previousYearStats) {
        this.previousYearStats = previousYearStats;
    }
}
