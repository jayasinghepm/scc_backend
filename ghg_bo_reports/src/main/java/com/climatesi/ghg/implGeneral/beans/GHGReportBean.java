package com.climatesi.ghg.implGeneral.beans;

import com.climatesi.ghg.api.beans.GHGReport;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "GHG_REPORT")
@NamedQueries({
        @NamedQuery(name = "GHGReportBean.findById",
                query = "SELECT i FROM GHGReportBean i WHERE i.projectId = :projectId"),
})
public class GHGReportBean implements GHGReport {

   
    @Id
    private int projectId;

    private int comId;

    private Date addedDate;

    @Lob
    private String pdf;

    private int reportType;

    private String year;

    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }
}
