package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.ReportMetaData;
import com.climatesi.ghg.implGeneral.beans.ReportMetaDataBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;

import javax.persistence.EntityManager;
import java.util.Map;

public class ReportMetaDataDAO  extends AbstractDAO<ReportMetaDataBean, ReportMetaData> {

    private EntityManager em;

    public ReportMetaDataDAO(EntityManager em) {
        super(em, ReportMetaDataBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("REPORT_META_DATA", "id", "ReportMetaDataBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(ReportMetaDataBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(ReportMetaDataBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(ReportMetaDataBean entityImpl) throws GHGException {
        return null;
    }
}
