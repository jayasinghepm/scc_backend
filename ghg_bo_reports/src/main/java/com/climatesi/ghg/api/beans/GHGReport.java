package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface GHGReport {



    public int getProjectId();

    public void setProjectId(int projectId);

    public int getComId();

    public void setComId(int comId);

    public Date getAddedDate() ;

    public void setAddedDate(Date addedDate);

    public String getPdf();

    public void setPdf(String pdf);

    public int getReportType();

    public void setReportType(int reportType);

    public int isDeleted();

    public void setDeleted(int deleted);

    public String getYear();

    public void setYear(String year);
}
