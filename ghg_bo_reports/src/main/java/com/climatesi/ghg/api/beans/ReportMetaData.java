package com.climatesi.ghg.api.beans;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

public interface ReportMetaData {

    public int getId();

    public void setId(int id);

    public int getProjectId();

    public void setProjectId(int projectId);

    public String getComName_header();

    public void setComName_header(String comName_header);

    public String getComName_address();

    public void setComName_address(String comName_address);

    public String getAddr1();

    public void setAddr1(String addr1);

    public String getAddr2();

    public void setAddr2(String addr2);

    public String getDistrict();

    public void setDistrict(String district);
    public String getComRegNo();

    public void setComRegNo(String comRegNo);

    public String getEmail();

    public void setEmail(String email);

    public String getTelphone();

    public void setTelphone(String telphone);

    public String getFax();

    public void setFax(String fax);

    public String getClimateSi_Name();

    public void setClimateSi_Name(String climateSi_Name);

    public String getClimateSi_addr1();

    public void setClimateSi_addr1(String climateSi_addr1);

    public String getClimateSi_addr2();

    public void setClimateSi_addr2(String climateSi_addr2);
    public String getClimateSi_district();

    public void setClimateSi_district(String climateSi_district);

    public String getClimateSi_email();

    public void setClimateSi_email(String climateSi_email);

    public String getClimaeSi_telephone() ;

    public void setClimaeSi_telephone(String climaeSi_telephone);

    public String getClimatesi_fax();

    public void setClimatesi_fax(String climatesi_fax);

    public String getYear();

    public void setYear(String year) ;

    public String getYearStartDate() ;

    public void setYearStartDate(String yearStartDate);

    public String getYearEndDate();

    public void setYearEndDate(String yearEndDate);

    public int getAttempt();

    public void setAttempt(int attempt);

    public HashMap<String, List<String>> getExcludedSrcAndReasons() ;

    public void setExcludedSrcAndReasons(HashMap<String, List<String>> excludedSrcAndReasons);

    public String getProposedDate();

    public void setProposedDate(String proposedDate) ;

    public String getDesCompany();

    public void setDesCompany(String desCompany);

    public String getDesResUnit() ;

    public void setDesResUnit(String desResUnit);

    public String getDesEnvironmentalMgmntFramework() ;

    public void setDesEnvironmentalMgmntFramework(String desEnvironmentalMgmntFramework);

    public String getIsoStandard();

    public void setIsoStandard(String isoStandard);

    public String getPreviousYearISOStandard();

    public void setPreviousYearISOStandard(String previousYearISOStandard);

    public List<String> getSuggestions();

    public void setSuggestions(List<String> suggestions);

    public String getDesGHGMitigationActions() ;

    public void setDesGHGMitigationActions(String desGHGMitigationActions) ;

    public List<String> getNextSteps();

    public void setNextSteps(List<String> nextSteps);

    public List<String> getProducts() ;

    public void setProducts(List<String> products);

    public int isDeleted();

    public void setDeleted(int deleted);

    public String getFigEnvMgmtFramework();

    public void setFigEnvMgmtFramework(String figEnvMgmtFramework);

    public String getFigResUnit();

    public void setFigResUnit(String figResUnit);

    public String getSrcEnvMgmtFramework();

    public void setSrcEnvMgmtFramework(String srcEnvMgmtFramework);

    public String getSrcResUnit();

    public void setSrcResUnit(String srcResUnit);

    public JsonObject getPreviousYearEmission();

    public void setPreviousYearEmission(JsonObject previousYearEmission);

    public JsonObject getPreviousYearEfs();

    public void setPreviousYearEfs(JsonObject previousYearEfs);

    public String getGr_dir_v_indir();

    public void setGr_dir_v_indir(String gr_dir_v_indir);

    public String getGr_result_by_src();

    public void setGr_result_by_src(String gr_result_by_src);

    public String getGr_pie_direct();

    public void setGr_pie_direct(String gr_pie_direct);

    public String getGr_pie_indirect();

    public void setGr_pie_indirect(String gr_pie_indirect);

    public String getGr_com_ghg();

    public void setGr_com_ghg(String gr_com_ghg);

    public String getGr_com_direct_by_src();

    public void setGr_com_direct_by_src(String gr_com_direct_by_src);

    public String getGr_com_indirect_by_src();

    public void setGr_com_indirect_by_src(String gr_com_indirect_by_src);
    public String getGr_percaptia();

    public void setGr_percaptia(String gr_percaptia);

    public String getGr_intensity();

    public void setGr_intensity(String gr_intensity);

    public String getGr_per_production();

    public void setGr_per_production(String gr_per_production);
    public JsonObject getPreviousYearStats();

    public void setPreviousYearStats(JsonObject previousYearStats);
}
