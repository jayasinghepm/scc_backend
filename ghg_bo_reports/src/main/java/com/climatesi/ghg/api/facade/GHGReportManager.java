package com.climatesi.ghg.api.facade;

import com.climatesi.ghg.api.beans.GHGReport;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface  GHGReportManager  extends DataEntryManager<GHGReport, Integer> {

    int getLastSortedProjectId() throws Exception;

    int deleteReport(int id) ;
}
