package com.climatesi.ghg.api.consts;

public class ReportType {

    public final static int DRAFT =1;
    public final static int FINAL =2;
}
