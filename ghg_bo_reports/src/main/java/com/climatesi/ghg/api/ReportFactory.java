package com.climatesi.ghg.api;

import com.climatesi.ghg.api.beans.ReportMetaData;
import com.climatesi.ghg.api.facade.GHGReportManager;
import com.climatesi.ghg.api.facade.ReportMetaDataManager;
import com.climatesi.ghg.implGeneral.facade.GHGReportManagerFacade;
import com.climatesi.ghg.implGeneral.facade.ReportManagerFacade;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class ReportFactory {

    public static Logger logger = Logger.getLogger(ReportFactory.class);
    private static ReportFactory instance;
    private GHGReportManager ghgReportManager;
    private ReportMetaDataManager reportMetaDataManager;

    private ReportFactory() {

    }

    public static ReportFactory getInstance() {
        if (instance == null) {
            instance = new ReportFactory();
        }
        return instance;
    }

    public GHGReportManager getGHGReportManager(EntityManager em) {
        if (ghgReportManager == null) {
            ghgReportManager = new GHGReportManagerFacade();
            ghgReportManager.injectEntityManager(em);
        }
        return ghgReportManager;
    }

    public ReportMetaDataManager getReportMetaDataManager(EntityManager em) {
        if (reportMetaDataManager == null) {
            reportMetaDataManager = new ReportManagerFacade();
            reportMetaDataManager.injectEntityManager(em);
        }
        return reportMetaDataManager;
    }

}
