package com.climatesi.ghg.api.facade;

import com.climatesi.ghg.api.beans.ReportMetaData;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface ReportMetaDataManager extends DataEntryManager<ReportMetaData, Integer> {
}
