package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.WebNotification;
import com.climatesi.ghg.implGeneral.beans.WebNotificationBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;

import javax.persistence.EntityManager;
import java.util.Map;

public class WebNotificationDAO extends AbstractDAO<WebNotificationBean, WebNotification> {
    private EntityManager em;
    public WebNotificationDAO(EntityManager em) {
        super(em, WebNotificationBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("WEB_NOTIFICATION", "ID", "WebNotificationBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(WebNotificationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(WebNotificationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(WebNotificationBean entityImpl) throws GHGException {
        return null;
    }
}
