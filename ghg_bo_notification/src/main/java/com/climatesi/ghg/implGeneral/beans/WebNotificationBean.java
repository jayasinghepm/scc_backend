package com.climatesi.ghg.implGeneral.beans;

import com.climatesi.ghg.api.beans.WebNotification;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "WEB_NOTIFICATION")
@NamedQueries({
        @NamedQuery(name = "WebNotificationBean.findById",
                query = "SELECT i FROM WebNotificationBean i WHERE i.id = :id"),
})
public class WebNotificationBean implements WebNotification {

    @Id
    @GeneratedValue(generator = "webNotificationIdSeq")
    @SequenceGenerator(name = "webNotificationIdSeq", sequenceName = "WEB_NOTIFI_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "ID")
    private long id;

    @Column(name="MESSAGE")
    private String message;

    @Column(name="ADDED_DATE")
    private Date addedDate;

    @Column(name="IS_READ")
    private int read;

    @Column(name="LINK")
    private String link;

    @Column(name="USER_ID")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

}
