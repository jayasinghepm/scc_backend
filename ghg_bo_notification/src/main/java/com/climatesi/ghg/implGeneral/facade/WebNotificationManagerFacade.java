package com.climatesi.ghg.implGeneral.facade;

import com.climatesi.ghg.api.beans.WebNotification;
import com.climatesi.ghg.api.facade.WebNotificationManger;
import com.climatesi.ghg.implGeneral.beans.WebNotificationBean;
import com.climatesi.ghg.implGeneral.persistence.WebNotificationDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class WebNotificationManagerFacade implements WebNotificationManger {
    private WebNotificationDAO dao;
    private static Logger logger = Logger.getLogger(WebNotificationManagerFacade.class);

    @Override
    public Object addEntity(WebNotification entity) {
        try {
            return  dao.insert((WebNotificationBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding Web Notifaication ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(WebNotification entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(WebNotification entity) {
        try {
            return  dao.update((WebNotificationBean) entity);
        } catch (Exception e) {
            logger.error("Error occured  while updating adding Web Notifaication ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(WebNotification entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(WebNotification entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(WebNotification entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(WebNotification entity) {
        return null;
    }

    @Override
    public WebNotification getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<WebNotification> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<WebNotification> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public WebNotification getEntityByKey(Long id) {
        return dao.findByKey(id);
    }



    @Override
    public ListResult<WebNotification> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<WebNotification> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);


    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new WebNotificationDAO(entityManager);
    }
}
