package com.climatesi.ghg.api.facade;

import com.climatesi.ghg.api.beans.WebNotification;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface WebNotificationManger extends DataEntryManager<WebNotification, Long> {
}
