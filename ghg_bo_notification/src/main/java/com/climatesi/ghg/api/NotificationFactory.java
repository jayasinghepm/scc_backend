package com.climatesi.ghg.api;

import com.climatesi.ghg.api.facade.WebNotificationManger;
import com.climatesi.ghg.implGeneral.facade.WebNotificationManagerFacade;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class NotificationFactory {

    private static Logger logger = Logger.getLogger(NotificationFactory.class);

    private static NotificationFactory instance;
    private static WebNotificationManger webNotificationManger;

    private NotificationFactory () {

    }

    public static NotificationFactory getInstance() {
        if (instance == null) {
            instance = new NotificationFactory();
        }
        return instance;
    }

    public WebNotificationManger getWebNotificationManger(EntityManager em) {
        if (webNotificationManger == null) {
            webNotificationManger = new WebNotificationManagerFacade();
            webNotificationManger.injectEntityManager(em);
        }
        return webNotificationManger;
    }
}
