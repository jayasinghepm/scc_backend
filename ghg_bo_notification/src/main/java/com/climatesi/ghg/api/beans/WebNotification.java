package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface WebNotification {

    public long getId();

    public void setId(long id);
    public String getMessage();

    public void setMessage(String message);

    public Date getAddedDate();

    public void setAddedDate(Date addedDate);

    public int getRead();

    public void setRead(int read);

    public String getLink();

    public void setLink(String link);


    public int isDeleted();

    public void setDeleted(int deleted);

    public int getUserId();

    public void setUserId(int userId);
}
