package com.climatesi.ghg.business_users.api.beans;

import java.util.Date;
import java.util.List;

public interface Client {

    public int isDeleted();

    public void setDeleted(int deleted);


    public String getSector();

    public void setSector(String sector);

    public Date getAgreementStartDate();

    public void setAgreementStartDate(Date agreementStartDate);

    public Date getAgreementEndDate();

    public void setAgreementEndDate(Date agreementEndDate);

    public String getClientCode();

    public void setClientCode(String clientCode);

    public int getId();

    public void setId(int id);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public String getTitle();

    public void setTitle(String title);

    public String getEmail();

    public void setEmail(String email);

    public String getTelephoneNo();

    public void setTelephoneNo(String telephoneNo);

    public String getMobileNo();

    public void setMobileNo(String mobileNo);

    public int getAddedBy();

    public void setAddedBy(int addedBy);

    public Date getAddedDate();

    public void setAddedDate(Date addedDate);

    public int getUserId();

    public void setUserId(int userId);

    public int getBranchId();

    public void setBranchId(int branchId);

    public int getCompanyId();

    public void setCompanyId(int companyId);

    public List<String> getEntitlements();

    public void setEntitlements(List<String> entitlements);

    public String getBranch();

    public void setBranch(String branch);

    public String getCompany();

    public void setCompany(String company);
}
