package com.climatesi.ghg.business_users.api.facades;

import com.climatesi.ghg.business_users.api.beans.DataEntryUser;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface DataEntryUserManager extends DataEntryManager<DataEntryUser, Integer> {

    int deleteDataEntryUser(int id);
}
