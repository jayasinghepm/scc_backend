package com.climatesi.ghg.business_users.api;

import com.climatesi.ghg.business_users.api.facades.*;
import com.climatesi.ghg.business_users.implGeneral.facades.*;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class BusinessUserFactory {

    private static Logger logger = Logger.getLogger(BusinessUserFactory.class);
    private static BusinessUserFactory instance;
    private EmployeerManager employeerManager;
    private AdminManager adminManager;
    private ClientManager clientManager;
    private CadminManager cadminManager;
    private DataEntryUserManager dataEntryUserManager;

    public static BusinessUserFactory getInstance() {
        if (instance == null) {
            instance = new BusinessUserFactory();
        }
        return instance;
    }

    public EmployeerManager getEmployeerManager(EntityManager em) {
        if (employeerManager == null) {
            employeerManager = new EmployeeManagerFacade();
            employeerManager.injectEntityManager(em);
        }
        return employeerManager;
    }

    public DataEntryUserManager getDataEntryUserManager(EntityManager em) {
        if (dataEntryUserManager == null) {
            dataEntryUserManager = new DataEntryUserFacade();
            dataEntryUserManager.injectEntityManager(em);
        }
        return dataEntryUserManager;
    }

    public AdminManager getAdminManager(EntityManager em) {
        if (adminManager == null) {
            adminManager = new AdminManagerFacade();
            adminManager.injectEntityManager(em);
        }
        return adminManager;
    }

    public ClientManager getClientManager(EntityManager em) {
        if (clientManager == null) {
            clientManager = new ClientManagerFacade();
        }
        clientManager.injectEntityManager(em);
        return clientManager;
    }

    public CadminManager getCadminManager(EntityManager em) {
        if (cadminManager == null) {
            cadminManager = new CadminManagerFacade();
        }
        cadminManager.injectEntityManager(em);
        return cadminManager;
    }


}
