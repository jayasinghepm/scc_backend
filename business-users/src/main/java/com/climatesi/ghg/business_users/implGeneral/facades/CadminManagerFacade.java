package com.climatesi.ghg.business_users.implGeneral.facades;

import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.business_users.api.facades.CadminManager;
import com.climatesi.ghg.business_users.implGeneral.beans.ComAdminBean;
import com.climatesi.ghg.business_users.implGeneral.persistence.CadminDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class CadminManagerFacade implements CadminManager {

    private static Logger logger = Logger.getLogger(CadminManagerFacade.class);
    private CadminDAO dao;

    @Override
    public Object addEntity(ComAdmin entity) {
        try {
            return dao.insert((ComAdminBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding Comadmin ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(ComAdmin entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(ComAdmin entity) {
        try {
            return dao.insert((ComAdminBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while updating Comadmin ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(ComAdmin entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(ComAdmin entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(ComAdmin entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(ComAdmin entity) {
        return null;
    }

    @Override
    public ComAdmin getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<ComAdmin> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<ComAdmin> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public ComAdmin getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<ComAdmin> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<ComAdmin> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new CadminDAO(entityManager);
    }

    @Override
    public int deleteCadmin(int id) {
        try {
            return dao.deleteCadmin(id);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
