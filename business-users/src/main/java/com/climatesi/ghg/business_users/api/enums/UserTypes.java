package com.climatesi.ghg.business_users.api.enums;

public class UserTypes {

    public static final int ADMIN = 3;
    public static final int CLIENT = 1;
    public static final int CADMIN = 2;
    public static final int DATA_ENTRY_USER = 4;
}
