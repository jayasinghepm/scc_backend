package com.climatesi.ghg.business_users.implGeneral.beans;

import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.utility.implGeneral.persistence.StringListConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "BUS_USERS_CADMIN")
@NamedQueries({
        @NamedQuery(name = "ComAdminBean.findById",
                query = "SELECT i FROM ComAdminBean i WHERE i.id = :id"),
})
public class ComAdminBean implements ComAdmin {


    @Id
    @GeneratedValue(generator = "cadminIdSeq")
    @SequenceGenerator(name = "cadminIdSeq", sequenceName = "CADMIN_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "CADMIN_ID")
    private int id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "TEL_NO")
    private String telephoneNo;

    @Column(name = "MOBILE_NO")
    private String mobileNo;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "POSITION")
    private String position;

    @Column(name = "USER_ID")
    private int userId;

    @Column(name = "BRANCH_ID")
    private int branchId;


    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ENTILEMENTS")
    @Convert(converter = StringListConverter.class)
    private List<String> entitlements;

    @Column(name = "STATUS")
    private int status;

    @Column(name = "STATUS_MESSAGE")
    private String statusMessage;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public List<String> getEntitlements() {
        return entitlements;
    }

    public void setEntitlements(List<String> entitlements) {
        this.entitlements = entitlements;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }
}
