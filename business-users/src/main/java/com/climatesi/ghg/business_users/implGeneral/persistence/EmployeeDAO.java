package com.climatesi.ghg.business_users.implGeneral.persistence;

import com.climatesi.ghg.business_users.api.beans.Employee;
import com.climatesi.ghg.business_users.implGeneral.beans.EmployeeBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class EmployeeDAO extends AbstractDAO<EmployeeBean, Employee> {

    private static Logger logger = Logger.getLogger(EmployeeDAO.class);
    private static EntityManager em;


    public EmployeeDAO(EntityManager em) {
        super(em, EmployeeBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("BUS_USERS_EMPLOYEE", "EMP_ID", "EmployeeBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(EmployeeBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(EmployeeBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(EmployeeBean entityImpl) throws GHGException {
        return null;
    }
}
