package com.climatesi.ghg.business_users.implGeneral.persistence;

import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.business_users.implGeneral.beans.ComAdminBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class CadminDAO extends AbstractDAO<ComAdminBean, ComAdmin> {

    private static Logger logger = Logger.getLogger(CadminDAO.class);
    private EntityManager em;


    public CadminDAO(EntityManager em) {
        super(em, ComAdminBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("BUS_USERS_CADMIN", "CADMIN_ID", "ComAdminBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(ComAdminBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(ComAdminBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(ComAdminBean entityImpl) throws GHGException {
        return null;
    }

    public int deleteCadmin(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from ComAdminBean l where l.id = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
            logger.info("Deleted Company admins : " +  rows);
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }
}
