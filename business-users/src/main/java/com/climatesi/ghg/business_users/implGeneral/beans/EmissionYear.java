package com.climatesi.ghg.business_users.implGeneral.beans;

public class EmissionYear {

    private int year;
    private String emission;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getEmission() {
        return emission;
    }

    public void setEmission(String emission) {
        this.emission = emission;
    }

}
