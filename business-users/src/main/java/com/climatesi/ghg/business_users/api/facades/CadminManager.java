package com.climatesi.ghg.business_users.api.facades;

import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface CadminManager extends DataEntryManager<ComAdmin, Integer> {

    int deleteCadmin(int id);
}
