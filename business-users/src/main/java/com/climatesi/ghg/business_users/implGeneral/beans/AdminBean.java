package com.climatesi.ghg.business_users.implGeneral.beans;

import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.utility.implGeneral.persistence.StringListConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "BUS_USERS_ADMIN")
@NamedQueries({
        @NamedQuery(name = "AdminBean.findById",
                query = "SELECT i FROM AdminBean i WHERE i.id = :id"),
})
public class AdminBean implements Admin {

    @Id
    @GeneratedValue(generator = "adminIdSeq")
    @SequenceGenerator(name = "adminIdSeq", sequenceName = "ADMIN_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "ADMIN_ID")
    private int id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "TEL_NO")
    private String telephoneNo;

    @Column(name = "MOBILE_NO")
    private String mobileNo;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "POSITION")
    private String position;

    @Column(name = "USER_ID")
    private int userId;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    @Column(name = "ENTILEMENTS")
    @Convert(converter = StringListConverter.class)
    private List<String> entitlements;


    @Column(name = "ROLE")
    private int role;

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public List<String> getEntitlements() {
        return entitlements;
    }

    public void setEntitlements(List<String> entitlements) {
        this.entitlements = entitlements;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }
}
