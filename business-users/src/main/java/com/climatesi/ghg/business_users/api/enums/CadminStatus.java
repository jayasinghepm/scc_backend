package com.climatesi.ghg.business_users.api.enums;

public class CadminStatus {

    public static final int NOT_SUBMITTED = 1;
    public static final int PENDING = 2;
    public static final int APPROVED = 3;
    public static final int NEED_INFO = 4;


}
