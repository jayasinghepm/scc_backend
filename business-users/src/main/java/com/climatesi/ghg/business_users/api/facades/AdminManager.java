package com.climatesi.ghg.business_users.api.facades;

import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface AdminManager extends DataEntryManager<Admin, Integer> {

    int deletAdmin(int id);
}
