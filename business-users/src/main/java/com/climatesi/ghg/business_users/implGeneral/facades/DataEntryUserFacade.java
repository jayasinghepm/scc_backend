package com.climatesi.ghg.business_users.implGeneral.facades;

import com.climatesi.ghg.business_users.api.beans.DataEntryUser;
import com.climatesi.ghg.business_users.api.facades.DataEntryUserManager;
import com.climatesi.ghg.business_users.implGeneral.beans.DataEntryUserBean;
import com.climatesi.ghg.business_users.implGeneral.persistence.DataEntryUserDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class DataEntryUserFacade implements DataEntryUserManager {
    private static Logger logger = Logger.getLogger(DataEntryUserFacade.class);
    private DataEntryUserDAO dao;


    @Override
    public Object addEntity(DataEntryUser entity) {
        try {
            return dao.insert((DataEntryUserBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding DataEntryUserBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(DataEntryUser entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(DataEntryUser entity) {
        try {
            return dao.update((DataEntryUserBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while updating DataEntryUserBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(DataEntryUser entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(DataEntryUser entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(DataEntryUser entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(DataEntryUser entity) {
        return null;
    }

    @Override
    public DataEntryUser getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<DataEntryUser> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<DataEntryUser> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public DataEntryUser getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<DataEntryUser> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<DataEntryUser> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new DataEntryUserDAO(entityManager);
    }

    @Override
    public int deleteDataEntryUser(int id) {
        try {
            return dao.deleteDataEntryUser(id);
        } catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
