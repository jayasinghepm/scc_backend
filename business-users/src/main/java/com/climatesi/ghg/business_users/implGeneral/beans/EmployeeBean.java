package com.climatesi.ghg.business_users.implGeneral.beans;

import com.climatesi.ghg.business_users.api.beans.Employee;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "BUS_USERS_EMPLOYEE")
@NamedQueries({
        @NamedQuery(name = "EmployeeBean.findById",
                query = "SELECT i FROM EmployeeBean i WHERE i.id = :id"),
})
public class EmployeeBean implements Employee {

    @Id
    @GeneratedValue(generator = "empIdSeq")
    @SequenceGenerator(name = "empIdSeq", sequenceName = "EMP_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "EMP_ID")
    private int id;


    @Column(name = "EMP_NO")
    private String empNo;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COM_ID")
    private int comId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "IS_DELETED")
    private int isDeleted;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmpNo() {
        return empNo;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }
}
