package com.climatesi.ghg.business_users.implGeneral.persistence;

import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.implGeneral.beans.ClientBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class ClientDAO extends AbstractDAO<ClientBean, Client> {

    private static Logger logger = Logger.getLogger(ClientDAO.class);
    private EntityManager em;

    public ClientDAO(EntityManager em) {
        super(em, ClientBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("BUS_USERS_CLIENT", "CLIENT_ID", "ClientBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(ClientBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(ClientBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(ClientBean entityImpl) throws GHGException {
        return null;
    }

    public int deleteClient(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from ClientBean l where l.id = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }
}
