package com.climatesi.ghg.business_users.implGeneral.facades;

import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.facades.AdminManager;
import com.climatesi.ghg.business_users.implGeneral.beans.AdminBean;
import com.climatesi.ghg.business_users.implGeneral.persistence.AdminDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class AdminManagerFacade implements AdminManager {

    private static Logger logger = Logger.getLogger(AdminManagerFacade.class);
    private AdminDAO dao;

    @Override
    public Object addEntity(Admin entity) {
        try {
            return dao.insert((AdminBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while updating Comadmin ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(Admin entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Admin entity) {
        try {
            return dao.update((AdminBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while updating Comadmin ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(Admin entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Admin entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Admin entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Admin entity) {
        return null;
    }

    @Override
    public Admin getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Admin> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Admin> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Admin getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<Admin> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Admin> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new AdminDAO(entityManager);
    }

    @Override
    public int deletAdmin(int id) {
        try {
            return dao.deleteAdmin(id);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
