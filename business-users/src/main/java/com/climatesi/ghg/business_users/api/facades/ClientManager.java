package com.climatesi.ghg.business_users.api.facades;

import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface ClientManager extends DataEntryManager<Client, Integer> {


    int deleteClient(int id);
}
