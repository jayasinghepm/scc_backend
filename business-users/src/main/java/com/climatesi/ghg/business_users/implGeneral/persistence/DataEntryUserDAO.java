package com.climatesi.ghg.business_users.implGeneral.persistence;

import com.climatesi.ghg.business_users.api.beans.DataEntryUser;
import com.climatesi.ghg.business_users.implGeneral.beans.DataEntryUserBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class DataEntryUserDAO extends AbstractDAO<DataEntryUserBean, DataEntryUser> {

    private static Logger logger = Logger.getLogger(DataEntryUserDAO.class);
    private EntityManager em;

    public DataEntryUserDAO(EntityManager em) {
        super(em, DataEntryUserBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("BUS_USERS_DATA_ENTRY_USER", "DATA_ENTRY_USER_ID", "DataEntryUserBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(DataEntryUserBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(DataEntryUserBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(DataEntryUserBean entityImpl) throws GHGException {
        return null;
    }

    public int deleteDataEntryUser(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from DataEntryUserBean l where l.id = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }
}
