package com.climatesi.ghg.business_users.implGeneral.persistence;

import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.implGeneral.beans.AdminBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class AdminDAO extends AbstractDAO<AdminBean, Admin> {

    private static Logger logger = Logger.getLogger(AdminDAO.class);
    private EntityManager em;


    public AdminDAO(EntityManager em) {
        super(em, AdminBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("BUS_USERS_ADMIN", "ADMIN_ID", "AdminBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(AdminBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(AdminBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(AdminBean entityImpl) throws GHGException {
        return null;
    }

    public int deleteAdmin(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from AdminBean l where l.id= :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }
}
