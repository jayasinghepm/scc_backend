package com.climatesi.ghg.business_users.api.facades;

import com.climatesi.ghg.business_users.api.beans.Employee;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface EmployeerManager extends DataEntryManager<Employee, Integer> {
}
