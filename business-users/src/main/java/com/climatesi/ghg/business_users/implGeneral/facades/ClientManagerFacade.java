package com.climatesi.ghg.business_users.implGeneral.facades;

import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.facades.ClientManager;
import com.climatesi.ghg.business_users.implGeneral.beans.ClientBean;
import com.climatesi.ghg.business_users.implGeneral.persistence.ClientDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class ClientManagerFacade implements ClientManager {

    private static Logger logger = Logger.getLogger(ClientManagerFacade.class);
    private ClientDAO dao;

    @Override
    public Object addEntity(Client entity) {
        try {
            return dao.insert((ClientBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding Client ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(Client entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Client entity) {
        try {
            return dao.update((ClientBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding Client ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(Client entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Client entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Client entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Client entity) {
        return null;
    }

    @Override
    public Client getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Client> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Client> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Client getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<Client> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Client> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new ClientDAO(entityManager);
    }

    @Override
    public int deleteClient(int id) {
        try {
            return dao.deleteClient(id);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
