package com.climatesi.ghg.business_users.api.beans;

import java.util.Date;

public interface Employee {

    public int isDeleted();

    public void setDeleted(int deleted);


    public int getId();

    public void setId(int id);


    public int getBranchId();

    public void setBranchId(int branchId);

    public int getComId();

    public void setComId(int comId);

    public String getName();

    public void setName(String name);

    public String getEmpNo();

    public void setEmpNo(String empNo);

    public int getAddedBy();

    public void setAddedBy(int addedBy);

    public Date getAddedDate();

    public void setAddedDate(Date addedDate);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date lastUpdatedDate);
}
