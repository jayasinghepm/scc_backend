package com.climatesi.ghg.business_users.implGeneral.facades;

import com.climatesi.ghg.business_users.api.beans.Employee;
import com.climatesi.ghg.business_users.api.facades.EmployeerManager;
import com.climatesi.ghg.business_users.implGeneral.beans.EmployeeBean;
import com.climatesi.ghg.business_users.implGeneral.persistence.EmployeeDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class EmployeeManagerFacade implements EmployeerManager {

    private static Logger logger = Logger.getLogger(EmployeeManagerFacade.class);
    private EmployeeDAO dao;

    @Override
    public Object addEntity(Employee entity) {
        try {
            return dao.insert((EmployeeBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding EmployeeBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(Employee entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Employee entity) {
        try {
            return dao.update((EmployeeBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding EmployeeBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(Employee entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Employee entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Employee entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Employee entity) {
        return null;
    }

    @Override
    public Employee getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Employee> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Employee> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Employee getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<Employee> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Employee> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new EmployeeDAO(entityManager);
    }
}
