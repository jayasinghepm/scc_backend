package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.response;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationActDataByCompany;

public class GetMitigationActDataResMsg extends EntityMasterResponseBean {

    private int comId;

    private MitigationActDataByCompany actData;

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public MitigationActDataByCompany getActData() {
        return actData;
    }

    public void setActData(MitigationActDataByCompany actData) {
        this.actData = actData;
    }
}
