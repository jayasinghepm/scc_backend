package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;

import java.util.List;

public class CompanyListResponseMsg extends EntityListResponseBean {

    private List<CompanyDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
