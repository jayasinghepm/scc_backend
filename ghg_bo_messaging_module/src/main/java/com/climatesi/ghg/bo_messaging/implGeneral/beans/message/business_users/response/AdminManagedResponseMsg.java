package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.AdminDTO;

public class AdminManagedResponseMsg extends EntityMasterResponseBean {

    private AdminDTO dto;

    public AdminDTO getDto() {
        return dto;
    }

    public void setDto(AdminDTO dto) {
        this.dto = dto;
    }
}
