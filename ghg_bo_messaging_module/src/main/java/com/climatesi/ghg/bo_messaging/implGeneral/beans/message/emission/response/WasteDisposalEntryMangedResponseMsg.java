package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteDisposalEntryDTO;
import com.google.gson.annotations.SerializedName;

public class WasteDisposalEntryMangedResponseMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private WasteDisposalEntryDTO dto;

    public WasteDisposalEntryDTO getDto() {
        return this.dto;
    }

    public void setDto(WasteDisposalEntryDTO dto) {
        this.dto = dto;
    }


}
