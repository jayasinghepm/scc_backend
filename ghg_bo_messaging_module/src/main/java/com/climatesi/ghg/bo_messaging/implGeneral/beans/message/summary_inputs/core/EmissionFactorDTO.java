package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

public class EmissionFactorDTO {

    private String name;

    private float value;

    private int unit;

    private String unitStr;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public String getUnitStr() {
        return unitStr;
    }

    public void setUnitStr(String unitStr) {
        this.unitStr = unitStr;
    }
}
