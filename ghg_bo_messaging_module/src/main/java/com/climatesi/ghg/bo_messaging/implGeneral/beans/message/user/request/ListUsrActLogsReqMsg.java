package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListRequestBean;

public class ListUsrActLogsReqMsg extends EntityListRequestBean {

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    private int userId;


}
