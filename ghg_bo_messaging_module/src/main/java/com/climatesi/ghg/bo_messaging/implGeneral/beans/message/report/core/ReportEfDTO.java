package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core;

public class ReportEfDTO {

    private String nameWithUnit;

    private float value;

    public String getNameWithUnit() {
        return nameWithUnit;
    }

    public void setNameWithUnit(String nameWithUnit) {
        this.nameWithUnit = nameWithUnit;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
