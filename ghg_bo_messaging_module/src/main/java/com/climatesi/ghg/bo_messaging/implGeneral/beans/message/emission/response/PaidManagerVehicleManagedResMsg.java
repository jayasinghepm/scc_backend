package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaidManagerVehicleDTO;
import com.google.gson.annotations.SerializedName;

public class PaidManagerVehicleManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private PaidManagerVehicleDTO dto;

    public PaidManagerVehicleDTO getDto() {
        return dto;
    }

    public void setDto(PaidManagerVehicleDTO dto) {
        this.dto = dto;
    }
}
