package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.DataEntryUserDTO;

import java.util.List;

public class DataEntryUserListResMsg extends EntityListResponseBean {

    private List<DataEntryUserDTO> list;

    public List<DataEntryUserDTO> getList() {
        return list;
    }

    public void setList(List<DataEntryUserDTO> list) {
        this.list = list;
    }
}
