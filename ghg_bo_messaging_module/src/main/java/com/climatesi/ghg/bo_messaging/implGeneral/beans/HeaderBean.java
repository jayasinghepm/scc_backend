package com.climatesi.ghg.bo_messaging.implGeneral.beans;

import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.google.gson.annotations.SerializedName;

public class HeaderBean implements Header {

    @SerializedName(HeaderConstants.PROTOCOL_VERSION)
    private String protocolVersion = "GHG_JSON_1.0";

    @SerializedName(HeaderConstants.SESSION_ID)
    private String sessionId = null;

    @SerializedName(HeaderConstants.MESSAGE_GROUP)
    private int messageGroup = -1;

    @SerializedName(HeaderConstants.MESSAGE_TYPE)
    private int messageType = -1;

    @SerializedName(HeaderConstants.CHANNEL_ID)
    private int channelId = -1;

    @SerializedName(HeaderConstants.CLIENT_IP)
    private String clientIp = null;

    @SerializedName(HeaderConstants.CLIENT_VERSION)
    private String clientVersion = null;

    @SerializedName(HeaderConstants.CLIENT_REQUEST_ID)
    private String clientReqID = null;

    @SerializedName(HeaderConstants.RESPONSE_STATUS)
    private int responseStatus; //1=Success, -1=Failed, -2=Failed due to unknown request, -3=Failed due to session validation

    @SerializedName(HeaderConstants.RESPONSE_REASON)
    private String responseNarration = null;

    @SerializedName(HeaderConstants.USER_ID)
    private String loggedInUserId = null;

    @SerializedName(HeaderConstants.INSTITUTION_ID)
    private int institutionId;

    @SerializedName(HeaderConstants.INSTITUTION_CODE)
    private String institutionCode;

    @SerializedName(HeaderConstants.LANGUAGE)
    private String language;

    //region Getters and Setters


    public String getProtocolVersion() {
        return protocolVersion;
    }


    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }


    public String getSessionId() {
        return sessionId;
    }


    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }


    public int getMessageGroup() {
        return messageGroup;
    }


    public void setMessageGroup(int messageGroup) {
        this.messageGroup = messageGroup;
    }


    public int getMessageType() {
        return messageType;
    }


    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }


    public int getChannelId() {
        return channelId;
    }


    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }


    public String getClientIp() {
        return clientIp;
    }


    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }


    public String getClientVersion() {
        return clientVersion;
    }


    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }


    public String getClientReqID() {
        return clientReqID;
    }


    public void setClientReqID(String clientReqID) {
        this.clientReqID = clientReqID;
    }


    public int getResponseStatus() {
        return responseStatus;
    }


    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }


    public String getResponseNarration() {
        return responseNarration;
    }


    public void setResponseNarration(String responseNarration) {
        this.responseNarration = responseNarration;
    }


    public String getLoggedInUserId() {
        return loggedInUserId;
    }


    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }


    public int getInstitutionId() {
        return institutionId;
    }


    public void setInstitutionId(int institutionId) {
        this.institutionId = institutionId;
    }


    public String getInstitutionCode() {
        return institutionCode;
    }


    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }


    public String getLanguage() {
        return language;
    }


    public void setLanguage(String language) {
        this.language = language;
    }

    //endregion

    @Override
    public String toString() {
        return "HeaderBean{" +
                "protocolVersion='" + protocolVersion + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", messageGroup=" + messageGroup +
                ", messageType=" + messageType +
                ", channelId=" + channelId +
                ", clientIp='" + clientIp + '\'' +
                ", clientVersion='" + clientVersion + '\'' +
                ", clientReqID='" + clientReqID + '\'' +
                ", responseStatus=" + responseStatus +
                ", responseNarration='" + responseNarration + '\'' +
                ", loggedInUserId='" + loggedInUserId + '\'' +
                ", institutionId=" + institutionId +
                ", institutionCode='" + institutionCode + '\'' +
                '}';
    }
}

