package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.WasteDisFactorDTO;
import com.google.gson.annotations.SerializedName;

public class ManageWasteDisFactorsResMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private WasteDisFactorDTO dto;

    public WasteDisFactorDTO getDto() {
        return dto;
    }

    public void setDto(WasteDisFactorDTO dto) {
        this.dto = dto;
    }
}
