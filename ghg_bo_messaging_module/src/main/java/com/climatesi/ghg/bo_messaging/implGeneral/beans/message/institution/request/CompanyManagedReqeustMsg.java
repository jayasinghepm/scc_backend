package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;
import com.google.gson.annotations.SerializedName;

public class CompanyManagedReqeustMsg implements Message {

    @SerializedName("DATA")
    private CompanyDTO dto;

    public CompanyDTO getDto() {
        return dto;
    }

    public void setDto(CompanyDTO dto) {
        this.dto = dto;
    }
}
