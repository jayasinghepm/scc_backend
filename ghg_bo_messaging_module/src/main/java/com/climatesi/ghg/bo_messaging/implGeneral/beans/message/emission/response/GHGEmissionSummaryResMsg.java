package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GHGEmissionSummaryDTO;

import java.util.List;

public class GHGEmissionSummaryResMsg extends EntityMasterResponseBean {

    private List<GHGEmissionSummaryDTO> list;


    public List<GHGEmissionSummaryDTO> getList() {
        return list;
    }

    public void setList(List<GHGEmissionSummaryDTO> list) {
        this.list = list;
    }
}
