package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.EmployeeDTO;
import com.google.gson.annotations.SerializedName;

public class EmployeeMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private EmployeeDTO dto;

    public EmployeeDTO getDto() {
        return dto;
    }

    public void setDto(EmployeeDTO dto) {
        this.dto = dto;
    }
}
