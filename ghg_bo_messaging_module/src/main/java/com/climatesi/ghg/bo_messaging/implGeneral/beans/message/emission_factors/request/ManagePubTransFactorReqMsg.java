package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.PubTransEmFactorDTO;
import com.google.gson.annotations.SerializedName;

public class ManagePubTransFactorReqMsg implements Message {

    @SerializedName("DATA")
    private PubTransEmFactorDTO dto;

    public PubTransEmFactorDTO getDto() {
        return dto;
    }

    public void setDto(PubTransEmFactorDTO dto) {
        this.dto = dto;
    }
}
