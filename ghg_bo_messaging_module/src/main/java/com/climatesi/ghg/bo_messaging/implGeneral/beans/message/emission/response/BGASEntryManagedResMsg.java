package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BGASEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LPGEntryDTO;
import com.google.gson.annotations.SerializedName;

public class BGASEntryManagedResMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private BGASEntryDTO dto;

    public BGASEntryDTO getDto() {
        return dto;
    }

    public void setDto(BGASEntryDTO dto) {
        this.dto = dto;
    }
}
