package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

public class ProjectDataEntryStatusReqMsg implements Message {

    private int companyId;
    private int branchId;
    private String fy;

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getFy() {
        return fy;
    }

    public void setFy(String fy) {
        this.fy = fy;
    }
}
