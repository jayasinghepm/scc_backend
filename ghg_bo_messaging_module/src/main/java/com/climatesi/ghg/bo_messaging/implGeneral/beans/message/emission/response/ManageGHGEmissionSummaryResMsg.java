package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GHGEmissionSummaryDTO;
import com.google.gson.annotations.SerializedName;

public class ManageGHGEmissionSummaryResMsg extends EntityMasterResponseBean {

    @SerializedName("DATA")
    private GHGEmissionSummaryDTO dto;

    public GHGEmissionSummaryDTO getDto() {
        return dto;
    }

    public void setDto(GHGEmissionSummaryDTO dto) {
        this.dto = dto;
    }
}
