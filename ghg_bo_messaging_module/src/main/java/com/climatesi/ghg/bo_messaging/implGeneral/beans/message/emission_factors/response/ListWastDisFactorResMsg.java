package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.WasteDisFactorDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListWastDisFactorResMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<WasteDisFactorDTO> list;

    public List<WasteDisFactorDTO> getList() {
        return list;
    }

    public void setList(List<WasteDisFactorDTO> list) {
        this.list = list;
    }
}
