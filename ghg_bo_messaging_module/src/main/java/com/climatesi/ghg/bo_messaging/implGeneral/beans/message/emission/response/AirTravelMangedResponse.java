package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.google.gson.annotations.SerializedName;

public class AirTravelMangedResponse extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private BusinessAirTravelEntryDTO dto;

    public BusinessAirTravelEntryDTO getDto() {
        return dto;
    }

    public void setDto(BusinessAirTravelEntryDTO dto) {
        this.dto = dto;
    }
}
