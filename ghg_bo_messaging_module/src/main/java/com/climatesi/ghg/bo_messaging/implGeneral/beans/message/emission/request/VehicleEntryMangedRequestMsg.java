package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.VehicleEntryDTO;
import com.google.gson.annotations.SerializedName;

public class VehicleEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private VehicleEntryDTO dto;

    public VehicleEntryDTO getDto() {
        return dto;
    }

    public void setDto(VehicleEntryDTO dto) {
        this.dto = dto;
    }
}
