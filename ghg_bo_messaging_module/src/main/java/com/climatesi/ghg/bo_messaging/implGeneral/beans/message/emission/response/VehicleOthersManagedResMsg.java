package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.AshTransportationDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.VehicleOthersDTO;
import com.google.gson.annotations.SerializedName;

public class VehicleOthersManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private VehicleOthersDTO dto;

    public VehicleOthersDTO getDto() {
        return dto;
    }

    public void setDto(VehicleOthersDTO dto) {
        this.dto = dto;
    }
}
