package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RefrigerantsEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SeaAirFreightDTO;
import com.google.gson.annotations.SerializedName;

public class SeaFrieghtManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private SeaAirFreightDTO dto;

    public SeaAirFreightDTO getDto() {
        return this.dto;
    }

    public void setDto(SeaAirFreightDTO dto) {
        this.dto = dto;
    }
}
