package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.VehicleEntryDTO;
import com.google.gson.annotations.SerializedName;

public class VehicleEntryMangedResponseMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private VehicleEntryDTO dto;

    public VehicleEntryDTO getDto() {
        return this.dto;
    }

    public void setDto(VehicleEntryDTO dto) {
        this.dto = dto;
    }

}
