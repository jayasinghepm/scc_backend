package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ExcludedReasonDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectFullSummaryDTO;

import java.util.List;

public class UpdateCompanyExcludedReasonAndSuggestionResMsg extends EntityMasterResponseBean {


    private List<ExcludedReasonDTO> excludedReasons;

    private List<ProjectFullSummaryDTO> suggestions;

    public List<ExcludedReasonDTO> getExcludedReasons() {
        return excludedReasons;
    }

    public void setExcludedReasons(List<ExcludedReasonDTO> excludedReasons) {
        this.excludedReasons = excludedReasons;
    }

    public List<ProjectFullSummaryDTO> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<ProjectFullSummaryDTO> suggestions) {
        this.suggestions = suggestions;
    }
}
