package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core;

public class MitigationProjectMonthlyActData {

    private int projectId;

    private String year;

    private int month;

    private double value; // kwh

    private int unit;

    private double emissionSaved ; //tco2e

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getEmissionSaved() {
        return emissionSaved;
    }

    public void setEmissionSaved(double emissionSaved) {
        this.emissionSaved = emissionSaved;
    }
}
