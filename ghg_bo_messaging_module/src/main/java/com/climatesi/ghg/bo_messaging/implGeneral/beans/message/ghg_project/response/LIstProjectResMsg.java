package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.core.ProjectDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LIstProjectResMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<ProjectDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
