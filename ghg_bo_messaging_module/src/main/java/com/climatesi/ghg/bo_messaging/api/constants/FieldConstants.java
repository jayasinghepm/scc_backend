package com.climatesi.ghg.bo_messaging.api.constants;

public class FieldConstants {

    public static final String USER_NAME = "USER_NAME";

    public static final String PASSWORD = "PASSWORD";

    public static final String AUTHENTICATION_STATUS = "AUTHENTICATION_STATUS";

    public static final String NARRATION = "NARRATION";

    public static final String SESSION_ID = "SESSION_ID";

    public static final String INSTITUTION_ID = "INSTITUTION_ID";

    public static final String USER_ID = "USER_ID";

    public static final String USER_FIRST_NAME = "USER_FIRST_NAME";

    public static final String USER_LAST_NAME = "USER_LAST_NAME";

    public static final String USER_TYPE = "USER_TYPE";

    public static final String MOBILE = "MOBILE";

    public static final String EMAIL = "EMAIL";

    public static final String USER_ADDRESS = "USER_ADDRESS";



}
