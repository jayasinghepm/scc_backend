package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.WasteDisFactorDTO;
import com.google.gson.annotations.SerializedName;

public class ManageWasteDisFactorReqMsg implements Message {


    @SerializedName("DATA")
    private WasteDisFactorDTO dto;

    public WasteDisFactorDTO getDto() {
        return dto;
    }

    public void setDto(WasteDisFactorDTO dto) {
        this.dto = dto;
    }
}
