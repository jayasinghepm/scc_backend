package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.EmFactorDTO;
import com.google.gson.annotations.SerializedName;

public class ManageEmFactorReqMsg implements Message {

    @SerializedName("DATA")
    private EmFactorDTO dto;

    public EmFactorDTO getDto() {
        return dto;
    }

    public void setDto(EmFactorDTO dto) {
        this.dto = dto;
    }
}
