package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.core;

public class CountryDTO {

    private int id;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
