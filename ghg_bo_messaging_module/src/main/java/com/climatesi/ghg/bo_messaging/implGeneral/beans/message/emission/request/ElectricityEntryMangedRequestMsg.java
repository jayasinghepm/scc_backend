package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityEntryDTO;
import com.google.gson.annotations.SerializedName;

public class ElectricityEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private ElectricityEntryDTO dto;

    public ElectricityEntryDTO getDto() {
        return dto;
    }

    public void setDto(ElectricityEntryDTO dto) {
        this.dto = dto;
    }
}
