package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class EmpCommutingDTO {

    private int id;

    private int companyId;

    private int branchId;

    private String empId;

    private int noEmissionMode_UP;

    private int noEmissionMode_DOWN;

    private float noEmission_distance_UP;

    private float noEmission_distance_DOWN;


    private int publicTransMode_UP;

    private int publicTransMode_DOWN;


    private float publicTrans_distance_UP;


    private float publicTrans_distance_DOWN;


    private int ownTransMode_UP;


    private int ownTransMode_DOWN;


    private float ownTrans_distance_UP;


    private float ownTrans_distance_DOWN;


    private float ownTrans_fuelEconomy_UP;


    private float ownTrans_fuelEconomy_DOWN;


    private int companyFuelMode_UP;


    private int companyFuelMode_DOWN;


    private float companyFuel_distance_UP;


    private float companyFuel_distance_DOWN;


    private float companyFuel_fuelEconomy_UP;


    private float companyFuel_fuelEconomy_DOWN;


    private float annualEmission;


    private int month;


    private String year;


    private int company_fuelType_UP;


    private int company_fuelType_DOWN;


    private int ownTrans_fuelType_UP;


    private int ownTrans_fuelType_DOWN;


    private boolean paidByCom;


    private int noOfWorkingDays;

    private boolean isTwoWay;

    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    private String twoWay;

    private String paid;

    @SerializedName("EMISSION_DETAILS")
    private JsonObject emissionDetails;

    private float companyPetrolLiters;

    private float companyDieselLiters;

    private float ownPetrolLiters;

    private float ownDieselLiters;

    public String getTwoWay() {
        return twoWay;
    }

    public void setTwoWay(String twoWay) {
        this.twoWay = twoWay;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public int getNoEmissionMode_UP() {
        return noEmissionMode_UP;
    }

    public void setNoEmissionMode_UP(int noEmissionMode_UP) {
        this.noEmissionMode_UP = noEmissionMode_UP;
    }

    public int getNoEmissionMode_DOWN() {
        return noEmissionMode_DOWN;
    }

    public void setNoEmissionMode_DOWN(int noEmissionMode_DOWN) {
        this.noEmissionMode_DOWN = noEmissionMode_DOWN;
    }

    public float getNoEmission_distance_UP() {
        return noEmission_distance_UP;
    }

    public void setNoEmission_distance_UP(float noEmission_distance_UP) {
        this.noEmission_distance_UP = noEmission_distance_UP;
    }

    public float getNoEmission_distance_DOWN() {
        return noEmission_distance_DOWN;
    }

    public void setNoEmission_distance_DOWN(float noEmission_distance_DOWN) {
        this.noEmission_distance_DOWN = noEmission_distance_DOWN;
    }

    public int getPublicTransMode_UP() {
        return publicTransMode_UP;
    }

    public void setPublicTransMode_UP(int publicTransMode_UP) {
        this.publicTransMode_UP = publicTransMode_UP;
    }

    public int getPublicTransMode_DOWN() {
        return publicTransMode_DOWN;
    }

    public void setPublicTransMode_DOWN(int publicTransMode_DOWN) {
        this.publicTransMode_DOWN = publicTransMode_DOWN;
    }

    public float getPublicTrans_distance_UP() {
        return publicTrans_distance_UP;
    }

    public void setPublicTrans_distance_UP(float publicTrans_distance_UP) {
        this.publicTrans_distance_UP = publicTrans_distance_UP;
    }

    public float getPublicTrans_distance_DOWN() {
        return publicTrans_distance_DOWN;
    }

    public void setPublicTrans_distance_DOWN(float publicTrans_distance_DOWN) {
        this.publicTrans_distance_DOWN = publicTrans_distance_DOWN;
    }

    public int getOwnTransMode_UP() {
        return ownTransMode_UP;
    }

    public void setOwnTransMode_UP(int ownTransMode_UP) {
        this.ownTransMode_UP = ownTransMode_UP;
    }

    public int getOwnTransMode_DOWN() {
        return ownTransMode_DOWN;
    }

    public void setOwnTransMode_DOWN(int ownTransMode_DOWN) {
        this.ownTransMode_DOWN = ownTransMode_DOWN;
    }

    public float getOwnTrans_distance_UP() {
        return ownTrans_distance_UP;
    }

    public void setOwnTrans_distance_UP(float ownTrans_distance_UP) {
        this.ownTrans_distance_UP = ownTrans_distance_UP;
    }

    public float getOwnTrans_distance_DOWN() {
        return ownTrans_distance_DOWN;
    }

    public void setOwnTrans_distance_DOWN(float ownTrans_distance_DOWN) {
        this.ownTrans_distance_DOWN = ownTrans_distance_DOWN;
    }

    public float getOwnTrans_fuelEconomy_UP() {
        return ownTrans_fuelEconomy_UP;
    }

    public void setOwnTrans_fuelEconomy_UP(float ownTrans_fuelEconomy_UP) {
        this.ownTrans_fuelEconomy_UP = ownTrans_fuelEconomy_UP;
    }

    public float getOwnTrans_fuelEconomy_DOWN() {
        return ownTrans_fuelEconomy_DOWN;
    }

    public void setOwnTrans_fuelEconomy_DOWN(float ownTrans_fuelEconomy_DOWN) {
        this.ownTrans_fuelEconomy_DOWN = ownTrans_fuelEconomy_DOWN;
    }

    public int getCompanyFuelMode_UP() {
        return companyFuelMode_UP;
    }

    public void setCompanyFuelMode_UP(int companyFuelMode_UP) {
        this.companyFuelMode_UP = companyFuelMode_UP;
    }

    public int getCompanyFuelMode_DOWN() {
        return companyFuelMode_DOWN;
    }

    public void setCompanyFuelMode_DOWN(int companyFuelMode_DOWN) {
        this.companyFuelMode_DOWN = companyFuelMode_DOWN;
    }

    public float getCompanyFuel_distance_UP() {
        return companyFuel_distance_UP;
    }

    public void setCompanyFuel_distance_UP(float companyFuel_distance_UP) {
        this.companyFuel_distance_UP = companyFuel_distance_UP;
    }

    public float getCompanyFuel_distance_DOWN() {
        return companyFuel_distance_DOWN;
    }

    public void setCompanyFuel_distance_DOWN(float companyFuel_distance_DOWN) {
        this.companyFuel_distance_DOWN = companyFuel_distance_DOWN;
    }

    public float getCompanyFuel_fuelEconomy_UP() {
        return companyFuel_fuelEconomy_UP;
    }

    public void setCompanyFuel_fuelEconomy_UP(float companyFuel_fuelEconomy_UP) {
        this.companyFuel_fuelEconomy_UP = companyFuel_fuelEconomy_UP;
    }

    public float getCompanyFuel_fuelEconomy_DOWN() {
        return companyFuel_fuelEconomy_DOWN;
    }

    public void setCompanyFuel_fuelEconomy_DOWN(float companyFuel_fuelEconomy_DOWN) {
        this.companyFuel_fuelEconomy_DOWN = companyFuel_fuelEconomy_DOWN;
    }

    public float getAnnualEmission() {
        return annualEmission;
    }

    public void setAnnualEmission(float annualEmission) {
        this.annualEmission = annualEmission;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getCompany_fuelType_UP() {
        return company_fuelType_UP;
    }

    public void setCompany_fuelType_UP(int company_fuelType_UP) {
        this.company_fuelType_UP = company_fuelType_UP;
    }

    public int getCompany_fuelType_DOWN() {
        return company_fuelType_DOWN;
    }

    public void setCompany_fuelType_DOWN(int company_fuelType_DOWN) {
        this.company_fuelType_DOWN = company_fuelType_DOWN;
    }

    public int getOwnTrans_fuelType_UP() {
        return ownTrans_fuelType_UP;
    }

    public void setOwnTrans_fuelType_UP(int ownTrans_fuelType_UP) {
        this.ownTrans_fuelType_UP = ownTrans_fuelType_UP;
    }

    public int getOwnTrans_fuelType_DOWN() {
        return ownTrans_fuelType_DOWN;
    }

    public void setOwnTrans_fuelType_DOWN(int ownTrans_fuelType_DOWN) {
        this.ownTrans_fuelType_DOWN = ownTrans_fuelType_DOWN;
    }

    public boolean isPaidByCom() {
        return paidByCom;
    }

    public void setPaidByCom(boolean paidByCom) {
        this.paidByCom = paidByCom;
    }

    public int getNoOfWorkingDays() {
        return noOfWorkingDays;
    }

    public void setNoOfWorkingDays(int noOfWorkingDays) {
        this.noOfWorkingDays = noOfWorkingDays;
    }

    public boolean isTwoWay() {
        return isTwoWay;
    }

    public void setTwoWay(boolean twoWay) {
        isTwoWay = twoWay;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    public float getCompanyPetrolLiters() {
        return companyPetrolLiters;
    }

    public void setCompanyPetrolLiters(float companyPetrolLiters) {
        this.companyPetrolLiters = companyPetrolLiters;
    }

    public float getCompanyDieselLiters() {
        return companyDieselLiters;
    }

    public void setCompanyDieselLiters(float companyDieselLiters) {
        this.companyDieselLiters = companyDieselLiters;
    }

    public float getOwnPetrolLiters() {
        return ownPetrolLiters;
    }

    public void setOwnPetrolLiters(float ownPetrolLiters) {
        this.ownPetrolLiters = ownPetrolLiters;
    }

    public float getOwnDieselLiters() {
        return ownDieselLiters;
    }

    public void setOwnDieselLiters(float ownDieselLiters) {
        this.ownDieselLiters = ownDieselLiters;
    }
}
