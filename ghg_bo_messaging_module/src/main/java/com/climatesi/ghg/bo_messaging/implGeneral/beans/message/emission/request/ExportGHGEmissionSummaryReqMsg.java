package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

public class ExportGHGEmissionSummaryReqMsg implements Message {

    private int comId;

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }
}
