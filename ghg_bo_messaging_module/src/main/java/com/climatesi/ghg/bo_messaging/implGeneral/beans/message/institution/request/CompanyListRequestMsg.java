package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListRequestBean;

public class CompanyListRequestMsg extends EntityListRequestBean {
    private int isLogoRequired;

    public int getIsLogoRequired() {
        return isLogoRequired;
    }

    public void setIsLogoRequired(int isLogoRequired) {
        this.isLogoRequired = isLogoRequired;
    }
}
