package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.FurnaceOilDTO;
import com.google.gson.annotations.SerializedName;

public class FurnaceManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private FurnaceOilDTO dto;

    public FurnaceOilDTO getDto() {
        return dto;
    }

    public void setDto(FurnaceOilDTO dto) {
        this.dto = dto;
    }
}