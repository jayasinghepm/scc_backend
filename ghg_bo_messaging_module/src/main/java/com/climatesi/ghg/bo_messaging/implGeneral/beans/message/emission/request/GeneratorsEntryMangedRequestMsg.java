package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GeneratorsEntryDTO;
import com.google.gson.annotations.SerializedName;

public class GeneratorsEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private GeneratorsEntryDTO dto;

    public GeneratorsEntryDTO getDto() {
        return dto;
    }

    public void setDto(GeneratorsEntryDTO dto) {
        this.dto = dto;
    }
}
