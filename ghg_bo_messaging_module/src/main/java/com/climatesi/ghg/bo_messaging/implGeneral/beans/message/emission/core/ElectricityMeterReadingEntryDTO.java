package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ElectricityMeterReadingEntryDTO {

    @SerializedName("ELEC_METER_READING_ID")
    private int entryId;


    @SerializedName("ADDED_BY")
    private int addedBy;

    @SerializedName("BRANCH_ID")
    private int branchId;

    @SerializedName("COMPANY_ID")
    private int companyId;

    @SerializedName("ADDED_DATE")
    private Date addedDate;

    @SerializedName("LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @SerializedName("MONTH")
    private int month;

    @SerializedName("YEAR")
    private int year;

    @SerializedName("READING_MONTH")
    private float readingMonth;

    @SerializedName("METER_NUMBER")
    private String meterNumber;

    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public int getEntryId() {
        return this.entryId;
    }


    public void setEntryId(int id) {
        this.entryId = id;
    }


    public String getMeterNumber() {
        return this.meterNumber;
    }

    public void setMeterNumber(String number) {
        this.meterNumber = number;
    }

    public float getReadingForMonth() {
        return this.readingMonth;
    }

    public void setReadingForMonth(float reading) {
        this.readingMonth = reading;
    }

    public int getAddedBy() {
        return this.addedBy;
    }


    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }


    public int getBranchId() {
        return this.branchId;
    }


    public void setBranchId(int id) {
        this.branchId = id;
    }


    public int getCompanyId() {
        return this.companyId;
    }


    public void setCompanyId(int id) {
        this.companyId = id;
    }


    public Date getEntryAddedDate() {
        return this.addedDate;
    }


    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }


    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }


    public int getMonth() {
        return this.month;
    }


    public void setMonth(int month) {
        this.month = month;
    }


    public int getYear() {
        return this.year;
    }


    public void setYear(int year) {
        this.year = year;
    }
}
