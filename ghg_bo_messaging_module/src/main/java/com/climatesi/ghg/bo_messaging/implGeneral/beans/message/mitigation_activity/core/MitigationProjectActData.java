package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core;

import java.util.ArrayList;
import java.util.List;

public class MitigationProjectActData {

    private int projectId;

    private String projectName;

    private double savedEmissions; //tco2e

    private double value; //eg: tco2e

    private int projectType;

    private List<MitigationProjectAnnualData> annualData = new ArrayList<MitigationProjectAnnualData>();

    public int getProjectType() {
        return projectType;
    }

    public void setProjectType(int projectType) {
        this.projectType = projectType;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public double getSavedEmissions() {
        return savedEmissions;
    }

    public void setSavedEmissions(double savedEmissions) {
        this.savedEmissions = savedEmissions;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public List<MitigationProjectAnnualData> getAnnualData() {
        return annualData;
    }

    public void setAnnualData(List<MitigationProjectAnnualData> annualData) {
        this.annualData = annualData;
    }
}
