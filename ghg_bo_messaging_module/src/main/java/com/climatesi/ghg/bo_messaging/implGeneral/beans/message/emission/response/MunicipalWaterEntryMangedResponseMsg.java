package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterEntryDTO;
import com.google.gson.annotations.SerializedName;

public class MunicipalWaterEntryMangedResponseMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private MunicipalWaterEntryDTO dto;

    public MunicipalWaterEntryDTO getDto() {
        return dto;
    }

    public void setDto(MunicipalWaterEntryDTO dto) {
        this.dto = dto;
    }
}
