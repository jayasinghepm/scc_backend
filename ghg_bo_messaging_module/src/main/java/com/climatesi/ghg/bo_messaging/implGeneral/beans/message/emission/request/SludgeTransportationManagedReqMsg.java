package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SludgeTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class SludgeTransportationManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private SludgeTransportationDTO dto;

    public SludgeTransportationDTO getDto() {
        return dto;
    }

    public void setDto(SludgeTransportationDTO dto) {
        this.dto = dto;
    }
}