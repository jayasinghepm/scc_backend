package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.BranchDTO;

import java.util.List;

public class BranchListResponseMsg extends EntityListResponseBean {

    private List<BranchDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
