package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityMeterReadingEntryDTO;
import com.google.gson.annotations.SerializedName;

public class ElectricityMeterReadingEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private ElectricityMeterReadingEntryDTO dto;

    public ElectricityMeterReadingEntryDTO getDto() {
        return dto;
    }

    public void setDto(ElectricityMeterReadingEntryDTO dto) {
        this.dto = dto;
    }
}
