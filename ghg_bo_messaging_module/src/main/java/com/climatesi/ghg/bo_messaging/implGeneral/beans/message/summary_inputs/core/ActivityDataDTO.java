package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

import java.util.List;

public class ActivityDataDTO {

    private String metricsName;

    private List<Float> values;

    private int unit;

    private String unitStr;

    public String getMetricsName() {
        return metricsName;
    }

    public void setMetricsName(String metricsName) {
        this.metricsName = metricsName;
    }

    public List<Float> getValues() {
        return values;
    }

    public void setValues(List<Float> values) {
        this.values = values;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public String getUnitStr() {
        return unitStr;
    }

    public void setUnitStr(String unitStr) {
        this.unitStr = unitStr;
    }
}
