package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.DataEntryUserDTO;

public class DataEntryUserManagedResMsg extends EntityMasterResponseBean {

    private DataEntryUserDTO dto;

    public DataEntryUserDTO getDto() {
        return dto;
    }

    public void setDto(DataEntryUserDTO dto) {
        this.dto = dto;
    }
}
