package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.AshTransportationDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.VehicleOthersDTO;
import com.google.gson.annotations.SerializedName;

public class VehicleOthersManagedReqMsg implements Message {

    @SerializedName("DATA")
    private VehicleOthersDTO dto;

    public VehicleOthersDTO getDto() {
        return dto;
    }

    public void setDto(VehicleOthersDTO dto) {
        this.dto = dto;
    }
}