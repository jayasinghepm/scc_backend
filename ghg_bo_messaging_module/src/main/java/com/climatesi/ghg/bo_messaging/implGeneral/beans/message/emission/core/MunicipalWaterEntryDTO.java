package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class MunicipalWaterEntryDTO {

    @SerializedName("MUNICIPAL_WATER_ENTRY_ID")
    private int entryId;

    @SerializedName("METER_NO")
    private String meterNo;
    @SerializedName("EMISSION_SRC_ID")
    private int emissionSrcId;
    @SerializedName("ADDED_BY")
    private int addedBy;
    @SerializedName("BRANCH_ID")
    private int branchId;
    @SerializedName("COMPANY_ID")
    private int companyId;
    @SerializedName("ADDED_DATE")
    private Date addedDate;
    @SerializedName("LAST_UPDATED_DATE")
    private Date lastUpdatedDate;
    @SerializedName("MONTH")
    private int month;
    @SerializedName("YEAR")
    private String year;
    @SerializedName("UNITS")
    private int units;
    @SerializedName("WATER_CONSUMPTION")
    private float consumption;
    @SerializedName("NUMS_OF_METERS")
    private int numOfMeters;
    @SerializedName("METER_NUMBERS")
    private List<String> meterNumbers;

    private int isDeleted;
    @SerializedName("EMISSION_DETAILS")
    private JsonObject emissionDetails;

    private String company;

    private String branch;

    private String mon;

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getEntryId() {
        return this.entryId;
    }


    public void setEntryId(int id) {
        this.entryId = id;
    }


    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }


    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }


    public int getAddedBy() {
        return this.addedBy;
    }


    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }


    public int getBranchId() {
        return this.branchId;
    }


    public void setBranchId(int id) {
        this.branchId = id;
    }


    public int getCompanyId() {
        return this.companyId;
    }


    public void setCompanyId(int id) {
        this.companyId = id;
    }


    public Date getEntryAddedDate() {
        return this.addedDate;
    }


    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }


    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }


    public int getMonth() {
        return this.month;
    }


    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return this.year;
    }


    public void setYear(String year) {
        this.year = year;
    }


    public int getNumOfMeters() {
        return this.numOfMeters;
    }


    public void setNumOfMeters(int meters) {
        this.numOfMeters = meters;
    }


    public List<String> getMeterNumbers() {
        return this.meterNumbers;
    }


    public void setMeterNumbers(List<String> meterNumbers) {
        this.meterNumbers = meterNumbers;
    }


    public float getConsumption() {
        return this.consumption;
    }


    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }


    public int getUnits() {
        return this.units;
    }


    public void setUnits(int units) {
        this.units = units;
    }
}
