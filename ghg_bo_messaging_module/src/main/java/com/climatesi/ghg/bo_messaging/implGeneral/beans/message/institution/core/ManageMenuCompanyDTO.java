package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core;

import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;

import java.util.HashMap;
import java.util.List;

public class ManageMenuCompanyDTO {


    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private List<Integer> pages;

    private List<Integer> emSources ;

    private HashMap<Integer, AllowedEmissionSrcBean> allowedEmissionSources;


    public HashMap<Integer, AllowedEmissionSrcBean> getAllowedEmissionSources() {
        return allowedEmissionSources;
    }

    public void setAllowedEmissionSources(HashMap<Integer, AllowedEmissionSrcBean> allowedEmissionSources) {
        this.allowedEmissionSources = allowedEmissionSources;
    }

    public List<Integer> getPages() {
        return pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }

    public List<Integer> getEmSources() {
        return emSources;
    }

    public void setEmSources(List<Integer> emSources) {
        this.emSources = emSources;
    }
}
