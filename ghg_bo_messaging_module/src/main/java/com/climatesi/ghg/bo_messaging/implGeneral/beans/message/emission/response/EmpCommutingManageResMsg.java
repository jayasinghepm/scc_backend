package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.EmpCommutingDTO;

public class EmpCommutingManageResMsg extends EntityMasterResponseBean {

    private EmpCommutingDTO dto;

    public EmpCommutingDTO getDto() {
        return dto;
    }

    public void setDto(EmpCommutingDTO dto) {
        this.dto = dto;
    }
}
