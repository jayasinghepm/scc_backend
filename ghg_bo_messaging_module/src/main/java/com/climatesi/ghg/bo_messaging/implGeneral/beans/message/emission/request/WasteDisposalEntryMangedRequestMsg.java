package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteDisposalEntryDTO;
import com.google.gson.annotations.SerializedName;

public class WasteDisposalEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private WasteDisposalEntryDTO dto;

    public WasteDisposalEntryMangedRequestMsg(WasteDisposalEntryDTO dto) {
        this.dto = dto;
    }

    public WasteDisposalEntryDTO getDto() {
        return dto;
    }

    public void setDto(WasteDisposalEntryDTO dto) {
        this.dto = dto;
    }
}
