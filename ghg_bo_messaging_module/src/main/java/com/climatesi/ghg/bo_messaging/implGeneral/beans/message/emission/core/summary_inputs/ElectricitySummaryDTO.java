package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.summary_inputs;

import java.util.List;

public class ElectricitySummaryDTO {


    private int branchId;
    private int comId;
    //<meter-consumption-emission>
    private List<String> meters;
    //    private List<String> emission;
    private String fy;
    //    <consumption-emission>
    private String total;
    //    <branch-consumption-emission>
    private String max;
    //    <branch-consumption-emission>
    private String min;
    //    <consumption-emission>
    private String avg;

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public List<String> getMeters() {
        return meters;
    }

    public void setMeters(List<String> meters) {
        this.meters = meters;
    }

    public String getFy() {
        return fy;
    }

    public void setFy(String fy) {
        this.fy = fy;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }
}
