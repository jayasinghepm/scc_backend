package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core;

public class EmissionCategoryDTO {

    private int id;

    private String name;

    private int comId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }
}
