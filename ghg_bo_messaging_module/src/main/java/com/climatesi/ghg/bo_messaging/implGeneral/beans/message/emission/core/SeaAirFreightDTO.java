package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;
import com.google.gson.JsonObject;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Lob;
import java.util.Date;
import java.util.HashMap;

public class SeaAirFreightDTO {

    private int id;

    private int portFrom;

    private String portFromText;

    private int isAirFreight;

    private float freightKg;

    private int containerType; // 1-40ft 2-20ft 3-14.5




    private int emissionSrcId;


    private int addedBy;


    private int branchId;


    private int companyId;


    private Date addedDate;


    private Date lastUpdatedDate;


    private int month;


    private String year;



    private JsonObject emissionDetails;



    private int isDeleted = 0;


    private String company;

    private String branch;

    private String mon;

    public String getPortFromText() {
        return portFromText;
    }

    public void setPortFromText(String portFromText) {
        this.portFromText = portFromText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortFrom() {
        return portFrom;
    }

    public void setPortFrom(int portFrom) {
        this.portFrom = portFrom;
    }

    public int getIsAirFreight() {
        return isAirFreight;
    }

    public void setIsAirFreight(int isAirFreight) {
        this.isAirFreight = isAirFreight;
    }

    public float getFreightKg() {
        return freightKg;
    }

    public void setFreightKg(float freightKg) {
        this.freightKg = freightKg;
    }

    public int getContainerType() {
        return containerType;
    }

    public void setContainerType(int containerType) {
        this.containerType = containerType;
    }

    public int getEmissionSrcId() {
        return emissionSrcId;
    }

    public void setEmissionSrcId(int emissionSrcId) {
        this.emissionSrcId = emissionSrcId;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }
}
