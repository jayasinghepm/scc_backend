package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.ManageMitigationProjectDTO;

public class ManageMitigationProjectReqMsg  implements Message {

    private ManageMitigationProjectDTO dto;

    public ManageMitigationProjectDTO getDto() {
        return dto;
    }

    public void setDto(ManageMitigationProjectDTO dto) {
        this.dto = dto;
    }
}
