package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteTransportEntryDTO;
import com.google.gson.annotations.SerializedName;

public class WasteTransportEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private WasteTransportEntryDTO dto;

    public WasteTransportEntryDTO getDto() {
        return dto;
    }

    public void setDto(WasteTransportEntryDTO dto) {
        this.dto = dto;
    }
}
