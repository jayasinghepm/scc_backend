package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.core.UsrActLogDto;

import java.util.Date;
import java.util.List;

public class ListUsrActLogsResMsg extends EntityListResponseBean {

   private List<UsrActLogDto> list;

    public List<UsrActLogDto> getList() {
        return list;
    }

    public void setList(List<UsrActLogDto> list) {
        this.list = list;
    }
}
