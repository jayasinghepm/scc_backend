package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class VehicleEntryDTO {

    @SerializedName("VEHICLE_ENTRY_ID")
    private int entryId;


    @SerializedName("EMISSION_SRC_ID")
    private int emissionSrcId;

    @SerializedName("ADDED_BY")
    private int addedBy;

    @SerializedName("BRANCH_ID")
    private int branchId;

    @SerializedName("COMPANY_ID")
    private int companyId;

    @SerializedName("ADDED_DATE")
    private Date addedDate;

    @SerializedName("LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @SerializedName("MONTH")
    private int month;

    @SerializedName("YEAR")
    private String year;


    @SerializedName("FUEL_TYPE")
    private int fuleType;

    @SerializedName("UNITS")
    private int units;

    @SerializedName("FUEL_CONSUMPTION")
    private float consumption;

    @SerializedName("VEHICLE_MODEL")
    private int vehicleModel;

    @SerializedName("VEHICLE_CATEGORY")
    private int vehicleCategory;

    @SerializedName("VEHICLE_NUMBER")
    private String vehicleNumber;

    @SerializedName("IS_FUEL_PAID_BY_COMPANY")
    private boolean isFuelPaidbyCompany;

    @SerializedName("EMISSION_INFO")
    private JsonObject emissionDetails;

    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    private String model;

    private String category;

    private String fuel;

    private float fuelEconomy;

    private float distance;


    public float getFuelEconomy() {
        return fuelEconomy;
    }

    public void setFuelEconomy(float fuelEconomy) {
        this.fuelEconomy = fuelEconomy;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getFuleType() {
        return fuleType;
    }

    public void setFuleType(int fuleType) {
        this.fuleType = fuleType;
    }

    public void setVehicleModel(int vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public void setFuelPaidbyCompany(boolean fuelPaidbyCompany) {
        isFuelPaidbyCompany = fuelPaidbyCompany;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }


    public int getEntryId() {
        return this.entryId;
    }


    public void setEntryId(int id) {
        this.entryId = id;
    }


    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }


    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }


    public int getAddedBy() {
        return this.addedBy;
    }


    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }


    public int getBranchId() {
        return this.branchId;
    }


    public void setBranchId(int id) {
        this.branchId = id;
    }


    public int getCompanyId() {
        return this.companyId;
    }


    public void setCompanyId(int id) {
        this.companyId = id;
    }


    public Date getEntryAddedDate() {
        return this.addedDate;
    }


    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }


    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }


    public int getMonth() {
        return this.month;
    }


    public void setMonth(int month) {
        this.month = month;
    }


    public String getYear() {
        return this.year;
    }


    public void setYear(String year) {
        this.year = year;
    }


    public int getFuelType() {
        return this.fuleType;
    }


    public void setFuelType(int type) {
        this.fuleType = type;
    }


    public float getConsumption() {
        return this.consumption;
    }


    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }


    public int getUnits() {
        return this.units;
    }


    public void setUnits(int units) {
        this.units = units;
    }


    public boolean isFuelPaidbyCompany() {
        return this.isFuelPaidbyCompany;
    }


    public void setIsFuelPaidbyCompany(boolean value) {
        this.isFuelPaidbyCompany = value;
    }


    public int getVehicleModel() {
        return this.vehicleModel;
    }


    public void setVehiclModel(int model) {
        this.vehicleModel = model;
    }


    public int getVehicleCategory() {
        return this.vehicleCategory;
    }


    public void setVehicleCategory(int category) {
        this.vehicleCategory = category;
    }


    public String getVehicleNumber() {
        return vehicleNumber;
    }


    public void setVehicleNumber(String number) {
        this.vehicleNumber = number;
    }
}
