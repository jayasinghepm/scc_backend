package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.req;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.GHGReportDTO;
import com.google.gson.annotations.SerializedName;

public class ManageGHGReportReqMsg implements Message {

    @SerializedName("DATA")
    private GHGReportDTO dto;

    public GHGReportDTO getDto() {
        return dto;
    }

    public void setDto(GHGReportDTO dto) {
        this.dto = dto;
    }
}
