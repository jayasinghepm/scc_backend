package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.TransportLocalEntryDTO;
import com.google.gson.annotations.SerializedName;

public class TransportLocalEntryMangedResponseMsg extends EntityMasterResponseBean {
    @SerializedName("DTO")
    private TransportLocalEntryDTO dto;

    public TransportLocalEntryDTO getDto() {
        return this.dto;
    }

    public void setDto(TransportLocalEntryDTO dto) {
        this.dto = dto;
    }
}
