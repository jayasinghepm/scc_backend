package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SeaAirFreightDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteTransportEntryDTO;
import com.google.gson.annotations.SerializedName;

public class SeaAirFreightMangedReqMsg  implements Message {

    @SerializedName("DATA")
    private SeaAirFreightDTO dto;

    public SeaAirFreightDTO getDto() {
        return dto;
    }

    public void setDto(SeaAirFreightDTO dto) {
        this.dto = dto;
    }
}
