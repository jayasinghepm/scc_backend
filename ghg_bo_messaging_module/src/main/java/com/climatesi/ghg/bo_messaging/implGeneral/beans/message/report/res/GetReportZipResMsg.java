package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.res;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;

public class GetReportZipResMsg extends EntityMasterResponseBean {

    private String s3url;

    public String getS3url() {
        return s3url;
    }

    public void setS3url(String s3url) {
        this.s3url = s3url;
    }
}
