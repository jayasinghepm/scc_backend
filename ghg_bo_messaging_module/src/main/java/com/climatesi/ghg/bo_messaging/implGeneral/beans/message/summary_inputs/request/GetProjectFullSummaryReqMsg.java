package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

public class GetProjectFullSummaryReqMsg implements Message {

    private int projectId;

    private int companyId;


    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }
}
