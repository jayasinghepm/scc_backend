package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core;

import java.util.ArrayList;
import java.util.List;

public class MitigationProjectAnnualData {

    private int projectId;

    private String year;

    private String projectName;

    private int comId;

    private double value; //eg: kwh

    private double emissionsSaved; //tco2e

    private List<MitigationProjectMonthlyActData> monthlyData = new ArrayList<MitigationProjectMonthlyActData>();

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getEmissionsSaved() {
        return emissionsSaved;
    }

    public void setEmissionsSaved(double emissionsSaved) {
        this.emissionsSaved = emissionsSaved;
    }

    public List<MitigationProjectMonthlyActData> getMonthlyData() {
        return monthlyData;
    }

    public void setMonthlyData(List<MitigationProjectMonthlyActData> monthlyData) {
        this.monthlyData = monthlyData;
    }
}
