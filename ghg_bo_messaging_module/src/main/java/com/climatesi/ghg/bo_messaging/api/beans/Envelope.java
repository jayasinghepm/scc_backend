package com.climatesi.ghg.bo_messaging.api.beans;

public interface Envelope {

    /**
     * Gets header.
     *
     * @return the header
     */
    Header getHeader();

    /**
     * Sets header.
     *
     * @param header the header
     */
    void setHeader(Header header);

    /**
     * Gets body.
     *
     * @return the body
     */
    Object getBody();

    /**
     * Sets body.
     *
     * @param body the body
     */
    void setBody(Object body);
}
