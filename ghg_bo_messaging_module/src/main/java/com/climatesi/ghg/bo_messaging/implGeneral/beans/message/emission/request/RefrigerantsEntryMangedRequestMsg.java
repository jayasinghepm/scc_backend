package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RefrigerantsEntryDTO;
import com.google.gson.annotations.SerializedName;

public class RefrigerantsEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private RefrigerantsEntryDTO dto;

    public RefrigerantsEntryDTO getDto() {
        return dto;
    }

    public void setDto(RefrigerantsEntryDTO dto) {
        this.dto = dto;
    }
}
