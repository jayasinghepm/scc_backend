package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.core;

public class ProjectDTO {

    private int id;


    private int companyId;


    private String endDate;


    private boolean isExtended;


    private int signedWith;


    private int status;

    private int numberOfPayments;


    private String name;


    private String intiatedDate;


    private String signedDate;


    private String firstPayDate;


    private String secondPayDate;


    private String finalPayDate;

    private String extendedDate;

    private int isDeleted;

    private String company;

    private String statusStr;

    private int disableFormula;

    public int getDisableFormula() {
        return disableFormula;
    }

    public void setDisableFormula(int disableFormula) {
        this.disableFormula = disableFormula;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isExtended() {
        return isExtended;
    }

    public void setExtended(boolean extended) {
        isExtended = extended;
    }

    public int getSignedWith() {
        return signedWith;
    }

    public void setSignedWith(int signedWith) {
        this.signedWith = signedWith;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNumberOfPayments() {
        return numberOfPayments;
    }

    public void setNumberOfPayments(int numberOfPayments) {
        this.numberOfPayments = numberOfPayments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntiatedDate() {
        return intiatedDate;
    }

    public void setIntiatedDate(String intiatedDate) {
        this.intiatedDate = intiatedDate;
    }

    public String getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(String signedDate) {
        this.signedDate = signedDate;
    }

    public String getFirstPayDate() {
        return firstPayDate;
    }

    public void setFirstPayDate(String firstPayDate) {
        this.firstPayDate = firstPayDate;
    }

    public String getSecondPayDate() {
        return secondPayDate;
    }

    public void setSecondPayDate(String secondPayDate) {
        this.secondPayDate = secondPayDate;
    }

    public String getFinalPayDate() {
        return finalPayDate;
    }

    public void setFinalPayDate(String finalPayDate) {
        this.finalPayDate = finalPayDate;
    }

    public String getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(String extendedDate) {
        this.extendedDate = extendedDate;
    }
}
