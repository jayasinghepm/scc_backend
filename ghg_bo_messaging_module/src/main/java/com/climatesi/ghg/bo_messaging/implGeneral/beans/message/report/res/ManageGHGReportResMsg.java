package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.res;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.GHGReportDTO;
import com.google.gson.annotations.SerializedName;

public class ManageGHGReportResMsg extends EntityMasterResponseBean {

    @SerializedName("DATA")
    private GHGReportDTO dto;

    public GHGReportDTO getDto() {
        return dto;
    }

    public void setDto(GHGReportDTO dto) {
        this.dto = dto;
    }
}
