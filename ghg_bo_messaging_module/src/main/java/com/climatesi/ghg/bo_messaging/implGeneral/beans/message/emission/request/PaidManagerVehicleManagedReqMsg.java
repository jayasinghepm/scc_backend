package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaidManagerVehicleDTO;
import com.google.gson.annotations.SerializedName;

public class PaidManagerVehicleManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private PaidManagerVehicleDTO dto;

    public PaidManagerVehicleDTO getDto() {
        return dto;
    }

    public void setDto(PaidManagerVehicleDTO dto) {
        this.dto = dto;
    }
}