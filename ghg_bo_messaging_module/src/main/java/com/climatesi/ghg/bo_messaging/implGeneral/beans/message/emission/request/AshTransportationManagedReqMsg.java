package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.AshTransportationDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.google.gson.annotations.SerializedName;

public class AshTransportationManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private AshTransportationDTO dto;

    public AshTransportationDTO getDto() {
        return dto;
    }

    public void setDto(AshTransportationDTO dto) {
        this.dto = dto;
    }
}