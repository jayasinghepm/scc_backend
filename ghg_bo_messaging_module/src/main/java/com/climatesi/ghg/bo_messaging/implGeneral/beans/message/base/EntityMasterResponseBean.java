package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.google.gson.annotations.SerializedName;

public class EntityMasterResponseBean implements Message {

    @SerializedName("ENTITY_KEY_RETURN")
    private Object entityKey;

    @SerializedName("ENTITY_ACTION_STATUS")
    private Integer actionStatus;

    @SerializedName("NARRATION")
    private String narration;

    /**
     * Gets entity key.
     *
     * @return the entity key
     */
    public Object getEntityKey() {
        return entityKey;
    }

    /**
     * Sets entity key.
     *
     * @param entityKey the entity key
     */
    public void setEntityKey(Object entityKey) {
        this.entityKey = entityKey;
    }

    /**
     * Gets action status.
     *
     * @return the action status
     */
    public Integer getActionStatus() {
        return actionStatus;
    }

    /**
     * Sets action status.
     *
     * @param actionStatus the action status
     */
    public void setActionStatus(Integer actionStatus) {
        this.actionStatus = actionStatus;
    }

    /**
     * Gets narration.
     *
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * Sets narration.
     *
     * @param narration the narration
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }
}
