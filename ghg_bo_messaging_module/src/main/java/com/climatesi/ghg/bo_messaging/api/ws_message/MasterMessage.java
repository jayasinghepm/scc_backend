package com.climatesi.ghg.bo_messaging.api.ws_message;

public class MasterMessage {

    public int group;
    public int type;

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
