package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectDataEntryStatusDTO;
import com.google.gson.annotations.SerializedName;

public class ManageProjectDataEntryStatusResMsg extends EntityMasterResponseBean {

    @SerializedName("DATA")
    private ProjectDataEntryStatusDTO dto;

    public ProjectDataEntryStatusDTO getDto() {
        return dto;
    }

    public void setDto(ProjectDataEntryStatusDTO dto) {
        this.dto = dto;
    }
}
