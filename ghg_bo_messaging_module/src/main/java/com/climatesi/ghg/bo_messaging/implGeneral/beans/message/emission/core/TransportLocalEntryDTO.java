package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class TransportLocalEntryDTO {

    @SerializedName("TRANSPORT_LOCAL_ENTRY_ID")
    private int entryId;


    @SerializedName("EMISSION_SRC_ID")
    private int emissionSrcId;

    @SerializedName("ADDED_BY")
    private int addedBy;

    @SerializedName("BRANCH_ID")
    private int branchId;

    @SerializedName("COMPANY_ID")
    private int companyId;

    @SerializedName("ADDED_DATE")
    private Date addedDate;

    @SerializedName("LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @SerializedName("MONTH")
    private int month;

    @SerializedName("YEAR")
    private String year;

    @SerializedName("MATERIAL_TYPE")
    private String materialType;

    @SerializedName("SUB_CONTRACTOR_NAME")
    private String subContractorName;

    @SerializedName("FUEL_TYPE")
    private int fuleType;

    @SerializedName("VEHICLE_TYPE")
    private int vehicleType;

    @SerializedName("FUEL_ECONOMY")
    private float fuelEconomy;

    @SerializedName("LOADING_CAPACITY")
    private float loadingCapacity;

    @SerializedName("DISTANCE_TRAVELLED")
    private float distanceTravelled;

    @SerializedName("EMISSION_INFO")
    private JsonObject emissionDetails;


    private float noOfTrips = 1;

    private float fuelConsumption; //liters

    private float weight; //tons


    private int isDeleted;

    private String company;

    private String branch;

    private String mon;


    private String fuel;

    private String vehicle;

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(float noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public int getFuleType() {
        return fuleType;
    }

    public void setFuleType(int fuleType) {
        this.fuleType = fuleType;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }


    public int getEntryId() {
        return this.entryId;
    }


    public void setEntryId(int id) {
        this.entryId = id;
    }


    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }


    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }


    public int getAddedBy() {
        return this.addedBy;
    }


    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }


    public int getBranchId() {
        return this.branchId;
    }


    public void setBranchId(int id) {
        this.branchId = id;
    }


    public int getCompanyId() {
        return this.companyId;
    }


    public void setCompanyId(int id) {
        this.companyId = id;
    }


    public Date getEntryAddedDate() {
        return this.addedDate;
    }


    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }


    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }


    public int getMonth() {
        return this.month;
    }


    public void setMonth(int month) {
        this.month = month;
    }


    public String getYear() {
        return this.year;
    }


    public void setYear(String year) {
        this.year = year;
    }


    public String getMaterialType() {
        return this.materialType;
    }


    public void setMaterialype(String type) {
        this.materialType = type;
    }


    public String getSubContractorName() {
        return this.subContractorName;
    }


    public void setSubContractorName(String name) {
        this.subContractorName = name;
    }


    public int getFuelType() {
        return this.fuleType;
    }


    public void setFuelType(int type) {
        this.fuleType = type;
    }


    public int getVehicleType() {
        return this.vehicleType;
    }


    public void setVehicleType(int type) {
        this.vehicleType = type;
    }


    public float getDistanceTravelled() {
        return this.distanceTravelled;
    }


    public void setDistanceTravelled(float distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }


    public float getFuelEconomy() {
        return this.fuelEconomy;
    }


    public void setFuelEconomy(float economy) {
        this.fuelEconomy = economy;
    }


    public float getLoadingCapacity() {
        return this.loadingCapacity;
    }


    public void setLoadingCapacity(float capacity) {
        this.loadingCapacity = capacity;
    }
}
