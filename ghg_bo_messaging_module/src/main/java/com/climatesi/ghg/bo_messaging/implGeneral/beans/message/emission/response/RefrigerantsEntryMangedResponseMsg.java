package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RefrigerantsEntryDTO;
import com.google.gson.annotations.SerializedName;

public class RefrigerantsEntryMangedResponseMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private RefrigerantsEntryDTO dto;

    public RefrigerantsEntryDTO getDto() {
        return this.dto;
    }

    public void setDto(RefrigerantsEntryDTO dto) {
        this.dto = dto;
    }


}
