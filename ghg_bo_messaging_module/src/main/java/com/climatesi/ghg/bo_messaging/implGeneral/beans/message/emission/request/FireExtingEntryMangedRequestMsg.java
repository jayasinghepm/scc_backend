package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.FireExtingEntryDTO;
import com.google.gson.annotations.SerializedName;

public class FireExtingEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private FireExtingEntryDTO dto;

    public FireExtingEntryDTO getDto() {
        return dto;
    }

    public void setDto(FireExtingEntryDTO dto) {
        this.dto = dto;
    }
}
