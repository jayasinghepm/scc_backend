package com.climatesi.ghg.bo_messaging.api.constants;

public class HeaderConstants {

    public static final String PROTOCOL_VERSION = "VER";
    /**
     * The constant MESSAGE_GROUP.
     */
    public static final String MESSAGE_GROUP = "MSG_GRP";
    /**
     * The constant MESSAGE_TYPE.
     */
    public static final String MESSAGE_TYPE = "MSG_TYP";
    /**
     * The constant CHANNEL_ID.
     */
    public static final String CHANNEL_ID = "CHANNEL";
    /**
     * The constant SESSION_ID.
     */
    public static final String SESSION_ID = "SESN_ID";
    /**
     * The constant RESPONSE_STATUS.
     */
    public static final String RESPONSE_STATUS = "RES_STS";
    /**
     * The constant RESPONSE_REASON.
     */
    public static final String RESPONSE_REASON = "RES_REASN";
    /**
     * The constant CLIENT_REQUEST_ID.
     */
    public static final String CLIENT_REQUEST_ID = "CL_REQ_ID";
    /**
     * The constant CLIENT_IP.
     */
    public static final String CLIENT_IP = "CL_IP";
    /**
     * The constant CLIENT_VERSION.
     */
    public static final String CLIENT_VERSION = "CL_VER";

    public static final String USER_ID = "USER_ID";

    public static final String INSTITUTION_ID = "INSTITUTION_ID";

    public static final String INSTITUTION_CODE = "INSTITUTION_CODE" ;

    public static final String LANGUAGE = "LANGUAGE" ;

    //region Response Status Id's
    public static final int RESPONSE_HEADER_STATUS_SUCCESS_GENERAL = 1;
    /**
     * The constant RESPONSE_HEADER_STATUS_FAILED_GENERAL.
     */
    public static final int RESPONSE_HEADER_STATUS_FAILED_GENERAL = -1;
    /**
     * The constant RESPONSE_HEADER_STATUS_FAILED_UNKNOWN_MSG.
     */
    public static final int RESPONSE_HEADER_STATUS_FAILED_UNKNOWN_MSG = -2;
    /**
     * The constant RESPONSE_HEADER_STATUS_FAILED_SESSION.
     */
    public static final int RESPONSE_HEADER_STATUS_FAILED_SESSION = -3;
    /**
     * The constant RESPONSE_HEADER_STATUS_FAILED_AUTHORIZED.
     */
    public static final int RESPONSE_HEADER_STATUS_FAILED_AUTHORIZED = -4;
    /**
     * The constant RESPONSE_AUTHORIZED_SUCCESS.
     */
    public static final int RESPONSE_AUTHORIZED_SUCCESS = 1;
    /**
     * The constant RESPONSE_AUTHORIZED_FAIL.
     */
    public static final int RESPONSE_AUTHORIZED_FAIL = -1;
    /**
     * The constant RESPONSE_AUTHORIZED_FAIL.
     */
    public static final int RESPONSE_INVALID_FILE = -99;
    //endregion



    //region GROUP_AUTHENTICATION - request Type Ids
    public static final int REQUEST_TYPE_LOGIN_NORMAL = 1;
    /**
     * The constant REQUEST_TYPE_LOGOUT_NORMAL.
     */
    public static final int REQUEST_TYPE_LOGOUT_NORMAL = 2;


    public static final int RESPONSE_TYPE_LOGIN_NORMAL = 1001;

    public static final int RESPONSE_TYPE_LOGOUT_NORMAL = 1002;

    //***********************   END GROUP_AUTHENTICATION ***********************

//    BEGIN EMISSON

    public static final int REQUEST_TYPE_MANAGED_AIRTRAVEL = 1;
    public static final int REQUEST_TYPE_MANAGED_ELECTRICTY = 2;
    public static final int REQUEST_TYPE_MANAGED_FIRE_EXT = 3;
    public static final int REQUEST_TYPE_MANAGED_GENERATORS = 4;
    public static final int REQUEST_TYPE_MANAGED_MUN_WATER = 5;
    public static final int REQUEST_TYPE_MANAGED_REFRI = 6;
    public static final int REQUEST_TYPE_MANAGED_TRANS_LOCAL = 7;
    public static final int REQUEST_TYPE_MANAGED_VEHICLE = 8;
    public static final int REQUEST_TYPE_MANAGED_WASTE_DIS = 9;
    public static final int REQUEST_TYPE_MANAGED_WASTE_TRANS = 10;
    public static final int REQUEST_TYPE_LIST_AIRTRAVEL = 11;
    public static final int REQUEST_TYPE_LIST_ELECTRICTY = 12;
    public static final int REQUEST_TYPE_LIST_FIRE_EXT = 13;
    public static final int REQUEST_TYPE_LIST_GENERATORS = 14;
    public static final int REQUEST_TYPE_LIST_MUN_WATER = 15;
    public static final int REQUEST_TYPE_LIST_REFRI = 16;
    public static final int REQUEST_TYPE_LIST_TRANS_LOCAL = 17;
    public static final int REQUEST_TYPE_LIST_VEHICLE = 18;
    public static final int REQUEST_TYPE_LIST_WASTE_DIS = 19;
    public static final int REQUEST_TYPE_LIST_WASTE_TRANS = 20;
    public static final int REQUEST_TYPE_LIST_EMP_COMMUTING = 21;
    public static final int REQUEST_TYPE_MANAGE_EMP_COMMUTING = 22;
    public static final int REQUEST_TYPE_GHG_EMISSION_SUMMARY = 23;
    public static final int REQUEST_TYPE_GHG_EMISSION_BRANCHWISE = 24;
    public static final int REQUEST_TYPE_GHG_EMISSION_SUMMARY_EXCLUDED = 25;
    public static final int REQUEST_TYPE_MANAGE_GHG_EMISSION_SUMMARY = 26;
    public static final int REQUEST_TYPE_EXPORT_GHG_EMISSION_SUMMARY = 27;
    public static final int REQUEST_TYPE_LIST_SEA_AIR_FRIGHT = 28;
    public static final int REQUEST_TYPE_MANAGED_SEA_AIR_FREIGHT = 29;
    public static final int REQUEST_TYPE_LIST_BIO_MASS = 30;
    public static final int REQUEST_TYPE_MANAGED_BIO_MASS = 31;
    public static final int REQUEST_TYPE_LIST_LPG_ENTRY = 32;
    public static final int REQUEST_TYPE_MANAGED_LPG_ENTRY = 33;
    public static final int REQUEST_TYPE_LIST_BGAS_ENTRY = 58;
    public static final int REQUEST_TYPE_MANAGED_BGAS_ENTRY = 59;

    public static final int REQUEST_TYPE_MANAGED_ASH_TRANSPORTATION = 34;
    public static final int REQUEST_TYPE_LIST_ASH_TRANSPORTATION = 35;
    public static final int REQUEST_TYPE_MANAGED_FORKLIFTS = 36;
    public static final int REQUEST_TYPE_LIST_FORKLIFTS = 37;
    public static final int REQUEST_TYPE_MANAGED_FURNACE_OIL = 38;
    public static final int REQUEST_TYPE_LIST_FURNACE_OIL = 39;
    public static final int REQUEST_TYPE_MANAGED_LORRY_TRANSPORTATION = 40;
    public static final int REQUEST_TYPE_LIST_LORRY_TRANSPORTATION = 41;
    public static final int REQUEST_TYPE_MANAGED_OILGAS_TRANSPORTATION = 42;
    public static final int REQUEST_TYPE_LIST_OILGAS_TRANSPORTATION = 43;
    public static final int REQUEST_TYPE_MANAGED_PAID_MANAGER_VEHICLE = 44;
    public static final int REQUEST_TYPE_LIST_PAID_MANAGER_VEHICLE = 45;
    public static final int REQUEST_TYPE_MANAGED_PAPER_WASTE = 46;
    public static final int REQUEST_TYPE_LIST_PAPER_WASTE = 47;
    public static final int REQUEST_TYPE_MANAGED_RAW_MATERIAL_TRANSPORTATION = 48;
    public static final int REQUEST_TYPE_LIST_RAW_MATERIAL_TRANSPORTATION = 49;
    public static final int REQUEST_TYPE_MANAGED_SAW_DUST_TRANSPORTATION = 50;
    public static final int REQUEST_TYPE_LIST_SAW_DUST_TRANSPORTATION = 51;
    public static final int REQUEST_TYPE_MANAGED_SLUDGE_TRANSPORTATION = 52;
    public static final int REQUEST_TYPE_LIST_SLUDGE_TRANSPORTATION  = 53;
    public static final int REQUEST_TYPE_MANAGED_STAFF_TRANSPORTATION = 54;
    public static final int REQUEST_TYPE_LIST_STAFF_TRANSPORTATION = 55;
    public static final int REQUEST_TYPE_MANAGED_VEHICLE_OTHERS_TRANSPORTATION = 56;
    public static final int REQUEST_TYPE_LIST_VEHICLE_OTHERS_TRANSPORTATION = 57;







    public static final int RESPONSE_TYPE_MANAGED_AIRTRAVEL = 1001;
    public static final int RESPONSE_TYPE_MANAGED_ELECTRICTY = 1002;
    public static final int RESPONSE_TYPE_MANAGED_FIRE_EXT = 1003;
    public static final int RESPONSE_TYPE_MANAGED_GENERATORS = 1004;
    public static final int RESPONSE_TYPE_MANAGED_MUN_WATER = 1005;
    public static final int RESPONSE_TYPE_MANAGED_REFRI = 1006;
    public static final int RESPONSE_TYPE_MANAGED_TRANS_LOCAL = 1007;
    public static final int RESPONSE_TYPE_MANAGED_VEHICLE = 1008;
    public static final int RESPONSE_TYPE_MANAGED_WASTE_DIS = 1009;
    public static final int RESPONSE_TYPE_MANAGED_WASTE_TRANS = 1010;
    public static final int RESPONSE_TYPE_LIST_AIRTRAVEL = 1011;
    public static final int RESPONSE_TYPE_LIST_ELECTRICTY = 1012;
    public static final int RESPONSE_TYPE_LIST_FIRE_EXT = 1013;
    public static final int RESPONSE_TYPE_LIST_GENERATORS = 1014;
    public static final int RESPONSE_TYPE_LIST_MUN_WATER = 1015;
    public static final int RESPONSE_TYPE_LIST_REFRI = 1016;
    public static final int RESPONSE_TYPE_LIST_TRANS_LOCAL = 1017;
    public static final int RESPONSE_TYPE_LIST_VEHICLE = 1018;
    public static final int RESPONSE_TYPE_LIST_WASTE_DIS = 1019;
    public static final int RESPONSE_TYPE_LIST_WASTE_TRANS = 1020;

//    END EMISSION

    //    Begin business Users
    public static final int REQUEST_TYPE_MANAGED_EMPLOYEE = 1;
    public static final int REQUEST_TYPE_MANAGED_ADMIN = 2;
    public static final int REQUEST_TYPE_MANAGED_CLIENT = 3;
    public static final int REQUEST_TYPE_MANAGED_COMADMIN = 4;
    public static final int REQUEST_TYPE_LIST_EMPLOYEE = 5;
    public static final int REQUEST_TYPE_LIST_ADMIN = 6;
    public static final int REQUEST_TYPE_LIST_CLIENT = 7;
    public static final int REQUEST_TYPE_LIST_COMADMIN = 8;
    public static final int REQUEST_TYPE_MANAGED_DATA_ENTRY_USER = 9;
    public static final int REQUEST_TYPE_LIST_DATA_ENTRY_USER = 10;


//    end business users

//    begin institution

    public static final int REQUEST_TYPE_MANAGED_BRANCH = 1;
    public static final int REQUEST_TYPE_MANAGED_COMPANY = 2;
    public static final int REQUEST_TYPE_LIST_BRANCH = 3;
    public static final int REQUEST_TYPE_LIST_COMPANY = 4;
    public static final int REQUEST_TYPE_LIST_COMPANY_DATA_ENTRY = 5;
    public static final int REQUEST_TYPE_LIST_COMPANY_SUMMARY_INPUTS = 6;
    public static final int REQUEST_TYPE_MANAGE_MENU_ = 7;
    public static final int REQUEST_TYPE_UPDATE_CATEGORY = 8; // ADD , EDIT AND DELETE
    public static final int REQUEST_TYPE_UPDATE_EXCLUDED_REASONS_AND_SUGGESTIONS = 9;



//    end institution


    //    begin login user
    public static final int REQUEST_TYPE_MANAGE_LOGIN_PROFILE = 1;
    public static final int REQUEST_TYPE_FIND_LOGIN_PROFILE = 2;
    public static final int REQUEST_TYPE_LIST_USR_ACT_LOGS = 3;
    public static final int REQUEST_TYPE_CREATE_LOGIN_PROFILE_FOR_BRANCHES = 4;

//


    //    Master Data
    public static final int REQUEST_TYPE_LIST_COUNTRIES = 1;
    public static final int REQUEST_TYPE_LIST_AIRPORTS = 2;


    //    Group Emission factors
    public static final int REQUEST_TYPE_MANAGED_EM_FACTOR = 1;
    public static final int REQUEST_TYPE_MANAGED_PUB_TRANS_FACTOR = 2;
    public static final int REQUEST_TYPE_MANAGED_WASTE_DIS_FACTOR = 3;
    public static final int REQUEST_TYPE_LIST_EM_FACTOR = 4;
    public static final int REQUEST_TYPE_LIST_PUB_TRANS_FACTOR = 5;
    public static final int REQUEST_TYPE_LIST_WASTE_DIS_FACTOR = 6;


    //    group ghg project
    public static final int REQUEST_TYPE_MANAGE_PROJECT = 1;
    public static final int REQUEST_TYPE_LIST_PROJECT = 2;

//    summary inputs

    public static final int  REQUEST_TYPE_ELEC_SUMM_INPUTS =  1;
    public static final int  REQUEST_TYPE_MUN_WATER_SUMM_INPUTS =  2;
    public static final int  REQUEST_TYPE_REFRI_SUMM_INPUTS =  3;
    public static final int  REQUEST_TYPE_FIRE_EXT_SUMM_INPUTS =  4;
    public static final int  REQUEST_TYPE_WASTE_TRANS_SUMM_INPUTS =  5;
    public static final int  REQUEST_TYPE_COMPANY_OWNED_SUMM_INPUTS =  6;
    public static final int  REQUEST_TYPE_EMP_COMM_SUMM_INPUTS =  7;
    public static final int  REQUEST_TYPE_BUS_AIRTRAVEL_SUMM_INPUTS =  8;
    public static final int  REQUEST_TYPE_RENTED_SUMM_INPUTS =  9;
    public static final int  REQUEST_TYPE_HIRED_SUMM_INPUTS =  10;
    public static final int  REQUEST_TYPE_GENERATORS_SUMM_INPUTS =  11;
    public static final int  REQUEST_TYPE_WASTE_DIS_SUMM_INPUTS =  12;
    public static final int  REQUEST_TYPE_TRANS_LOC_PUR_SUMM_INPUTS =  13;

    public static final int REQUEST_TYPE_DATA_ENTRY_STATUS = 14;
    public static final int REQUEST_TYPE_MANAGE_DATA_ENTRY_STATUS = 15;

    public static final int REQUEST_TYPE_SEA_AIR_FREIGHT_SUMM_INPUTS= 16;

    public static final int REQUEST_TYPE_BIO_MASS_SUMM_INPUTS= 17;
    public static final int REQUEST_TYPE_LPG_ENTRY_SUMM_INPUTS= 18;

    public static final int REQUEST_TYPE_GET_PROJECT_FULL_SUMMARY = 19;


//    Report
    public static final int REQUEST_TYPE_MANAGE_GHG_REPORT = 1;
    public static final int REQUEST_TYPE_LIST_GHG_REPORT = 2;
    public static final int REQUEST_TYPE_MANAGE_REPORT_META_DATA = 3;
    public static final int REQUEST_TYPE_LIST_REPORT_META_DATA = 4;
    public static final int REQUEST_TYPE_GET_REPORT_ZIP = 5;

    
    public static final int REQUEST_TYPE_MANAGE_MITIGATION_PROJECT =1;
    public static final int REQUEST_TYPE_GET_MITIGATION_PROJECTS = 2;
    public static final int REQUEST_TYPE_MANAGE_MITIGATION_PROJECT_MONTHLY_ACTIVITY=3;
    public static final int REQUEST_TYPE_GET_MITIGATION_PROJECT_MONTHLY_ACTIVITY=4;



}
