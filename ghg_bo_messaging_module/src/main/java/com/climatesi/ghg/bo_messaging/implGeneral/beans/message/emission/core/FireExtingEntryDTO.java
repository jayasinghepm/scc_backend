package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class FireExtingEntryDTO {

    @SerializedName("FIRE_EXT_ENTRY_ID")
    private int fireExtEntryId;


    @SerializedName("EMISSION_SRC_ID")
    private int emissionSrcId;

    @SerializedName("ADDED_BY")
    private int addedBy;

    @SerializedName("BRANCH_ID")
    private int branchId;

    @SerializedName("COMPANY_ID")
    private int companyId;

    @SerializedName("ADDED_DATE")
    private Date addedDate;

    @SerializedName("LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @SerializedName("MONTH")
    private int month;

    @SerializedName("YEAR")
    private String year;

    @SerializedName("WEIGHT")
    private float weight;

    @SerializedName("AMOUNT_REFILLED")
    private float amountRefilled;

    @SerializedName("FIRE_EXT_TYPE")
    private int fireExtType;

    @SerializedName("NO_OF_TANKS")
    private int noOfTanks;

    @SerializedName("EMISSION_INFO")
    private JsonObject emissionDetails;

    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    private String fireExt;

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getFireExt() {
        return fireExt;
    }

    public void setFireExt(String fireExt) {
        this.fireExt = fireExt;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getFireExtType() {
        return fireExtType;
    }

    public void setFireExtType(int fireExtType) {
        this.fireExtType = fireExtType;
    }

    public int getNoOfTanks() {
        return noOfTanks;
    }

    public void setNoOfTanks(int noOfTanks) {
        this.noOfTanks = noOfTanks;
    }

    public int getFireExtEntryId() {
        return this.fireExtEntryId;
    }


    public void setFireExtEntryId(int id) {
        this.fireExtEntryId = id;
    }


    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }


    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }


    public int getAddedBy() {
        return this.addedBy;
    }


    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }


    public int getBranchId() {
        return this.branchId;
    }


    public void setBranchId(int id) {
        this.branchId = id;
    }


    public int getCompanyId() {
        return this.companyId;
    }


    public void setCompanyId(int id) {
        this.companyId = id;
    }


    public Date getEntryAddedDate() {
        return this.addedDate;
    }


    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }


    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }


    public int getMonth() {
        return this.month;
    }


    public void setMonth(int month) {
        this.month = month;
    }


    public String getYear() {
        return this.year;
    }


    public void setYear(String year) {
        this.year = year;
    }


    public float getWeight() {
        return this.weight;
    }


    public void setWeight(float weight) {
        this.weight = weight;
    }


    public float getAmountRefilled() {
        return this.amountRefilled;
    }


    public void setAmountRefilled(float amountRefilled) {
        this.amountRefilled = amountRefilled;
    }


    public int getFireExtinguisherType() {
        return this.fireExtType;
    }


    public void setFireExtinguisherType(int type) {
        this.fireExtType = type;
    }
}
