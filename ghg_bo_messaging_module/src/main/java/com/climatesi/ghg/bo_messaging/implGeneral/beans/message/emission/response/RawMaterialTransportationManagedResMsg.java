package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RawMaterialTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class RawMaterialTransportationManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private RawMaterialTransportationDTO dto;

    public RawMaterialTransportationDTO getDto() {
        return dto;
    }

    public void setDto(RawMaterialTransportationDTO dto) {
        this.dto = dto;
    }
}
