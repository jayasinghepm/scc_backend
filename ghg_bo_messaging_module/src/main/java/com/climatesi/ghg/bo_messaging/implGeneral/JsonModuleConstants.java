package com.climatesi.ghg.bo_messaging.implGeneral;

public class JsonModuleConstants {

    /**
     * The constant JSON_MODULE_BASE_PACKAGE.
     */
    public static final String JSON_MODULE_BASE_PACKAGE = "com.climatesi.ghg.bo_messaging.";
    /**
     * The constant ENVELOP_BEAN_IMPL_PATH.
     */
    public static final String ENVELOP_BEAN_IMPL_PATH = "implGeneral.beans.envelope.";
}
