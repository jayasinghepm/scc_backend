package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;

public class GHGEmissionSummaryDTO {

    private int mode;

    private int companyId;

    private String fy;

    private int isDeleted;

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    private JsonObject emissionInfo;

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getFy() {
        return fy;
    }

    public void setFy(String fy) {
        this.fy = fy;
    }

    public JsonObject getEmissionInfo() {
        return emissionInfo;
    }

    public void setEmissionInfo(JsonObject emissionInfo) {
        this.emissionInfo = emissionInfo;
    }
}
