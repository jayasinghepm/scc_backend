package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.res;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.GHGReportDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListGHGReportResMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<GHGReportDTO> list;

    public List<GHGReportDTO> getList() {
        return list;
    }

    public void setList(List<GHGReportDTO> list) {
        this.list = list;
    }
}
