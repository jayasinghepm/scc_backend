package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityEntryDTO;
import com.google.gson.annotations.SerializedName;

public class ElectricityEntryMangedResponseMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private ElectricityEntryDTO dto;

    public ElectricityEntryDTO getDto() {
        return dto;
    }

    public void setDto(ElectricityEntryDTO dto) {
        this.dto = dto;
    }


}
