package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.CommonSummaryInputDTO;
import com.google.gson.annotations.SerializedName;

public class CommonSummaryInputResMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private CommonSummaryInputDTO dto;

    public CommonSummaryInputDTO getDto() {
        return dto;
    }

    public void setDto(CommonSummaryInputDTO dto) {
        this.dto = dto;
    }
}
