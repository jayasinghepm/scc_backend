package com.climatesi.ghg.bo_messaging.api.exceptions;

public class MessageProtocolException extends Exception {

    /**
     * Instantiates a new Message protocol exception.
     *
     * @param message the message
     */
    public MessageProtocolException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Message protocol exception.
     *
     * @param message the message
     * @param ex      the ex
     */
    public MessageProtocolException(String message, Exception ex) {
        super(message, ex);
    }
}

