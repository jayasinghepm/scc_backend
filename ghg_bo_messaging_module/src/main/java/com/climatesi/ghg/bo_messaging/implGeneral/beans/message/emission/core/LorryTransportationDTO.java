package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;
import com.google.gson.JsonObject;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

public class LorryTransportationDTO {


    private int entryId;



    private int emissionSrcId;


    private int addedBy;


    private int branchId;


    private int companyId;


    private Date addedDate;


    private Date lastUpdatedDate;


    private int month;


    private String year;

    private float distance;

    private float fuelEconomy;


    private int transportMode; //1-external , 2-interal




    private JsonObject emissionDetails;


    private String generatorNumber;


    private int isDeleted;

    private String company;

    private String branch;

    private String mon;


    private float noOfTrips = 1;

    private float fuelConsumption; //liters

    public float getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(float noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }


    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getEmissionSrcId() {
        return emissionSrcId;
    }

    public void setEmissionSrcId(int emissionSrcId) {
        this.emissionSrcId = emissionSrcId;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getFuelEconomy() {
        return fuelEconomy;
    }

    public void setFuelEconomy(float fuelEconomy) {
        this.fuelEconomy = fuelEconomy;
    }

    public int getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(int transportMode) {
        this.transportMode = transportMode;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public String getGeneratorNumber() {
        return generatorNumber;
    }

    public void setGeneratorNumber(String generatorNumber) {
        this.generatorNumber = generatorNumber;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }
}
