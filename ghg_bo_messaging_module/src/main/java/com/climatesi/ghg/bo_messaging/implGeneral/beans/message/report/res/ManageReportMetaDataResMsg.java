package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.res;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.ReportMetaDataDTO;
import com.google.gson.annotations.SerializedName;

public class ManageReportMetaDataResMsg extends EntityMasterResponseBean {

    @SerializedName("DATA")
    private ReportMetaDataDTO dto;

    public ReportMetaDataDTO getDto() {
        return dto;
    }

    public void setDto(ReportMetaDataDTO dto) {
        this.dto = dto;
    }
}
