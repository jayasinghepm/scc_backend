package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

public class FindLoginUserReqMsg implements Message {

    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
