package com.climatesi.ghg.bo_messaging.implGeneral.beans.envelope.emission_sources.request;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.google.gson.annotations.SerializedName;

import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.HEADER;

public class EmissionSourceListRequestEnv implements Envelope {

    @SerializedName(HEADER)
    private HeaderBean header;


    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean) header;
    }

    public Object getBody() {
        return null;
    }

    public void setBody(Object body) {

    }
}
