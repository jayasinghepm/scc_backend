package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.response;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;

public class ManageMitigationProjectResMsg extends EntityMasterResponseBean {

    private int projectId;

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
