package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GeneratorsEntryDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GeneratorsEntryListResponeMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<GeneratorsEntryDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
