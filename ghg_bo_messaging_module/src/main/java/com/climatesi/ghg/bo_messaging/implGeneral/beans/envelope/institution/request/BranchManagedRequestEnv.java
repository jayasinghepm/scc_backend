package com.climatesi.ghg.bo_messaging.implGeneral.beans.envelope.institution.request;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request.BranchManagedRequestMsg;
import com.google.gson.annotations.SerializedName;

import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.HEADER;
import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.MESSAGE_BODY;

public class BranchManagedRequestEnv implements Envelope {
    @SerializedName(HEADER)
    private HeaderBean header;

    @SerializedName(MESSAGE_BODY)
    private BranchManagedRequestMsg body;

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean) header;
    }

    public Object getBody() {
        return this.body;
    }

    public void setBody(Object body) {
        this.body = (BranchManagedRequestMsg) body;
    }
}
