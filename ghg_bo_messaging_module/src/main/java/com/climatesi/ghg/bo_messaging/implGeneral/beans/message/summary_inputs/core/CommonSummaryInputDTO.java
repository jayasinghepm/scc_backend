package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

import com.google.gson.JsonObject;

public class CommonSummaryInputDTO {

    private JsonObject jan;
    private JsonObject feb;
    private JsonObject mar;
    private JsonObject apr;
    private JsonObject may;
    private JsonObject jun;
    private JsonObject jul;
    private JsonObject aug;
    private JsonObject sep;
    private JsonObject oct;
    private JsonObject nov;
    private JsonObject dec;
    private JsonObject all;

    public JsonObject getJan() {
        return jan;
    }

    public void setJan(JsonObject jan) {
        this.jan = jan;
    }

    public JsonObject getFeb() {
        return feb;
    }

    public void setFeb(JsonObject feb) {
        this.feb = feb;
    }

    public JsonObject getMar() {
        return mar;
    }

    public void setMar(JsonObject mar) {
        this.mar = mar;
    }

    public JsonObject getApr() {
        return apr;
    }

    public void setApr(JsonObject apr) {
        this.apr = apr;
    }

    public JsonObject getMay() {
        return may;
    }

    public void setMay(JsonObject may) {
        this.may = may;
    }

    public JsonObject getJun() {
        return jun;
    }

    public void setJun(JsonObject jun) {
        this.jun = jun;
    }

    public JsonObject getJul() {
        return jul;
    }

    public void setJul(JsonObject jul) {
        this.jul = jul;
    }

    public JsonObject getAug() {
        return aug;
    }

    public void setAug(JsonObject aug) {
        this.aug = aug;
    }

    public JsonObject getSep() {
        return sep;
    }

    public void setSep(JsonObject sep) {
        this.sep = sep;
    }

    public JsonObject getOct() {
        return oct;
    }

    public void setOct(JsonObject oct) {
        this.oct = oct;
    }

    public JsonObject getNov() {
        return nov;
    }

    public void setNov(JsonObject nov) {
        this.nov = nov;
    }

    public JsonObject getDec() {
        return dec;
    }

    public void setDec(JsonObject dec) {
        this.dec = dec;
    }

    public JsonObject getAll() {
        return all;
    }

    public void setAll(JsonObject all) {
        this.all = all;
    }
}
