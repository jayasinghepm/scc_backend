package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.BranchDTO;
import com.google.gson.annotations.SerializedName;

public class BranchManagedRequestMsg implements Message {

    @SerializedName("DATA")
    private BranchDTO dto;


    public BranchDTO getDto() {
        return dto;
    }

    public void setDto(BranchDTO dto) {
        this.dto = dto;
    }
}
