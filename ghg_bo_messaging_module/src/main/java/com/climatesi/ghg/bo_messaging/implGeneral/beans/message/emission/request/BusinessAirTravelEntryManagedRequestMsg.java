package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.google.gson.annotations.SerializedName;

public class BusinessAirTravelEntryManagedRequestMsg implements Message {

    @SerializedName("DATA")
    private BusinessAirTravelEntryDTO dto;

    public BusinessAirTravelEntryDTO getDto() {
        return dto;
    }

    public void setDto(BusinessAirTravelEntryDTO dto) {
        this.dto = dto;
    }
}
