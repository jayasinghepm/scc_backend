package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.ManageMitigationActivityData;

public class ManageMitigationActDataReqMsg implements Message {

    private ManageMitigationActivityData data;

    public ManageMitigationActivityData getData() {
        return data;
    }

    public void setData(ManageMitigationActivityData data) {
        this.data = data;
    }
}
