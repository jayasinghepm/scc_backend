package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SludgeTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class SludgeTransportationManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private SludgeTransportationDTO dto;

    public SludgeTransportationDTO getDto() {
        return dto;
    }

    public void setDto(SludgeTransportationDTO dto) {
        this.dto = dto;
    }
}
