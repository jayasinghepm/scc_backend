package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationActDataByCompany;

public class GetMitigationActDataReqMsg implements Message {


    private int comId;

    private int branchId;

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }
}
