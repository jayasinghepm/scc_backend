package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GHGEmissionSummaryDTO;
import com.google.gson.annotations.SerializedName;

public class ManageGHGEmissionSummaryReqMsg implements Message {


    @SerializedName("DATA")
    private GHGEmissionSummaryDTO dto;

    public GHGEmissionSummaryDTO getDto() {
        return dto;
    }

    public void setDto(GHGEmissionSummaryDTO dto) {
        this.dto = dto;
    }
}
