package com.climatesi.ghg.bo_messaging.implGeneral.beans.message;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;

import java.util.List;
import java.util.Map;

public class AuthenticateResponseBean implements Message {


    private int authenticationStatus;


    private String narration;


    private String sessionId;


    private int instituteId;


    private long userId;


    private String userName;


    private String userFirstName;


    private String userLastName;


    private int userType;


    private String mobile;


    private String email;


    private String userAddress;

    private int existActiveProject;




    private long clientId;
    private long adminId;
    private int dataEntryUserId;
    private long cadminId;
    private int isFirstime;
    private long companyId;
    private long branchId;

    private String firstName;
    private String lastName;

    private List<String> userEntitlementList;
    private int cadminStatus;
    private String cadminStatusMsg;

    private int dataEntryExpired;

    private int projectStatus;


    private String branchName;
    private String companyName;

    private int isMasterAdmin;


    private List<Integer> pages;

    private List<Integer> emSources;



    private Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources ;

    private int disableFormulas;

    public int getDisableFormulas() {
        return disableFormulas;
    }

    public void setDisableFormulas(int disableFormulas) {
        this.disableFormulas = disableFormulas;
    }

    public Map<Integer, AllowedEmissionSrcBean> getAllowedEmissionSources() {
        return allowedEmissionSources;
    }

    public void setAllowedEmissionSources(Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources) {
        this.allowedEmissionSources = allowedEmissionSources;
    }


    public List<Integer> getPages() {
        return pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }

    public List<Integer> getEmSources() {
        return emSources;
    }

    public void setEmSources(List<Integer> emSources) {
        this.emSources = emSources;
    }

    public int getDataEntryUserId() {
        return dataEntryUserId;
    }

    public void setDataEntryUserId(int dataEntryUserId) {
        this.dataEntryUserId = dataEntryUserId;
    }

    public int getIsMasterAdmin() {
        return isMasterAdmin;
    }

    public void setIsMasterAdmin(int isMasterAdmin) {
        this.isMasterAdmin = isMasterAdmin;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public int getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(int projectStatus) {
        this.projectStatus = projectStatus;
    }

    public void setDataEntryExpired(int dataEntryExpired) {
        this.dataEntryExpired = dataEntryExpired;
    }

    public int getExistActiveProject() {
        return existActiveProject;
    }

    public void setExistActiveProject(int existActiveProject) {
        this.existActiveProject = existActiveProject;
    }

    public int getCadminStatus() {
        return cadminStatus;
    }

    public void setCadminStatus(int cadminStatus) {
        this.cadminStatus = cadminStatus;
    }

    public String getCadminStatusMsg() {
        return cadminStatusMsg;
    }

    public void setCadminStatusMsg(String cadminStatusMsg) {
        this.cadminStatusMsg = cadminStatusMsg;
    }


    public void setAuthenticationStatus(int authenticationStatus) {
        this.authenticationStatus = authenticationStatus;
    }

    public void setInstituteId(int instituteId) {
        this.instituteId = instituteId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getAdminId() {
        return adminId;
    }

    public void setAdminId(long adminId) {
        this.adminId = adminId;
    }

    public long getCadminId() {
        return cadminId;
    }

    public void setCadminId(long cadminId) {
        this.cadminId = cadminId;
    }

    public int getIsFirstime() {
        return isFirstime;
    }

    public void setIsFirstime(int isFirstime) {
        this.isFirstime = isFirstime;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public long getBranchId() {
        return branchId;
    }

    public void setBranchId(long branchId) {
        this.branchId = branchId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<String> getUserEntitlementList() {
        return userEntitlementList;
    }

    public void setUserEntitlementList(List<String> userEntitlementList) {
        this.userEntitlementList = userEntitlementList;
    }

    /**
     * Gets authentication status.
     *
     * @return the authentication status
     */
    public Integer getAuthenticationStatus() {
        return authenticationStatus;
    }

    /**
     * Sets authentication status.
     *
     * @param authenticationStatus the authentication status
     */
    public void setAuthenticationStatus(Integer authenticationStatus) {
        this.authenticationStatus = authenticationStatus;
    }

    /**
     * Gets narration.
     *
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * Sets narration.
     *
     * @param narration the narration
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * Gets session id.
     *
     * @return the session id
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets session id.
     *
     * @param sessionId the session id
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Gets emp id.
     *
     * @return the emp id
     */
    public long getUserId() {
        return this.userId;
    }

    /**
     * Gets institute id.
     *
     * @return the institute id
     */
    public Integer getInstituteId() {
        return instituteId;
    }

    /**
     * Sets emp id.
     *
     * @param userId the emp id
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Sets institute id.
     *
     * @param instituteId the institute id
     */
    public void setInstituteId(Integer instituteId) {
        this.instituteId = instituteId;
    }

    /**
     * Gets user first name.
     *
     * @return the user first name
     */
    public String getUserFirstName() {
        return userFirstName;
    }

    /**
     * Sets user first name.
     *
     * @param userFirstName the user first name
     */
    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    /**
     * Gets user second name.
     *
     * @return the user second name
     */
    public String getUserLastName() {
        return userLastName;
    }

    /**
     * Sets user second name.
     *
     * @param userLastName the user second name
     */
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    /**
     * Gets user type.
     *
     * @return the user type
     */
    public int getUserType() {
        return userType;
    }

    /**
     * Sets user type.
     *
     * @param userType the user type
     */
    public void setUserType(int userType) {
        this.userType = userType;
    }

    /**
     * Gets mobile.
     *
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets mobile.
     *
     * @param mobile the mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets user address.
     *
     * @return the user address
     */
    public String getUserAddress() {
        return userAddress;
    }

    /**
     * Sets user address.
     *
     * @param userAddress the user address
     */
    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

//    /**
//     * Gets user entitlement list.
//     *
//     * @return the user entitlement list
//     */
//    public List<Integer> getUserEntitlementList() {
//        return userEntitlementList;
//    }
//
//    /**
//     * Sets user entitlement list.
//     *
//     * @param userEntitlementList the user entitlement list
//     */
//    public void setUserEntitlementList(List<Integer> userEntitlementList) {
//        this.userEntitlementList = userEntitlementList;
//    }
}