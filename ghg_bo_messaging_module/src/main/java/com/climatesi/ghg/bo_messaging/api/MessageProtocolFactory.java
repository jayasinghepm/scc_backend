package com.climatesi.ghg.bo_messaging.api;

import com.climatesi.ghg.bo_messaging.implGeneral.MessageProtocolFacade;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

public class MessageProtocolFactory {

    private static Logger logger = Logger.getLogger(MessageProtocolFactory.class);

    private static MessageProtocolFactory messageProtocolFactory;
    private static MessageProtocolManager messageProtocolManager;

    private MessageProtocolFactory() {
        //Left Intentionally
    }

    /**
     * Gets instance.
     *
     * @return the instance
     * @throws GHGException the mubasher exception
     */
    public static MessageProtocolFactory getInstance() throws GHGException {
        try {
            if (messageProtocolFactory == null) {
                messageProtocolFactory = new MessageProtocolFactory();
            }
            if (messageProtocolManager == null) {
                messageProtocolManager = new MessageProtocolFacade();
            }

            return messageProtocolFactory;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new GHGException("Error in get instance of MessageProtocolFactory", ex);
        }
    }

    /**
     * Gets message protocol manager.
     *
     * @return the message protocol manager
     */
    public MessageProtocolManager getMessageProtocolManager() {
        return messageProtocolManager;
    }

}

