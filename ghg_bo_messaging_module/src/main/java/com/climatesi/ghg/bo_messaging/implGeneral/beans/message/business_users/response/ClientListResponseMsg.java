package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.ClientDTO;

import java.util.List;

public class ClientListResponseMsg extends EntityListResponseBean {

    private List<ClientDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
