package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RefrigerantsEntryDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RefrigerantsEntryListResponeMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<RefrigerantsEntryDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

}
