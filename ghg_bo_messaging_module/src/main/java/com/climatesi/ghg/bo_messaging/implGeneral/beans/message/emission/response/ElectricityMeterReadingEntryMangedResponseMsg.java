package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityMeterReadingEntryDTO;
import com.google.gson.annotations.SerializedName;

public class ElectricityMeterReadingEntryMangedResponseMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private ElectricityMeterReadingEntryDTO dto;

    public ElectricityMeterReadingEntryDTO getDto() {
        return dto;
    }

    public void setDto(ElectricityMeterReadingEntryDTO dto) {
        this.dto = dto;
    }
}
