package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core;

public class PubTransEmFactorDTO {

    private int vehicleType;

    private String vehicleName;

    private float g_per_km;

    private float co2_per_km;

    private String assumption;

    public int getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(int vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public float getG_per_km() {
        return g_per_km;
    }

    public void setG_per_km(float g_per_km) {
        this.g_per_km = g_per_km;
    }

    public float getCo2_per_km() {
        return co2_per_km;
    }

    public void setCo2_per_km(float co2_per_km) {
        this.co2_per_km = co2_per_km;
    }

    public String getAssumption() {
        return assumption;
    }

    public void setAssumption(String assumption) {
        this.assumption = assumption;
    }
}
