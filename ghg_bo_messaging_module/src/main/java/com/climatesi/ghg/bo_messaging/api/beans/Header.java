package com.climatesi.ghg.bo_messaging.api.beans;

public interface Header {
    /**
     * Gets protocol version.
     *
     * @return the protocol version
     */
    String getProtocolVersion();

    /**
     * Sets protocol version.
     *
     * @param protocolVersion the protocol version
     */
    void setProtocolVersion(String protocolVersion);

    /**
     * Gets session id.
     *
     * @return the session id
     */
    String getSessionId();

    /**
     * Sets session id.
     *
     * @param sessionId the session id
     */
    void setSessionId(String sessionId);

    /**
     * Gets message group.
     *
     * @return the message group
     */
    int getMessageGroup();

    /**
     * Sets message group.
     *
     * @param messageGroup the message group
     */
    void setMessageGroup(int messageGroup);

    /**
     * Gets message type.
     *
     * @return the message type
     */
    int getMessageType();

    /**
     * Sets message type.
     *
     * @param messageType the message type
     */
    void setMessageType(int messageType);

    /**
     * Gets channel id.
     *
     * @return the channel id
     */
    int getChannelId();

    /**
     * Sets channel id.
     *
     * @param channelId the channel id
     */
    void setChannelId(int channelId);

    /**
     * Gets client ip.
     *
     * @return the client ip
     */
    String getClientIp();

    /**
     * Sets client ip.
     *
     * @param clientIp the client ip
     */
    void setClientIp(String clientIp);

    /**
     * Gets client version.
     *
     * @return the client version
     */
    String getClientVersion();

    /**
     * Sets client version.
     *
     * @param clientVersion the client version
     */
    void setClientVersion(String clientVersion);

    /**
     * Gets client req id.
     *
     * @return the client req id
     */
    String getClientReqID();

    /**
     * Sets client req id.
     *
     * @param clientReqID the client req id
     */
    void setClientReqID(String clientReqID);

    /**
     * Gets response status.
     *
     * @return the response status
     */
    int getResponseStatus();

    /**
     * Sets response status.
     *
     * @param responseStatus the response status
     */
    void setResponseStatus(int responseStatus);

    /**
     * Gets response narration.
     *
     * @return the response narration
     */
    String getResponseNarration();

    /**
     * Sets response narration.
     *
     * @param responseNarration the response narration
     */
    void setResponseNarration(String responseNarration);

    /**
     * Gets logged in user id.
     *
     * @return the logged in user id
     */
    String getLoggedInUserId();

    /**
     * Sets logged in user id.
     *
     * @param loggedInUserId the logged in user id
     */
    void setLoggedInUserId(String loggedInUserId);

    /**
     * Gets institution id.
     *
     * @return the institution id
     */
    int getInstitutionId();

    /**
     * Sets institution id.
     *
     * @param institutionId the institution id
     */
    void setInstitutionId(int institutionId);

    /**
     * Gets institution code.
     *
     * @return the institution code
     */
    String getInstitutionCode();

    /**
     * Sets institution code.
     *
     * @param institutionCode the institution code
     */
    void setInstitutionCode(String institutionCode);

    /**
     * Gets language.
     *
     * @return the language
     */
    String getLanguage();

    /**
     * Sets language.
     *
     * @param language the language
     */
    void setLanguage(String language);
}

