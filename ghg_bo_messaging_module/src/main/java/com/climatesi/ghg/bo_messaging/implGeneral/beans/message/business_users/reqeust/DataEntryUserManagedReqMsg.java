package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.DataEntryUserDTO;
import com.google.gson.annotations.SerializedName;

public class DataEntryUserManagedReqMsg implements Message {

    @SerializedName("DATA")
    private DataEntryUserDTO dto;

    public DataEntryUserDTO getDto() {
        return dto;
    }

    public void setDto(DataEntryUserDTO dto) {
        this.dto = dto;
    }
}
