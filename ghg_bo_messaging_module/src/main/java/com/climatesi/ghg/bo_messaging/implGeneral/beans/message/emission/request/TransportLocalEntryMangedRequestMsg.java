package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.TransportLocalEntryDTO;
import com.google.gson.annotations.SerializedName;

public class TransportLocalEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private TransportLocalEntryDTO dto;

    public TransportLocalEntryDTO getDto() {
        return dto;
    }

    public void setDto(TransportLocalEntryDTO dto) {
        this.dto = dto;
    }
}
