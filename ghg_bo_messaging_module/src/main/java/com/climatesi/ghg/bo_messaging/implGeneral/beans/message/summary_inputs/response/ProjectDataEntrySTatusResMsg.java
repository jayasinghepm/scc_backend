package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectDataEntryStatusDTO;

import java.util.List;

public class ProjectDataEntrySTatusResMsg extends EntityMasterResponseBean {

    private List<ProjectDataEntryStatusDTO> list;

    private int companyId;
    private int branchId;
    private String fy;

    public List<ProjectDataEntryStatusDTO> getList() {
        return list;
    }

    public void setList(List<ProjectDataEntryStatusDTO> list) {
        this.list = list;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getFy() {
        return fy;
    }

    public void setFy(String fy) {
        this.fy = fy;
    }
}
