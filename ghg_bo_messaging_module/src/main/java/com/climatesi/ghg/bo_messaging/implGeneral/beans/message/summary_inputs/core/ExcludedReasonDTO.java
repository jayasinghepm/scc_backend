package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

public class ExcludedReasonDTO {


    public ExcludedReasonDTO(String srcName, String reason) {
        this.srcName = srcName;
        this.reason = reason;
    }

    private String srcName;

    private String reason;

    public String getSrcName() {
        return srcName;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
