package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

import java.util.List;

public class EmissionSrcDataDTO {


    private String srcName;

    private int srcId;

    private List<EmissionFactorDTO> emissionFactors;


    private List<ActivityDataDTO> activityData;


    private String sourceOfData;

    private float emission;

    public List<EmissionFactorDTO> getEmissionFactors() {
        return emissionFactors;
    }

    public float getEmission() {
        return emission;
    }

    public void setEmission(float emission) {
        this.emission = emission;
    }

    public void setEmissionFactors(List<EmissionFactorDTO> emissionFactors) {
        this.emissionFactors = emissionFactors;
    }

    public String getSrcName() {
        return srcName;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }

    public int getSrcId() {
        return srcId;
    }

    public void setSrcId(int srcId) {
        this.srcId = srcId;
    }

    public List<ActivityDataDTO> getActivityData() {
        return activityData;
    }

    public void setActivityData(List<ActivityDataDTO> activityData) {
        this.activityData = activityData;
    }

    public String getSourceOfData() {
        return sourceOfData;
    }

    public void setSourceOfData(String sourceOfData) {
        this.sourceOfData = sourceOfData;
    }
}
