package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

public class SuggestionDTO {

    public SuggestionDTO(String title, String description) {
        this.title = title;
        this.description = description;
    }

    private String title;

    private String description;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
