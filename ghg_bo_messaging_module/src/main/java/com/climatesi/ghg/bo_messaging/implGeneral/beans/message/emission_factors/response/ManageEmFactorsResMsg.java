package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.EmFactorDTO;
import com.google.gson.annotations.SerializedName;

public class ManageEmFactorsResMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private EmFactorDTO dto;

    public EmFactorDTO getDto() {
        return dto;
    }

    public void setDto(EmFactorDTO dto) {
        this.dto = dto;
    }
}
