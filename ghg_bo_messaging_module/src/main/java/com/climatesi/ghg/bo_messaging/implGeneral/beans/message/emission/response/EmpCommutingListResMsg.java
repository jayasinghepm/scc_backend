package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.EmpCommutingDTO;

import java.util.List;

public class EmpCommutingListResMsg extends EntityListResponseBean {

    private List<EmpCommutingDTO> list;


    public List<EmpCommutingDTO> getList() {
        return list;
    }

    public void setList(List<EmpCommutingDTO> list) {
        this.list = list;
    }
}
