package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.AshTransportationDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.google.gson.annotations.SerializedName;

public class AshTransportationManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private AshTransportationDTO dto;

    public AshTransportationDTO getDto() {
        return dto;
    }

    public void setDto(AshTransportationDTO dto) {
        this.dto = dto;
    }
}
