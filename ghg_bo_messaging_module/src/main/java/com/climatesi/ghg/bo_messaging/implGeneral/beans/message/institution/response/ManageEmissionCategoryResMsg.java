package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.EmissionCategoryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.ManageMenuCompanyDTO;

public class ManageEmissionCategoryResMsg extends EntityMasterResponseBean {

    private EmissionCategoryDTO dto;

    public EmissionCategoryDTO getDto() {
        return dto;
    }

    public void setDto(EmissionCategoryDTO dto) {
        this.dto = dto;
    }
}