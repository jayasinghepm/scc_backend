package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteTransportEntryDTO;
import com.google.gson.annotations.SerializedName;

public class WasteTransportEntryMangedResponseMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private WasteTransportEntryDTO dto;

    public WasteTransportEntryDTO getDto() {
        return this.dto;
    }

    public void setDto(WasteTransportEntryDTO dto) {
        this.dto = dto;
    }

}
