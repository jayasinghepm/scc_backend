package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.CadminDTO;

public class CadminManagedResponseMsg extends EntityMasterResponseBean {

    private CadminDTO dto;

    public CadminDTO getDto() {
        return dto;
    }

    public void setDto(CadminDTO dto) {
        this.dto = dto;
    }
}
