package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BusinessAirTravelEntryDTO {

    @SerializedName("EMISSION_SRC_ID")
    private int emissionSrcId;

    @SerializedName("AIR_TRAVEL_ENTRY_ID")
    private int airTravelEntryId;

    @SerializedName("ADDED_BY")
    private int addedBy;

    @SerializedName("BRANCH_ID")
    private int branchId;

    @SerializedName("COMPANY_ID")
    private int companyId;

    @SerializedName("EMP_ID")
    private int empId;

    @SerializedName("TRAVEL_YEAR")
    private String travelYear;

    @SerializedName("TRAVEL_MONTH")
    private int travelMonth;

    @SerializedName("DEP_AIRPORT")
    private int depAirport;

    @SerializedName("DEP_COUNTRY")
    private int depCountry;

    @SerializedName("TRANSIST_1")
    private int transist1;

    @SerializedName("TRANSIST_2")
    private int transist2;

    @SerializedName("TRANSIST_3")
    private int transist3;

    @SerializedName("AIR_TRAVEL_WAY")
    private int airTravelway;

    @SerializedName("SEAT_CLASS")
    private int seatClass;

    @SerializedName("ADDED_DATE")
    private Date addedDate;

    @SerializedName("LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @SerializedName("DEST_AIRPORT")
    private int destAirport;

    @SerializedName("DEST_COUNTRY")
    private int destCountry;

    private int isDeleted;
    @SerializedName("NO_OF_EMPLOYEES")
    private int noOfEmps;
    @SerializedName("EMISSION_INFO")
    private JsonObject emissionDetails;

    private String company;

    private String branch;

    private String mon;

    private String depCoun;

    private String destCoun;

    private String desPort;
    private String depPort;

    private String trans1;

    private String trans2;

    private String trans3;

    private String seat;

    private String travelWay;

    private float distance;


    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getDepCoun() {
        return depCoun;
    }

    public void setDepCoun(String depCoun) {
        this.depCoun = depCoun;
    }

    public String getDestCoun() {
        return destCoun;
    }

    public void setDestCoun(String destCoun) {
        this.destCoun = destCoun;
    }

    public String getDesPort() {
        return desPort;
    }

    public void setDesPort(String desPort) {
        this.desPort = desPort;
    }

    public String getDepPort() {
        return depPort;
    }

    public void setDepPort(String depPort) {
        this.depPort = depPort;
    }

    public String getTrans1() {
        return trans1;
    }

    public void setTrans1(String trans1) {
        this.trans1 = trans1;
    }

    public String getTrans2() {
        return trans2;
    }

    public void setTrans2(String trans2) {
        this.trans2 = trans2;
    }

    public String getTrans3() {
        return trans3;
    }

    public void setTrans3(String trans3) {
        this.trans3 = trans3;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getTravelWay() {
        return travelWay;
    }

    public void setTravelWay(String travelWay) {
        this.travelWay = travelWay;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getDepAirport() {
        return depAirport;
    }

    public void setDepAirport(int depAirport) {
        this.depAirport = depAirport;
    }

    public int getDepCountry() {
        return depCountry;
    }

    public void setDepCountry(int depCountry) {
        this.depCountry = depCountry;
    }

    public int getTransist1() {
        return transist1;
    }

    public void setTransist1(int transist1) {
        this.transist1 = transist1;
    }

    public int getTransist2() {
        return transist2;
    }

    public void setTransist2(int transist2) {
        this.transist2 = transist2;
    }

    public int getTransist3() {
        return transist3;
    }

    public void setTransist3(int transist3) {
        this.transist3 = transist3;
    }

    public int getAirTravelway() {
        return airTravelway;
    }

    public void setAirTravelway(int airTravelway) {
        this.airTravelway = airTravelway;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getNoOfEmps() {
        return noOfEmps;
    }

    public void setNoOfEmps(int noOfEmps) {
        this.noOfEmps = noOfEmps;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }


    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }


    public int getAirTravelEntryId() {
        return this.airTravelEntryId;
    }


    public void setAirTravelEntryId(int id) {
        this.airTravelEntryId = id;
    }


    public int getAddedBy() {
        return this.addedBy;
    }


    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }


    public int getBranchId() {
        return this.branchId;
    }


    public void setBranchId(int id) {
        this.branchId = id;
    }


    public int getCompanyId() {
        return this.companyId;
    }


    public void setCompanyId(int id) {
        this.companyId = id;
    }


    public int getEmployeeId() {
        return this.empId;
    }


    public void setEmployeeId(int id) {
        this.empId = id;
    }


    public String getTravelYear() {
        return this.travelYear;
    }


    public void setTravelYear(String year) {
        this.travelYear = year;
    }


    public int getTravelMonth() {
        return this.travelMonth;
    }


    public void setTravelMonth(int month) {
        this.travelMonth = month;
    }


    public int getDepartureAirport() {
        return this.depAirport;
    }


    public void setDepartureAirport(int port) {
        this.depAirport = port;
    }


    public int geDepartureCountry() {
        return this.depCountry;
    }


    public void setDepartureCountry(int country) {
        this.depCountry = country;
    }


    public int getTransist1Port() {
        return transist1;
    }


    public void setTransist1Port(int port) {
        this.transist1 = port;
    }


    public int getTransist2Port() {
        return this.transist2;
    }


    public void setTransist2Port(int port) {
        this.transist2 = port;
    }


    public int getTransist3Port() {
        return this.transist3;
    }


    public void setTransist3Port(int port) {
        this.transist3 = port;
    }


    public int getAirTravelWay() {
        return this.airTravelway;
    }


    public void setAirTravelWay(int way) {
        this.airTravelway = way;
    }


    public int getSeatClass() {
        return this.seatClass;
    }


    public void setSeatClass(int seatClass) {
        this.seatClass = seatClass;
    }


    public Date getEntryAddedDate() {
        return this.addedDate;
    }


    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }


    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    public int getDestAirport() {
        return destAirport;
    }

    public void setDestAirport(int destAirport) {
        this.destAirport = destAirport;
    }


    public int getDestCountry() {
        return this.destCountry;
    }


    public void setDestCountry(int country) {
        this.destCountry = country;
    }
}
