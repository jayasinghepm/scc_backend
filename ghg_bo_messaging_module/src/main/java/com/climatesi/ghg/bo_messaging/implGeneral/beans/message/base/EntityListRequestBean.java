package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.google.gson.annotations.SerializedName;

public class EntityListRequestBean implements Message {

    @SerializedName("EXTENDED_FILTER_CRITERIA")
    private String filterCriteria = "";

    @SerializedName("FILTER_MODEL")
    private Object filterModel = "";

    @SerializedName("SORT_MODEL")
    private Object sortModel = "";

    @SerializedName("PAGE_NUMBER")
    private int pageNumber = 0;


    /**
     * Gets filter model.
     *
     * @return the filter model
     */
    public Object getFilterModel() {
        return filterModel;
    }

    /**
     * Sets filter model.
     *
     * @param filterModel the filter model
     */
    public void setFilterModel(Object filterModel) {
        this.filterModel = filterModel;
    }

    /**
     * Gets sort model.
     *
     * @return the sort model
     */
    public Object getSortModel() {
        return sortModel;
    }

    /**
     * Sets sort model.
     *
     * @param sortModel the sort model
     */
    public void setSortModel(Object sortModel) {
        this.sortModel = sortModel;
    }

    /**
     * Gets filter criteria.
     *
     * @return the filter criteria
     */
    public String getFilterCriteria() {
        return filterCriteria;
    }

    /**
     * Sets filter criteria.
     *
     * @param filterCriteria the filter criteria
     */
    public void setFilterCriteria(String filterCriteria) {
        this.filterCriteria = filterCriteria;
    }

    /**
     * Gets search letter.
     *
     * @return the search letter
     */
    public String getSearchLetter() {
        return filterCriteria;
    }

    /**
     * Sets search letter.
     *
     * @param filterCriteria the filter criteria
     */
    public void setSearchLetter(String filterCriteria) {
        this.filterCriteria = filterCriteria;
    }

    /**
     * Gets page number.
     *
     * @return the page number
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets page number.
     *
     * @param pageNumber the page number
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

}

