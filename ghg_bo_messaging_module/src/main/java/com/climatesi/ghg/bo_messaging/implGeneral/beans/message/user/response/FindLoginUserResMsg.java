package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

import java.util.Date;

public class FindLoginUserResMsg implements Message {

    private int userId;

    private int status;

    private String lastLoginDate;

    private String loginName;

    private int failedAttempts;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public int getFailedAttempts() {
        return failedAttempts;
    }

    public void setFailedAttempts(int failedAttempts) {
        this.failedAttempts = failedAttempts;
    }
}
