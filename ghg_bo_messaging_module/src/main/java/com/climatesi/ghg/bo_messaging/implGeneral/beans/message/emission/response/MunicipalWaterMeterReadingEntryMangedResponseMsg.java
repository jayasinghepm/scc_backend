package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterMeterReadingEntryDTO;
import com.google.gson.annotations.SerializedName;

public class MunicipalWaterMeterReadingEntryMangedResponseMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private MunicipalWaterMeterReadingEntryDTO dto;

    public MunicipalWaterMeterReadingEntryDTO getDto() {
        return this.dto;
    }

    public void setDto(MunicipalWaterMeterReadingEntryDTO dto) {
        this.dto = dto;
    }
}
