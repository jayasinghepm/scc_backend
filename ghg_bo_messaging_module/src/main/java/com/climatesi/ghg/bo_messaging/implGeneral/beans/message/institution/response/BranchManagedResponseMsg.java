package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.BranchDTO;

public class BranchManagedResponseMsg extends EntityMasterResponseBean {

    private BranchDTO dto;

    public BranchDTO getDto() {
        return dto;
    }

    public void setDto(BranchDTO dto) {
        this.dto = dto;
    }
}
