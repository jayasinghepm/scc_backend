package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.ClientDTO;

public class ClientManagedResponseMsg extends EntityMasterResponseBean {

    private ClientDTO dto;

    public ClientDTO getDto() {
        return dto;
    }

    public void setDto(ClientDTO dto) {
        this.dto = dto;
    }
}
