package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.core.LoginUserDTO;
import com.google.gson.annotations.SerializedName;

public class ManageLoginUserReqMsg implements Message {

    @SerializedName("DATA")
    private LoginUserDTO dto;

    public LoginUserDTO getDto() {
        return dto;
    }

    public void setDto(LoginUserDTO dto) {
        this.dto = dto;
    }


}
