package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.EmissionSourceDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmissionSourceListResponeMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<EmissionSourceDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

}
