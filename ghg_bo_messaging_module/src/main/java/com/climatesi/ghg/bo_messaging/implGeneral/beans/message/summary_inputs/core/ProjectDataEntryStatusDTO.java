package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

public class ProjectDataEntryStatusDTO {
    private int status;
    private int emissionSrc;
    private String fy;
    private int branchId;
    private int companyId;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getEmissionSrc() {
        return emissionSrc;
    }

    public void setEmissionSrc(int emissionSrc) {
        this.emissionSrc = emissionSrc;
    }

    public String getFy() {
        return fy;
    }

    public void setFy(String fy) {
        this.fy = fy;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }
}
