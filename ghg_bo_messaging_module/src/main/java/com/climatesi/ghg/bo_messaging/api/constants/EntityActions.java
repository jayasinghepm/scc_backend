package com.climatesi.ghg.bo_messaging.api.constants;

public enum EntityActions {
    Add(1),
    Update(2),
    Delete(3);


    private int action ;

    EntityActions(int action) {
        this.action = action;
    }

    public int getAction() {
        return action;
    }
}
