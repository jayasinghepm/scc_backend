package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.PubTransEmFactorDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListPubTransFactorResMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<PubTransEmFactorDTO> list;

    public List<PubTransEmFactorDTO> getList() {
        return list;
    }

    public void setList(List<PubTransEmFactorDTO> list) {
        this.list = list;
    }
}
