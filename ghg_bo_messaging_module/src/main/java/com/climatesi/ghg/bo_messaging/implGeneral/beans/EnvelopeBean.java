package com.climatesi.ghg.bo_messaging.implGeneral.beans;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.google.gson.annotations.SerializedName;

import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.HEADER;
import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.MESSAGE_BODY;

public class EnvelopeBean implements Envelope {

    @SerializedName(HEADER)
    private HeaderBean header;

    @SerializedName(MESSAGE_BODY)
    private Object body;

    /**
     * Instantiates a new Envelope bean.
     */
    public EnvelopeBean() {
    }

    /**
     * Instantiates a new Envelope bean.
     *
     * @param header the header
     * @param body   the body
     */
    public EnvelopeBean(HeaderBean header, Object body) {
        this.header = header;
        this.body = body;
    }


    public Header getHeader() {
        return (Header)this.header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean)header;
    }

    public Object getBody() {
        return this.body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
