package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.res;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.ReportMetaDataDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListReportMetaResMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<ReportMetaDataDTO> list;

    public List<ReportMetaDataDTO> getList() {
        return list;
    }

    public void setList(List<ReportMetaDataDTO> list) {
        this.list = list;
    }
}
