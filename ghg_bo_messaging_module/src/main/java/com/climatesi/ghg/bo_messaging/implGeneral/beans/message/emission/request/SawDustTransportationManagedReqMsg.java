package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SawDustTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class SawDustTransportationManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private SawDustTransportationDTO dto;

    public SawDustTransportationDTO getDto() {
        return dto;
    }

    public void setDto(SawDustTransportationDTO dto) {
        this.dto = dto;
    }
}