package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.core.ProjectDTO;
import com.google.gson.annotations.SerializedName;

public class ManageProjectResMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private ProjectDTO dto;

    public ProjectDTO getDto() {
        return dto;
    }

    public void setDto(ProjectDTO dto) {
        this.dto = dto;
    }
}
