package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.FireExtingEntryDTO;
import com.google.gson.annotations.SerializedName;

public class FireExtingEntryMangedResponseMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private FireExtingEntryDTO dto;

    public FireExtingEntryDTO getDto() {
        return dto;
    }

    public void setDto(FireExtingEntryDTO dto) {
        this.dto = dto;
    }
}
