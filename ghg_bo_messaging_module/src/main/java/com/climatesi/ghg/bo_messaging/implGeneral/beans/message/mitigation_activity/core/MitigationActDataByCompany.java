package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core;

import java.util.ArrayList;
import java.util.List;

public class MitigationActDataByCompany {

    private int comId;

    private int branchId;

    private double savedEmissions;

    private List<MitigationProjectActData> projectsData = new ArrayList<MitigationProjectActData>();

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public double getSavedEmissions() {
        return savedEmissions;
    }

    public void setSavedEmissions(double savedEmissions) {
        this.savedEmissions = savedEmissions;
    }

    public List<MitigationProjectActData> getProjectsData() {
        return projectsData;
    }

    public void setProjectsData(List<MitigationProjectActData> projectsData) {
        this.projectsData = projectsData;
    }
}
