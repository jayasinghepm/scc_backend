package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.google.gson.JsonObject;

public class CreateLoginProfileBranchResMsg implements Message {

    //-1 no branches found
    // 1 success
    // 2 error
    private int status;

    private JsonObject profiles;


    public JsonObject getProfiles() {
        return profiles;
    }

    public void setProfiles(JsonObject profiles) {
        this.profiles = profiles;
    }

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
