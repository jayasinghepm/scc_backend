package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.core.LoginUserDTO;


public class ManageLoginUserResMsg extends EntityMasterResponseBean {


    private LoginUserDTO dto;

    public LoginUserDTO getDto() {
        return dto;
    }

    public void setDto(LoginUserDTO dto) {
        this.dto = dto;
    }
}
