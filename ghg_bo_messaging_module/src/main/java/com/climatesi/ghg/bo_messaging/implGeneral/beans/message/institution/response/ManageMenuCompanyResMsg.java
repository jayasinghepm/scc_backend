package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.ManageMenuCompanyDTO;

public class ManageMenuCompanyResMsg extends EntityMasterResponseBean {

    private ManageMenuCompanyDTO dto;

    public ManageMenuCompanyDTO getDto() {
        return dto;
    }

    public void setDto(ManageMenuCompanyDTO dto) {
        this.dto = dto;
    }
}
