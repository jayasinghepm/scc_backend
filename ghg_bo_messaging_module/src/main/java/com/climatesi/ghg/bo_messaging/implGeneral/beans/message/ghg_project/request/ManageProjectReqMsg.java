package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.core.ProjectDTO;
import com.google.gson.annotations.SerializedName;

public class ManageProjectReqMsg implements Message {

    @SerializedName("DATA")
    private ProjectDTO dto;

    public ProjectDTO getDto() {
        return dto;
    }

    public void setDto(ProjectDTO dto) {
        this.dto = dto;
    }
}
