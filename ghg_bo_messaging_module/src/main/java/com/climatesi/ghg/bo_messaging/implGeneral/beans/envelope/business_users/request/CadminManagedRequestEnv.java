package com.climatesi.ghg.bo_messaging.implGeneral.beans.envelope.business_users.request;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust.CadminManagedRequestMsg;
import com.google.gson.annotations.SerializedName;

import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.HEADER;
import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.MESSAGE_BODY;

public class CadminManagedRequestEnv implements Envelope {
    @SerializedName(HEADER)
    private HeaderBean header;

    @SerializedName(MESSAGE_BODY)
    private CadminManagedRequestMsg body;

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean) header;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = (CadminManagedRequestMsg) body;
    }
}
