package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ExcludedReasonDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectFullSummaryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.SuggestionDTO;

import java.util.List;

public class UpdateCompanyExcludedReasonAndSuggestionReqMsg implements Message {

    private int companyId;

    private List<ExcludedReasonDTO> excludedReasons;

    private List<SuggestionDTO> suggestions;


    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public List<ExcludedReasonDTO> getExcludedReasons() {
        return excludedReasons;
    }

    public void setExcludedReasons(List<ExcludedReasonDTO> excludedReasons) {
        this.excludedReasons = excludedReasons;
    }

    public List<SuggestionDTO> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<SuggestionDTO> suggestions) {
        this.suggestions = suggestions;
    }
}
