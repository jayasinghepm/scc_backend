package com.climatesi.ghg.bo_messaging.implGeneral.beans.envelope.emission_sources.request;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request.GHGEmissionSummaryReqMsg;
import com.google.gson.annotations.SerializedName;

public class GHGEmissionSummaryReqEnv implements Envelope {

    @SerializedName(EnvelopeConstants.HEADER)
    private HeaderBean header;

    @SerializedName(EnvelopeConstants.MESSAGE_BODY)
    private GHGEmissionSummaryReqMsg body;


    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean) header;
    }

    public Object getBody() {
        return this.body;
    }

    public void setBody(Object body) {
        this.body = (GHGEmissionSummaryReqMsg) body;
    }
}
