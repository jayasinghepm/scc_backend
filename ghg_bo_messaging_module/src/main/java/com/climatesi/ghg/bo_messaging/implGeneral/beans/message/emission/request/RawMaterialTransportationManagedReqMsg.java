package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RawMaterialTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class RawMaterialTransportationManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private RawMaterialTransportationDTO dto;

    public RawMaterialTransportationDTO getDto() {
        return dto;
    }

    public void setDto(RawMaterialTransportationDTO dto) {
        this.dto = dto;
    }
}