package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.annotations.SerializedName;

public class EmissionSourceDTO {


    @SerializedName("EMISSION_SRC_ID")
    private int emissionSrcId;

    @SerializedName("EMISSION_NAME")
    private String emissionSrcName;

    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }


    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }


    public String getEmissionSrcName() {
        return this.emissionSrcName;
    }


    public void setEmissionSrcName(String srcName) {
        this.emissionSrcName = srcName;
    }
    
}
