package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.response;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectDTO;

import java.util.ArrayList;
import java.util.List;

public class GetMitigationResMsg extends EntityMasterResponseBean {

    private int comId;

    private List<MitigationProjectDTO> projects = new ArrayList<MitigationProjectDTO>();

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public List<MitigationProjectDTO> getProjects() {
        return projects;
    }

    public void setProjects(List<MitigationProjectDTO> projects) {
        this.projects = projects;
    }
}
