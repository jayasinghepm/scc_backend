package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.ClientDTO;
import com.google.gson.annotations.SerializedName;

public class ClientManagedRequestMsg implements Message {


    @SerializedName("DATA")
    private ClientDTO dto;

    public ClientDTO getDto() {
        return dto;
    }

    public void setDto(ClientDTO dto) {
        this.dto = dto;
    }
}
