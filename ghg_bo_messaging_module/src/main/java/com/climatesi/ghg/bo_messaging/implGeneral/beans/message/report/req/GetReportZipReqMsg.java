package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.req;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.ReportMetaDataDTO;
import com.google.gson.JsonObject;

public class GetReportZipReqMsg implements Message {

    private int projectId;

    private String indirect_v_direct;
    private String all_src ;
    private String pie_direct;
    private String pie_indirect;
    private String com_ghg_overyear;
    private String direct_over_year;
    private String indirect_over_year;
    private String capita;
    private String intensity;
    private String per_prod;
    public ReportMetaDataDTO  metadata;
    public JsonObject params;


    public JsonObject getParams() {
        return params;
    }

    public void setParams(JsonObject params) {
        this.params = params;
    }

    public ReportMetaDataDTO getMetadata() {
        return metadata;
    }

    public void setMetadata(ReportMetaDataDTO metadata) {
        this.metadata = metadata;
    }

    public String getIndirect_v_direct() {
        return indirect_v_direct;
    }

    public void setIndirect_v_direct(String indirect_v_direct) {
        this.indirect_v_direct = indirect_v_direct;
    }

    public String getAll_src() {
        return all_src;
    }

    public void setAll_src(String all_src) {
        this.all_src = all_src;
    }

    public String getPie_direct() {
        return pie_direct;
    }

    public void setPie_direct(String pie_direct) {
        this.pie_direct = pie_direct;
    }

    public String getPie_indirect() {
        return pie_indirect;
    }

    public void setPie_indirect(String pie_indirect) {
        this.pie_indirect = pie_indirect;
    }

    public String getCom_ghg_overyear() {
        return com_ghg_overyear;
    }

    public void setCom_ghg_overyear(String com_ghg_overyear) {
        this.com_ghg_overyear = com_ghg_overyear;
    }

    public String getDirect_over_year() {
        return direct_over_year;
    }

    public void setDirect_over_year(String direct_over_year) {
        this.direct_over_year = direct_over_year;
    }

    public String getIndirect_over_year() {
        return indirect_over_year;
    }

    public void setIndirect_over_year(String indirect_over_year) {
        this.indirect_over_year = indirect_over_year;
    }

    public String getCapita() {
        return capita;
    }

    public void setCapita(String capita) {
        this.capita = capita;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public String getPer_prod() {
        return per_prod;
    }

    public void setPer_prod(String per_prod) {
        this.per_prod = per_prod;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
