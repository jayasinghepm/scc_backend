package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.core.CountryDTO;

import java.util.List;

public class CountryListResponseMsg extends EntityListResponseBean {

    private List<CountryDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
