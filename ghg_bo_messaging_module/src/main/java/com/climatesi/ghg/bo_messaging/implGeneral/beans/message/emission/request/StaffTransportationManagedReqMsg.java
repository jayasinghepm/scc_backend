package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.StaffTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class StaffTransportationManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private StaffTransportationDTO dto;

    public StaffTransportationDTO getDto() {
        return dto;
    }

    public void setDto(StaffTransportationDTO dto) {
        this.dto = dto;
    }
}