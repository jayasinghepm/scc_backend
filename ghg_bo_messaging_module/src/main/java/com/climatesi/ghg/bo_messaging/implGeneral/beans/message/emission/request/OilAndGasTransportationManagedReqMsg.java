package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.OilAndGasDTO;
import com.google.gson.annotations.SerializedName;

public class OilAndGasTransportationManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private OilAndGasDTO dto;

    public OilAndGasDTO getDto() {
        return dto;
    }

    public void setDto(OilAndGasDTO dto) {
        this.dto = dto;
    }
}