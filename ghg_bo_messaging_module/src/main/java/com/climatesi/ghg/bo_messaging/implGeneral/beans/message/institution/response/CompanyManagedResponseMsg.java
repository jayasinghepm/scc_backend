package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;

public class CompanyManagedResponseMsg extends EntityMasterResponseBean {

    private CompanyDTO dto;

    public CompanyDTO getDto() {
        return dto;
    }

    public void setDto(CompanyDTO dto) {
        this.dto = dto;
    }
}
