package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.EmployeeDTO;

public class EmployeeManagedResponseMsg extends EntityMasterResponseBean {

    private EmployeeDTO dto;

    public EmployeeDTO getDto() {
        return dto;
    }

    public void setDto(EmployeeDTO dto) {
        this.dto = dto;
    }
}
