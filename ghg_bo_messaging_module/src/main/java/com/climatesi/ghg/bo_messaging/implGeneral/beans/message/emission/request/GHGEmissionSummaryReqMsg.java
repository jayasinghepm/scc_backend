package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

public class GHGEmissionSummaryReqMsg implements Message {

    int companyId;
    int branchId;
    int yearsBack;

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getYearsBack() {
        return yearsBack;
    }

    public void setYearsBack(int yearsBack) {
        this.yearsBack = yearsBack;
    }
}
