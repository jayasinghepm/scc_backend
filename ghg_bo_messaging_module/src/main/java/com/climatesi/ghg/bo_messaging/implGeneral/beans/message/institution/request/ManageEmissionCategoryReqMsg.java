package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.EmissionCategoryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.ManageMenuCompanyDTO;
import com.google.gson.annotations.SerializedName;

public class ManageEmissionCategoryReqMsg implements  Message {

    @SerializedName("DATA")
    private EmissionCategoryDTO dto;


    private int action;

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public EmissionCategoryDTO getDto() {
        return dto;
    }

    public void setDto(EmissionCategoryDTO reqDto) {
        this.dto = reqDto;
    }
}