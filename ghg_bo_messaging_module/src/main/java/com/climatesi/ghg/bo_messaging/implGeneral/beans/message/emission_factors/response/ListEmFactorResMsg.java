package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.EmFactorDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListEmFactorResMsg extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<EmFactorDTO> list;

    public List<EmFactorDTO> getList() {
        return list;
    }

    public void setList(List<EmFactorDTO> list) {
        this.list = list;
    }


}
