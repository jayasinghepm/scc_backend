package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.OilAndGasDTO;
import com.google.gson.annotations.SerializedName;

public class OilAndGasTransportationManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private OilAndGasDTO dto;

    public OilAndGasDTO getDto() {
        return dto;
    }

    public void setDto(OilAndGasDTO dto) {
        this.dto = dto;
    }
}
