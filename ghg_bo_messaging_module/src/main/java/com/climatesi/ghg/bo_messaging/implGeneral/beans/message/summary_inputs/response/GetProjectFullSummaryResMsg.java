package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectFullSummaryDTO;

public class GetProjectFullSummaryResMsg extends EntityMasterResponseBean {

    ProjectFullSummaryDTO summary;

    public ProjectFullSummaryDTO getSummary() {
        return summary;
    }

    public void setSummary(ProjectFullSummaryDTO summary) {
        this.summary = summary;
    }
}
