package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.core;

import java.util.Date;

public class UsrActLogDto {

    private long id;
    private String username; // fname + lname
    private int userId;
    private int companyId;
    private int branchId;
    private String actLog;
    private String logTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getActLog() {
        return actLog;
    }

    public void setActLog(String actLog) {
        this.actLog = actLog;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }
}
