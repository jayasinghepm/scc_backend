package com.climatesi.ghg.bo_messaging.api.constants;

public class GroupConstants {

    private GroupConstants() {

    }

    public static final int GROUP_AUTH = 1;
    public static final int GROUP_EMISSION = 2;
    public static final int GROUP_BUSINESS_USERS = 3;
    public static final int GROUP_INSTITUTION = 4;
    public static final int GROUP_LOGIN_USER = 5;

    public static final int GROUP_MASTER_DATA = 6;

    public static final int GROUP_EM_FACTORS = 7;


    public static final int GROUP_GHG_PROJECT = 8;
    public static final int GROUP_EM_CALC = 9;

    public static final int GROUP_SUMMARY_INPUTS = 10;

    public static final int GROUP_REPORT = 11;

    public static final int GROUP_MITIGATION = 12;

}
