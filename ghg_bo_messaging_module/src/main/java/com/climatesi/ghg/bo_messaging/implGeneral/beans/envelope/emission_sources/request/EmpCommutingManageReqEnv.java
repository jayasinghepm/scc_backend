package com.climatesi.ghg.bo_messaging.implGeneral.beans.envelope.emission_sources.request;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request.EmpCommutingManagedReqMsg;
import com.google.gson.annotations.SerializedName;

import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.HEADER;
import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.MESSAGE_BODY;

public class EmpCommutingManageReqEnv implements Envelope {
    @SerializedName(HEADER)
    private HeaderBean header;

    @SerializedName(MESSAGE_BODY)
    private EmpCommutingManagedReqMsg body;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean) header;
    }

    public EmpCommutingManagedReqMsg getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = (EmpCommutingManagedReqMsg) body;
    }
}
