package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterMeterReadingEntryDTO;
import com.google.gson.annotations.SerializedName;

public class MunicipalWaterMeterReadingEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private MunicipalWaterMeterReadingEntryDTO dto;

    public MunicipalWaterMeterReadingEntryDTO getDto() {
        return dto;
    }

    public void setDto(MunicipalWaterMeterReadingEntryDTO dto) {
        this.dto = dto;
    }
}
