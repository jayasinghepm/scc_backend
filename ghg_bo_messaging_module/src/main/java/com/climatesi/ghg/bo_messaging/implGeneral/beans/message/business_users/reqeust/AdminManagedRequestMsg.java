package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.AdminDTO;
import com.google.gson.annotations.SerializedName;

public class AdminManagedRequestMsg implements Message {

    @SerializedName("DATA")
    private AdminDTO dto;

    public AdminDTO getDto() {
        return dto;
    }

    public void setDto(AdminDTO dto) {
        this.dto = dto;
    }
}
