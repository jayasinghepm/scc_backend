package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.req;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.ReportMetaDataDTO;
import com.google.gson.annotations.SerializedName;

public class ManageReportMetaDataReqMsg implements Message {

    @SerializedName("DATA")
    private ReportMetaDataDTO dto;

    public ReportMetaDataDTO getDto() {
        return dto;
    }

    public void setDto(ReportMetaDataDTO dto) {
        this.dto = dto;
    }
}
