package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

public class SummaryInputsRequestMessage implements Message {

    private int branchId;
    private int companyId;

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }
}
