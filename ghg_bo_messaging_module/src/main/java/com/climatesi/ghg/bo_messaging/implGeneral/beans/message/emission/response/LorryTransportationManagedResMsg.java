package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LorryTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class LorryTransportationManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private LorryTransportationDTO dto;

    public LorryTransportationDTO getDto() {
        return dto;
    }

    public void setDto(LorryTransportationDTO dto) {
        this.dto = dto;
    }
}
