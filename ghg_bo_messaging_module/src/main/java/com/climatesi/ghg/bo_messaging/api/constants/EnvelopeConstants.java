package com.climatesi.ghg.bo_messaging.api.constants;

public class EnvelopeConstants {

    private EnvelopeConstants() {
        //Implicitly added for compliant with sonar rule 'Utility classes should not have public constructors'
    }

    /**
     * The constant HEADER.
     */
    public static final String HEADER = "HED";
    /**
     * The constant MESSAGE_BODY.
     */
    public static final String MESSAGE_BODY = "DAT";
}

