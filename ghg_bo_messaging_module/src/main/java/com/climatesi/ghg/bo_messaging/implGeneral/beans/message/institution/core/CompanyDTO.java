package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core;

import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;
import com.climatesi.ghg.implGeneral.EmissionCategoryBean;
import com.climatesi.ghg.utility.implGeneral.persistence.IntegerListConverter;
import com.google.gson.JsonObject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanyDTO {

    private int id;


    private String name;


    private String sector;

    private String code;


    private List<Integer> entitlements;


    private JsonObject yearEmission;


    private int noOfEmployees;

    private int noOfBranches;


    private float annumRevFY;


    private String baseYear;

    private String addr1;

    private String addr2;

    private String district;

    private String logo;

    private int fyMonthStart;

    private int fyMonthEnd;


    private int fyCurrentStart;


    private int fyCurrentEnd;
    private String fyCurrent;

    private int isDeleted;
    private int isFinancialYear;

    private float targetGHG;


    private List<Integer> pages;

    private List<Integer> emSources ;



    private Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources ;


    private List<EmissionCategoryBean> emissionCategories;


    public Map<Integer, AllowedEmissionSrcBean> getAllowedEmissionSources() {
        return allowedEmissionSources;
    }

    public void setAllowedEmissionSources(Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources) {
        this.allowedEmissionSources = allowedEmissionSources;
    }

    public List<EmissionCategoryBean> getEmissionCategories() {
        return emissionCategories;
    }

    public void setEmissionCategories(List<EmissionCategoryBean> emissionCategories) {
        this.emissionCategories = emissionCategories;
    }

    public List<Integer> getPages() {
        return pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }

    public List<Integer> getEmSources() {
        return emSources;
    }

    public void setEmSources(List<Integer> emSources) {
        this.emSources = emSources;
    }

    public float getTargetGHG() {
        return targetGHG;
    }

    public void setTargetGHG(float targetGHG) {
        this.targetGHG = targetGHG;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int isFinancialYear() {
        return isFinancialYear;
    }

    public void setFinancialYear(int financialYear) {
        isFinancialYear = financialYear;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public int getFyCurrentStart() {
        return fyCurrentStart;
    }

    public void setFyCurrentStart(int fyCurrentStart) {
        this.fyCurrentStart = fyCurrentStart;
    }

    public int getFyCurrentEnd() {
        return fyCurrentEnd;
    }

    public void setFyCurrentEnd(int fyCurrentEnd) {
        this.fyCurrentEnd = fyCurrentEnd;
    }

    public List<Integer> getEntitlements() {
        return entitlements;
    }

    public void setEntitlements(List<Integer> entitlements) {
        this.entitlements = entitlements;
    }

    public JsonObject getYearEmission() {
        return yearEmission;
    }

    public void setYearEmission(JsonObject yearEmission) {
        this.yearEmission = yearEmission;
    }

    public int getNoOfEmployees() {
        return noOfEmployees;
    }

    public void setNoOfEmployees(int noOfEmployees) {
        this.noOfEmployees = noOfEmployees;
    }

    public int getNoOfBranches() {
        return noOfBranches;
    }

    public void setNoOfBranches(int noOfBranches) {
        this.noOfBranches = noOfBranches;
    }

    public float getAnnumRevFY() {
        return annumRevFY;
    }

    public void setAnnumRevFY(float annumRevFY) {
        this.annumRevFY = annumRevFY;
    }

    public String getBaseYear() {
        return baseYear;
    }

    public void setBaseYear(String baseYear) {
        this.baseYear = baseYear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getFyMonthStart() {
        return fyMonthStart;
    }

    public void setFyMonthStart(int fyMonthStart) {
        this.fyMonthStart = fyMonthStart;
    }

    public int getFyMonthEnd() {
        return fyMonthEnd;
    }

    public void setFyMonthEnd(int fyMonthEnd) {
        this.fyMonthEnd = fyMonthEnd;
    }

    public String getFyCurrent() {
        return fyCurrent;
    }

    public void setFyCurrent(String fyCurrent) {
        this.fyCurrent = fyCurrent;
    }
}
