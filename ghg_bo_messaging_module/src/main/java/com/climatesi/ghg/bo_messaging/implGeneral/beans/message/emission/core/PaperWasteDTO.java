package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

public class PaperWasteDTO {

    private int entryId;


    private int emissionSrcId;

    private int addedBy;

    private int branchId;

    private int companyId;

    private Date addedDate;

    private Date lastUpdatedDate;

    private int month;

    private String year;

    private float quantity;//kg

    private JsonObject emissionDetails;

    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    //new----------------------------
//    @SerializedName("DISPOSAL")
//    private String disposal;
//
//    @SerializedName("DISPOSAL_METHOD")
//    private int disposalMethod;
//
//    public String getDisposal() {
//        return disposal;
//    }
//
//    public void setDisposal(String disposal) {
//        this.disposal = disposal;
//    }
//
//
//    public int getDisposalMethod() {
//        return this.disposalMethod;
//    }

//
//    public void setDisposalMethod(int disposalMethod) {
//        this.disposalMethod = disposalMethod;
//    }
//------------------------------
    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getEmissionSrcId() {
        return emissionSrcId;
    }

    public void setEmissionSrcId(int emissionSrcId) {
        this.emissionSrcId = emissionSrcId;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

}
