package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ForkliftsEntryDTO;
import com.google.gson.annotations.SerializedName;

public class ForkliftsManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private ForkliftsEntryDTO dto;

    public ForkliftsEntryDTO getDto() {
        return dto;
    }

    public void setDto(ForkliftsEntryDTO dto) {
        this.dto = dto;
    }
}
