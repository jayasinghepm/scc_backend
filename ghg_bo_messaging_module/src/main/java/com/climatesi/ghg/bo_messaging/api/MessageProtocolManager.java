package com.climatesi.ghg.bo_messaging.api;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.exceptions.InvalidVersionException;
import com.climatesi.ghg.bo_messaging.api.exceptions.MessageProtocolException;
import com.google.gson.JsonObject;

public interface MessageProtocolManager {

    /**
     * Parses the json message string and populate a generic envelope
     * The generic envelope contains a parsed header object and a generic body object
     *
     * @param jsonEnvelopeMessage full json message
     * @return generic envelope with parsed header information
     * @throws MessageProtocolException the message protocol exception
     */
    Envelope getEnvelope(String jsonEnvelopeMessage) throws MessageProtocolException;

    /**
     * Parses the json message string and populate a parsed envelope
     * The envelope contains the parsed header and a parsed body object
     * <p>
     * This will populate parsed body object by processing header information.
     * <p>
     * WARNING: This operation uses String operations to determine group/type.
     * Assumption/Limitation: Header must come first in serialized JSON string
     *
     * @param jsonEnvelopeMessage full json message
     * @return fully parsed envelope
     * @throws MessageProtocolException the message protocol exception
     */
    Envelope getEnvelopeSpecific(String jsonEnvelopeMessage) throws MessageProtocolException;

    /**
     * Parses the json message string and populate a parsed header object
     *
     * @param jsonEnvelopeMessage full json message
     * @return parsed header
     * @throws MessageProtocolException the message protocol exception
     */
    Header getHeaderFromEnvelopeMessage(String jsonEnvelopeMessage) throws MessageProtocolException;

    /**
     * Parses the json message string and populate a parsed message object
     * Performs a two-phase parse: first to determine message type, second to parse using specific type
     *
     * @param jsonEnvelopeMessage full json message
     * @return parsed message
     * @throws MessageProtocolException the message protocol exception
     */
    Message getMessageFromEnvelopeMessage(String jsonEnvelopeMessage) throws MessageProtocolException;

    /**
     * Parses the json message string and populate a parsed message object
     *
     * @param header              is used to identify message type
     * @param jsonEnvelopeMessage full json message
     * @return parsed message
     * @throws MessageProtocolException the message protocol exception
     * @throws InvalidVersionException  the invalid version exception
     */
    Message getMessageFromEnvelopeMessage(Header header, String jsonEnvelopeMessage) throws MessageProtocolException, InvalidVersionException;

    /**
     * Parses the json body string and populate a parsed message object
     *
     * @param <T>            subtype of message
     * @param jsonBodyString json string of message body
     * @param type           is the message type which is used to identify message
     * @return message from body string
     * @throws MessageProtocolException the message protocol exception
     */
    <T extends Message> T getMessageFromBodyString(String jsonBodyString, Class<T> type) throws MessageProtocolException;

    /**
     * Serializes and returns json strings from given message envelope
     *
     * @param envelopeMessage is envelope object with message details
     * @return json string of message
     */
    String getJSonString(Envelope envelopeMessage);

    /**
     * Serializes and returns json string from given header and message
     *
     * @param header is a parsed header object
     * @param data   is a parsed message body object
     * @return json string of message
     */
    String getJSonString(Header header, Message data);

    /**
     * Get the JSON object of given Bean
     *
     * @param messageData is message bean
     * @return j son string
     */
    String getJSonString(Object messageData);

    /**
     * Gets json object from request.
     *
     * @param header         the header
     * @param requestMessage the request message
     * @return the json object from request
     */
    JsonObject getJsonObjectFromRequest(Header header, String requestMessage) throws MessageProtocolException, InvalidVersionException;

}

