package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GeneratorsEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LPGEntryDTO;
import com.google.gson.annotations.SerializedName;

public class LPGEntryManagedResMsg  extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private LPGEntryDTO dto;

    public LPGEntryDTO getDto() {
        return dto;
    }

    public void setDto(LPGEntryDTO dto) {
        this.dto = dto;
    }
}
