package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.PubTransEmFactorDTO;
import com.google.gson.annotations.SerializedName;

public class ManagePubTransFactorsResMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private PubTransEmFactorDTO dto;

    public PubTransEmFactorDTO getDto() {
        return dto;
    }

    public void setDto(PubTransEmFactorDTO dto) {
        this.dto = dto;
    }
}
