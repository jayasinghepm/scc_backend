package com.climatesi.ghg.bo_messaging.api.exceptions;

public class InvalidVersionException extends Exception {

    /**
     * Instantiates a new Invalid version exception.
     *
     * @param message the message
     */
    public InvalidVersionException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Invalid version exception.
     *
     * @param message the message
     * @param ex      the ex
     */
    public InvalidVersionException(String message, Exception ex) {
        super(message, ex);
    }
}
