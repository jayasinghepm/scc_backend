package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaidManagerVehicleDTO;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaidManagerVehicleEntryListResMsg  extends EntityListResponseBean {

    @SerializedName("LIST")
    private List<PaidManagerVehicleDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
