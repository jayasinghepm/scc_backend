package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;

public class CreateLoginProfileBranchReqMsg implements Message {

    private int comId;

    private int resetIfexist;


    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public int getResetIfexist() {
        return resetIfexist;
    }

    public void setResetIfexist(int resetIfexist) {
        this.resetIfexist = resetIfexist;
    }
}
