package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LorryTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class LorryTransportationManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private LorryTransportationDTO dto;

    public LorryTransportationDTO getDto() {
        return dto;
    }

    public void setDto(LorryTransportationDTO dto) {
        this.dto = dto;
    }
}