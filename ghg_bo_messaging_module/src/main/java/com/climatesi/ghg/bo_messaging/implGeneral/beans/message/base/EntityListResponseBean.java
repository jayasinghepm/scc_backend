package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntityListResponseBean implements Message {

    @SerializedName("TOTAL_COUNT")
    private int totalRecordCount;

    @Expose(serialize = false, deserialize = false)
    private transient Integer actionStatus;

    @Expose(serialize = false, deserialize = false)
    private String narration;

    /**
     * Gets total record count.
     *
     * @return the total record count
     */
    public int getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * Sets total record count.
     *
     * @param totalRecordCount the total record count
     */
    public void setTotalRecordCount(int totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    /**
     * Gets action status.
     *
     * @return the action status
     */
    public Integer getActionStatus() {
        return actionStatus;
    }

    /**
     * Sets action status.
     *
     * @param actionStatus the action status
     */
    public void setActionStatus(Integer actionStatus) {
        this.actionStatus = actionStatus;
    }

    /**
     * Gets narration.
     *
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * Sets narration.
     *
     * @param narration the narration
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }
}