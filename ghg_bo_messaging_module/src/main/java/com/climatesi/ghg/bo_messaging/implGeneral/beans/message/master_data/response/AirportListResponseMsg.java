package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityListResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.core.AirportDTO;

import java.util.List;

public class AirportListResponseMsg extends EntityListResponseBean {

    private List<AirportDTO> list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
