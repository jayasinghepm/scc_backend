package com.climatesi.ghg.bo_messaging.implGeneral.beans.envelope.summary_inputs.req;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.SummaryInputsRequestMessage;
import com.google.gson.annotations.SerializedName;

import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.HEADER;
import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.MESSAGE_BODY;

public class CommonSummaryInputReqEnv  implements Envelope {

    @SerializedName(HEADER )
    private HeaderBean header;

    @SerializedName(MESSAGE_BODY)
    private SummaryInputsRequestMessage body;


    public HeaderBean getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean)header;
    }

    public SummaryInputsRequestMessage getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = (SummaryInputsRequestMessage)body;
    }


}
