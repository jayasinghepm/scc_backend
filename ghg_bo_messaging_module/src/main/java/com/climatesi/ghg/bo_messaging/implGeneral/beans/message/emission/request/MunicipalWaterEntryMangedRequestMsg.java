package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterEntryDTO;
import com.google.gson.annotations.SerializedName;

public class MunicipalWaterEntryMangedRequestMsg implements Message {

    @SerializedName("DATA")
    private MunicipalWaterEntryDTO dto;

    public MunicipalWaterEntryDTO getDto() {
        return dto;
    }

    public void setDto(MunicipalWaterEntryDTO dto) {
        this.dto = dto;
    }
}
