package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RefrigerantsEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SawDustTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class SawDustTransportationManagedResMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private SawDustTransportationDTO dto;

    public SawDustTransportationDTO getDto() {
        return this.dto;
    }

    public void setDto(SawDustTransportationDTO dto) {
        this.dto = dto;
    }


}
