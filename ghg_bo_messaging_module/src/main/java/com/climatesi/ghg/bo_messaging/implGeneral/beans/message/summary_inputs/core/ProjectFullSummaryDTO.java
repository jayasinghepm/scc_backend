package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core;

import java.util.List;

public class ProjectFullSummaryDTO {


    private int companyId;

    private int projectId;

    private String companyName;

    private String methodology;

    private String protocol;

    private float totalEmission;

    private float totalDirectEmission;

    private float totalIndirectEmission;

    private List<ExcludedReasonDTO> excludedReasons;

    private List<SuggestionDTO> suggestions;



    private List<EmissionSrcDataDTO> directSources;

    private List<EmissionSrcDataDTO> indirectSources;

    private List<EmissionSrcDataDTO> unCategorized;

    private boolean isFy;

    private String fy;

    public boolean isFy() {
        return isFy;
    }

    public void setFy(boolean fy) {
        isFy = fy;
    }

    public String getFy() {
        return fy;
    }

    public void setFy(String fy) {
        this.fy = fy;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMethodology() {
        return methodology;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public float getTotalEmission() {
        return totalEmission;
    }

    public void setTotalEmission(float totalEmission) {
        this.totalEmission = totalEmission;
    }

    public float getTotalDirectEmission() {
        return totalDirectEmission;
    }

    public void setTotalDirectEmission(float totalDirectEmission) {
        this.totalDirectEmission = totalDirectEmission;
    }

    public float getTotalIndirectEmission() {
        return totalIndirectEmission;
    }

    public void setTotalIndirectEmission(float totalIndirectEmission) {
        this.totalIndirectEmission = totalIndirectEmission;
    }

    public List<ExcludedReasonDTO> getExcludedReasons() {
        return excludedReasons;
    }

    public void setExcludedReasons(List<ExcludedReasonDTO> excludedReasons) {
        this.excludedReasons = excludedReasons;
    }

    public List<SuggestionDTO> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<SuggestionDTO> suggestions) {
        this.suggestions = suggestions;
    }

    public List<EmissionSrcDataDTO> getDirectSources() {
        return directSources;
    }

    public void setDirectSources(List<EmissionSrcDataDTO> directSources) {
        this.directSources = directSources;
    }

    public List<EmissionSrcDataDTO> getIndirectSources() {
        return indirectSources;
    }

    public void setIndirectSources(List<EmissionSrcDataDTO> indirectSources) {
        this.indirectSources = indirectSources;
    }

    public List<EmissionSrcDataDTO> getUnCategorized() {
        return unCategorized;
    }

    public void setUnCategorized(List<EmissionSrcDataDTO> unCategorized) {
        this.unCategorized = unCategorized;
    }
}
