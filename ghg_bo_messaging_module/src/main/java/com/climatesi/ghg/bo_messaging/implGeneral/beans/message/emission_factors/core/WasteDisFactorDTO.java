package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core;

public class WasteDisFactorDTO {

    private int wasteType;


    private String wasteName;


    private String units;


    private float reuse;

    private float open_loop;

    private float closed_loop;

    private float combustion;

    private float composting;

    private float landfill;

    private float anaerobic_digestion;

    public int getWasteType() {
        return wasteType;
    }

    public void setWasteType(int wasteType) {
        this.wasteType = wasteType;
    }

    public String getWasteName() {
        return wasteName;
    }

    public void setWasteName(String wasteName) {
        this.wasteName = wasteName;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public float getReuse() {
        return reuse;
    }

    public void setReuse(float reuse) {
        this.reuse = reuse;
    }

    public float getOpen_loop() {
        return open_loop;
    }

    public void setOpen_loop(float open_loop) {
        this.open_loop = open_loop;
    }

    public float getClosed_loop() {
        return closed_loop;
    }

    public void setClosed_loop(float closed_loop) {
        this.closed_loop = closed_loop;
    }

    public float getCombustion() {
        return combustion;
    }

    public void setCombustion(float combustion) {
        this.combustion = combustion;
    }

    public float getComposting() {
        return composting;
    }

    public void setComposting(float composting) {
        this.composting = composting;
    }

    public float getLandfill() {
        return landfill;
    }

    public void setLandfill(float landfill) {
        this.landfill = landfill;
    }

    public float getAnaerobic_digestion() {
        return anaerobic_digestion;
    }

    public void setAnaerobic_digestion(float anaerobic_digestion) {
        this.anaerobic_digestion = anaerobic_digestion;
    }

}
