package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request;


import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectDataEntryStatusDTO;
import com.google.gson.annotations.SerializedName;

public class ManageProjectDataEntryStatusReqMsg implements Message {

    @SerializedName("DATA")
    private ProjectDataEntryStatusDTO dto;

    public ProjectDataEntryStatusDTO getDto() {
        return dto;
    }

    public void setDto(ProjectDataEntryStatusDTO dto) {
        this.dto = dto;
    }
}
