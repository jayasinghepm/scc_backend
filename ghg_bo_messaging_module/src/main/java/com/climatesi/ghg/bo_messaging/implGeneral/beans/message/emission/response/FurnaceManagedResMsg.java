package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.FurnaceOilDTO;
import com.google.gson.annotations.SerializedName;

public class FurnaceManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private FurnaceOilDTO dto;

    public  FurnaceOilDTO getDto() {
        return dto;
    }

    public void setDto( FurnaceOilDTO dto) {
        this.dto = dto;
    }
}
