package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LPGEntryDTO;
import com.google.gson.annotations.SerializedName;

public class LPGEntryManagedReqMsg implements Message {

    @SerializedName("DATA")
    private LPGEntryDTO dto;

    public LPGEntryDTO getDto() {
        return dto;
    }

    public void setDto(LPGEntryDTO dto) {
        this.dto = dto;
    }
}
