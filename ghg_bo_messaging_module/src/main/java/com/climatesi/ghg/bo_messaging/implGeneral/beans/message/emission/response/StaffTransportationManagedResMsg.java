package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.StaffTransportationDTO;
import com.google.gson.annotations.SerializedName;

public class StaffTransportationManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private StaffTransportationDTO dto;

    public StaffTransportationDTO getDto() {
        return dto;
    }

    public void setDto(StaffTransportationDTO dto) {
        this.dto = dto;
    }
}
