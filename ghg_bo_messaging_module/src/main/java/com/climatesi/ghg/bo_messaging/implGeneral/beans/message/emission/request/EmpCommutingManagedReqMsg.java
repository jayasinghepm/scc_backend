package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.EmpCommutingDTO;
import com.google.gson.annotations.SerializedName;

public class EmpCommutingManagedReqMsg implements Message {
    @SerializedName("DATA")
    private EmpCommutingDTO dto;

    public EmpCommutingDTO getDto() {
        return dto;
    }

    public void setDto(EmpCommutingDTO dto) {
        this.dto = dto;
    }
}
