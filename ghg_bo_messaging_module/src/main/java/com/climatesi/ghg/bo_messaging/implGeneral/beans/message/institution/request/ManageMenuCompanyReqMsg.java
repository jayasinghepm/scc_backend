package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.ManageMenuCompanyDTO;
import com.google.gson.annotations.SerializedName;

public class ManageMenuCompanyReqMsg  implements Message {

    @SerializedName("DATA")
    private ManageMenuCompanyDTO dto;

    public ManageMenuCompanyDTO getDto() {
        return dto;
    }

    public void setDto(ManageMenuCompanyDTO reqDto) {
        this.dto = reqDto;
    }
}
