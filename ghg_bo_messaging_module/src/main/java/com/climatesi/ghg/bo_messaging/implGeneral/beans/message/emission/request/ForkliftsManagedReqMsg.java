package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ForkliftsEntryDTO;
import com.google.gson.annotations.SerializedName;

public class ForkliftsManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private ForkliftsEntryDTO dto;

    public ForkliftsEntryDTO getDto() {
        return dto;
    }

    public void setDto(ForkliftsEntryDTO dto) {
        this.dto = dto;
    }
}