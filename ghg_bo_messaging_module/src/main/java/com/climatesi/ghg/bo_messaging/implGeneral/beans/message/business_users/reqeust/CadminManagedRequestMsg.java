package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.CadminDTO;
import com.google.gson.annotations.SerializedName;

public class CadminManagedRequestMsg implements Message {

    @SerializedName("DATA")
    private CadminDTO dto;

    public CadminDTO getDto() {
        return dto;
    }

    public void setDto(CadminDTO dto) {
        this.dto = dto;
    }
}
