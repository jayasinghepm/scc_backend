package com.climatesi.ghg.bo_messaging.implGeneral.beans.envelope.emission_sources.request;

import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request.BioMassEntryListReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request.SludgeTransportationEntryListReqMsg;
import com.google.gson.annotations.SerializedName;

import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.HEADER;
import static com.climatesi.ghg.bo_messaging.api.constants.EnvelopeConstants.MESSAGE_BODY;

public class SludgeTransportationEntryListReqEnv implements Envelope {

    @SerializedName(HEADER)
    private HeaderBean header;

    @SerializedName(MESSAGE_BODY)
    private SludgeTransportationEntryListReqMsg body;


    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = (HeaderBean) header;
    }

    public Object getBody() {
        return this.body;
    }

    public void setBody(Object body) {
        this.body = (SludgeTransportationEntryListReqMsg) body;
    }
}