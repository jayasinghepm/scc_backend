package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BGASEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LPGEntryDTO;
import com.google.gson.annotations.SerializedName;

public class BGASEntryManagedReqMsg implements Message {

    @SerializedName("DATA")
    private BGASEntryDTO dto;

    public BGASEntryDTO getDto() {
        return dto;
    }

    public void setDto(BGASEntryDTO dto) {
        this.dto = dto;
    }
}
