package com.climatesi.ghg.bo_messaging.implGeneral;

import com.climatesi.ghg.bo_messaging.api.MessageProtocolManager;
import com.climatesi.ghg.bo_messaging.api.beans.Envelope;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.exceptions.InvalidVersionException;
import com.climatesi.ghg.bo_messaging.api.exceptions.MessageProtocolException;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.EnvelopeBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.HeaderBean;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

public class MessageProtocolFacade implements MessageProtocolManager {
    private Gson jsonParser;
    private Map<String, String> requestMap;
    private JsonParser jsonObjectParser;

    /**
     * Instantiates a new Message protocol impl.facades.
     */
    public MessageProtocolFacade() {
        jsonParser = new Gson();
        requestMap = getRequestMappingTable();
        jsonObjectParser = new JsonParser();
    }

    private Map<String, String> getRequestMappingTable() {

        Map<String, String> mappingPaths = new HashMap<String, String>();

//        Authentication
        mappingPaths.put("1-1", "authentication.request.AuthenticateRequestEnvelopeBean");
        mappingPaths.put("1-2", "authentication.request.AuthenticateRequestEnvelopeBean");

//        Emission
        mappingPaths.put("2-11", "emission_sources.request.BusinessAirTravelEntryListRequestEnv");
        mappingPaths.put("2-1", "emission_sources.request.BusinessAirTravelEntryManagedRequestEnv");
        mappingPaths.put("2-12", "emission_sources.request.ElectricityEntryListRequestEnv");
        mappingPaths.put("2-2", "emission_sources.request.ElectricityEntryManagedRequestEnv");
        mappingPaths.put("2-13", "emission_sources.request.FireExtingEntryListRequetEnv");
        mappingPaths.put("2-3", "emission_sources.request.FireExtingEntryMangedRequestEnv");
        mappingPaths.put("2-14", "emission_sources.request.GeneratorsEntryListRequestEnv");
        mappingPaths.put("2-4", "emission_sources.request.GeneratorsEntryMangedRequestEnv");
        mappingPaths.put("2-15", "emission_sources.request.MunicipalWaterEntryListRequestEnv");
        mappingPaths.put("2-5", "emission_sources.request.MunicipalWaterEntryManagedRequsetEnv");
        mappingPaths.put("2-16", "emission_sources.request.RefrigerantsEntryListRequestEnv");
        mappingPaths.put("2-6", "emission_sources.request.RefrigerantsEntryMangedRequestEnv");
        mappingPaths.put("2-17", "emission_sources.request.TransportLocalEntryListRequestEnv");
        mappingPaths.put("2-7", "emission_sources.request.TransportLocalEntryMangedRequestEnv");
        mappingPaths.put("2-18", "emission_sources.request.VehicleEntryListRequestEnv");
        mappingPaths.put("2-8", "emission_sources.request.VehicleEntryManagedRequestEnv");
        mappingPaths.put("2-19", "emission_sources.request.WasteDisposalEntryListRequestEnv");
        mappingPaths.put("2-9", "emission_sources.request.WasteDisposalEntryManagedRequestEnv");
        mappingPaths.put("2-20", "emission_sources.request.WasteTransportEntryListRequstEnv");
        mappingPaths.put("2-10", "emission_sources.request.WasteTransportEntryManagedRequestEnv");
        mappingPaths.put("2-21", "emission_sources.request.EmpCommutingListReqEnv");
        mappingPaths.put("2-22", "emission_sources.request.EmpCommutingManageReqEnv");
        mappingPaths.put("2-23", "emission_sources.request.GHGEmissionSummaryReqEnv");
        mappingPaths.put("2-24", "emission_sources.request.GHGEmissionSummaryReqEnv");
        mappingPaths.put("2-25", "emission_sources.request.GHGEmissionSummaryExcludedReqEnv");
        mappingPaths.put("2-26", "emission_sources.request.ManageGHGEmissionSummaryReqEnv");
        mappingPaths.put("2-27", "emission_sources.request.ExportGHGEmissionSummaryReqEnv");
        mappingPaths.put("2-28", "emission_sources.request.SeaAirFreightEntryListReqEnv");
        mappingPaths.put("2-29", "emission_sources.request.SeaAirFreightEntryManagedReqEnv");
        mappingPaths.put("2-30", "emission_sources.request.BioMassEntryListReqEnv");
        mappingPaths.put("2-31", "emission_sources.request.BioMassEntryManagedReqEnv");
        mappingPaths.put("2-32", "emission_sources.request.LPGEntryListReqEnv");
        mappingPaths.put("2-33", "emission_sources.request.LPGEntryManagedReqEnv");
        mappingPaths.put("2-34", "emission_sources.request.AshTransportationManagedReqEnv");
        mappingPaths.put("2-35", "emission_sources.request.AshTransportationEntryListReqEnv");
        mappingPaths.put("2-36", "emission_sources.request.ForkliftsManagedReqEnv");
        mappingPaths.put("2-37", "emission_sources.request.ForkliftsEntryListReqEnv");
        mappingPaths.put("2-38", "emission_sources.request.FurnaceOilManagedReqEnv");
        mappingPaths.put("2-39", "emission_sources.request.FurnaceOilEntryListReqEnv");
        mappingPaths.put("2-40", "emission_sources.request.LorryTransportationManagedReqEnv");
        mappingPaths.put("2-41", "emission_sources.request.LorryTransportationEntryListReqEnv");
        mappingPaths.put("2-42", "emission_sources.request.OilAndGasManagedReqEnv");
        mappingPaths.put("2-43", "emission_sources.request.OilAndGasEntryListReqEnv");
        mappingPaths.put("2-44", "emission_sources.request.PaidManagerVehicleManagedReqEnv");
        mappingPaths.put("2-45", "emission_sources.request.PaidManagerVehicleEntryListReqEnv");
        mappingPaths.put("2-46", "emission_sources.request.PaperWasteManagedReqEnv");
        mappingPaths.put("2-47", "emission_sources.request.PaperWasteEntryListReqEnv");
        mappingPaths.put("2-48", "emission_sources.request.RawMaterialTransportationManagedReqEnv");
        mappingPaths.put("2-49", "emission_sources.request.RawMaterialTransportationEntryListReqEnv");
        mappingPaths.put("2-50", "emission_sources.request.SawDustTransportationManagedReqEnv");
        mappingPaths.put("2-51", "emission_sources.request.SawDustTransportationEntryListReqEnv");
        mappingPaths.put("2-52", "emission_sources.request.SludgeTransportationManagedReqEnv");
        mappingPaths.put("2-53", "emission_sources.request.SludgeTransportationEntryListReqEnv");
        mappingPaths.put("2-54", "emission_sources.request.StaffTransportationManagedReqEnv");
        mappingPaths.put("2-55", "emission_sources.request.StaffTransportationEntryListReqEnv");
        mappingPaths.put("2-56", "emission_sources.request.VehicleOthersManagedReqEnv");
        mappingPaths.put("2-57", "emission_sources.request.VehicleOthersEntryListReqEnv");
        mappingPaths.put("2-58", "emission_sources.request.BGASEntryListReqEnv");
        mappingPaths.put("2-59", "emission_sources.request.BGASEntryManagedReqEnv");



//        business users
        mappingPaths.put("3-1", "business_users.request.EmployeeManagedRequestEnv");
        mappingPaths.put("3-2", "business_users.request.AdminManagedRequestEnv");
        mappingPaths.put("3-3", "business_users.request.ClientManagedRequestEnv");
        mappingPaths.put("3-4", "business_users.request.CadminManagedRequestEnv");
        mappingPaths.put("3-5", "business_users.request.EmployeeListRequestEnv");
        mappingPaths.put("3-6", "business_users.request.AdminListRequestEnv");
        mappingPaths.put("3-7", "business_users.request.ClientListRequestEnv");
        mappingPaths.put("3-8", "business_users.request.CadminListRequestEnv");
        mappingPaths.put("3-9", "business_users.request.DataEntryUserManagedReqEnv");
        mappingPaths.put("3-10", "business_users.request.DataEntryUserListReqEnv");


//        institution
        mappingPaths.put("4-1", "institution.request.BranchManagedRequestEnv");
        mappingPaths.put("4-2", "institution.request.CompanyManagedRequestEnv");
        mappingPaths.put("4-3", "institution.request.BranchListRequestEnv");
        mappingPaths.put("4-4", "institution.request.CompanyListRequestEnv");
        mappingPaths.put("4-5", "institution.request.CompanyListRequestEnv");
        mappingPaths.put("4-6", "institution.request.CompanyListRequestEnv");
        mappingPaths.put("4-7", "institution.request.ManageMenuCompanyReqEnv");
        mappingPaths.put("4-8", "institution.request.ManageEmissionCategoryReqEnv");
        mappingPaths.put("4-9", "institution.request.UpdateCompanyExcludedReasonAndSuggestionReqEnv");

//        login user
        mappingPaths.put("5-1", "users.request.ManageLoginUserReqEnv");
        mappingPaths.put("5-2", "users.request.FindLoginUserReqEnv");
        mappingPaths.put("5-3", "users.request.ListUsrActLogsReqEnv");
        mappingPaths.put("5-4", "users.request.CreateLoginProfileBranchReqEnv");

//        Master data
        mappingPaths.put("6-1", "master_data.request.CountyListReqEnv");
        mappingPaths.put("6-2", "master_data.request.AirportLisReqEnv");

//        eMISSION FACTORS
        mappingPaths.put("7-1", "emission_factors.request.ManageEmFactorReqEnv");
        mappingPaths.put("7-2", "emission_factors.request.ManagePubTransFacReqEnv");
        mappingPaths.put("7-3", "emission_factors.request.ManageWasteDisFacReqEnv");
        mappingPaths.put("7-4", "emission_factors.request.ListEmFactorsReqEnv");
        mappingPaths.put("7-5", "emission_factors.request.ListPubTransFacReqEnv");
        mappingPaths.put("7-6", "emission_factors.request.ListWasteDisFacReqEnv");


//        ghg project
        mappingPaths.put("8-1", "ghg_project.request.ManageProjectReqEnv");
        mappingPaths.put("8-2", "ghg_project.request.ListProjectReqEnv");

//        summary inputs
        mappingPaths.put("10-1","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-2","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-3","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-4","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-5","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-6","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-7","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-8","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-9","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-10","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-11","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-12","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-13","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-14","summary_inputs.req.ProjectDataEntryStatusReqEnv");
        mappingPaths.put("10-15","summary_inputs.req.ManageProjectDataEntryStatusReqEnv");
        mappingPaths.put("10-16","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-17","summary_inputs.req.CommonSummaryInputReqEnv");
        mappingPaths.put("10-18","summary_inputs.req.CommonSummaryInputReqEnv");


        mappingPaths.put("10-19","summary_inputs.req.GetProjectFullSummaryReqEnv");

//        Report
        mappingPaths.put("11-1", "reports.req.ManageGHGReportReqEnv");
        mappingPaths.put("11-2", "reports.req.ListReportReqEnv");
        mappingPaths.put("11-3", "reports.req.ManageReportMetaDataReqEnv");
        mappingPaths.put("11-4", "reports.req.ListReportMetaReqEnv");
        mappingPaths.put("11-5", "reports.req.GetReportZipReqEnv");


        //mitigation
        mappingPaths.put("12-1", "mitigation_activity.request.ManageMitigationProjectReqEnv");
        mappingPaths.put("12-2", "mitigation_activity.request.GetMitigationProjectsReqEnv");
        mappingPaths.put("12-3", "mitigation_activity.request.ManageMitigationProjectMonthlyActivityReqEnv");
        mappingPaths.put("12-4", "mitigation_activity.request.GetMitigationActDataReqEnv");

        return mappingPaths;

    }


    public Envelope getEnvelope(String jsonEnvelopeMessage) throws MessageProtocolException {
        return jsonParser.fromJson(jsonEnvelopeMessage, EnvelopeBean.class);
    }


    public Envelope getEnvelopeSpecific(String jsonEnvelopeMessage) throws MessageProtocolException {
        return null;
    }


    public Header getHeaderFromEnvelopeMessage(String jsonEnvelopeMessage) throws MessageProtocolException {
        try {
            EnvelopeBean envelopeBean = jsonParser.fromJson(jsonEnvelopeMessage, EnvelopeBean.class);
            return envelopeBean.getHeader();
        } catch (Exception ex) {
            throw new MessageProtocolException("Error parsing message", ex);
        }
    }


    public Message getMessageFromEnvelopeMessage(String jsonEnvelopeMessage) throws MessageProtocolException {
        try {
            //Get header from full envelop json message
            EnvelopeBean envelopeBean = jsonParser.fromJson(jsonEnvelopeMessage, EnvelopeBean.class);
            Header header = envelopeBean.getHeader();

            //Generate message using overloading method
            return getMessageFromEnvelopeMessage(header, jsonEnvelopeMessage);
        } catch (MessageProtocolException e) {
            throw e;
        } catch (Exception ex) {
            throw new MessageProtocolException("Error parsing message", ex);
        }
    }


    public Message getMessageFromEnvelopeMessage(Header header, String jsonEnvelopeMessage) throws MessageProtocolException, InvalidVersionException {
        String key = header.getMessageGroup() + "-" + header.getMessageType();
        String messageClass = requestMap.get(key);
        if (messageClass == null) {
            throw new MessageProtocolException("No such message: " + key);
        }

        String messageClassFqn = JsonModuleConstants.JSON_MODULE_BASE_PACKAGE + JsonModuleConstants.ENVELOP_BEAN_IMPL_PATH + messageClass;

        try {
            Class specificClass = Class.forName(messageClassFqn);
            Envelope specificEnvelope = (Envelope) jsonParser.fromJson(jsonEnvelopeMessage, specificClass);
            return (Message) specificEnvelope.getBody();
        } catch (ClassNotFoundException e) {
            throw new InvalidVersionException("Invalid version. " + e.getMessage(), e);
        } catch (Exception e) {
            throw new MessageProtocolException("Error parsing message of type " + header.getMessageGroup() + "-" + header.getMessageType() + " from channel: " + header.getChannelId(), e);
        }
    }


    public <T extends Message> T getMessageFromBodyString(String jsonBodyString, Class<T> type) throws MessageProtocolException {
        try {
            return jsonParser.fromJson(jsonBodyString, type);
        } catch (Exception ex) {
            throw new MessageProtocolException("Error parsing message", ex);
        }
    }


    public String getJSonString(Envelope envelopeMessage) {
        return jsonParser.toJson(envelopeMessage);
    }


    public String getJSonString(Header header, Message data) {
        Envelope envelope = new EnvelopeBean((HeaderBean) header, data);
        return jsonParser.toJson(envelope);
    }


    public String getJSonString(Object messageData) {
        return jsonParser.toJson(messageData);
    }


    public JsonObject getJsonObjectFromRequest(Header header, String requestMessage) throws MessageProtocolException, InvalidVersionException {
        String key = header.getMessageGroup() + "-" + header.getMessageType();
        String messageClass = requestMap.get(key);
        if (messageClass == null) {
            throw new MessageProtocolException("No such message: " + key);
        }
        JsonObject requestData = null;
        try {
            JsonObject jsonObject = jsonObjectParser.parse(requestMessage).getAsJsonObject();
            for (Map.Entry<String, JsonElement> element : jsonObject.entrySet()) {
                if ("DAT".equals(element.getKey())) {
                    for (Map.Entry<String, JsonElement> innerElement : element.getValue().getAsJsonObject().entrySet()) {
                        requestData = innerElement.getValue().getAsJsonObject();
                    }
                }
            }
        } catch (Exception e) {
            throw new MessageProtocolException("Error parsing message of type " + header.getMessageGroup() + "-" + header.getMessageType() + " from channel: " + header.getChannelId(), e);
        }
        return requestData;
    }

}