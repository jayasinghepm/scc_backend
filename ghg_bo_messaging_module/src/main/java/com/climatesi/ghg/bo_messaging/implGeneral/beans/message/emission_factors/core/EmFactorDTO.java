package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core;

public class EmFactorDTO {

    private int id;

    private String description;

    private float value;

    private String reference;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
