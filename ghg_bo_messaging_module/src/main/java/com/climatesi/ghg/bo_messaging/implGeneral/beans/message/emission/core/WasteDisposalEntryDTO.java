package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class WasteDisposalEntryDTO {

    @SerializedName("WASTE_DISPOSAL_ENTRY_ID")
    private int entryId;


    @SerializedName("EMISSION_SRC_ID")
    private int emissionSrcId;

    @SerializedName("ADDED_BY")
    private int addedBy;

    @SerializedName("BRANCH_ID")
    private int branchId;

    @SerializedName("COMPANY_ID")
    private int companyId;

    @SerializedName("ADDED_DATE")
    private Date addedDate;

    @SerializedName("LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @SerializedName("MONTH")
    private int month;

    @SerializedName("YEAR")
    private String year;

    @SerializedName("UNITS")
    private int units;

    @SerializedName("AMOUNT_DISPOSED")
    private float amountDisposed;

    @SerializedName("DISPOSAL_METHOD")
    private int disposalMethod;

    @SerializedName("WASTE_TYPE")
    private int wasteType;

    @SerializedName("SUB_CONTRACTOR_NAME")
    private String subContractorName;

    @SerializedName("EMISSION_INFO")
    private JsonObject emissionDetails;

    private int isDeleted;

    private String company;

    private String branch;

    private String mon;


    private String disposal;

    private String waste;

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public void setWasteType(int wasteType) {
        this.wasteType = wasteType;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getDisposal() {
        return disposal;
    }

    public void setDisposal(String disposal) {
        this.disposal = disposal;
    }

    public String getWaste() {
        return waste;
    }

    public void setWaste(String waste) {
        this.waste = waste;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public JsonObject getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(JsonObject emissionDetails) {
        this.emissionDetails = emissionDetails;
    }


    public int getEntryId() {
        return this.entryId;
    }


    public void setEntryId(int id) {
        this.entryId = id;
    }


    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }


    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }


    public int getAddedBy() {
        return this.addedBy;
    }


    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }


    public int getBranchId() {
        return this.branchId;
    }


    public void setBranchId(int id) {
        this.branchId = id;
    }


    public int getCompanyId() {
        return this.companyId;
    }


    public void setCompanyId(int id) {
        this.companyId = id;
    }


    public Date getEntryAddedDate() {
        return this.addedDate;
    }


    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }


    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }


    public int getMonth() {
        return this.month;
    }


    public void setMonth(int month) {
        this.month = month;
    }


    public String getYear() {
        return this.year;
    }


    public void setYear(String year) {
        this.year = year;
    }


    public float getAmountDisposed() {
        return this.amountDisposed;
    }


    public void setAmountDisposed(float amount) {
        this.amountDisposed = amount;
    }


    public int getDisposalMethod() {
        return this.disposalMethod;
    }


    public void setDisposalMethod(int disposalMethod) {
        this.disposalMethod = disposalMethod;
    }


    public int getWasteType() {
        return this.wasteType;
    }


    public void setWasteype(int type) {
        this.wasteType = type;
    }


    public String getSubContractorName() {
        return this.subContractorName;
    }


    public void setSubContractorName(String name) {
        this.subContractorName = name;
    }


    public int getUnits() {
        return this.units;
    }


    public void setUnits(int units) {
        this.units = units;
    }
}
