package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaperWasteDTO;
import com.google.gson.annotations.SerializedName;

public class PaperWasteManagedResMsg  extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private PaperWasteDTO dto;

    public PaperWasteDTO getDto() {
        return dto;
    }

    public void setDto(PaperWasteDTO dto) {
        this.dto = dto;
    }
}
