package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GeneratorsEntryDTO;
import com.google.gson.annotations.SerializedName;

public class GeneratorsEntryMangedResponseMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private GeneratorsEntryDTO dto;

    public GeneratorsEntryDTO getDto() {
        return dto;
    }

    public void setDto(GeneratorsEntryDTO dto) {
        this.dto = dto;
    }
}
