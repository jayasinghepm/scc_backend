package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LPGEntryDTO;
import com.google.gson.annotations.SerializedName;

public class BioMassEntryManagedResMsg extends EntityMasterResponseBean {


    @SerializedName("DTO")
    private BioMassEntryDTO dto;

    public BioMassEntryDTO getDto() {
        return dto;
    }

    public void setDto(BioMassEntryDTO dto) {
        this.dto = dto;
    }
}