package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaperWasteDTO;
import com.google.gson.annotations.SerializedName;

public class PaperWasteManagedReqMsg  implements Message {

    @SerializedName("DATA")
    private PaperWasteDTO dto;

    public PaperWasteDTO getDto() {
        return dto;
    }

    public void setDto(PaperWasteDTO dto) {
        this.dto = dto;
    }
}