package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.base.EntityMasterResponseBean;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.EmissionSourceDTO;
import com.google.gson.annotations.SerializedName;

public class EmissionSourceMangedResponseMsg extends EntityMasterResponseBean {

    @SerializedName("DTO")
    private EmissionSourceDTO dto;

    public EmissionSourceDTO getDto() {
        return dto;
    }

    public void setDto(EmissionSourceDTO dto) {
        this.dto = dto;
    }
}
