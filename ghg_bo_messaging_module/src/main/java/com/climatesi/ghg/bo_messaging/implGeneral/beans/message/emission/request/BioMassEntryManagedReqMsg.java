package com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request;

import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.google.gson.annotations.SerializedName;

public class BioMassEntryManagedReqMsg implements Message {

    @SerializedName("DATA")
    private BioMassEntryDTO dto;

    public BioMassEntryDTO getDto() {
        return dto;
    }

    public void setDto(BioMassEntryDTO dto) {
        this.dto = dto;
    }
}