package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.BioMassPopulator;
import com.climatesi.ghg.bo_controller.populators.LorryTransportationPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LorryTransportationDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.LorryTransporationBean;

import com.climatesi.ghg.emission_source.implGeneral.facades.LorryTransportationEntryManagerFacade;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class LorryTransportationController extends AbstractController {
    private static final Logger logger = Logger.getLogger(LorryTransportationController.class);


    private LorryTransportationEntryManagerFacade lorryTransportationEntryManagerFacade;

    private EmissionFactorsManager emissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {

        try {
            lorryTransportationEntryManagerFacade = (LorryTransportationEntryManagerFacade) EmsissionSourceFactory.getInstance().getLorryTransportationManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing MunicipalWaterController", e);
        }
    }


    public ListResult<LorryTransportation> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return lorryTransportationEntryManagerFacade.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public LorryTransportation edit(LorryTransportationDTO dto) throws GHGException {
        LorryTransportation entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                LorryTransportationPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.getIsDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) lorryTransportationEntryManagerFacade.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new LorryTransporationBean();
                LorryTransportationPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emission(entry));
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) lorryTransportationEntryManagerFacade.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public LorryTransportation findById(Integer id) {
        return lorryTransportationEntryManagerFacade.getEntityByKey(id);
    }

    public HashMap<String, String> emission(LorryTransportation entry) {
        float consumptionLiters = entry.getFuelConsumption();
//        float consumptionLiters = entry.getDistance() / entry.getFuelEconomy();
        float ncv = 0.043f;
        float efCo2 = 74.1f;
        float efCh4 =  0.1092f;
        float efN20 = 1.0335f;
        float density  = 0.84f;

        ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();
        efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL).getValue();
        efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL).getValue();
        efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL).getValue();
        density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();
        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();



        ResultDTO e1 = Calculator.emissionVehicles(
                consumptionLiters,
                ncv,
                density,
                efCo2,
                efCh4,
                efN20,
                gwp_co2,
                gwp_ch4,
                gwp_n20
        );
        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", consumptionLiters * 0.001) );
        results.put("direct", Boolean.toString(false)); //todo
        results.put("ncv",String.format("%.5f", ncv) );
        results.put("density",String.format("%.5f",density) );
        results.put("efCO2",String.format("%.5f", efCo2) );
        results.put("efCH4",String.format("%.5f", efCh4) );
        results.put("efN20",String.format("%.5f", efN20) );
        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20) );
        results.put("distance",String.format("%.5f", entry.getDistance()) );
        results.put("fuel_economy",String.format("%.5f", entry.getFuelEconomy()) );
        results.put("te",String.format("%.5f",e1.totalEnergy) );
        return results;
    }

}
