package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.AdminDTO;
import com.climatesi.ghg.business_users.api.beans.Admin;
import org.jboss.logging.Logger;

public class AdminPopulator {

    private final static Logger logger = Logger.getLogger(AdminPopulator.class);
    private static AdminPopulator instance;

    private AdminPopulator() {

    }

    public static AdminPopulator getInstance() {
        if (instance == null) {
            instance = new AdminPopulator();
        }
        return instance;
    }

    public AdminDTO populateDTO(Admin admin) {

        AdminDTO dto = new AdminDTO();
        try {
            dto.setId(admin.getId());
            dto.setFirstName(admin.getFirstName());
            dto.setLastName(admin.getLastName());
            dto.setTitle(admin.getTitle());
            dto.setEmail(admin.getEmail());
            dto.setTelephoneNo(admin.getTelephoneNo());
            dto.setMobileNo(admin.getMobileNo());
            dto.setUserId(admin.getUserId());
            dto.setDeleted(admin.isDeleted());
            dto.setEntitlements(admin.getEntitlements());
            dto.setRole(admin.getRole());
            dto.setPosition(admin.getPosition());
//            dto.setUserId(admin.getUserId());
        } catch (NullPointerException e) {
            logger.error("Error in populating AdminDTO", e);

        }
        return dto;

//        dto.setAddedDate(admin.getAddedDate());
    }

    public Admin populate(Admin admin, AdminDTO dto) {
        try {
            if (dto.getId() > 0) {
                admin.setId(dto.getId());
                if (dto.isDeleted() == 1) {
                    admin.setDeleted(dto.isDeleted());
                    return admin;
                }

            }
            admin.setUserId(dto.getUserId() == 0 ? admin.getUserId() : dto.getUserId());
            admin.setPosition(dto.getPosition() == null ? admin.getPosition() : dto.getPosition());
            admin.setEntitlements(dto.getEntitlements() == null ? admin.getEntitlements() : dto.getEntitlements());
            admin.setRole(dto.getRole() == 0 ? admin.getRole() : dto.getRole());
            admin.setEmail(dto.getEmail() == null ? admin.getEmail() : dto.getEmail());
            admin.setFirstName(dto.getFirstName() == null ? admin.getFirstName() : dto.getFirstName());
            admin.setLastName(dto.getLastName() == null ? admin.getLastName() : dto.getLastName());
            admin.setTitle(dto.getTitle() == null ? admin.getTitle() : dto.getTitle());
            admin.setTelephoneNo(dto.getTelephoneNo() == null ? admin.getTelephoneNo() : dto.getTelephoneNo());
            admin.setMobileNo(dto.getMobileNo() == null ? admin.getMobileNo() : dto.getMobileNo());
        } catch (NullPointerException e) {
            logger.error("Error in populating Admin", e);
        }
        return admin;
    }
}
