package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_controller.populators.*;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.reqeust.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.response.*;
import com.climatesi.ghg.business_users.api.beans.*;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class BusinessUserHelper {

    private static Logger logger = Logger.getLogger(BusinessUserHelper.class);
    private BusinessUserController controller;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;

    public BusinessUserHelper() {
        try {
            controller = GHGControllerFactory.getInstance().getBusinessUserController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
        } catch (Exception e) {
            logger.error("Error in intializing controllers", e);
        }
    }

    public EmployeeListResponseMsg getEmployees(EmployeeListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Employee> list = null;
        EmployeeListResponseMsg res = new EmployeeListResponseMsg();
        list = controller.getEmployeesFiltered(pageNumber, sorting, filter);

        List<EmployeeDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                EmployeeDTO dto = EmployeePopulator.getInstance().populateDTO((Employee) o);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public AdminListResponseMsg getAdmins(AdminListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Admin> list = null;
        AdminListResponseMsg res = new AdminListResponseMsg();
        list = controller.getAdminsFiltered(pageNumber, sorting, filter);

        List<AdminDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                AdminDTO dto = AdminPopulator.getInstance().populateDTO((Admin) o);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public CadminListResponseMsg getCadmins(CadminListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<ComAdmin> list = null;
        CadminListResponseMsg res = new CadminListResponseMsg();
        list = controller.getComAdminsFiltered(pageNumber, sorting, filter);

        List<CadminDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                CadminDTO dto = ComAdminPopulator.getInstance().populateDTO((ComAdmin) o);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public DataEntryUserListResMsg getDataEntryUsers(DataEntryUserListReqMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<DataEntryUser> list = null;
        DataEntryUserListResMsg res = new DataEntryUserListResMsg();
        list = controller.getDataEntryUsersFiltered(pageNumber, sorting, filter);

        List<DataEntryUserDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                DataEntryUserDTO dto = DataEntryUserPopulator.getInstance().populateDTO((DataEntryUser) o);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public ClientListResponseMsg getClients(ClientListRequestMsg msg, int user) {

        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Client> list = null;
        ClientListResponseMsg res = new ClientListResponseMsg();
        list = controller.getClientsFiltered(pageNumber, sorting, filter);

        List<ClientDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                ClientDTO dto = ClientPopulator.getInstance().populateDTO((Client) o);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }


    public EmployeeManagedResponseMsg manageEmployee(EmployeeMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        Employee entry;
        EmployeeManagedResponseMsg res = new EmployeeManagedResponseMsg();
        try {
            entry = controller.editEmployee(msg.getDto());
            if (entry != null) {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(EmployeePopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Employee", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public AdminManagedResponseMsg manageAdmin(AdminManagedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        Admin entry;
        AdminManagedResponseMsg res = new AdminManagedResponseMsg();
        try {
            entry = controller.editAdmin(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Admin "+ msg.getDto().getFirstName() + " " + msg.getDto().getLastName() + " was"  + (msg.getDto().getId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(AdminPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Admin", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public CadminManagedResponseMsg manageCadmin(CadminManagedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        ComAdmin entry;
        CadminManagedResponseMsg res = new CadminManagedResponseMsg();
        try {

            // limit 1 cadmin per company check when creating new one
            if (msg != null && msg.getDto() != null && msg.getDto().getId() <= 0) {
                JsonObject filter = new JsonObject();
                JsonObject companyFilter = new JsonObject();
                companyFilter.addProperty("value", msg.getDto().getCompanyId());
                companyFilter.addProperty("col", 1);
                companyFilter.addProperty("type", 1);
                filter.add("companyId", companyFilter);
                List<ComAdmin> comAdmins = controller.getComAdminsFiltered(-1, "", filter).getList();
                if (comAdmins != null && comAdmins.size() > 0) {
                    res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                    res.setNarration("Can't create more than one Company Admins.");
                    return res;
                }
            }
            entry = controller.editComAdmin(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Company Admim was" + (msg.getDto().getId() <=0 ? "added " : "apdated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(ComAdminPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage ComADmin", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public ClientManagedResponseMsg manageClient(ClientManagedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        Client entry;
        ClientManagedResponseMsg res = new ClientManagedResponseMsg();
        try {
            entry = controller.editCliente(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Client was" + (msg.getDto().getId() <=0 ? "added " : "apdated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------

                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(ClientPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Client", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public DataEntryUserManagedResMsg manageDataEntryUser(DataEntryUserManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        DataEntryUser entry;
        DataEntryUserManagedResMsg res = new DataEntryUserManagedResMsg();
        try {
            entry = controller.editDataEntryUser(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin = userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type", 1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result = businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName() + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result = businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName() + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result = businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName() + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog("Data Entry User was" + (msg.getDto().getId() <= 0 ? "added " : "updated ") + " by" + log.getUsername());
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(DataEntryUserPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Data Entry User", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }
}

