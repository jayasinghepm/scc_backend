package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg_mitication.api.ProjectStatus;
import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project.GHGProjectController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.insitution.InsstitutionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_controller.populators.BranchPopulator;
import com.climatesi.ghg.bo_controller.populators.CompanyPopulator;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.BranchDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.response.*;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.implGeneral.EmissionCategoryBean;
import com.climatesi.ghg.implGeneral.ExcludedReasonBean;
import com.climatesi.ghg.implGeneral.ProjectSuggestionBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InsititutionHelper {

    private static Logger logger = Logger.getLogger(InsstitutionController.class);
    private InsstitutionController controller;
    private GHGProjectController projectController;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;

    public InsititutionHelper() {
        try {
            controller = GHGControllerFactory.getInstance().getInsstitutionController();
            projectController = GHGControllerFactory.getInstance().getGHGProjectController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
        } catch (Exception e) {
            logger.error("Error in getting instance of institution controllers >>", e);
        }
    }

    public BranchListResponseMsg getBranchList(BranchListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Branch> list = null;
        BranchListResponseMsg res = new BranchListResponseMsg();
        list = controller.getBranchFiltered(pageNumber, sorting, filter);

        List<BranchDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                BranchDTO dto = BranchPopulator.getInstance().populateDTO((Branch) o);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public CompanyListResponseMsg getCompanyList(CompanyListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Company> list = null;
        CompanyListResponseMsg res = new CompanyListResponseMsg();
        list = controller.getCompanyFiltered(pageNumber, sorting,  filter);

        List<CompanyDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                CompanyDTO dto = CompanyPopulator.getInstance().popoulateDTO((Company) o, msg.getIsLogoRequired());
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public CompanyManagedResponseMsg manageCompany(CompanyManagedReqeustMsg msg, int user) {
        logger.info("Message : " + msg);
        Company entry;
        CompanyManagedResponseMsg res = new CompanyManagedResponseMsg();
        try {
            entry = controller.editCompany(msg.getDto());
            if (entry != null) {

                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

//                        if (userLogin.getUserType() == 1) {
//                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(result.getList().get(0).getBranchId());
//                                log.setCompanyId(result.getList().get(0).getCompanyId());
//                            }
//                        }
//                        if (userLogin.getUserType() == 2) {
//                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(-1);
//                                log.setCompanyId(result.getList().get(0).getCompanyId());
//                            }
//                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog((msg.getDto().getId() <= 0? "Added " : "Updated ")+ "company " +  (msg.getDto().getName()) + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------

                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(CompanyPopulator.getInstance().popoulateDTO(entry, 0));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }

        } catch (GHGException e) {
            logger.error("Failed in manage Company", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public ManageMenuCompanyResMsg manageMenuCompany(ManageMenuCompanyReqMsg msg, int user) {
        Company entry;
        ManageMenuCompanyResMsg res = new ManageMenuCompanyResMsg();
        try {
            Company company = controller.findCompanyById(msg.getDto().getId());
            if (company == null) throw  new GHGException("Company cannot be null");
            company.setPages(msg.getDto().getPages());
            company.setEmSources(msg.getDto().getEmSources());
            company.setAllowedEmissionSources(msg.getDto().getAllowedEmissionSources());
            controller.updateMenus(company);

        }catch (GHGException e) {
            logger.error("Failed in manage Company", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public BranchManagedResponseMsg manageBranch(BranchManagedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        Branch entry;
        BranchManagedResponseMsg res = new BranchManagedResponseMsg();
        try {
            entry = controller.editBranch(msg.getDto());
            if (entry != null) {

                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog((msg.getDto().getId() <= 0? "Added " : "Updated ")+ " branch " + (msg.getDto().getName()) + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------

                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(BranchPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Branch", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public Message getCompanyListDataEntry(CompanyListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Company> list = null;
        CompanyListResponseMsg res = new CompanyListResponseMsg();
        list = controller.getCompanyFiltered(pageNumber, sorting, filter);

        List<CompanyDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                JsonObject filt = new JsonObject();
                JsonObject projectFilter = new JsonObject();
                projectFilter.addProperty("type",1);
                projectFilter.addProperty("col",1);
                projectFilter.addProperty("value", ((Company)o).getId());
                filt.add("companyId", projectFilter);
                List<Project> projects = projectController.getFilteredResults(-1, "",filt).getList();
                if (projects.size() == 0) {

                }else {
                    for(Project p : projects) {
                        if (p.getStatus() == ProjectStatus.DATA_ENTRY) {
                            CompanyDTO dto = CompanyPopulator.getInstance().popoulateDTO((Company) o, msg.getIsLogoRequired());
                            results.add(dto);
                            break;
                        }
                    }
                }



            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public Message getCompanyListSummaryInputs(CompanyListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Company> list = null;
        CompanyListResponseMsg res = new CompanyListResponseMsg();
        list = controller.getCompanyFiltered(pageNumber, sorting, filter);

        List<CompanyDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                JsonObject filt = new JsonObject();
                JsonObject projectFilter = new JsonObject();
                projectFilter.addProperty("type",1);
                projectFilter.addProperty("col",1);
                projectFilter.addProperty("value", ((Company)o).getId());
                filt.add("companyId", projectFilter);
                List<Project> projects = projectController.getFilteredResults(-1, "",filt).getList();
                if (projects.size() == 0) {

                }else {
                    for(Project p : projects) {
                        if (p.getStatus()>= ProjectStatus.DATA_ENTRY &&  p.getStatus() < ProjectStatus.CLOSED) {
                            CompanyDTO dto = CompanyPopulator.getInstance().popoulateDTO((Company) o, msg.getIsLogoRequired());
                            results.add(dto);
                            break;
                        }
                    }
                }



            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }


    public ManageEmissionCategoryResMsg manageEmissionCategory(ManageEmissionCategoryReqMsg msg, int user) {
        ManageEmissionCategoryResMsg res = new  ManageEmissionCategoryResMsg();

        if (msg == null || msg.getDto() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            return  res;
        }

        EmissionCategoryBean categoryBean = controller.updateEmissionCategory(msg);
        if (categoryBean == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            return  res;
        }

        res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
        res.setDto(msg.getDto());
        return  res;



    }


    public UpdateCompanyExcludedReasonAndSuggestionResMsg updateSuggestionsAndExcludedEmissionSources(UpdateCompanyExcludedReasonAndSuggestionReqMsg msg, int user) {

        UpdateCompanyExcludedReasonAndSuggestionResMsg res = new UpdateCompanyExcludedReasonAndSuggestionResMsg();

        if (msg == null || msg.getExcludedReasons() == null  || msg.getSuggestions() == null || msg.getCompanyId() <= 0) {
            logger.info("######################### Failed here 418");

            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            return res;
        }

        Company company = controller.findCompanyById(msg.getCompanyId());

        if (company == null) {
            logger.info("##########################             Failed here 427");
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            return res;
        }

        company.setExcludedReasons(msg.getExcludedReasons().stream().map(dto -> {
            ExcludedReasonBean bean = new ExcludedReasonBean();
            bean.setReason(dto.getReason());
            bean.setSrcName(dto.getSrcName());

            return bean;

        }).collect(Collectors.toSet()));

        company.setProjectSuggestions(msg.getSuggestions().stream().map(dto -> {
            ProjectSuggestionBean bean = new ProjectSuggestionBean();
            bean.setDescription(dto.getDescription());
            bean.setTitle(bean.getTitle());

            return bean;

        }).collect(Collectors.toSet()));



        try {
            controller.updateSuggestionsAndExcludedReasons(company);
        } catch (Exception e) {
            logger.error("Failed " , e);
           res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
           return res;
        }


        return res;
    }
}
