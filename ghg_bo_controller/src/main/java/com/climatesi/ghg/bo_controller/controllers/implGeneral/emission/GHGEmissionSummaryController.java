package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.summary.GHGEmsissionSummary;
import com.climatesi.ghg.emission_source.api.facades.GHGEmissionSummaryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.GHGEmissionSummaryIdBean;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Stateless
public class GHGEmissionSummaryController extends AbstractController {

    private static final Logger logger = Logger.getLogger(GHGEmissionSummaryController.class);
    private GHGEmissionSummaryManager ghgEmissionSummaryManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            ghgEmissionSummaryManager = EmsissionSourceFactory.getInstance().getGHGEmissionSummaryManager(em);
        } catch (Exception e) {
            logger.error("Error in initilizing GHGEmissionSummaryController");
        }
    }

    public GHGEmsissionSummary edit(GHGEmsissionSummary entry) {
        try {
            String status = (String) ghgEmissionSummaryManager.updateEntity(entry);
        } catch (Exception e) {
            logger.error("Error in updating GHGEmission Summary", e);
        }
        return entry;
    }

    public int delete(int comId, String fy) {
        try {
            return ghgEmissionSummaryManager.delete(comId, fy);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }

    public GHGEmsissionSummary findById(int companyId, int branchId, String fy) {
        GHGEmissionSummaryIdBean id = new GHGEmissionSummaryIdBean();
        id.setCompanyId(companyId);
        id.setFY(fy);
        GHGEmsissionSummary summary;
        try {
            summary = ghgEmissionSummaryManager.getEntityByKey(id);
            return summary;
        } catch (Exception e) {
            logger.error("Error in finding ghg summary bean");

        }
        return null;
    }
    public GHGEmsissionSummary find(int companyId, int modeOfEntry, String year) {

        GHGEmsissionSummary summary;
        try {
            summary = ghgEmissionSummaryManager.find(companyId, year, modeOfEntry);
            return summary;
        } catch (Exception e) {
            logger.error("Error in finding ghg summary bean");

        }
        return null;
    }

    public List<GHGEmsissionSummary> getPaginatedGHGEmissonSummary(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return ghgEmissionSummaryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria).getList();
    }

    /*
    This method will handle uploding created file to s3 and return url on success
     */
    public String getExportedGHGEmissionSummary(
            String comName,
            String currentYear,
            List<String> years,
            List<HashMap<String, String>> actData,
            List<HashMap<String, String>> directData,
            List<HashMap<String, String>> indiretData,
            List<HashMap<String, String>> summaryData,
            HashMap<String, List<List<String>>> catDataMap
    ) {
//        ghgsummary
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                "AKIA2R3JT3XWOWX6TG6N", "UeSER3oPyJMBDkbejfB37LX8zFyEG4SqAT2AwDz4");
        // us-west-2 is AWS Oregon
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();

        String filePath = createExcel(
                comName,
                currentYear,
                years,
                actData,
                directData,
                indiretData,
                summaryData,
                catDataMap
        );

        String fileName = comName.trim() + ".xlsx";

        PutObjectRequest request = new PutObjectRequest("ghgsummary", fileName, new File(filePath));
        request.setCannedAcl(CannedAccessControlList.PublicRead);
        PutObjectResult res = s3Client.putObject(request);


        System.out.println("--Uploading file done");
        System.out.println("--Downloading file done");

        return "https://ghgsummary.s3.ap-south-1.amazonaws.com/" + fileName ;
    }

    /*
     This method will handle creating and writing excel tempory file
     */
    private String createExcel(String comName,
                               String currentYear,
                               List<String> years,
                               List<HashMap<String, String>> actData,
                               List<HashMap<String, String>> directData,
                               List<HashMap<String, String>> indiretData,
                               List<HashMap<String, String>> summaryData,
                               HashMap<String, List<List<String>>> catDataMap
                               ) {


        int rowIndex = 0;
        int colIndex = 0;


        File tempFile = null;
        Workbook wb = new XSSFWorkbook();

        Sheet sheet = wb.createSheet("Summary");
        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 4000);

        //----------------------- fonts-----------------------------


        XSSFFont font = ((XSSFWorkbook) wb).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);

        //---------------------- common style-----------------------
        CellStyle tabCellStyle = wb.createCellStyle();
        tabCellStyle.setFont(font);
        tabCellStyle.setAlignment(CellStyle.ALIGN_LEFT);

        CellStyle tabHeaderStyle = wb.createCellStyle();
        tabHeaderStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        tabHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        tabHeaderStyle.setAlignment(CellStyle.ALIGN_CENTER);
        tabHeaderStyle.setFont(font);

        CellStyle tabHeadRowStyle = wb.createCellStyle();
        tabHeadRowStyle.setFont(font);
        tabHeadRowStyle.setAlignment(CellStyle.ALIGN_CENTER);

        //---------------   Project Name ------------------------------
        Row header = sheet.createRow(0);

        CellStyle headerStyle = wb.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setAlignment(CellStyle.ALIGN_CENTER);


        headerStyle.setFont(font);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, 20));

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("GHG Emission Summary");
        headerCell.setCellStyle(headerStyle);

        rowIndex++;

        //*********************************************************************************

        //----------------------------------Company and Fy------------------------------------

        Row comRow = sheet.createRow(1);

        CellStyle comRowStyle = wb.createCellStyle();
        comRowStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        comRowStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        comRowStyle.setAlignment(CellStyle.ALIGN_CENTER);
        comRowStyle.setFont(font);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, 20));

        Cell comCell = comRow.createCell(0);
        comCell.setCellValue(comName + " " + currentYear);
        comCell.setCellStyle(headerStyle);

        rowIndex++;

        //************************************************************************************

        //------------------------------ Summary ---------------------------------------------
        int summaryTableMergeCols = years.size();

        //summary Heading row
        rowIndex += 2;
        Row summaryRow = sheet.createRow(rowIndex);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, summaryTableMergeCols));


        Cell summaryHeadingcell = summaryRow.createCell(0);
        summaryHeadingcell.setCellValue("Summary");
        summaryHeadingcell.setCellStyle(tabHeaderStyle);

        rowIndex++;


        //table first column
        Row sumTabheadRow = sheet.createRow(rowIndex);

//        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, 3));

        //Cell for header first column

        Cell sumTabHeadRowFirstCol = sumTabheadRow.createCell(0);
        sumTabHeadRowFirstCol.setCellValue("Categories/Years");
        sumTabHeadRowFirstCol.setCellStyle(tabHeadRowStyle);

        colIndex = 1;

        for (String year : years) {

            Cell cell = sumTabheadRow.createCell(colIndex);
            colIndex++;
            cell.setCellValue(year);
            cell.setCellStyle(tabHeadRowStyle);
        }

        colIndex = 0;

        rowIndex += 2;

        List<List<String>> sumTabMat = new ArrayList<List<String>>();
        for (String key : summaryData.get(0).keySet()) {
            List<String> row = new ArrayList<String>();
            row.add(key);
            //go through all maps
            for (HashMap<String, String> map : summaryData) {
                row.add(map.get(key));
            }
            sumTabMat.add(row);
        }

        // use sumTabMat to create rows

        for (List<String> row : sumTabMat) {
            Row tabRow = sheet.createRow(rowIndex);
            rowIndex++;
            colIndex = 0;
            for (String colVals : row) {
                Cell cell = tabRow.createCell(colIndex);
                cell.setCellValue(colVals);
                cell.setCellStyle(tabCellStyle);
                colIndex++;
            }
        }


        colIndex = 0;
        rowIndex += 2;


        //***********************************************************************************

        //----------------------------- Activity Table --------------------------------------

        int actDataTableMergeCols = years.size();

        //summary Heading row
//        rowIndex += 2;
        Row actDataRow = sheet.createRow(rowIndex);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, actDataTableMergeCols));


        Cell actDataHeadingCell = actDataRow.createCell(0);
        actDataHeadingCell.setCellValue("Activity Data");
        actDataHeadingCell.setCellStyle(tabHeaderStyle);

        rowIndex++;


        //table first row
        Row actTabHeaderRow = sheet.createRow(rowIndex);


        //Cell for header first column


        Cell actTabHeaderRowFirstCol = actTabHeaderRow.createCell(0);
        actTabHeaderRowFirstCol.setCellValue("Categories/Years");
        actTabHeaderRowFirstCol.setCellStyle(tabHeadRowStyle);

        colIndex = 1;

//        for (String year : years) {
//
//            Cell cell = actTabHeaderRow.createCell(colIndex);
//            colIndex ++;
//            cell.setCellValue(year);
//            cell.setCellStyle(tabHeadRowStyle);
//        }
        {
            Cell cell = actTabHeaderRow.createCell(colIndex);
            cell.setCellValue(currentYear);
            cell.setCellStyle(tabHeadRowStyle);
        }
        rowIndex++;

        colIndex = 0;


        List<List<String>> actDataTabMat = new ArrayList<List<String>>();
        for (String key : actData.get(0).keySet()) {
            List<String> row = new ArrayList<String>();
            row.add(key);
            //go through all maps
            for (HashMap<String, String> map : actData) {
                row.add(map.get(key));
            }
            actDataTabMat.add(row);
        }

        // use sumTabMat to create rows

        for (List<String> row : actDataTabMat) {
            Row tabRow = sheet.createRow(rowIndex);
            rowIndex++;
            colIndex = 0;
            for (String colVals : row) {
                Cell cell = tabRow.createCell(colIndex);
                cell.setCellValue(colVals);
                cell.setCellStyle(tabCellStyle);
                colIndex++;
            }
        }


        colIndex = 0;
        rowIndex += 2;

        //***********************************************************************************

        //---------------------------  Direct emission --------------------------------------

        int directEmTableMergeCols = years.size();

        //summary Heading row
//        rowIndex += 2;
        Row directTabHeaderRow = sheet.createRow(rowIndex);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, directEmTableMergeCols));


        Cell directEmHeadingCell = directTabHeaderRow.createCell(0);
        directEmHeadingCell.setCellValue("Direct Emission");
        directEmHeadingCell.setCellStyle(tabHeaderStyle);

        rowIndex++;


        //table first row
        Row directCatRow = sheet.createRow(rowIndex);


        //Cell for header first column


        Cell directRowFirstCol = directCatRow.createCell(0);
        directRowFirstCol.setCellValue("Categories/Years");
        directRowFirstCol.setCellStyle(tabHeadRowStyle);

        colIndex = 1;

        for (String year : years) {

            Cell cell = directCatRow.createCell(colIndex);
            colIndex++;
            cell.setCellValue(year);
            cell.setCellStyle(tabHeadRowStyle);
        }

        colIndex = 0;
        rowIndex++;


        List<List<String>> directEmTabMat = new ArrayList<List<String>>();
        for (String key : directData.get(0).keySet()) {
            List<String> row = new ArrayList<String>();
            row.add(key);
            //go through all maps
            for (HashMap<String, String> map : directData) {
                row.add(map.get(key));
            }
            directEmTabMat.add(row);
        }

        // use sumTabMat to create rows

        for (List<String> row : directEmTabMat) {
            Row tabRow = sheet.createRow(rowIndex);
            rowIndex++;
            colIndex = 0;
            for (String colVals : row) {
                Cell cell = tabRow.createCell(colIndex);
                cell.setCellValue(colVals);
                cell.setCellStyle(tabCellStyle);
                colIndex++;
            }
        }


        colIndex = 0;
        rowIndex += 2;


        //***********************************************************************************


        //---------------------------  INDirect emission --------------------------------------

        int indirectEmTableMergeCols = years.size();

        //summary Heading row
//        rowIndex += 2;
        Row indirectTabHeaderRow = sheet.createRow(rowIndex);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, indirectEmTableMergeCols));


        Cell indirectEmHeadingCell = indirectTabHeaderRow.createCell(0);
        indirectEmHeadingCell.setCellValue("Indirect Emission");
        indirectEmHeadingCell.setCellStyle(tabHeaderStyle);

        rowIndex++;


        //table first row
        Row indirectCatRow = sheet.createRow(rowIndex);


        //Cell for header first column


        Cell indirectRowFirstCol = indirectCatRow.createCell(0);
        indirectRowFirstCol.setCellValue("Categories/Years");
        indirectRowFirstCol.setCellStyle(tabHeadRowStyle);

        colIndex = 1;

        for (String year : years) {

            Cell cell = indirectCatRow.createCell(colIndex);
            colIndex++;
            cell.setCellValue(year);
            cell.setCellStyle(tabHeadRowStyle);
        }

        colIndex = 0;
        rowIndex++;


        List<List<String>> indirectEmTabMat = new ArrayList<List<String>>();
        for (String key : indiretData.get(0).keySet()) {
            List<String> row = new ArrayList<String>();
            row.add(key);
            //go through all maps
            for (HashMap<String, String> map : indiretData) {
                row.add(map.get(key));
            }
            indirectEmTabMat.add(row);
        }

        // use sumTabMat to create rows

        for (List<String> row : indirectEmTabMat) {
            Row tabRow = sheet.createRow(rowIndex);
            rowIndex++;
            colIndex = 0;
            for (String colVals : row) {
                Cell cell = tabRow.createCell(colIndex);
                cell.setCellValue(colVals);
                cell.setCellStyle(tabCellStyle);
                colIndex++;
            }
        }


        colIndex = 0;
        rowIndex += 2;


//        ****************************************  Emission Sources with categories *****************************


//        int catTableMergeCols = 6;
//
//        //summary Heading row
////        rowIndex += 2;
//        Row catTableTitleRow = sheet.createRow(rowIndex);
//
//        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, catTableMergeCols));
//
//
//        Cell catTableTitleCell = indirectTabHeaderRow.createCell(0);
//        catTableTitleCell.setCellValue("Emission Categories");
//        catTableTitleCell.setCellStyle(tabHeaderStyle);
//
//        rowIndex++;
//
//
//        //table header row
//        Row catTableHeaderRow = sheet.createRow(rowIndex);
//        Cell catTableCategoriesCell = catTableHeaderRow.createCell(0);
//        catTableTitleCell.setCellValue("Categories");
//        catTableTitleCell.setCellStyle(tabHeaderStyle);
//
//        Cell catTableEmissionSourceCell = catTableHeaderRow.createCell(1);
//        catTableEmissionSourceCell.setCellValue("Emission Sources");
//        catTableEmissionSourceCell.setCellStyle(tabHeaderStyle);
//
//        Cell catTabletCo2Cell = catTableHeaderRow.createCell(2);
//        catTabletCo2Cell.setCellValue("tCO2e");
//        catTabletCo2Cell.setCellStyle(tabHeaderStyle);
//
//        Cell catTableCo2Cell = catTableHeaderRow.createCell(3);
//        catTableCo2Cell.setCellValue("CO2");
//        catTableCo2Cell.setCellStyle(tabHeaderStyle);
//
//
//        Cell catTableCH4Cell = catTableHeaderRow.createCell(3);
//        catTableCH4Cell.setCellValue("CH4");
//        catTableCH4Cell.setCellStyle(tabHeaderStyle);
//
//
//        Cell catTableN2OCell = catTableHeaderRow.createCell(3);
//        catTableN2OCell.setCellValue("N2O");
//        catTableN2OCell.setCellStyle(tabHeaderStyle);
//
//        rowIndex++;
//
//
//        Iterator it = catDataMap.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry pair = (Map.Entry) it.next();
//
//            String catName = (String) pair.getKey();
//            List<List<String>> emissionsSources = (List<List<String>>) pair.getValue();
//            int rowMerges = emissionsSources.size();
//            colIndex = 0;
//
//            Row catRow = sheet.createRow(rowIndex);
//
////            sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + rowMerges, colIndex, 0));
//
//
//            Cell catCell = catRow.createCell(0);
//            catTableTitleCell.setCellValue(catName);
//
//
//            for (int index = 0; index < emissionsSources.size(); index++) {
//                List<String> details = emissionsSources.get(index);
//                Row srcRow = catRow;
//                if (index != 0) {
//                    srcRow = sheet.createRow(rowIndex);
//                }
//                colIndex = 1;
//                for (String value : details) {
//                    Cell srcValCell = srcRow.createCell(colIndex);
//                    srcValCell.setCellValue(value);
//
//                    colIndex++;
//
//                }
//
//
////                it.remove(); // avoids a ConcurrentModificationException
//            }
//        }


            //***********************************************************************************

//        headerCell = header.createCell(1);
//        headerCell.setCellValue("Age");

//        headerCell.setCellStyle(headerStyle);


            try {
                tempFile = File.createTempFile(comName.trim(), ".xlsx");

//            tempFile.deleteOnExit();
                try {
                    OutputStream stream = new FileOutputStream(tempFile);
                    wb.write(stream);
                } catch (IOException e) {
                    System.out.println(e);
                }
            } catch (IOException e) {
                System.out.println(e);
            }

//File tempFile = File.createTempFile("MyAppName-", ".tmp");


            return tempFile != null ? tempFile.getPath() : "";
        }



}

