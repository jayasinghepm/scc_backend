package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteTransportEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.WasteTransportEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class WasteTransportPopulator {

    private static Logger logger = Logger.getLogger(WasteTransportPopulator.class);
    private static WasteTransportPopulator instance = null;

    public static WasteTransportPopulator getInstance() {
        if (instance == null) {
            instance = new WasteTransportPopulator();
        }
        return instance;
    }

    public WasteTransportEntryDTO populateDTO(WasteTransportEntry entry) {
        WasteTransportEntryDTO dto = new WasteTransportEntryDTO();
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setEntryId(entry.getEntryId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setWasteype(entry.getWasteType());
        dto.setSubContractorName(entry.getSubContractorName());
        dto.setFuelType(entry.getFuelType());
        dto.setVehicleType(entry.getVehicleType());
        dto.setFuelEconomy(entry.getFuelEconomy());
        dto.setLoadingCapacity(entry.getLoadingCapacity());
        dto.setNoOfTurns(entry.getNoOfTurns());//edit by pasindu
        dto.setDistanceTravelled(entry.getDistanceTravelled());
        dto.setWasteTons(entry.getWasteTons());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setFuel(entry.getFuel());
        dto.setVehicle(entry.getVehicle());
        dto.setWaste(entry.getWaste());
        dto.setNoOfTurns(entry.getNoOfTurns());
        return dto;
    }

    public WasteTransportEntry populate(WasteTransportEntry entry, WasteTransportEntryDTO dto) {
        try {
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }
            }
//            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId() == 0 ? entry.getBranchId() : dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId() == 0 ? entry.getCompanyId() : dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setWasteype(dto.getWasteType());
            entry.setSubContractorName(dto.getSubContractorName());
            entry.setFuelType(dto.getFuelType());
            entry.setVehicleType(dto.getVehicleType());
            entry.setFuelEconomy(dto.getFuelEconomy());
            entry.setLoadingCapacity(dto.getLoadingCapacity());
            entry.setNoOfTurns(dto.getNoOfTurns());//edit by pasindu
            entry.setDistanceTravelled(dto.getDistanceTravelled());
            entry.setWasteTons(dto.getWasteTons());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
            entry.setFuel(dto.getFuel() == null ? entry.getFuel() : dto.getFuel());
            entry.setVehicle(dto.getVehicle() == null ? entry.getVehicle() : dto.getVehicle());
            entry.setWaste(dto.getWaste() == null ? entry.getWaste() : dto.getWaste());
            entry.setNoOfTurns(dto.getNoOfTurns());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;

    }

}
