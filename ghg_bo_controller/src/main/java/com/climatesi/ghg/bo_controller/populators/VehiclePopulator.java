package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.VehicleEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.VehicleEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class VehiclePopulator {

    private static Logger logger = Logger.getLogger(VehiclePopulator.class);
    private static VehiclePopulator instance;

    public static VehiclePopulator getInstance() {
        if (instance == null) {
            instance = new VehiclePopulator();
        }
        return instance;
    }

    public VehicleEntryDTO populateDTO(VehicleEntry entry) {
        VehicleEntryDTO dto = new VehicleEntryDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setFuelType(entry.getFuelType());
        dto.setUnits(entry.getUnits());
        dto.setConsumption(entry.getConsumption());
        dto.setVehiclModel(entry.getVehicleModel());
        dto.setVehicleCategory(entry.getVehicleCategory());
        dto.setVehicleNumber(entry.getVehicleNumber());
        dto.setIsFuelPaidbyCompany(entry.isFuelPaidbyCompany());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setModel(entry.getModel());
        dto.setCategory(entry.getCategory());
        dto.setFuel(entry.getFuel());
        dto.setFuelEconomy(entry.getFuelEconomy());
        dto.setDistance(entry.getDistance());
        return dto;
    }

    public VehicleEntry populate(VehicleEntry entry, VehicleEntryDTO dto) {

        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }
            }
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setFuelType(dto.getFuelType());
            entry.setUnits(dto.getUnits());
            entry.setConsumption(dto.getConsumption());
            entry.setVehiclModel(dto.getVehicleModel());
            entry.setVehicleCategory(dto.getVehicleCategory());
            entry.setVehicleNumber(dto.getVehicleNumber());
            entry.setIsFuelPaidbyCompany(dto.isFuelPaidbyCompany());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
            entry.setFuel(dto.getFuel() == null ? entry.getFuel() : dto.getFuel());
            entry.setCategory(dto.getCategory() == null ? entry.getCategory() : dto.getCategory());
            entry.setModel(dto.getModel() == null ? entry.getModel() : dto.getModel());
            entry.setDistance(dto.getDistance());
            entry.setFuelEconomy(dto.getFuelEconomy());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }

}
