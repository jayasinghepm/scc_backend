package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.AshTransportationDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.AshTransportationDTO;
import com.climatesi.ghg.emission_source.api.beans.AshTransportation;
import com.climatesi.ghg.emission_source.api.beans.AshTransportation;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class AshTransportationPopulator {

    private static Logger logger = Logger.getLogger(AshTransportationPopulator.class);
    private static AshTransportationPopulator instance;



    public static AshTransportationPopulator getInstance() {
        if (instance == null) {
            instance = new AshTransportationPopulator();
        }
        return instance;
    }

    public AshTransportationDTO populateDTO(AshTransportation entry) {
        AshTransportationDTO dto = new AshTransportationDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());

        dto.setFuelEconomy(entry.getFuelEconomy());
        dto.setDistance(entry.getDistance());
        dto.setFuleType(entry.getFuleType());//pasindu


        dto.setFuelConsumption(entry.getFuelConsumption());
        dto.setNoOfTrips(entry.getNoOfTrips());


        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public AshTransportation populate(AshTransportation entry, AshTransportationDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(dto.getIsDeleted());
                    return entry;
                }
            }

            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());

            entry.setFuelEconomy(dto.getFuelEconomy());
            entry.setFuleType(dto.getFuleType());//pasindu
            entry.setDistance(dto.getDistance());

            entry.setFuelConsumption(dto.getFuelConsumption());
            entry.setNoOfTrips(dto.getNoOfTrips());

            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
