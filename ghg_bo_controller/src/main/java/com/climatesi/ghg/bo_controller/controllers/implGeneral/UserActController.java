package com.climatesi.ghg.bo_controller.controllers.implGeneral;

import com.climatesi.ghg.api.UserFactory;
import com.climatesi.ghg.api.beans.UserActivityLog;
import com.climatesi.ghg.api.facades.UserActLogManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

@Stateless
public class UserActController extends AbstractController {
    private static Logger logger = Logger.getLogger(UserActLogManager.class);
    private UserActLogManager userActLogManager ;

    @PostConstruct
    @Override
    public void initialize() {
        try {
            userActLogManager = UserFactory.getInstance().getUserActLogManager(em);
        } catch (GHGException e) {
           logger.error(e);
        }
    }

    public ListResult<UserActivityLog> getFilteredLogs(int pageNum, Object sort, Object filter) {
        return userActLogManager.getPaginatedEntityListByFilter(pageNum, sort, filter);
    }

    public UserActivityLog addActLog(UserActivityLog log) throws GHGException {
        String status;
        try {
            log.setLogTime(new Date());
            status = (String) userActLogManager.addEntity(log);
        }catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }
        return log;
    }
}
