package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.StaffTransportationDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.StaffTransportationDTO;
import com.climatesi.ghg.emission_source.api.beans.StaffTransportation;
import com.climatesi.ghg.emission_source.api.beans.StaffTransportation;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class StaffTransportationPopulator {

    private static Logger logger = Logger.getLogger(StaffTransportationPopulator.class);
    private static StaffTransportationPopulator instance;



    public static StaffTransportationPopulator getInstance() {
        if (instance == null) {
            instance = new StaffTransportationPopulator();
        }
        return instance;
    }

    public StaffTransportationDTO populateDTO(StaffTransportation entry) {
        StaffTransportationDTO dto = new StaffTransportationDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());

        dto.setNoOfTrips(entry.getNoOfTrips());
        dto.setFuelConsumption(entry.getFuelConsumption());

        dto.setFuelEconomy(entry.getFuelEconomy());
        dto.setDistance(entry.getDistance());
        dto.setFuelType(entry.getFuelType());

        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public StaffTransportation populate(StaffTransportation entry, StaffTransportationDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(dto.getIsDeleted());
                    return entry;
                }
            }

            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());

            entry.setNoOfTrips(dto.getNoOfTrips());
            entry.setFuelConsumption(dto.getFuelConsumption());

            entry.setFuelEconomy(dto.getFuelEconomy());
            entry.setDistance(dto.getDistance());
            entry.setFuelType(dto.getFuelType());

            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
