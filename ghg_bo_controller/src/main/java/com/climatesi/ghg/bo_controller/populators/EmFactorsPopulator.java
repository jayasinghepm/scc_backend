package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.EmFactorDTO;
import org.jboss.logging.Logger;

public class EmFactorsPopulator {

    private static EmFactorsPopulator instance;
    private static Logger logger = Logger.getLogger(EmFactorsPopulator.class);

    private EmFactorsPopulator() {

    }

    public static EmFactorsPopulator getInstance() {
        if (instance == null) {
            instance = new EmFactorsPopulator();
        }
        return instance;
    }

    public EmFactorDTO populateDTO(EmissionFactors factors) {
        EmFactorDTO dto = new EmFactorDTO();

        try {
            dto.setId(factors.getId());
            dto.setDescription(factors.getDescription());
            dto.setValue(factors.getValue());
            dto.setReference(factors.getReference());
        } catch (NullPointerException e) {
            logger.error("Null pointer");
        }
        return dto;
    }

    public EmissionFactors populate(EmissionFactors factors, EmFactorDTO dto) {
        try {
            factors.setId(dto.getId());
            factors.setDescription(dto.getDescription());
            factors.setReference(dto.getReference());
            factors.setValue(dto.getValue());
        } catch (NullPointerException e) {
            logger.error("Null pointer");
        }
        return factors;
    }
}
