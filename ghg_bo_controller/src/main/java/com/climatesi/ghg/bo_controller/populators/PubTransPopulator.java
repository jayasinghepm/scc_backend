package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.PubTransEmFactorDTO;
import org.jboss.logging.Logger;

public class PubTransPopulator {

    private static PubTransPopulator instance;
    private static Logger logger = Logger.getLogger(PubTransPopulator.class);

    private PubTransPopulator() {

    }

    public static PubTransPopulator getInstance() {
        if (instance == null) {
            instance = new PubTransPopulator();
        }
        return instance;
    }

    public PubTransEmFactorDTO populateDTO(PublicTransposrtEmisssionFactor factor) {
        PubTransEmFactorDTO dto = new PubTransEmFactorDTO();
        try {

            dto.setVehicleType(factor.getVehicleType());
            dto.setVehicleName(factor.getVehicleName());
            dto.setG_per_km(factor.getG_per_km());
            dto.setCo2_per_km(factor.getCo2_per_km());
            dto.setAssumption(factor.getAssumption());
        } catch (NullPointerException e) {
            logger.error("Null pointer");
        }
        return dto;
    }

    public PublicTransposrtEmisssionFactor populate(PublicTransposrtEmisssionFactor factor, PubTransEmFactorDTO dto) {

        try {
            factor.setVehicleType(dto.getVehicleType());
            factor.setVehicleName(dto.getVehicleName());
            factor.setG_per_km(dto.getG_per_km());
            factor.setCo2_per_km(dto.getCo2_per_km());
            factor.setAssumption(dto.getAssumption());
        } catch (NullPointerException e) {
            logger.error("Null pointer");
        }
        return factor;
    }
}
