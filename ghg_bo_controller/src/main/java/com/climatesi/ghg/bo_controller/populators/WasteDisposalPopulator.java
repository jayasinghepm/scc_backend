package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteDisposalEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.WasteDisposalEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class WasteDisposalPopulator {

    private static Logger logger = Logger.getLogger(WasteDisposalPopulator.class);
    private static WasteDisposalPopulator instance = null;

    public static WasteDisposalPopulator getInstance() {
        if (instance == null) {
            instance = new WasteDisposalPopulator();
        }
        return instance;
    }

    public WasteDisposalEntryDTO populateDTO(WasteDisposalEntry entry) {
        WasteDisposalEntryDTO dto = new WasteDisposalEntryDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setUnits(entry.getUnits());
        dto.setAmountDisposed(entry.getAmountDisposed());
        dto.setDisposalMethod(entry.getDisposalMethod());
        dto.setWasteype(entry.getWasteType());
        dto.setSubContractorName(entry.getSubContractorName());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setDisposal(entry.getDisposal());
        dto.setWaste(entry.getWaste());
        return dto;
    }

    public WasteDisposalEntry populate(WasteDisposalEntry entry, WasteDisposalEntryDTO dto) {

        try {
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.isDeleted() == 2) {
                    entry.setDeleted(1);
                    return entry;
                }
            }
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setUnits(dto.getUnits());
            entry.setAmountDisposed(dto.getAmountDisposed());
            entry.setDisposalMethod(dto.getDisposalMethod());
            entry.setWasteype(dto.getWasteType());
            entry.setSubContractorName(dto.getSubContractorName());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
            entry.setDisposal(dto.getDisposal() == null ? entry.getDisposal() : dto.getDisposal());
            entry.setWaste(dto.getWaste() == null ? entry.getWaste() : dto.getWaste());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }
        return entry;
    }
}
