package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_calc;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.api.facades.BranchManager;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.*;
import com.climatesi.ghg.emission_source.api.enums.FireExtinguisherType;
import com.climatesi.ghg.emission_source.api.enums.RefrigerantTypes;
import com.climatesi.ghg.emission_source.api.enums.VehicleCategory;
import com.climatesi.ghg.emission_source.api.enums.VehicleTypes;
import com.climatesi.ghg.emission_source.api.facades.*;
import com.climatesi.ghg.emission_source.implGeneral.facades.SeaAirFreightFacadeManager;
import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.enums.*;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.transaction.Transactional;
import java.awt.geom.FlatteningPathIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Float.NaN;

@Stateless
public class EmissionController extends AbstractController {

    private static Logger logger = Logger.getLogger(EmissionController.class);
    private MunicipalWaterEntryManager municipalWaterEntryManager;
    private ElectricityEntryManager electricityEntryManager;
    private GeneratorsEntryManager generatorsEntryManager;
    private RefrigerantsEntryManager refrigerantsEntryManager;
    private FireExtingEntryManager fireExtingEntryManager;
    private WasteTransportEntryManager wasteTransportEntryManager;
    private EmployeCommutingEntryManager employeCommutingEntryManager;
    private VehicleEntryManager vehicleEntryManager;
    private WasteDisposalEntryManager wasteDisposalEntryManager;
    private TransportLocalEntryManager transportLocalEntryManager;
    private BusinessAirTravelEntryManager businessAirTravelEntryManager;
    private CompanyManager companyManager;
    private BranchManager branchManager;
    private EmissionFactorsManager emissionFactorsManager;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;
    private SeaAirFreightFacadeManager seaAirFreightFacadeManager;
    private BioMassEntryManager bioMassEntryManager;
    private LPGEntryEntryManager lpgEntryEntryManager;
    private BGASEntryEntryManager bgasEntryEntryManager;

    private AshTransportationEntryManager ashTransportationEntryManager;
    private OilAndGasTransportationEntryManager oilAndGasTransportationEntryManager;
    private LorryTransportationManager lorryTransportationManager;
    private ForkliftsEntryManager forkliftsEntryManager;
    private FurnaceEntryManager furnaceEntryManager;
    private PaperWasteManager paperWasteManager;
    private PaidManagerVehiclesEntryManager paidManagerVehiclesEntryManager;
    private RawMaterialTransportationLocalManager rawMaterialTransportationLocalManager;
    private SawDustTransportationManager sawDustTransportationManager;
    private SludgeTransportationManager sludgeTransportationManager;
    private VehicleOthersEntryManager vehicleOthersEntryManager;
    private StaffTransportationEntryManager staffTransportationEntryManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            transportLocalEntryManager = EmsissionSourceFactory.getInstance().getTransportLocalEntryManager(em);
            municipalWaterEntryManager = EmsissionSourceFactory.getInstance().getMunicipalWaterEntryManager(em);
            electricityEntryManager = EmsissionSourceFactory.getInstance().getElectricityEntryManager(em);
            generatorsEntryManager = EmsissionSourceFactory.getInstance().getGeneratorsEntryManager(em);
            refrigerantsEntryManager = EmsissionSourceFactory.getInstance().getRefrigerantsEntryManager(em);
            fireExtingEntryManager = EmsissionSourceFactory.getInstance().getFireExtingEntryManager(em);
            wasteTransportEntryManager = EmsissionSourceFactory.getInstance().getWasteTransportEntryManager(em);
            employeCommutingEntryManager = EmsissionSourceFactory.getInstance().getEmployeeCommEntryManager(em);
            vehicleEntryManager = EmsissionSourceFactory.getInstance().getVehicleEntryManager(em);
            wasteDisposalEntryManager = EmsissionSourceFactory.getInstance().getWasteDisposalEntryManager(em);
            businessAirTravelEntryManager = EmsissionSourceFactory.getInstance().getBusinessAirTravelEntryManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            seaAirFreightFacadeManager = EmsissionSourceFactory.getInstance().getSeaAirFreightFacadeManager(em);
            bioMassEntryManager = EmsissionSourceFactory.getInstance().getBioMassEntryManager(em);
            lpgEntryEntryManager = EmsissionSourceFactory.getInstance().getLPGEntryEntryManager(em);
            bgasEntryEntryManager = EmsissionSourceFactory.getInstance().getBGASEntryEntryManager(em);
            ashTransportationEntryManager = EmsissionSourceFactory.getInstance().getAshTransportationEntryManager(em);
            forkliftsEntryManager = EmsissionSourceFactory.getInstance().getForkliftsEntryManager(em);
            furnaceEntryManager = EmsissionSourceFactory.getInstance().getFurnaceEntryManager(em);
            lorryTransportationManager = EmsissionSourceFactory.getInstance().getLorryTransportationManager(em);
            paidManagerVehiclesEntryManager = EmsissionSourceFactory.getInstance().getPaidManagerVehiclesEntryManager(em);
            paperWasteManager = EmsissionSourceFactory.getInstance().getPaperWasteManager(em);
            rawMaterialTransportationLocalManager = EmsissionSourceFactory.getInstance().getRawMaterialTransportationLocalManager(em);
            sawDustTransportationManager = EmsissionSourceFactory.getInstance().getSawDustTransportationManager(em);
            sludgeTransportationManager = EmsissionSourceFactory.getInstance().getSludgeTransportationManager(em);
            vehicleOthersEntryManager = EmsissionSourceFactory.getInstance().getVehicleOthersEntryManager(em);
            staffTransportationEntryManager = EmsissionSourceFactory.getInstance().getStaffTransportationEntryManager(em);
            oilAndGasTransportationEntryManager = EmsissionSourceFactory.getInstance().getOilAndGasTransportationEntryManager(em);

        } catch (Exception e) {
            logger.error("Error in intializing Emission Calc Controller", e);
        }
    }




    public ResultDTO emissionAshTrans(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float m3 = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<AshTransportation> list = ashTransportationEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (AshTransportation o : list) {
            if (o != null) {
                m3 = (o.getFuelEconomy() == 0 ? 0 : o.getDistance() / o.getFuelEconomy())*  0.001f;
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = m3;
        return dto;
    }

    public ResultDTO emissionForkliftsPetrol(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<Forklifts> list = forkliftsEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (Forklifts o : list) {
            if (o != null && o.getFuleType() == FuelTypes.Petrol.getFuel()) {
                liters += o.getFuelConsumption();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionForkliftsDiesel(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<Forklifts> list = forkliftsEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (Forklifts o : list) {
            if (o != null && o.getFuleType() == FuelTypes.Diesel.getFuel()) {
                liters += o.getFuelConsumption();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionFurnaceOil(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<FurnaceOil> list = furnaceEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (FurnaceOil o : list) {
            if (o != null) {
                liters += o.getFuelConsumption();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionLorryTransInternal(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<LorryTransportation> list = lorryTransportationManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (LorryTransportation o : list) {
            if (o != null && o.getTransportMode() == LorryTransModes.Internal.getMode()) {
                liters += o.getFuelEconomy() > 0 ? (o.getDistance()/o.getFuelEconomy()) : 0;
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionLorryTransExternal(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<LorryTransportation> list = lorryTransportationManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (LorryTransportation o : list) {
            if (o != null && o.getTransportMode() == LorryTransModes.External.getMode()) {
                liters += o.getFuelEconomy() == 0 ? 0 :  o.getDistance()/o.getFuelEconomy();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionPaidManagerVehicle(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float p_liters = 0;
        float d_liters = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<PaidManagerVehicles> list = paidManagerVehiclesEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (PaidManagerVehicles o : list) {
            if (o != null) {
                if (o.getFuleType() == FuelTypes.Petrol.getFuel()) {
                    p_liters  += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy()) ;
                }else if (o.getFuleType() == FuelTypes.Diesel.getFuel()) {
                    d_liters += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy());
                }
                liters += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy());
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        dto.petrol = p_liters/1000;
        dto.diesel = p_liters/1000;
        return dto;
    }

    public ResultDTO emissionPaperWaste(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float kg = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<PaperWaste> list =  paperWasteManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (PaperWaste o : list) {
            if (o != null) {
                kg += o.getQuantity();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.tons = kg/1000;
        return dto;
    }

    public ResultDTO emissionOilAndGasTrans(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<OilAndGasTransportation> list = oilAndGasTransportationEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (OilAndGasTransportation o : list) {
            if (o != null) {
                liters += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy());
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionRawMatTrans(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<RawMaterialTransporationLocal> list = rawMaterialTransportationLocalManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (RawMaterialTransporationLocal o : list) {
            if (o != null) {
                liters += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy());
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionSawDustTrans(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<SawDustTransportation> list = sawDustTransportationManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (SawDustTransportation o : list) {
            if (o != null) {
                liters += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy());
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionStaffTrans(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<StaffTransportation> list = staffTransportationEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (StaffTransportation o : list) {
            if (o != null) {
                liters += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy());
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionSludgeTrans(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<SludgeTransportation> list = sludgeTransportationManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (SludgeTransportation o : list) {
            if (o != null) {
                liters += (o.getFuelEconomy() == 0 ? 0 : o.getDistance()/o.getFuelEconomy()) * o.getNoOfTrips();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }

    public ResultDTO emissionVehicleOthers(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float liters = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<VehicleOthers> list = vehicleOthersEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleOthers o : list) {
            if (o != null) {
                if (o.getFuelEconomy() > 0) {
                    liters += (o.getDistance() / o.getFuelEconomy());
                    emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                    co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                    n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                    ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                }
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.m3 = liters/1000;
        return dto;
    }


    public ResultDTO emissionLPGas(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float kg = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<LPGEntry> list = lpgEntryEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (LPGEntry o : list) {
            if (o != null) {
                kg += o.getConsumption();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.tons = kg/1000;
        return dto;
    }
    //bgas
    public ResultDTO emissionBGas(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float kg = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<BGASEntry> list = bgasEntryEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (BGASEntry o : list) {
            if (o != null) {
                kg += o.getConsumption();
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.tons = kg/1000;
        return dto;
    }

    public ResultDTO emissionBiomass(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;//
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float kg = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<BioMassEntry> list = bioMassEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (BioMassEntry o : list) {
            if (o != null) {
                kg += (o.getQuantity());
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.tons = kg/1000;
        return dto;
    }

    public ResultDTO emissionWasteTransport(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n2o = 0;
        float ch4 = 0;
        float km = 0;
        float diesel = 0;
        float petrol = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<WasteTransportEntry> list = wasteTransportEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (WasteTransportEntry o : list) {
            if (o != null) {
                if (o.getFuelEconomy() > 0) {
                    if (o.getFuelType() == com.climatesi.ghg.emission_source.api.enums.FuelTypes.Petrol.type) {
                        petrol += ((o.getWasteTons() / o.getLoadingCapacity()) * o.getDistanceTravelled() / o.getFuelEconomy());
                    }else if (o.getFuelType() == com.climatesi.ghg.emission_source.api.enums.FuelTypes.Diesel.type) {
                        diesel += ((o.getWasteTons() / o.getLoadingCapacity()) * o.getDistanceTravelled() / o.getFuelEconomy());
                    }
                    km += o.getDistanceTravelled();
                    emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                    co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                    n2o += Float.parseFloat(o.getEmissionDetails().get("n20"));
                    ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                }

            }
        }
        ResultDTO dto = new ResultDTO(emission, co2,ch4, n2o);
        dto.km = km;
        dto.petrol = petrol /1000;
        dto.diesel = diesel /1000;
        return dto;
    }

    public ResultDTO emissionWasteDisposal(int companyId, int branchId, int yearBegin, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float ch4 = 0;
        float n20 = 0;
        float tons = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<WasteDisposalEntry> list = wasteDisposalEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (WasteDisposalEntry o : list) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                tons += o.getAmountDisposed();
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2, ch4, n20);
        dto.tons = tons;
        return dto;
    }

    public ResultDTO emissionMunWater(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float ch4 = 0;
        float n2o = 0;
        float m3 = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<MunicipalWaterEntry> list = municipalWaterEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (MunicipalWaterEntry o : list) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                m3 += Float.parseFloat(o.getEmissionDetails().get("quantity"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, 0, 0, 0);
        dto.m3 = m3;
        return dto;
    }

    public ResultDTO emissionElectricity(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 =0;
        float ch4 = 0;
        float n20 = 0;
        float kwh = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<ElectricityEntry> list = electricityEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (ElectricityEntry o : list) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                kwh += o.getConsumption();
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2, ch4, n20);
        dto.kwh = kwh;
        return dto;
    }

    public ResultDTO emissionTandDLoss(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<ElectricityEntry> list = electricityEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (ElectricityEntry o : list) {
            if (o != null && o.getEmissionDetails() != null && o.getEmissionDetails().size() > 0) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2_t_d"));
            }else {
                if (o != null) {
                    logger.info("#####################    " + o.getEntryId() + "     ###############");
                }
            }
        }
        return new ResultDTO(emission, 0, 0, 0);
    }

    public ResultDTO emissionDieselGenerator(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20 = 0;
        float ch4 = 0;
        float diesel = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);

        List<GeneratorsEntry> list = generatorsEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (GeneratorsEntry o : list) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                diesel += o.getConsumption();
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2, ch4, n20 );
        dto.diesel = diesel;
        return dto;
    }

    public ResultDTO emissionAirFreight(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20 = 0;
        float ch4 = 0;
        float tons = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);

        List<SeaAirFreightEntry> list = seaAirFreightFacadeManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        List<SeaAirFreightEntry> filtered = list.stream().filter(entry -> entry.getIsAirFreight() == 2).collect(Collectors.toList());

        for (SeaAirFreightEntry o : filtered) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
//                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
//                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
//                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                tons += o.getFreightKg();
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2, ch4, n20 );
        dto.tons = tons;
        return dto;
    }


    public ResultDTO emissionSeaFreight(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20 = 0;
        float ch4 = 0;
        float tons = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);

        List<SeaAirFreightEntry> list = seaAirFreightFacadeManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        List<SeaAirFreightEntry> filtered = list.stream().filter(entry -> entry.getIsAirFreight() == 1).collect(Collectors.toList());

        for (SeaAirFreightEntry o : filtered) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
//                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
//                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
//                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                tons += o.getFreightKg();
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2, ch4, n20 );
        dto.tons = tons;
        return dto;
    }




    public ResultDTO emissionRefriLeak(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float r22 = 0;
        float r407c = 0;
        float r410a  = 0;

        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<RefrigerantsEntry> list = refrigerantsEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (RefrigerantsEntry o : list) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                if (o.getTypeofRefrigerent() == RefrigerantTypes.R22.type) {
                    r22 += o.getAmountRefilled();
                }else if(o.getTypeofRefrigerent() == RefrigerantTypes.R407C.type){
                    r407c += o.getAmountRefilled();
                }else if (o.getTypeofRefrigerent() == RefrigerantTypes.R410A.type){
                    r410a += o.getAmountRefilled();
                }
            }
        }
        ResultDTO dto =  new ResultDTO(emission, 0, 0,0);
        dto.r22 = r22;
        dto.r407c = r407c;
        dto.r410a = r410a;
        return dto;
    }

    public ResultDTO emissionFireExt(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float wgas = 0;
        float fire_co2 = 0;
        float dcp = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<FireExtingEntry> list = fireExtingEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (FireExtingEntry o : list) {
            if (o != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                if (o.getFireExtinguisherType() == 1) {
                    wgas += o.getWeight();
                }else if (o.getFireExtinguisherType() == 2) {
                    fire_co2 += o.getWeight();
                }else if (o.getFireExtinguisherType() == 3) {
                    dcp += o.getWeight();
                }
            }
        }
        ResultDTO dto = new ResultDTO(emission, 0, 0, 0);
        dto.fire_co2 = fire_co2;
        dto.dcp = dcp;
        dto.wgas = wgas;
        return dto ;
    }

    public ResultDTO emissionOffRoad(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float ch4 = 0;
        float n20 = 0;
        float diesel = 0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleEntry o : list) {
            if (o != null &&
                    (o.getVehicleModel() == VehicleTypes.AirportGroudSupportEquipment.type || o.getVehicleModel() == VehicleTypes.ForkLifts.type ||
                            o.getVehicleModel() == VehicleTypes.Chain_saws.type ||
                            o.getVehicleModel() == VehicleTypes.Agriculture_Tractors.type ||
                            o.getVehicleModel() == VehicleTypes.Other_Offroad.type)
            ) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));

            }
        }
        ResultDTO dto =new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionCompanyOwned(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20= 0;
        float ch4 = 0;
        float diesel = 0;
        float petrol = 0;

        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleEntry o : list) {
            if (o != null
                    && o.getVehicleCategory() == VehicleCategory.COMPANY_OWNED.category
                    && (o.getVehicleModel() != VehicleTypes.AirportGroudSupportEquipment.type &&
                    o.getVehicleModel() != VehicleTypes.ForkLifts.type &&
                    o.getVehicleModel() != VehicleTypes.Chain_saws.type &&
                    o.getVehicleModel() != VehicleTypes.Agriculture_Tractors.type &&
                    o.getVehicleModel() != VehicleTypes.Other_Offroad.type)) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));
            }
        }
        ResultDTO dto =new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionTransRented(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20= 0;
        float ch4 = 0;
        float diesel = 0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleEntry o : list) {
            if (o != null
                    && o.getVehicleCategory() == VehicleCategory.RENTED.category
                    && (o.getVehicleModel() != VehicleTypes.AirportGroudSupportEquipment.type &&
                    o.getVehicleModel() != VehicleTypes.ForkLifts.type &&
                    o.getVehicleModel() != VehicleTypes.Chain_saws.type &&
                    o.getVehicleModel() != VehicleTypes.Agriculture_Tractors.type &&
                    o.getVehicleModel() != VehicleTypes.Other_Offroad.type)) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));
            }
        }
        ResultDTO dto =new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionTransRentedPaid(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20= 0;
        float ch4 = 0;
        float diesel = 0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleEntry o : list) {
            if (o != null
                    && o.getVehicleCategory() == VehicleCategory.RENTED.category && o.isFuelPaidbyCompany()
                    && (o.getVehicleModel() != VehicleTypes.AirportGroudSupportEquipment.type &&
                    o.getVehicleModel() != VehicleTypes.ForkLifts.type &&
                    o.getVehicleModel() != VehicleTypes.Chain_saws.type &&
                    o.getVehicleModel() != VehicleTypes.Agriculture_Tractors.type &&
                    o.getVehicleModel() != VehicleTypes.Other_Offroad.type)) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));
            }
        }
        ResultDTO dto =new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionTransRentedNotPaid(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20= 0;
        float ch4 = 0;
        float diesel = 0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleEntry o : list) {
            if (o != null
                    && o.getVehicleCategory() == VehicleCategory.RENTED.category && !o.isFuelPaidbyCompany()
                    && (o.getVehicleModel() != VehicleTypes.AirportGroudSupportEquipment.type &&
                    o.getVehicleModel() != VehicleTypes.ForkLifts.type &&
                    o.getVehicleModel() != VehicleTypes.Chain_saws.type &&
                    o.getVehicleModel() != VehicleTypes.Agriculture_Tractors.type &&
                    o.getVehicleModel() != VehicleTypes.Other_Offroad.type)) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));
            }
        }
        ResultDTO dto =new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionTransHiredPaid(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float ch4 = 0;
        float n20 =0;
        float diesel = 0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleEntry o : list) {
            if (o != null
                    && o.getVehicleCategory() == VehicleCategory.HIRED.category
                    && o.isFuelPaidbyCompany()
                    && (o.getVehicleModel() != VehicleTypes.AirportGroudSupportEquipment.type &&
                    o.getVehicleModel() != VehicleTypes.ForkLifts.type &&
                    o.getVehicleModel() != VehicleTypes.Chain_saws.type &&
                    o.getVehicleModel() != VehicleTypes.Agriculture_Tractors.type &&
                    o.getVehicleModel() != VehicleTypes.Other_Offroad.type)) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));
            }
        }
        ResultDTO dto =new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionTransHiredNotPaid(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float ch4 = 0;
        float n20 =0;
        float diesel = 0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (VehicleEntry o : list) {
            if (o != null
                    && o.getVehicleCategory() == VehicleCategory.HIRED.category
                    && !o.isFuelPaidbyCompany()
                    && (o.getVehicleModel() != VehicleTypes.AirportGroudSupportEquipment.type &&
                    o.getVehicleModel() != VehicleTypes.ForkLifts.type &&
                    o.getVehicleModel() != VehicleTypes.Chain_saws.type &&
                    o.getVehicleModel() != VehicleTypes.Agriculture_Tractors.type &&
                    o.getVehicleModel() != VehicleTypes.Other_Offroad.type)) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));

            }
        }
        ResultDTO dto =new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionEmpCommNotPaid(int companyId, int branchId, int yearStart, int yearEnd) {
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        float km =0;
        float diesel =0;
        float petrol = 0;
        float emission = 0;
        List<EmpCommutingEntry> list = employeCommutingEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (EmpCommutingEntry o : list) {
            if (o != null && !o.isPaidByCom() && (o.getOwnDieselLiters() > 0  || o.getOwnPetrolLiters() > 0)) {
                if (Float.isNaN(Float.parseFloat(o.getEmissionDetails().get("tco2")))) {
                    logger.error("##############################################################");
                    logger.error(o.getEmpId());
                    logger.error("###################################################################");
                }
                emission += Float.isNaN(Float.parseFloat(o.getEmissionDetails().get("tco2"))) ? 0 : Float.parseFloat(o.getEmissionDetails().get("tco2"));
                km += Float.parseFloat(o.getEmissionDetails().get("km_public"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));


            }
        }
        ResultDTO dto = new ResultDTO(emission, 0, 0, 0);
        dto.km = km;
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionEmpCommPaid(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float km =0;
        float diesel =0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<EmpCommutingEntry> list = employeCommutingEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (EmpCommutingEntry o : list) {
            if (o != null && o.isPaidByCom() && (o.getCompanyPetrolLiters() > 0 || o.getCompanyDieselLiters() > 0)) {
                if ( Float.isNaN(Float.parseFloat(o.getEmissionDetails().get("tco2")))) {

                }
                emission +=  Float.isNaN(Float.parseFloat(o.getEmissionDetails().get("tco2"))) ? 0 : Float.parseFloat(o.getEmissionDetails().get("tco2"));
                km += Float.parseFloat(o.getEmissionDetails().get("km_public"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, 0, 0, 0);
        dto.km = km;
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    public ResultDTO emissionAirtravel(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float ch4 = 0;
        float n20 = 0;
        float km = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<BusinessAirTravelEntry> list = businessAirTravelEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (BusinessAirTravelEntry o: list) {
            if (o != null && o.getEmissionDetails() != null) {
                emission += Float.parseFloat(o.getEmissionDetails().get("tco2") == null? "0": (o.getEmissionDetails().get("tco2")));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2")== null? "0": (o.getEmissionDetails().get("co2")));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("ch4") == null ? "0": o.getEmissionDetails().get("ch4"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("n20") == null ? "0": o.getEmissionDetails().get("n20"));
                km += Float.parseFloat(o.getEmissionDetails().get("km") == null ? "0": o.getEmissionDetails().get("km"));
            }
        }
        ResultDTO dto= new ResultDTO(emission, co2, ch4, n20);
        dto.km = km;
        return dto;
    }

    public ResultDTO emissionTransportLocPurchased(int companyId, int branchId, int yearStart, int yearEnd) {
        float emission = 0;
        float co2 = 0;
        float n20 = 0;
        float ch4  = 0;
        float diesel =0;
        float petrol = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<TransportLocalEntry> list = transportLocalEntryManager.getCustomFiltered(branchId, companyId, fy);
        if (list== null || list.size() ==0) {
            new ResultDTO(0,0,0,0);
        }
        for (TransportLocalEntry o : list) {
            if (o != null) {

                emission += Float.parseFloat(o.getEmissionDetails().get("tco2"));
                co2 += Float.parseFloat(o.getEmissionDetails().get("co2"));
                ch4 += Float.parseFloat(o.getEmissionDetails().get("n20"));
                n20 += Float.parseFloat(o.getEmissionDetails().get("ch4"));
                diesel += Float.parseFloat(o.getEmissionDetails().get("diesel"));
                petrol += Float.parseFloat(o.getEmissionDetails().get("petrol"));
            }
        }
        ResultDTO dto = new ResultDTO(emission, co2, ch4, n20);
        dto.diesel = diesel;
        dto.petrol = petrol;
        return dto;
    }

    @Transactional
    public HashMap<String, String> emissionBranchWise(int companyId, int yearStart, int yearEnd) {
        HashMap<String, String> results = new HashMap<>();
        JsonObject object = new JsonObject();
        JsonObject obj = new JsonObject();
        obj.addProperty("type", 1);
        obj.addProperty("value", companyId);
        obj.addProperty("col", 1);
        object.add("companyId", obj);

        List<Branch> branches = branchManager.getPaginatedEntityListByFilter(-1, "", object).getList();
        for (Branch b : branches) {
            if (b != null) {
                HashMap<String, String> map = emission_tco2(companyId, b.getId(), yearStart, yearEnd);
                if (map != null && map.size() != 0) {
                    results.put(b.getName(), map.get("scope_1") + "-" + map.get("scope_2") + "-" + map.get("scope_3"));
                }

            }
        }
        return results;
    }



    public HashMap<String, String> emission_tco2_excluded(
            int companyId,
            int branchId,
            int yearStart,
            int yearEnd,
            JsonObject params
    ) {
        HashMap<String, String> results = new HashMap<>();
        float numEmps = 1;
        float revenue = 1;
        float prod = 0;
        if (companyId != -1) {
            Company company = companyManager.getEntityByKey(companyId);
            if (params.get("num_emps") != null) {
                numEmps = params.get("num_emps").getAsInt();
            }else {
                numEmps = company.getNoOfEmployees();
            }
            if (params.get("revenue") != null) {
                revenue = params.get("revenue").getAsFloat();
            }else {
                revenue = company.getAnnumRevFY();
            }
            if (params.get("prod_ton") != null && !params.get("prod_ton").getAsString().isEmpty()) {
                prod = params.get("prod_ton").getAsFloat();
            }

            logger.info("line 602: " + company);

        }
        if (branchId != -1) {//!= -1
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);


        ResultDTO e_waste_transport = emissionWasteTransport(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_t_d = emissionTandDLoss(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_waste_disposal = emissionWasteDisposal(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_mun_water = emissionMunWater(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_emp_comm_not_paid = emissionEmpCommNotPaid(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_emp_comm_paid = emissionEmpCommPaid(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_elec = emissionElectricity(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_company_owned = emissionCompanyOwned(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_offroad = emissionOffRoad(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_fire_ext = emissionFireExt(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_refri_leakage = emissionRefriLeak(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_diesel_gen = emissionDieselGenerator(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_transport_hired_paid = emissionTransHiredPaid(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_transport_hired_not_paid = emissionEmpCommNotPaid(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_transport_rented = emissionTransRented(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_transport_loca_pur = emissionTransportLocPurchased(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_airtravel = emissionAirtravel(companyId, branchId, yearStart, yearEnd); //
        ResultDTO e_seaFreight = emissionSeaFreight(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_airFreight = emissionAirFreight(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_bioMass = emissionBiomass(companyId, branchId, yearStart, yearEnd);

        ResultDTO e_lpgGas = emissionLPGas(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_bGas = emissionBGas(companyId, branchId, yearStart, yearEnd);


        ResultDTO e_ashTrans = emissionAshTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_forkliftsPetrol = emissionForkliftsPetrol(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_forkliftsDiesel = emissionForkliftsDiesel(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_furnaceOil = emissionFurnaceOil(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_lorryTransInternal = emissionLorryTransInternal(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_lorryTransExternal = emissionLorryTransExternal(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_paidManagerVehicle = emissionPaidManagerVehicle(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_paperWaste = emissionPaperWaste(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_rawMatTrans = emissionRawMatTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_sawDustTrans = emissionSawDustTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_sludgeTrans = emissionSludgeTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_vehicleOthers = emissionVehicleOthers(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_oil_gas_trans = emissionVehicleOthers(companyId, branchId, yearStart, yearEnd);

        if (e_waste_transport == null) {
            logger.info("line 630: null");
            e_waste_transport = new ResultDTO();
        }
        if (e_t_d == null) {
            logger.info("line 634: null");
            e_t_d = new ResultDTO();
        }
        if (e_waste_disposal == null) {
            logger.info("line 638: null");
            e_waste_disposal = new ResultDTO();
        }
        if (e_mun_water == null) {
            logger.info("line 642: null");
            e_mun_water = new ResultDTO();
        }
        if (e_emp_comm_not_paid == null) {
            logger.info("line 646: null");
            e_emp_comm_not_paid = new ResultDTO();
        }
        if (e_emp_comm_paid == null) {
            logger.info("line 649: null");
            e_emp_comm_paid = new ResultDTO();
        }
        if (e_elec == null) {
            logger.info("line 654: null");
            e_elec = new ResultDTO();
        }
        if (e_company_owned == null) {
            logger.info("line 658: null");
            e_company_owned = new ResultDTO();
        }
        if (e_offroad == null) {
            logger.info("line 662: null");
            e_offroad = new ResultDTO();
        }
        if (e_fire_ext == null) {
            logger.info("line 666: null");
            e_fire_ext = new ResultDTO();
        }
        if (e_refri_leakage == null) {
            logger.info("line 670: null");
            e_refri_leakage = new ResultDTO();
        }
        if (e_diesel_gen == null) {
            logger.info("line 674: null");
            e_diesel_gen = new ResultDTO();
        }
        if (e_transport_hired_paid == null) {
            logger.info("line 678: null");
            e_transport_hired_paid = new ResultDTO();
        }
        if (e_transport_hired_not_paid == null) {
            logger.info("line 681: null");
            e_transport_hired_not_paid = new ResultDTO();
        }
        if (e_transport_rented == null) {
            logger.info("line 686: null");
            e_transport_rented = new ResultDTO();
        }
        if (e_transport_loca_pur == null) {
            logger.info("line 690: null");
            e_transport_loca_pur = new ResultDTO();
        }
        if (e_airtravel == null) {
            logger.info("line 694: null");
            e_airtravel= new ResultDTO();
        }
        if (e_airFreight == null) {
            e_airFreight = new ResultDTO();
        }
        if (e_seaFreight == null) {
            e_seaFreight = new ResultDTO();
        }
        if (e_bioMass == null) {//edit by pasindu
            e_bioMass = new ResultDTO();
        }
        if (e_ashTrans == null) {
            e_ashTrans = new ResultDTO();
        }
        if (e_forkliftsPetrol == null) {
            e_forkliftsPetrol  = new ResultDTO();
        }
        if (e_forkliftsDiesel == null) {
            e_forkliftsDiesel  = new ResultDTO();
        }
        if (e_furnaceOil == null) {
            e_furnaceOil  = new ResultDTO();
        }
        if (e_lorryTransExternal == null) {
            e_lorryTransExternal  = new ResultDTO();
        }
        if (e_lorryTransInternal == null) {
            e_lorryTransInternal  = new ResultDTO();
        }
        if (e_oil_gas_trans == null) {
            e_oil_gas_trans  = new ResultDTO();
        }
        if (e_paidManagerVehicle == null) {
            e_paidManagerVehicle  = new ResultDTO();
        }
        if (e_paperWaste == null) {
            e_paperWaste  = new ResultDTO();
        }
        if (e_rawMatTrans == null) {
            e_rawMatTrans  = new ResultDTO();
        }
        if (e_sawDustTrans == null) {
            e_sawDustTrans  = new ResultDTO();
        }
        if (e_sludgeTrans == null) {
            e_sludgeTrans  = new ResultDTO();
        }
        if (e_vehicleOthers == null) {
            e_vehicleOthers  = new ResultDTO();
        }
        if (e_lpgGas == null) {
            e_lpgGas = new ResultDTO();
        }
        if (e_bGas == null) {
            e_bGas = new ResultDTO();
        }


        Company company = companyManager.getEntityByKey(companyId);
        if (company == null) return null;

        Map<Integer, AllowedEmissionSrcBean> allowedEmissionSrcBeanMap = company.getAllowedEmissionSources();

        float scope_1 = 0;
        float scope_2 = 0;
        float scope_3 = 0;
        float indirect = 0;
        float direct = 0;

        if (params.get("elec") != null && !params.get("elec").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Elec.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_elec.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_elec.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_elec.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_elec.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_elec.tco2e;
                }

                results.put("electricity", Float.toString(e_elec.tco2e));
            }

        }
        if (params.get("td") != null && !params.get("td").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TD.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_t_d.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_t_d.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_t_d.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_t_d.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_t_d.tco2e;
                }

                results.put("t_d", Float.toString(e_t_d.tco2e));
            }

        }
        if (params.get("mw") != null && !params.get("mw").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.MunWater.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_mun_water.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_mun_water.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_mun_water.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_mun_water.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_mun_water.tco2e;
                }

                results.put("mun_water", Float.toString(e_mun_water.tco2e));
            }

        }
        if (params.get("wasTrans") != null && !params.get("wasTrans").getAsBoolean()){
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteTrans.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_waste_transport.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_waste_transport.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_waste_transport.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_waste_transport.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_waste_transport.tco2e;
                }

                results.put("waste_transport", Float.toString(e_waste_transport.tco2e));
            }

        }
        if(params.get("wasteDis") != null && !params.get("wasteDis").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteDisposal.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_waste_disposal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_waste_disposal.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_waste_disposal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_waste_disposal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_waste_disposal.tco2e;
                }

                results.put("waste_disposal", Float.toString(e_waste_disposal.tco2e));
            }

        }
        if (params.get("gen") != null && !params.get("gen").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Generator.getSrcId())) {



                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_diesel_gen.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_diesel_gen.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_diesel_gen.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_diesel_gen.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_diesel_gen.tco2e;
                }

                results.put("diesel_generators", Float.toString(e_diesel_gen.tco2e));
            }

        }
        if (params.get("refri") != null && !params.get("refri").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Refri.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_refri_leakage.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_refri_leakage.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_refri_leakage.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 +=e_refri_leakage.tco2e ;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_refri_leakage.tco2e;
                }

                results.put("refri_leakage", Float.toString(e_refri_leakage.tco2e));
            }

        }
        if (params.get("fire") != null && !params.get("fire").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FireExt.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_fire_ext.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_fire_ext.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_fire_ext.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_fire_ext.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_fire_ext.tco2e;
                }

                results.put("fire_ext", Float.toString(e_fire_ext.tco2e));
            }
        }
        if (params.get("offRoad") != null && !params.get("offRoad").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OffRoad.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_offroad.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect +=e_offroad.tco2e ;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_offroad.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 +=e_offroad.tco2e ;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_offroad.tco2e;
                }

                results.put("offroad", Float.toString(e_offroad.tco2e));
            }

        }
        if (params.get("comOwn") != null && !params.get("comOwn").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ComOwned.getSrcId())) {

                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_company_owned.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_company_owned.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_company_owned.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_company_owned.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_company_owned.tco2e;
                }

                results.put("company_owned", Float.toString(e_company_owned.tco2e));
            }

        }
        if (params.get("hired_paid") != null && !params.get("hired_paid").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredPaid.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_transport_hired_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_transport_hired_paid.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_transport_hired_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_transport_hired_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_transport_hired_paid.tco2e;
                }

                results.put("transport_hired_paid", Float.toString(e_transport_hired_paid.tco2e));
            }

        }
        if (params.get("hired_notpaid") != null && !params.get("hired_notpaid").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_transport_hired_not_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_transport_hired_not_paid.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_transport_hired_not_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_transport_hired_not_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_transport_hired_not_paid.tco2e;
                }

                results.put("transport_hired_not_paid", Float.toString(e_transport_hired_not_paid.tco2e));
            }

        }
        if (params.get("rented") != null && !params.get("rented").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleRented.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_transport_rented.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_transport_rented.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_transport_rented.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_transport_rented.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_transport_rented.tco2e;
                }

                results.put("transport_rented", Float.toString(e_transport_rented.tco2e));
            }

        }
        if (params.get("empComm_paid") != null && !params.get("empComm_paid").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommPaid.getSrcId())) {

                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_emp_comm_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_emp_comm_paid.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_emp_comm_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_emp_comm_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_emp_comm_paid.tco2e;
                }

                results.put("emp_comm_paid", Float.toString(e_emp_comm_paid.tco2e));
            }


        }
        if (params.get("empComm_notpaid") != null && !params.get("empComm_notpaid").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommNotPaid.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_emp_comm_not_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_emp_comm_not_paid.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_emp_comm_not_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_emp_comm_not_paid.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_emp_comm_not_paid.tco2e;
                }

                results.put("emp_comm_not_paid", Float.toString(e_emp_comm_not_paid.tco2e));
            }

        }
        if (params.get("air") != null && !params.get("air").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirTravel.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_airtravel.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_airtravel.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_airtravel.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_airtravel.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_airtravel.tco2e;
                }

                results.put("air_travel", Float.toString(e_airtravel.tco2e));
            }
        }
        if (params.get("transLoc") != null && !params.get("transLoc").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TransLocPur.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_transport_loca_pur.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_transport_loca_pur.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_transport_loca_pur.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_transport_loca_pur.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_transport_loca_pur.tco2e;
                }

                results.put("transport_loc_pur", Float.toString(e_transport_loca_pur.tco2e));
            }
        }

        if (params.get("airFreight") != null && !params.get("airFreight").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirFreight.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_airFreight.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_airFreight.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_airFreight.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_airFreight.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_airFreight.tco2e;
                }

                results.put("air_freight", Float.toString(e_airFreight.tco2e));
            }
        }
        if (params.get("seaFreight") != null && !params.get("seaFreight").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SeaFreight.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_seaFreight.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_seaFreight.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_seaFreight.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_seaFreight.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_seaFreight.tco2e;
                }

                results.put("sea_freight", Float.toString(e_seaFreight.tco2e));
            }

        }
        if (params.get("bioMass") != null && !params.get("bioMass").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.BioMass.getSrcId())) {

                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.BioMass.getSrcId()).getDirect() == DirectCatEnum.None.getCat()) {
                    results.put("bio_mass", Float.toString(e_bioMass.tco2e));
                }

            }

        }
        if (params.get("lpgGas") != null && !params.get("lpgGas").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LPG.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_lpgGas.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_lpgGas.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_lpgGas.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_lpgGas.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_lpgGas.tco2e;
                }

                results.put("lpg", Float.toString(e_lpgGas.tco2e));
            }
        }
        //bgas
        if (params.get("bGas") != null && !params.get("bGas").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Bgas.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_bGas.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_bGas.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_bGas.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_bGas.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_bGas.tco2e;
                }

                results.put("bgas", Float.toString(e_bGas.tco2e));
            }
        }
        if (params.get("ashTrans") != null && !params.get("ashTrans").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AshTrans.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_ashTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_ashTrans.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_ashTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_ashTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_ashTrans.tco2e;
                }

                results.put("ash_trans", Float.toString(e_ashTrans.tco2e));
            }
        }
        if (params.get("forkliftsPetrol") != null && !params.get("forkliftsPetrol").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsPetrol.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_forkliftsPetrol.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_forkliftsPetrol.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_forkliftsPetrol.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_forkliftsPetrol.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_forkliftsPetrol.tco2e;
                }

                results.put("forklifts_petrol", Float.toString(e_forkliftsPetrol.tco2e));
            }
        }
        if (params.get("forkliftsDiesel") != null && !params.get("forkliftsDiesel").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsDiesel.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_forkliftsDiesel.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_forkliftsDiesel.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_forkliftsDiesel.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_forkliftsDiesel.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_forkliftsDiesel.tco2e;
                }

                results.put("forklifts_diesel", Float.toString(e_forkliftsDiesel.tco2e));
            }
        }
        if (params.get("furnaceOil") != null && !params.get("furnaceOil").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FurnaceOil.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_furnaceOil.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_furnaceOil.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_furnaceOil.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_furnaceOil.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_furnaceOil.tco2e;
                }

                results.put("furnace_oil", Float.toString(e_furnaceOil.tco2e));
            }
        }
        if (params.get("lorryTransInternal") != null && !params.get("lorryTransInternal").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransInternal.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_lorryTransInternal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_lorryTransInternal.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_lorryTransInternal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_lorryTransInternal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_lorryTransInternal.tco2e;
                }

                results.put("lorry_trans_internal", Float.toString(e_lorryTransInternal.tco2e));
            }
        }
        if (params.get("lorryTransExternal") != null && !params.get("lorryTransExternal").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransExternal.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_lorryTransExternal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_lorryTransExternal.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_lorryTransExternal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_lorryTransExternal.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_lorryTransExternal.tco2e;
                }

                results.put("lorry_trans_external", Float.toString(e_lorryTransExternal.tco2e));
            }
        }
        if (params.get("paidManagerVehicle") != null && !params.get("paidManagerVehicle").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaidManagerVehicle.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_paidManagerVehicle.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_paidManagerVehicle.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_paidManagerVehicle.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_paidManagerVehicle.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_paidManagerVehicle.tco2e;
                }

                results.put("paid_manager_vehicles", Float.toString(e_paidManagerVehicle.tco2e));
            }

        }
        if (params.get("paperWaste") != null && !params.get("paperWaste").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaperWaste.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_paperWaste.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_paperWaste.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_paperWaste.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_paperWaste.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_paperWaste.tco2e;
                }

                results.put("paper_waste", Float.toString(e_paperWaste.tco2e));
            }

        }
        if (params.get("rawMatTrans") != null && !params.get("rawMatTrans").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RawMatTrans.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_rawMatTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_rawMatTrans.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_rawMatTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_rawMatTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_rawMatTrans.tco2e;
                }

                results.put("raw_mat_trans", Float.toString(e_rawMatTrans.tco2e));
            }

        }
        if (params.get("sawDustTrans") != null && !params.get("sawDustTrans").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SawDustTrans.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_sawDustTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_sawDustTrans.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_sawDustTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_sawDustTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_sawDustTrans.tco2e;
                }

                results.put("saw_dust_trans", Float.toString(e_sawDustTrans.tco2e));
            }

        }

        if (params.get("sludgeTrans") != null && !params.get("sludgeTrans").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SludgeTrans.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_sludgeTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_sludgeTrans.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_sludgeTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_sludgeTrans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_sludgeTrans.tco2e;
                }

                results.put("sludge_trans", Float.toString(e_sludgeTrans.tco2e));
            }

        }
        if (params.get("vehicleOthers") != null && !params.get("vehicleOthers").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehiclesOthers.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_vehicleOthers.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_vehicleOthers.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_vehicleOthers.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_vehicleOthers.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_vehicleOthers.tco2e;
                }





                results.put("vehicle_others", Float.toString(e_vehicleOthers.tco2e));
            }

        }
        if (params.get("oil_gas_trans") != null && !params.get("oil_gas_trans").getAsBoolean()) {
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OilGasTrans.getSrcId())) {


                //direct
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    direct += e_oil_gas_trans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirect += e_oil_gas_trans.tco2e;
                }

                //scope
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                    scope_1 += e_oil_gas_trans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                    scope_2 += e_oil_gas_trans.tco2e;
                }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                    scope_3 += e_oil_gas_trans.tco2e;
                }

                results.put("oil_gas_trans", Float.toString(e_oil_gas_trans.tco2e));
            }


        }




//        float indirect = e_transport_hired_not_paid.tco2e +
//                e_elec.tco2e
//                + e_airtravel.tco2e +
//                e_emp_comm_not_paid.tco2e +
//                e_t_d.tco2e
//                + e_waste_disposal.tco2e
//                + e_mun_water.tco2e
//                + e_waste_transport.tco2e;
//        float direct = e_transport_hired_paid.tco2e
//                + e_transport_rented.tco2e
//                + e_transport_loca_pur.tco2e +
//                e_company_owned.tco2e
//                + e_diesel_gen.tco2e +
//                e_refri_leakage.tco2e +
//                e_fire_ext.tco2e +
//                e_offroad.tco2e +
//                e_emp_comm_paid.tco2e;

        logger.info("direct " + direct);
        logger.info("indirect " + indirect);
        float total = direct + indirect;
        logger.info("total: "+ total);
        float per_capita = (total / numEmps);
        float intensity = (total / revenue);
        float perProd =  prod == 0 ? 0 :  (total/prod);


        results.put("total", Float.toString(total));
        results.put("direct", Float.toString(direct));
        results.put("indirect", Float.toString(indirect));
        results.put("per_capita", Float.toString(per_capita));
        results.put("intensity", Float.toString(intensity));
//        results.put("waste_transport", Float.toString(e_waste_transport.tco2e));
//        results.put("t_d", Float.toString(e_t_d.tco2e));
//        results.put("waste_disposal", Float.toString(e_waste_disposal.tco2e));
//        results.put("mun_water", Float.toString(e_mun_water.tco2e));
//        results.put("emp_comm_not_paid", Float.toString(e_emp_comm_not_paid.tco2e));
//        results.put("emp_comm_paid", Float.toString(e_emp_comm_paid.tco2e));
//        results.put("air_travel", Float.toString(e_airtravel.tco2e));
//        results.put("electricity", Float.toString(e_elec.tco2e));
//        results.put("company_owned", Float.toString(e_company_owned.tco2e));
//        results.put("offroad", Float.toString(e_offroad.tco2e));
//        results.put("fire_ext", Float.toString(e_fire_ext.tco2e));
//        results.put("refri_leakage", Float.toString(e_refri_leakage.tco2e));
//        results.put("diesel_generators", Float.toString(e_diesel_gen.tco2e));
//        results.put("transport_loc_pur", Float.toString(e_transport_loca_pur.tco2e));
//        results.put("transport_hired_paid", Float.toString(e_transport_hired_paid.tco2e));
//        results.put("transport_hired_not_paid", Float.toString(e_transport_hired_not_paid.tco2e));
//        results.put("transport_rented", Float.toString(e_transport_rented.tco2e));


//        results.put("waste_transport_json", e_waste_transport.toJson().toString());
//        results.put("t_d_json", e_t_d.toJson().toString());
//        results.put("waste_disposal_json", e_waste_disposal.toJson().toString());
//        results.put("mun_water_json", e_mun_water.toJson().toString());
//        results.put("emp_comm_not_paid_json", e_emp_comm_not_paid.toJson().toString());
//        results.put("emp_comm_paid_json", e_emp_comm_paid.toJson().toString());
//        results.put("air_travel_json", e_airtravel.toJson().toString());
//        results.put("electricity_json", e_elec.toJson().toString());
//        results.put("company_owned_json",  e_company_owned.toJson().toString());
//        results.put("offroad_json", e_offroad.toJson().toString());
//        results.put("fire_ext_json", e_fire_ext.toJson().toString());
//        results.put("refri_leakage_json", e_refri_leakage.toJson().toString());
//        results.put("diesel_generators_json", e_diesel_gen.toJson().toString());
//        results.put("transport_loc_pur_json", e_transport_loca_pur.toJson().toString());
//        results.put("transport_hired_paid_json", e_transport_hired_paid.toJson().toString());
//        results.put("transport_hired_not_paid_json", e_transport_hired_not_paid.toJson().toString());
//        results.put("transport_rented_json", e_transport_rented.toJson().toString());
        results.put("scope_1", Float.toString(scope_1));
        results.put("scope_2", Float.toString(scope_2));
        results.put("scope_3", Float.toString(scope_3));
        results.put("fy", fy);
        results.put("prod", Float.toString(perProd));
        return results;
    }



    public HashMap<String, String> emission_tco2(int companyId, int branchId, int yearStart, int yearEnd) {
        HashMap<String, String> results = new HashMap<>();
        float numEmps = 1;
        float revenue = 1;
        if (companyId != -1) {
            Company company = companyManager.getEntityByKey(companyId);
            numEmps = company.getNoOfEmployees();
            revenue = company.getAnnumRevFY();

        }
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);


        ResultDTO e_waste_transport = emissionWasteTransport(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_t_d = emissionTandDLoss(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_waste_disposal = emissionWasteDisposal(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_mun_water = emissionMunWater(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_emp_comm_not_paid = emissionEmpCommNotPaid(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_emp_comm_paid = emissionEmpCommPaid(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_elec = emissionElectricity(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_company_owned = emissionCompanyOwned(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_offroad = emissionOffRoad(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_fire_ext = emissionFireExt(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_refri_leakage = emissionRefriLeak(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_diesel_gen = emissionDieselGenerator(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_transport_hired_paid = emissionTransHiredPaid(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_transport_hired_not_paid = emissionTransHiredNotPaid(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_transport_rented = emissionTransRented(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_transport_rented_paid = emissionTransRentedPaid(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_transport_rented_not_paid = emissionTransRentedNotPaid(companyId, branchId, yearStart, yearEnd);

        ResultDTO e_transport_loca_pur = emissionTransportLocPurchased(companyId, branchId, yearStart, yearEnd);//
        ResultDTO e_airtravel = emissionAirtravel(companyId, branchId, yearStart, yearEnd); //
        ResultDTO e_seaFreight = emissionSeaFreight(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_airFreight = emissionAirFreight(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_bioMass = emissionBiomass(companyId, branchId, yearStart, yearEnd);

        ResultDTO e_lpgGas = emissionLPGas(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_bGas = emissionBGas(companyId, branchId, yearStart, yearEnd);

        ResultDTO e_ashTrans = emissionAshTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_forkliftsPetrol = emissionForkliftsPetrol(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_forkliftsDiesel = emissionForkliftsDiesel(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_furnaceOil = emissionFurnaceOil(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_lorryTransInternal = emissionLorryTransInternal(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_lorryTransExternal = emissionLorryTransExternal(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_paidManagerVehicle = emissionPaidManagerVehicle(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_paperWaste = emissionPaperWaste(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_rawMatTrans = emissionRawMatTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_sawDustTrans = emissionSawDustTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_staffTrans = emissionStaffTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_sludgeTrans = emissionSludgeTrans(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_vehicleOthers = emissionVehicleOthers(companyId, branchId, yearStart, yearEnd);
        ResultDTO e_oil_gas_trans = emissionOilAndGasTrans(companyId, branchId, yearStart, yearEnd);






        if (e_waste_transport == null && e_t_d == null && e_waste_disposal == null && e_mun_water == null &&
                e_emp_comm_not_paid == null && e_emp_comm_paid == null && e_elec == null && e_company_owned == null &&
                e_offroad == null && e_fire_ext == null && e_refri_leakage == null && e_diesel_gen == null &&
                e_transport_hired_not_paid == null && e_transport_hired_paid == null && e_transport_rented == null
                && e_transport_rented == null && e_transport_loca_pur == null && e_airtravel == null  &&
                e_airFreight == null && e_seaFreight == null && e_bioMass == null //edit by pasindu
                && e_ashTrans == null && e_forkliftsDiesel == null && e_forkliftsPetrol == null &&
                e_furnaceOil == null && e_lorryTransExternal == null && e_lorryTransInternal == null &&
                e_paidManagerVehicle == null && e_paperWaste == null && e_rawMatTrans == null &&
                e_sawDustTrans == null && e_sludgeTrans == null && e_vehicleOthers == null &&
                e_oil_gas_trans == null && e_staffTrans == null && e_transport_rented_not_paid == null &&
                e_transport_rented_paid == null
        ) {
            results.put("total", Float.toString(0));
            results.put("direct", Float.toString(0));
            results.put("indirect", Float.toString(0));
            results.put("per_capita", Float.toString(0));
            results.put("intensity", Float.toString(0));
            results.put("waste_transport", Float.toString(0));
            results.put("t_d", Float.toString(0));
            results.put("waste_disposal", Float.toString(0));
            results.put("mun_water", Float.toString(0));
            results.put("emp_comm_not_paid", Float.toString(0));
            results.put("emp_comm_paid", Float.toString(0));
            results.put("air_travel", Float.toString(0));
            results.put("electricity", Float.toString(0));
            results.put("company_owned", Float.toString(0));
            results.put("offroad", Float.toString(0));
            results.put("fire_ext", Float.toString(0));
            results.put("refri_leakage", Float.toString(0));
            results.put("diesel_generators", Float.toString(0));
            results.put("transport_loc_pur", Float.toString(0));
            results.put("transport_hired_paid", Float.toString(0));
            results.put("transport_hired_not_paid", Float.toString(0));
            results.put("transport_rented", Float.toString(0));
            results.put("transport_rented_paid", Float.toString(0));
            results.put("transport_rented_not_paid", Float.toString(0));
            results.put("sea_freight", Float.toString(0));
            results.put("ash_trans", Float.toString(0));
            results.put("bio_mass", Float.toString(0));//edit by pasindu

            results.put("forklifts_petrol", Float.toString(0));
            results.put("forklifts_diesel", Float.toString(0));
            results.put("furnace_oil", Float.toString(0));
            results.put("lorry_trans_internal", Float.toString(0));
            results.put("lorry_trans_external", Float.toString(0));
            results.put("paper_waste", Float.toString(0));
            results.put("paid_manager_vehicles", Float.toString(0));
            results.put("oil_gas_trans", Float.toString(0));
            results.put("raw_mat_trans", Float.toString(0));
            results.put("saw_dust_trans", Float.toString(0));
            results.put("sludge_trans", Float.toString(0));
            results.put("vehicle_others", Float.toString(0));
            results.put("staff_trans", Float.toString(0));




            results.put("waste_transport_json", "");
            results.put("t_d_json", "");
            results.put("waste_disposal_json", "");
            results.put("mun_water_json", "");
            results.put("emp_comm_not_paid_json", "");
            results.put("emp_comm_paid_json", "");
            results.put("air_travel_json", "");
            results.put("electricity_json", "");
            results.put("company_owned_json", "");
            results.put("offroad_json", "");
            results.put("fire_ext_json", "");
            results.put("refri_leakage_json", "");
            results.put("diesel_generators_json", "");
            results.put("transport_loc_pur_json", "");
            results.put("transport_hired_paid_json", "");
            results.put("transport_hired_not_paid_json", "");
            results.put("transport_rented_json", "");
            results.put("transport_rented_paid_json", "");
            results.put("transport_rented_not_paid_json", "");
            results.put("sea_freight_json", Float.toString(0));
            results.put("air_freight_json", Float.toString(0));
            results.put("bio_mass_json", Float.toString(0));//edit by pasindu
            results.put("ash_trans_json", Float.toString(0));
            results.put("forklifts_petrol_json", Float.toString(0));
            results.put("forklifts_diesel_json", Float.toString(0));
            results.put("furnace_oil_json", Float.toString(0));
            results.put("lorry_trans_internal_json", Float.toString(0));
            results.put("lorry_trans_external_json", Float.toString(0));
            results.put("paper_waste_json", Float.toString(0));
            results.put("paid_manager_vehicles_json", Float.toString(0));
            results.put("oil_gas_trans_json", Float.toString(0));
            results.put("raw_mat_trans_json", Float.toString(0));
            results.put("saw_dust_trans_json", Float.toString(0));
            results.put("staff_trans_json", Float.toString(0));
            results.put("sludge_trans_json", Float.toString(0));
            results.put("vehicle_others_json", Float.toString(0));
            results.put("scope_1", Float.toString(0));
            results.put("scope_2", Float.toString(0));
            results.put("scope_3", Float.toString(0));
            results.put("fy", fy);

            return results;
        }


        if (e_waste_transport == null) {
            e_waste_transport = new ResultDTO();
        }
        if (e_t_d == null) {
            e_t_d = new ResultDTO();
        }
        if (e_waste_disposal == null) {
            e_waste_disposal = new ResultDTO();
        }
        if (e_mun_water == null) {
            e_mun_water = new ResultDTO();
        }
        if (e_emp_comm_not_paid == null) {
            e_emp_comm_not_paid = new ResultDTO();
        }
        if (e_emp_comm_paid == null) {
            e_emp_comm_paid = new ResultDTO();
        }
        if (e_elec == null) {
            e_elec = new ResultDTO();
        }
        if (e_company_owned == null) {
            e_company_owned = new ResultDTO();
        }
        if (e_offroad == null) {
            e_offroad = new ResultDTO();
        }
        if (e_fire_ext == null) {
            e_fire_ext = new ResultDTO();
        }
        if (e_refri_leakage == null) {
            e_refri_leakage = new ResultDTO();
        }
        if (e_diesel_gen == null) {
            e_diesel_gen = new ResultDTO();
        }
        if (e_transport_hired_paid == null) {
            e_transport_hired_paid = new ResultDTO();
        }
        if (e_transport_hired_not_paid == null) {
            e_transport_hired_not_paid = new ResultDTO();
        }
        if (e_transport_rented == null) {
            e_transport_rented = new ResultDTO();
        }
        if (e_transport_loca_pur == null) {
            e_transport_loca_pur = new ResultDTO();
        }
        if (e_airtravel == null) {
            e_airtravel= new ResultDTO();
        }
        if (e_airFreight == null) {
            e_airFreight = new ResultDTO();
        }
        if (e_seaFreight == null) {
            e_seaFreight = new ResultDTO();
        }
        //--edit by pasindu-----
        if (e_bioMass == null) {
            e_bioMass = new ResultDTO();
        }

        if (e_ashTrans == null) {
            e_ashTrans = new ResultDTO();
        }
        if (e_forkliftsPetrol == null) {
            e_forkliftsPetrol  = new ResultDTO();
        }
        if (e_forkliftsDiesel == null) {
            e_forkliftsDiesel  = new ResultDTO();
        }
        if (e_furnaceOil == null) {
            e_furnaceOil  = new ResultDTO();
        }
        if (e_lorryTransExternal == null) {
            e_lorryTransExternal  = new ResultDTO();
        }
        if (e_lorryTransInternal == null) {
            e_lorryTransInternal  = new ResultDTO();
        }
        if (e_oil_gas_trans == null) {
            e_oil_gas_trans  = new ResultDTO();
        }
        if (e_paidManagerVehicle == null) {
            e_paidManagerVehicle  = new ResultDTO();
        }
        if (e_paperWaste == null) {
            e_paperWaste  = new ResultDTO();
        }
        if (e_rawMatTrans == null) {
            e_rawMatTrans  = new ResultDTO();
        }
        if (e_sawDustTrans == null) {
            e_sawDustTrans  = new ResultDTO();
        }
        if (e_staffTrans == null) {
            e_staffTrans = new ResultDTO();
        }

        if (e_sludgeTrans == null) {
            e_sludgeTrans  = new ResultDTO();
        }
        if (e_vehicleOthers == null) {
            e_vehicleOthers  = new ResultDTO();
        }
        if (e_lpgGas == null) {
            e_lpgGas = new ResultDTO();
        }
        if (e_bGas == null) {
            e_bGas = new ResultDTO();
        }
        if (e_transport_rented_paid == null) {
            e_transport_rented_paid = new ResultDTO();
        }
        if (e_transport_rented_not_paid == null) {
            e_transport_rented_not_paid = new ResultDTO();
        }

        Company company = companyManager.getEntityByKey(companyId);
        if (company == null) {
            return null;//checked point
        }
            List<Integer> allowedEmissionSrcs = company.getEmSources();

            Map<Integer, AllowedEmissionSrcBean> allowedEmissionSrcBeanMap = company.getAllowedEmissionSources();

            float scope_1 = 0;
            float scope_2 = 0;
            float scope_3 = 0;
            float indirect = 0;
            float direct = 0;

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleRented.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_transport_rented.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_transport_rented.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_transport_rented.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_transport_rented.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_transport_rented.tco2e;
            }

            results.put("transport_rented", Float.toString(e_transport_rented.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RentedPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_transport_rented_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_transport_rented_paid.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_transport_rented_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_transport_rented_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_transport_rented_paid.tco2e;
            }

            results.put("transport_rented_paid", Float.toString(e_transport_rented_paid.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RentedNotPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_transport_rented_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_transport_rented_not_paid.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_transport_rented_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_transport_rented_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RentedNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_transport_rented_not_paid.tco2e;
            }

            results.put("transport_rented_not_paid", Float.toString(e_transport_rented_not_paid.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Generator.getSrcId())) {



            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_diesel_gen.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_diesel_gen.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_diesel_gen.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_diesel_gen.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_diesel_gen.tco2e;
            }

            results.put("diesel_generators", Float.toString(e_diesel_gen.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ComOwned.getSrcId())) {

            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_company_owned.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_company_owned.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_company_owned.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_company_owned.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_company_owned.tco2e;
            }

            results.put("company_owned", Float.toString(e_company_owned.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommPaid.getSrcId())) {

            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_emp_comm_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_emp_comm_paid.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_emp_comm_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_emp_comm_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_emp_comm_paid.tco2e;
            }

            results.put("emp_comm_paid", Float.toString(e_emp_comm_paid.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Refri.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_refri_leakage.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_refri_leakage.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_refri_leakage.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 +=e_refri_leakage.tco2e ;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_refri_leakage.tco2e;
            }

            results.put("refri_leakage", Float.toString(e_refri_leakage.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FireExt.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_fire_ext.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_fire_ext.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_fire_ext.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_fire_ext.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_fire_ext.tco2e;
            }

            results.put("fire_ext", Float.toString(e_fire_ext.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OffRoad.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_offroad.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect +=e_offroad.tco2e ;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_offroad.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 +=e_offroad.tco2e ;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_offroad.tco2e;
            }

            results.put("offroad", Float.toString(e_offroad.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LPG.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_lpgGas.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_lpgGas.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_lpgGas.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_lpgGas.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_lpgGas.tco2e;
            }

            results.put("lpg", Float.toString(e_lpgGas.tco2e));
        }
        //bgas
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Bgas.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_bGas.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_bGas.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_bGas.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_bGas.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_bGas.tco2e;
            }

            results.put("bgas", Float.toString(e_bGas.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Elec.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_elec.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_elec.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_elec.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_elec.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_elec.tco2e;
            }

            results.put("electricity", Float.toString(e_elec.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TD.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_t_d.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_t_d.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_t_d.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_t_d.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_t_d.tco2e;
            }

            results.put("t_d", Float.toString(e_t_d.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteDisposal.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_waste_disposal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_waste_disposal.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_waste_disposal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_waste_disposal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_waste_disposal.tco2e;
            }

            results.put("waste_disposal", Float.toString(e_waste_disposal.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.MunWater.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_mun_water.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_mun_water.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_mun_water.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_mun_water.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_mun_water.tco2e;
            }

            results.put("mun_water", Float.toString(e_mun_water.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirTravel.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_airtravel.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_airtravel.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_airtravel.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_airtravel.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_airtravel.tco2e;
            }

            results.put("air_travel", Float.toString(e_airtravel.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_transport_hired_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_transport_hired_not_paid.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_transport_hired_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_transport_hired_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_transport_hired_not_paid.tco2e;
            }

            results.put("transport_hired_not_paid", Float.toString(e_transport_hired_not_paid.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteTrans.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_waste_transport.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_waste_transport.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_waste_transport.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_waste_transport.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_waste_transport.tco2e;
            }

            results.put("waste_transport", Float.toString(e_waste_transport.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommNotPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_emp_comm_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_emp_comm_not_paid.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_emp_comm_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_emp_comm_not_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_emp_comm_not_paid.tco2e;
            }

            results.put("emp_comm_not_paid", Float.toString(e_emp_comm_not_paid.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_transport_hired_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_transport_hired_paid.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_transport_hired_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_transport_hired_paid.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_transport_hired_paid.tco2e;
            }

            results.put("transport_hired_paid", Float.toString(e_transport_hired_paid.tco2e));
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TransLocPur.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_transport_loca_pur.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_transport_loca_pur.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_transport_loca_pur.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_transport_loca_pur.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_transport_loca_pur.tco2e;
            }

            results.put("transport_loc_pur", Float.toString(e_transport_loca_pur.tco2e));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirFreight.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_airFreight.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_airFreight.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_airFreight.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_airFreight.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_airFreight.tco2e;
            }

            results.put("air_freight", Float.toString(e_airFreight.tco2e));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SeaFreight.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_seaFreight.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_seaFreight.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_seaFreight.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_seaFreight.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_seaFreight.tco2e;
            }

            results.put("sea_freight", Float.toString(e_seaFreight.tco2e));
        }

       //----pasindu------
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.BioMass.getSrcId())) {

            //-----added by pasindu---
            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.BioMass.getSrcId()).getDirect() == DirectCatEnum.None.getCat()) {
                results.put("bio_mass", Float.toString(e_bioMass.tco2e));
            }


        }

        //ash trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AshTrans.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_ashTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_ashTrans.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_ashTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_ashTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_ashTrans.tco2e;
            }

            results.put("ash_trans", Float.toString(e_ashTrans.tco2e));
        }

        //forklift pertorl
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsPetrol.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_forkliftsPetrol.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_forkliftsPetrol.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_forkliftsPetrol.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_forkliftsPetrol.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_forkliftsPetrol.tco2e;
            }

            results.put("forklifts_petrol", Float.toString(e_forkliftsPetrol.tco2e));
        }


        //forklifts diesel
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsDiesel.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_forkliftsDiesel.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_forkliftsDiesel.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_forkliftsDiesel.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_forkliftsDiesel.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_forkliftsDiesel.tco2e;
            }

            results.put("forklifts_diesel", Float.toString(e_forkliftsDiesel.tco2e));
        }

        //furnace oil
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FurnaceOil.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_furnaceOil.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_furnaceOil.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_furnaceOil.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_furnaceOil.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_furnaceOil.tco2e;
            }

            results.put("furnace_oil", Float.toString(e_furnaceOil.tco2e));
        }


        //lory tran in
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransInternal.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_lorryTransInternal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_lorryTransInternal.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_lorryTransInternal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_lorryTransInternal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_lorryTransInternal.tco2e;
            }

            results.put("lorry_trans_internal", Float.toString(e_lorryTransInternal.tco2e));
        }


        //lorry tran ex
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransExternal.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_lorryTransExternal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_lorryTransExternal.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_lorryTransExternal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_lorryTransExternal.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_lorryTransExternal.tco2e;
            }

            results.put("lorry_trans_external", Float.toString(e_lorryTransExternal.tco2e));
        }

        //oil gas trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OilGasTrans.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_oil_gas_trans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_oil_gas_trans.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_oil_gas_trans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_oil_gas_trans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_oil_gas_trans.tco2e;
            }

            results.put("oil_gas_trans", Float.toString(e_oil_gas_trans.tco2e));
        }

        //paid manager vehicle
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaidManagerVehicle.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_paidManagerVehicle.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_paidManagerVehicle.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_paidManagerVehicle.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_paidManagerVehicle.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_paidManagerVehicle.tco2e;
            }

            results.put("paid_manager_vehicles", Float.toString(e_paidManagerVehicle.tco2e));
        }

        //paper waste
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaperWaste.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_paperWaste.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_paperWaste.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_paperWaste.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_paperWaste.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_paperWaste.tco2e;
            }

            results.put("paper_waste", Float.toString(e_paperWaste.tco2e));
        }

        //raw mat trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RawMatTrans.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_rawMatTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_rawMatTrans.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_rawMatTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_rawMatTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_rawMatTrans.tco2e;
            }

            results.put("raw_mat_trans", Float.toString(e_rawMatTrans.tco2e));
        }


        //saw dust trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SawDustTrans.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_sawDustTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_sawDustTrans.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_sawDustTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_sawDustTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_sawDustTrans.tco2e;
            }

            results.put("saw_dust_trans", Float.toString(e_sawDustTrans.tco2e));
        }

        //sludge trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SludgeTrans.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_sludgeTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_sludgeTrans.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_sludgeTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_sludgeTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_sludgeTrans.tco2e;
            }

            results.put("sludge_trans", Float.toString(e_sludgeTrans.tco2e));
        }

        //vehicle others
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehiclesOthers.getSrcId())) {


            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_vehicleOthers.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_vehicleOthers.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_vehicleOthers.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_vehicleOthers.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_vehicleOthers.tco2e;
            }





            results.put("vehicle_others", Float.toString(e_vehicleOthers.tco2e));
        }

        //staff trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.StaffTrans.getSrcId())) {
            //direct
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.StaffTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                direct += e_staffTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.StaffTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirect += e_staffTrans.tco2e;
            }

            //scope
            if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.StaffTrans.getSrcId()).getScopeId() == EmissinScope.Scope1.getScope()) {
                scope_1 += e_staffTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.StaffTrans.getSrcId()).getScopeId() == EmissinScope.Scope2.getScope()) {
                scope_2 += e_staffTrans.tco2e;
            }else if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.StaffTrans.getSrcId()).getScopeId() == EmissinScope.Scope3.getScope()) {
                scope_3 += e_staffTrans.tco2e;
            }

            results.put("staff_trans", Float.toString(e_staffTrans.tco2e));
        }









            float total = direct + indirect;
            float per_capita = (total / numEmps);
            float intensity = (total / revenue);


            results.put("total", Float.toString(total));
            results.put("direct", Float.toString(direct));
            results.put("indirect", Float.toString(indirect));
            results.put("per_capita", Float.toString(per_capita));
            results.put("intensity", Float.toString(intensity));













//            results.put("waste_transport_json", e_waste_transport.toJson().toString());
//            results.put("t_d_json", e_t_d.toJson().toString());
//            results.put("waste_disposal_json", e_waste_disposal.toJson().toString());
//            results.put("mun_water_json", e_mun_water.toJson().toString());
//            results.put("emp_comm_not_paid_json", e_emp_comm_not_paid.toJson().toString());
//            results.put("emp_comm_paid_json", e_emp_comm_paid.toJson().toString());
//            results.put("air_travel_json", e_airtravel.toJson().toString());
//            results.put("electricity_json", e_elec.toJson().toString());
//            results.put("company_owned_json", e_company_owned.toJson().toString());
//            results.put("offroad_json", e_offroad.toJson().toString());
//            results.put("fire_ext_json", e_fire_ext.toJson().toString());
//            results.put("refri_leakage_json", e_refri_leakage.toJson().toString());
//            results.put("diesel_generators_json", e_diesel_gen.toJson().toString());
//            results.put("transport_loc_pur_json", e_transport_loca_pur.toJson().toString());
//            results.put("transport_hired_paid_json", e_transport_hired_paid.toJson().toString());
//            results.put("transport_hired_not_paid_json", e_transport_hired_not_paid.toJson().toString());
//            results.put("transport_rented_json", e_transport_rented.toJson().toString());
            results.put("scope_1", Float.toString(scope_1));
            results.put("scope_2", Float.toString(scope_2));
            results.put("scope_3", Float.toString(scope_3));
            results.put("fy", fy);

            //find emissin factors
            float ef_mw = 0;
            float ef_td = 0;
            float ef_grid = 0;
            //todo uncomment
//        ef_grid = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NATIONAL_GRID_EMISSION_FACTOR).getValue();
//        ef_td = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.T_AND_D_LOSS_PERCENT).getValue();
//        ef_mw = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CF_MUNICIPAL_WATER).getValue();
        results.put("ef_mw", Float.toString(ef_mw));
        results.put("ef_td", Float.toString(ef_td));
        results.put("ef_grid_elec", Float.toString(ef_grid));

        return results;
        }


}
