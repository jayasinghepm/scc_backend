package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SeaAirFreightDTO;
import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.climatesi.ghg.emission_source.api.beans.SeaAirFreightEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class SeaAirFrieghtPopulator {



    private static Logger logger = Logger.getLogger(SeaAirFrieghtPopulator.class);
    private static SeaAirFrieghtPopulator instance;

    public static SeaAirFrieghtPopulator getInstance() {
        if (instance == null) {

            instance = new SeaAirFrieghtPopulator();
        }
        return instance;
    }


    public SeaAirFreightDTO populateDTO(SeaAirFreightEntry entry) {
        SeaAirFreightDTO dto = new SeaAirFreightDTO();
        dto.setId(entry.getId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setPortFrom(entry.getPortFrom());
        dto.setFreightKg(entry.getFreightKg());
        dto.setPortFromText(entry.getPortFromText());
        dto.setIsAirFreight(entry.getIsAirFreight());


        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public SeaAirFreightEntry populate(SeaAirFreightEntry entry, SeaAirFreightDTO dto) {
        try {
            if (dto.getId() > 0) {
                entry.setId(dto.getId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(1);
                    return entry;
                }

            }
            entry.setEmissionSrcId(dto.getEmissionSrcId() == 0 ? entry.getEmissionSrcId() : dto.getEmissionSrcId());
//            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId() == 0 ? entry.getBranchId() : dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId() == 0 ? entry.getCompanyId() : dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear() == null ? entry.getYear() : dto.getYear());
           entry.setPortFrom(dto.getPortFrom() == 0 ? entry.getPortFrom() : dto.getPortFrom());
           entry.setFreightKg(dto.getFreightKg() == 0 ? entry.getFreightKg() : dto.getFreightKg());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
            entry.setPortFromText(dto.getPortFromText() == null ? entry.getPortFromText() : dto.getPortFromText());
            entry.setIsAirFreight(dto.getIsAirFreight());


        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
