package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.EmpCommutingDTO;
import com.climatesi.ghg.emission_source.api.beans.EmpCommutingEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class EmployeeCommutingPopulator {

    private static Logger logger = Logger.getLogger(EmployeeCommutingPopulator.class);
    private static EmployeeCommutingPopulator instance;

    public static EmployeeCommutingPopulator getInstance() {
        if (instance == null) {
            instance = new EmployeeCommutingPopulator();
        }
        return instance;
    }

    public EmpCommutingDTO populateDTO(EmpCommutingEntry entry) {
        EmpCommutingDTO dto = new EmpCommutingDTO();
        dto.setId(entry.getId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setBranchId(entry.getBranchId());
        dto.setEmpId(entry.getEmpId());
        dto.setNoEmissionMode_UP(entry.getNoEmissionMode_UP());
        dto.setNoEmissionMode_DOWN(entry.getNoEmissionMode_DOWN());
        dto.setNoEmission_distance_UP(entry.getNoEmission_distance_UP());
        dto.setNoEmission_distance_DOWN(entry.getNoEmission_distance_DOWN());
        dto.setPublicTransMode_UP(entry.getPublicTransMode_UP());
        dto.setPublicTransMode_DOWN(entry.getPublicTransMode_DOWN());
        dto.setPublicTrans_distance_UP(entry.getPublicTrans_distance_UP());
        dto.setPublicTrans_distance_DOWN(entry.getPublicTrans_distance_DOWN());
        dto.setOwnTransMode_UP(entry.getOwnTransMode_UP());
        dto.setOwnTransMode_DOWN(entry.getOwnTransMode_DOWN());
        dto.setOwnTrans_distance_UP(entry.getOwnTrans_distance_UP());
        dto.setOwnTrans_distance_DOWN(entry.getOwnTrans_distance_DOWN());
        dto.setCompanyFuel_distance_UP(entry.getCompanyFuel_distance_UP());
        dto.setCompanyFuel_distance_DOWN(entry.getCompanyFuel_distance_DOWN());
        dto.setCompanyFuelMode_UP(entry.getCompanyFuelMode_UP());
        dto.setCompanyFuelMode_DOWN(entry.getCompanyFuelMode_DOWN());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setCompany_fuelType_UP(entry.getCompany_fuelType_UP());
        dto.setCompany_fuelType_DOWN(entry.getCompany_fuelType_DOWN());
        dto.setCompanyFuel_fuelEconomy_UP(entry.getCompanyFuel_fuelEconomy_UP());
        dto.setCompanyFuel_fuelEconomy_DOWN(entry.getCompanyFuel_fuelEconomy_DOWN());
        dto.setOwnTrans_fuelEconomy_UP(entry.getOwnTrans_fuelEconomy_UP());
        dto.setOwnTrans_fuelEconomy_DOWN(entry.getOwnTrans_fuelEconomy_DOWN());
        dto.setOwnTrans_fuelType_UP(entry.getOwnTrans_fuelType_UP());
        dto.setOwnTrans_fuelType_DOWN(entry.getOwnTrans_fuelType_DOWN());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setPaidByCom(entry.isPaidByCom());
        dto.setTwoWay(entry.isTwoWay());
        dto.setNoOfWorkingDays(entry.getNoOfWorkingDays());
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setPaid(entry.getPaid());
        dto.setTwoWay(entry.getTwoWay());

        dto.setCompanyPetrolLiters(entry.getCompanyPetrolLiters());
        dto.setCompanyDieselLiters(entry.getCompanyDieselLiters());
        dto.setOwnDieselLiters(entry.getOwnDieselLiters());
        dto.setOwnPetrolLiters(entry.getOwnPetrolLiters());

        return dto;
    }

    public EmpCommutingEntry populate(EmpCommutingEntry entry, EmpCommutingDTO dto) {
        if (dto.getId() > 0) {
            entry.setId(dto.getId());
            if (dto.isDeleted() == 1) {
                entry.setDeleted(dto.isDeleted());
                return entry;
            }

        }
//        entry.setEmissionSrcId(dto.getEmissionSrcId() == 0 ? entry.getEmissionSrcId(): dto.getEmissionSrcId());
//            entry.setAddedBy(dto.getAddedBy());
        entry.setBranchId(dto.getBranchId() == 0 ? entry.getBranchId() : dto.getBranchId());
        entry.setCompanyId(dto.getCompanyId() == 0 ? entry.getCompanyId() : dto.getCompanyId());
        entry.setMonth(dto.getMonth());
        entry.setYear(dto.getYear() == null ? entry.getYear() : dto.getYear());
        entry.setEmpId(dto.getEmpId() == null ? entry.getEmpId() : dto.getEmpId());
        entry.setNoEmissionMode_UP(dto.getNoEmissionMode_UP());
        entry.setNoEmissionMode_DOWN(dto.getNoEmissionMode_DOWN());
        entry.setNoEmission_distance_DOWN(dto.getNoEmission_distance_DOWN());
        entry.setNoEmission_distance_UP(dto.getNoEmission_distance_UP());

        entry.setPublicTransMode_UP(dto.getPublicTransMode_UP());
        entry.setPublicTransMode_DOWN(dto.getPublicTransMode_DOWN());
        entry.setPublicTrans_distance_UP(dto.getPublicTrans_distance_UP());
        entry.setPublicTrans_distance_DOWN(dto.getPublicTrans_distance_DOWN());

        entry.setOwnTransMode_UP(dto.getOwnTransMode_UP());
        entry.setOwnTransMode_DOWN(dto.getOwnTransMode_DOWN());
        entry.setOwnTrans_distance_UP(dto.getOwnTrans_distance_UP());
        entry.setOwnTrans_distance_DOWN(dto.getOwnTrans_distance_DOWN());
        entry.setOwnTrans_fuelEconomy_UP(dto.getOwnTrans_fuelEconomy_UP());
        entry.setOwnTrans_fuelEconomy_DOWN(dto.getOwnTrans_fuelEconomy_DOWN());
        entry.setOwnTrans_fuelType_DOWN(dto.getOwnTrans_fuelType_DOWN());
        entry.setOwnTrans_fuelType_UP(dto.getOwnTrans_fuelType_UP());

        entry.setCompany_fuelType_UP(dto.getCompany_fuelType_UP());
        entry.setCompany_fuelType_DOWN(dto.getCompany_fuelType_DOWN());
        entry.setCompanyFuel_distance_UP(dto.getCompanyFuel_distance_UP());
        entry.setCompanyFuel_distance_DOWN(dto.getCompanyFuel_distance_DOWN());
        entry.setCompanyFuel_fuelEconomy_UP(dto.getCompanyFuel_fuelEconomy_UP());
        entry.setCompanyFuel_fuelEconomy_DOWN(dto.getCompanyFuel_fuelEconomy_DOWN());

        entry.setPaidByCom(dto.isPaidByCom());
        entry.setTwoWay(dto.isTwoWay());
        entry.setNoOfWorkingDays(dto.getNoOfWorkingDays());
        entry.setCompany(dto.getCompany() == null ? entry.getCompany(): dto.getCompany());
        entry.setBranch(dto.getBranch() == null ? entry.getBranch(): dto.getBranch());
        entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        entry.setPaid(dto.getPaid() == null ? entry.getPaid(): dto.getPaid());
        entry.setTwoWay(dto.getTwoWay() == null ? entry.getTwoWay() : dto.getTwoWay());



        entry.setCompanyPetrolLiters(dto.getCompanyPetrolLiters());
        entry.setCompanyDieselLiters(dto.getCompanyDieselLiters());
        entry.setOwnDieselLiters(dto.getOwnDieselLiters());
        entry.setOwnPetrolLiters(dto.getOwnPetrolLiters());
        return entry;
    }
}
