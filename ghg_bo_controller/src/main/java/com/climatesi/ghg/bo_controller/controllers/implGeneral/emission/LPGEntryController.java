package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.LPGEntryPopulator;
import com.climatesi.ghg.bo_controller.populators.MunicipalWaterPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LPGEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.api.enums.Units;
import com.climatesi.ghg.emission_source.implGeneral.beans.LPGEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.MunicipalWaterEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.LPGEntryManagerFacade;
import com.climatesi.ghg.implGeneral.data.MunicipalWaterCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class LPGEntryController extends AbstractController {

    private static final Logger logger = Logger.getLogger(LPGEntryController.class);

    private LPGEntryManagerFacade lpgEntryManagerFacade;
    private EmissionFactorsManager emissionFactorsManager;



    @PostConstruct
    @Override
    public void initialize() {

        try {
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

            lpgEntryManagerFacade = (LPGEntryManagerFacade) EmsissionSourceFactory.getInstance().getLPGEntryEntryManager(em);


        } catch (GHGException e) {
            logger.error("Error in intializing MunicipalWaterController", e);
        }
    }


    public ListResult<LPGEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return lpgEntryManagerFacade.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public LPGEntry edit(LPGEntryDTO dto) throws GHGException {
        LPGEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                LPGEntryPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.getIsDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) lpgEntryManagerFacade.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new LPGEntryBean();
                LPGEntryPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emission(entry));
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) lpgEntryManagerFacade.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public LPGEntry findById(Integer id) {
        return lpgEntryManagerFacade.getEntityByKey(id);
    }

    public HashMap<String, String> emission(LPGEntry entry) {

        float ncv = 0.0000473f;
        float efCo2 = 63.1f;
        float efCh4 = 0.028f;
        float efN2o = 0.0265f;


        ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_NCV).getValue();
        efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_EF_CO2).getValue();
        efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_EF_CH4).getValue();
        efN2o = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_EF_N20).getValue();
        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();


//

        ResultDTO e1 = Calculator.emissionLPG(
                entry.getConsumption(),
                efCo2,
                efCh4,
                efN2o,
                ncv
        );
        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", entry.getConsumption()/1000) );
        results.put("direct", Boolean.toString(true));
        results.put("kg",String.format("%.5f", entry.getConsumption()));
        results.put("ncv",String.format("%.5f", ncv));
        results.put("efCO2",String.format("%.5f", efCo2));
        results.put("efCh4",String.format("%.5f", efCh4));
        results.put("efN2O",String.format("%.5f", efN2o));
        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20) );

        results.put("te",String.format("%.5f",e1.totalEnergy) );

        return results;
    }


}
