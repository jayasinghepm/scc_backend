package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.VehiclePopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.VehicleEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.VehicleEntry;
import com.climatesi.ghg.emission_source.api.enums.FuelTypes;
import com.climatesi.ghg.emission_source.api.enums.Units;
import com.climatesi.ghg.emission_source.api.enums.VehicleTypes;
import com.climatesi.ghg.emission_source.api.facades.VehicleEntryManager;
import com.climatesi.ghg.emission_source.api.facades.WasteTransportEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.VehicleEntryBean;
import com.climatesi.ghg.implGeneral.data.CompanyOwnedVehicleCalcData;
import com.climatesi.ghg.implGeneral.data.OffRoadVehiclesCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

@Stateless
public class VehicleController extends AbstractController {
    private static final Logger logger = Logger.getLogger(VehicleController.class);
    private VehicleEntryManager vehicleEntryManager;
    private WasteTransportEntryManager wasteTransportEntryManager;
    private EmissionFactorsManager emissionFactorsManager;
    private CompanyManager companyManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

            vehicleEntryManager = EmsissionSourceFactory.getInstance().getVehicleEntryManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing VehicleController", e);
        }
    }

    public ListResult<VehicleEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return vehicleEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public VehicleEntry editVehicleEntry(VehicleEntryDTO dto) throws GHGException {
        VehicleEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                VehiclePopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.isDeleted() !=1) {
                    Company c = companyManager.getEntityByKey(entry.getCompanyId());
                    entry.setEmissionDetails(emission(entry, c.getFyCurrentEnd() != c.getFyCurrentStart()));

                }

                //          Todo: add user id
                status = (String) vehicleEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new VehicleEntryBean();
                VehiclePopulator.getInstance().populate(entry, dto);
                entry.setEntryAddedDate(new Date());
                Company c = companyManager.getEntityByKey(entry.getCompanyId());
                entry.setEmissionDetails(emission(entry, c.getFyCurrentEnd() != c.getFyCurrentStart()));
//          Todo: add user id
                status = (String) vehicleEntryManager.addEntity(entry);
            }
            // todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public VehicleEntry findById(Integer id) {
        return vehicleEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emission(VehicleEntry o, boolean isFy) {
        HashMap<String, String> results = new HashMap<>();
        float quantity_diesel = 0;
        float quantity_petrol = 0;

        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();

        float ef_co2_m_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE).getValue();
        float ef_co2_m_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL).getValue();
        float ef_ch4_m_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL).getValue();
        float ef_ch4_m_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE).getValue();
        float ef_n20_m_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE).getValue();
        float ef_n20_m_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL).getValue();

        float ef_co2_o_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_GASOLINE).getValue();
        float ef_co2_o_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_DIESEL).getValue();
        float ef_ch4_o_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_DIESEL).getValue();
        float ef_ch4_o_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_GASOLINE).getValue();
        float ef_n20_o_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_GASOLINE).getValue();
        float ef_n20_o_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_DIESEL).getValue();


        float den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();
        float net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();

        float den_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL).getValue();
        float net_cal_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL).getValue();

       //------------2021----
        float price_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_DIESEL_LITER).getValue();//lad
        float price_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_PETROL_LITER).getValue();//lp95
        float price_lsd = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LSD).getValue();//lsd
        float price_lp92 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP92).getValue();//lp92

        float price_diesel_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_DIESEL_LITER_CALENDER).getValue();//lad
        float price_petrol_cal =emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_PETROL_LITER_CALENDER).getValue();//lp95
        float price_lsd_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LSD_CALENDER).getValue();//lsd
        float price_lp92_cal =emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP92_CALENDER).getValue();//lp92

        //------2020------
        float price_lad_p = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LAD_P).getValue();
        float price_lsd_p = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LSD_P).getValue();
        float price_lp92_p = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP92_P).getValue();
        float price_lp95_p =emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP95_P).getValue();

        float price_lad_p_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LAD_P_CALENDER).getValue();
        float price_lsd_p_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LSD_P_CALENDER).getValue();
        float price_lp92_p_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP92_P_CALENDER).getValue();
        float price_lp95_p_cal =emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP95_P_CALENDER).getValue();

        //-----2019---------
        float price_lad_pp = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LAD_PP).getValue();
        float price_lsd_pp = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LSD_PP).getValue();
        float price_lp92_pp = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP92_PP).getValue();
        float price_lp95_pp =emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP95_PP).getValue();

        float price_lad_pp_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LAD_PP_CALENDER).getValue();
        float price_lsd_pp_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LSD_PP_CALENDER).getValue();
        float price_lp92_pp_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP92_PP_CALENDER).getValue();
        float price_lp95_pp_cal =emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_LP95_PP_CALENDER).getValue();





        float consumption = 0;
        ResultDTO emission = null;
        if (o != null) {
            if (o.getVehicleModel() == VehicleTypes.AirportGroudSupportEquipment.type || o.getVehicleModel() == VehicleTypes.ForkLifts.type ||
                    o.getVehicleModel() == VehicleTypes.Chain_saws.type ||
                    o.getVehicleModel() == VehicleTypes.Agriculture_Tractors.type ||
                    o.getVehicleModel() == VehicleTypes.Other_Offroad.type) {
                OffRoadVehiclesCalcData offRoadVehiclesCalcData = new OffRoadVehiclesCalcData();

                if (o.getFuelType() == FuelTypes.Diesel.type) {
                    if (o.getUnits() == Units.liters.unit) {
                        consumption = o.getConsumption();
                        quantity_diesel += ( o.getConsumption());
                    } else if (o.getUnits() == Units.m3.unit) {
                        consumption = (o.getConsumption() * 1000);
                        quantity_diesel += o.getConsumption();
                    } else if (o.getUnits() == Units.LKR.unit) {
                        if (isFy) {
                            consumption = (o.getConsumption() / price_diesel);
                        }else {
                            consumption = (o.getConsumption() / price_diesel_cal);
                        }
//                        consumption = (o.getConsumption() / price_diesel);
                        quantity_diesel += (consumption);
                    }
                    offRoadVehiclesCalcData.setConsumption_liters(consumption);
                    offRoadVehiclesCalcData.setDensity(den_diesel);
                    offRoadVehiclesCalcData.setNet_caloric_value(net_cal_diesel);
                    offRoadVehiclesCalcData.setEf_co2(ef_co2_o_diesel);
                    offRoadVehiclesCalcData.setEf_ch4(ef_ch4_o_diesel);
                    offRoadVehiclesCalcData.setEf_n2o(ef_n20_o_diesel);
                    offRoadVehiclesCalcData.setGwp_co2(gwp_co2);
                    offRoadVehiclesCalcData.setGwp_n2o(gwp_n20);
                    offRoadVehiclesCalcData.setGwp_ch4(gwp_ch4);
                    emission = Calculator.emissionOffroadVehicles(offRoadVehiclesCalcData);
                    results.put("ef_co2",String.format("%.5f", ef_co2_o_diesel ) );
                    results.put("ef_ch4",String.format("%.5f",ef_ch4_o_diesel ) );
                    results.put("ef_n20",String.format("%.5f", ef_n20_o_diesel) );
                    results.put("den",String.format("%.5f", den_diesel) );
                    results.put("net_cal",String.format("%.5f", net_cal_diesel ) );
                } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                    if (o.getUnits() == Units.liters.unit) {
                        consumption = o.getConsumption();
                        quantity_petrol += (consumption);
                    } else if (o.getUnits() == Units.m3.unit) {
                        consumption = (o.getConsumption() * 1000);
                        quantity_petrol += o.getConsumption();
                    } else if (o.getUnits() == Units.LKR.unit) {
                        if (isFy) {
                            consumption = (o.getConsumption() / price_petrol);
                        }else {
                            consumption = (o.getConsumption() / price_petrol_cal);
                        }

                        quantity_petrol += (consumption);
                    }

                    offRoadVehiclesCalcData.setConsumption_liters(consumption);
                    offRoadVehiclesCalcData.setDensity(den_petrol);
                    offRoadVehiclesCalcData.setNet_caloric_value(net_cal_petrol);
                    offRoadVehiclesCalcData.setEf_co2(ef_co2_o_petrol);
                    offRoadVehiclesCalcData.setEf_ch4(ef_ch4_o_petrol);
                    offRoadVehiclesCalcData.setEf_n2o(ef_n20_o_petrol);
                    offRoadVehiclesCalcData.setGwp_co2(gwp_co2);
                    offRoadVehiclesCalcData.setGwp_n2o(gwp_n20);
                    offRoadVehiclesCalcData.setGwp_ch4(gwp_ch4);
                    emission = Calculator.emissionOffroadVehicles(offRoadVehiclesCalcData);
                    results.put("ef_co2",String.format("%.5f", ef_co2_o_petrol ) );
                    results.put("ef_ch4",String.format("%.5f", ef_ch4_o_petrol ) );
                    results.put("ef_n20",String.format("%.5f", ef_n20_o_petrol ) );
                    results.put("den",String.format("%.5f", den_petrol ) );
                    results.put("net_cal",String.format("%.5f", net_cal_petrol ) );
                }
            } else {

                Calendar calendar = new GregorianCalendar();

                int year = calendar.get(Calendar.YEAR);//2021
                int ny = year+1;
                String sny = String.valueOf(ny);//2021


                int py = year-1;//2020
                int ppy = year -2;//2019

                //Financial Years
                String sy = String.valueOf(year);//2021
                String spy = String.valueOf(py);//2020
                String sppy = String.valueOf(ppy);//2019

                String csny = sny.substring(sny.length() - 2);//22
                String csy = sy.substring(sy.length() - 2);//21
                String cspy = spy.substring(spy.length() - 2);//20
                String csppy = sppy.substring(sppy.length() - 2);//19

                //Calander Years
                String cy = sy+"/"+csny;//2021/22
                String cpy = spy+"/"+csy; //2020/21
                String cppy = sppy+"/"+cspy; //2019/20




                CompanyOwnedVehicleCalcData calcData = new CompanyOwnedVehicleCalcData();

                //-------------LAD--------------------------------------
                if (o.getFuelType() == FuelTypes.Diesel.type) {
                    if(o.getYear().equals(sy) || o.getYear().equals(cy) ){
                    if (o.getUnits() == Units.liters.unit) {
                        consumption = o.getConsumption();
                        quantity_diesel += ( o.getConsumption());
                    } else if (o.getUnits() == Units.m3.unit) {
                        consumption = (o.getConsumption() * 1000);
                        quantity_diesel += o.getConsumption();
                    } else if (o.getUnits() == Units.LKR.unit) {
                        if (isFy) {
                            consumption = (o.getConsumption() / price_diesel);
                        }else {
                            consumption = (o.getConsumption() / price_diesel_cal);
                        }
                        quantity_diesel += (consumption);
                    }
                    }
                   else if(o.getYear().equals(spy) || o.getYear().equals(cpy) ){
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_diesel += ( o.getConsumption());
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_diesel += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lad_p);
                            }else {
                                consumption = (o.getConsumption() / price_lad_p_cal);
                            }
                            quantity_diesel += (consumption);
                        }
                    }

                    else if(o.getYear().equals(sppy) || o.getYear().equals(cppy) ){
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_diesel += ( o.getConsumption());
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_diesel += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lad_pp);
                            }else {
                                consumption = (o.getConsumption() / price_lad_pp_cal);
                            }
                            quantity_diesel += (consumption);
                        }
                    }
                    calcData.setConsumption_liters(consumption);
                    calcData.setDensity(den_diesel);//0.84
                    calcData.setNet_caloric_value(net_cal_diesel);
                    calcData.setEf_co2(ef_co2_m_diesel);
                    calcData.setEf_ch4(ef_ch4_m_diesel);
                    calcData.setEf_n2o(ef_n20_m_diesel);
                    calcData.setGwp_co2(gwp_co2);
                    calcData.setGwp_ch4(gwp_ch4);
                    calcData.setGwp_n2o(gwp_n20);
                    emission = Calculator.emissionCompanyOwnedVehicle(calcData);
                    results.put("ef_co2",String.format("%.5f", ef_co2_m_diesel ) );
                    results.put("ef_ch4",String.format("%.5f", ef_ch4_m_diesel ) );
                    results.put("ef_n20",String.format("%.5f", ef_n20_m_diesel) );
                    results.put("den",String.format("%.5f",den_diesel ) );
                    results.put("net_cal",String.format("%.5f", net_cal_diesel ) );
                    //------------------LAD---------------------------------------



                 //-----------------LP95----------------------------------------
                } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                    if(o.getYear().equals(sy) || o.getYear().equals(cy) ){
                        if (o.getUnits() == Units.liters.unit) {
                        consumption = o.getConsumption();
                        quantity_petrol += (consumption);
                    } else if (o.getUnits() == Units.m3.unit) {
                        consumption = (o.getConsumption() * 1000);
                        quantity_petrol += o.getConsumption();
                    } else if (o.getUnits() == Units.LKR.unit) {
                        if (isFy) {
                            consumption = (o.getConsumption() / price_petrol);
                        }else {
                            consumption = (o.getConsumption() / price_petrol_cal);
                        }
                        quantity_petrol += (consumption);
                    }
                        }

                  else if(o.getYear().equals(spy) || o.getYear().equals(cpy) ){
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_petrol += (consumption);
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_petrol += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lp95_p);
                            }else {
                                consumption = (o.getConsumption() / price_lp95_p_cal);
                            }
                            quantity_petrol += (consumption);
                        }
                    }
                    else if(o.getYear().equals(sppy) || o.getYear().equals(cppy) ){
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_petrol += (consumption);
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_petrol += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lp95_pp);
                            }else {
                                consumption = (o.getConsumption() / price_lp95_pp_cal);
                            }
                            quantity_petrol += (consumption);
                        }
                    }
                    calcData.setConsumption_liters(consumption);
                    calcData.setDensity(den_petrol);
                    calcData.setNet_caloric_value(net_cal_petrol);
                    calcData.setEf_co2(ef_co2_m_petrol);
                    calcData.setEf_ch4(ef_ch4_m_petrol); // Spring boot  Jakarta EE  java
                    calcData.setEf_n2o(ef_n20_m_petrol);
                    calcData.setGwp_co2(gwp_co2);
                    calcData.setGwp_ch4(gwp_ch4);
                    calcData.setGwp_n2o(gwp_n20);
                    emission = Calculator.emissionCompanyOwnedVehicle(calcData);
                    results.put("ef_co2",String.format("%.5f", ef_co2_m_petrol ) );
                    results.put("ef_ch4",String.format("%.5f", ef_ch4_m_petrol ) );
                    results.put("ef_n20",String.format("%.5f", ef_n20_m_petrol) );
                    results.put("den",String.format("%.5f", den_petrol) );
                    results.put("net_cal",String.format("%.5f", net_cal_petrol ) );
                }
                //--------------------LP95--------------------------------

                //----------------------LP92----------------------------------
                else if (o.getFuelType() == FuelTypes.LP_92.type) {
                    if(o.getYear().equals(sy) || o.getYear().equals(cy) ) {
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_petrol += (consumption);
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_petrol += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lp92);
                            } else {
                                consumption = (o.getConsumption() / price_lp92_cal);
                            }
                            quantity_petrol += (consumption);
                        }
                    }
                   else if(o.getYear().equals(spy) || o.getYear().equals(cpy) ) {
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_petrol += (consumption);
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_petrol += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lp92_p);
                            } else {
                                consumption = (o.getConsumption() / price_lp92_p_cal);
                            }
                            quantity_petrol += (consumption);
                        }
                    }
                   else if(o.getYear().equals(sppy) || o.getYear().equals(cppy) ) {
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_petrol += (consumption);
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_petrol += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lp92_pp);
                            } else {
                                consumption = (o.getConsumption() / price_lp92_pp_cal);
                            }
                            quantity_petrol += (consumption);
                        }
                    }
                    calcData.setConsumption_liters(consumption);
                    calcData.setDensity(den_petrol);
                    calcData.setNet_caloric_value(net_cal_petrol);
                    calcData.setEf_co2(ef_co2_m_petrol);
                    calcData.setEf_ch4(ef_ch4_m_petrol);
                    calcData.setEf_n2o(ef_n20_m_petrol);
                    calcData.setGwp_co2(gwp_co2);
                    calcData.setGwp_ch4(gwp_ch4);
                    calcData.setGwp_n2o(gwp_n20);
                    emission = Calculator.emissionCompanyOwnedVehicle(calcData);
                    results.put("ef_co2",String.format("%.5f", ef_co2_m_petrol ) );
                    results.put("ef_ch4",String.format("%.5f", ef_ch4_m_petrol ) );
                    results.put("ef_n20",String.format("%.5f", ef_n20_m_petrol) );
                    results.put("den",String.format("%.5f", den_petrol) );
                    results.put("net_cal",String.format("%.5f", net_cal_petrol ) );
                }
                //--------------------LP92-------------------------------------


                //-------------------LSD------------------------------
                else if (o.getFuelType() == FuelTypes.LSD.type) {
                    if(o.getYear().equals(sy) || o.getYear().equals(cy) ) {
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_diesel += (o.getConsumption());
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_diesel += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lsd);
                            } else {
                                consumption = (o.getConsumption() / price_lsd_cal);
                            }
                            quantity_diesel += (consumption);
                        }
                    }
                  else if(o.getYear().equals(spy) || o.getYear().equals(cpy) ) {
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_diesel += (o.getConsumption());
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_diesel += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lsd_p);
                            } else {
                                consumption = (o.getConsumption() / price_lsd_p_cal);
                            }
                            quantity_diesel += (consumption);
                        }
                    }
                    else if(o.getYear().equals(sppy) || o.getYear().equals(cppy) ) {
                        if (o.getUnits() == Units.liters.unit) {
                            consumption = o.getConsumption();
                            quantity_diesel += (o.getConsumption());
                        } else if (o.getUnits() == Units.m3.unit) {
                            consumption = (o.getConsumption() * 1000);
                            quantity_diesel += o.getConsumption();
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (isFy) {
                                consumption = (o.getConsumption() / price_lsd_pp);
                            } else {
                                consumption = (o.getConsumption() / price_lsd_pp_cal);
                            }
                            quantity_diesel += (consumption);
                        }
                    }
                    calcData.setConsumption_liters(consumption);
                    calcData.setDensity(den_diesel);
                    calcData.setNet_caloric_value(net_cal_diesel);
                    calcData.setEf_co2(ef_co2_m_diesel);
                    calcData.setEf_ch4(ef_ch4_m_diesel);
                    calcData.setEf_n2o(ef_n20_m_diesel);
                    calcData.setGwp_co2(gwp_co2);
                    calcData.setGwp_ch4(gwp_ch4);
                    calcData.setGwp_n2o(gwp_n20);
                    emission = Calculator.emissionCompanyOwnedVehicle(calcData);
                    results.put("ef_co2",String.format("%.5f", ef_co2_m_diesel ) );
                    results.put("ef_ch4",String.format("%.5f", ef_ch4_m_diesel ) );
                    results.put("ef_n20",String.format("%.5f", ef_n20_m_diesel) );
                    results.put("den",String.format("%.5f",den_diesel ) );
                    results.put("net_cal",String.format("%.5f", net_cal_diesel ) );
                }
                //-----------------LSD------------------------



            }
        }
        results.put("tco2",String.format("%.5f", emission.tco2e ) );
        results.put("co2",String.format("%.5f", emission.co2) );
        results.put("n20",String.format("%.5f", emission.n2o) );
        results.put("ch4",String.format("%.5f", emission.ch4 ) );
        results.put("quantity",String.format("%.5f", consumption ));
        results.put("diesel",String.format("%.5f", quantity_diesel ) );
        results.put("petrol",String.format("%.5f", quantity_petrol ) );
        results.put("gwp_co2",String.format("%.5f", gwp_co2 ) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4 ) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20 ) );
        return results;
    }
}
