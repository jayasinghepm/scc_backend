package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.GHGReport;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.GHGReportDTO;
import com.climatesi.ghg.implGeneral.beans.GHGReportBean;
import org.jboss.logging.Logger;

public class ReportPopulator {

    private static Logger logger = Logger.getLogger(ReportPopulator.class);
    private static ReportPopulator instance;

    public static ReportPopulator getInstance() {
        if (instance == null) {
            instance = new ReportPopulator();
        }
        return instance;
    }

    public GHGReportDTO populateDTO(GHGReport report) {
        GHGReportDTO dto =new GHGReportDTO();
        dto.setProjectId(report.getProjectId());
        dto.setComId(report.getComId());
        dto.setAddedDate(report.getAddedDate());
        dto.setPdf(report.getPdf());
        dto.setReportType(report.getReportType());
        dto.setYear(report.getYear());


        return dto;
    }

    public GHGReportBean populate(GHGReportBean bean, GHGReportDTO dto) {
        if (dto.isDeleted() == 2) {
                bean.setDeleted(1);
                return  bean;
        }
        bean.setYear(dto.getYear());
        bean.setPdf(dto.getPdf() == null ? bean.getPdf() : dto.getPdf());
        bean.setProjectId(dto.getProjectId() == 0 ? bean.getProjectId() : dto.getProjectId());
        bean.setComId(dto.getComId() == 0 ? bean.getComId() : dto.getComId());
//        bean.setAddedDate(dto.getAddedDate() == null ? bean.getAddedDate() : dto.getAddedDate());
        bean.setReportType(dto.getReportType() == 0 ? bean.getReportType() : dto.getReportType());
        return bean;
    }
}
