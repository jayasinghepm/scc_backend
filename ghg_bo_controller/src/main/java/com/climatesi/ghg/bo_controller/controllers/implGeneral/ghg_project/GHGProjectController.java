package com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project;

import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission.BgasEntryController;
import com.climatesi.ghg_mitication.api.ProjectFactory;
import com.climatesi.ghg_mitication.api.ProjectStatus;
import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg_mitication.api.facade.ProejctManager;
import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.facades.BranchManager;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.ProjectPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.core.ProjectDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.ws_message.WebNotificationDTO;
import com.climatesi.ghg.business_users.api.BusinessUserFactory;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.business_users.api.beans.DataEntryUser;
import com.climatesi.ghg.business_users.api.enums.CadminStatus;
import com.climatesi.ghg.business_users.api.facades.AdminManager;
import com.climatesi.ghg.business_users.api.facades.CadminManager;
import com.climatesi.ghg.business_users.api.facades.ClientManager;
import com.climatesi.ghg.business_users.api.facades.DataEntryUserManager;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.*;
import com.climatesi.ghg.emission_source.api.enums.DataEntryStatus;
import com.climatesi.ghg.emission_source.api.facades.*;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.ProjectDataEntryStatusIdBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.EmissionSrcDataEntryStatusBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import implGeneral.beans.ProjectBean;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class GHGProjectController extends AbstractController {

    private static Logger logger = Logger.getLogger(GHGProjectController.class);
    private ProejctManager proejctManager;
    private ClientManager clientManager;
    private CadminManager cadminManager;
    private CompanyManager companyManager;
    private BranchManager branchManager;
    private AdminManager adminManager;
    private DataEntryUserManager dataEntryUserManager;
    private EmissionSrcDataEntryStatusManager emissionSrcDataEntryStatusManager;

    private BusinessAirTravelEntryManager businessAirTravelEntryManager;
    private ElectricityEntryManager electricityEntryManager;
    private EmployeCommutingEntryManager employeCommutingEntryManager;
    private FireExtingEntryManager fireExtingEntryManager;
    private GeneratorsEntryManager generatorsEntryManager;
    private MunicipalWaterEntryManager municipalWaterEntryManager;
    private RefrigerantsEntryManager refrigerantsEntryManager;
    private TransportLocalEntryManager transportLocalEntryManager;
    private VehicleEntryManager vehicleEntryManager;
    private WasteDisposalEntryManager wasteDisposalEntryManager;
    private WasteTransportEntryManager wasteTransportEntryManager;

    private BioMassEntryManager bioMassEntryManager;
    private LPGEntryEntryManager lpgEntryEntryManager;
    private BGASEntryEntryManager bgasEntryEntryManager;
    private AshTransportationEntryManager ashTransportationEntryManager;
    private ForkliftsEntryManager forkliftsEntryManager;
    private FurnaceEntryManager furnaceEntryManager;
    private OilAndGasTransportationEntryManager oilAndGasTransportationEntryManager;
    private LorryTransportationManager lorryTransportationManager;
    private PaperWasteManager paperWasteManager;
    private PaidManagerVehiclesEntryManager paidManagerVehiclesEntryManager;
    private RawMaterialTransportationLocalManager rawMaterialTransportationLocalManager;
    private SawDustTransportationManager sawDustTransportationManager;
    private SludgeTransportationManager sludgeTransportationManager;
    private VehicleOthersEntryManager vehicleOthersEntryManager;
    private SeaAirFreightFacade seaAirFreightFacade;



    @Inject
    private Event<WebNotificationDTO> notificationSrcEvent;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            adminManager = BusinessUserFactory.getInstance().getAdminManager(em);
            proejctManager = ProjectFactory.getInstance().getProejctManager(em);
            cadminManager = BusinessUserFactory.getInstance().getCadminManager(em);
            clientManager = BusinessUserFactory.getInstance().getClientManager(em);
            dataEntryUserManager = BusinessUserFactory.getInstance().getDataEntryUserManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            emissionSrcDataEntryStatusManager = EmsissionSourceFactory.getInstance().getEmissionSrcDataEntryManager(em);
            businessAirTravelEntryManager = EmsissionSourceFactory.getInstance().getBusinessAirTravelEntryManager(em);
            electricityEntryManager = EmsissionSourceFactory.getInstance().getElectricityEntryManager(em);
            employeCommutingEntryManager = EmsissionSourceFactory.getInstance().getEmployeeCommEntryManager(em);
            fireExtingEntryManager = EmsissionSourceFactory.getInstance().getFireExtingEntryManager(em);
            generatorsEntryManager = EmsissionSourceFactory.getInstance().getGeneratorsEntryManager(em);
            municipalWaterEntryManager = EmsissionSourceFactory.getInstance().getMunicipalWaterEntryManager(em);
            refrigerantsEntryManager = EmsissionSourceFactory.getInstance().getRefrigerantsEntryManager(em);
            transportLocalEntryManager = EmsissionSourceFactory.getInstance().getTransportLocalEntryManager(em);
            vehicleEntryManager = EmsissionSourceFactory.getInstance().getVehicleEntryManager(em);
            wasteDisposalEntryManager = EmsissionSourceFactory.getInstance().getWasteDisposalEntryManager(em);
            wasteTransportEntryManager = EmsissionSourceFactory.getInstance().getWasteTransportEntryManager(em);

            lpgEntryEntryManager = EmsissionSourceFactory.getInstance().getLPGEntryEntryManager(em);
            bgasEntryEntryManager = EmsissionSourceFactory.getInstance().getBGASEntryEntryManager(em);

            bioMassEntryManager = EmsissionSourceFactory.getInstance().getBioMassEntryManager(em);
            seaAirFreightFacade = EmsissionSourceFactory.getInstance().getSeaAirFreightFacadeManager(em);
            ashTransportationEntryManager = EmsissionSourceFactory.getInstance().getAshTransportationEntryManager(em);
            forkliftsEntryManager = EmsissionSourceFactory.getInstance().getForkliftsEntryManager(em);
            furnaceEntryManager = EmsissionSourceFactory.getInstance().getFurnaceEntryManager(em);
            oilAndGasTransportationEntryManager = EmsissionSourceFactory.getInstance().getOilAndGasTransportationEntryManager(em);
            lorryTransportationManager = EmsissionSourceFactory.getInstance().getLorryTransportationManager(em);
            paidManagerVehiclesEntryManager = EmsissionSourceFactory.getInstance().getPaidManagerVehiclesEntryManager(em);
            paperWasteManager = EmsissionSourceFactory.getInstance().getPaperWasteManager(em);
            rawMaterialTransportationLocalManager = EmsissionSourceFactory.getInstance().getRawMaterialTransportationLocalManager(em);
            sawDustTransportationManager = EmsissionSourceFactory.getInstance().getSawDustTransportationManager(em);
            sludgeTransportationManager = EmsissionSourceFactory.getInstance().getSludgeTransportationManager(em);
            vehicleOthersEntryManager = EmsissionSourceFactory.getInstance().getVehicleOthersEntryManager(em);


        } catch (GHGException e) {
            logger.error("Error in intializing  Controller", e);
        }
    }

    public Project editProject(ProjectDTO dto) throws GHGException {
        Project entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findById(dto.getId());
                if (entry == null) {
                    throw new GHGException("Project is not found");
                }

                if (dto.getStatus() != entry.getStatus() && dto.getStatus() == ProjectStatus.DATA_ENTRY) {
                    fireNotification(
                            "Project " + entry.getName() + " status was changed to Data Entry.",
                            "You can start entering data for emission sources.",
                            "You can start entering data for emission sources.",
                            true,
                            true,
                            true,
                            true,
                            dto.getCompanyId()
                    );

                    Company company  = companyManager.getEntityByKey(dto.getCompanyId());
                    String fy = company.isFinacialYear() ? company.getFyCurrentStart() + "/" + (company.getFyCurrentEnd()%100) : "" + company.getFyCurrentEnd();
                    this.initDataEntryStatus(dto.getCompanyId(), fy);

                    JsonObject object = new JsonObject();
                    JsonObject obj = new JsonObject();
                    obj.addProperty("type", 1);
                    obj.addProperty("value", entry.getCompanyId());
                    obj.addProperty("col", 1);
                    object.add("companyId", obj);
                    ListResult<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", object);
                    JsonObject object1 = new JsonObject();
                    JsonObject obj1 = new JsonObject();
                    obj1.addProperty("type", 1);
                    obj1.addProperty("value", entry.getCompanyId());
                    obj1.addProperty("col", 1);
                    object1.add("companyId", obj1);
                    ListResult<ComAdmin> cadmins = cadminManager.getPaginatedEntityListByFilter(0, "", object);
                    List<String> entitlementsNew = new ArrayList<>();
                    entitlementsNew.add("1-1-1-1");
                    entitlementsNew.add("2-1-1-1");
                    entitlementsNew.add("3-1-1-1");
                    entitlementsNew.add("4-1-1-1");
                    entitlementsNew.add("5-1-1-1");
                    entitlementsNew.add("6-1-1-1");
                    entitlementsNew.add("7-1-1-1");
                    entitlementsNew.add("8-1-1-1");
                    entitlementsNew.add("9-1-1-1");
                    entitlementsNew.add("10-1-1-1");
                    entitlementsNew.add("11-1-1-1");
                    entitlementsNew.add("12-1-1-1");
                    entitlementsNew.add("13-1-1-1");
                    entitlementsNew.add("14-1-1-1");
                    entitlementsNew.add("15-1-1-1");
                    entitlementsNew.add("16-1-1-1");
                    entitlementsNew.add("17-1-1-1");
                    entitlementsNew.add("18-1-1-1");
                    entitlementsNew.add("19-1-1-1");
                    entitlementsNew.add("20-1-1-1");
                    entitlementsNew.add("21-1-1-1");
                    entitlementsNew.add("22-1-1-1");
                    entitlementsNew.add("23-1-1-1");
                    entitlementsNew.add("24-1-1-1");
                    entitlementsNew.add("25-1-1-1");
                    for (Client c : clients.getList()) {
                        c.setEntitlements(entitlementsNew);
                        clientManager.updateEntity(c);
//                        WebNotificationDTO notificationDTO = new WebNotificationDTO();
//                        notificationDTO.setMessage("You can start Entering Emission source Data");
//                        notificationDTO.setRead(0);
////                        notificationDTO.setAddedTime(new Date());
//                        notificationDTO.setUserId(c.getUserId());
//                        notificationSrcEvent.fire(notificationDTO);
                    }
                    for (ComAdmin c : cadmins.getList()) {
                        c.setEntitlements(entitlementsNew);
                        cadminManager.updateEntity(c);
                    }

                }
                if (dto.getStatus() != entry.getStatus() && dto.getStatus() == ProjectStatus.FINAL_SUBMISSION) {
                    JsonObject object = new JsonObject();
                    JsonObject obj = new JsonObject();
                    obj.addProperty("type", 1);
                    obj.addProperty("value", dto.getCompanyId());
                    obj.addProperty("col", 1);
                    object.add("companyId", obj);
                    ListResult<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", object);
                    List<String> entitlementsNew = new ArrayList<>();

                    entitlementsNew.add("1-0-0-0");
                    entitlementsNew.add("2-0-0-0");
                    entitlementsNew.add("3-0-0-0");
                    entitlementsNew.add("4-0-0-0");
                    entitlementsNew.add("5-0-0-0");
                    entitlementsNew.add("6-0-0-0");
                    entitlementsNew.add("7-0-0-0");
                    entitlementsNew.add("8-0-0-0");
                    entitlementsNew.add("9-0-0-0");
                    entitlementsNew.add("10-0-0-0");
                    entitlementsNew.add("11-0-0-0");
                    entitlementsNew.add("12-0-0-0");
                    entitlementsNew.add("13-0-0-0");
                    entitlementsNew.add("14-0-0-0");
                    entitlementsNew.add("15-0-0-0");
                    entitlementsNew.add("16-0-0-0");
                    entitlementsNew.add("17-0-0-0");
                    entitlementsNew.add("18-0-0-0");
                    entitlementsNew.add("19-0-0-0");
                    entitlementsNew.add("20-0-0-0");
                    entitlementsNew.add("21-0-0-0");
                    entitlementsNew.add("22-0-0-0");
                    entitlementsNew.add("23-0-0-0");
                    entitlementsNew.add("24-0-0-0");
                    entitlementsNew.add("25-0-0-0");
                    for (Client c : clients.getList()) {
                        c.setEntitlements(entitlementsNew);
                        clientManager.updateEntity(c);
                    }
                    JsonObject object1 = new JsonObject();
                    JsonObject obj1 = new JsonObject();
                    obj1.addProperty("type", 1);
                    obj1.addProperty("value", dto.getCompanyId());
                    obj1.addProperty("col", 1);
                    object1.add("companyId", obj1);
                    ListResult<ComAdmin> cadmins = cadminManager.getPaginatedEntityListByFilter(0, "", object);
                    for (ComAdmin c : cadmins.getList()) {
                        c.setEntitlements(entitlementsNew);
                        cadminManager.updateEntity(c);
                    }
                }
                if (dto.getStatus() != entry.getStatus() && dto.getStatus() == ProjectStatus.CLOSED) {

                    fireNotification(
                            "Project " + entry.getName() + " was closed.",
                            "Project " + entry.getName() + " was marked as finished.",
                            "Project " + entry.getName() + " was marked as finished.",
                            true,
                            true,
                            true,
                            true,
                            dto.getCompanyId()
                    );

                    Company company  = companyManager.getEntityByKey(dto.getCompanyId());
                    String fy = company.isFinacialYear() ? company.getFyCurrentStart() + "/" + (company.getFyCurrentEnd()%100) : "" + company.getFyCurrentEnd();
                    this.onCloseProject(fy, dto.getCompanyId());
                }
                if (dto.getStatus() != entry.getStatus() && dto.getStatus() == ProjectStatus.SIGNED) {
                    fireNotification(
                            "Project " + entry.getName() + " status  was changed to  Signed.",
                            "Project " + entry.getName() + " was marked as finished.",
                            "Project " + entry.getName() + " was marked as finished.",
                            true,
                            false,
                            false,
                            false,
                            dto.getCompanyId()
                    );
                }
                if (dto.getStatus() != entry.getStatus() && dto.getStatus() == ProjectStatus.APPROVED) {
                    fireNotification(
                            "Project " + entry.getName() + " was approved.",
                            "Project " + entry.getName() + " was approved.",
                            "Project " + entry.getName() + " was marked as finished.",
                            true,
                            true,
                            false,
                            false,
                            dto.getCompanyId()
                    );
                }
                ProjectPopulator.getInstance().populate(entry, dto);

//                entry.se(new Date());
                //          Todo: add user id

                if(dto.isDeleted() == 1) {
                    deleteProject(dto.getId());
                    return entry;
                }
                status = (String) proejctManager.updateEntity(entry);
//                if (status.equals("1") && dto.isDeleted() == 1 && entry.isDeleted() == 0) {
//                    // delete company
//                    Company delCom = companyManager.getEntityByKey(dto.getCompanyId());
//                    if (delCom != null) {
//                        delCom.setDeleted(1);
//                        companyManager.updateEntity(delCom);
//                    }
//                    // delete branches
//                    JsonObject filObjBranch = new JsonObject();
//                    JsonObject filBranch = new JsonObject();
//                    filBranch.addProperty("type", 1);
//                    filBranch.addProperty("value", dto.getCompanyId());
//                    filBranch.addProperty("col", 1);
//                    filObjBranch.add("companyId", filBranch);
//                    List<Branch> delBranches = branchManager.getPaginatedEntityListByFilter(-1, "", filObjBranch).getList();
//                    for(Branch b : delBranches) {
//                        b.setDeleted(1);
//                        branchManager.updateEntity(b);
//                    }
//                    // delete clients
//                    JsonObject filObjClients = new JsonObject();
//                    JsonObject fillClients = new JsonObject();
//                    fillClients.addProperty("type", 1);
//                    fillClients.addProperty("value", dto.getCompanyId());
//                    fillClients.addProperty("col", 1);
//                    filObjClients.add("companyId", fillClients);
//
//                    List<Client> delClients = clientManager.getPaginatedEntityListByFilter(-1, "", filObjClients).getList();
//                    for (Client c : delClients) {
//                        c.setDeleted(1);
//                        clientManager.updateEntity(c);
//                    }
//
//
//                    // delete cadmins
//                    JsonObject filObjCadmins = new JsonObject();
//                    JsonObject filCadmins = new JsonObject();
//                    filCadmins.addProperty("type", 1);
//                    filCadmins.addProperty("value", dto.getCompanyId());
//                    filCadmins.addProperty("col", 1);
//                    filObjCadmins.add("companyId", filBranch);
//
//                    List<ComAdmin> delCadmins = cadminManager.getPaginatedEntityListByFilter(-1, "", filObjCadmins).getList();
//                    for (ComAdmin c : delCadmins) {
//                        c.setDeleted(1);
//                        cadminManager.updateEntity(c);
//                    }
//                }

//         add new
            } else {
                entry = new ProjectBean();
                ProjectPopulator.getInstance().populate(entry, dto);
                entry.setStatus(ProjectStatus.NEW);


                JsonObject object = new JsonObject();
                JsonObject obj = new JsonObject();
                obj.addProperty("type", 1);
                obj.addProperty("value", dto.getCompanyId());
                obj.addProperty("col", 1);
                object.add("companyId", obj);
                ListResult<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", object);
                List<String> entitlementsNew = new ArrayList<>();
                List<Project> projects = proejctManager.getPaginatedEntityListByFilter(-1, "", object).getList();
                for (Project p : projects) {
                    if (p != null && p.getStatus() != ProjectStatus.CLOSED) {
                        throw new GHGException("Already active project exist.");
                    }
                }
                //change cadmin status not approved
                JsonObject filter = new JsonObject();
                JsonObject comFilter = new JsonObject();
                comFilter.addProperty("type", 1);
                comFilter.addProperty("value", dto.getCompanyId());
                comFilter.addProperty("col", 1);
                filter.add("companyId", comFilter);

                List<ComAdmin> comAdmins = cadminManager.getPaginatedEntityListByFilter(-1, "", filter).getList();
                for(ComAdmin c: comAdmins) {
                    c.setStatus(CadminStatus.NOT_SUBMITTED);
                    cadminManager.updateEntity(c);
                }


                entitlementsNew.add("1-0-0-0");
                entitlementsNew.add("2-0-0-0");
                entitlementsNew.add("3-0-0-0");
                entitlementsNew.add("4-0-0-0");
                entitlementsNew.add("5-0-0-0");
                entitlementsNew.add("6-0-0-0");
                entitlementsNew.add("7-0-0-0");
                entitlementsNew.add("8-0-0-0");
                entitlementsNew.add("9-0-0-0");
                entitlementsNew.add("10-0-0-0");
                entitlementsNew.add("11-0-0-0");
                entitlementsNew.add("12-0-0-0");
                entitlementsNew.add("13-0-0-0");
                entitlementsNew.add("14-0-0-0");
                entitlementsNew.add("15-0-0-0");
                entitlementsNew.add("16-0-0-0");
                entitlementsNew.add("17-0-0-0");
                entitlementsNew.add("18-0-0-0");
                entitlementsNew.add("19-0-0-0");
                entitlementsNew.add("20-0-0-0");
                entitlementsNew.add("21-0-0-0");
                entitlementsNew.add("22-0-0-0");
                entitlementsNew.add("23-0-0-0");
                entitlementsNew.add("24-0-0-0");
                entitlementsNew.add("25-0-0-0");
                for (Client c : clients.getList()) {
                    c.setEntitlements(entitlementsNew);
                    clientManager.updateEntity(c);
                }
                JsonObject object1 = new JsonObject();
                JsonObject obj1 = new JsonObject();
                obj1.addProperty("type", 1);
                obj1.addProperty("value", dto.getCompanyId());
                obj1.addProperty("col", 1);
                object1.add("companyId", obj1);
                ListResult<ComAdmin> cadmins = cadminManager.getPaginatedEntityListByFilter(0, "", object);
                for (ComAdmin c : cadmins.getList()) {
                    c.setEntitlements(entitlementsNew);
                    cadminManager.updateEntity(c);
                }
//                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) proejctManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public ListResult<Project> getFilteredResults(int pageNum, Object sorting, Object filter) {
        return proejctManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public Project findById(Integer id) {
        return proejctManager.getEntityByKey(id);
    }


    public int deleteProject(int id) {
        try {
            Project project = proejctManager.getEntityByKey(id);
            int comId = project.getCompanyId();
            Company company = companyManager.getEntityByKey(comId);

            String fy = company.getFyCurrentStart() == company.getFyCurrentEnd() ? "" + company.getFyCurrentEnd()  : "" + company.getFyCurrentStart() + "/" + (company.getFyCurrentEnd() % 100);
            emissionSrcDataEntryStatusManager.deleteDataEntryStatues(comId, fy);
            resetUsersEntitlementsAndStatus(comId);
            return proejctManager.deleteProject(id);

        }catch (Exception e) {
            logger.error(e);
        }

        return 0;
    }


    private void resetUsersEntitlementsAndStatus(int comId) {
        List<String> entitlements= new ArrayList<>();
        entitlements.add("1-0-0-0");
        entitlements.add("2-0-0-0");
        entitlements.add("3-0-0-0");
        entitlements.add("4-0-0-0");
        entitlements.add("5-0-0-0");
        entitlements.add("6-0-0-0");
        entitlements.add("7-0-0-0");
        entitlements.add("8-0-0-0");
        entitlements.add("9-0-0-0");
        entitlements.add("10-0-0-0");
        entitlements.add("11-0-0-0");
        entitlements.add("12-0-0-0");
        entitlements.add("13-0-0-0");
        entitlements.add("14-0-0-0");
        entitlements.add("15-0-0-0");
        entitlements.add("16-0-0-0");
        entitlements.add("17-0-0-0");
        entitlements.add("18-0-0-0");
        entitlements.add("19-0-0-0");
        entitlements.add("20-0-0-0");
        entitlements.add("21-0-0-0");
        entitlements.add("22-0-0-0");
        entitlements.add("23-0-0-0");
        entitlements.add("24-0-0-0");
        entitlements.add("25-0-0-0");

        JsonObject clientFilter = new JsonObject();
        JsonObject clientFilterObj = new JsonObject();
        clientFilterObj.addProperty("type", 1);
        clientFilterObj.addProperty("value", comId);
        clientFilterObj.addProperty("col", 1);
        clientFilter.add("companyId", clientFilterObj);

        List<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", clientFilter).getList();

        for(Client c: clients) {
            c.setEntitlements(entitlements);
            clientManager.updateEntity(c);
        }

        JsonObject cadminFilter = new JsonObject();
        JsonObject cadminFilterObj = new JsonObject();
        cadminFilterObj.addProperty("type", 1);
        cadminFilterObj.addProperty("value", comId);
        cadminFilterObj.addProperty("col", 1);
        cadminFilter.add("companyId",  cadminFilterObj);

        List<ComAdmin> comAdmins = cadminManager.getPaginatedEntityListByFilter(-1, "", cadminFilter).getList();

        for(ComAdmin c : comAdmins) {
            c.setEntitlements(entitlements);
            c.setStatus(CadminStatus.NOT_SUBMITTED);
            cadminManager.updateEntity(c);
        }

        JsonObject dataEntryUserFilter = new JsonObject();
        JsonObject dataEntryUserFilterObj = new JsonObject();
        dataEntryUserFilterObj.addProperty("type", 1);
        dataEntryUserFilterObj.addProperty("value", comId);
        dataEntryUserFilterObj.addProperty("col", 1);
        dataEntryUserFilter.add("companyId", dataEntryUserFilterObj);

        List<DataEntryUser> dataEntryUsers = dataEntryUserManager.getPaginatedEntityListByFilter(-1, "", dataEntryUserFilter).getList();

        for(DataEntryUser dataEntryUser : dataEntryUsers) {
            dataEntryUser.setEntitlements(entitlements);
            dataEntryUserManager.updateEntity(dataEntryUser);
        }


    }




    private void initDataEntryStatus(int comId, String fy) {

        for (int src = 1; src < 15; src++) {
            EmissionSrcDataEntryStatusBean bean = new EmissionSrcDataEntryStatusBean();
            ProjectDataEntryStatusIdBean id = new ProjectDataEntryStatusIdBean();
            id.setEmissionSrc(src);
            id.setFY(fy);
            id.setCompanyId(comId);
            id.setBranchId(-1);
            bean.setStatus(DataEntryStatus.PENDING.status);
            bean.setId(id);
            emissionSrcDataEntryStatusManager.addEntity(bean);
        }


//        JsonObject comFilter = new JsonObject();
//        JsonObject filter = new JsonObject();
//        filter.addProperty("type", 1);
//        filter.addProperty("col", 1);
//        filter.addProperty("value", comId);
//        comFilter.add("companyId", filter);
//        List<Branch> branches = branchManager.getPaginatedEntityListByFilter(-1, "", comFilter).getList();
//        for (Branch b : branches) {
//            for (int src = 1; src < 14; src++) {
//                EmissionSrcDataEntryStatusBean bean = new EmissionSrcDataEntryStatusBean();
//                ProjectDataEntryStatusIdBean id = new ProjectDataEntryStatusIdBean();
//                id.setEmissionSrc(src);
//                id.setFY(fy);
//                id.setCompanyId(comId);
//                id.setBranchId(b.getId());
//                bean.setStatus(DataEntryStatus.PENDING.status);
//                bean.setId(id);
//                emissionSrcDataEntryStatusManager.addEntity(bean);
//            }
//
//
//        }
    }


    private void onCloseProject(String fy, int companyId) {

        JsonObject fyFilter = new JsonObject();
        JsonObject comFilter = new JsonObject();
        fyFilter.addProperty("type", 1);
        fyFilter.addProperty("col", 3);
        fyFilter.addProperty("value", fy);

        comFilter.addProperty("type",1);
        comFilter.addProperty("value", companyId);
        comFilter.addProperty("col",1);

        //business

        List<BusinessAirTravelEntry> businessAirTravelEntries =  businessAirTravelEntryManager.getCustomFiltered(-1, companyId, fy);
        List<ElectricityEntry> electricityEntries = electricityEntryManager.getCustomFiltered(-1, companyId, fy);
        List<EmpCommutingEntry> empCommutingEntries = employeCommutingEntryManager.getCustomFiltered(-1, companyId, fy);
        List<FireExtingEntry> fireExtingEntries = fireExtingEntryManager.getCustomFiltered(-1, companyId, fy);
        List<GeneratorsEntry> generatorsEntries = generatorsEntryManager.getCustomFiltered(-1, companyId, fy);
        List<MunicipalWaterEntry> municipalWaterEntries = municipalWaterEntryManager.getCustomFiltered(-1, companyId, fy);
        List<RefrigerantsEntry> refrigerantsEntries = refrigerantsEntryManager.getCustomFiltered(-1, companyId, fy);;
        List<TransportLocalEntry> transportLocalEntries = transportLocalEntryManager.getCustomFiltered(-1, companyId, fy);;
        List<VehicleEntry> vehicleEntries = vehicleEntryManager.getCustomFiltered(-1, companyId, fy);
        List<WasteDisposalEntry> wasteDisposalEntries = wasteDisposalEntryManager.getCustomFiltered(-1, companyId, fy);
        List<WasteTransportEntry> wasteTransportEntries = wasteTransportEntryManager.getCustomFiltered(-1, companyId, fy);
        List<LPGEntry> lpgEntries = lpgEntryEntryManager.getCustomFiltered(-1, companyId, fy);

        List<BGASEntry> bgasEntries = bgasEntryEntryManager.getCustomFiltered(-1,companyId,fy);
        List<BioMassEntry> bioMassEntries = bioMassEntryManager.getCustomFiltered(-1, companyId, fy);
        List<SeaAirFreightEntry> seaAirFreightEntries = seaAirFreightFacade.getCustomFiltered(-1, companyId, fy);
        List<AshTransportation> ashTransportations = ashTransportationEntryManager.getCustomFiltered(-1, companyId, fy);
        List<Forklifts> forklifts = forkliftsEntryManager.getCustomFiltered(-1, companyId, fy);
        List<FurnaceOil> furnaceOils = furnaceEntryManager.getCustomFiltered(-1, companyId, fy);
        List<LorryTransportation> lorryTransportations = lorryTransportationManager.getCustomFiltered(-1, companyId, fy);
        List<PaidManagerVehicles> paidManagerVehicles = paidManagerVehiclesEntryManager.getCustomFiltered(-1, companyId, fy);
        List<PaperWaste> paperWastes = paperWasteManager.getCustomFiltered(-1, companyId, fy);
        List<OilAndGasTransportation> oilAndGasTransportations = oilAndGasTransportationEntryManager.getCustomFiltered(-1, companyId, fy);
        List<RawMaterialTransporationLocal> rawMaterialTransporationLocals = rawMaterialTransportationLocalManager.getCustomFiltered(-1, companyId, fy);
        List<SawDustTransportation> sawDustTransportations = sawDustTransportationManager.getCustomFiltered(-1, companyId, fy);
        List<SludgeTransportation> sludgeTransportations = sludgeTransportationManager.getCustomFiltered(-1, companyId, fy);
        List<VehicleOthers> vehicleOthers = vehicleOthersEntryManager.getCustomFiltered(-1, companyId, fy);

        if(businessAirTravelEntries != null) {
            for(BusinessAirTravelEntry entry : businessAirTravelEntries) {
                entry.setDeleted(1);
                businessAirTravelEntryManager.updateEntity(entry);
            }
        }
        if(electricityEntries != null) {
            for(ElectricityEntry entry : electricityEntries) {
                entry.setDeleted(1);
                electricityEntryManager.updateEntity(entry);
            }
        }
        if (empCommutingEntries != null) {
            for(EmpCommutingEntry entry : empCommutingEntries) {
                entry.setDeleted(1);
                employeCommutingEntryManager.updateEntity(entry);
            }
        }
        if (fireExtingEntries != null) {
            for (FireExtingEntry entry : fireExtingEntries) {
                entry.setDeleted(1);
                fireExtingEntryManager.updateEntity(entry);
            }
        }
        if (generatorsEntries != null) {
            for(GeneratorsEntry entry : generatorsEntries) {
                entry.setDeleted(1);
                generatorsEntryManager.updateEntity(entry);
            }
        }
        if (municipalWaterEntries != null) {
            for(MunicipalWaterEntry entry : municipalWaterEntries) {
                entry.setDeleted(1);
                municipalWaterEntryManager.updateEntity(entry);
            }
        }
        if (refrigerantsEntries != null) {
            for(RefrigerantsEntry entry: refrigerantsEntries) {
                entry.setDeleted(1);
                refrigerantsEntryManager.updateEntity(entry);
            }
        }
        if (transportLocalEntries != null) {
            for(TransportLocalEntry entry: transportLocalEntries) {
                entry.setDeleted(1);
                transportLocalEntryManager.updateEntity(entry);
            }
        }
        if (vehicleEntries != null) {
            for(VehicleEntry entry : vehicleEntries) {
                entry.setDeleted(1);
                vehicleEntryManager.updateEntity(entry);
            }
        }
        if (wasteDisposalEntries != null) {
            for(WasteDisposalEntry entry: wasteDisposalEntries) {
                entry.setDeleted(1);
                wasteDisposalEntryManager.updateEntity(entry);
            }
        }
        if (wasteTransportEntries != null) {
            for (WasteTransportEntry entry: wasteTransportEntries) {
                entry.setDeleted(1);
                wasteTransportEntryManager.updateEntity(entry);
            }
        }

        if (lpgEntries != null) {
            for (LPGEntry entry:lpgEntries) {
                entry.setIsDeleted(1);
                lpgEntryEntryManager.updateEntity(entry);
            }
        }
        if (bgasEntries != null) {
            for (BGASEntry entry:bgasEntries) {
                entry.setIsDeleted(1);
                bgasEntryEntryManager.updateEntity(entry);
            }
        }
        if (bioMassEntries != null) {
            for (BioMassEntry entry: bioMassEntries) {
                entry.setIsDeleted(1);
                bioMassEntryManager.updateEntity(entry);
            }
        }

        if (seaAirFreightEntries != null) {
            for (SeaAirFreightEntry entry: seaAirFreightEntries) {
                entry.setIsDeleted(1);
                seaAirFreightFacade.updateEntity(entry);
            }
        }
        if (ashTransportations != null) {
            for (AshTransportation entry: ashTransportations) {
                entry.setIsDeleted(1);
                ashTransportationEntryManager.updateEntity(entry);
            }
        }
        if (forklifts != null) {
            for (Forklifts entry: forklifts) {
                entry.setIsDeleted(1);
                forkliftsEntryManager.updateEntity(entry);
            }
        }
        if (furnaceOils != null) {
            for (FurnaceOil entry: furnaceOils) {
                entry.setIsDeleted(1);
                furnaceEntryManager.updateEntity(entry);
            }
        }
        if (lorryTransportations != null) {
            for (LorryTransportation entry: lorryTransportations) {
                entry.setIsDeleted(1);
                lorryTransportationManager.updateEntity(entry);
            }
        }
        if (oilAndGasTransportations != null) {
            for (OilAndGasTransportation entry: oilAndGasTransportations) {
                entry.setIsDeleted(1);
                oilAndGasTransportationEntryManager.updateEntity(entry);
            }
        }
        if (paperWastes != null) {
            for (PaperWaste entry: paperWastes) {
                entry.setIsDeleted(1);
                paperWasteManager.updateEntity(entry);
            }
        }
        if (paidManagerVehicles != null) {
            for (PaidManagerVehicles entry: paidManagerVehicles) {
                entry.setIsDeleted(1);
                paidManagerVehiclesEntryManager.updateEntity(entry);
            }
        }
        if (rawMaterialTransporationLocals != null) {
            for (RawMaterialTransporationLocal entry: rawMaterialTransporationLocals) {
                entry.setIsDeleted(1);
                rawMaterialTransportationLocalManager.updateEntity(entry);
            }
        }
        if (sawDustTransportations != null) {
            for (SawDustTransportation entry: sawDustTransportations) {
                entry.setIsDeleted(1);
                sawDustTransportationManager.updateEntity(entry);
            }
        }
        if (sludgeTransportations != null) {
            for (SludgeTransportation entry: sludgeTransportations) {
                entry.setIsDeleted(1);
                sludgeTransportationManager.updateEntity(entry);
            }
        }
        if (vehicleOthers != null) {
            for (VehicleOthers entry: vehicleOthers) {
                entry.setIsDeleted(1);
                vehicleOthersEntryManager.updateEntity(entry);
            }
        }






    }


    public void fireNotification(String adminMsg, String cadminMsg, String clientMsg, boolean admin, boolean cadmin, boolean client, boolean dataEntryUser, int comId) {
        JsonObject cadminFilter = new JsonObject();
        cadminFilter.addProperty("value", comId);
        cadminFilter.addProperty("col",1);
        cadminFilter.addProperty("type",1);
        JsonObject filter1 = new JsonObject();
        filter1.add("companyId", cadminFilter);

        JsonObject clientFilter = new JsonObject();
        clientFilter.addProperty("value", comId);
        clientFilter.addProperty("type",1);
        clientFilter.addProperty("col",1);
        JsonObject filter2 = new JsonObject();
        filter2.add("companyId", clientFilter);

        JsonObject dataEntryUserFilter = new JsonObject();
        dataEntryUserFilter.addProperty("value", comId);
        dataEntryUserFilter.addProperty("col", 1);
        dataEntryUserFilter.addProperty("type", 1);
        JsonObject filter3 = new JsonObject();
        filter3.add("companyId", dataEntryUserFilter);

        List<ComAdmin> comAdmins =  cadminManager.getPaginatedEntityListByFilter(-1, "",filter1).getList();
        List<Admin> admins = adminManager.getPaginatedEntityListByFilter(-1, "", "").getList();
        List<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", filter2).getList();
        List<DataEntryUser> dataEntryUsers = dataEntryUserManager.getPaginatedEntityListByFilter(-1, "", filter3).getList();
        String comName = "";
        if (comAdmins != null && cadmin) {
            WebNotificationDTO notificationDTO = new WebNotificationDTO();
            notificationDTO.setMessage(cadminMsg);
            notificationDTO.setRead(1);
            for (ComAdmin c : comAdmins) {
                comName = c.getCompany();
                notificationDTO.setUserId(c.getUserId());
                notificationSrcEvent.fire(notificationDTO);
            }

        }
        if (dataEntryUsers != null && dataEntryUser) {
            WebNotificationDTO notificationDTO = new WebNotificationDTO();
            notificationDTO.setMessage(cadminMsg);
            notificationDTO.setRead(1);
            for (DataEntryUser c : dataEntryUsers) {
                comName = c.getCompany();
                notificationDTO.setUserId(c.getUserId());
                notificationSrcEvent.fire(notificationDTO);
            }

        }
        if (admins != null && admin) {
            WebNotificationDTO notificationDTO = new WebNotificationDTO();
            notificationDTO.setMessage(adminMsg);
            notificationDTO.setRead(1);
            for(Admin a : admins) {
                notificationDTO.setUserId(a.getUserId());
                notificationSrcEvent.fire(notificationDTO);
            }
        }

        if (clients != null && client) {
            WebNotificationDTO notificationDTO = new WebNotificationDTO();
            notificationDTO.setMessage(clientMsg);
            notificationDTO.setRead(1);
            for (Client c : clients) {
                notificationDTO.setUserId(c.getUserId());
                notificationSrcEvent.fire(notificationDTO);
            }
        }

    }
}
