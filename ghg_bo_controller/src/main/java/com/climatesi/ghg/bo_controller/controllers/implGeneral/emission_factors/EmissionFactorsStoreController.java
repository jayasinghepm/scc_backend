package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.EmFactorsPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.EmFactorDTO;
import com.climatesi.ghg.implGeneral.EmissionFactorsStoreBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

@Stateless
public class EmissionFactorsStoreController extends AbstractController {
    private static final Logger logger = Logger.getLogger(EmissionFactorsStoreController.class);
    private EmissionFactorsManager manager;

    @PostConstruct
    @Override
    public void initialize() {
        try {
            manager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (Exception e) {
            logger.error("Error in intializing controllers");
        }
    }

    public EmissionFactors findById(int id) {
        return manager.getEntityByKey(id);
    }

    public ListResult<EmissionFactors> getFilteredResult(int pageNum, Object sorting, Object filter) {
        return manager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public EmissionFactors edit(EmFactorDTO dto) throws GHGException {
        EmissionFactors entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findById(dto.getId());
                if (entry == null) {
                    throw new GHGException("Branch is not found");
                }

                EmFactorsPopulator.getInstance().populate(entry, dto);
//                entry.se(new Date());
                //          Todo: add user id
                status = (String) manager.updateEntity(entry);

//         add new
            } else {
                entry = new EmissionFactorsStoreBean();
                EmFactorsPopulator.getInstance().populate(entry, dto);
//                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) manager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }
}
