package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.OilAndGasDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.OilAndGasDTO;
import com.climatesi.ghg.emission_source.api.beans.OilAndGasTransportation;
import com.climatesi.ghg.emission_source.api.beans.OilAndGasTransportation;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class OilAndGasTransportationPopulator {

    private static Logger logger = Logger.getLogger(OilAndGasTransportationPopulator.class);
    private static OilAndGasTransportationPopulator instance;



    public static OilAndGasTransportationPopulator getInstance() {
        if (instance == null) {
            instance = new OilAndGasTransportationPopulator();
        }
        return instance;
    }

    public OilAndGasDTO populateDTO(OilAndGasTransportation entry) {
        OilAndGasDTO dto = new OilAndGasDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setDistance(entry.getDistance());
        dto.setFuleType(entry.getFuleType());//pasindu
        dto.setFuelEconomy(entry.getFuelEconomy());

        dto.setFuelConsumption(entry.getFuelConsumption());
        dto.setNoOfTrips(entry.getNoOfTrips());

        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public OilAndGasTransportation populate(OilAndGasTransportation entry, OilAndGasDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(dto.getIsDeleted());
                    return entry;
                }
            }

            entry.setEmissionSrcId(dto.getEmissionSrcId() != 0 ?  dto.getEmissionSrcId() :entry.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy() != 0 ? dto.getAddedBy() : entry.getAddedBy());
            entry.setBranchId(dto.getBranchId() != 0 ? dto.getBranchId() : entry.getBranchId());
            entry.setCompanyId(dto.getCompanyId() != 0 ? dto.getCompanyId() : entry.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear().equals("") ? entry.getYear() : dto.getYear());
            entry.setDistance(dto.getDistance());
            entry.setFuelEconomy(dto.getFuelEconomy());
            entry.setFuleType(dto.getFuleType());//pasindu


            entry.setNoOfTrips(dto.getNoOfTrips());
            entry.setFuelConsumption(dto.getFuelConsumption());


            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
