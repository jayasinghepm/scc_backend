package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.CadminDTO;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import org.jboss.logging.Logger;


public class ComAdminPopulator {

    private static Logger logger = Logger.getLogger(ComAdminPopulator.class);
    private static ComAdminPopulator instance;

    public static ComAdminPopulator getInstance() {
        if (instance == null) {
            instance = new ComAdminPopulator();
        }
        return instance;
    }

    public CadminDTO populateDTO(ComAdmin comAdmin) {
        CadminDTO dto = new CadminDTO();
        try {
            dto.setStatus(comAdmin.getStatus());
            dto.setStatusMessage(comAdmin.getStatusMessage());
            dto.setId(comAdmin.getId());
            dto.setFirstName(comAdmin.getFirstName());
            dto.setLastName(comAdmin.getLastName());
            dto.setTitle(comAdmin.getTitle());
            dto.setEmail(comAdmin.getEmail());
            dto.setTelephoneNo(comAdmin.getTelephoneNo());
            dto.setMobileNo(comAdmin.getMobileNo());
            dto.setPosition(comAdmin.getPosition());
            dto.setCompanyId(comAdmin.getCompanyId());
            dto.setBranchId(comAdmin.getBranchId());
            dto.setEntitlements(comAdmin.getEntitlements());
            dto.setUserId(comAdmin.getUserId());
            dto.setCompany(comAdmin.getCompany());
        } catch (NullPointerException e) {
            logger.error("Error in populating,", e);
        }
        return dto;
    }

    public ComAdmin populate(ComAdmin admin, CadminDTO dto) {
        try {
            if (dto.getId() > 0) {
                admin.setId(dto.getId());
                if (dto.isDeleted() == 1) {
                    admin.setDeleted(dto.isDeleted());
                    return admin;
                }

            }
            admin.setStatus(dto.getStatus() == 0 ? admin.getStatus() : dto.getStatus());
            admin.setStatusMessage(dto.getStatusMessage() == null ? admin.getStatusMessage() : dto.getStatusMessage());
            admin.setFirstName(dto.getFirstName() == null ? admin.getFirstName() : dto.getFirstName());
            admin.setLastName(dto.getLastName() == null ? admin.getLastName() : dto.getLastName());
            admin.setTitle(dto.getTitle() == null ? admin.getTitle() : dto.getTitle());
            admin.setTelephoneNo(dto.getTelephoneNo() == null ? admin.getTelephoneNo() : dto.getTelephoneNo());
            admin.setMobileNo(dto.getMobileNo() == null ? admin.getMobileNo() : dto.getMobileNo());
            admin.setPosition(dto.getPosition() == null ? admin.getPosition() : dto.getPosition());
            admin.setBranchId(dto.getBranchId() == 0 ? admin.getBranchId() : dto.getBranchId());
            admin.setCompanyId(dto.getCompanyId() == 0 ? admin.getCompanyId() : dto.getCompanyId());
            admin.setEntitlements(dto.getEntitlements() == null ? admin.getEntitlements() : dto.getEntitlements());
            admin.setUserId(dto.getUserId() == 0 ? admin.getUserId() : dto.getUserId());
            admin.setEmail(dto.getEmail() == null ? admin.getEmail() : dto.getEmail());
            admin.setCompany(dto.getCompany() == null ? admin.getCompany() : dto.getCompany());
        } catch (NullPointerException e) {
            logger.error("Error in populating", e);
        }
        return admin;
    }
}
