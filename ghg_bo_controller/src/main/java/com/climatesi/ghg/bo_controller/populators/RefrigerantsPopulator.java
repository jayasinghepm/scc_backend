package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RefrigerantsEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.RefrigerantsEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class RefrigerantsPopulator {

    private static Logger logger = Logger.getLogger(RefrigerantsPopulator.class);
    private static RefrigerantsPopulator instance;

    public static RefrigerantsPopulator getInstance() {
        if (instance == null) {
            instance = new RefrigerantsPopulator();
        }
        return instance;
    }

    public RefrigerantsEntryDTO populateDTO(RefrigerantsEntry entry) {
        RefrigerantsEntryDTO dto = new RefrigerantsEntryDTO();
        dto.setRefrigerantsEntryId(Integer.parseInt("" + entry.getRefrigerantsEntryId() + ""));
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setEntryAddedDate(entry.getEntryAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setAmountRefilled(entry.getAmountRefilled());
        dto.setTypeofRefrigerent(entry.getTypeofRefrigerent());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setRefrigerent(entry.getRefrigerent());
        return dto;

    }

    public RefrigerantsEntry populate(RefrigerantsEntry entry, RefrigerantsEntryDTO dto) {

        try {
            if (dto.getRefrigerantsEntryId() > 0) {
                entry.setRefrigerantsEntryId(dto.getRefrigerantsEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }
            }
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setAmountRefilled(dto.getAmountRefilled());
            entry.setTypeofRefrigerent(dto.getTypeofRefrigerent());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
            entry.setRefrigerent(dto.getRefrigerent() == null ? entry.getRefrigerent() : dto.getRefrigerent());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
