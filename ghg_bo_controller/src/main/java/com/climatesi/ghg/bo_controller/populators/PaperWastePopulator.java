package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaperWasteDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaperWasteDTO;
import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class PaperWastePopulator {

    private static Logger logger = Logger.getLogger(PaperWastePopulator.class);
    private static PaperWastePopulator instance;



    public static PaperWastePopulator getInstance() {
        if (instance == null) {
            instance = new PaperWastePopulator();
        }
        return instance;
    }

    public PaperWasteDTO populateDTO(PaperWaste entry) {
        PaperWasteDTO dto = new PaperWasteDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setQuantity(entry.getQuantity());
      //  dto.setDisposalMethod(entry.getDisposalMethod());
      //  dto.setDisposal(entry.getDisposal());


        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public PaperWaste populate(PaperWaste entry, PaperWasteDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(dto.getIsDeleted());
                    return entry;
                }
            }

            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setQuantity(dto.getQuantity());


         //   entry.setDisposalMethod(dto.getDisposalMethod());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
       //     entry.setDisposal(dto.getDisposal() == null ? entry.getDisposal() : dto.getDisposal());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}

