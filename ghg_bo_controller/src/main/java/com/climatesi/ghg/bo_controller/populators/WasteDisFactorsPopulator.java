package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.WasteDisFactorDTO;
import org.jboss.logging.Logger;

public class WasteDisFactorsPopulator {

    private static WasteDisFactorsPopulator instance;
    private Logger logger = Logger.getLogger(WasteDisFactorsPopulator.class);

    private WasteDisFactorsPopulator() {

    }

    public static WasteDisFactorsPopulator getInstance() {
        if (instance == null) {
            instance = new WasteDisFactorsPopulator();
        }
        return instance;
    }

    public WasteDisFactorDTO populateDTO(WasteDisposalFactors factors) {
        WasteDisFactorDTO dto = new WasteDisFactorDTO();

        try {
            dto.setWasteType(factors.getWasteType());
            dto.setWasteName(factors.getWasteName());
            dto.setUnits(factors.getUnits());
            dto.setReuse(factors.getReuse());
            dto.setOpen_loop(factors.getOpen_loop());
            dto.setClosed_loop(factors.getClosed_loop());
            dto.setCombustion(factors.getCombustion());
            dto.setComposting(factors.getComposting());
            dto.setLandfill(factors.getLandfill());
            dto.setAnaerobic_digestion(factors.getAnaerobic_digestion());
        } catch (NullPointerException e) {
            logger.error("Null pointer");
        }
        return dto;
    }

    public WasteDisposalFactors populate(WasteDisposalFactors factor, WasteDisFactorDTO dto) {

        try {
            factor.setWasteType(dto.getWasteType());
            factor.setWasteName(dto.getWasteName());
            factor.setUnits(dto.getUnits());
            factor.setReuse(dto.getReuse());
            factor.setOpen_loop(dto.getOpen_loop());
            factor.setClosed_loop(dto.getClosed_loop());
            factor.setCombustion(dto.getCombustion());
            factor.setComposting(dto.getComposting());
            factor.setLandfill(dto.getLandfill());
            factor.setAnaerobic_digestion(dto.getAnaerobic_digestion());
        } catch (NullPointerException e) {
            logger.error("Null pointer");
        }
        return factor;
    }
}

