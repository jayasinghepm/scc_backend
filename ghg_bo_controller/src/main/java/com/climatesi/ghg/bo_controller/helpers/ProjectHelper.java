package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg_mitication.api.ProjectStatus;
import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project.GHGProjectController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_controller.populators.ProjectPopulator;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.core.ProjectDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.request.ListProjectReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.request.ManageProjectReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.response.LIstProjectResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.response.ManageProjectResMsg;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class ProjectHelper {

    private static Logger logger = Logger.getLogger(ProjectHelper.class);
    private GHGProjectController controller;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;

    public ProjectHelper() {
        try {
            controller = GHGControllerFactory.getInstance().getGHGProjectController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
        } catch (Exception e) {
            logger.error("Error in intialing controllers", e);
        }
    }

    public LIstProjectResMsg getProjects(ListProjectReqMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Project> list = null;
        LIstProjectResMsg res = new LIstProjectResMsg();
        list = controller.getFilteredResults(pageNumber, sorting, filter);

        List<ProjectDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                ProjectDTO dto = ProjectPopulator.getInstance().populateDTO((Project) o);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public ManageProjectResMsg manageProject(ManageProjectReqMsg msg, int user) {
        logger.info("Message : " + msg);
        Project entry;
        ManageProjectResMsg res = new ManageProjectResMsg();
        try {
            entry = controller.editProject(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    if (entry.getStatus() == ProjectStatus.NEW && msg.getDto().getCompany() != null) {
                        log.setActLog( "Project " +  (msg.getDto().getName()) + " was created by" + log.getUsername()) ;
                        userActController.addActLog(log);
                    }else {
                        log.setActLog("Project " + (msg.getDto().getName()) + " was updated by" + log.getUsername()) ;
                        userActController.addActLog(log);
                    }
//                    else
//                    if (entry.getStatus() == ProjectStatus.SIGNED) {
//
//                    }else
//                    if (entry.getStatus() == ProjectStatus.APPROVED) {
//
//                    }else
//                    if (entry.getStatus() == ProjectStatus.DATA_ENTRY) {
//
//                    }else
//                    if (entry.getStatus() == ProjectStatus.COMPLETENESS_CHECK) {
//
//                    }else
//                    if (entry.getStatus() == ProjectStatus.FIRST_DRAFT_SUBMISSION) {
//
//                    }else
//                    if (entry.getStatus() == ProjectStatus.FINAL_SUBMISSION) {
//
//                    }else
//                    if (entry.getStatus() == ProjectStatus.CLOSED) {
//
//                    }else{
//
//                    }

//                    log.setActLog("Added "+ (msg.getDto().getReportType() == 1 ? "Draft " : "Final ") + " was added by" + log.getUsername()) ;
//                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(ProjectPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Project", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

}
