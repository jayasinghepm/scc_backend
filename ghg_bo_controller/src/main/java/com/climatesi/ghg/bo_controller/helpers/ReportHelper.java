package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.beans.GHGReport;
import com.climatesi.ghg.api.beans.ReportMetaData;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission.GHGEmissionSummaryController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_calc.EmissionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors.EmissionFactorsStoreController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project.GHGProjectController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.insitution.InsstitutionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.report.ReportController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_controller.populators.GHGSummaryPopulator;
import com.climatesi.ghg.bo_controller.populators.ReportMetaDataPopulator;
import com.climatesi.ghg.bo_controller.populators.ReportPopulator;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GHGEmissionSummaryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.GHGReportDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.ReportEfDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.ReportMetaDataDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.req.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.res.*;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.emission_source.api.beans.summary.GHGEmsissionSummary;
import com.climatesi.ghg.emission_source.api.enums.EmissionSummaryModeofEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.GHGEmissionSummaryIdBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.GHGEmissionSummaryBean;
import com.climatesi.ghg.implGeneral.beans.GHGReportBean;
import com.climatesi.ghg.implGeneral.beans.ReportMetaDataBean;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.EmissionSourceEnum;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.climatesi.ghg.utility.implGeneral.FloatRounder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.*;

public class ReportHelper {

    private static Logger logger = Logger.getLogger(ReportHelper.class);
    private ReportController controller;
    private GHGProjectController projectController;
    private InsstitutionController insstitutionController;
    private EmissionController emissionController;
    private EmissionFactorsStoreController emissionFactorsStoreController;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;
    private GHGEmissionSummaryController ghgEmissionSummaryController;


    public ReportHelper() {
        try {
            controller = GHGControllerFactory.getInstance().getReportController();
            projectController = GHGControllerFactory.getInstance().getGHGProjectController();
            insstitutionController = GHGControllerFactory.getInstance().getInsstitutionController();
            emissionController =GHGControllerFactory.getInstance().getEmissionController();
            emissionFactorsStoreController = GHGControllerFactory.getInstance().getEmissionFactorsStoreController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
            ghgEmissionSummaryController = GHGControllerFactory.getInstance().getGHGEmissionSummaryController();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public ListGHGReportResMsg getReports(ListGHGReportReqMsg msg, int user) {
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        List<GHGReport> list = null;
        ListGHGReportResMsg res = new ListGHGReportResMsg();
        list = controller.listReport(pageNumber, sorting, filter);
        List<GHGReportDTO> dtos = new ArrayList<>();

        if (list == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        }else {
            logger.info("Fetched count: " + list.size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list) {
                GHGReportDTO dto = ReportPopulator.getInstance().populateDTO((GHGReportBean) o);
                dtos.add(dto);
            }
        }
        res.setList(dtos);
        return res;

    }

    public ManageGHGReportResMsg manageGHGReport(ManageGHGReportReqMsg msg, int user) {
        logger.info("Message : " + msg);
        GHGReport entry;
        ManageGHGReportResMsg res = new ManageGHGReportResMsg();
        if (msg != null && msg.getDto() != null && msg.getDto().isDeleted() == 1) {
            try {
                GHGReport report = controller.deleteReport(msg.getDto());
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(report.getProjectId());
                res.setDto(msg.getDto());
            } catch (Exception e) {
                logger.error("Failed in manage Project", e);
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration(e.getMessage());
            }
            return res;
        }

        //  ----------------log------------------------
        UserActivityLogBean log = null;
        try {

            UserLogin userLogin =  userLoginController.findById(user);
            if (userLogin != null) {
                log = new UserActivityLogBean();
                log.setUserId(user);
                log.setDeleted(0);
                JsonObject filter = new JsonObject();
                JsonObject loginFilter = new JsonObject();
                loginFilter.addProperty("value", user);
                loginFilter.addProperty("col", 1);
                loginFilter.addProperty("type",1);
                filter.add("userId", loginFilter);

                if (userLogin.getUserType() == 1) {
                  ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                  if (result.getList().size() == 1) {
                      log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                      log.setBranchId(result.getList().get(0).getBranchId());
                      log.setCompanyId(result.getList().get(0).getCompanyId());
                  }
                }
                if (userLogin.getUserType() == 2) {
                    ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                    if (result.getList().size() == 1) {
                        log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                        log.setBranchId(-1);
                        log.setCompanyId(result.getList().get(0).getCompanyId());
                    }
                }
                if (userLogin.getUserType() == 3) {
                    ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                    if (result.getList().size() == 1) {
                        log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                        log.setBranchId(-1);
                        log.setCompanyId(-1);
                    }
                }
            }
        } catch (GHGException e) {
            logger.error(e);
        }

        //------------------------log

        try {
            entry = controller.editReport(msg.getDto());
            if (entry != null) {
                if (log != null) {
                    log.setActLog("Added "+ (msg.getDto().getReportType() == 1 ? "Draft " : "Final ") + "report  was added by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                // saving summary on final submission
                if (msg.getDto().getReportType() ==2) {
                    Company company = insstitutionController.findCompanyById(msg.getDto().getComId());
                    if(company != null) {
                        saveGHGEmissionSummary(company.getFyCurrentStart(), company.getFyCurrentEnd(), company.getId(), -1);
                    }
                }
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getProjectId());
                res.setDto(ReportPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Project", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    private void saveGHGEmissionSummary(int yStart, int yEnd, int comId, int branchId) {
        String fy = (yEnd == yStart) ? ""+ yEnd : yStart + "/" +(yEnd%100);
        GHGEmissionSummaryIdBean idBean = new GHGEmissionSummaryIdBean();
        idBean.setFY(fy);
        idBean.setCompanyId(comId);

        HashMap<String, String> summary = this.emissionController.emission_tco2(comId, branchId,yStart,yEnd );
        if (summary != null) {
            GHGEmissionSummaryBean summaryBean = new GHGEmissionSummaryBean();
            summaryBean.setId(idBean);
            summaryBean.setAddedDate(new Date());
            summaryBean.setEmission(summary);
            summaryBean.setModeOfEntry(EmissionSummaryModeofEntry.Auto.mode);
            ghgEmissionSummaryController.edit(summaryBean);

        }
    }


    public ManageReportMetaDataResMsg manageReportMetaData(ManageReportMetaDataReqMsg msg, int user) {
        logger.info("Message : " + msg);
        ReportMetaData entry;
        ManageReportMetaDataResMsg res = new ManageReportMetaDataResMsg();
        try {
            entry = controller.editReportMetaData(msg.getDto());
            if (entry != null) {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(ReportMetaDataPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Project", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public Message listReportMetaData(ListReportMetaReqMsg msg, int user) {
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        List<ReportMetaData> list = null;
        ListReportMetaResMsg res = new ListReportMetaResMsg();
        list = controller.metaDataList(pageNumber, sorting, filter);
        List<ReportMetaDataDTO> dtos = new ArrayList<>();

        if (list == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        }else {
            logger.info("Fetched count: " + list.size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list) {
                ReportMetaDataDTO dto = ReportMetaDataPopulator.getInstance().populateDTO((ReportMetaDataBean) o);
                dtos.add(dto);
            }
        }
        res.setList(dtos);
        return res;
    }

    public Message getReportZip(GetReportZipReqMsg message, int user) {
        GetReportZipResMsg res =new GetReportZipResMsg();
        JsonObject filter = new JsonObject();
        JsonObject comfilter = new JsonObject();
        comfilter.addProperty("col",1);
        comfilter.addProperty("type",1);
        comfilter.addProperty("value", message.getProjectId());
        filter.add("id", comfilter);
        List<Project> projects = projectController.getFilteredResults(-1, "", filter).getList();
        if (projects.size() > 0) {
           int compId = projects.get(0).getCompanyId();
           Company c = insstitutionController.findCompanyById(compId);
            int yStart = c.getFyCurrentStart();
            int yEnd = c.getFyCurrentEnd();
            JsonObject params = message.getParams();
//            params.addProperty("num_emps",message.getMetadata().getNum_emps() );
//            params.addProperty("revenue", message.getMetadata().getRevenue());
//            params.addProperty("prod_ton", message.getMetadata().getProd_ton());
            HashMap<String, String> current = emissionController.emission_tco2_excluded(
                   c.getId(),
                   -1,
                    yStart, yEnd,
                    params);
           if (c != null) {
               //load summary from ghg emission summary
               try {
                   List<GHGEmissionSummaryDTO> summaryDTOS = getGHGEmissionSummary(c.getId());
                   HashMap<String, JsonObject> summarySplitedMap = splitSummary(summaryDTOS, message);

                   HashMap<String, Object> tableData = getTableData(
                           c.getId(),
                           -1,
                           yStart,
                           yEnd,
                           c.getName(),
                           message.metadata.getExcludedSrcAndReasons(),
//                           message.metadata.getPreviousYearEfs(),
                           summarySplitedMap.get("ef"),
//                           message.metadata.getPreviousYearEmission(),
                           summarySplitedMap.get("emission"),
//                           message.metadata.getPreviousYearStats()
                           summarySplitedMap.get("summary"),
                           message.metadata.getEfDTOS()
                   );
                   String url = controller.getReportZip(message.getProjectId(), message, c.getLogo(), c, current, tableData);
                   res.setS3url(url);

               }catch (Exception e) {
                   logger.error(e);
                   res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                   res.setNarration(e.getMessage());
               }
               // prepare 3 json object from ther




           }
        }

        return res;
    }

    private HashMap<String, JsonObject> splitSummary(List<GHGEmissionSummaryDTO> summaryDTOS, GetReportZipReqMsg message) {

        HashMap<String, JsonObject> outMap = new LinkedHashMap<>();
        if (summaryDTOS != null && summaryDTOS.size() > 0) {
            summaryDTOS.stream()
                    .sorted((object1, object2) -> object1.getFy().compareTo(object2.getFy()));
            JsonObject ef = new JsonObject();
            JsonObject emission = new JsonObject();
            JsonObject summary = new JsonObject();


            for(GHGEmissionSummaryDTO dto : summaryDTOS) {
                if (dto != null) {
                    JsonObject ef_year = new JsonObject();
                    JsonObject emission_year = new JsonObject();
                    JsonObject summary_year = new JsonObject();

                    Company company = insstitutionController.findCompanyById(dto.getCompanyId());
                    if (company == null) continue;
                    List<Integer> allowedEmissionSrcs = company.getEmSources();


                    ef_year.addProperty("grid",dto.getEmissionInfo().get("ef_grid_elec") == null ? "": dto.getEmissionInfo().get("ef_grid_elec").getAsString());
                    ef_year.addProperty("mw",dto.getEmissionInfo().get("ef_mw") == null ? "":  dto.getEmissionInfo().get("ef_mw").getAsString());
                    ef_year.addProperty("td_loss",dto.getEmissionInfo().get("ef_td") == null ? "": dto.getEmissionInfo().get("ef_td").getAsString());


                    if(allowedEmissionSrcs.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_trans_hired_not_paid", dto.getEmissionInfo().get("transport_hired_not_paid") == null ? "" : dto.getEmissionInfo().get("transport_hired_not_paid").getAsString());

                    }

                    if(allowedEmissionSrcs.get(EmissionSourceEnum.Elec.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_elec",dto.getEmissionInfo().get("electricity") == null ? "": dto.getEmissionInfo().get("electricity").getAsString());

                    }

                    if(allowedEmissionSrcs.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_emp_comm_not_paid",dto.getEmissionInfo().get("emp_comm_not_paid") == null ? "": dto.getEmissionInfo().get("emp_comm_not_paid").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.TD.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_t_d",dto.getEmissionInfo().get("t_d") == null ? "":  dto.getEmissionInfo().get("t_d").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.WasteDisposal.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_wasted_dis", dto.getEmissionInfo().get("waste_disposal") == null ? "":  dto.getEmissionInfo().get("waste_disposal").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.MunWater.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_mw", dto.getEmissionInfo().get("mun_water") == null ? "": dto.getEmissionInfo().get("mun_water").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.WasteTrans.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_waste_trans",  dto.getEmissionInfo().get("waste_transport") == null ? "": dto.getEmissionInfo().get("waste_transport").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_trans_hired_paid", dto.getEmissionInfo().get("transport_hired_paid") == null ? "": dto.getEmissionInfo().get("transport_hired_paid").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.VehicleRented.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_trans_rented", dto.getEmissionInfo().get("transport_rented") == null ? "":  dto.getEmissionInfo().get("transport_rented").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.TransLocPur.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_trans_loc_pur", dto.getEmissionInfo().get("transport_loc_pur") == null ? "": dto.getEmissionInfo().get("transport_loc_pur").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.ComOwned.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_com_owned", dto.getEmissionInfo().get("company_owned").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.Generator.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_gen", dto.getEmissionInfo().get("diesel_generators") == null ? "": dto.getEmissionInfo().get("diesel_generators").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.Refri.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_refri", dto.getEmissionInfo().get("refri_leakage") == null ? "":  dto.getEmissionInfo().get("refri_leakage").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.FireExt.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_fire", dto.getEmissionInfo().get("fire_ext").getAsString() == null ? "": dto.getEmissionInfo().get("fire_ext").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.OffRoad.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_offroad", dto.getEmissionInfo().get("offroad") == null ? "" : dto.getEmissionInfo().get("offroad").getAsString());

                    }
                    if(allowedEmissionSrcs.get(EmissionSourceEnum.EmpCommPaid.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_emp_comm_paid", dto.getEmissionInfo().get("emp_comm_paid") == null ? "": dto.getEmissionInfo().get("emp_comm_paid").getAsString());

                    }

                    if(allowedEmissionSrcs.get(EmissionSourceEnum.AirTravel.getSrcId() -1) == 1) {
                        emission_year.addProperty("e_air_travel",  dto.getEmissionInfo().get("air_travel") == null ? "":  dto.getEmissionInfo().get("air_travel").getAsString());
                    }

                    summary_year.addProperty("capita",dto.getEmissionInfo().get("per_capita") == null ? "": dto.getEmissionInfo().get("per_capita").getAsString());

                    summary_year.addProperty("prod", dto.getEmissionInfo().get("per_prod") == null ? "": dto.getEmissionInfo().get("per_prod").getAsString());
                    summary_year.addProperty("intensity", dto.getEmissionInfo().get("intensity") == null ? "" : dto.getEmissionInfo().get("intensity").getAsString());
                    summary_year.addProperty("t_direct", dto.getEmissionInfo().get("direct") == null ? "":  dto.getEmissionInfo().get("direct").getAsString());
                    summary_year.addProperty("t_indirect", dto.getEmissionInfo().get("indirect") == null? "" : dto.getEmissionInfo().get("indirect").getAsString());

                    ef.add(dto.getFy(), ef_year);
                    emission.add(dto.getFy(), emission_year);
                    summary.add(dto.getFy(), summary_year);
                }

            }
            outMap.put("ef", ef);
            outMap.put("emission", emission);
            outMap.put("summary", summary);
        }
        return outMap;
    }

    private List<GHGEmissionSummaryDTO> getGHGEmissionSummary(int comId) throws Exception {
        Company company = insstitutionController.findCompanyById(comId);
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();

        String fy = yStart == yEnd ? "" + yStart : "" + yStart + "/" + (yEnd % 100);

        try {
            List<GHGEmissionSummaryDTO> dtos = new ArrayList<>();
//            //current
//            HashMap<String, String> current = emissionController.emission_tco2(comId, -1, yStart, yEnd);

            int y =1;
            while(true) {
                String fy_prev = company.isFinacialYear() ? ("" + (yStart - y) + "/" + ((yEnd - y ) % 100) + "") : ("" + (yStart - y) + "");
                GHGEmsissionSummary summary = null;
                try {
                    //The mode of entry is not used for fetching
                    summary = ghgEmissionSummaryController.find(comId, -1,fy_prev);
                    if (summary != null) {
                        dtos.add(GHGSummaryPopulator.getInstance().fromHashMap(comId, summary.getId().getFY(), summary.getModeOfEntry(), summary.getEmission()));

                    }
                    if(dtos.size() >= 5 || y >= 10) {
                        break;
                    }

                }catch (Exception e) {
                    logger.error(e.getMessage());
                    continue;
                }
                y++;
            }
            return dtos;

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw  new Exception(e.getMessage());
        }
    }

    public HashMap<String, Object> getTableData(
            int comId,
            int branchId,
            int yearStart,
            int yearEnd,
            String comName,
            JsonObject excludedReasons,
            JsonObject efComparision,
            JsonObject ghgComparision,
            JsonObject ghgStatsComparision,
            List<ReportEfDTO> efDTOS) {

        Company company = insstitutionController.findCompanyById(comId);
        if (company == null) return  new HashMap<>();

        List<Integer> allowedEmissionSrcs = company.getEmSources();

        List<HashMap<String, String>> activityData = new ArrayList<>();

        List<HashMap<String, String>> excludedData = new ArrayList<>();
        List<HashMap<String, String>> indirectData = new ArrayList<>();
        List<HashMap<String, String>> directData = new ArrayList<>();

        //years comparision
        List<List<String>> directCom = new ArrayList<>();
        List<List<String>> indirectCom = new ArrayList<>();
        List<String> tot_indirect = new ArrayList<>();
        List<String> tot_direct = new ArrayList<>();
        List<String> totals = new ArrayList<>();
        List<String> years = new ArrayList<>();

        // gas comparision
        List<List<String>> cat2 = new ArrayList<>();
        List<List<String>> cat3 = new ArrayList<>();
        List<List<String>> cat4 = new ArrayList<>();
        List<List<String>> cat5 = new ArrayList<>();
        List<List<String>> cat6 = new ArrayList<>();
        List<List<String>> gasDirect = new ArrayList<>();
        List<String> td_cat = new ArrayList<>();
        List<String> gasTotals = new ArrayList();


        float total  = 0;
        float indirect = 0;
        float direct =0;

        float t_dir_tco2 =0;
        float t_dir_co2=0;
        float t_dir_ch4 =0;
        float t_dir_n20 = 0;
        float t_ind_tco2 = 0;
        float t_ind_co2 = 0;
        float t_ind_ch4 =0;
        float t_ind_n20 = 0;



        if (allowedEmissionSrcs.get(EmissionSourceEnum.Generator.getSrcId() - 1)  == 1) {
            ResultDTO generators = emissionController.emissionDieselGenerator(comId, branchId, yearStart, yearEnd);
            if (generators == null) {
                generators = new ResultDTO();
            }

            if (generators.diesel != 0) {
                HashMap<String, String> m1 = new HashMap<>();
                m1.put("c", "Onsite Diesel Generators\n");
                m1.put("q", Float.toString(FloatRounder.round2(generators.diesel, 0)));
                m1.put("u", "m \\textsuperscript{3}/year");
                m1.put("d", comName);
                activityData.add(m1);
            }

            if(!excludedReasons.get("gen").getAsJsonObject().get("gen").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s",  "Onsite Diesel Generators");
                d1.put("v", Float.toString(FloatRounder.round2(generators.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Onsite Diesel Generators");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_gen").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(generators.tco2e, 0)));
                directCom.add(comdata);
                direct += generators.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Onsite Diesel Generators");
                gas.add(Float.toString(FloatRounder.round2(generators.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(generators.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(generators.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(generators.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += generators.tco2e;
                t_dir_co2 += generators.co2;
                t_dir_ch4 += generators.ch4;
                t_dir_n20 += generators.n2o;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Onsite Diesel Generators");
                e1.put("r", excludedReasons.get("gen").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.Refri.getSrcId() - 1)  == 1) {
            ResultDTO refri = emissionController.emissionRefriLeak(comId, branchId, yearStart, yearEnd);
            if (refri == null) {
                refri   = new ResultDTO();
            }

            if(refri.r22 != 0) {
                HashMap<String, String> m2 = new HashMap<>();
                m2.put("c", "Refrigerant Leakages(R22)\n");
                m2.put("q", Float.toString(FloatRounder.round2(refri.r22, 0)));
                m2.put("u", "t/year");
                m2.put("d", comName);
                activityData.add(m2);
            }

            if (refri.r407c != 0) {
                HashMap<String, String> m3 = new HashMap<>();
                m3.put("c", "Refrigerant Leakages(R407C)\n");
                m3.put("q", Float.toString(FloatRounder.round2(refri.r407c, 0)));
                m3.put("u", "t/year");
                m3.put("d", comName);
                activityData.add(m3);
            }

            if (refri.r410a != 0) {
                HashMap<String, String> m4 = new HashMap<>();
                m4.put("c", "Refrigerant Leakages(R410A)\n");
                m4.put("q", Float.toString(FloatRounder.round2(refri.r410a, 0)));
                m4.put("u", "t/year");
                m4.put("d", comName);
                activityData.add(m4);
            }

            if(!excludedReasons.get("refri").getAsJsonObject().get("refri").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s",  "Refrigerant Leakages");
                d1.put("v", Float.toString(FloatRounder.round2(refri.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Refrigerant Leakages");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_refri").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(refri.tco2e, 0)));
                directCom.add(comdata);
                direct += refri.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Refrigerant Leakages");
                gas.add(Float.toString(FloatRounder.round2(refri.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(refri.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(refri.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(refri.n2o, 0)));
                gasDirect.add(gas);


                t_dir_tco2 += refri.tco2e;
                t_dir_ch4 += refri.ch4;
                t_dir_n20 += refri.n2o;
                t_dir_co2 += refri.co2;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Refrigerant Leakages");
                e1.put("r", excludedReasons.get("refri").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.ComOwned.getSrcId() - 1)  == 1) {
            ResultDTO comOwn = emissionController.emissionCompanyOwned(comId, branchId, yearStart, yearEnd);
            if (comOwn == null) {
                comOwn = new ResultDTO();
            }

            if (comOwn.petrol != 0) {
                HashMap<String, String> m8 = new HashMap<>();
                m8.put("c", "Company owned vehicles(Petrol)\n");
                m8.put("q", Float.toString(FloatRounder.round2(comOwn.petrol, 0)));
                m8.put("u", "m \\textsuperscript{3}/year");
                m8.put("d", comName);
                activityData.add(m8);
            }

            if (comOwn.diesel != 0) {
                HashMap<String, String> m9 = new HashMap<>();
                m9.put("c", "Company owned vehicles(Diesel)\n");
                m9.put("q", Float.toString(FloatRounder.round2(comOwn.diesel, 0)));
                m9.put("u", "m \\textsuperscript{3}/year");
                m9.put("d", comName);
                activityData.add(m9);
            }

            if(!excludedReasons.get("comOwn").getAsJsonObject().get("comOwn").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Company Owned Vehicles");
                d1.put("v", Float.toString(FloatRounder.round2(comOwn.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Company Owned Vehicles");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_com_owned").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(comOwn.tco2e, 0)));
                directCom.add(comdata);
                direct += comOwn.tco2e;


                List<String>  gas = new ArrayList<>();
                gas.add("Company Owned Vehicles");
                gas.add(Float.toString(FloatRounder.round2(comOwn.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(comOwn.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(comOwn.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(comOwn.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += comOwn.tco2e;
                t_dir_ch4 += comOwn.ch4;
                t_dir_co2 += comOwn.co2;
                t_dir_n20 += comOwn.n2o;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Company Owned Vehicles");
                e1.put("r", excludedReasons.get("comOwn").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if(allowedEmissionSrcs.get(EmissionSourceEnum.FireExt.getSrcId() -1)  == 1) {
            ResultDTO fireEx = emissionController.emissionFireExt(comId, branchId, yearStart, yearEnd);
            if (fireEx == null) {
                fireEx = new ResultDTO();
            }

            if (fireEx.wgas != 0) {
                HashMap<String, String> m5 = new HashMap<>();
                m5.put("c", "Fire Extinguisher(W Gas)\n");
                m5.put("q", Float.toString(FloatRounder.round2(fireEx.wgas, 0)));
                m5.put("u", "t/year");
                m5.put("d", comName);
                activityData.add(m5);
            }

            if (fireEx.fire_co2 != 0) {
                HashMap<String, String> m6 = new HashMap<>();
                m6.put("c", "Fire Extinguishers(CO\\textsuperscript{2}) \n");
                m6.put("q", Float.toString(FloatRounder.round2(fireEx.fire_co2, 0)));
                m6.put("u", "t/year");
                m6.put("d", comName);
                activityData.add(m6);
            }


            if (fireEx.dcp == 0) {
                HashMap<String, String> m7 = new HashMap<>();
                m7.put("c", "Fire Extinguishers(DCP)\n");
                m7.put("q", Float.toString(FloatRounder.round2(fireEx.dcp, 0)));
                m7.put("u", "t/year");
                m7.put("d", comName);
                activityData.add(m7);
            }

            if(!excludedReasons.get("fire").getAsJsonObject().get("fire").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s",  "Fire Extinguisher");
                d1.put("v", Float.toString(FloatRounder.round2(fireEx.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Fire Extinguisher");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_fire").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(fireEx.tco2e, 0)));
                directCom.add(comdata);
                direct += fireEx.tco2e;


                List<String>  gas = new ArrayList<>();
                gas.add("Fire Extinguisher");
                gas.add(Float.toString(FloatRounder.round2(fireEx.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(fireEx.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(fireEx.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(fireEx.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += fireEx.tco2e;
                t_dir_co2 += fireEx.co2;
                t_dir_n20 += fireEx.n2o;
                t_dir_ch4 += fireEx.ch4;


            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Fire Extinguisher");
                e1.put("r", excludedReasons.get("fire").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }

        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.OffRoad.getSrcId() - 1)  == 1) {
            ResultDTO offRoad = emissionController.emissionOffRoad(comId, branchId, yearStart, yearEnd);
            if (offRoad == null) {
                offRoad = new ResultDTO();
            }

            if (offRoad.petrol != 0) {
                HashMap<String, String> m10 = new HashMap<>();
                m10.put("c", "Off-road mobile sources and machineries forklifts (Petrol) \n");
                m10.put("q", Float.toString(FloatRounder.round2(offRoad.petrol, 0)));
                m10.put("u", "m \\textsuperscript{3}/year");
                m10.put("d", comName);
                activityData.add(m10);
            }

            if (offRoad.diesel != 0) {
                HashMap<String, String> m11 = new HashMap<>();
                m11.put("c", "Off-road mobile sources and machineries forklifts (Diesel)\n");
                m11.put("q", Float.toString(FloatRounder.round2(offRoad.diesel, 0)));
                m11.put("u", "m \\textsuperscript{3}/year");
                m11.put("d", comName);
                activityData.add(m11);
            }

            if(!excludedReasons.get("offRoad").getAsJsonObject().get("offRoad").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Off-road mobile sources and machineries forklifts");
                d1.put("v", Float.toString(FloatRounder.round2(offRoad.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Off-road mobile sources and machineries forklifts");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_offroad").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(offRoad.tco2e, 0)));
                directCom.add(comdata);
                direct += offRoad.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Off-road mobile sources and machineries forklifts");
                gas.add(Float.toString(FloatRounder.round2(offRoad.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(offRoad.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(offRoad.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(offRoad.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += offRoad.tco2e;
                t_dir_ch4 += offRoad.ch4;
                t_dir_co2 += offRoad.co2;
                t_dir_n20 += offRoad.n2o;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Off-road mobile sources and machineries forklifts");
                e1.put("r", excludedReasons.get("offRoad").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.EmpCommPaid.getSrcId() - 1)  == 1) {
            ResultDTO empCommPaid = emissionController.emissionEmpCommPaid(comId, branchId, yearStart, yearEnd);
            if (empCommPaid == null){
                empCommPaid = new ResultDTO();
            }


            if (empCommPaid.petrol != 0) {
                HashMap<String, String> m12 = new HashMap<>();
                m12.put("c", "Employee Transport, Paid By The Company\n" +
                        "(Petrol) \n");
                m12.put("q", Float.toString(FloatRounder.round2(empCommPaid.petrol, 0)));
                m12.put("u", "m \\textsuperscript{3}/year");
                m12.put("d", comName);
                activityData.add(m12);
            }

            if (empCommPaid.petrol != 0) {
                HashMap<String, String> m13 = new HashMap<>();
                m13.put("c", "Employee Transport, Paid By The Company\n" +
                        "(Diesel) \n");
                m13.put("q", Float.toString(FloatRounder.round2(empCommPaid.petrol, 0)));
                m13.put("u", "m \\textsuperscript{3}/year");
                m13.put("d", comName);
                activityData.add(m13);
            }


            if(!excludedReasons.get("empComm_paid").getAsJsonObject().get("empComm_paid").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s",  "Employee Commuting, Paid by The\n" +
                        "Company");
                d1.put("v", Float.toString(FloatRounder.round2(empCommPaid.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Employee Commuting, Paid by The\n" +
                        "Company");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_emp_comm_paid").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(empCommPaid.tco2e, 0)));
                directCom.add(comdata);
                direct += empCommPaid.tco2e;


                List<String>  gas = new ArrayList<>();
                gas.add("Employee Commuting, Paid by The\n" +
                        "Company");
                gas.add(Float.toString(FloatRounder.round2(empCommPaid.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(empCommPaid.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(empCommPaid.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(empCommPaid.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += empCommPaid.tco2e;
                t_dir_co2 += empCommPaid.co2;
                t_dir_n20 += empCommPaid.n2o;
                t_dir_ch4 += empCommPaid.ch4;
            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Employee Commuting, Paid by The\n" +
                        "Company");
                e1.put("r", excludedReasons.get("empComm_paid").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }

        }


        if (allowedEmissionSrcs.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId() - 1)  == 1) {
            ResultDTO empComNotPaid = emissionController.emissionEmpCommNotPaid(comId, branchId, yearStart, yearEnd);
            if (empComNotPaid == null){
                empComNotPaid = new ResultDTO();
            }

            if (empComNotPaid.km != 0) {
                HashMap<String, String> m15 = new HashMap<>();
                m15.put("c", "Employee Commuting, Not Paid by The\n" +
                        "Company (Public) \n");
                m15.put("q", Float.toString(FloatRounder.round2(empComNotPaid.km, 0)
                ));
                m15.put("u", "km/year");
                m15.put("d", comName);
                activityData.add(m15);
            }

            if (empComNotPaid.petrol != 0) {

                HashMap<String, String> m17 = new HashMap<>();
                m17.put("c", "Employee Commuting, Not Paid by The\n" +
                        "Company (Petrol)\n");
                m17.put("q", Float.toString(FloatRounder.round2(empComNotPaid.petrol, 0)));
                m17.put("u", "m \\textsuperscript{3}/year");
                m17.put("d", comName);
                activityData.add(m17);
            }

            if (empComNotPaid.diesel != 0) {
                HashMap<String, String> m18 = new HashMap<>();
                m18.put("c", "Employee Commuting, Not Paid by The\n" +
                        "Company (Diesel)\n");
                m18.put("q", Float.toString(FloatRounder.round2(empComNotPaid.diesel, 0)));
                m18.put("u", "m \\textsuperscript{3}/year");
                m18.put("d", comName);
                activityData.add(m18);
            }

            if(!excludedReasons.get("empComm_notpaid").getAsJsonObject().get("empComm_notpaid").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s",  "Employee Commuting,Not Paid by The\n" +
                        "Company");
                d1.put("v", Float.toString(FloatRounder.round2(empComNotPaid.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Employee Commuting, Not Paid by The\n" +
                        "Company ");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_emp_comm_not_paid").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(empComNotPaid.tco2e, 0)));
                indirectCom.add(comdata);

                indirect += empComNotPaid.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Employee Commuting, Not Paid by The\n" +
                        "Company ");
                gas.add(Float.toString(FloatRounder.round2(empComNotPaid.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(empComNotPaid.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(empComNotPaid.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(empComNotPaid.n2o, 0)));
                cat2.add(gas);

                t_ind_ch4 += empComNotPaid.ch4;
                t_ind_tco2 += empComNotPaid.tco2e;
                t_ind_co2 += empComNotPaid.co2;
                t_ind_n20 += empComNotPaid.n2o;
            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Employee Commuting, Not Paid by The\n" +
                        "Company ");
                e1.put("r", excludedReasons.get("empComm_notpaid").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }

        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.MunWater.getSrcId() - 1)  == 1) {
            ResultDTO  mw = emissionController.emissionMunWater(comId, branchId, yearStart, yearEnd);
            if (mw == null){
                mw = new ResultDTO();
            }

            if (mw.m3 != 0) {
                HashMap<String, String> m20 = new HashMap<>();
                m20.put("c", "Municipal Water\n");
                m20.put("q", Float.toString(FloatRounder.round2(mw.m3, 0)));
                m20.put("u", "m \\textsuperscript{3}/year");
                m20.put("d", comName);
                activityData.add(m20);
            }

            if(!excludedReasons.get("mw").getAsJsonObject().get("mw").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Municipal Water");
                d1.put("v", Float.toString(FloatRounder.round2(mw.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Municipal Water");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_mw").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(mw.tco2e, 0)));
                indirectCom.add(comdata);

                indirect += mw.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Municipal Water");
                gas.add(Float.toString(FloatRounder.round2(mw.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(mw.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(mw.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(mw.n2o, 0)));
                cat6.add(gas);

                t_ind_tco2 += mw.tco2e;
                t_ind_co2 += mw.co2;
                t_ind_ch4 += mw.co2;
                t_ind_n20 += mw.ch4;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Municipal Water");
                e1.put("r", excludedReasons.get("mw").getAsJsonObject().get("rented").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.WasteDisposal.getSrcId() - 1)  == 1) {
            ResultDTO wasteDis = emissionController.emissionWasteDisposal(comId, branchId, yearStart, yearEnd);
            if (wasteDis == null) {
                wasteDis = new ResultDTO();
            }

            if (wasteDis.tons != 0) {

                HashMap<String, String> m21 = new HashMap<>();
                m21.put("c", "Waste Disposal\n");
                m21.put("q", Float.toString(FloatRounder.round2(wasteDis.tons, 0)));
                m21.put("u", "t/year");
                m21.put("d", comName);
                activityData.add(m21);
            }

            if(!excludedReasons.get("wasteDis").getAsJsonObject().get("wastDis").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Waste Disposal");
                d1.put("v", Float.toString(FloatRounder.round2(wasteDis.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add( "Waste Disposal");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_wasted_dis").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(wasteDis.tco2e, 0)));
                indirectCom.add(comdata);
                indirect += wasteDis.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Waste Disposal");
                gas.add(Float.toString(FloatRounder.round2(wasteDis.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(wasteDis.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(wasteDis.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(wasteDis.n2o, 0)));
                cat4.add(gas);

                t_ind_tco2 += wasteDis.tco2e;
                t_ind_ch4 += wasteDis.ch4;
                t_ind_co2 += wasteDis.co2;
                t_ind_n20 += wasteDis.n2o;


            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Waste Disposal");
                e1.put("r", excludedReasons.get("wasteDis").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }

        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId() - 1)  == 1) {
            ResultDTO transHiredNotPaid = emissionController.emissionTransHiredNotPaid(comId, branchId, yearStart, yearEnd);
            if (transHiredNotPaid == null) {
                transHiredNotPaid = new ResultDTO();
            }

            if (transHiredNotPaid.petrol != 0) {
                HashMap<String, String> m27 = new HashMap<>();
                m27.put("c", "Transport Hired, Not Paid(Petrol) \n");
                m27.put("q", Float.toString(FloatRounder.round2(transHiredNotPaid.petrol, 0)));
                m27.put("u", "m \\textsuperscript{3}/year");
                m27.put("d", comName);
                activityData.add(m27);
            }

            if ( transHiredNotPaid.diesel != 0) {
                HashMap<String, String> m28 = new HashMap<>();
                m28.put("c", "Transport Hired, Not Paid(Diesel) \n");
                m28.put("q", Float.toString(FloatRounder.round2(transHiredNotPaid.diesel, 0)));
                m28.put("u", "m \\textsuperscript{3}/year");
                m28.put("d", comName);
                activityData.add(m28);
            }

            if(!excludedReasons.get("hired_notpaid").getAsJsonObject().get("hired_notpaid").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Vehicle Hired, Not Paid");
                d1.put("v", Float.toString(FloatRounder.round2(transHiredNotPaid.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Vehicle Hired, Not Paid");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_trans_hired_not_paid").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(transHiredNotPaid.tco2e, 0)));
                indirectCom.add(comdata);

                indirect += transHiredNotPaid.tco2e ;


                List<String>  gas = new ArrayList<>();
                gas.add("Vehicle Hired, Not Paid");
                gas.add(Float.toString(FloatRounder.round2(transHiredNotPaid.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(transHiredNotPaid.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(transHiredNotPaid.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(transHiredNotPaid.n2o, 0)));
                cat2.add(gas);

                t_ind_tco2 += transHiredNotPaid.tco2e;
                t_ind_ch4 += transHiredNotPaid.ch4;
                t_ind_co2 += transHiredNotPaid.co2;
                t_ind_n20 += transHiredNotPaid.n2o;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Vehicle Hired, Not Paid");
                e1.put("r", excludedReasons.get("hired_notpaid").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }


        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId() - 1)  == 1) {
            ResultDTO transHiredPaid = emissionController.emissionTransHiredPaid(comId, branchId, yearStart, yearEnd);
            if(transHiredPaid == null) {
                transHiredPaid = new ResultDTO();
            }

            if ( transHiredPaid.petrol!= 0) {
                HashMap<String, String> m26 = new HashMap<>();
                m26.put("c", "Transport Hired, Paid(Petrol)  \n");
                m26.put("q", Float.toString(FloatRounder.round2(transHiredPaid.petrol, 0)));
                m26.put("u", "m \\textsuperscript{3}/year");
                m26.put("d", comName);
                activityData.add(m26);
            }

            if (transHiredPaid.diesel !=0) {
                HashMap<String, String> m25 = new HashMap<>();
                m25.put("c", "Transport Hired, Paid(Diesel) \n");
                m25.put("q", Float.toString(FloatRounder.round2(transHiredPaid.diesel, 0)));
                m25.put("u", "m \\textsuperscript{3}/year");
                m25.put("d", comName);
                activityData.add(m25);
            }


            if(!excludedReasons.get("hired_paid").getAsJsonObject().get("hired_paid").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Vehicle Hired, Paid");
                d1.put("v", Float.toString(FloatRounder.round2(transHiredPaid.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Vehicle Hired, Paid");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_trans_hired_paid").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(transHiredPaid.tco2e, 0)));
                directCom.add(comdata);
                direct += transHiredPaid.tco2e;


                List<String>  gas = new ArrayList<>();
                gas.add("Vehicle Hired, Paid");
                gas.add(Float.toString(FloatRounder.round2(transHiredPaid.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(transHiredPaid.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(transHiredPaid.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(transHiredPaid.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += transHiredPaid.tco2e;
                t_dir_co2 += transHiredPaid.co2;
                t_dir_n20 += transHiredPaid.n2o;
                t_dir_ch4 += transHiredPaid.ch4;


            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Vehicle Hired, Paid");
                e1.put("r", excludedReasons.get("hired_paid").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.VehicleRented.getSrcId() - 1)  == 1) {
            ResultDTO transRented = emissionController.emissionTransRented(comId, branchId, yearStart, yearEnd);
            if (transRented == null){
                transRented = new ResultDTO();
            }

            if (transRented.petrol != 0) {
                HashMap<String, String> m29 = new HashMap<>();
                m29.put("c", "Transport Rented (Petrol) \n");
                m29.put("q", Float.toString(FloatRounder.round2(transRented.petrol, 0)));
                m29.put("u", "m \\textsuperscript{3}/year");
                m29.put("d", comName);
                activityData.add(m29);
            }

            if (transRented.diesel != 0) {
                HashMap<String, String> m30 = new HashMap<>();
                m30.put("c", "Transport Rented (Diesel) \n");
                m30.put("q", Float.toString(FloatRounder.round2(transRented.diesel, 0)));
                m30.put("u", "m \\textsuperscript{3}/year");
                m30.put("d", comName);
                activityData.add(m30);
            }

            if(!excludedReasons.get("rented").getAsJsonObject().get("rented").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Vehicle Rented");
                d1.put("v", Float.toString(FloatRounder.round2(transRented.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Vehicle Rented");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_trans_rented").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(transRented.tco2e, 0)));
                directCom.add(comdata);

                direct += transRented.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Vehicle Rented");
                gas.add(Float.toString(FloatRounder.round2(transRented.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(transRented.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(transRented.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(transRented.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += transRented.tco2e;
                t_dir_co2 += transRented.co2;
                t_dir_n20 += transRented.n2o;
                t_dir_ch4 += transRented.ch4;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Vehicle Rented");
                e1.put("r", excludedReasons.get("rented").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.Elec.getSrcId() - 1)  == 1) {
            ResultDTO elec = emissionController.emissionElectricity(comId, branchId, yearStart, yearEnd);
            if (elec == null){
                elec = new ResultDTO();
            }

            if (elec.kwh != 0) {
                HashMap<String, String> m14 = new HashMap<>();
                m14.put("c", "Grid connected electricity\n");
                m14.put("q", Float.toString(FloatRounder.round2(elec.kwh, 0)));
                m14.put("u", "kWh/year");
                m14.put("d", comName);
                activityData.add(m14);
            }

            if(!excludedReasons.get("elec").getAsJsonObject().get("elec").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Grid Connected Electricity");
                d1.put("v", Float.toString(FloatRounder.round2(elec.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Grid Connected Electricity");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_elec").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(elec.tco2e, 0)));
                indirectCom.add(comdata);
                indirect += elec.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Grid Connected Electricity");
                gas.add(Float.toString(FloatRounder.round2(elec.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(elec.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(elec.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(elec.n2o, 0)));
                cat2.add(gas);

                t_ind_co2 += elec.co2;
                t_ind_tco2 += elec.tco2e;
                t_ind_ch4 += elec.ch4;
                t_ind_n20 += elec.n2o;


            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Grid Connected Electricity");
                e1.put("r", excludedReasons.get("elec").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }


        if (allowedEmissionSrcs.get(EmissionSourceEnum.AirTravel.getSrcId() - 1)  == 1) {
            ResultDTO airtravel = emissionController.emissionAirtravel(comId, branchId, yearStart, yearEnd);
            if (airtravel == null){
                airtravel = new ResultDTO();
            }

            if (airtravel.km !=0) {
                HashMap<String, String> m16 = new HashMap<>();
                m16.put("c", "Business air travels\n");
                m16.put("q", Float.toString(FloatRounder.round2(airtravel.km, 0)));
                m16.put("u", "km/year");
                m16.put("d", comName);
                activityData.add(m16);
            }

            if(!excludedReasons.get("air").getAsJsonObject().get("air").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Business air travels");
                d1.put("v", Float.toString(FloatRounder.round2(airtravel.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Business air travels");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_air_travel").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(airtravel.tco2e, 0)));
                indirectCom.add(comdata);
                indirect += airtravel.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Business air travels");
                gas.add(Float.toString(FloatRounder.round2(airtravel.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(airtravel.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(airtravel.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(airtravel.n2o, 0)));
                cat2.add(gas);

                t_ind_tco2 += airtravel.tco2e;
                t_ind_co2 += airtravel.co2;
                t_ind_ch4 += airtravel.ch4;
                t_ind_n20 += airtravel.n2o;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Business air travels");
                e1.put("r", excludedReasons.get("air").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.WasteTrans.getSrcId() - 1)  == 1) {
            ResultDTO wasteTrans = emissionController.emissionWasteTransport(comId, branchId, yearStart, yearEnd);
            if (wasteTrans == null){
                wasteTrans =  new ResultDTO();
            }

            if (wasteTrans.m3 != 0) {

                HashMap<String, String> m22 = new HashMap<>();
                m22.put("c", "Waste transport \n");
                m22.put("q", Float.toString(FloatRounder.round2(wasteTrans.m3, 0)));
                m22.put("u", "m \\textsuperscript{3}/year");
                m22.put("d", comName);
                activityData.add(m22);
            }

            if(!excludedReasons.get("wasTrans").getAsJsonObject().get("wasTrans").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Waste Transportation");
                d1.put("v", Float.toString(FloatRounder.round2(wasteTrans.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Waste Transportation");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_waste_trans").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(wasteTrans.tco2e, 0)));
                indirectCom.add(comdata);

                indirect += wasteTrans.tco2e;


                List<String>  gas = new ArrayList<>();
                gas.add("Waste Transportation");
                gas.add(Float.toString(FloatRounder.round2(wasteTrans.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(wasteTrans.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(wasteTrans.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(wasteTrans.n2o, 0)));
                cat4.add(gas);

                t_ind_tco2 += wasteTrans.tco2e;
                t_ind_co2 += wasteTrans.co2;
                t_ind_ch4 += wasteTrans.ch4;
                t_ind_n20 += wasteTrans.n2o;


            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Waste Transportation");
                e1.put("r", excludedReasons.get("wasTrans").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.TransLocPur.getSrcId() - 1)  == 1) {
            ResultDTO transLoc = emissionController.emissionTransportLocPurchased(comId, branchId, yearStart, yearEnd);
            if (transLoc == null){
                transLoc = new ResultDTO();
            }

            if (transLoc.petrol != 0) {
                HashMap<String, String> m23 = new HashMap<>();
                m23.put("c", "Transport Locally Purchased(Petrol) \n");
                m23.put("q", Float.toString(FloatRounder.round2(transLoc.petrol, 0)));
                m23.put("u", "m \\textsuperscript{3}/year");
                m23.put("d", comName);
                activityData.add(m23);
            }

            if ( transLoc.diesel!= 0) {
                HashMap<String, String> m24 = new HashMap<>();
                m24.put("c", "Transport Locally Purchased(Diesel) \n");
                m24.put("q", Float.toString(FloatRounder.round2(transLoc.diesel, 0)));
                m24.put("u", "m \\textsuperscript{3}/year");
                m24.put("d", comName);
                activityData.add(m24);
            }

            if(!excludedReasons.get("transLoc").getAsJsonObject().get("transLoc").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Transport Locally Purchased");
                d1.put("v", Float.toString(FloatRounder.round2(transLoc.tco2e, 0)));
                directData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Transport Locally Purchased");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_trans_loc_pur").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(transLoc.tco2e, 0)));
                directCom.add(comdata);
                direct += transLoc.tco2e;

                List<String>  gas = new ArrayList<>();
                gas.add("Transport Locally Purchased");
                gas.add(Float.toString(FloatRounder.round2(transLoc.tco2e, 0)));
                gas.add(Float.toString(FloatRounder.round2(transLoc.co2, 0)));
                gas.add(Float.toString(FloatRounder.round2(transLoc.ch4, 0)));
                gas.add(Float.toString(FloatRounder.round2(transLoc.n2o, 0)));
                gasDirect.add(gas);

                t_dir_tco2 += transLoc.tco2e;
                t_dir_n20 += transLoc.n2o;
                t_dir_ch4 += transLoc.ch4;
                t_dir_co2 += transLoc.co2;


            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Transport Locally Purchased");
                e1.put("r", excludedReasons.get("transLoc").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }

        if (allowedEmissionSrcs.get(EmissionSourceEnum.TD.getSrcId() - 1)  == 1) {
            ResultDTO tdLoss = emissionController.emissionTandDLoss(comId, branchId, yearStart, yearEnd);
            if (tdLoss == null){
                tdLoss = new ResultDTO();
            }

            if(!excludedReasons.get("td").getAsJsonObject().get("td").getAsBoolean()) {
                HashMap<String, String> d1 = new HashMap<>();
                d1.put("s", "Transmission and Distribution Loss");
                d1.put("v", Float.toString(FloatRounder.round2(tdLoss.tco2e, 0)));
                indirectData.add(d1);

                List<String> comdata = new ArrayList<>();
                comdata.add("Transmission and Distribution Loss");
                for(String y : years) {
                    comdata.add(ghgComparision.get(y).getAsJsonObject().get("e_t_d").getAsString());
                }
                comdata.add( Float.toString(FloatRounder.round2(tdLoss.tco2e, 0)));
                indirectCom.add(comdata);
                indirect += tdLoss.tco2e;


//            List<String>  gas = new ArrayList<>();
//            gas.add("Indirect emissions from T & D Loss from Imported\n" +
//                    "Electricity ");
                td_cat.add(Float.toString(FloatRounder.round2(tdLoss.tco2e, 0)));
                td_cat.add(Float.toString(FloatRounder.round2(tdLoss.co2, 0)));
                td_cat.add(Float.toString(FloatRounder.round2(tdLoss.ch4, 0)));
                td_cat.add(Float.toString(FloatRounder.round2(tdLoss.n2o, 0)));

//            cat2.add(gas);

                t_ind_tco2 += tdLoss.tco2e;
                t_ind_ch4 += tdLoss.ch4;
                t_ind_co2 += tdLoss.co2;
                t_ind_n20 += tdLoss.n2o;

            }else {
                HashMap<String, String> e1 = new HashMap<>();
                e1.put("s", "Transmission and Distribution Loss");
                e1.put("r", excludedReasons.get("td").getAsJsonObject().get("reason").getAsString());
                excludedData.add(e1);
            }
        }


        //----------------------Activity Data ----------------------------------------------
        // ----------------------------------------------------------------------------------
        // --------------------------------- Excluded Src Table data------------------------


        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);

        if (efComparision != null) {
            for (Map.Entry<String, JsonElement> entry: efComparision.entrySet()) {
                years.add(entry.getKey());
            }
        }


        if ( ghgStatsComparision != null && ghgStatsComparision.size() == years.size()) {
            for (String y : years) {
                tot_direct.add( ghgStatsComparision.get(y).getAsJsonObject().get("t_direct").getAsString());
                float tot_di =  ghgStatsComparision.get(y).getAsJsonObject().get("t_direct").getAsFloat();
                float tot_ind = ghgStatsComparision.get(y).getAsJsonObject().get("t_indirect").getAsFloat();
                tot_indirect.add(ghgStatsComparision.get(y).getAsJsonObject().get("t_indirect").getAsString());
                totals.add(Float.toString(tot_di + tot_ind));
            }
        }else {
            logger.error("Size not mached");
        }


        //----------------------------------------------------------------------------------
        List<List<String>> efStats = new ArrayList<>();
        List<String> grid = new ArrayList<>();
        grid.add("Grid electricity (kgCO\\textsubscript{2}e/kWh)");
        List<String> mwEf = new ArrayList<>();
        mwEf.add("Municipal Water(kgCO\\textsubscript{2}e/m\\textsuperscript{3})\n");
        List<String> tdEf = new ArrayList<>();
        tdEf.add("Transmission \\& Distribution" +
                " Loss (\\%) ");
        for(String year: years){
            grid.add(efComparision.get(year).getAsJsonObject().get("grid").getAsString());
            mwEf.add(efComparision.get(year).getAsJsonObject().get("mw").getAsString());
            tdEf.add(efComparision.get(year).getAsJsonObject().get("td_loss").getAsString());
        }
        float ef_grid = emissionFactorsStoreController.findById(EmissionFactors_Keys.NATIONAL_GRID_EMISSION_FACTOR).getValue();
        float ef_td = emissionFactorsStoreController.findById(EmissionFactors_Keys.T_AND_D_LOSS_PERCENT).getValue();
        float cf_mun = emissionFactorsStoreController.findById(EmissionFactors_Keys.CF_MUNICIPAL_WATER).getValue();
        grid.add(Float.toString(FloatRounder.round2(ef_grid, 0)));
        mwEf.add(Float.toString(FloatRounder.round2(cf_mun, 0)));
        tdEf.add(Float.toString(FloatRounder.round2(ef_td, 0)));
//        efStats.add(grid);
//        efStats.add(mwEf);
//        efStats.add(tdEf);

        if (efDTOS != null ) {
            for (ReportEfDTO dto : efDTOS) {
                List<String> ef = new ArrayList<>();
                ef.add(dto.getNameWithUnit());
                ef.add(Float.toString(FloatRounder.round2(dto.getValue(), 0)));
                efStats.add(ef);
            }
        }


        //---------------------------------------------------------------------------------


            years.add(fy);
        tot_direct.add(Float.toString(FloatRounder.round2(direct, 0)));
        tot_indirect.add(Float.toString(FloatRounder.round2(indirect, 0)));
        totals.add(Float.toString(FloatRounder.round2(indirect + direct, 0)));

        List<String> gas_t_dir = new ArrayList<>();
        List<String> gas_t_ind = new ArrayList<>();

        gas_t_dir.add(Float.toString(FloatRounder.round2(t_dir_tco2, 0)));
        gas_t_dir.add(Float.toString(FloatRounder.round2(t_dir_co2, 0)));
        gas_t_dir.add(Float.toString(FloatRounder.round2(t_dir_ch4, 0)));
        gas_t_dir.add(Float.toString(FloatRounder.round2(t_dir_n20, 0)));

        gas_t_ind.add(Float.toString(FloatRounder.round2(t_ind_tco2, 0)));
        gas_t_ind.add(Float.toString(FloatRounder.round2(t_ind_co2, 0)));
        gas_t_ind.add(Float.toString(FloatRounder.round2(t_ind_ch4, 0)));
        gas_t_ind.add(Float.toString(FloatRounder.round2(t_ind_n20, 0)));

        gasTotals.add(Float.toString(FloatRounder.round2(t_dir_tco2 + t_ind_tco2, 0)));
        gasTotals.add(Float.toString(FloatRounder.round2(t_dir_co2 + t_ind_co2, 0)));
        gasTotals.add(Float.toString(FloatRounder.round2(t_dir_ch4 + t_ind_ch4, 0)));
        gasTotals.add(Float.toString(FloatRounder.round2(t_dir_n20 + t_ind_n20, 0)));




        HashMap<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("activity", activityData);
        rtnMap.put("exclusion", excludedData);
        rtnMap.put("direct", directData );
        rtnMap.put("indirect", indirectData);
        rtnMap.put("ghg_com_indirect",directCom);
        rtnMap.put("ghg_com_direc", indirectCom);
        rtnMap.put("ghg_com_years", years);
        rtnMap.put("ghg_com_t_dir", tot_direct);
        rtnMap.put("ghg_com_t_indir", tot_indirect);
        rtnMap.put("ghg_com_t_total", totals);
        rtnMap.put("cat2", cat2);
        rtnMap.put("cat3", cat3);
        rtnMap.put("cat4", cat4);
        rtnMap.put("cat5", cat5);
        rtnMap.put("cat6", cat6);
        rtnMap.put("td_cat", td_cat);
        rtnMap.put("gas_direct", gasDirect);
        rtnMap.put("gas_direct_total", gas_t_dir);
        rtnMap.put("gas_indirect_total", gas_t_ind);
        rtnMap.put("gas_total", gasTotals);
        rtnMap.put("gas_total", gasTotals);
        rtnMap.put("efstats", efStats);


        return rtnMap;
    }


}
