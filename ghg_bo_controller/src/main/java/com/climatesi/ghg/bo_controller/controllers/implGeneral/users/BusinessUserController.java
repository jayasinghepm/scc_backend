package com.climatesi.ghg.bo_controller.controllers.implGeneral.users;

import com.climatesi.ghg.api.UserFactory;
import com.climatesi.ghg.api.facades.UserLoginManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.*;
import com.climatesi.ghg.bo_messaging.implGeneral.ws_message.WebNotificationDTO;
import com.climatesi.ghg.business_users.api.BusinessUserFactory;
import com.climatesi.ghg.business_users.api.beans.*;
import com.climatesi.ghg.business_users.api.enums.CadminStatus;
import com.climatesi.ghg.business_users.api.facades.*;
import com.climatesi.ghg.business_users.implGeneral.beans.*;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class BusinessUserController extends AbstractController {

    private static final Logger logger = Logger.getLogger(BusinessUserController.class);
    private EmployeerManager employeerManager;
    private AdminManager adminManager;
    private CadminManager cadminManager;
    private ClientManager clientManager;
    private DataEntryUserManager dataEntryUserManager;
    private UserLoginManager loginManager;

    @Inject
    private Event<WebNotificationDTO> notificationSrcEvent;

    @PostConstruct
    @Override
    public void initialize() {
        try {
            employeerManager = BusinessUserFactory.getInstance().getEmployeerManager(em);
            adminManager = BusinessUserFactory.getInstance().getAdminManager(em);
            cadminManager = BusinessUserFactory.getInstance().getCadminManager(em);
            clientManager = BusinessUserFactory.getInstance().getClientManager(em);
            dataEntryUserManager = BusinessUserFactory.getInstance().getDataEntryUserManager(em);
            loginManager = UserFactory.getInstance().getUserLoginManager(em);
        } catch (Exception e) {
            logger.error("Error in intializing BusinessUserController", e);
        }
    }

    public Employee findEmployeeById(int id) {
        return employeerManager.getEntityByKey(id);
    }

    public Admin findAdminById(int id) {
        return adminManager.getEntityByKey(id);
    }

    public ComAdmin findCadminById(int id) {
        return cadminManager.getEntityByKey(id);
    }

    public Client findClientById(int id) {
        return clientManager.getEntityByKey(id);
    }

    public DataEntryUser findDataEntryUser(int id) {
        return dataEntryUserManager.getEntityByKey(id);
    }

    public ListResult<Employee> getEmployeesFiltered(int pageNum, Object sorting, Object filter) {
        return employeerManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public ListResult<Client> getClientsFiltered(int pageNum, Object sorting, Object filter) {
        return clientManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public ListResult<Admin> getAdminsFiltered(int pageNum, Object sorting, Object filter) {
        return adminManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public ListResult<ComAdmin> getComAdminsFiltered(int pageNum, Object sorting, Object filter) {
        return cadminManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public ListResult<DataEntryUser> getDataEntryUsersFiltered(int pageNum, Object sorting, Object filter) {
        return dataEntryUserManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }



    public Employee editEmployee(EmployeeDTO dto) throws GHGException {
        Employee entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findEmployeeById(dto.getId());
                if (entry == null) {
                    throw new GHGException("Employee is not found");
                }
                EmployeePopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id
                status = (String) employeerManager.updateEntity(entry);

//         add new
            } else {
                entry = new EmployeeBean();
                EmployeePopulator.getInstance().populate(entry, dto);
                entry.setAddedDate(new Date());

//          Todo: add user id
                status = (String) employeerManager.addEntity(entry);

            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public Client editCliente(ClientDTO dto) throws GHGException {
        Client entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findClientById(dto.getId());
                if (entry == null) {
                    throw new GHGException("Client is not found");
                }
                ClientPopulator.getInstance().populate(entry, dto);
//                entry.se(new Date());
                if(dto.isDeleted() ==1) {
                    deleteClient(dto.getId());
                    return entry;
                }
                //          Todo: add user id
                status = (String) clientManager.updateEntity(entry);

//         add new
            } else {
                entry = new ClientBean();
                ClientPopulator.getInstance().populate(entry, dto);
                entry.setAddedDate(new Date());
                List<String> entitlementsNew = new ArrayList<>();
                entitlementsNew.add("1-0-0-0");
                entitlementsNew.add("2-0-0-0");
                entitlementsNew.add("3-0-0-0");
                entitlementsNew.add("4-0-0-0");
                entitlementsNew.add("5-0-0-0");
                entitlementsNew.add("6-0-0-0");
                entitlementsNew.add("7-0-0-0");
                entitlementsNew.add("8-0-0-0");
                entitlementsNew.add("9-0-0-0");
                entitlementsNew.add("10-0-0-0");
                entitlementsNew.add("11-0-0-0");
                entitlementsNew.add("12-0-0-0");
                entitlementsNew.add("13-0-0-0");
                entitlementsNew.add("14-0-0-0");
                entitlementsNew.add("15-0-0-0");
                entitlementsNew.add("16-0-0-0");
                entitlementsNew.add("17-0-0-0");
                entitlementsNew.add("18-0-0-0");
                entitlementsNew.add("19-0-0-0");
                entitlementsNew.add("20-0-0-0");
                entitlementsNew.add("21-0-0-0");
                entitlementsNew.add("22-0-0-0");
                entitlementsNew.add("23-0-0-0");
                entitlementsNew.add("24-0-0-0");
                entitlementsNew.add("25-0-0-0");
                entry.setEntitlements(entitlementsNew);

//          Todo: add user id
                status = (String) clientManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public Admin editAdmin(AdminDTO dto) throws GHGException {
        Admin entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findAdminById(dto.getId());
                if (entry == null) {
                    throw new GHGException("Admin is not found");
                }
                AdminPopulator.getInstance().populate(entry, dto);
//                entry.setLastUpdatedDate(new Date());
                if(dto.isDeleted() == 1) {
                    deleteAdmin(dto.getId());
                    return entry;
                }
                //          Todo: add user id
                status = (String) adminManager.updateEntity(entry);

//         add new
            } else {
                entry = new AdminBean();
                AdminPopulator.getInstance().populate(entry, dto);
                List<String> entitlementsNew = new ArrayList<>();
                entitlementsNew.add("1-1-1-1");
                entitlementsNew.add("2-1-1-1");
                entitlementsNew.add("3-1-1-1");
                entitlementsNew.add("4-1-1-1");
                entitlementsNew.add("5-1-1-1");
                entitlementsNew.add("6-1-1-1");
                entitlementsNew.add("7-1-1-1");
                entitlementsNew.add("8-1-1-1");
                entitlementsNew.add("9-1-1-1");
                entitlementsNew.add("10-1-1-1");
                entitlementsNew.add("11-1-1-1");
                entitlementsNew.add("12-1-1-1");
                entry.setEntitlements(entitlementsNew);
                entry.setAddedDate(new Date());

//          Todo: add user id
                status = (String) adminManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public ComAdmin editComAdmin(CadminDTO dto) throws GHGException {
        ComAdmin entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findCadminById(dto.getId());
                if (dto.isDeleted() ==1) {
                    deleteCadmin(dto.getId());
                    return entry;
                }

                if(entry.getStatus() != dto.getStatus() && dto.getStatus() == 2) {
                    fireNotification(
                            entry.getFirstName() + " " + entry.getLastName() + " " + "submitted basic info for reviewing",
                            "",
                            "",
                            true,
                            false,
                            false,
                            entry.getCompanyId()
                    );
                }
                if(entry.getStatus() != dto.getStatus() && dto.getStatus() == 3) {
                    fireNotification( "",
                            "Climate SI approved submitted infomation.",
                            "",
                            false,
                            true,
                            false,
                            entry.getCompanyId()
                    );
                }
                if(entry.getStatus() != dto.getStatus() && dto.getStatus() == 4) {
                    fireNotification( "",
                            "Climate SI wants more  information.",
                            "",
                            false,
                            true,
                            false,
                            entry.getCompanyId()
                    );
                }
                if (entry == null) {
                    throw new GHGException("ComAdmin is not found");
                }
//                entry.setLastUpdatedDate(new Date());
                ComAdminPopulator.getInstance().populate(entry, dto);
                //          Todo: add user id
                status = (String) cadminManager.updateEntity(entry);


//         add new
            } else {
                entry = new ComAdminBean();
                ComAdminPopulator.getInstance().populate(entry, dto);
                entry.setStatus(CadminStatus.NOT_SUBMITTED);
                entry.setAddedDate(new Date());
                List<String> entitlementsNew = new ArrayList<>();
                entitlementsNew.add("1-0-0-0");
                entitlementsNew.add("2-0-0-0");
                entitlementsNew.add("3-0-0-0");
                entitlementsNew.add("4-0-0-0");
                entitlementsNew.add("5-0-0-0");
                entitlementsNew.add("6-0-0-0");
                entitlementsNew.add("7-0-0-0");
                entitlementsNew.add("8-0-0-0");
                entitlementsNew.add("9-0-0-0");
                entitlementsNew.add("10-0-0-0");
                entitlementsNew.add("11-0-0-0");
                entitlementsNew.add("12-0-0-0");
                entitlementsNew.add("13-0-0-0");
                entitlementsNew.add("14-0-0-0");
                entitlementsNew.add("15-0-0-0");
                entitlementsNew.add("16-0-0-0");
                entitlementsNew.add("17-0-0-0");
                entitlementsNew.add("18-0-0-0");
                entitlementsNew.add("19-0-0-0");
                entitlementsNew.add("20-0-0-0");
                entitlementsNew.add("21-0-0-0");
                entitlementsNew.add("22-0-0-0");
                entitlementsNew.add("23-0-0-0");
                entitlementsNew.add("24-0-0-0");
                entitlementsNew.add("25-0-0-0");
                entry.setEntitlements(entitlementsNew);
//          Todo: add user id
                status = (String) cadminManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public DataEntryUser editDataEntryUser(DataEntryUserDTO dto) throws GHGException {
        DataEntryUser entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findDataEntryUser(dto.getId());

                if (entry == null) {
                    throw new GHGException("DataEntryUser is not found");
                }
//                entry.setLastUpdatedDate(new Date());
                DataEntryUserPopulator.getInstance().populate(entry, dto);
                if (dto.getIsDeleted() ==1) {
                    deleteDataEntryUser(dto.getId());
                    return entry;
                }
                //          Todo: add user id
                status = (String) dataEntryUserManager.updateEntity(entry);


//         add new
            } else {
                entry = new DataEntryUserBean();
                DataEntryUserPopulator.getInstance().populate(entry, dto);
//                entry.setStatus(CadminStatus.NOT_SUBMITTED);
                entry.setAddedDate(new Date());
                List<String> entitlementsNew = new ArrayList<>();
                entitlementsNew.add("1-0-0-0");
                entitlementsNew.add("2-0-0-0");
                entitlementsNew.add("3-0-0-0");
                entitlementsNew.add("4-0-0-0");
                entitlementsNew.add("5-0-0-0");
                entitlementsNew.add("6-0-0-0");
                entitlementsNew.add("7-0-0-0");
                entitlementsNew.add("8-0-0-0");
                entitlementsNew.add("9-0-0-0");
                entitlementsNew.add("10-0-0-0");
                entitlementsNew.add("11-0-0-0");
                entitlementsNew.add("12-0-0-0");
                entitlementsNew.add("13-0-0-0");
                entitlementsNew.add("14-0-0-0");
                entitlementsNew.add("15-0-0-0");
                entitlementsNew.add("16-0-0-0");
                entitlementsNew.add("17-0-0-0");
                entitlementsNew.add("18-0-0-0");
                entitlementsNew.add("19-0-0-0");
                entitlementsNew.add("20-0-0-0");
                entitlementsNew.add("21-0-0-0");
                entitlementsNew.add("22-0-0-0");
                entitlementsNew.add("23-0-0-0");
                entitlementsNew.add("24-0-0-0");
                entitlementsNew.add("25-0-0-0");
                entry.setEntitlements(entitlementsNew);
//          Todo: add user id
                status = (String) dataEntryUserManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public int deleteClient(int clientId) {
        try {
            Client client = clientManager.getEntityByKey(clientId);
            int loginId = client.getUserId();
            loginManager.deleteLoginProfile(loginId);
            return clientManager.deleteClient(clientId);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }

    public int deleteDataEntryUser(int deuId) {
        try {
            DataEntryUser dau = dataEntryUserManager.getEntityByKey(deuId);
            int loginId = dau.getUserId();
            loginManager.deleteLoginProfile(loginId);
            return dataEntryUserManager.deleteDataEntryUser(deuId);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }

    public int deleteAdmin(int adminId) {
        try {
            Admin admin = adminManager.getEntityByKey(adminId);
            int loginId = admin.getUserId();
            loginManager.deleteLoginProfile(loginId);
            return adminManager.deletAdmin(adminId);
        }catch(Exception e) {
            logger.error(e);
        }
        return 0;
    }

    public int deleteCadmin(int cadminId) {
        try {
            ComAdmin cadmin = cadminManager.getEntityByKey(cadminId);
            int loginId = cadmin.getUserId();
            loginManager.deleteLoginProfile(loginId);
            return cadminManager.deleteCadmin(cadminId);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }

    public void fireNotification(String adminMsg, String cadminMsg, String clientMsg, boolean admin, boolean cadmin, boolean client, int comId){
        JsonObject cadminFilter = new JsonObject();
        cadminFilter.addProperty("value", comId);
        cadminFilter.addProperty("col",1);
        cadminFilter.addProperty("type",1);
        JsonObject filter1 = new JsonObject();
        filter1.add("companyId", cadminFilter);

        JsonObject clientFilter = new JsonObject();
        clientFilter.addProperty("value", comId);
        clientFilter.addProperty("type",1);
        clientFilter.addProperty("col",1);
        JsonObject filter2 = new JsonObject();
        filter2.add("companyId", clientFilter);

        List<ComAdmin> comAdmins =  cadminManager.getPaginatedEntityListByFilter(-1, "",filter1).getList();
        List<Admin> admins = adminManager.getPaginatedEntityListByFilter(-1, "", "").getList();
        List<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", filter2).getList();

        String comName = "";
        if (comAdmins != null && cadmin) {
            WebNotificationDTO notificationDTO = new WebNotificationDTO();
            notificationDTO.setMessage(cadminMsg);
            notificationDTO.setRead(1);
            for (ComAdmin c : comAdmins) {
                comName = c.getCompany();
                notificationDTO.setUserId(c.getUserId());
                notificationSrcEvent.fire(notificationDTO);
            }
        }
        logger.info("Admins: " + admins.size());
        if (admins != null && admin) {
            WebNotificationDTO notificationDTO = new WebNotificationDTO();
            notificationDTO.setMessage(adminMsg);
            notificationDTO.setRead(1);
            for(Admin a : admins) {
                notificationDTO.setUserId(a.getUserId());
                notificationSrcEvent.fire(notificationDTO);
            }
        }
        if (clients != null && client) {
            WebNotificationDTO notificationDTO = new WebNotificationDTO();
            notificationDTO.setMessage(clientMsg);
            notificationDTO.setRead(1);
            for (Client c : clients) {
                notificationDTO.setUserId(c.getUserId());
                notificationSrcEvent.fire(notificationDTO);
            }
        }

    }

}
