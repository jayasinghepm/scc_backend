package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.FireExtingEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.FireExtingEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class FireExtingPopulator {

    private static Logger logger = Logger.getLogger(FireExtingEntry.class);
    private static FireExtingPopulator instance = null;

    public static FireExtingPopulator getInstance() {
        if (instance == null) {
            instance = new FireExtingPopulator();
        }
        return instance;
    }


    public FireExtingEntryDTO populateDTO(FireExtingEntry entry) {
        FireExtingEntryDTO dto = new FireExtingEntryDTO();
        dto.setFireExtEntryId(entry.getFireExtEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setEntryAddedDate(entry.getEntryAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setWeight(entry.getWeight());
        dto.setAmountRefilled(entry.getAmountRefilled());
        dto.setFireExtinguisherType(entry.getFireExtinguisherType());
        dto.setNoOfTanks(entry.getNoOfTanks());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setFireExt(entry.getFireExt());
        return dto;
    }

    public FireExtingEntry populate(FireExtingEntry entry, FireExtingEntryDTO dto) {

        try {
            if (dto.getFireExtEntryId() > 0) {
                entry.setFireExtEntryId(dto.getFireExtEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }
            }
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setWeight(dto.getWeight());
            entry.setAmountRefilled(dto.getAmountRefilled());
            entry.setFireExtinguisherType(dto.getFireExtinguisherType());
            entry.setNoOfTanks(dto.getNoOfTanks());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
            entry.setFireExt(dto.getFireExt() == null ? entry.getFireExt() : dto.getFireExt());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
