package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;


import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.BioMassPopulator;
import com.climatesi.ghg.bo_controller.populators.FurnaceOilPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.FurnaceOilDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.FurnaceOil;
import com.climatesi.ghg.emission_source.api.beans.FurnaceOil;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.FurnaceOilEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.BioMassEntryManagerFacade;
import com.climatesi.ghg.emission_source.implGeneral.facades.FurnaceEntryManagerFacade;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class FurnaceController   extends AbstractController {

    private static final Logger logger = Logger.getLogger(FurnaceController.class);


    private FurnaceEntryManagerFacade furnaceEntryManagerFacade;
    private EmissionFactorsManager emissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {

        try {
            furnaceEntryManagerFacade = (FurnaceEntryManagerFacade) EmsissionSourceFactory.getInstance().getFurnaceEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing MunicipalWaterController", e);
        }
    }


    public ListResult<FurnaceOil> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return furnaceEntryManagerFacade.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public FurnaceOil edit(FurnaceOilDTO dto) throws GHGException {
        FurnaceOil entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                FurnaceOilPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.getIsDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) furnaceEntryManagerFacade.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new FurnaceOilEntryBean();
                FurnaceOilPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emission(entry));
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) furnaceEntryManagerFacade.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public FurnaceOil findById(Integer id) {
        return furnaceEntryManagerFacade.getEntityByKey(id);
    }

    public HashMap<String, String> emission(FurnaceOil entry) {

        float consumptionLiters = entry.getFuelConsumption();
        float ncv = 0.0404f;
        float efCo2 = 77.4f;
        float efCh4 =  0.804f;
        float efN20 = 0.159f;
        float density  = 0.97f;

        ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_NCV).getValue();
        efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_EF_CO2).getValue();
        efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_EF_CH4).getValue();
        efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_EF_N20).getValue();
        density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_DENSITY).getValue();
        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();


        ResultDTO e1 = Calculator.emissionVehicles(
                consumptionLiters,
                ncv,
                density,
                efCo2,
                efCh4,
                efN20,
                gwp_co2,
                gwp_ch4,
                gwp_n20
        );
        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", consumptionLiters * 0.001) );
        results.put("direct", Boolean.toString(false)); // todo
        results.put("ncv",String.format("%.5f", ncv) );
        results.put("efCO2",String.format("%.5f", efCo2) );
        results.put("efCH4",String.format("%.5f",efCh4) );
        results.put("efN20",String.format("%.5f", efN20) );
        results.put("density",String.format("%.5f", density) );

        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20) );

        results.put("te",String.format("%.5f",e1.totalEnergy) );
        return results;
    }

}
