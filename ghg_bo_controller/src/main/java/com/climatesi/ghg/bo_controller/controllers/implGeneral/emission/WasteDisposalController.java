package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.beans.IncinerationFactor;
import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.constants.IncinerationWasteTypeKeys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.api.facade.IncinerationFactorsManager;
import com.climatesi.ghg.api.facade.WasteDisposalFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.WasteDisposalPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteDisposalEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.WasteDisposalEntry;
import com.climatesi.ghg.emission_source.api.enums.Units;
import com.climatesi.ghg.emission_source.api.enums.WasteDispoalMethods;
import com.climatesi.ghg.emission_source.api.facades.WasteDisposalEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.WasteDisposalEntryBean;
import com.climatesi.ghg.implGeneral.data.IncinerationCalcData;
import com.climatesi.ghg.implGeneral.data.PiggeryCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.implGeneral.data.WasteDisposalCalcData;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class WasteDisposalController extends AbstractController {
    private static final Logger logger = Logger.getLogger(WasteDisposalController.class);
    private WasteDisposalEntryManager wasteDisposalEntryManager;
    private WasteDisposalFactorsManager wasteDisposalFactorsManager;
    private EmissionFactorsManager emissionFactorsManager;
    private IncinerationFactorsManager incinerationFactorsManager;




    @PostConstruct
    @Override
    public void initialize() {
        try {
            wasteDisposalFactorsManager = EmissionCalcFacotory.getInstance().getWasteDisposalFactorsManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);
            incinerationFactorsManager = EmissionCalcFacotory.getInstance().getIncinerationFactorsManager(em);

            wasteDisposalEntryManager = EmsissionSourceFactory.getInstance().getWasteDisposalEntryManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing WasteDisposalController", e);
        }
    }

    public ListResult<WasteDisposalEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return wasteDisposalEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public WasteDisposalEntry editWasteDisposalEntry(WasteDisposalEntryDTO dto) throws GHGException {
        WasteDisposalEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                WasteDisposalPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) wasteDisposalEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new WasteDisposalEntryBean();
                WasteDisposalPopulator.getInstance().populate(entry, dto);
                entry.setEntryAddedDate(new Date());
                entry.setEmissionDetails(emission(entry));

//          Todo: add user id
                status = (String) wasteDisposalEntryManager.addEntity(entry);
            }
//        todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public WasteDisposalEntry findById(Integer id) {
        return wasteDisposalEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emission(WasteDisposalEntry o) {
        ResultDTO emission = null;

        HashMap<String, String> results = new HashMap<>();

        float ef_feedrate = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.AVG_FEED_RATE_PER_PIG).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();

        float weight = 0;

        WasteDisposalCalcData calcData = new WasteDisposalCalcData();
        if (o != null) {

            int wasteType = o.getWasteType();
            int wasteDisposalMethod = o.getDisposalMethod();
            WasteDisposalFactors factors = wasteDisposalFactorsManager.getEntityByKey(wasteType);

            if (o.getDisposalMethod() == WasteDispoalMethods.Reuse.method) {
                results.put("ef",String.format("%.5f", factors.getReuse()) );
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() / 1000);
//                    weight += o.getAmountDisposed() / 1000;
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() / 1000 ) );
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed();
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );
//                }
                calcData.setWeight_waste(o.getAmountDisposed());
                weight += o.getAmountDisposed();
                results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );

                results.put("ef",String.format("%.5f", factors.getReuse() ) );
                calcData.setCo2_waste_method(factors.getReuse());
                emission = Calculator.emissionWasteDisposal(calcData);
            } else if (o.getDisposalMethod() == WasteDispoalMethods.OpenLoop.method) {
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() / 1000);
//                    weight += o.getAmountDisposed() / 1000;
//                    results.put("tons", Float.toString(o.getAmountDisposed() / 1000));
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed();
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );
//                }
                calcData.setWeight_waste(o.getAmountDisposed());
                weight += o.getAmountDisposed();
                results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );

                results.put("ef",String.format("%.5f", factors.getOpen_loop() ) );
                calcData.setCo2_waste_method(factors.getOpen_loop());
                emission = Calculator.emissionWasteDisposal(calcData);
            } else if (o.getDisposalMethod() == WasteDispoalMethods.ClosedLoop.method) {
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() / 1000);
//                    weight += o.getAmountDisposed() / 1000;
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() / 1000 ) );
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed();
//                    results.put("tons",String.format("%.5f",o.getAmountDisposed() ) );
//                }

                calcData.setWeight_waste(o.getAmountDisposed());
                weight += o.getAmountDisposed();
                results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );

                results.put("ef",String.format("%.5f", factors.getClosed_loop() ) );
                calcData.setCo2_waste_method(factors.getClosed_loop());
                emission = Calculator.emissionWasteDisposal(calcData);
            } else if (o.getDisposalMethod() == WasteDispoalMethods.Combustion.method) {
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() / 1000);
//                    weight += o.getAmountDisposed() / 1000;
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed();
//                }
                calcData.setWeight_waste(o.getAmountDisposed());
                weight += o.getAmountDisposed();
                results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );

                results.put("ef",String.format("%.5f", factors.getCombustion()) );
                calcData.setCo2_waste_method(factors.getCombustion());
                emission = Calculator.emissionWasteDisposal(calcData);
            } else if (o.getDisposalMethod() == WasteDispoalMethods.Composting.method) {
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() / 1000);
//                    weight += o.getAmountDisposed() / 1000;
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed();
//                }
                calcData.setWeight_waste(o.getAmountDisposed());
                weight += o.getAmountDisposed();
                results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );

                results.put("ef",String.format("%.5f", factors.getComposting() ) );
                calcData.setCo2_waste_method(factors.getComposting());
                emission = Calculator.emissionWasteDisposal(calcData);
            } else if (o.getDisposalMethod() == WasteDispoalMethods.LandFill.method) {
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() / 1000);
//                    weight += o.getAmountDisposed() / 1000;
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() / 1000) );
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed();
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );
//                }
                calcData.setWeight_waste(o.getAmountDisposed());
                weight += o.getAmountDisposed();
                results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );

                results.put("ef",String.format("%.5f", factors.getLandfill() ) );
                calcData.setCo2_waste_method(factors.getLandfill());
                emission = Calculator.emissionWasteDisposal(calcData);
            } else if (o.getDisposalMethod() == WasteDispoalMethods.Anaerobic_Digestion.method) {
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() / 1000);
//                    weight += o.getAmountDisposed() / 1000;
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() / 1000 ) );
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed();
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed()) );
//                }
                calcData.setWeight_waste(o.getAmountDisposed());
                weight += o.getAmountDisposed();
                results.put("tons",String.format("%.5f", o.getAmountDisposed()) );


                results.put("ef",String.format("%.5f", factors.getAnaerobic_digestion()) );
                calcData.setCo2_waste_method(factors.getAnaerobic_digestion());
                emission = Calculator.emissionWasteDisposal(calcData);
            } else if (o.getDisposalMethod() == WasteDispoalMethods.Piggery.method) {
//                if (o.getUnits() == Units.kg.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed());
//                    weight += o.getAmountDisposed() / 1000;
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() / 1000 ) );
//                } else if (o.getUnits() == Units.tons.unit) {
//                    calcData.setWeight_waste(o.getAmountDisposed() * 1000);
//                    weight += o.getAmountDisposed();
//                    results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );
//                }
//                calcData.setWeight_waste(o.getAmountDisposed() * 1000);
//                weight += o.getAmountDisposed();
//                results.put("tons",String.format("%.5f", o.getAmountDisposed() ) );

                PiggeryCalcData piggeryCalcData = new PiggeryCalcData();
                piggeryCalcData.setFeed_rate(ef_feedrate);
                piggeryCalcData.setGwp_ch4(gwp_ch4);
                piggeryCalcData.setNum_of_days(365);
                piggeryCalcData.setWeight(o.getAmountDisposed() * 1000);
                emission = Calculator.emissionPiggery(piggeryCalcData);
                results.put("gwp",String.format("%.5f", gwp_ch4 ) );
                results.put("feedrate",String.format("%.5f", ef_feedrate ) );
            } else if (o.getDisposalMethod() == WasteDispoalMethods.Incineration.method) {
               IncinerationFactor factor = incinerationFactorsManager.getEntityByKey(o.getWasteType());
               if (factor != null) {
                   IncinerationCalcData data = new IncinerationCalcData();
                   data.setCarbon_fraction(factor.getCarbon_fraction());
                   data.setDry_content_percen(factor.getDry_matter_per());
                   data.setFossil_carbon_fraction(factor.getFossil_carbon_fraction());
                   data.setWeight_solid_waste(o.getAmountDisposed());
                   data.setWeightwaste(o.getAmountDisposed());

                   results.put("c_f",String.format("%.5f", factor.getCarbon_fraction() ) );
                   results.put("dry_per",String.format("%.5f", factor.getDry_matter_per() ) );
                   results.put("fossil_carbon",String.format("%.5f", factor.getFossil_carbon_fraction()) );
                   results.put("tons",String.format("%.5f", o.getAmountDisposed()) );
                   data.setOxidation_factor(100);
                   if (o.getWasteType() == IncinerationWasteTypeKeys.DOMESTIC || o.getWasteType() == IncinerationWasteTypeKeys.INDUSTRIAL) {
                       data.setEf_ch4((9.7f/ 1000000f));
                       data.setEf_n20(60f/1000000f);
                       results.put("ef_ch4", "9.7 g/Tons");
                       results.put("ef_n20", "60 g/Tons");
                   }else {
                       data.setEf_ch4(60f/1000000f);
                       data.setEf_n20(900f/1000000f);
                       results.put("ef_ch4", "60 kg/Tons");
                       results.put("ef_n20", "900 g/Tons");
                   }

                   emission = Calculator.emissionIncineration(data);
               }

            }


        }

        results.put("tco2",String.format("%.5f", emission.tco2e ) );
        results.put("co2",String.format("%.5f", emission.co2 ) );
        results.put("n20",String.format("%.5f", emission.n2o ) );
        results.put("ch4",String.format("%.5f", emission.ch4 ) );
        results.put("quantity",String.format("%.5f", o.getAmountDisposed() ) );
        results.put("direct", Boolean.toString(false));
        return results;
    }
}
