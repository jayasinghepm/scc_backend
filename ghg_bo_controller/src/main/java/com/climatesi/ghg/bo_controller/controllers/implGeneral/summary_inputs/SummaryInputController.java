package com.climatesi.ghg.bo_controller.controllers.implGeneral.summary_inputs;

import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.api.facades.BranchManager;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.*;
import com.climatesi.ghg.emission_source.api.beans.summary.EmissionSrcDataEntryStatus;
import com.climatesi.ghg.emission_source.api.enums.*;
import com.climatesi.ghg.emission_source.api.facades.*;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.ProjectDataEntryStatusIdBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.EmissionSrcDataEntryStatusBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.SeaAirFreightFacadeManager;
import com.climatesi.ghg.implGeneral.facades.SeaAirFreightPortFacadeManger;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Stateless
public class SummaryInputController extends AbstractController {

    private static Logger logger = Logger.getLogger(SummaryInputController.class);
    private MunicipalWaterEntryManager municipalWaterEntryManager;
    private ElectricityEntryManager electricityEntryManager;
    private GeneratorsEntryManager generatorsEntryManager;
    private RefrigerantsEntryManager refrigerantsEntryManager;
    private FireExtingEntryManager fireExtingEntryManager;
    private WasteTransportEntryManager wasteTransportEntryManager;
    private EmployeCommutingEntryManager employeCommutingEntryManager;
    private VehicleEntryManager vehicleEntryManager;
    private WasteDisposalEntryManager wasteDisposalEntryManager;
    private TransportLocalEntryManager transportLocalEntryManager;
    private BusinessAirTravelEntryManager businessAirTravelEntryManager;
    private CompanyManager companyManager;
    private BranchManager branchManager;
    private EmissionSrcDataEntryStatusManager  emissionSrcDataEntryStatusManager;
    private SeaAirFreightFacadeManager seaAirFreightFacadeManager;
    private LPGEntryEntryManager lpgEntryEntryManager;
    private BioMassEntryManager bioMassEntryManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            bioMassEntryManager = EmsissionSourceFactory.getInstance().getBioMassEntryManager(em);
            lpgEntryEntryManager = EmsissionSourceFactory.getInstance().getLPGEntryEntryManager(em);
            transportLocalEntryManager = EmsissionSourceFactory.getInstance().getTransportLocalEntryManager(em);
            municipalWaterEntryManager = EmsissionSourceFactory.getInstance().getMunicipalWaterEntryManager(em);
            electricityEntryManager = EmsissionSourceFactory.getInstance().getElectricityEntryManager(em);
            generatorsEntryManager = EmsissionSourceFactory.getInstance().getGeneratorsEntryManager(em);
            refrigerantsEntryManager = EmsissionSourceFactory.getInstance().getRefrigerantsEntryManager(em);
            fireExtingEntryManager = EmsissionSourceFactory.getInstance().getFireExtingEntryManager(em);
            wasteTransportEntryManager = EmsissionSourceFactory.getInstance().getWasteTransportEntryManager(em);
            employeCommutingEntryManager = EmsissionSourceFactory.getInstance().getEmployeeCommEntryManager(em);
            vehicleEntryManager = EmsissionSourceFactory.getInstance().getVehicleEntryManager(em);
            wasteDisposalEntryManager = EmsissionSourceFactory.getInstance().getWasteDisposalEntryManager(em);
            businessAirTravelEntryManager = EmsissionSourceFactory.getInstance().getBusinessAirTravelEntryManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            emissionSrcDataEntryStatusManager = EmsissionSourceFactory.getInstance().getEmissionSrcDataEntryManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            seaAirFreightFacadeManager = EmsissionSourceFactory.getInstance().getSeaAirFreightFacadeManager(em);
        } catch (Exception e) {
            logger.error("Error in intializing Emission Calc Controller", e);
        }
    }



    public HashMap<String, HashMap<String, String> >  biomassSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<BioMassEntry> list = bioMassEntryManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_meterNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("weight_in", Float.toString(0));
        jan_summary.put("weight_out", Float.toString(0));
        jan_summary.put("quantity", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_meterNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("weight_in", Float.toString(0));
        feb_summary.put("weight_out", Float.toString(0));
        feb_summary.put("quantity", Float.toString(0));

        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_meterNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("weight_in", Float.toString(0));
        mar_summary.put("weight_out", Float.toString(0));
        mar_summary.put("quantity", Float.toString(0));

        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_meterNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("weight_in", Float.toString(0));
        apr_summary.put("weight_out", Float.toString(0));
        apr_summary.put("quantity", Float.toString(0));

        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_meterNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("weight_in", Float.toString(0));
        may_summary.put("weight_out", Float.toString(0));
        may_summary.put("quantity", Float.toString(0));

        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_meterNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("weight_in", Float.toString(0));
        jun_summary.put("weight_out", Float.toString(0));
        jun_summary.put("quantity", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_meterNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("weight_in", Float.toString(0));
        jul_summary.put("weight_out", Float.toString(0));
        jul_summary.put("quantity", Float.toString(0));

        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_meterNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("weight_in", Float.toString(0));
        aug_summary.put("weight_out", Float.toString(0));
        aug_summary.put("quantity", Float.toString(0));

        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_meterNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("weight_in", Float.toString(0));
        sep_summary.put("weight_out", Float.toString(0));
        sep_summary.put("quantity", Float.toString(0));

        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_meterNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("weight_in", Float.toString(0));
        oct_summary.put("weight_out", Float.toString(0));
        oct_summary.put("quantity", Float.toString(0));

        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_meterNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("weight_in", Float.toString(0));
        nov_summary.put("weight_out", Float.toString(0));
        nov_summary.put("quantity", Float.toString(0));

        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_meterNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("weight_in", Float.toString(0));
        dec_summary.put("weight_out", Float.toString(0));
        dec_summary.put("quantity", Float.toString(0));

        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_meterNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("weight_in", Float.toString(0));
        all_summary.put("weight_out", Float.toString(0));
        all_summary.put("quantity", Float.toString(0));

        for (BioMassEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {

                        jan_summary.put("na", Boolean.toString(false));

                        jan_summary.put("quantity", Float.toString(Float.parseFloat(jan_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 1: {

                        feb_summary.put("na", Boolean.toString(false));

                        feb_summary.put("quantity", Float.toString(Float.parseFloat(feb_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 2: {

                        mar_summary.put("na", Boolean.toString(false));
                        mar_summary.put("quantity", Float.toString(Float.parseFloat(mar_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 3: {

                        apr_summary.put("na", Boolean.toString(false));
                        apr_summary.put("quantity", Float.toString(Float.parseFloat(apr_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 4: {

                        may_summary.put("na", Boolean.toString(false));

                        may_summary.put("quantity", Float.toString(Float.parseFloat(may_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 5: {

                        jun_summary.put("na", Boolean.toString(false));

                        jun_summary.put("quantity", Float.toString(Float.parseFloat(jun_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 6: {

                        jul_summary.put("na", Boolean.toString(false));

                        jul_summary.put("quantity", Float.toString(Float.parseFloat(jul_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 7: {

                        aug_summary.put("na", Boolean.toString(false));

                        aug_summary.put("quantity", Float.toString(Float.parseFloat(aug_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 8: {

                        sep_summary.put("na", Boolean.toString(false));
                        sep_summary.put("quantity", Float.toString(Float.parseFloat(sep_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 9: {

                        oct_summary.put("na", Boolean.toString(false));
                        oct_summary.put("quantity", Float.toString(Float.parseFloat(oct_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 10: {

                        nov_summary.put("na", Boolean.toString(false));
                        nov_summary.put("quantity", Float.toString(Float.parseFloat(nov_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 11: {

                        dec_summary.put("na", Boolean.toString(false));
                        dec_summary.put("quantity", Float.toString(Float.parseFloat(dec_summary.get("quantity")) +o.getQuantity() * 0.001f));

                        break;
                    }
                    case 12: {

                        all_summary.put("na", Boolean.toString(false));

                        all_summary.put("quantity", Float.toString(Float.parseFloat(all_summary.get("quantity")) +o.getQuantity() * 0.001f));


                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();

        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }


    public HashMap<String, HashMap<String, String> > lpgSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<LPGEntry> list = lpgEntryEntryManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_meterNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("quantity", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_meterNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("quantity", Float.toString(0));


        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_meterNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("quantity", Float.toString(0));


        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_meterNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("quantity", Float.toString(0));


        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_meterNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("quantity", Float.toString(0));


        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_meterNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("quantity", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_meterNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("quantity", Float.toString(0));


        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_meterNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("quantity", Float.toString(0));


        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_meterNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("quantity", Float.toString(0));


        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_meterNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("quantity", Float.toString(0));


        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_meterNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("quantity", Float.toString(0));


        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_meterNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("quantity", Float.toString(0));


        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_meterNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("quantity", Float.toString(0));


        for (LPGEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {

                        jan_summary.put("na", Boolean.toString(false));
                        jan_summary.put("quantity", Float.toString(Float.parseFloat(jan_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 1: {

                        feb_summary.put("na", Boolean.toString(false));
                        feb_summary.put("quantity", Float.toString(Float.parseFloat(feb_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 2: {

                        mar_summary.put("na", Boolean.toString(false));
                        mar_summary.put("quantity", Float.toString(Float.parseFloat(mar_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 3: {

                        apr_summary.put("na", Boolean.toString(false));
                        apr_summary.put("quantity", Float.toString(Float.parseFloat(apr_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 4: {

                        may_summary.put("na", Boolean.toString(false));
                        may_summary.put("quantity", Float.toString(Float.parseFloat(may_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 5: {

                        jun_summary.put("na", Boolean.toString(false));
                        jun_summary.put("quantity", Float.toString(Float.parseFloat(jun_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 6: {

                        jul_summary.put("na", Boolean.toString(false));
                        jul_summary.put("quantity", Float.toString(Float.parseFloat(jul_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 7: {

                        aug_summary.put("na", Boolean.toString(false));
                        aug_summary.put("quantity", Float.toString(Float.parseFloat(aug_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 8: {

                        sep_summary.put("na", Boolean.toString(false));
                        sep_summary.put("quantity", Float.toString(Float.parseFloat(sep_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 9: {

                        oct_summary.put("na", Boolean.toString(false));
                        oct_summary.put("quantity", Float.toString(Float.parseFloat(oct_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 10: {

                        nov_summary.put("na", Boolean.toString(false));
                        nov_summary.put("quantity", Float.toString(Float.parseFloat(nov_summary.get("quantity")) +o.getConsumption() * 0.001f));

                        break;
                    }
                    case 11: {

                        dec_summary.put("na", Boolean.toString(false));
                        dec_summary.put("quantity", Float.toString(Float.parseFloat(dec_summary.get("quantity")) +o.getConsumption() * 0.001f));


                        break;
                    }
                    case 12: {

                        all_summary.put("na", Boolean.toString(false));
                        all_summary.put("quantity", Float.toString(Float.parseFloat(all_summary.get("quantity")) +o.getConsumption() * 0.001f));



                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();

        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }



    public HashMap<String, HashMap<String, String> > freightSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<SeaAirFreightEntry> list = seaAirFreightFacadeManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_meterNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("air_quantity", Float.toString(0));
        jan_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_meterNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("air_quantity", Float.toString(0));
        feb_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_meterNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("air_quantity", Float.toString(0));
        mar_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_meterNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("air_quantity", Float.toString(0));
        apr_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_meterNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("air_quantity", Float.toString(0));
        may_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_meterNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("air_quantity", Float.toString(0));
        jun_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_meterNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("air_quantity", Float.toString(0));
        jul_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_meterNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("air_quantity", Float.toString(0));
        aug_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_meterNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("air_quantity", Float.toString(0));
        sep_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_meterNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("air_quantity", Float.toString(0));
        oct_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_meterNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("air_quantity", Float.toString(0));
        nov_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_meterNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("air_quantity", Float.toString(0));
        dec_summary.put("sea_quantity", Float.toString(0));

        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_meterNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("air_quantity", Float.toString(0));
        all_summary.put("sea_quantity", Float.toString(0));

        for (SeaAirFreightEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {

                        jan_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            jan_summary.put("sea_quantity", Float.toString(Float.parseFloat(jan_summary.get("sea_quantity")) + o.getFreightKg()));

                        }else { // 2 - air
                            jan_summary.put("air_quantity", Float.toString(Float.parseFloat(jan_summary.get("air_quantity")) + o.getFreightKg()));

                        }
                        break;
                    }
                    case 1: {

                        feb_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            feb_summary.put("sea_quantity", Float.toString(Float.parseFloat(feb_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            feb_summary.put("air_quantity", Float.toString(Float.parseFloat(feb_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 2: {

                        mar_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            mar_summary.put("sea_quantity", Float.toString(Float.parseFloat(mar_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            mar_summary.put("air_quantity", Float.toString(Float.parseFloat(mar_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 3: {

                        apr_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            apr_summary.put("sea_quantity", Float.toString(Float.parseFloat(apr_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            apr_summary.put("air_quantity", Float.toString(Float.parseFloat(apr_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 4: {

                        may_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            may_summary.put("sea_quantity", Float.toString(Float.parseFloat(may_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            may_summary.put("air_quantity", Float.toString(Float.parseFloat(may_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 5: {

                        jun_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            jun_summary.put("sea_quantity", Float.toString(Float.parseFloat(jun_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            jun_summary.put("air_quantity", Float.toString(Float.parseFloat(jun_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 6: {

                        jul_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            jul_summary.put("sea_quantity", Float.toString(Float.parseFloat(jul_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            jul_summary.put("air_quantity", Float.toString(Float.parseFloat(jul_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 7: {

                        aug_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            aug_summary.put("sea_quantity", Float.toString(Float.parseFloat(aug_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            aug_summary.put("air_quantity", Float.toString(Float.parseFloat(aug_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 8: {

                        sep_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            sep_summary.put("sea_quantity", Float.toString(Float.parseFloat(sep_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            sep_summary.put("air_quantity", Float.toString(Float.parseFloat(sep_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 9: {

                        oct_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            oct_summary.put("sea_quantity", Float.toString(Float.parseFloat(oct_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            oct_summary.put("air_quantity", Float.toString(Float.parseFloat(oct_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 10: {

                        nov_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            nov_summary.put("sea_quantity", Float.toString(Float.parseFloat(nov_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            nov_summary.put("air_quantity", Float.toString(Float.parseFloat(nov_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 11: {

                        dec_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            dec_summary.put("sea_quantity", Float.toString(Float.parseFloat(dec_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            dec_summary.put("air_quantity", Float.toString(Float.parseFloat(dec_summary.get("air_quantity")) +o.getFreightKg()));

                        }
                        break;
                    }
                    case 12: {

                        all_summary.put("na", Boolean.toString(false));
                        if (o.getIsAirFreight() == 1) { //1-sea
                            all_summary.put("sea_quantity", Float.toString(Float.parseFloat(all_summary.get("sea_quantity")) +o.getFreightKg()));

                        }else { // 2 - air
                            all_summary.put("air_quantity", Float.toString(Float.parseFloat(all_summary.get("air_quantity")) +o.getFreightKg()));

                        }

                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();

        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }


    public HashMap<String, HashMap<String, String> > elecSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<ElectricityEntry> list = electricityEntryManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_meterNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("kwh", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_meterNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("kwh", Float.toString(0));

        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_meterNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("kwh", Float.toString(0));

        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_meterNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("kwh", Float.toString(0));

        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_meterNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("kwh", Float.toString(0));

        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_meterNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("kwh", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_meterNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("kwh", Float.toString(0));

        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_meterNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("kwh", Float.toString(0));

        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_meterNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("kwh", Float.toString(0));

        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_meterNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("kwh", Float.toString(0));

        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_meterNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("kwh", Float.toString(0));

        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_meterNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("kwh", Float.toString(0));

        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_meterNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("kwh", Float.toString(0));

        for (ElectricityEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {
                        jan_meterNums.add(o.getMeterNo());
                        jan_summary.put("na", Boolean.toString(false));
                        jan_summary.put("kwh", Float.toString(Float.parseFloat(jan_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 1: {
                        feb_meterNums.add(o.getMeterNo());
                        feb_summary.put("na", Boolean.toString(false));
                        feb_summary.put("kwh", Float.toString(Float.parseFloat(feb_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 2: {
                        mar_meterNums.add(o.getMeterNo());
                        mar_summary.put("na", Boolean.toString(false));
                        mar_summary.put("kwh", Float.toString(Float.parseFloat(mar_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 3: {
                        apr_meterNums.add(o.getMeterNo());
                        apr_summary.put("na", Boolean.toString(false));
                        apr_summary.put("kwh", Float.toString(Float.parseFloat(apr_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 4: {
                        may_meterNums.add(o.getMeterNo());
                        may_summary.put("na", Boolean.toString(false));
                        may_summary.put("kwh", Float.toString(Float.parseFloat(may_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 5: {
                        jun_meterNums.add(o.getMeterNo());
                        jun_summary.put("na", Boolean.toString(false));
                        jun_summary.put("kwh", Float.toString(Float.parseFloat(jun_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 6: {
                        jul_meterNums.add(o.getMeterNo());
                        jul_summary.put("na", Boolean.toString(false));
                        jul_summary.put("kwh", Float.toString(Float.parseFloat(jul_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 7: {
                        aug_meterNums.add(o.getMeterNo());
                        aug_summary.put("na", Boolean.toString(false));
                        aug_summary.put("kwh", Float.toString(Float.parseFloat(aug_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 8: {
                        sep_meterNums.add(o.getMeterNo());
                        sep_summary.put("na", Boolean.toString(false));
                        sep_summary.put("kwh", Float.toString(Float.parseFloat(sep_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 9: {
                        oct_meterNums.add(o.getMeterNo());
                        oct_summary.put("na", Boolean.toString(false));
                        oct_summary.put("kwh", Float.toString(Float.parseFloat(oct_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 10: {
                        nov_meterNums.add(o.getMeterNo());
                        nov_summary.put("na", Boolean.toString(false));
                        nov_summary.put("kwh", Float.toString(Float.parseFloat(nov_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 11: {
                        dec_meterNums.add(o.getMeterNo());
                        dec_summary.put("na", Boolean.toString(false));
                        dec_summary.put("kwh", Float.toString(Float.parseFloat(dec_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                    case 12: {
                        all_meterNums.add(o.getMeterNo());
                        all_summary.put("na", Boolean.toString(false));
                        all_summary.put("kwh", Float.toString(Float.parseFloat(all_summary.get("kwh")) + o.getConsumption()));
                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        jan_summary.put("nums",  fromListToString(jan_meterNums));
        feb_summary.put("nums", fromListToString(feb_meterNums));
        mar_summary.put("nums", fromListToString(mar_meterNums));
        apr_summary.put("nums",  fromListToString(apr_meterNums));
        may_summary.put("nums",  fromListToString(may_meterNums));
        jun_summary.put("nums",  fromListToString(jun_meterNums));
        jul_summary.put("nums",  fromListToString(jul_meterNums));
        aug_summary.put("nums",  fromListToString(aug_meterNums));
        sep_summary.put("nums",  fromListToString(sep_meterNums));
        oct_summary.put("nums",  fromListToString(oct_meterNums));
        nov_summary.put("nums",  fromListToString(nov_meterNums));
        dec_summary.put("nums",  fromListToString(dec_meterNums));
        all_summary.put("nums",  fromListToString(all_meterNums));
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String> > munWaterSummary(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<MunicipalWaterEntry> list = municipalWaterEntryManager.getCustomFiltered(branchId, companyId, fy);

        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_meterNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("lkr", Float.toString(0));
        jan_summary.put("m3", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_meterNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("lkr", Float.toString(0));
        feb_summary.put("m3", Float.toString(0));

        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_meterNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("lkr", Float.toString(0));
        mar_summary.put("m3", Float.toString(0));

        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_meterNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("lkr", Float.toString(0));
        apr_summary.put("m3", Float.toString(0));

        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_meterNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("lkr", Float.toString(0));
        may_summary.put("m3", Float.toString(0));

        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_meterNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("lkr", Float.toString(0));
        jun_summary.put("m3", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_meterNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("lkr", Float.toString(0));
        jul_summary.put("m3", Float.toString(0));

        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_meterNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("lkr", Float.toString(0));
        aug_summary.put("m3", Float.toString(0));

        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_meterNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("lkr", Float.toString(0));
        sep_summary.put("m3", Float.toString(0));

        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_meterNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("lkr", Float.toString(0));
        oct_summary.put("m3", Float.toString(0));

        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_meterNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("lkr", Float.toString(0));
        nov_summary.put("m3", Float.toString(0));

        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_meterNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("lkr", Float.toString(0));
        dec_summary.put("m3", Float.toString(0));

        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_meterNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("lkr", Float.toString(0));
        all_summary.put("m3", Float.toString(0));

        for (MunicipalWaterEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {
                        jan_meterNums.add(o.getMeterNo());
                        jan_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            jan_summary.put("m3", Float.toString(Float.parseFloat(jan_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            jan_summary.put("lkr", Float.toString(Float.parseFloat(jan_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 1: {
                        feb_meterNums.add(o.getMeterNo());
                        feb_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            feb_summary.put("m3", Float.toString(Float.parseFloat(feb_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            feb_summary.put("lkr", Float.toString(Float.parseFloat(feb_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 2: {
                        mar_meterNums.add(o.getMeterNo());
                        mar_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            mar_summary.put("m3", Float.toString(Float.parseFloat(mar_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            mar_summary.put("lkr", Float.toString(Float.parseFloat(mar_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 3: {
                        apr_meterNums.add(o.getMeterNo());
                        apr_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            apr_summary.put("m3", Float.toString(Float.parseFloat(apr_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            apr_summary.put("lkr", Float.toString(Float.parseFloat(apr_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 4: {
                        may_meterNums.add(o.getMeterNo());
                        may_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            may_summary.put("m3", Float.toString(Float.parseFloat(may_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            may_summary.put("lkr", Float.toString(Float.parseFloat(may_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 5: {
                        jun_meterNums.add(o.getMeterNo());
                        jun_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            jun_summary.put("m3", Float.toString(Float.parseFloat(jun_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            jun_summary.put("lkr", Float.toString(Float.parseFloat(jun_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 6: {
                        jul_meterNums.add(o.getMeterNo());
                        jul_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            jul_summary.put("m3", Float.toString(Float.parseFloat(jul_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            jul_summary.put("lkr", Float.toString(Float.parseFloat(jul_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 7: {
                        aug_meterNums.add(o.getMeterNo());
                        aug_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            aug_summary.put("m3", Float.toString(Float.parseFloat(aug_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            aug_summary.put("lkr", Float.toString(Float.parseFloat(aug_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 8: {
                        sep_meterNums.add(o.getMeterNo());
                        sep_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            sep_summary.put("m3", Float.toString(Float.parseFloat(sep_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            sep_summary.put("lkr", Float.toString(Float.parseFloat(sep_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 9: {
                        oct_meterNums.add(o.getMeterNo());
                        oct_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            oct_summary.put("m3", Float.toString(Float.parseFloat(oct_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            oct_summary.put("lkr", Float.toString(Float.parseFloat(oct_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 10: {
                        nov_meterNums.add(o.getMeterNo());
                        nov_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            nov_summary.put("m3", Float.toString(Float.parseFloat(nov_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            nov_summary.put("lkr", Float.toString(Float.parseFloat(nov_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 11: {
                        dec_meterNums.add(o.getMeterNo());
                        dec_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            dec_summary.put("m3", Float.toString(Float.parseFloat(dec_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            dec_summary.put("lkr", Float.toString(Float.parseFloat(dec_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                    case 12: {
                        all_meterNums.add(o.getMeterNo());
                        all_summary.put("na", Boolean.toString(true));
                        if (o.getUnits() == Units.m3.unit) {
                            all_summary.put("m3", Float.toString(Float.parseFloat(all_summary.get("m3")) + o.getConsumption()));
                        }
                        else if (o.getUnits() == Units.LKR.unit) {
                            all_summary.put("lkr", Float.toString(Float.parseFloat(all_summary.get("lkr")) + o.getConsumption()));
                        }
                        break;
                    }
                }
            }
        }
        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        jan_summary.put("nums", fromListToString(jan_meterNums));
        feb_summary.put("nums", fromListToString(feb_meterNums));
        mar_summary.put("nums",  fromListToString(mar_meterNums));
        apr_summary.put("nums", fromListToString(apr_meterNums));
        may_summary.put("nums", fromListToString(may_meterNums));
        jun_summary.put("nums", fromListToString(jun_meterNums));
        jul_summary.put("nums", fromListToString(jul_meterNums));
        aug_summary.put("nums", fromListToString(aug_meterNums));
        sep_summary.put("nums", fromListToString(sep_meterNums));
        oct_summary.put("nums", fromListToString(oct_meterNums));
        nov_summary.put("nums", fromListToString(nov_meterNums));
        dec_summary.put("nums", fromListToString(dec_meterNums));
        all_summary.put("nums", fromListToString(all_meterNums));
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String> > companyOwnedSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_vehicleNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("m3_diesel", Float.toString(0));
        jan_summary.put("m3_petrol", Float.toString(0));
        jan_summary.put("l_diesel", Float.toString(0));
        jan_summary.put("l_petrol", Float.toString(0));
        jan_summary.put("lkr_diesel", Float.toString(0));
        jan_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_vehicleNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("m3_petrol", Float.toString(0));
        feb_summary.put("m3_diesel", Float.toString(0));
        feb_summary.put("l_petrol", Float.toString(0));
        feb_summary.put("l_diesel", Float.toString(0));
        feb_summary.put("lkr_petrol", Float.toString(0));
        feb_summary.put("lkr_diesel", Float.toString(0));
        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_vehicleNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("m3_diesel", Float.toString(0));
        mar_summary.put("m3_petrol", Float.toString(0));
        mar_summary.put("l_diesel", Float.toString(0));
        mar_summary.put("l_petrol", Float.toString(0));
        mar_summary.put("lkr_diesel", Float.toString(0));
        mar_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_vehicleNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("m3_diesel", Float.toString(0));
        apr_summary.put("m3_petrol", Float.toString(0));
        apr_summary.put("l_diesel", Float.toString(0));
        apr_summary.put("l_petrol", Float.toString(0));
        apr_summary.put("lkr_diesel", Float.toString(0));
        apr_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_vehicleNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("m3_diesel", Float.toString(0));
        may_summary.put("m3_petrol", Float.toString(0));
        may_summary.put("l_diesel", Float.toString(0));
        may_summary.put("l_petrol", Float.toString(0));
        may_summary.put("lkr_diesel", Float.toString(0));
        may_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_vehicleNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("m3_diesel", Float.toString(0));
        jun_summary.put("m3_petrol", Float.toString(0));
        jun_summary.put("l_diesel", Float.toString(0));
        jun_summary.put("l_petrol", Float.toString(0));
        jun_summary.put("lkr_diesel", Float.toString(0));
        jun_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_vehicleNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("m3_diesel", Float.toString(0));
        jul_summary.put("m3_petrol", Float.toString(0));
        jul_summary.put("l_diesel", Float.toString(0));
        jul_summary.put("l_petrol", Float.toString(0));
        jul_summary.put("lkr_diesel", Float.toString(0));
        jul_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_vehicleNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("m3_diesel", Float.toString(0));
        aug_summary.put("m3_petrol", Float.toString(0));
        aug_summary.put("l_diesel", Float.toString(0));
        aug_summary.put("l_petrol", Float.toString(0));
        aug_summary.put("lkr_diesel", Float.toString(0));
        aug_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_vehicleNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("m3_diesel", Float.toString(0));
        sep_summary.put("m3_petrol", Float.toString(0));
        sep_summary.put("l_diesel", Float.toString(0));
        sep_summary.put("l_petrol", Float.toString(0));
        sep_summary.put("lkr_diesel", Float.toString(0));
        sep_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_vehicleNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("m3_diesel", Float.toString(0));
        oct_summary.put("m3_petrol", Float.toString(0));
        oct_summary.put("l_diesel", Float.toString(0));
        oct_summary.put("l_petrol", Float.toString(0));
        oct_summary.put("lkr_diesel", Float.toString(0));
        oct_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_vehicleNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("m3_diesel", Float.toString(0));
        nov_summary.put("m3_petrol", Float.toString(0));
        nov_summary.put("l_diesel", Float.toString(0));
        nov_summary.put("l_petrol", Float.toString(0));
        nov_summary.put("lkr_diesel", Float.toString(0));
        nov_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_vehicleNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("m3_diesel", Float.toString(0));
        dec_summary.put("m3_petrol", Float.toString(0));
        dec_summary.put("l_diesel", Float.toString(0));
        dec_summary.put("l_petrol", Float.toString(0));
        dec_summary.put("lkr_diesel", Float.toString(0));
        dec_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_vehicleNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("m3_diesel", Float.toString(0));
        all_summary.put("m3_petrol", Float.toString(0));
        all_summary.put("l_diesel", Float.toString(0));
        all_summary.put("l_petrol", Float.toString(0));
        all_summary.put("lkr_diesel", Float.toString(0));
        all_summary.put("lkr_petrol", Float.toString(0));
        for (VehicleEntry o : list) {
            if (o.getVehicleCategory() == VehicleCategory.COMPANY_OWNED.category) {
                switch (o.getMonth()) {
                    case 0: {
                        jan_vehicleNums.add(o.getVehicleNumber());
                        jan_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("m3_diesel", Float.toString(Float.parseFloat(jan_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("m3_petrol", Float.toString(Float.parseFloat(jan_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("l_diesel", Float.toString(Float.parseFloat(jan_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("l_petrol", Float.toString(Float.parseFloat(jan_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jan_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jan_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 1: {
                        feb_vehicleNums.add(o.getVehicleNumber());
                        feb_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("m3_diesel", Float.toString(Float.parseFloat(feb_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("m3_petrol", Float.toString(Float.parseFloat(feb_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("l_diesel", Float.toString(Float.parseFloat(feb_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("l_petrol", Float.toString(Float.parseFloat(feb_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("lkr_diesel", Float.toString(Float.parseFloat(feb_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("lkr_petrol", Float.toString(Float.parseFloat(feb_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 2: {
                        mar_vehicleNums.add(o.getVehicleNumber());
                        mar_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("m3_diesel", Float.toString(Float.parseFloat(mar_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("m3_petrol", Float.toString(Float.parseFloat(mar_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("l_diesel", Float.toString(Float.parseFloat(mar_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("l_petrol", Float.toString(Float.parseFloat(mar_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("lkr_diesel", Float.toString(Float.parseFloat(mar_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("lkr_petrol", Float.toString(Float.parseFloat(mar_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 3: {
                        apr_vehicleNums.add(o.getVehicleNumber());
                        apr_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("m3_diesel", Float.toString(Float.parseFloat(apr_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("m3_petrol", Float.toString(Float.parseFloat(apr_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("l_diesel", Float.toString(Float.parseFloat(apr_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("l_petrol", Float.toString(Float.parseFloat(apr_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("lkr_diesel", Float.toString(Float.parseFloat(apr_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("lkr_petrol", Float.toString(Float.parseFloat(apr_summary.get("lkr_petrol")) + o.getConsumption()));
                            }
                        }
                        break;
                    }
                    case 4: {
                        may_vehicleNums.add(o.getVehicleNumber());
                        may_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("m3_diesel", Float.toString(Float.parseFloat(may_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("m3_petrol", Float.toString(Float.parseFloat(may_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("l_diesel", Float.toString(Float.parseFloat(may_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("l_petrol", Float.toString(Float.parseFloat(may_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("lkr_diesel", Float.toString(Float.parseFloat(may_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("lkr_petrol", Float.toString(Float.parseFloat(may_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 5: {
                        jun_vehicleNums.add(o.getVehicleNumber());
                        jun_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("m3_diesel", Float.toString(Float.parseFloat(jun_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("m3_petrol", Float.toString(Float.parseFloat(jun_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("l_diesel", Float.toString(Float.parseFloat(jun_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("l_petrol", Float.toString(Float.parseFloat(jun_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jun_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jun_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 6: {
                        jul_vehicleNums.add(o.getVehicleNumber());
                        jul_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("m3_diesel", Float.toString(Float.parseFloat(jul_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("m3_petrol", Float.toString(Float.parseFloat(jul_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("l_diesel", Float.toString(Float.parseFloat(jul_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("l_petrol", Float.toString(Float.parseFloat(jul_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jul_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jul_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 7: {
                        aug_vehicleNums.add(o.getVehicleNumber());
                        aug_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("m3_diesel", Float.toString(Float.parseFloat(aug_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("m3_petrol", Float.toString(Float.parseFloat(aug_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("l_diesel", Float.toString(Float.parseFloat(aug_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("l_petrol", Float.toString(Float.parseFloat(aug_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("lkr_diesel", Float.toString(Float.parseFloat(aug_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("lkr_petrol", Float.toString(Float.parseFloat(aug_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 8: {
                        sep_vehicleNums.add(o.getVehicleNumber());
                        sep_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("m3_diesel", Float.toString(Float.parseFloat(sep_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("m3_petrol", Float.toString(Float.parseFloat(sep_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("l_diesel", Float.toString(Float.parseFloat(sep_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("l_petrol", Float.toString(Float.parseFloat(sep_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("lkr_diesel", Float.toString(Float.parseFloat(sep_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("lkr_petrol", Float.toString(Float.parseFloat(sep_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 9: {
                        oct_vehicleNums.add(o.getVehicleNumber());
                        oct_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("m3_diesel", Float.toString(Float.parseFloat(oct_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("m3_petrol", Float.toString(Float.parseFloat(oct_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("l_diesel", Float.toString(Float.parseFloat(oct_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("l_petrol", Float.toString(Float.parseFloat(oct_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("lkr_diesel", Float.toString(Float.parseFloat(oct_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("lkr_petrol", Float.toString(Float.parseFloat(oct_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 10: {
                        nov_vehicleNums.add(o.getVehicleNumber());
                        nov_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("m3_diesel", Float.toString(Float.parseFloat(nov_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("m3_petrol", Float.toString(Float.parseFloat(nov_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("l_diesel", Float.toString(Float.parseFloat(nov_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("l_petrol", Float.toString(Float.parseFloat(nov_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("lkr_diesel", Float.toString(Float.parseFloat(nov_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("lkr_petrol", Float.toString(Float.parseFloat(nov_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 11: {
                        dec_vehicleNums.add(o.getVehicleNumber());
                        dec_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("m3_diesel", Float.toString(Float.parseFloat(dec_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("m3_petrol", Float.toString(Float.parseFloat(dec_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("l_diesel", Float.toString(Float.parseFloat(dec_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("l_petrol", Float.toString(Float.parseFloat(dec_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("lkr_diesel", Float.toString(Float.parseFloat(dec_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("lkr_petrol", Float.toString(Float.parseFloat(dec_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 12: {
                        all_vehicleNums.add(o.getVehicleNumber());
                        all_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("m3_diesel", Float.toString(Float.parseFloat(all_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("m3_petrol", Float.toString(Float.parseFloat(all_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("l_diesel", Float.toString(Float.parseFloat(all_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("l_petrol", Float.toString(Float.parseFloat(all_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("lkr_diesel", Float.toString(Float.parseFloat(all_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("lkr_petrol", Float.toString(Float.parseFloat(all_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                }
            }

        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        jan_summary.put("nums",  fromListToString(jan_vehicleNums));
        feb_summary.put("nums",  fromListToString(feb_vehicleNums));
        mar_summary.put("nums",   fromListToString(mar_vehicleNums));
        apr_summary.put("nums", fromListToString(apr_vehicleNums));
        may_summary.put("nums",  fromListToString(may_vehicleNums));
        jun_summary.put("nums",  fromListToString(jun_vehicleNums));
        jul_summary.put("nums",  fromListToString(jul_vehicleNums));
        aug_summary.put("nums",  fromListToString(aug_vehicleNums));
        sep_summary.put("nums",  fromListToString(sep_vehicleNums));
        oct_summary.put("nums",  fromListToString(oct_vehicleNums));
        nov_summary.put("nums",  fromListToString(nov_vehicleNums));
        dec_summary.put("nums",  fromListToString(dec_vehicleNums));
        all_summary.put("nums",  fromListToString(all_vehicleNums));
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String> > rentedSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }

        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_vehicleNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("m3_diesel", Float.toString(0));
        jan_summary.put("m3_petrol", Float.toString(0));
        jan_summary.put("l_diesel", Float.toString(0));
        jan_summary.put("l_petrol", Float.toString(0));
        jan_summary.put("lkr_diesel", Float.toString(0));
        jan_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_vehicleNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("m3_petrol", Float.toString(0));
        feb_summary.put("m3_diesel", Float.toString(0));
        feb_summary.put("l_petrol", Float.toString(0));
        feb_summary.put("l_diesel", Float.toString(0));
        feb_summary.put("lkr_petrol", Float.toString(0));
        feb_summary.put("lkr_diesel", Float.toString(0));
        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_vehicleNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("m3_diesel", Float.toString(0));
        mar_summary.put("m3_petrol", Float.toString(0));
        mar_summary.put("l_diesel", Float.toString(0));
        mar_summary.put("l_petrol", Float.toString(0));
        mar_summary.put("lkr_diesel", Float.toString(0));
        mar_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_vehicleNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("m3_diesel", Float.toString(0));
        apr_summary.put("m3_petrol", Float.toString(0));
        apr_summary.put("l_diesel", Float.toString(0));
        apr_summary.put("l_petrol", Float.toString(0));
        apr_summary.put("lkr_diesel", Float.toString(0));
        apr_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_vehicleNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("m3_diesel", Float.toString(0));
        may_summary.put("m3_petrol", Float.toString(0));
        may_summary.put("l_diesel", Float.toString(0));
        may_summary.put("l_petrol", Float.toString(0));
        may_summary.put("lkr_diesel", Float.toString(0));
        may_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_vehicleNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("m3_diesel", Float.toString(0));
        jun_summary.put("m3_petrol", Float.toString(0));
        jun_summary.put("l_diesel", Float.toString(0));
        jun_summary.put("l_petrol", Float.toString(0));
        jun_summary.put("lkr_diesel", Float.toString(0));
        jun_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_vehicleNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("m3_diesel", Float.toString(0));
        jul_summary.put("m3_petrol", Float.toString(0));
        jul_summary.put("l_diesel", Float.toString(0));
        jul_summary.put("l_petrol", Float.toString(0));
        jul_summary.put("lkr_diesel", Float.toString(0));
        jul_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_vehicleNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("m3_diesel", Float.toString(0));
        aug_summary.put("m3_petrol", Float.toString(0));
        aug_summary.put("l_diesel", Float.toString(0));
        aug_summary.put("l_petrol", Float.toString(0));
        aug_summary.put("lkr_diesel", Float.toString(0));
        aug_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_vehicleNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("m3_diesel", Float.toString(0));
        sep_summary.put("m3_petrol", Float.toString(0));
        sep_summary.put("l_diesel", Float.toString(0));
        sep_summary.put("l_petrol", Float.toString(0));
        sep_summary.put("lkr_diesel", Float.toString(0));
        sep_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_vehicleNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("m3_diesel", Float.toString(0));
        oct_summary.put("m3_petrol", Float.toString(0));
        oct_summary.put("l_diesel", Float.toString(0));
        oct_summary.put("l_petrol", Float.toString(0));
        oct_summary.put("lkr_diesel", Float.toString(0));
        oct_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_vehicleNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("m3_diesel", Float.toString(0));
        nov_summary.put("m3_petrol", Float.toString(0));
        nov_summary.put("l_diesel", Float.toString(0));
        nov_summary.put("l_petrol", Float.toString(0));
        nov_summary.put("lkr_diesel", Float.toString(0));
        nov_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_vehicleNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("m3_diesel", Float.toString(0));
        dec_summary.put("m3_petrol", Float.toString(0));
        dec_summary.put("l_diesel", Float.toString(0));
        dec_summary.put("l_petrol", Float.toString(0));
        dec_summary.put("lkr_diesel", Float.toString(0));
        dec_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_vehicleNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("m3_diesel", Float.toString(0));
        all_summary.put("m3_petrol", Float.toString(0));
        all_summary.put("l_diesel", Float.toString(0));
        all_summary.put("l_petrol", Float.toString(0));
        all_summary.put("lkr_diesel", Float.toString(0));
        all_summary.put("lkr_petrol", Float.toString(0));
        for (VehicleEntry o : list) {
            if (o.getVehicleCategory() == VehicleCategory.RENTED.category) {
                switch (o.getMonth()) {
                    case 0: {
                        jan_vehicleNums.add(o.getVehicleNumber());
                        jan_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("m3_diesel", Float.toString(Float.parseFloat(jan_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("m3_petrol", Float.toString(Float.parseFloat(jan_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("l_diesel", Float.toString(Float.parseFloat(jan_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("l_petrol", Float.toString(Float.parseFloat(jan_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jan_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jan_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 1: {
                        feb_vehicleNums.add(o.getVehicleNumber());
                        feb_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("m3_diesel", Float.toString(Float.parseFloat(feb_summary.get("m3_diesel") + o.getConsumption())));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("m3_petrol", Float.toString(Float.parseFloat(feb_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("l_diesel", Float.toString(Float.parseFloat(feb_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("l_petrol", Float.toString(Float.parseFloat(feb_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("lkr_diesel", Float.toString(Float.parseFloat(feb_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("lkr_petrol", Float.toString(Float.parseFloat(feb_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 2: {
                        mar_vehicleNums.add(o.getVehicleNumber());
                        mar_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("m3_diesel", Float.toString(Float.parseFloat(mar_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("m3_petrol", Float.toString(Float.parseFloat(mar_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("l_diesel", Float.toString(Float.parseFloat(mar_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("l_petrol", Float.toString(Float.parseFloat(mar_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("lkr_diesel", Float.toString(Float.parseFloat(mar_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("lkr_petrol", Float.toString(Float.parseFloat(mar_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 3: {
                        apr_vehicleNums.add(o.getVehicleNumber());
                        apr_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("m3_diesel", Float.toString(Float.parseFloat(apr_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("m3_petrol", Float.toString(Float.parseFloat(apr_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("l_diesel", Float.toString(Float.parseFloat(apr_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("l_petrol", Float.toString(Float.parseFloat(apr_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("lkr_diesel", Float.toString(Float.parseFloat(apr_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("lkr_petrol", Float.toString(Float.parseFloat(apr_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 4: {
                        may_vehicleNums.add(o.getVehicleNumber());
                        may_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("m3_diesel", Float.toString(Float.parseFloat(may_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("m3_petrol", Float.toString(Float.parseFloat(may_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("l_diesel", Float.toString(Float.parseFloat(may_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("l_petrol", Float.toString(Float.parseFloat(may_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("lkr_diesel", Float.toString(Float.parseFloat(may_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("lkr_petrol", Float.toString(Float.parseFloat(may_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 5: {
                        jun_vehicleNums.add(o.getVehicleNumber());
                        jun_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("m3_diesel", Float.toString(Float.parseFloat(jun_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("m3_petrol", Float.toString(Float.parseFloat(jun_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("l_diesel", Float.toString(Float.parseFloat(jun_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("l_petrol", Float.toString(Float.parseFloat(jun_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jun_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jun_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 6: {
                        jul_vehicleNums.add(o.getVehicleNumber());
                        jul_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("m3_diesel", Float.toString(Float.parseFloat(jul_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("m3_petrol", Float.toString(Float.parseFloat(jul_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("l_diesel", Float.toString(Float.parseFloat(jul_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("l_petrol", Float.toString(Float.parseFloat(jul_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jul_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jul_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 7: {
                        aug_vehicleNums.add(o.getVehicleNumber());
                        aug_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("m3_diesel", Float.toString(Float.parseFloat(aug_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("m3_petrol", Float.toString(Float.parseFloat(aug_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("l_diesel", Float.toString(Float.parseFloat(aug_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("l_petrol", Float.toString(Float.parseFloat(aug_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("lkr_diesel", Float.toString(Float.parseFloat(aug_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("lkr_petrol", Float.toString(Float.parseFloat(aug_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 8: {
                        sep_vehicleNums.add(o.getVehicleNumber());
                        sep_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("m3_diesel", Float.toString(Float.parseFloat(sep_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("m3_petrol", Float.toString(Float.parseFloat(sep_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("l_diesel", Float.toString(Float.parseFloat(sep_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("l_petrol", Float.toString(Float.parseFloat(sep_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("lkr_diesel", Float.toString(Float.parseFloat(sep_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("lkr_petrol", Float.toString(Float.parseFloat(sep_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 9: {
                        oct_vehicleNums.add(o.getVehicleNumber());
                        oct_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("m3_diesel", Float.toString(Float.parseFloat(oct_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("m3_petrol", Float.toString(Float.parseFloat(oct_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("l_diesel", Float.toString(Float.parseFloat(oct_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("l_petrol", Float.toString(Float.parseFloat(oct_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("lkr_diesel", Float.toString(Float.parseFloat(oct_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("lkr_petrol", Float.toString(Float.parseFloat(oct_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 10: {
                        nov_vehicleNums.add(o.getVehicleNumber());
                        nov_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("m3_diesel", Float.toString(Float.parseFloat(nov_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("m3_petrol", Float.toString(Float.parseFloat(nov_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("l_diesel", Float.toString(Float.parseFloat(nov_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("l_petrol", Float.toString(Float.parseFloat(nov_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("lkr_diesel", Float.toString(Float.parseFloat(nov_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("lkr_petrol", Float.toString(Float.parseFloat(nov_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 11: {
                        dec_vehicleNums.add(o.getVehicleNumber());
                        dec_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("m3_diesel", Float.toString(Float.parseFloat(dec_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("m3_petrol", Float.toString(Float.parseFloat(dec_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("l_diesel", Float.toString(Float.parseFloat(dec_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("l_petrol", Float.toString(Float.parseFloat(dec_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("lkr_diesel", Float.toString(Float.parseFloat(dec_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("lkr_petrol", Float.toString(Float.parseFloat(dec_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 12: {
                        all_vehicleNums.add(o.getVehicleNumber());
                        all_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("m3_diesel", Float.toString(Float.parseFloat(all_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("m3_petrol", Float.toString(Float.parseFloat(all_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("l_diesel", Float.toString(Float.parseFloat(all_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("l_petrol", Float.toString(Float.parseFloat(all_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("lkr_diesel", Float.toString(Float.parseFloat(all_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("lkr_petrol", Float.toString(Float.parseFloat(all_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                }
            }

        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        jan_summary.put("nums",  fromListToString(jan_vehicleNums));
        feb_summary.put("nums",  fromListToString(feb_vehicleNums));
        mar_summary.put("nums",   fromListToString(mar_vehicleNums));
        apr_summary.put("nums", fromListToString(apr_vehicleNums));
        may_summary.put("nums",  fromListToString(may_vehicleNums));
        jun_summary.put("nums",  fromListToString(jun_vehicleNums));
        jul_summary.put("nums",  fromListToString(jul_vehicleNums));
        aug_summary.put("nums",  fromListToString(aug_vehicleNums));
        sep_summary.put("nums",  fromListToString(sep_vehicleNums));
        oct_summary.put("nums",  fromListToString(oct_vehicleNums));
        nov_summary.put("nums",  fromListToString(nov_vehicleNums));
        dec_summary.put("nums",  fromListToString(dec_vehicleNums));
        all_summary.put("nums",  fromListToString(all_vehicleNums));
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String> > hiredSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }

        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<VehicleEntry> list = vehicleEntryManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_vehicleNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("m3_diesel", Float.toString(0));
        jan_summary.put("m3_petrol", Float.toString(0));
        jan_summary.put("l_diesel", Float.toString(0));
        jan_summary.put("l_petrol", Float.toString(0));
        jan_summary.put("lkr_diesel", Float.toString(0));
        jan_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_vehicleNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("m3_petrol", Float.toString(0));
        feb_summary.put("m3_diesel", Float.toString(0));
        feb_summary.put("l_petrol", Float.toString(0));
        feb_summary.put("l_diesel", Float.toString(0));
        feb_summary.put("lkr_petrol", Float.toString(0));
        feb_summary.put("lkr_diesel", Float.toString(0));
        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_vehicleNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("m3_diesel", Float.toString(0));
        mar_summary.put("m3_petrol", Float.toString(0));
        mar_summary.put("l_diesel", Float.toString(0));
        mar_summary.put("l_petrol", Float.toString(0));
        mar_summary.put("lkr_diesel", Float.toString(0));
        mar_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_vehicleNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("m3_diesel", Float.toString(0));
        apr_summary.put("m3_petrol", Float.toString(0));
        apr_summary.put("l_diesel", Float.toString(0));
        apr_summary.put("l_petrol", Float.toString(0));
        apr_summary.put("lkr_diesel", Float.toString(0));
        apr_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_vehicleNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("m3_diesel", Float.toString(0));
        may_summary.put("m3_petrol", Float.toString(0));
        may_summary.put("l_diesel", Float.toString(0));
        may_summary.put("l_petrol", Float.toString(0));
        may_summary.put("lkr_diesel", Float.toString(0));
        may_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_vehicleNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("m3_diesel", Float.toString(0));
        jun_summary.put("m3_petrol", Float.toString(0));
        jun_summary.put("l_diesel", Float.toString(0));
        jun_summary.put("l_petrol", Float.toString(0));
        jun_summary.put("lkr_diesel", Float.toString(0));
        jun_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_vehicleNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("m3_diesel", Float.toString(0));
        jul_summary.put("m3_petrol", Float.toString(0));
        jul_summary.put("l_diesel", Float.toString(0));
        jul_summary.put("l_petrol", Float.toString(0));
        jul_summary.put("lkr_diesel", Float.toString(0));
        jul_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_vehicleNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("m3_diesel", Float.toString(0));
        aug_summary.put("m3_petrol", Float.toString(0));
        aug_summary.put("l_diesel", Float.toString(0));
        aug_summary.put("l_petrol", Float.toString(0));
        aug_summary.put("lkr_diesel", Float.toString(0));
        aug_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_vehicleNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("m3_diesel", Float.toString(0));
        sep_summary.put("m3_petrol", Float.toString(0));
        sep_summary.put("l_diesel", Float.toString(0));
        sep_summary.put("l_petrol", Float.toString(0));
        sep_summary.put("lkr_diesel", Float.toString(0));
        sep_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_vehicleNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("m3_diesel", Float.toString(0));
        oct_summary.put("m3_petrol", Float.toString(0));
        oct_summary.put("l_diesel", Float.toString(0));
        oct_summary.put("l_petrol", Float.toString(0));
        oct_summary.put("lkr_diesel", Float.toString(0));
        oct_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_vehicleNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("m3_diesel", Float.toString(0));
        nov_summary.put("m3_petrol", Float.toString(0));
        nov_summary.put("l_diesel", Float.toString(0));
        nov_summary.put("l_petrol", Float.toString(0));
        nov_summary.put("lkr_diesel", Float.toString(0));
        nov_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_vehicleNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("m3_diesel", Float.toString(0));
        dec_summary.put("m3_petrol", Float.toString(0));
        dec_summary.put("l_diesel", Float.toString(0));
        dec_summary.put("l_petrol", Float.toString(0));
        dec_summary.put("lkr_diesel", Float.toString(0));
        dec_summary.put("lkr_petrol", Float.toString(0));
        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_vehicleNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("m3_diesel", Float.toString(0));
        all_summary.put("m3_petrol", Float.toString(0));
        all_summary.put("l_diesel", Float.toString(0));
        all_summary.put("l_petrol", Float.toString(0));
        all_summary.put("lkr_diesel", Float.toString(0));
        all_summary.put("lkr_petrol", Float.toString(0));
        for (VehicleEntry o : list) {
            if (o.getVehicleCategory() == VehicleCategory.HIRED.category) {
                switch (o.getMonth()) {
                    case 0: {
                        jan_vehicleNums.add(o.getVehicleNumber());
                        jan_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("m3_diesel", Float.toString(Float.parseFloat(jan_summary.get("m3_diesel") )+ o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("m3_petrol", Float.toString(Float.parseFloat(jan_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("l_diesel", Float.toString(Float.parseFloat(jan_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("l_petrol", Float.toString(Float.parseFloat(jan_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jan_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jan_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 1: {
                        feb_vehicleNums.add(o.getVehicleNumber());
                        feb_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("m3_diesel", Float.toString(Float.parseFloat(feb_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("m3_petrol", Float.toString(Float.parseFloat(feb_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("l_diesel", Float.toString(Float.parseFloat(feb_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("l_petrol", Float.toString(Float.parseFloat(feb_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("lkr_diesel", Float.toString(Float.parseFloat(feb_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("lkr_petrol", Float.toString(Float.parseFloat(feb_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 2: {
                        mar_vehicleNums.add(o.getVehicleNumber());
                        mar_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("m3_diesel", Float.toString(Float.parseFloat(mar_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("m3_petrol", Float.toString(Float.parseFloat(mar_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("l_diesel", Float.toString(Float.parseFloat(mar_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("l_petrol", Float.toString(Float.parseFloat(mar_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("lkr_diesel", Float.toString(Float.parseFloat(mar_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("lkr_petrol", Float.toString(Float.parseFloat(mar_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 3: {
                        apr_vehicleNums.add(o.getVehicleNumber());
                        apr_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("m3_diesel", Float.toString(Float.parseFloat(apr_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("m3_petrol", Float.toString(Float.parseFloat(apr_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("l_diesel", Float.toString(Float.parseFloat(apr_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("l_petrol", Float.toString(Float.parseFloat(apr_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("lkr_diesel", Float.toString(Float.parseFloat(apr_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("lkr_petrol", Float.toString(Float.parseFloat(apr_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 4: {
                        may_vehicleNums.add(o.getVehicleNumber());
                        may_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("m3_diesel", Float.toString(Float.parseFloat(may_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("m3_petrol", Float.toString(Float.parseFloat(may_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("l_diesel", Float.toString(Float.parseFloat(may_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("l_petrol", Float.toString(Float.parseFloat(may_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("lkr_diesel", Float.toString(Float.parseFloat(may_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("lkr_petrol", Float.toString(Float.parseFloat(may_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 5: {
                        jun_vehicleNums.add(o.getVehicleNumber());
                        jun_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("m3_diesel", Float.toString(Float.parseFloat(jun_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("m3_petrol", Float.toString(Float.parseFloat(jun_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("l_diesel", Float.toString(Float.parseFloat(jun_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("l_petrol", Float.toString(Float.parseFloat(jun_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jun_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jun_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 6: {
                        jul_vehicleNums.add(o.getVehicleNumber());
                        jul_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("m3_diesel", Float.toString(Float.parseFloat(jul_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("m3_petrol", Float.toString(Float.parseFloat(jul_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("l_diesel", Float.toString(Float.parseFloat(jul_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("l_petrol", Float.toString(Float.parseFloat(jul_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("lkr_diesel", Float.toString(Float.parseFloat(jul_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("lkr_petrol", Float.toString(Float.parseFloat(jul_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 7: {
                        aug_vehicleNums.add(o.getVehicleNumber());
                        aug_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("m3_diesel", Float.toString(Float.parseFloat(aug_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("m3_petrol", Float.toString(Float.parseFloat(aug_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("l_diesel", Float.toString(Float.parseFloat(aug_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("l_petrol", Float.toString(Float.parseFloat(aug_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("lkr_diesel", Float.toString(Float.parseFloat(aug_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("lkr_petrol", Float.toString(Float.parseFloat(aug_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 8: {
                        sep_vehicleNums.add(o.getVehicleNumber());
                        sep_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("m3_diesel", Float.toString(Float.parseFloat(sep_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("m3_petrol", Float.toString(Float.parseFloat(sep_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("l_diesel", Float.toString(Float.parseFloat(sep_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("l_petrol", Float.toString(Float.parseFloat(sep_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("lkr_diesel", Float.toString(Float.parseFloat(sep_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("lkr_petrol", Float.toString(Float.parseFloat(sep_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 9: {
                        oct_vehicleNums.add(o.getVehicleNumber());
                        oct_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("m3_diesel", Float.toString(Float.parseFloat(oct_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("m3_petrol", Float.toString(Float.parseFloat(oct_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("l_diesel", Float.toString(Float.parseFloat(oct_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("l_petrol", Float.toString(Float.parseFloat(oct_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("lkr_diesel", Float.toString(Float.parseFloat(oct_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("lkr_petrol", Float.toString(Float.parseFloat(oct_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 10: {
                        nov_vehicleNums.add(o.getVehicleNumber());
                        nov_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("m3_diesel", Float.toString(Float.parseFloat(nov_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("m3_petrol", Float.toString(Float.parseFloat(nov_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("l_diesel", Float.toString(Float.parseFloat(nov_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("l_petrol", Float.toString(Float.parseFloat(nov_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("lkr_diesel", Float.toString(Float.parseFloat(nov_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("lkr_petrol", Float.toString(Float.parseFloat(nov_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 11: {
                        dec_vehicleNums.add(o.getVehicleNumber());
                        dec_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("m3_diesel", Float.toString(Float.parseFloat(dec_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("m3_petrol", Float.toString(Float.parseFloat(dec_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("l_diesel", Float.toString(Float.parseFloat(dec_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("l_petrol", Float.toString(Float.parseFloat(dec_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("lkr_diesel", Float.toString(Float.parseFloat(dec_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("lkr_petrol", Float.toString(Float.parseFloat(dec_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                    case 12: {
                        all_vehicleNums.add(o.getVehicleNumber());
                        all_summary.put("na", Boolean.toString(false));
                        if (o.getUnits() == Units.m3.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("m3_diesel", Float.toString(Float.parseFloat(all_summary.get("m3_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("m3_petrol", Float.toString(Float.parseFloat(all_summary.get("m3_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.liters.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("l_diesel", Float.toString(Float.parseFloat(all_summary.get("l_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("l_petrol", Float.toString(Float.parseFloat(all_summary.get("l_petrol")) + o.getConsumption()));

                            }
                        } else if (o.getUnits() == Units.LKR.unit) {
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("lkr_diesel", Float.toString(Float.parseFloat(all_summary.get("lkr_diesel")) + o.getConsumption()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("lkr_petrol", Float.toString(Float.parseFloat(all_summary.get("lkr_petrol")) + o.getConsumption()));

                            }
                        }
                        break;
                    }
                }
            }

        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        jan_summary.put("nums",  fromListToString(jan_vehicleNums));
        feb_summary.put("nums",  fromListToString(feb_vehicleNums));
        mar_summary.put("nums",   fromListToString(mar_vehicleNums));
        apr_summary.put("nums", fromListToString(apr_vehicleNums));
        may_summary.put("nums",  fromListToString(may_vehicleNums));
        jun_summary.put("nums",  fromListToString(jun_vehicleNums));
        jul_summary.put("nums",  fromListToString(jul_vehicleNums));
        aug_summary.put("nums",  fromListToString(aug_vehicleNums));
        sep_summary.put("nums",  fromListToString(sep_vehicleNums));
        oct_summary.put("nums",  fromListToString(oct_vehicleNums));
        nov_summary.put("nums",  fromListToString(nov_vehicleNums));
        dec_summary.put("nums",  fromListToString(dec_vehicleNums));
        all_summary.put("nums",  fromListToString(all_vehicleNums));
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String> > fireSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<FireExtingEntry> list = fireExtingEntryManager.getCustomFiltered(branchId, companyId, fy);

        HashMap<String, String> jan_summary = new HashMap<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("kg", Float.toString(0));
        HashMap<String, String> feb_summary = new HashMap<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("kg", Float.toString(0));
        HashMap<String, String> mar_summary = new HashMap<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("kg", Float.toString(0));
        HashMap<String, String> apr_summary = new HashMap<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("kg", Float.toString(0));
        HashMap<String, String> may_summary = new HashMap<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("kg", Float.toString(0));
        HashMap<String, String> jun_summary = new HashMap<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("kg", Float.toString(0));
        HashMap<String, String> jul_summary = new HashMap<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("kg", Float.toString(0));
        HashMap<String, String> aug_summary = new HashMap<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("kg", Float.toString(0));
        HashMap<String, String> sep_summary = new HashMap<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("kg", Float.toString(0));
        HashMap<String, String> oct_summary = new HashMap<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("kg", Float.toString(0));
        HashMap<String, String> nov_summary = new HashMap<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("kg", Float.toString(0));
        HashMap<String, String> dec_summary = new HashMap<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("kg", Float.toString(0));
        HashMap<String, String> all_summary = new HashMap<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("kg", Float.toString(0));


        for (FireExtingEntry o : list) {
            if (o != null) {
               switch (o.getMonth()) {
                   case 0: {
                       jan_summary.put("na", Boolean.toString(false));
                       jan_summary.put("kg", Float.toString(Float.parseFloat(jan_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));
                       break;
                   }
                   case 1: {
                       feb_summary.put("na", Boolean.toString(false));
                       feb_summary.put("kg", Float.toString(Float.parseFloat(feb_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 2: {
                       mar_summary.put("na", Boolean.toString(false));
                       mar_summary.put("kg", Float.toString(Float.parseFloat(mar_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 3: {
                       apr_summary.put("na", Boolean.toString(false));
                       apr_summary.put("kg", Float.toString(Float.parseFloat(apr_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 4: {
                       may_summary.put("na", Boolean.toString(false));
                       may_summary.put("kg", Float.toString(Float.parseFloat(may_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 5: {
                       jun_summary.put("na", Boolean.toString(false));
                       jun_summary.put("kg", Float.toString(Float.parseFloat(jun_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 6: {
                       jul_summary.put("na", Boolean.toString(false));
                       jul_summary.put("kg", Float.toString(Float.parseFloat(jul_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 7: {
                       aug_summary.put("na", Boolean.toString(false));
                       aug_summary.put("kg", Float.toString(Float.parseFloat(aug_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 8: {
                       sep_summary.put("na", Boolean.toString(false));
                       sep_summary.put("kg", Float.toString(Float.parseFloat(sep_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 9: {
                       oct_summary.put("na", Boolean.toString(false));
                       oct_summary.put("kg", Float.toString(Float.parseFloat(oct_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 10: {
                       nov_summary.put("na", Boolean.toString(false));
                       nov_summary.put("kg", Float.toString(Float.parseFloat(nov_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 11: {
                       dec_summary.put("na", Boolean.toString(false));
                       dec_summary.put("kg", Float.toString(Float.parseFloat(dec_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
                   case 12: {
                       all_summary.put("na", Boolean.toString(false));
                       all_summary.put("kg", Float.toString(Float.parseFloat(all_summary.get("kg")) + o.getNoOfTanks() * o.getWeight()));

                       break;
                   }
               }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String> > refriSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        float emission = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<RefrigerantsEntry> list = refrigerantsEntryManager.getCustomFiltered(branchId, companyId, fy);

        HashMap<String, String> jan_summary = new HashMap<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("kg", Float.toString(0));
        HashMap<String, String> feb_summary = new HashMap<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("kg", Float.toString(0));
        HashMap<String, String> mar_summary = new HashMap<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("kg", Float.toString(0));
        HashMap<String, String> apr_summary = new HashMap<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("kg", Float.toString(0));
        HashMap<String, String> may_summary = new HashMap<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("kg", Float.toString(0));
        HashMap<String, String> jun_summary = new HashMap<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("kg", Float.toString(0));
        HashMap<String, String> jul_summary = new HashMap<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("kg", Float.toString(0));
        HashMap<String, String> aug_summary = new HashMap<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("kg", Float.toString(0));
        HashMap<String, String> sep_summary = new HashMap<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("kg", Float.toString(0));
        HashMap<String, String> oct_summary = new HashMap<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("kg", Float.toString(0));
        HashMap<String, String> nov_summary = new HashMap<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("kg", Float.toString(0));
        HashMap<String, String> dec_summary = new HashMap<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("kg", Float.toString(0));
        HashMap<String, String> all_summary = new HashMap<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("kg", Float.toString(0));
        for (RefrigerantsEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {
                        jan_summary.put("na", Boolean.toString(false));
                        jan_summary.put("kg", Float.toString(Float.parseFloat(jan_summary.get("kg")) +  o.getAmountRefilled()));
                        break;
                    }
                    case 1: {
                        feb_summary.put("na", Boolean.toString(false));
                        feb_summary.put("kg", Float.toString(Float.parseFloat(feb_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 2: {
                        mar_summary.put("na", Boolean.toString(false));
                        mar_summary.put("kg", Float.toString(Float.parseFloat(mar_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 3: {
                        apr_summary.put("na", Boolean.toString(false));
                        apr_summary.put("kg", Float.toString(Float.parseFloat(apr_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 4: {
                        may_summary.put("na", Boolean.toString(false));
                        may_summary.put("kg", Float.toString(Float.parseFloat(may_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 5: {
                        jun_summary.put("na", Boolean.toString(false));
                        jun_summary.put("kg", Float.toString(Float.parseFloat(jun_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 6: {
                        jul_summary.put("na", Boolean.toString(false));
                        jul_summary.put("kg", Float.toString(Float.parseFloat(jul_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 7: {
                        aug_summary.put("na", Boolean.toString(false));
                        aug_summary.put("kg", Float.toString(Float.parseFloat(aug_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 8: {
                        sep_summary.put("na", Boolean.toString(false));
                        sep_summary.put("kg", Float.toString(Float.parseFloat(sep_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 9: {
                        oct_summary.put("na", Boolean.toString(false));
                        oct_summary.put("kg", Float.toString(Float.parseFloat(oct_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 10: {
                        nov_summary.put("na", Boolean.toString(false));
                        nov_summary.put("kg", Float.toString(Float.parseFloat(nov_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 11: {
                        dec_summary.put("na", Boolean.toString(false));
                        dec_summary.put("kg", Float.toString(Float.parseFloat(dec_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                    case 12: {
                        all_summary.put("na", Boolean.toString(false));
                        all_summary.put("kg", Float.toString(Float.parseFloat(all_summary.get("kg")) +  o.getAmountRefilled()));

                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);

        return  summaryMap;
    }

    public HashMap<String, HashMap<String, String> > airTravelSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        float emission = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);

        HashMap<String, String> jan_summary = new HashMap<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("emps", Float.toString(0));
        HashMap<String, String> feb_summary = new HashMap<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("emps", Float.toString(0));
        HashMap<String, String> mar_summary = new HashMap<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("emps", Float.toString(0));
        HashMap<String, String> apr_summary = new HashMap<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("emps", Float.toString(0));
        HashMap<String, String> may_summary = new HashMap<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("emps", Float.toString(0));
        HashMap<String, String> jun_summary = new HashMap<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("emps", Float.toString(0));
        HashMap<String, String> jul_summary = new HashMap<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("emps", Float.toString(0));
        HashMap<String, String> aug_summary = new HashMap<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("emps", Float.toString(0));
        HashMap<String, String> sep_summary = new HashMap<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("emps", Float.toString(0));
        HashMap<String, String> oct_summary = new HashMap<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("emps", Float.toString(0));
        HashMap<String, String> nov_summary = new HashMap<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("emps", Float.toString(0));
        HashMap<String, String> dec_summary = new HashMap<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("emps", Float.toString(0));
        HashMap<String, String> all_summary = new HashMap<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("emps", Float.toString(0));

        // todo:
        List<BusinessAirTravelEntry> list = businessAirTravelEntryManager.getCustomFiltered(branchId, companyId, fy);
        for(BusinessAirTravelEntry o : list) {
            if (o != null) {
                switch (o.getTravelMonth()) {
                    case 0: {
                        jan_summary.put("na", Boolean.toString(false));
                        jan_summary.put("emps", Float.toString(Float.parseFloat(jan_summary.get("emps")) +  o.getNoOfEmps()));
                        break;
                    }
                    case 1: {
                        feb_summary.put("na", Boolean.toString(false));
                        feb_summary.put("emps", Float.toString(Float.parseFloat(feb_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 2: {
                        mar_summary.put("na", Boolean.toString(false));
                        mar_summary.put("emps", Float.toString(Float.parseFloat(mar_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 3: {
                        apr_summary.put("na", Boolean.toString(false));
                        apr_summary.put("emps", Float.toString(Float.parseFloat(apr_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 4: {
                        may_summary.put("na", Boolean.toString(false));
                        may_summary.put("emps", Float.toString(Float.parseFloat(may_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 5: {
                        jun_summary.put("na", Boolean.toString(false));
                        jun_summary.put("emps", Float.toString(Float.parseFloat(jun_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 6: {
                        jul_summary.put("na", Boolean.toString(false));
                        jul_summary.put("emps", Float.toString(Float.parseFloat(jul_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 7: {
                        aug_summary.put("na", Boolean.toString(false));
                        aug_summary.put("emps", Float.toString(Float.parseFloat(aug_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 8: {
                        sep_summary.put("na", Boolean.toString(false));
                        sep_summary.put("emps", Float.toString(Float.parseFloat(sep_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 9: {
                        oct_summary.put("na", Boolean.toString(false));
                        oct_summary.put("emps", Float.toString(Float.parseFloat(oct_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 10: {
                        nov_summary.put("na", Boolean.toString(false));
                        nov_summary.put("emps", Float.toString(Float.parseFloat(nov_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 11: {
                        dec_summary.put("na", Boolean.toString(false));
                        dec_summary.put("emps", Float.toString(Float.parseFloat(dec_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                    case 12: {
                        all_summary.put("na", Boolean.toString(false));
                        all_summary.put("emps", Float.toString(Float.parseFloat(all_summary.get("emps")) +   o.getNoOfEmps()));

                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);

        return  summaryMap;
    }

    public HashMap<String, HashMap<String, String>> transLocPurchasedInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        float emission = 0;
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        List<TransportLocalEntry> list = transportLocalEntryManager.getCustomFiltered(branchId, companyId, fy);
        HashMap<String, String> jan_summary = new HashMap<>();

        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("km_diesel", Float.toString(0));
        jan_summary.put("km_petrol", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();

        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("km_petrol", Float.toString(0));
        feb_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> mar_summary = new HashMap<>();

        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("km_petrol", Float.toString(0));
        mar_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> apr_summary = new HashMap<>();

        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("km_petrol", Float.toString(0));
        apr_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> may_summary = new HashMap<>();

        may_summary.put("na", Boolean.toString(true));
        may_summary.put("km_petrol", Float.toString(0));
        may_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> jun_summary = new HashMap<>();

        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("km_petrol", Float.toString(0));
        jun_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();

        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("km_petrol", Float.toString(0));
        jul_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> aug_summary = new HashMap<>();

        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("km_petrol", Float.toString(0));
        aug_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> sep_summary = new HashMap<>();

        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("km_petrol", Float.toString(0));
        sep_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> oct_summary = new HashMap<>();

        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("km_petrol", Float.toString(0));
        oct_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> nov_summary = new HashMap<>();

        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("km_petrol", Float.toString(0));
        nov_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> dec_summary = new HashMap<>();

        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("km_petrol", Float.toString(0));
        dec_summary.put("km_diesel", Float.toString(0));

        HashMap<String, String> all_summary = new HashMap<>();

        all_summary.put("na", Boolean.toString(true));
        all_summary.put("km_petrol", Float.toString(0));
        all_summary.put("km_diesel", Float.toString(0));

        for (TransportLocalEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {

                        jan_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jan_summary.put("km_diesel", Float.toString(Float.parseFloat(jan_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jan_summary.put("km_petrol", Float.toString(Float.parseFloat(jan_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }

                        break;
                    }
                    case 1: {

                        feb_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                feb_summary.put("km_diesel", Float.toString(Float.parseFloat(feb_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                feb_summary.put("km_petrol", Float.toString(Float.parseFloat(feb_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }

                        break;
                    }
                    case 2: {

                        mar_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                mar_summary.put("km_diesel", Float.toString(Float.parseFloat(mar_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                mar_summary.put("km_petrol", Float.toString(Float.parseFloat(mar_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }
                        break;
                    }
                    case 3: {

                        apr_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                apr_summary.put("km_diesel", Float.toString(Float.parseFloat(apr_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                apr_summary.put("km_petrol", Float.toString(Float.parseFloat(apr_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }

                        break;
                    }
                    case 4: {

                        may_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                may_summary.put("km_diesel", Float.toString(Float.parseFloat(may_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                may_summary.put("km_petrol", Float.toString(Float.parseFloat(may_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }

                        break;
                    }
                    case 5: {

                        jun_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jun_summary.put("km_diesel", Float.toString(Float.parseFloat(jun_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jun_summary.put("km_petrol", Float.toString(Float.parseFloat(jun_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }

                        break;
                    }
                    case 6: {

                        jul_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                jul_summary.put("km_diesel", Float.toString(Float.parseFloat(jul_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                jul_summary.put("km_petrol", Float.toString(Float.parseFloat(jul_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }

                        break;
                    }
                    case 7: {

                        aug_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                aug_summary.put("km_diesel", Float.toString(Float.parseFloat(aug_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                aug_summary.put("km_petrol", Float.toString(Float.parseFloat(aug_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }
                        break;
                    }
                    case 8: {

                        sep_summary.put("na", Boolean.toString(false));
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                sep_summary.put("km_diesel", Float.toString(Float.parseFloat(sep_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                sep_summary.put("km_petrol", Float.toString(Float.parseFloat(sep_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }
                        break;
                    }
                    case 9: {

                        oct_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                oct_summary.put("km_diesel", Float.toString(Float.parseFloat(oct_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                oct_summary.put("km_petrol", Float.toString(Float.parseFloat(oct_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }
                        break;
                    }
                    case 10: {

                        nov_summary.put("na", Boolean.toString(false));
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                nov_summary.put("km_diesel", Float.toString(Float.parseFloat(nov_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                nov_summary.put("km_petrol", Float.toString(Float.parseFloat(nov_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }
                        break;
                    }
                    case 11: {

                        dec_summary.put("na", Boolean.toString(false));
                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                dec_summary.put("km_diesel", Float.toString(Float.parseFloat(dec_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                dec_summary.put("km_petrol", Float.toString(Float.parseFloat(dec_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }

                        break;
                    }
                    case 12: {

                        all_summary.put("na", Boolean.toString(false));

                            if (o.getFuelType() == FuelTypes.Diesel.type) {
                                all_summary.put("km_diesel", Float.toString(Float.parseFloat(all_summary.get("km_diesel")) + o.getDistanceTravelled()));

                            } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                                all_summary.put("km_petrol", Float.toString(Float.parseFloat(all_summary.get("km_petrol")) + o.getDistanceTravelled()));

                            }
                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String>> wasteDisSummaryInputs(int companyId, int branchId, int yearBegin, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        float emission = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<WasteDisposalEntry> list = wasteDisposalEntryManager.getCustomFiltered(branchId, companyId, fy);

        HashMap<String, String> jan_summary = new HashMap<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("reuse", Float.toString(0));
        jan_summary.put("oloop", Float.toString(0));
        jan_summary.put("cloop", Float.toString(0));
        jan_summary.put("combu", Float.toString(0));
        jan_summary.put("compo", Float.toString(0));
        jan_summary.put("land", Float.toString(0));
        jan_summary.put("anaer", Float.toString(0));
        jan_summary.put("piggery", Float.toString(0));
        jan_summary.put("incin", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("reuse", Float.toString(0));
        feb_summary.put("oloop", Float.toString(0));
        feb_summary.put("cloop", Float.toString(0));
        feb_summary.put("combu", Float.toString(0));
        feb_summary.put("compo", Float.toString(0));
        feb_summary.put("land", Float.toString(0));
        feb_summary.put("anaer", Float.toString(0));
        feb_summary.put("piggery", Float.toString(0));
        feb_summary.put("incin", Float.toString(0));

        HashMap<String, String> mar_summary = new HashMap<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("reuse", Float.toString(0));
        mar_summary.put("oloop", Float.toString(0));
        mar_summary.put("cloop", Float.toString(0));
        mar_summary.put("combu", Float.toString(0));
        mar_summary.put("compo", Float.toString(0));
        mar_summary.put("land", Float.toString(0));
        mar_summary.put("anaer", Float.toString(0));
        mar_summary.put("piggery", Float.toString(0));
        mar_summary.put("incin", Float.toString(0));

        HashMap<String, String> apr_summary = new HashMap<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("reuse", Float.toString(0));
        apr_summary.put("oloop", Float.toString(0));
        apr_summary.put("cloop", Float.toString(0));
        apr_summary.put("combu", Float.toString(0));
        apr_summary.put("compo", Float.toString(0));
        apr_summary.put("land", Float.toString(0));
        apr_summary.put("anaer", Float.toString(0));
        apr_summary.put("piggery", Float.toString(0));
        apr_summary.put("incin", Float.toString(0));

        HashMap<String, String> may_summary = new HashMap<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("reuse", Float.toString(0));
        may_summary.put("oloop", Float.toString(0));
        may_summary.put("cloop", Float.toString(0));
        may_summary.put("combu", Float.toString(0));
        may_summary.put("compo", Float.toString(0));
        may_summary.put("land", Float.toString(0));
        may_summary.put("anaer", Float.toString(0));
        may_summary.put("piggery", Float.toString(0));
        may_summary.put("incin", Float.toString(0));

        HashMap<String, String> jun_summary = new HashMap<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("reuse", Float.toString(0));
        jun_summary.put("oloop", Float.toString(0));
        jun_summary.put("cloop", Float.toString(0));
        jun_summary.put("combu", Float.toString(0));
        jun_summary.put("compo", Float.toString(0));
        jun_summary.put("land", Float.toString(0));
        jun_summary.put("anaer", Float.toString(0));
        jun_summary.put("piggery", Float.toString(0));
        jun_summary.put("incin", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("reuse", Float.toString(0));
        jul_summary.put("oloop", Float.toString(0));
        jul_summary.put("cloop", Float.toString(0));
        jul_summary.put("combu", Float.toString(0));
        jul_summary.put("compo", Float.toString(0));
        jul_summary.put("land", Float.toString(0));
        jul_summary.put("anaer", Float.toString(0));
        jul_summary.put("piggery", Float.toString(0));
        jul_summary.put("incin", Float.toString(0));

        HashMap<String, String> aug_summary = new HashMap<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("reuse", Float.toString(0));
        aug_summary.put("oloop", Float.toString(0));
        aug_summary.put("cloop", Float.toString(0));
        aug_summary.put("combu", Float.toString(0));
        aug_summary.put("compo", Float.toString(0));
        aug_summary.put("land", Float.toString(0));
        aug_summary.put("anaer", Float.toString(0));
        aug_summary.put("piggery", Float.toString(0));
        aug_summary.put("incin", Float.toString(0));

        HashMap<String, String> oct_summary = new HashMap<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("reuse", Float.toString(0));
        oct_summary.put("oloop", Float.toString(0));
        oct_summary.put("cloop", Float.toString(0));
        oct_summary.put("combu", Float.toString(0));
        oct_summary.put("compo", Float.toString(0));
        oct_summary.put("land", Float.toString(0));
        oct_summary.put("anaer", Float.toString(0));
        oct_summary.put("piggery", Float.toString(0));
        oct_summary.put("incin", Float.toString(0));

        HashMap<String, String> sep_summary = new HashMap<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("reuse", Float.toString(0));
        sep_summary.put("oloop", Float.toString(0));
        sep_summary.put("cloop", Float.toString(0));
        sep_summary.put("combu", Float.toString(0));
        sep_summary.put("compo", Float.toString(0));
        sep_summary.put("land", Float.toString(0));
        sep_summary.put("anaer", Float.toString(0));
        sep_summary.put("piggery", Float.toString(0));
        sep_summary.put("incin", Float.toString(0));

        HashMap<String, String> nov_summary = new HashMap<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("reuse", Float.toString(0));
        nov_summary.put("oloop", Float.toString(0));
        nov_summary.put("cloop", Float.toString(0));
        nov_summary.put("combu", Float.toString(0));
        nov_summary.put("compo", Float.toString(0));
        nov_summary.put("land", Float.toString(0));
        nov_summary.put("anaer", Float.toString(0));
        nov_summary.put("piggery", Float.toString(0));
        nov_summary.put("incin", Float.toString(0));

        HashMap<String, String> dec_summary = new HashMap<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("reuse", Float.toString(0));
        dec_summary.put("oloop", Float.toString(0));
        dec_summary.put("cloop", Float.toString(0));
        dec_summary.put("combu", Float.toString(0));
        dec_summary.put("compo", Float.toString(0));
        dec_summary.put("land", Float.toString(0));
        dec_summary.put("anaer", Float.toString(0));
        dec_summary.put("piggery", Float.toString(0));
        dec_summary.put("incin", Float.toString(0));

        HashMap<String, String> all_summary = new HashMap<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("reuse", Float.toString(0));
        all_summary.put("oloop", Float.toString(0));
        all_summary.put("cloop", Float.toString(0));
        all_summary.put("combu", Float.toString(0));
        all_summary.put("compo", Float.toString(0));
        all_summary.put("land", Float.toString(0));
        all_summary.put("anaer", Float.toString(0));
        all_summary.put("piggery", Float.toString(0));
        all_summary.put("incin", Float.toString(0));




        for (WasteDisposalEntry o : list) {
            if (o != null) {
                switch (o.getMonth()) {
                    case 0: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                jan_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                jan_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                jan_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                jan_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                jan_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                jan_summary.put("land", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                jan_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                jan_summary.put("piggery", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("piggery"))));
                                break;
                            }case 9: {
                                jan_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(jan_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 1: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                feb_summary.put("reuse",Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("reuse"))) );
                                break;
                            }
                            case 2: {
                                feb_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                feb_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                feb_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                feb_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                feb_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                feb_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                feb_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("piggery"))));
                                break;
                            }case 9: {
                                feb_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(feb_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 2: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                mar_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                mar_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                mar_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                mar_summary.put("combu",Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                mar_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                mar_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                mar_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                mar_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("piggery"))));
                                break;
                            }case 9: {
                                mar_summary.put("incin",Float.toString(o.getAmountDisposed() + Float.parseFloat(mar_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 3: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                apr_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                apr_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                apr_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                apr_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                apr_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                apr_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                apr_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                apr_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("piggery"))));
                                break;
                            }case 9: {
                                apr_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(apr_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 4: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                may_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                may_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                may_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                may_summary.put("combu",Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                may_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                may_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                may_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                may_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("piggery"))));
                                break;
                            }case 9: {
                                may_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(may_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 5: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                jun_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                jun_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                jun_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                jun_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                jun_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                jun_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                jun_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                jun_summary.put("piggery", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("piggery"))));
                                break;
                            }case 9: {
                                jun_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(jun_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 6: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                jul_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                jul_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                jul_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                jul_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                jul_summary.put("compo",Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                jul_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                jul_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                jul_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("piggery"))));
                                break;
                            }case 9: {
                                jul_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(jul_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 7: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                aug_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                aug_summary.put("oloop",Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                aug_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                aug_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                aug_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                aug_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                aug_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                aug_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("piggery"))));
                                break;
                            }case 9: {
                                aug_summary.put("incin",Float.toString(o.getAmountDisposed() + Float.parseFloat(aug_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 8: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                sep_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                sep_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                sep_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                sep_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                sep_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                sep_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                sep_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                sep_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("piggery"))));
                                break;
                            }case 9: {
                                sep_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(sep_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 9: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                oct_summary.put("reuse",Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                oct_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                oct_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                oct_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                oct_summary.put("compo",Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                oct_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                oct_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                oct_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("piggery"))));
                                break;
                            }case 9: {
                                oct_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(oct_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }

                    case 10: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                nov_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                nov_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                nov_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                nov_summary.put("combu", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                nov_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                nov_summary.put("land", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                nov_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                nov_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("piggery"))));
                                break;
                            }case 9: {
                                nov_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(nov_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case 11: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                dec_summary.put("reuse",Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                dec_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                dec_summary.put("cloop",Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                dec_summary.put("combu",Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                dec_summary.put("compo",Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                dec_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                dec_summary.put("anaer", Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                dec_summary.put("piggery",  Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("piggery"))));
                                break;
                            }case 9: {
                                dec_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(dec_summary.get("incin"))));
                                break;
                            }

                        }
                        break;
                    }
                    case  12: {
                        switch (o.getDisposalMethod()) {
                            case  1: {
                                all_summary.put("reuse", Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("reuse"))));
                                break;
                            }
                            case 2: {
                                all_summary.put("oloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("oloop"))));
                                break;
                            }
                            case 3: {
                                all_summary.put("cloop", Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("cloop"))));
                                break;
                            }
                            case 4: {
                                all_summary.put("combu",Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("combu"))));
                                break;
                            }
                            case 5: {
                                all_summary.put("compo", Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("compo"))));
                                break;
                            }
                            case 6: {
                                all_summary.put("land",  Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("land"))));
                                break;
                            }
                            case 7: {
                                all_summary.put("anaer",Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("anaer"))));
                                break;
                            }
                            case 8: {
                                all_summary.put("piggery", Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("piggery"))));
                                break;
                            }case 9: {
                                all_summary.put("incin", Float.toString(o.getAmountDisposed() + Float.parseFloat(all_summary.get("incin"))));
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String>> empCommSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);

        List<EmpCommutingEntry> list = employeCommutingEntryManager.getCustomFiltered(branchId, companyId, fy);

        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_emps = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("emps", Float.toString(0));
        jan_summary.put("days", Float.toString(0));

        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_emps = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("emps", Float.toString(0));
        feb_summary.put("days", Float.toString(0));

        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_emps = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("emps", Float.toString(0));
        mar_summary.put("days", Float.toString(0));

        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_emps = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("emps", Float.toString(0));
        apr_summary.put("days", Float.toString(0));

        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_emps = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("emps", Float.toString(0));
        may_summary.put("days", Float.toString(0));

        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_emps = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("emps", Float.toString(0));
        jun_summary.put("days", Float.toString(0));

        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_emps = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("emps", Float.toString(0));
        jul_summary.put("days", Float.toString(0));

        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_emps = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("emps", Float.toString(0));
        aug_summary.put("days", Float.toString(0));

        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_emps = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("emps", Float.toString(0));
        sep_summary.put("days", Float.toString(0));

        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_emps = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("emps", Float.toString(0));
        oct_summary.put("days", Float.toString(0));

        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_emps = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("emps", Float.toString(0));
        nov_summary.put("days", Float.toString(0));

        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_emps = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("emps", Float.toString(0));
        dec_summary.put("days", Float.toString(0));

        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_emps = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("emps", Float.toString(0));
        all_summary.put("days", Float.toString(0));


        for (EmpCommutingEntry o : list) {
            if (o != null ) {
                switch(o.getMonth()) {
                    case 0: {
                        jan_summary.put("na", Boolean.toString(false));
                        jan_emps.add(o.getEmpId());
                        jan_summary.put("emps", Float.toString(Float.parseFloat(jan_summary.get("emps")) + 1.0f ));
                        jan_summary.put("days", Float.toString(Float.parseFloat(jan_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 1: {
                        feb_summary.put("na", Boolean.toString(false));
                        feb_emps.add(o.getEmpId());
                        feb_summary.put("emps", Float.toString(Float.parseFloat(feb_summary.get("emps")) + 1.0f ));
                        feb_summary.put("days", Float.toString(Float.parseFloat(feb_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 2: {
                        mar_summary.put("na", Boolean.toString(false));
                        mar_emps.add(o.getEmpId());
                        mar_summary.put("emps", Float.toString(Float.parseFloat(mar_summary.get("emps")) + 1.0f ));
                        mar_summary.put("days", Float.toString(Float.parseFloat(mar_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 3: {
                        apr_summary.put("na", Boolean.toString(false));
                        apr_emps.add(o.getEmpId());
                        apr_summary.put("emps", Float.toString(Float.parseFloat(apr_summary.get("emps")) + 1.0f ));
                        apr_summary.put("days", Float.toString(Float.parseFloat(apr_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 4: {
                        may_summary.put("na", Boolean.toString(false));
                        may_emps.add(o.getEmpId());
                        may_summary.put("emps", Float.toString(Float.parseFloat(may_summary.get("emps")) + 1.0f ));
                        may_summary.put("days", Float.toString(Float.parseFloat(may_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 5: {
                        jun_summary.put("na", Boolean.toString(false));
                        jun_emps.add(o.getEmpId());
                        jun_summary.put("emps", Float.toString(Float.parseFloat(jun_summary.get("emps")) + 1.0f ));
                        jun_summary.put("days", Float.toString(Float.parseFloat(jun_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 6:{
                        jul_summary.put("na", Boolean.toString(false));
                        jul_emps.add(o.getEmpId());
                        jul_summary.put("emps", Float.toString(Float.parseFloat(jul_summary.get("emps")) + 1.0f ));
                        jul_summary.put("days", Float.toString(Float.parseFloat(jul_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 7: {
                        aug_summary.put("na", Boolean.toString(false));
                        aug_emps.add(o.getEmpId());
                        aug_summary.put("emps", Float.toString(Float.parseFloat(aug_summary.get("emps")) + 1.0f ));
                        aug_summary.put("days", Float.toString(Float.parseFloat(aug_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 8: {
                        sep_summary.put("na", Boolean.toString(false));
                        sep_emps.add(o.getEmpId());
                        sep_summary.put("emps", Float.toString(Float.parseFloat(sep_summary.get("emps")) + 1.0f ));
                        sep_summary.put("days", Float.toString(Float.parseFloat(sep_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 9: {
                        oct_summary.put("na", Boolean.toString(false));
                        oct_emps.add(o.getEmpId());
                        oct_summary.put("emps", Float.toString(Float.parseFloat(oct_summary.get("emps")) + 1.0f ));
                        oct_summary.put("days", Float.toString(Float.parseFloat(oct_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 10: {
                        nov_summary.put("na", Boolean.toString(false));
                        nov_emps.add(o.getEmpId());
                        nov_summary.put("emps", Float.toString(Float.parseFloat(nov_summary.get("emps")) + 1.0f ));
                        nov_summary.put("days", Float.toString(Float.parseFloat(nov_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 11: {
                        dec_summary.put("na", Boolean.toString(false));
                        dec_emps.add(o.getEmpId());
                        dec_summary.put("emps", Float.toString(Float.parseFloat(dec_summary.get("emps")) + 1.0f ));
                        dec_summary.put("days", Float.toString(Float.parseFloat(dec_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                    case 12: {
                        all_summary.put("na", Boolean.toString(false));
                        all_emps.add(o.getEmpId());
                        all_summary.put("emps", Float.toString(Float.parseFloat(all_summary.get("emps")) + 1.0f ));
                        all_summary.put("days", Float.toString(Float.parseFloat(all_summary.get("days")) + o.getNoOfWorkingDays() ));
                        break;
                    }
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public HashMap<String, HashMap<String, String>> wasteTransSummaryInputs(int companyId, int branchId, int yearBegin, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        float emission = 0;
        String fy = (yearEnd == yearBegin) ? "" + yearEnd + "" : yearBegin + "/" + (yearEnd % 100);
        List<WasteTransportEntry> list = wasteTransportEntryManager.getCustomFiltered(branchId, companyId, fy);

        HashMap<String, String> jan_summary = new HashMap<>();

        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("km_diesel", Float.toString(0));
        jan_summary.put("km_petrol", Float.toString(0));
        List<String> jan_subContracts = new ArrayList<>();

        HashMap<String, String> feb_summary = new HashMap<>();

        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("km_diesel", Float.toString(0));
        feb_summary.put("km_petrol", Float.toString(0));
        List<String> feb_subContracts = new ArrayList<>();

        HashMap<String, String> mar_summary = new HashMap<>();

        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("km_diesel", Float.toString(0));
        mar_summary.put("km_petrol", Float.toString(0));
        List<String> mar_subContracts = new ArrayList<>();

        HashMap<String, String> apr_summary = new HashMap<>();

        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("km_diesel", Float.toString(0));
        apr_summary.put("km_petrol", Float.toString(0));
        List<String> apr_subContracts = new ArrayList<>();

        HashMap<String, String> may_summary = new HashMap<>();

        may_summary.put("na", Boolean.toString(true));
        may_summary.put("km_diesel", Float.toString(0));
        may_summary.put("km_petrol", Float.toString(0));
        List<String> may_subContracts = new ArrayList<>();

        HashMap<String, String> jun_summary = new HashMap<>();

        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("km_diesel", Float.toString(0));
        jun_summary.put("km_petrol", Float.toString(0));
        List<String> jun_subContracts = new ArrayList<>();

        HashMap<String, String> jul_summary = new HashMap<>();

        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("km_diesel", Float.toString(0));
        jul_summary.put("km_petrol", Float.toString(0));
        List<String> jul_subContracts = new ArrayList<>();

        HashMap<String, String> aug_summary = new HashMap<>();

        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("km_diesel", Float.toString(0));
        aug_summary.put("km_petrol", Float.toString(0));
        List<String> aug_subContracts = new ArrayList<>();

        HashMap<String, String> sep_summary = new HashMap<>();

        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("km_diesel", Float.toString(0));
        sep_summary.put("km_petrol", Float.toString(0));
        List<String> sep_subContracts = new ArrayList<>();

        HashMap<String, String> oct_summary = new HashMap<>();

        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("km_diesel", Float.toString(0));
        oct_summary.put("km_petrol", Float.toString(0));
        List<String> oct_subContracts = new ArrayList<>();

        HashMap<String, String> nov_summary = new HashMap<>();

        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("km_diesel", Float.toString(0));
        nov_summary.put("km_petrol", Float.toString(0));
        List<String> nov_subContracts = new ArrayList<>();

        HashMap<String, String> dec_summary = new HashMap<>();

        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("km_diesel", Float.toString(0));
        dec_summary.put("km_petrol", Float.toString(0));
        List<String> dec_subContracts = new ArrayList<>();

        HashMap<String, String> all_summary = new HashMap<>();

        all_summary.put("na", Boolean.toString(true));
        all_summary.put("km_diesel", Float.toString(0));
        all_summary.put("km_petrol", Float.toString(0));
        List<String> all_subContracts = new ArrayList<>();

        for (WasteTransportEntry o : list) {
            if (o != null) {
               switch(o.getMonth()) {
                   case 0: {
                       jan_summary.put("na",Boolean.toString(false));
                       jan_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           jan_summary.put("km_diesel", Float.toString(Float.parseFloat(jan_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           jan_summary.put("km_petrol", Float.toString(Float.parseFloat(jan_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 1: {
                       feb_summary.put("na",Boolean.toString(false));
                       feb_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           feb_summary.put("km_diesel", Float.toString(Float.parseFloat(feb_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           feb_summary.put("km_petrol", Float.toString(Float.parseFloat(feb_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 2: {
                       mar_summary.put("na",Boolean.toString(false));
                       mar_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           mar_summary.put("km_diesel", Float.toString(Float.parseFloat(mar_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           mar_summary.put("km_petrol", Float.toString(Float.parseFloat(mar_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 3: {
                       apr_summary.put("na",Boolean.toString(false));
                       apr_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           apr_summary.put("km_diesel", Float.toString(Float.parseFloat(apr_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           apr_summary.put("km_petrol", Float.toString(Float.parseFloat(apr_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 4: {
                       may_summary.put("na",Boolean.toString(false));
                       may_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           may_summary.put("km_diesel", Float.toString(Float.parseFloat(may_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           may_summary.put("km_petrol", Float.toString(Float.parseFloat(may_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 5: {
                       jun_summary.put("na",Boolean.toString(false));
                       jun_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           jun_summary.put("km_diesel", Float.toString(Float.parseFloat(jun_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           jun_summary.put("km_petrol", Float.toString(Float.parseFloat(jun_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 6: {
                       jul_summary.put("na",Boolean.toString(false));
                       jul_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           jul_summary.put("km_diesel", Float.toString(Float.parseFloat(jul_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           jul_summary.put("km_petrol", Float.toString(Float.parseFloat(jul_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 7: {
                       aug_summary.put("na",Boolean.toString(false));
                       aug_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           aug_summary.put("km_diesel", Float.toString(Float.parseFloat(aug_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           aug_summary.put("km_petrol", Float.toString(Float.parseFloat(aug_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 8: {
                       sep_summary.put("na",Boolean.toString(false));
                       sep_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           sep_summary.put("km_diesel", Float.toString(Float.parseFloat(sep_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           sep_summary.put("km_petrol", Float.toString(Float.parseFloat(sep_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 9: {
                       oct_summary.put("na",Boolean.toString(false));
                       oct_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           oct_summary.put("km_diesel", Float.toString(Float.parseFloat(oct_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           oct_summary.put("km_petrol", Float.toString(Float.parseFloat(oct_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 10:{
                       nov_summary.put("na",Boolean.toString(false));
                       nov_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           nov_summary.put("km_diesel", Float.toString(Float.parseFloat(nov_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           nov_summary.put("km_petrol", Float.toString(Float.parseFloat(nov_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 11: {
                       dec_summary.put("na",Boolean.toString(false));
                       dec_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           dec_summary.put("km_diesel", Float.toString(Float.parseFloat(dec_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           dec_summary.put("km_petrol", Float.toString(Float.parseFloat(dec_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
                   case 12: {
                       all_summary.put("na",Boolean.toString(false));
                       all_subContracts.add(o.getSubContractorName());
                       if (o.getFuelType() == FuelTypes.Diesel.type) {
                           all_summary.put("km_diesel", Float.toString(Float.parseFloat(all_summary.get("km_diesel")) + o.getDistanceTravelled()));
                       } else if (o.getFuelType() == FuelTypes.Petrol.type) {
                           all_summary.put("km_petrol", Float.toString(Float.parseFloat(all_summary.get("km_petrol")) + o.getDistanceTravelled()));
                       }
                       break;
                   }
               }
            }
        }
        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        jan_summary.put("subcontractors", jan_subContracts.toString());
        feb_summary.put("subcontractors", feb_subContracts.toString());
        mar_summary.put("subcontractors", mar_subContracts.toString());
        apr_summary.put("subcontractors", apr_subContracts.toString());
        may_summary.put("subcontractors", may_subContracts.toString());
        jun_summary.put("subcontractors", jun_subContracts.toString());
        jul_summary.put("subcontractors", jul_subContracts.toString());
        aug_summary.put("subcontractors", aug_subContracts.toString());
        sep_summary.put("subcontractors", sep_subContracts.toString());
        oct_summary.put("subcontractors", oct_subContracts.toString());
        nov_summary.put("subcontractors", nov_subContracts.toString());
        dec_summary.put("subcontractors", dec_subContracts.toString());
        all_summary.put("subcontractors", all_subContracts.toString());

        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);

        return summaryMap;
    }

    public HashMap<String, HashMap<String, String>> generatorSummaryInputs(int companyId, int branchId, int yearStart, int yearEnd) {
        if (branchId != -1) {
            companyId = -1;
        }
        String fy = (yearEnd == yearStart) ? "" + yearEnd + "" : yearStart + "/" + (yearEnd % 100);
        if (branchId != -1) {
            companyId = -1;
        }
        List<GeneratorsEntry> list = generatorsEntryManager.getCustomFiltered(branchId, companyId, fy);

        HashMap<String, String> jan_summary = new HashMap<>();
        List<String> jan_genNums = new ArrayList<>();
        jan_summary.put("na", Boolean.toString(true));
        jan_summary.put("m3", Float.toString(0));
        jan_summary.put("l", Float.toString(0));
        jan_summary.put("lkr", Float.toString(0));
        HashMap<String, String> feb_summary = new HashMap<>();
        List<String> feb_genNums = new ArrayList<>();
        feb_summary.put("na", Boolean.toString(true));
        feb_summary.put("m3", Float.toString(0));
        feb_summary.put("l", Float.toString(0));
        feb_summary.put("lkr", Float.toString(0));
        HashMap<String, String> mar_summary = new HashMap<>();
        List<String> mar_genNums = new ArrayList<>();
        mar_summary.put("na", Boolean.toString(true));
        mar_summary.put("m3", Float.toString(0));
        mar_summary.put("l", Float.toString(0));
        mar_summary.put("lkr", Float.toString(0));
        HashMap<String, String> apr_summary = new HashMap<>();
        List<String> apr_genNums = new ArrayList<>();
        apr_summary.put("na", Boolean.toString(true));
        apr_summary.put("m3", Float.toString(0));
        apr_summary.put("l", Float.toString(0));
        apr_summary.put("lkr", Float.toString(0));
        HashMap<String, String> may_summary = new HashMap<>();
        List<String> may_genNums = new ArrayList<>();
        may_summary.put("na", Boolean.toString(true));
        may_summary.put("m3", Float.toString(0));
        may_summary.put("l", Float.toString(0));
        may_summary.put("lkr", Float.toString(0));
        HashMap<String, String> jun_summary = new HashMap<>();
        List<String> jun_genNums = new ArrayList<>();
        jun_summary.put("na", Boolean.toString(true));
        jun_summary.put("m3", Float.toString(0));
        jun_summary.put("l", Float.toString(0));
        jun_summary.put("lkr", Float.toString(0));
        HashMap<String, String> jul_summary = new HashMap<>();
        List<String> jul_genNums = new ArrayList<>();
        jul_summary.put("na", Boolean.toString(true));
        jul_summary.put("m3", Float.toString(0));
        jul_summary.put("l", Float.toString(0));
        jul_summary.put("lkr", Float.toString(0));
        HashMap<String, String> aug_summary = new HashMap<>();
        List<String> aug_genNums = new ArrayList<>();
        aug_summary.put("na", Boolean.toString(true));
        aug_summary.put("m3", Float.toString(0));
        aug_summary.put("l", Float.toString(0));
        aug_summary.put("lkr", Float.toString(0));
        HashMap<String, String> sep_summary = new HashMap<>();
        List<String> sep_genNums = new ArrayList<>();
        sep_summary.put("na", Boolean.toString(true));
        sep_summary.put("m3", Float.toString(0));
        sep_summary.put("l", Float.toString(0));
        sep_summary.put("lkr", Float.toString(0));
        HashMap<String, String> oct_summary = new HashMap<>();
        List<String> oct_genNums = new ArrayList<>();
        oct_summary.put("na", Boolean.toString(true));
        oct_summary.put("m3", Float.toString(0));
        oct_summary.put("l", Float.toString(0));
        oct_summary.put("lkr", Float.toString(0));
        HashMap<String, String> nov_summary = new HashMap<>();
        List<String> nov_genNums = new ArrayList<>();
        nov_summary.put("na", Boolean.toString(true));
        nov_summary.put("m3", Float.toString(0));
        nov_summary.put("l", Float.toString(0));
        nov_summary.put("lkr", Float.toString(0));
        HashMap<String, String> dec_summary = new HashMap<>();
        List<String> dec_genNums = new ArrayList<>();
        dec_summary.put("na", Boolean.toString(true));
        dec_summary.put("m3", Float.toString(0));
        dec_summary.put("l", Float.toString(0));
        dec_summary.put("lkr", Float.toString(0));
        HashMap<String, String> all_summary = new HashMap<>();
        List<String> all_genNums = new ArrayList<>();
        all_summary.put("na", Boolean.toString(true));
        all_summary.put("m3", Float.toString(0));
        all_summary.put("l", Float.toString(0));
        all_summary.put("lkr", Float.toString(0));
        for (GeneratorsEntry o : list) {
            switch (o.getMonth()) {
                case 0: {
                    jan_genNums.add(o.getGeneratorNumber());
                    jan_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        jan_summary.put("m3", Float.toString(Float.parseFloat(jan_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        jan_summary.put("l", Float.toString(Float.parseFloat(jan_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        jan_summary.put("lkr", Float.toString(Float.parseFloat(jan_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 1: {
                    feb_genNums.add(o.getGeneratorNumber());
                    feb_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        feb_summary.put("m3", Float.toString(Float.parseFloat(feb_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        feb_summary.put("l", Float.toString(Float.parseFloat(feb_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        feb_summary.put("lkr", Float.toString(Float.parseFloat(feb_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 2: {
                    mar_genNums.add(o.getGeneratorNumber());
                    mar_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        mar_summary.put("m3", Float.toString(Float.parseFloat(mar_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        mar_summary.put("l", Float.toString(Float.parseFloat(mar_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        mar_summary.put("lkr", Float.toString(Float.parseFloat(mar_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 3: {
                    apr_genNums.add(o.getGeneratorNumber());
                    apr_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        apr_summary.put("m3", Float.toString(Float.parseFloat(apr_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        apr_summary.put("l", Float.toString(Float.parseFloat(apr_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        apr_summary.put("lkr", Float.toString(Float.parseFloat(apr_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 4: {
                    may_genNums.add(o.getGeneratorNumber());
                    may_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        may_summary.put("m3", Float.toString(Float.parseFloat(may_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        may_summary.put("l", Float.toString(Float.parseFloat(may_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        may_summary.put("lkr", Float.toString(Float.parseFloat(may_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 5: {
                    jun_genNums.add(o.getGeneratorNumber());
                    jun_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        jun_summary.put("m3", Float.toString(Float.parseFloat(jun_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        jun_summary.put("l", Float.toString(Float.parseFloat(jun_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        jun_summary.put("lkr", Float.toString(Float.parseFloat(jun_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 6: {
                    jul_genNums.add(o.getGeneratorNumber());
                    jul_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        jul_summary.put("m3", Float.toString(Float.parseFloat(jul_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        jul_summary.put("l", Float.toString(Float.parseFloat(jul_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        jul_summary.put("lkr", Float.toString(Float.parseFloat(jul_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 7: {
                    aug_genNums.add(o.getGeneratorNumber());
                    aug_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        aug_summary.put("m3", Float.toString(Float.parseFloat(aug_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        aug_summary.put("l", Float.toString(Float.parseFloat(aug_summary.get("l") )+ o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        aug_summary.put("lkr", Float.toString(Float.parseFloat(aug_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 8: {
                    sep_genNums.add(o.getGeneratorNumber());
                    sep_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        sep_summary.put("m3", Float.toString(Float.parseFloat(sep_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        sep_summary.put("l", Float.toString(Float.parseFloat(sep_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        sep_summary.put("lkr", Float.toString(Float.parseFloat(sep_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 9: {
                    oct_genNums.add(o.getGeneratorNumber());
                    oct_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        oct_summary.put("m3", Float.toString(Float.parseFloat(oct_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        oct_summary.put("l", Float.toString(Float.parseFloat(oct_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        oct_summary.put("lkr", Float.toString(Float.parseFloat(oct_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 10: {
                    nov_genNums.add(o.getGeneratorNumber());
                    nov_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        nov_summary.put("m3", Float.toString(Float.parseFloat(nov_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        nov_summary.put("l", Float.toString(Float.parseFloat(nov_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        nov_summary.put("lkr", Float.toString(Float.parseFloat(nov_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 11: {
                    dec_genNums.add(o.getGeneratorNumber());
                    dec_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        dec_summary.put("m3", Float.toString(Float.parseFloat(dec_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        dec_summary.put("l", Float.toString(Float.parseFloat(dec_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        dec_summary.put("lkr", Float.toString(Float.parseFloat(dec_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
                case 12: {
                    all_genNums.add(o.getGeneratorNumber());
                    all_summary.put("na", Boolean.toString(false));
                    if (o.getUnits() == Units.m3.unit) {
                        all_summary.put("m3", Float.toString(Float.parseFloat(all_summary.get("m3")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.liters.unit) {
                        all_summary.put("l", Float.toString(Float.parseFloat(all_summary.get("l")) + o.getConsumption()));
                    } else if (o.getUnits() == Units.LKR.unit) {
                        all_summary.put("lkr", Float.toString(Float.parseFloat(all_summary.get("lkr")) + o.getConsumption()));
                    }
                    break;
                }
            }
        }

        HashMap<String, HashMap<String, String>> summaryMap = new HashMap<>();
        jan_summary.put("nums", fromListToString(jan_genNums));
        feb_summary.put("nums", fromListToString(feb_genNums));
        mar_summary.put("nums",  fromListToString(mar_genNums));
        apr_summary.put("nums",  fromListToString(apr_genNums));
        may_summary.put("nums",  fromListToString(may_genNums));
        jun_summary.put("nums",  fromListToString(jun_genNums));
        jul_summary.put("nums",  fromListToString(jul_genNums));
        aug_summary.put("nums",  fromListToString(aug_genNums));
        sep_summary.put("nums",  fromListToString(sep_genNums));
        oct_summary.put("nums",  fromListToString(oct_genNums));
        nov_summary.put("nums",  fromListToString(nov_genNums));
        dec_summary.put("nums",  fromListToString(dec_genNums));
        all_summary.put("nums",  fromListToString(all_genNums));
        summaryMap.put("jan", jan_summary);
        summaryMap.put("feb", feb_summary);
        summaryMap.put("mar", mar_summary);
        summaryMap.put("apr", apr_summary);
        summaryMap.put("may", may_summary);
        summaryMap.put("jun", jun_summary);
        summaryMap.put("jul", jul_summary);
        summaryMap.put("aug", aug_summary);
        summaryMap.put("sep", sep_summary);
        summaryMap.put("oct", oct_summary);
        summaryMap.put("nov", nov_summary);
        summaryMap.put("dec", dec_summary);
        summaryMap.put("all", all_summary);
        return summaryMap;
    }

    public List<EmissionSrcDataEntryStatusBean> getStatus(int companyId, int branchId, String fy) {
//        List<EmissionSrcDataEntryStatusBean> list = null;
//
//        try {
//            list = (List<EmissionSrcDataEntryStatusBean>) emissionSrcDataEntryStatusManager.findStatuses(companyId, branchId, fy);
//        }catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//        return list;

        ProjectDataEntryStatusIdBean idBean = new ProjectDataEntryStatusIdBean();
        idBean.setBranchId(branchId);
        idBean.setCompanyId(companyId);
        idBean.setFY(fy);
        idBean.setEmissionSrc(1);

        EmissionSrcDataEntryStatusBean elec = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(2);
        EmissionSrcDataEntryStatusBean mun_water = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(3);
        EmissionSrcDataEntryStatusBean refri = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(4);
        EmissionSrcDataEntryStatusBean fire_ext = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(5);
        EmissionSrcDataEntryStatusBean waste_trans = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(6);
        EmissionSrcDataEntryStatusBean com_owned = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(7);
        EmissionSrcDataEntryStatusBean emp_comm = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(8);
        EmissionSrcDataEntryStatusBean air_travel = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(9);
        EmissionSrcDataEntryStatusBean rented = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(10);
        EmissionSrcDataEntryStatusBean hired = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(11);
        EmissionSrcDataEntryStatusBean generator = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(12);
        EmissionSrcDataEntryStatusBean waste_dis = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
        idBean.setEmissionSrc(13);
        EmissionSrcDataEntryStatusBean trans_loc_pur = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);


        idBean.setEmissionSrc(14);
        EmissionSrcDataEntryStatusBean freight = (EmissionSrcDataEntryStatusBean) emissionSrcDataEntryStatusManager.getEntityByKey(idBean);


        List<EmissionSrcDataEntryStatusBean> list = new ArrayList<>();
        list.add(elec);
        list.add(mun_water);
        list.add(refri);
        list.add(fire_ext);
        list.add(waste_trans);
        list.add(com_owned);
        list.add(emp_comm);
        list.add(air_travel);
        list.add(rented);
        list.add(hired);
        list.add(generator);
        list.add(waste_dis);
        list.add(trans_loc_pur);
        list.add(freight);

        return list;
    }

    public EmissionSrcDataEntryStatusBean manageStatus(int companyId, int branchId, String fy, int emSrc, int status) {
        ProjectDataEntryStatusIdBean idBean = new ProjectDataEntryStatusIdBean();
        idBean.setBranchId(branchId);
        idBean.setCompanyId(companyId);
        idBean.setFY(fy);

        idBean.setEmissionSrc(emSrc);
        EmissionSrcDataEntryStatusBean bean = null;
        try {
            bean = (EmissionSrcDataEntryStatusBean)emissionSrcDataEntryStatusManager.getEntityByKey(idBean);
            bean.setStatus(status);
            if (bean.getStatus() != status && status == DataEntryStatus.COMPLETED.status && branchId == -1) {
                //fetch all ther branches
                JsonObject filter = new JsonObject();
                JsonObject comFiltr = new JsonObject();
                comFiltr.addProperty("type", 1);
                comFiltr.addProperty("value", companyId);
                comFiltr.addProperty("col",1);
                filter.add("companyId", comFiltr);
                List<Branch> branches = branchManager.getPaginatedEntityListByFilter(-1, "", filter).getList();
                for(Branch b : branches) {
                    for (int src = 1; src < 14; src++) {
                        EmissionSrcDataEntryStatusBean statusBean = new EmissionSrcDataEntryStatusBean();
                        ProjectDataEntryStatusIdBean id = new ProjectDataEntryStatusIdBean();
                        id.setEmissionSrc(src);
                        id.setFY(fy);
                        id.setCompanyId(companyId);
                        id.setBranchId(b.getId());
                        statusBean.setStatus(status);
                        statusBean.setId(id);
                        emissionSrcDataEntryStatusManager.updateEntity(bean);
                    }
                }
            }
            emissionSrcDataEntryStatusManager.updateEntity(bean);

        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return bean;
    }


    private String fromListToString(List<String> list) {
        StringBuilder stringBuilder =  new StringBuilder();
        if (list != null) {

            for(int i = 0; i < list.size(); i ++) {
                stringBuilder.append(list.get(i));
                if (i != list.size() -1) {
                    stringBuilder.append(",");
                }
            }
        }
        return stringBuilder.toString();
    }
}
