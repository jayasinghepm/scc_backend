package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.TransportLocalEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.TransportLocalEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class TransportLocalPopulator {

    private static Logger logger = Logger.getLogger(TransportLocalPopulator.class);
    private static TransportLocalPopulator instance;

    public static TransportLocalPopulator getInstance() {
        if (instance == null) {
            instance = new TransportLocalPopulator();
        }
        return instance;
    }

    public TransportLocalEntryDTO populateDTO(TransportLocalEntry entry) {
        TransportLocalEntryDTO dto = new TransportLocalEntryDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setEntryAddedDate(entry.getEntryAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMaterialype(entry.getMaterialType());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setSubContractorName(entry.getSubContractorName());
        dto.setVehicleType(entry.getVehicleType());
        dto.setFuelType(entry.getFuelType());
        dto.setFuelEconomy(entry.getFuelEconomy());
        dto.setLoadingCapacity(entry.getLoadingCapacity());
        dto.setDistanceTravelled(entry.getDistanceTravelled());
        dto.setWeight(entry.getWeight());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setFuel(entry.getFuel());
        dto.setVehicle(entry.getVehicle());
        dto.setNoOfTrips(entry.getNoOfTrips());
        dto.setFuelConsumption(entry.getFuelConsumption());

        return dto;
    }

    public TransportLocalEntry populate(TransportLocalEntry entry, TransportLocalEntryDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }
            }
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setMaterialype(dto.getMaterialType());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setSubContractorName(dto.getSubContractorName());
            entry.setVehicleType(dto.getVehicleType());
            entry.setFuelType(dto.getFuelType());
            entry.setFuelEconomy(dto.getFuelEconomy());
            entry.setLoadingCapacity(dto.getLoadingCapacity());
            entry.setDistanceTravelled(dto.getDistanceTravelled());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
            entry.setFuel(dto.getFuel() == null ? entry.getFuel() : dto.getFuel());
            entry.setVehicle(dto.getVehicle() == null ? entry.getVehicle() : dto.getVehicle());
            entry.setNoOfTrips(dto.getNoOfTrips());
            entry.setFuelConsumption(dto.getFuelConsumption());
            entry.setWeight(dto.getWeight());

        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
