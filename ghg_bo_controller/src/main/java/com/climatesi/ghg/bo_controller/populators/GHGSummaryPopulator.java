package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GHGEmissionSummaryDTO;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.GHGEmissionSummaryBean;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.HashMap;

public class GHGSummaryPopulator {

    private final static Logger logger = Logger.getLogger(GHGSummaryPopulator.class);
    private static GHGSummaryPopulator instance;

    public static GHGSummaryPopulator getInstance() {
        if (instance == null) {
            instance = new GHGSummaryPopulator();
        }
        return instance;
    }

    public GHGEmissionSummaryDTO populateDTO(GHGEmissionSummaryBean summaryBean) {
        GHGEmissionSummaryDTO dto = new GHGEmissionSummaryDTO();
        dto.setMode(summaryBean.getModeOfEntry());
        dto.setCompanyId(summaryBean.getId().getCompanyId());
        dto.setFy(summaryBean.getId().getFY());
        JsonObject jsonObject = new JsonObject();
        summaryBean.getEmission().forEach((k, v) -> {
            if (v instanceof String) {
                jsonObject.addProperty(k, (String) v);
            }
//            } else if (v instanceof HashMap) {
//                HashMap<String, String> map = (HashMap) v;
//                JsonObject innerObj = new JsonObject();
//                map.forEach((s, t) -> {
//                    innerObj.addProperty(s, t);
//                });
//                jsonObject.add(k, innerObj);
//            }
        });
        dto.setEmissionInfo(jsonObject);
        return dto;
    }

    public GHGEmissionSummaryDTO fromHashMap(int comId,String fy, int mode, HashMap<String, String> map) {
        GHGEmissionSummaryDTO dto = new GHGEmissionSummaryDTO();
        dto.setCompanyId(comId);
        dto.setMode(mode);
        dto.setFy(fy);
        JsonObject jsonObject = new JsonObject();
        map.forEach((k, v) -> {
            if (v instanceof String) {
                jsonObject.addProperty(k, (String) v);
            }
//            if (v instanceof List) {
//                List<String> vals = (List<String>)v;
//                if (vals.size() >= 3) {
//                    jsonObject.addProperty(k, vals.get(0) + "-" + vals.get(1) + "-" +vals.get(2));
//                }
//            }
//            }else if (v instanceof HashMap) {
//                HashMap<String, String> insideMap = (HashMap)v;
//                JsonObject innerObj = new JsonObject();
//                insideMap.forEach((s,t)-> {
//                    innerObj.addProperty(s,t);
//                });
//                jsonObject.add(k, innerObj);
//            }
        });
        dto.setEmissionInfo(jsonObject);
        return dto;
    }
}

