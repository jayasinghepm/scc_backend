package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.RefrigerantsPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.RefrigerantsEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.RefrigerantsEntry;
import com.climatesi.ghg.emission_source.api.enums.RefrigerantTypes;
import com.climatesi.ghg.emission_source.api.facades.RefrigerantsEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.RefrigerantsEntryBean;
import com.climatesi.ghg.implGeneral.data.GWP_R407C;
import com.climatesi.ghg.implGeneral.data.GWP_R410A;
import com.climatesi.ghg.implGeneral.data.RefrigerantLeakageCalData;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class RefrigerantsController extends AbstractController {
    private static final Logger logger = Logger.getLogger(RefrigerantsController.class);
    private RefrigerantsEntryManager refrigerantsEntryManager;
    private EmissionFactorsManager emissionFactorsManager;


    @PostConstruct
    @Override
    public void initialize() {

        try {
            refrigerantsEntryManager = EmsissionSourceFactory.getInstance().getRefrigerantsEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing Refrigerant Controller", e);
        }
    }

    public ListResult<RefrigerantsEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return refrigerantsEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public RefrigerantsEntry editRefrigerantsEntry(RefrigerantsEntryDTO dto) throws GHGException {
        RefrigerantsEntry entry;

        String status = null;
        try {
//        update
            if (dto.getRefrigerantsEntryId() > 0) {
                entry = findById(dto.getRefrigerantsEntryId());
                RefrigerantsPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emissionRefrigerantLeakage(entry));

                }

                //          Todo: add user id
                status = (String) refrigerantsEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("RefrigerantsEntry is not found");
                }
//         add new
            } else {
                entry = new RefrigerantsEntryBean();
                RefrigerantsPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emissionRefrigerantLeakage(entry));
                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) refrigerantsEntryManager.addEntity(entry);
            }
//        Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public RefrigerantsEntry findById(Integer id) {
        return refrigerantsEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emissionRefrigerantLeakage(RefrigerantsEntry entry) {

        float consumption_r22 = 0;
        float consumption_r407c = 0;
        float consumption_r410a = 0;
        float consumption_hfc134a = 0;

        // Todo consider other refrigerant types as well
        HashMap<String, String> results = new HashMap<>();
        if (entry != null) {
            if (entry.getTypeofRefrigerent() == RefrigerantTypes.R22.type) {
                consumption_r22 += entry.getAmountRefilled();
                consumption_r22 = (consumption_r22 / 1000); // tons
                float gwp_r22 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R22).getValue();
                RefrigerantLeakageCalData r22Data = new RefrigerantLeakageCalData();
                r22Data.setWeight(consumption_r22);
                r22Data.setGfp(gwp_r22);
                float e_r22 = Calculator.emissionRefrigernatLeakage(r22Data);
                results.put("tco2",String.format("%.5f", e_r22));
                results.put("co2",String.format("%.5f", 0f) );
                results.put("n20",String.format("%.5f", 0f) );
                results.put("ch4",String.format("%.5f", 0f) );
                results.put("quantity",String.format("%.5f", consumption_r22) );
                results.put("direct", Boolean.toString(true));
                results.put("r22",String.format("%.5f",consumption_r22) );
                results.put("gwp_r22",String.format("%.5f", gwp_r22));

            }
          else if (entry.getTypeofRefrigerent() == RefrigerantTypes.HFC134A.type) {
                consumption_hfc134a += entry.getAmountRefilled();
                consumption_hfc134a = (consumption_hfc134a / 1000); // tons
               // float gwp_r22 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R22).getValue();
                float gwp_hfc134a= 1300;
                RefrigerantLeakageCalData r22Data = new RefrigerantLeakageCalData();
                r22Data.setWeight(consumption_hfc134a);
                r22Data.setGfp(gwp_hfc134a);
                float e_r22 = Calculator.emissionRefrigernatLeakage(r22Data);
                results.put("tco2",String.format("%.5f", e_r22));
                results.put("co2",String.format("%.5f", 0f) );
                results.put("n20",String.format("%.5f", 0f) );
                results.put("ch4",String.format("%.5f", 0f) );
                results.put("quantity",String.format("%.5f", consumption_hfc134a) );
                results.put("direct", Boolean.toString(true));
                results.put("r22",String.format("%.5f",consumption_hfc134a) );
                results.put("gwp_r22",String.format("%.5f", gwp_hfc134a));

            }
            else if (entry.getTypeofRefrigerent() == RefrigerantTypes.R407C.type) {
                consumption_r407c += entry.getAmountRefilled();
                consumption_r407c = (consumption_r407c / 1000); // todo: verify tons
                // r407c
                float r407c_ch2f2_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_PERCENT_CH2F2).getValue();
                float r407c_ch2f2_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_COFF_CH2F2).getValue();
                float r407c_cf3chf2_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_PERCENT_CF3CHF2).getValue();
                float r407c_cf3chf2_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_COFF_CF3CHF2).getValue();
                float r407c_cf3ch2f_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_PERCENT_CF3CH2F).getValue();
                float r407c_cf3ch2f_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_COFF_CF3CH2F).getValue();
                GWP_R407C data = new GWP_R407C();
                data.setWeight(consumption_r407c);
                data.setCh2f2_percent(r407c_ch2f2_percent);
                data.setCh2f2_coff(r407c_ch2f2_coff);
                data.setCf3ch2f_coff(r407c_cf3ch2f_coff);
                data.setCf3ch2f_percent(r407c_cf3ch2f_percent);
                data.setCf3chf2_percent(r407c_cf3chf2_percent);
                data.setCf3chf2_coff(r407c_cf3chf2_coff);

                float gwp_r407c = Calculator.gwp_R407C(data);


                RefrigerantLeakageCalData r407cData = new RefrigerantLeakageCalData();
                //change based on WD's comments
//                r407cData.setWeight(consumption_r407c);
//                r407cData.setGfp(gwp_r407c);
//                float e_r407c = Calculator.emissionRefrigernatLeakage(r407cData);
                float e_r407c = gwp_r407c;
                results.put("tco2",String.format("%.5f", e_r407c) );
                results.put("co2",String.format("%.5f", 0f));
                results.put("n20",String.format("%.5f", 0f) );
                results.put("ch4",String.format("%.5f",0f));
                results.put("quantity",String.format("%.5f", consumption_r407c));
                results.put("direct", Boolean.toString(true));
                results.put("r407c",String.format("%.5f", consumption_r407c));
                results.put("r22",String.format("%.5f", consumption_r22) );
                results.put("r407c_ch2f2_percent",String.format("%.5f", r407c_ch2f2_percent));
                results.put("r407c_ch2f2_coff",String.format("%.5f",r407c_ch2f2_coff));
                results.put("r407c_cf3chf2_percent",String.format("%.5f",r407c_cf3chf2_percent) );
                results.put("r407c_cf3chf2_coff",String.format("%.5f", r407c_cf3chf2_coff));
                results.put("r407c_cf3ch2f_percent",String.format("%.5f", r407c_cf3ch2f_percent));
                results.put("r407c_cf3ch2f_coff",String.format("%.5f", r407c_cf3ch2f_coff) );
                results.put("gwp_r407c",String.format("%.5f", gwp_r407c));


            } else if (entry.getTypeofRefrigerent() == RefrigerantTypes.R410A.type) {
                consumption_r410a += entry.getAmountRefilled();
                consumption_r410a = (consumption_r410a / 1000); //todo: verify tons
                float r410a_ch2f2_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_PERCENT_CH2F2).getValue();
                float r410a_chf2cf3_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_PERCENT_CHF2CF3).getValue();
                float r410a_ch2f2_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_COFF_CH2F2).getValue();
                float r410a_chf2cf3_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_COFF_CHF2CF3).getValue();

                GWP_R410A gwp_r410A_data = new GWP_R410A();
                gwp_r410A_data.setWeight(consumption_r410a);
                gwp_r410A_data.setCh2f2_coff(r410a_ch2f2_coff);
                gwp_r410A_data.setCh2f2_percent(r410a_ch2f2_percent);
                gwp_r410A_data.setChf2cf3_coff(r410a_chf2cf3_coff);
                gwp_r410A_data.setChf2cf3_percent(r410a_chf2cf3_percent);

                float gwp_410a = Calculator.gwp_R410A(gwp_r410A_data);
                // changed according to WD comments
//                RefrigerantLeakageCalData r410aData = new RefrigerantLeakageCalData();
//                r410aData.setGfp(gwp_410a);
//                r410aData.setWeight(consumption_r410a);
//                float e_410a = Calculator.emissionRefrigernatLeakage(r410aData);
                float e_410a = gwp_410a;
                results.put("tco2",String.format("%.5f", e_410a));
                results.put("co2",String.format("%.5f", e_410a) );
                results.put("n20",String.format("%.5f", 0f));
                results.put("ch4",String.format("%.5f", 0f) );
                results.put("quantity",String.format("%.5f", consumption_r410a));
                results.put("direct", Boolean.toString(true));
                results.put("r410a",String.format("%.5f", consumption_r410a) );
                results.put("r410a_ch2f2_percent",String.format("%.5f", r410a_ch2f2_percent));
                results.put("r410a_chf2cf3_percent",String.format("%.5f",r410a_chf2cf3_percent));
                results.put("r410a_ch2f2_coff",String.format("%.5f", r410a_ch2f2_coff));
                results.put("r410a_chf2cf3_coff",String.format("%.5f", r410a_chf2cf3_coff) );
                results.put("gwp_410a",String.format("%.5f", gwp_410a) );
            }
        } else {
            logger.error("Error null entry");
        }


//        return e_r22 + e_410a + e_r407c;


        return results;
    }


}
