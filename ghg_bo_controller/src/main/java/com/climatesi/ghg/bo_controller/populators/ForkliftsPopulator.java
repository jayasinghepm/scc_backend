package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ForkliftsEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ForkliftsEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class ForkliftsPopulator {

    private static Logger logger = Logger.getLogger(ForkliftsPopulator.class);
    private static ForkliftsPopulator instance;



    public static ForkliftsPopulator getInstance() {
        if (instance == null) {
            instance = new ForkliftsPopulator();
        }
        return instance;
    }

    public ForkliftsEntryDTO populateDTO(Forklifts entry) {
        ForkliftsEntryDTO dto = new ForkliftsEntryDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setFuelConsumption(entry.getFuelConsumption());
        dto.setFuleType(entry.getFuleType());

        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public Forklifts populate(Forklifts entry, ForkliftsEntryDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(dto.getIsDeleted());
                    return entry;
                }
            }

            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setFuelConsumption(dto.getFuelConsumption());
            entry.setFuleType(dto.getFuleType());

            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
