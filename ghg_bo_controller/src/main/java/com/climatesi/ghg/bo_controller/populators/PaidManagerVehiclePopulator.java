package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaidManagerVehicleDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaidManagerVehicleDTO;
import com.climatesi.ghg.emission_source.api.beans.PaidManagerVehicles;
import com.climatesi.ghg.emission_source.api.beans.PaidManagerVehicles;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class PaidManagerVehiclePopulator {

    private static Logger logger = Logger.getLogger(PaidManagerVehiclePopulator.class);
    private static PaidManagerVehiclePopulator instance;



    public static PaidManagerVehiclePopulator getInstance() {
        if (instance == null) {
            instance = new PaidManagerVehiclePopulator();
        }
        return instance;
    }

    public PaidManagerVehicleDTO populateDTO(PaidManagerVehicles entry) {
        PaidManagerVehicleDTO dto = new PaidManagerVehicleDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setDistance(entry.getDistance());
        dto.setFuleType(entry.getFuleType());
        dto.setFuelEconomy(entry.getFuelEconomy());

        dto.setNoOfTrips(entry.getNoOfTrips());
        dto.setFuelConsumption(entry.getFuelConsumption());


        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public PaidManagerVehicles populate(PaidManagerVehicles entry, PaidManagerVehicleDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(dto.getIsDeleted());
                    return entry;
                }
            }

            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());

            entry.setFuelEconomy(dto.getFuelEconomy());
            entry.setFuleType(dto.getFuleType());
            entry.setDistance(dto.getDistance());

            entry.setFuelConsumption(dto.getFuelConsumption());
            entry.setNoOfTrips(dto.getNoOfTrips());

            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
