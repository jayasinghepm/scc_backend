package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.FireExtingPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.FireExtingEntryDTO;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.FireExtingEntry;
import com.climatesi.ghg.emission_source.api.facades.FireExtingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.FireExtingEntryBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class FireExtingController extends AbstractController {

    private static final Logger logger = Logger.getLogger(FireExtingController.class);
    private FireExtingEntryManager fireExtingEntryManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            fireExtingEntryManager = EmsissionSourceFactory.getInstance().getFireExtingEntryManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing FireExt controller", e);
        }
    }

    public ListResult<FireExtingEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return fireExtingEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public FireExtingEntry editFireExtingEntry(FireExtingEntryDTO dto) throws GHGException {
        FireExtingEntry entry;

        String status = null;
        try {
//        update
            if (dto.getFireExtEntryId() > 0f) {
                entry = findById(dto.getFireExtEntryId());
                entry = FireExtingPopulator.getInstance().populate(entry, dto);
                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emissionFireExt(entry));
                }

                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id
                status = (String) fireExtingEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new FireExtingEntryBean();
                entry = FireExtingPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emissionFireExt(entry));
                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) fireExtingEntryManager.addEntity(entry);
            }

//        todo: status
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public FireExtingEntry findById(Integer id) {
        return fireExtingEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emissionFireExt(FireExtingEntry o) {

        float emission = 0f;
        float totWeight = 0f;

        if (o != null) {
            emission += ((o.getWeight() / 1000) * o.getNoOfTanks());
            totWeight += o.getWeight();
        }

        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", emission));
        results.put("co2",String.format("%.5f", 0f));
        results.put("n20",String.format("%.5f", 0f) );
        results.put("ch4",String.format("%.5f", 0f) );
        results.put("weight",String.format("%.5f", 0f) );
        results.put("noOfTanks",String.format("%.5f", 0f) );
        results.put("direct", Boolean.toString(true));
        return results;
    }

}
