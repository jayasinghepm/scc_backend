package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.ghg_project.core.ProjectDTO;
import org.jboss.logging.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProjectPopulator {
    private static Logger logger = Logger.getLogger(ProjectPopulator.class);
    private static ProjectPopulator instance;

    private ProjectPopulator() {

    }

    public static ProjectPopulator getInstance() {
        if (instance == null) {
            instance = new ProjectPopulator();
        }
        return instance;
    }

    public ProjectDTO populateDTO(Project project) {
        ProjectDTO dto = new ProjectDTO();
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-d");
            dto.setId(project.getId());
            dto.setCompanyId(project.getCompanyId());

            dto.setExtended(project.isExtended());
            dto.setSignedWith(project.getSignedWith());
            dto.setStatus(project.getStatus());
            dto.setNumberOfPayments(project.getNumberOfPayments());
            dto.setName(project.getName());
            dto.setCompany(project.getCompany());
            dto.setDisableFormula(project.getDisableFormula());
            if (project.getEndDate() != null) {
                dto.setEndDate(format.format(project.getEndDate()));
            }
            if (project.getIntiatedDate() != null) {
                dto.setIntiatedDate(format.format(project.getIntiatedDate()));
            }
            if (project.getExtendedDate() != null) {
                dto.setExtendedDate(format.format(project.getExtendedDate()));
            }
            if (project.getSignedDate() != null) {
                dto.setSignedDate(format.format(project.getSignedDate()));
            }
            if (project.getFirstPayDate() != null) {
                dto.setFirstPayDate(format.format(project.getFirstPayDate()));

            }
            if (project.getSecondPayDate() != null) {
                dto.setSecondPayDate(format.format(project.getSecondPayDate()));
            }
            if (project.getFinalPayDate() != null) {
                dto.setFinalPayDate(format.format(project.getFinalPayDate()));

            }
        } catch (Exception e) {
            logger.error("Error in populateing ProjectDTO", e);
        }
        dto.setStatusStr(project.getStatusStr());
        return dto;
    }

    public Project populate(Project project, ProjectDTO dto) {
        try {
            if (dto.getId() > 0) {
                project.setId(dto.getId());
                if (dto.isDeleted() == 1) {
                    project.setDeleted(dto.isDeleted());
                    return project;
                }
                if (dto.getDisableFormula()!= project.getDisableFormula() ) {
                    project.setDisableFormula(dto.getDisableFormula());
                    return project;
                }
                if (project.getStatus() != dto.getStatus()) {
                    project.setStatus(dto.getStatus());
                    return project;
                }
                if (!project.isExtended() && dto.isExtended()) {
                    project.setExtended(dto.isExtended());
                }
            }
            project.setCompanyId(dto.getCompanyId() == 0 ? project.getCompanyId() : dto.getCompanyId());
//            project.setEndDate(dto.getEndDate() == null ? project.getEndDate() : dto.getEndDate());
            project.setSignedWith(dto.getSignedWith() == 0 ? project.getSignedWith() : dto.getSignedWith());
            project.setStatus(dto.getStatus() == 0 ? project.getStatus() : dto.getStatus());
            project.setNumberOfPayments(dto.getNumberOfPayments() == 0 ? project.getNumberOfPayments() : dto.getNumberOfPayments());
            project.setName(dto.getName() == null ? project.getName() : dto.getName());
//            project.setIntiatedDate(dto.getIntiatedDate().equals("") ? project.getIntiatedDate() : dto.getIntiatedDate());
//            project.setSignedDate(dto.getSignedDate() == null ? project.getSignedDate() : dto.getSignedDate());
//            project.setFinalPayDate(dto.getFinalPayDate() == null ? project.getFinalPayDate() : dto.getFinalPayDate());
//            project.setFirstPayDate(dto.getFirstPayDate() == null ? project.getFirstPayDate() : dto.getFirstPayDate());
//            project.setSecondPayDate(dto.getSecondPayDate() == null ? project.getSecondPayDate(): dto.getSecondPayDate());

            project.setCompany(dto.getCompany() == null ? project.getCompany() : dto.getCompany());
            project.setDisableFormula(project.getDisableFormula());
            try {
                DateFormat format = new SimpleDateFormat("yyyy-MM-d");
                Date date;
                String string = dto.getExtendedDate();
                if (string != null && !string.equals("")) {
                    date = format.parse(string);
                    project.setExtendedDate(date);
                }


                string = dto.getIntiatedDate();
                if (string != null && !string.equals("")) {
                    date = format.parse(string);
                    project.setIntiatedDate(date);
                }
                string = dto.getEndDate();
                if (string != null && !string.equals("")) {
                    date = format.parse(string);
                    project.setEndDate(date);
                }
                string = dto.getFirstPayDate();
                if (string != null && !string.equals("")) {
                    date = format.parse(string);
                    project.setFirstPayDate(date);
                }
                string = dto.getSecondPayDate();
                if (string != null && !string.equals("")) {
                    date = format.parse(string);
                    project.setSecondPayDate(date);
                }
                string = dto.getFinalPayDate();
                if (string != null && !string.equals("")) {
                    date = format.parse(string);
                    project.setFinalPayDate(date);
                }
                string = dto.getSignedDate();
                if (string != null && !string.equals("")) {
                    date = format.parse(string);
                    project.setSignedDate(date);
                }
                project.setStatusStr(dto.getStatusStr() == null ? project.getStatusStr() : dto.getStatusStr());

            } catch (Exception e) {
                logger.error("Error in parsing dates", e);
            }
        } catch (NullPointerException e) {
            logger.error("Error in poulating ", e);
        }
        return project;
    }

}
