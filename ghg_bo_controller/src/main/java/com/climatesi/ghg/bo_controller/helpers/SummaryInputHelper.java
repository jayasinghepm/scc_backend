package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project.ProjectSummaryController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.insitution.InsstitutionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.summary_inputs.SummaryInputController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_controller.populators.SummaryDataInputPopulator;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.CommonSummaryInputDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectDataEntryStatusDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectFullSummaryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.GetProjectFullSummaryReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.ManageProjectDataEntryStatusReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.ProjectDataEntryStatusReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.request.SummaryInputsRequestMessage;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response.CommonSummaryInputResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response.GetProjectFullSummaryResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response.ManageProjectDataEntryStatusResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.response.ProjectDataEntrySTatusResMsg;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.EmissionSrcDataEntryStatusBean;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SummaryInputHelper {

    private static Logger logger = Logger.getLogger(SummaryInputHelper.class);
    private SummaryInputController summaryInputController;
    private InsstitutionController insstitutionController;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;
    private ProjectSummaryController  projectSummaryController;

    public SummaryInputHelper (){
        try {
            summaryInputController = GHGControllerFactory.getInstance().getSummaryInputController();
            insstitutionController = GHGControllerFactory.getInstance().getInsstitutionController();

            projectSummaryController = GHGControllerFactory.getInstance().getProjectSummaryController();

        }catch (GHGException e) {
            logger.error("Error in getting instance of  controllers >>", e);
        }



    }

    public Message getProjectFullSummary(GetProjectFullSummaryReqMsg msg, int user) {
        logger.info("Message : " + msg);
        GetProjectFullSummaryResMsg res = new GetProjectFullSummaryResMsg();
        try {
            ProjectFullSummaryDTO summary = projectSummaryController.getProjectSummary(msg.getCompanyId());
            if (summary != null) {
//                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setSummary(summary);
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed to get Project Full Summary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;

    }

    public Message  getBioMassSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.biomassSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message  getLPGEntry(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.lpgSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }


    public Message  getSeaAirFreightSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.freightSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message  getElecSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.elecSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getMunWaterSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.munWaterSummary(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getRefriSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.refriSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getFireExtSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.fireSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getWasteTransSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.wasteTransSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getCompanyOwnedSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.companyOwnedSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getEmpCommutingSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.empCommSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getAirtravelSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.airTravelSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getRentedSummaray(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.rentedSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getHiredSummaray(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.hiredSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getGeneratorsSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.generatorSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getWasteDisSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.wasteDisSummaryInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }
        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getTransLocPurchSummary(SummaryInputsRequestMessage msg, int user) {
        logger.info("Message : " + msg);
        CommonSummaryInputResMsg res = new CommonSummaryInputResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();
        try {
            HashMap<String, HashMap<String, String> > summary = summaryInputController.transLocPurchasedInputs(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (summary != null) {
                CommonSummaryInputDTO dto = SummaryDataInputPopulator.getInstance().populateDTO(summary);
                res.setDto(dto);

            } else {
                throw new Exception("Summary not found.");
            }

        }catch (Exception e) {
            logger.error("Failed in  getElecSummary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getDataEntryStatus(ProjectDataEntryStatusReqMsg message, int user) {
        ProjectDataEntrySTatusResMsg res = new ProjectDataEntrySTatusResMsg();
        res.setBranchId(message.getBranchId());
        res.setCompanyId(message.getCompanyId());
        res.setFy(message.getFy());
        try {
            List<EmissionSrcDataEntryStatusBean> list = summaryInputController.getStatus(message.getCompanyId(), message.getBranchId(), message.getFy());
            List<ProjectDataEntryStatusDTO> dtos = new ArrayList<>();
            list.forEach(k -> {
                dtos.add(SummaryDataInputPopulator.getInstance().populateProjectStatusDTO(k));
            });
            res.setList(dtos);
        }catch (Exception e) {
            logger.error("Failed in  getDataEntryStatus", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message manageDataEntryStatus(ManageProjectDataEntryStatusReqMsg message, int user) {
        ManageProjectDataEntryStatusResMsg res = new ManageProjectDataEntryStatusResMsg();
        try {
            EmissionSrcDataEntryStatusBean bean = summaryInputController.manageStatus(message.getDto().getCompanyId(), message.getDto().getBranchId(), message.getDto().getFy(), message.getDto().getEmissionSrc(), message.getDto().getStatus());
            res.setDto(SummaryDataInputPopulator.getInstance().populateProjectStatusDTO(bean));
        }catch (Exception e) {
            logger.error("Failed in  manageDataEntryStatus", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }
}
