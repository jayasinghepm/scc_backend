package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission.*;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_calc.EmissionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.insitution.InsstitutionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_controller.populators.*;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.request.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.response.*;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.emission_source.api.beans.*;
import com.climatesi.ghg.emission_source.api.beans.summary.GHGEmsissionSummary;
import com.climatesi.ghg.emission_source.api.enums.EmissionSummaryModeofEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.*;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.GHGEmissionSummaryIdBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.GHGEmissionSummaryBean;
import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;
import com.climatesi.ghg.implGeneral.EmissionCategoryBean;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.DirectCatEnum;
import com.climatesi.ghg.utility.api.enums.EmissinScope;
import com.climatesi.ghg.utility.api.enums.EmissionSourceEnum;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;
import org.joda.time.Years;

import java.util.*;

public class EmissionHelper {

    private static Logger logger = Logger.getLogger(EmissionHelper.class);
    private BusinessAirTravelController businessAirTravelController;
    private ElectricityController electricityController;
    private FireExtingController fireExtingController;
    private GeneratorsController generatorsController;
    private MunicipalWaterController municipalWaterController;
    private RefrigerantsController refrigerantsController;
    private TransportLocalController transportLocalController;
    private VehicleController vehicleController;
    private WasteDisposalController wasteDisposalController;
    private WasteTransportController wasteTransportController;
    private EmployeeCommutingController employeeCommutingController;
    private EmissionController emissionController;
    private InsstitutionController insstitutionController;
    private GHGEmissionSummaryController ghgEmissionSummaryController;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;
    private SeaAirFreightController seaAirFreightController;
    private BioMassEntryController bioMassEntryController;
    private LPGEntryController lpgEntryController;
    private BgasEntryController bgasEntryController;

    private AshTransportationController ashTransportationController;
    private ForkliftsController forkliftsController;
    private FurnaceController furnaceController;
    private LorryTransportationController lorryTransportationController;
    private OilAndGasTransportationController oilAndGasTransportationController;
    private PaidManagerVehicleController paidManagerVehicleController;
    private PaperWasteController paperWasteController;
    private RawMaterialTransportController rawMaterialTransportController;
    private SawDustTransportationController sawDustTransportationController;
    private SludgeTransportationController sludgeTransportationController;
    private StaffTransportationController staffTransportationController;
    private VehicleOthersController vehicleOthersController;


    public EmissionHelper() {
        try {
            ghgEmissionSummaryController = GHGControllerFactory.getInstance().getGHGEmissionSummaryController();
            insstitutionController = GHGControllerFactory.getInstance().getInsstitutionController();
            emissionController = GHGControllerFactory.getInstance().getEmissionController();
            businessAirTravelController = GHGControllerFactory.getInstance().getBusinessAirTravelController();
            electricityController = GHGControllerFactory.getInstance().getElectricityController();
            fireExtingController = GHGControllerFactory.getInstance().getFireExtingController();
            generatorsController = GHGControllerFactory.getInstance().getGeneratorsController();
            municipalWaterController = GHGControllerFactory.getInstance().getMunicipalWaterController();
            refrigerantsController = GHGControllerFactory.getInstance().getRefrigerantsController();
            transportLocalController = GHGControllerFactory.getInstance().getTransportLocalController();
            vehicleController = GHGControllerFactory.getInstance().getVehicleController();
            wasteDisposalController = GHGControllerFactory.getInstance().getWasteDisposalController();
            wasteTransportController = GHGControllerFactory.getInstance().getWasteTransportController();
            employeeCommutingController = GHGControllerFactory.getInstance().getEmpCommutingController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            seaAirFreightController = GHGControllerFactory.getInstance().getSeaAirFreightController();
            bioMassEntryController = GHGControllerFactory.getInstance().getBioMassController();
            lpgEntryController = GHGControllerFactory.getInstance().getLPGEntryController();
            bgasEntryController = GHGControllerFactory.getInstance().getBGASEntryController();

            ashTransportationController = GHGControllerFactory.getInstance().getAshTransportationController();
            forkliftsController = GHGControllerFactory.getInstance().getForkliftsController();
            furnaceController = GHGControllerFactory.getInstance().getFurnaceController();
            lorryTransportationController = GHGControllerFactory.getInstance().getLorryTransportationController();
            oilAndGasTransportationController = GHGControllerFactory.getInstance().getOilAndGasTransportationController();
            paperWasteController = GHGControllerFactory.getInstance().getPaperWasteController();
            paidManagerVehicleController = GHGControllerFactory.getInstance().getPaidManagerVehicleController();
            rawMaterialTransportController = GHGControllerFactory.getInstance().getRawMaterialTransportController();
            sawDustTransportationController = GHGControllerFactory.getInstance().getSawDustTransportationController();
            sludgeTransportationController = GHGControllerFactory.getInstance().getSludgeTransportationController();
            staffTransportationController = GHGControllerFactory.getInstance().getStaffTransportationController();
            vehicleOthersController = GHGControllerFactory.getInstance().getVehicleOthersController();
        } catch (GHGException e) {
            logger.error("Error in getting instance of emission controllers >>", e);
        }
    }


    public VehicleOthersEntryListResMsg getVehicleOthersEntryList(VehicleOthersEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<VehicleOthers> entryList = null;
        VehicleOthersEntryListResMsg res = new VehicleOthersEntryListResMsg();
        entryList = vehicleOthersController.getFiteredReslt(pageNumber, sorting, filter);
        List<VehicleOthersDTO> list = new ArrayList<VehicleOthersDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                VehicleOthersDTO dto = VehicleOthersPopulator.getInstance().populateDTO((VehicleOthers) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }



    public AshTransportationEntryListResMsg getAshTransportationEntryList(AshTransportationEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<AshTransportation> entryList = null;
        AshTransportationEntryListResMsg res = new AshTransportationEntryListResMsg();
        entryList = ashTransportationController.getFiteredReslt(pageNumber, sorting, filter);
        List<AshTransportationDTO> list = new ArrayList<AshTransportationDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                AshTransportationDTO dto = AshTransportationPopulator.getInstance().populateDTO((AshTransportation) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public ForkliftsEntryListResMsg getForkliftsEntryList(ForkliftEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<Forklifts> entryList = null;
        ForkliftsEntryListResMsg res = new ForkliftsEntryListResMsg();
        entryList = forkliftsController.getFiteredReslt(pageNumber, sorting, filter);
        List<ForkliftsEntryDTO> list = new ArrayList<ForkliftsEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                ForkliftsEntryDTO dto = ForkliftsPopulator.getInstance().populateDTO((Forklifts) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public FurnaceOilEntryListResMsg getFurnaceOilEntryList(FurnaceEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<FurnaceOil> entryList = null;
        FurnaceOilEntryListResMsg res = new FurnaceOilEntryListResMsg();
        entryList = furnaceController.getFiteredReslt(pageNumber, sorting, filter);
        List<FurnaceOilDTO> list = new ArrayList<FurnaceOilDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                FurnaceOilDTO dto = FurnaceOilPopulator.getInstance().populateDTO((FurnaceOil) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public LorryTransportationEntryListResMsg getLorryTransportationEntryList(LorryTransportationEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<LorryTransportation> entryList = null;
        LorryTransportationEntryListResMsg res = new LorryTransportationEntryListResMsg();
        entryList = lorryTransportationController.getFiteredReslt(pageNumber, sorting, filter);
        List<LorryTransportationDTO> list = new ArrayList<LorryTransportationDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                LorryTransportationDTO dto = LorryTransportationPopulator.getInstance().populateDTO((LorryTransportation) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public OilAndGasTransportationEntryListResMsg getOilAndGasTransportationEntryList(OilAndGasEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<OilAndGasTransportation> entryList = null;
        OilAndGasTransportationEntryListResMsg res = new OilAndGasTransportationEntryListResMsg();
        entryList = oilAndGasTransportationController.getFiteredReslt(pageNumber, sorting, filter);
        List<OilAndGasDTO> list = new ArrayList<OilAndGasDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                OilAndGasDTO dto =OilAndGasTransportationPopulator.getInstance().populateDTO((OilAndGasTransportation) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public PaperWasteEntryListResMsg getPaperWasteEntryList(PaperWasteEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<PaperWaste> entryList = null;
        PaperWasteEntryListResMsg res = new PaperWasteEntryListResMsg();
        entryList = paperWasteController.getFiteredReslt(pageNumber, sorting, filter);
        List<PaperWasteDTO> list = new ArrayList<PaperWasteDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                PaperWasteDTO dto = PaperWastePopulator.getInstance().populateDTO((PaperWaste) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public PaidManagerVehicleEntryListResMsg getPaidManagerVehicleEntryList(PaidManagerVehicleEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<PaidManagerVehicles> entryList = null;
        PaidManagerVehicleEntryListResMsg res = new PaidManagerVehicleEntryListResMsg();
        entryList = paidManagerVehicleController.getFiteredReslt(pageNumber, sorting, filter);
        List<PaidManagerVehicleDTO> list = new ArrayList<PaidManagerVehicleDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                PaidManagerVehicleDTO dto = PaidManagerVehiclePopulator.getInstance().populateDTO((PaidManagerVehicles) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public RawMaterialTransportationEntryListResMsg getRawMaterialTransportationEntryList(RawMaterialTransportationEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<RawMaterialTransporationLocal> entryList = null;
        RawMaterialTransportationEntryListResMsg res = new RawMaterialTransportationEntryListResMsg();
        entryList = rawMaterialTransportController.getFiteredReslt(pageNumber, sorting, filter);
        List<RawMaterialTransportationDTO> list = new ArrayList<RawMaterialTransportationDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                RawMaterialTransportationDTO dto = RawMaterialTransportationPopulator.getInstance().populateDTO((RawMaterialTransporationLocal) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public SawDustTransportationEntityListResMsg getSawDustTransportationEntryList(SawDustTransportationEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<SawDustTransportation> entryList = null;
        SawDustTransportationEntityListResMsg res = new SawDustTransportationEntityListResMsg();
        entryList = sawDustTransportationController.getFiteredReslt(pageNumber, sorting, filter);
        List<SawDustTransportationDTO> list = new ArrayList<SawDustTransportationDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                SawDustTransportationDTO dto = SawDustTransportationPopulator.getInstance().populateDTO((SawDustTransportationBean) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public StaffTransportationEntryListResMsg getStaffTransportationEntryList(StaffTransportationEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<StaffTransportation> entryList = null;
        StaffTransportationEntryListResMsg res = new StaffTransportationEntryListResMsg();
        entryList = staffTransportationController.getFiteredReslt(pageNumber, sorting, filter);
        List<StaffTransportationDTO> list = new ArrayList<StaffTransportationDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                StaffTransportationDTO dto = StaffTransportationPopulator.getInstance().populateDTO((StaffTransportationBean) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }


    public SludgeTransportationEntryListResMsg getSludgeTransportationEntryList(SludgeTransportationEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<SludgeTransportation> entryList = null;
        SludgeTransportationEntryListResMsg res = new SludgeTransportationEntryListResMsg();
        entryList = sludgeTransportationController.getFiteredReslt(pageNumber, sorting, filter);
        List<SludgeTransportationDTO> list = new ArrayList<SludgeTransportationDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                SludgeTransportationDTO dto = SludgeTransportationPopulator.getInstance().populateDTO((SludeTransportationBean) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

//    public StaffTransportationEntryListResMsg getStaffTransportationEntryList(BioMassEntryListReqMsg msg, int user ) {
//        logger.info("Message : " + msg);
//
////        Todo:   integrate filter , sorting, pagenumber
//        Object filter = msg.getFilterModel();
//        Object sorting = msg.getSortModel();
//        int pageNumber = msg.getPageNumber();
//        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);
//
//        ListResult<StaffTransportation> entryList = null;
//        StaffTransportationEntryListResMsg res = new StaffTransportationEntryListResMsg();
//        entryList = staffTransportationController.getFiteredReslt(pageNumber, sorting, filter);
//        List<BioMassEntryDTO> list = new ArrayList<BioMassEntryDTO>();
//
//        if (entryList.getList() == null) {
//            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
//        } else {
//            logger.info("Fetched count: " + entryList.getList().size());
//            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
//            for (Object o : entryList.getList()) {
//                BioMassEntryDTO dto = BioMassPopulator.getInstance().populateDTO((BioMassEntry) o);
//                list.add(dto);
//            }
//        }
//        res.setList(list);
//        res.setTotalRecordCount(entryList.getTotalCount());
//        return res;
//    }




    public BioMassEntryListResMsg getBioMassEntryList(BioMassEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<BioMassEntry> entryList = null;
        BioMassEntryListResMsg res = new BioMassEntryListResMsg();
        entryList = bioMassEntryController.getFiteredReslt(pageNumber, sorting, filter);
        List<BioMassEntryDTO> list = new ArrayList<BioMassEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                BioMassEntryDTO dto = BioMassPopulator.getInstance().populateDTO((BioMassEntry) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public LPGEntryListResMsg getLPGEntryList(LPGEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<LPGEntry> entryList = null;
        LPGEntryListResMsg res = new LPGEntryListResMsg();
        entryList = lpgEntryController.getFiteredReslt(pageNumber, sorting, filter);
        List<LPGEntryDTO> list = new ArrayList<LPGEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                LPGEntryDTO dto = LPGEntryPopulator.getInstance().populateDTO((LPGEntry) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    //-----bgas-----
    public BGASEntryListResMsg getBGASEntryList(BGASEntryListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<BGASEntry> entryList = null;
        BGASEntryListResMsg res = new BGASEntryListResMsg();
        entryList = bgasEntryController.getFiteredReslt(pageNumber, sorting, filter);
        List<BGASEntryDTO> list = new ArrayList<BGASEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                BGASEntryDTO dto = BGASEntryPopulator.getInstance().populateDTO((BGASEntry) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }


    public SeaAirFreightListResMsg getSeaAirFreightEntryList(SeaAirFreightListReqMsg msg, int user ) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<SeaAirFreightEntry> entryList = null;
        SeaAirFreightListResMsg res = new SeaAirFreightListResMsg();
        entryList = seaAirFreightController.getFiteredReslt(pageNumber, sorting, filter);
        List<SeaAirFreightDTO> list = new ArrayList<SeaAirFreightDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                SeaAirFreightDTO dto = SeaAirFrieghtPopulator.getInstance().populateDTO((SeaAirFreightEntry) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }


    public AirTravelEntryListResponeMsg getAirTravelEntryList(BusinessAirTravelEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<BusinessAirTravelEntry> entryList = null;
        AirTravelEntryListResponeMsg res = new AirTravelEntryListResponeMsg();
        entryList = businessAirTravelController.getFiteredReslt(pageNumber, sorting, filter);
        List<BusinessAirTravelEntryDTO> list = new ArrayList<BusinessAirTravelEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : entryList.getList()) {
                BusinessAirTravelEntryDTO dto = AirTravelPopulator.getInstance().populateDTO((BusinessAirTravelEntry) o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;


    }

    public ElectricityEntryListResponeMsg getElectricityEntryList(ElectricityEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<ElectricityEntry> entryList = null;
        ElectricityEntryListResponeMsg res = new ElectricityEntryListResponeMsg();
        entryList = electricityController.getFiteredReslt(pageNumber, sorting, filter);
        List<ElectricityEntryDTO> list = new ArrayList<ElectricityEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (ElectricityEntry o : entryList.getList()) {
                ElectricityEntryDTO dto = ElectricityPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public FireExtingEntryListResponeMsg getFireExtEntryList(FireExtingEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<FireExtingEntry> entryList = null;
        FireExtingEntryListResponeMsg res = new FireExtingEntryListResponeMsg();
        entryList = fireExtingController.getFiteredReslt(pageNumber, sorting, filter);
        List<FireExtingEntryDTO> list = new ArrayList<FireExtingEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (FireExtingEntry o : entryList.getList()) {
                FireExtingEntryDTO dto = FireExtingPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public GeneratorsEntryListResponeMsg getGeneratorsEntryList(GeneratorsEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<GeneratorsEntry> entryList = null;
        GeneratorsEntryListResponeMsg res = new GeneratorsEntryListResponeMsg();
        entryList = generatorsController.getFiteredReslt(pageNumber, sorting, filter);
        List<GeneratorsEntryDTO> list = new ArrayList<GeneratorsEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (GeneratorsEntry o : entryList.getList()) {
                GeneratorsEntryDTO dto = GeneratorsPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public MunicipalWaterEntryListResponeMsg getMunWaterEntryList(MunicipalWaterEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<MunicipalWaterEntry> entryList = null;
        MunicipalWaterEntryListResponeMsg res = new MunicipalWaterEntryListResponeMsg();
        entryList = municipalWaterController.getFiteredReslt(pageNumber, sorting, filter);
        List<MunicipalWaterEntryDTO> list = new ArrayList<MunicipalWaterEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (MunicipalWaterEntry o : entryList.getList()) {
                MunicipalWaterEntryDTO dto = MunicipalWaterPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public RefrigerantsEntryListResponeMsg getRefriEntryList(RefrigerantsEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<RefrigerantsEntry> entryList = null;
        RefrigerantsEntryListResponeMsg res = new RefrigerantsEntryListResponeMsg();
        entryList = refrigerantsController.getFiteredReslt(pageNumber, sorting, filter);
        List<RefrigerantsEntryDTO> list = new ArrayList<RefrigerantsEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (RefrigerantsEntry o : entryList.getList()) {
                RefrigerantsEntryDTO dto = RefrigerantsPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public TransportLocalEntryListResponeMsg getTransLocalEntryList(TransportLocalEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<TransportLocalEntry> entryList = null;
        TransportLocalEntryListResponeMsg res = new TransportLocalEntryListResponeMsg();
        entryList = transportLocalController.getFiteredReslt(pageNumber, sorting, filter);
        List<TransportLocalEntryDTO> list = new ArrayList<TransportLocalEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (TransportLocalEntry o : entryList.getList()) {
                TransportLocalEntryDTO dto = TransportLocalPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public VehicleEntryListResponeMsg getVehicleEntryList(VehicleEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<VehicleEntry> entryList = null;
        VehicleEntryListResponeMsg res = new VehicleEntryListResponeMsg();
        entryList = vehicleController.getFiteredReslt(pageNumber, sorting, filter);
        List<VehicleEntryDTO> list = new ArrayList<VehicleEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (VehicleEntry o : entryList.getList()) {
                VehicleEntryDTO dto = VehiclePopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public WasteDisposalEntryListResponeMsg getWasteDisposalEntryList(WasteDisposalEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<WasteDisposalEntry> entryList = null;
        WasteDisposalEntryListResponeMsg res = new WasteDisposalEntryListResponeMsg();
        entryList = wasteDisposalController.getFiteredReslt(pageNumber, sorting, filter);
        List<WasteDisposalEntryDTO> list = new ArrayList<WasteDisposalEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (WasteDisposalEntry o : entryList.getList()) {
                WasteDisposalEntryDTO dto = WasteDisposalPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public WasteTransportEntryListResponeMsg getWasteTransportEntryList(WasteTransportEntryListRequestMsg msg, int user) {
        logger.info("Message : " + msg);


//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<WasteTransportEntry> entryList = null;
        WasteTransportEntryListResponeMsg res = new WasteTransportEntryListResponeMsg();
        entryList = wasteTransportController.getFiteredReslt(pageNumber, sorting, filter);
        List<WasteTransportEntryDTO> list = new ArrayList<WasteTransportEntryDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (WasteTransportEntry o : entryList.getList()) {
                WasteTransportEntryDTO dto = WasteTransportPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public EmpCommutingListResMsg getEmpCommutingList(EmpCommutingListReqMsg msg, int user) {
        logger.info("Message : " + msg);
//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<EmpCommutingEntry> entryList = null;
        EmpCommutingListResMsg res = new EmpCommutingListResMsg();
        entryList = employeeCommutingController.getFilteredResult(pageNumber, sorting, filter);
        List<EmpCommutingDTO> list = new ArrayList<EmpCommutingDTO>();

        if (entryList.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        } else {
            logger.info("Fetched count: " + entryList.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (EmpCommutingEntry o : entryList.getList()) {
                EmpCommutingDTO dto = EmployeeCommutingPopulator.getInstance().populateDTO(o);
                list.add(dto);
            }
        }
        res.setList(list);
        res.setTotalRecordCount(entryList.getTotalCount());
        return res;
    }

    public AirTravelMangedResponse manageAirTravelEntry(BusinessAirTravelEntryManagedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        BusinessAirTravelEntry entry;
        AirTravelMangedResponse res = new AirTravelMangedResponse();
        try {
            entry = businessAirTravelController.editWasteTransportEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Business Airtravel enrtry "+  (msg.getDto().getAirTravelEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getAirTravelEntryId());
                res.setDto(AirTravelPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage AirTravelEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public ElectricityEntryMangedResponseMsg manageElectricityEntry(ElectricityEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        ElectricityEntry entry;
        ElectricityEntryMangedResponseMsg res = new ElectricityEntryMangedResponseMsg();
        try {
            entry = electricityController.editElectricityEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Electricity enrtry "+  (msg.getDto().getEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(ElectricityPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage ElectricityEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public FireExtingEntryMangedResponseMsg manageFireExtEntry(FireExtingEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        FireExtingEntry entry;
        FireExtingEntryMangedResponseMsg res = new FireExtingEntryMangedResponseMsg();
        try {
            entry = fireExtingController.editFireExtingEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Fire Extinguisher  enrtry "+  (msg.getDto().getFireExtEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getFireExtEntryId());
                res.setDto(FireExtingPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage FireExtingEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public GeneratorsEntryMangedResponseMsg manageGeneratorsEntry(GeneratorsEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        GeneratorsEntry entry;
        GeneratorsEntryMangedResponseMsg res = new GeneratorsEntryMangedResponseMsg();
        try {
            entry = generatorsController.editGeneratorsEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Generator enrtry "+  (msg.getDto().getEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(GeneratorsPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage GeneratorsEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public MunicipalWaterEntryMangedResponseMsg manageMunWaterEntry(MunicipalWaterEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        MunicipalWaterEntry entry;
        MunicipalWaterEntryMangedResponseMsg res = new MunicipalWaterEntryMangedResponseMsg();
        try {
            entry = municipalWaterController.editMunicipalWaterEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Municipal Water enrtry "+  (msg.getDto().getEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(MunicipalWaterPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage MunicipalWaterEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public RefrigerantsEntryMangedResponseMsg manageRefriEntry(RefrigerantsEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        RefrigerantsEntry entry;
        RefrigerantsEntryMangedResponseMsg res = new RefrigerantsEntryMangedResponseMsg();
        try {
            entry = refrigerantsController.editRefrigerantsEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Refrigerants Leakage enrtry "+  (msg.getDto().getRefrigerantsEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getRefrigerantsEntryId());
                res.setDto(RefrigerantsPopulator.getInstance().populateDTO(entry));
                res.getDto().setRefrigerantsEntryId((Integer) res.getEntityKey());
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage RefrigerantsEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public TransportLocalEntryMangedResponseMsg manageTransLocalEntry(TransportLocalEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        TransportLocalEntry entry;
        TransportLocalEntryMangedResponseMsg res = new TransportLocalEntryMangedResponseMsg();
        try {
            entry = transportLocalController.editTransportLocalEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Tranport Locally purchased enrtry "+  (msg.getDto().getEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(TransportLocalPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage TransportLocalEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public VehicleEntryMangedResponseMsg manageVehicleEntry(VehicleEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        VehicleEntry entry;
        VehicleEntryMangedResponseMsg res = new VehicleEntryMangedResponseMsg();
        try {
            entry = vehicleController.editVehicleEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Vehicles enrtry "+  (msg.getDto().getEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(VehiclePopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage VehicleEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public WasteDisposalEntryMangedResponseMsg manageWasteDisEntry(WasteDisposalEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        WasteDisposalEntry entry;
        WasteDisposalEntryMangedResponseMsg res = new WasteDisposalEntryMangedResponseMsg();
        try {
            entry = wasteDisposalController.editWasteDisposalEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Waste Disposal enrtry "+  (msg.getDto().getEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(WasteDisposalPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage WasteDisposalEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public WasteTransportEntryMangedResponseMsg manageWasteTransEntry(WasteTransportEntryMangedRequestMsg msg, int user) {
        logger.info("Message : " + msg);
        WasteTransportEntry entry;
        WasteTransportEntryMangedResponseMsg res = new WasteTransportEntryMangedResponseMsg();
        try {
            entry = wasteTransportController.editWasteTransportEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Waste Transport  enrtry "+  (msg.getDto().getEntryId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(WasteTransportPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage WasteTransportEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }


    public LPGEntryManagedResMsg manageLPGEntry(LPGEntryManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        LPGEntry entry;
        LPGEntryManagedResMsg res = new LPGEntryManagedResMsg();
        try {
            entry = lpgEntryController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
//                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(LPGEntryPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    //bgas mange---
    public BGASEntryManagedResMsg manageBGASEntry(BGASEntryManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        BGASEntry entry;
        BGASEntryManagedResMsg res = new BGASEntryManagedResMsg();
        try {
            entry = bgasEntryController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
//                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(BGASEntryPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }


    public BioMassEntryManagedResMsg manageBioMassEntry(BioMassEntryManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        BioMassEntry entry;
        BioMassEntryManagedResMsg res = new BioMassEntryManagedResMsg();
        try {
            entry = bioMassEntryController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(BioMassPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public SeaFrieghtManagedResMsg manageSeaAirFreightEntry(SeaAirFreightMangedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        SeaAirFreightEntry entry;
        SeaFrieghtManagedResMsg res = new SeaFrieghtManagedResMsg();
        try {
            entry = seaAirFreightController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(SeaAirFrieghtPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }


    public AshTransportationManagedResMsg manageAshTransportationEntry(AshTransportationManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        AshTransportation entry;
        AshTransportationManagedResMsg res = new AshTransportationManagedResMsg();
        try {
            entry = ashTransportationController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(AshTransportationPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public ForkliftsManagedResMsg manageForkliftsEntry(ForkliftsManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        Forklifts entry;
        ForkliftsManagedResMsg res = new ForkliftsManagedResMsg();
        try {
            entry = forkliftsController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(ForkliftsPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public FurnaceManagedResMsg manageFurnaceEntry(FurnaceManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        FurnaceOil entry;
        FurnaceManagedResMsg res = new FurnaceManagedResMsg();
        try {
            entry = furnaceController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(FurnaceOilPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public OilAndGasTransportationManagedResMsg manageOilAndGasOiEntry(OilAndGasTransportationManagedReqMsg msg, int user) {

        OilAndGasTransportation entry = null;
        OilAndGasTransportationManagedResMsg res = new OilAndGasTransportationManagedResMsg();
        try {




            entry = oilAndGasTransportationController.edit(msg.getDto());

            if (entry != null) {
                //  ----------------log--------------------------------------------------

                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
//                res.setEntityKey(entry.getEntryId());
//                res.setDto(OilAndGasTransportationPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }


    public LorryTransportationManagedResMsg manageLorryTransportationEntry(LorryTransportationManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        LorryTransportation entry;
        LorryTransportationManagedResMsg res = new LorryTransportationManagedResMsg();
        try {
            entry = lorryTransportationController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(LorryTransportationPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public PaperWasteManagedResMsg managePaperWasteEntry(PaperWasteManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        PaperWaste entry;
        PaperWasteManagedResMsg res = new PaperWasteManagedResMsg();
        try {
            entry = paperWasteController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(PaperWastePopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public PaidManagerVehicleManagedResMsg managePaidManagerVehicleEntry(PaidManagerVehicleManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        PaidManagerVehicles entry;
        PaidManagerVehicleManagedResMsg res = new PaidManagerVehicleManagedResMsg();
        try {
            entry = paidManagerVehicleController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(PaidManagerVehiclePopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public RawMaterialTransportationManagedResMsg manageRawMaterialTransportationEntry(RawMaterialTransportationManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        RawMaterialTransporationLocal entry;
        RawMaterialTransportationManagedResMsg res = new RawMaterialTransportationManagedResMsg();
        try {
            entry = rawMaterialTransportController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(RawMaterialTransportationPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public SawDustTransportationManagedResMsg manageSawDustTransportationEntry(SawDustTransportationManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        SawDustTransportation entry;
        SawDustTransportationManagedResMsg res = new SawDustTransportationManagedResMsg();
        try {
            entry = sawDustTransportationController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(SawDustTransportationPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public StaffTransportationManagedResMsg manageStaffTransportationEntry(StaffTransportationManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        StaffTransportation entry;
        StaffTransportationManagedResMsg res = new StaffTransportationManagedResMsg();
        try {
            entry = staffTransportationController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(StaffTransportationPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage Staff Trans", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public SludgeTransportationManagedResMsg manageSludgeTransportationEntry(SludgeTransportationManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        SludgeTransportation entry;
        SludgeTransportationManagedResMsg res = new SludgeTransportationManagedResMsg();
        try {
            entry = sludgeTransportationController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(SludgeTransportationPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }


    public VehicleOthersManagedResMsg manageVehicleOthersEntry(VehicleOthersManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        VehicleOthers entry;
        VehicleOthersManagedResMsg res = new VehicleOthersManagedResMsg();
        try {
            entry = vehicleOthersController.edit(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
//                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getEntryId());
                res.setDto(VehicleOthersPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage SeaAirFreightEntry", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }



//    public StaffTransportationManagedResMsg manageStaffTransportionEntry(StaffTransportationManagedReqMsg msg, int user) {
//        logger.info("Message : " + msg);
//        BioMassEntry entry;
//        BioMassEntryManagedResMsg res = new BioMassEntryManagedResMsg();
//        try {
//            entry = bioMassEntryController.edit(msg.getDto());
//            if (entry != null) {
//                //  ----------------log--------------------------------------------------
//                UserActivityLogBean log = null;
//                try {
//
//                    UserLogin userLogin =  userLoginController.findById(user);
//                    if (userLogin != null) {
//                        log = new UserActivityLogBean();
//                        log.setUserId(user);
//                        log.setDeleted(0);
//                        JsonObject filter = new JsonObject();
//                        JsonObject loginFilter = new JsonObject();
//                        loginFilter.addProperty("value", user);
//                        loginFilter.addProperty("col", 1);
//                        loginFilter.addProperty("type",1);
//                        filter.add("userId", loginFilter);
//
//                        if (userLogin.getUserType() == 1) {
//                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(result.getList().get(0).getBranchId());
//                                log.setCompanyId(result.getList().get(0).getCompanyId());
//                            }
//                        }
//                        if (userLogin.getUserType() == 2) {
//                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(-1);
//                                log.setCompanyId(result.getList().get(0).getCompanyId());
//                            }
//                        }
//                        if (userLogin.getUserType() == 3) {
//                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(-1);
//                                log.setCompanyId(-1);
//                            }
//                        }
//                    }
//                } catch (GHGException e) {
//                    logger.error(e);
//                }
//                if (log != null) {
////                    log.setActLog( "Sea Air Freight  enrtry "+  (msg.getDto().getId()) <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
//                    userActController.addActLog(log);
//                }
//                //------------------------log-----------------------------------------
//                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
//                res.setEntityKey(entry.getEntryId());
//                res.setDto(BioMassPopulator.getInstance().populateDTO(entry));
//            } else {
//                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
//                res.setNarration("Object not found in db");
//            }
//
//
//        } catch (GHGException e) {
//            logger.error("Failed in manage SeaAirFreightEntry", e);
//            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
//            res.setNarration(e.getMessage());
//        }
//
//        return res;
//    }







    public EmpCommutingManageResMsg manageEmpCommuting(EmpCommutingManagedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        EmpCommutingEntry entry;
        EmpCommutingManageResMsg res = new EmpCommutingManageResMsg();
        try {
            entry = employeeCommutingController.editEmpCommutingEntry(msg.getDto());
            if (entry != null) {
                //  ----------------log--------------------------------------------------
                UserActivityLogBean log = null;
                try {

                    UserLogin userLogin =  userLoginController.findById(user);
                    if (userLogin != null) {
                        log = new UserActivityLogBean();
                        log.setUserId(user);
                        log.setDeleted(0);
                        JsonObject filter = new JsonObject();
                        JsonObject loginFilter = new JsonObject();
                        loginFilter.addProperty("value", user);
                        loginFilter.addProperty("col", 1);
                        loginFilter.addProperty("type",1);
                        filter.add("userId", loginFilter);

                        if (userLogin.getUserType() == 1) {
                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(result.getList().get(0).getBranchId());
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 2) {
                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(result.getList().get(0).getCompanyId());
                            }
                        }
                        if (userLogin.getUserType() == 3) {
                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                            if (result.getList().size() == 1) {
                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                                log.setBranchId(-1);
                                log.setCompanyId(-1);
                            }
                        }
                    }
                } catch (GHGException e) {
                    logger.error(e);
                }
                if (log != null) {
                    log.setActLog( "Employee Commuting enrtry "+  (msg.getDto().getId() <=0 ? "added " : "updated ") + " by" + log.getUsername()) ;
                    userActController.addActLog(log);
                }
                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(EmployeeCommutingPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage EmpCommutingManageResMsg", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getGHGEmissionSummary_excluded(GHHEmissionSummaryExcluedReqMsg msg, int user) {
        logger.info("Message : " + msg);
        List<GHGEmissionSummaryBean> list;
        GHGEmissionSummaryResMsg res = new GHGEmissionSummaryResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();

        try {
            List<GHGEmissionSummaryDTO> dtos = new ArrayList<>();
            //current
            HashMap<String, String> current = emissionController.emission_tco2_excluded(
                    msg.getCompanyId(),
                    msg.getBranchId(),
                    yStart, yEnd,
                    msg.getParams());
            String fy_current = (yStart != yEnd) ? ("" + (yStart) + "/" + ((yEnd) % 100) + "") : ("" + (yStart) + "");
            //previous years
            GHGEmissionSummaryDTO currentDTO = GHGSummaryPopulator.getInstance().fromHashMap(msg.getCompanyId(),fy_current, msg.getBranchId(), current);
            currentDTO.setFy(fy_current);
            dtos.add(currentDTO);
            //show only 4 years  back
            for (int y = 1; y <= 4; y++) {
                String fy = (yEnd != yStart) ? ("" + (yStart - y) + "/" + ((yEnd - y) % 100) + "") : ("" + (yStart - y) + "");
                GHGEmsissionSummary summary = ghgEmissionSummaryController.find(msg.getCompanyId(), EmissionSummaryModeofEntry.Manual.mode, fy);
                if (summary != null) {
                    dtos.add(GHGSummaryPopulator.getInstance().populateDTO((GHGEmissionSummaryBean) summary));
                }
            }
            res.setList(dtos);

        } catch (Exception e) {
            logger.error("Failed in manage GHGEmissionSummaryBean", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getGHGEmissionSummary(GHGEmissionSummaryReqMsg msg, int user) {
        logger.info("Message : " + msg);
        List<GHGEmissionSummaryBean> list;
        GHGEmissionSummaryResMsg res = new GHGEmissionSummaryResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();

        String fy = yStart == yEnd ? "" + yStart : "" + yStart + "/" + (yEnd % 100);

        try {
            List<GHGEmissionSummaryDTO> dtos = new ArrayList<>();
            //current
            HashMap<String, String> current = emissionController.emission_tco2(msg.getCompanyId(), msg.getBranchId(), yStart, yEnd);
            if (msg.getYearsBack() == -1) {
                JsonObject comFilter = new JsonObject();
                JsonObject comFilterObj = new JsonObject();
                comFilterObj.addProperty("value", msg.getCompanyId());
                comFilterObj.addProperty("type",1);
                comFilterObj.addProperty("col",1);
                comFilter.add("companyId", comFilterObj);
                List<GHGEmsissionSummary> ghgEmsissionSummaries = ghgEmissionSummaryController.getPaginatedGHGEmissonSummary(-1, "", comFilter);
                for(GHGEmsissionSummary summary : ghgEmsissionSummaries) {
                    //Todo: change 2nd argument
                    dtos.add(GHGSummaryPopulator.getInstance().fromHashMap(msg.getCompanyId(),fy, summary.getModeOfEntry() ,summary.getEmission()));

                }
                res.setList(dtos);
                return res;
            }
            //previous years
            //todo: changing msg.branchid => -1
            GHGEmissionSummaryDTO currentDTO = GHGSummaryPopulator.getInstance().fromHashMap(msg.getCompanyId(),fy, -1, current);
            currentDTO.setFy(company.isFinacialYear() ? ("" + (yStart) + "/" + ((yEnd) % 100) + "") : ("" + (yStart) + ""));
            dtos.add(currentDTO);
            for (int y = 1; y <= msg.getYearsBack(); y++) {
                String fy_prev = company.isFinacialYear() ? ("" + (yStart - y) + "/" + ((yEnd - y ) % 100) + "") : ("" + (yStart - y) + "");
                GHGEmsissionSummary summary = ghgEmissionSummaryController.findById(msg.getCompanyId(), -1,fy_prev);
                if (summary != null) {
                    dtos.add(GHGSummaryPopulator.getInstance().fromHashMap(msg.getCompanyId(), summary.getId().getFY(), summary.getModeOfEntry(), summary.getEmission()));
                }
//                HashMap<String, String> summary = emissionController.emission_tco2(msg.getCompanyId(), msg.getBranchId(), yStart-y, yEnd-y);
//                if (summary != null) {
//                    GHGEmissionSummaryDTO summaryDto = GHGSummaryPopulator.getInstance().fromHashMap(msg.getCompanyId(), msg.getBranchId(), summary);
//                    dtos.add(summaryDto);
//                }
            }
            res.setList(dtos);

        } catch (Exception e) {
            logger.error("Failed in manage GHGEmissionSummaryBean", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message getGHGEmissionSummaryBranchWise(GHGEmissionSummaryReqMsg msg, int user) {

        logger.info("Message : " + msg);
        List<GHGEmissionSummaryBean> list;
        GHGEmissionSummaryResMsg res = new GHGEmissionSummaryResMsg();
        Company company = insstitutionController.findCompanyById(msg.getCompanyId());
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();

        try {
            String fy_current = company.isFinacialYear() ? ("" + (yStart) + "/" + ((yEnd) % 100) + "") : ("" + (yStart) + "");
            List<GHGEmissionSummaryDTO> dtos = new ArrayList<>();
            HashMap<String, String> map = emissionController.emissionBranchWise(msg.getCompanyId(), yStart, yEnd);
            GHGEmissionSummaryDTO currentDTO = GHGSummaryPopulator.getInstance().fromHashMap(msg.getCompanyId(),fy_current, msg.getBranchId(), map);
            currentDTO.setFy(fy_current);
            dtos.add(currentDTO);
            res.setList(dtos);
        } catch (Exception e) {
            logger.error("Error in getting summary", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }
        return res;
    }

    public Message manageGHGEmissionSummary(ManageGHGEmissionSummaryReqMsg msg, int user) {
        ManageGHGEmissionSummaryResMsg res = new ManageGHGEmissionSummaryResMsg();

        String fy = msg.getDto().getFy();
        int comId = msg.getDto().getCompanyId();
        int mode = msg.getDto().getMode();


        if (msg.getDto().getIsDeleted() == 1) {
            if(ghgEmissionSummaryController.delete(comId, fy) > 0) {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
//            res.setEntityKey();
                res.setDto(msg.getDto());
                return res;
            }
        }

        GHGEmissionSummaryBean summaryBean = new GHGEmissionSummaryBean();
        GHGEmissionSummaryIdBean idBean = new GHGEmissionSummaryIdBean();
        idBean.setCompanyId(comId);
        idBean.setFY(fy);
        summaryBean.setId(idBean);
        summaryBean.setAddedDate(new Date());
        summaryBean.setModeOfEntry(mode);

        HashMap<String, String> emissionMap = new HashMap<>();
        if (msg.getDto().getEmissionInfo() != null) {
            for(Map.Entry<String, JsonElement> entry : msg.getDto().getEmissionInfo().entrySet()){
                emissionMap.put(entry.getKey(), msg.getDto().getEmissionInfo().get(entry.getKey()).getAsString());
            }
            summaryBean.setEmission(emissionMap);
            GHGEmsissionSummary  resBean =  ghgEmissionSummaryController.edit(summaryBean);
            if (resBean != null) {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
//            res.setEntityKey();
                res.setDto(msg.getDto());
                return  res;
            }else {
                logger.info("Error in persisting GHG Emission summary");
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Error in persisting GHG Emission summary");
                return res;
            }

        }else {
            logger.info("The request doesn't contains  emissionInfo");
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration("The request doesn't contains  emissionInfo");
            return res;
        }

    }

    public Message exportGHGEmissionSummary(ExportGHGEmissionSummaryReqMsg message) {
        //get all emissions and convert to the format

        // create a excel file
        //upload to s3
        // responsoe link
        ExportGHGEmissionSummaryResMsg res =  new ExportGHGEmissionSummaryResMsg();
        try {
            Company company = insstitutionController.findCompanyById(message.getComId());
            List<HashMap<String,String>> actData = getActivityData(message.getComId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            List<GHGEmissionSummaryDTO> dtos = getEmissionSummary(message.getComId());
            List<HashMap<String, String>> summaryData = getSummaryMapList(dtos);
            List<HashMap<String, String>> directData = getDirectEmissionListMap(dtos);
            List<HashMap<String, String>> indirectData = getIndirectEmissionListMap(dtos);
            HashMap<String, List<List<String>>> catMap = getCatDataMap(company);
            List<String> years = new ArrayList<>();
            for (GHGEmissionSummaryDTO dto : dtos) {
                years.add(dto.getFy());
            }
            String companyName = company.getName();
            String currentYear =  (company.getFyCurrentEnd() == company.getFyCurrentStart()) ? "" + company.getFyCurrentStart() : company.getFyCurrentStart() + "/" + (company.getFyCurrentEnd() % 100);

            String url = ghgEmissionSummaryController.getExportedGHGEmissionSummary(
                    companyName,
                    currentYear,
                    years,
                    actData,
                    directData,
                    indirectData,
                    summaryData,
                    catMap
            );
            res.setComId(company.getId());
            res.setFileUrl(url);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);

        }catch (Exception e) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration("Error in Exporting Excel Summary");
            logger.error(e);
        }
        return res;
    }

    private HashMap<String,List<List<String>>> getCatDataMap(Company company) {
        List<EmissionCategoryBean> categories = company.getEmissionCategories();
        Map<Integer, AllowedEmissionSrcBean> allowedEmissionSrcBeanMap = company.getAllowedEmissionSources();

        HashMap<String, List<List<String>>> catMap = new HashMap<>();
        for (EmissionCategoryBean category : categories) {
            List<List<String>> catData = new ArrayList<>();
            catMap.put(category.getName(), catData);
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleRented.getSrcId())) {

            ResultDTO dto = emissionController.emissionTransRented(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Vehicle Rented");
                emissionInfo.add( Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));



                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }
            }


        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Generator.getSrcId())) {

            ResultDTO dto = emissionController.emissionDieselGenerator(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("On-site Diesel Generators");
                emissionInfo.add( Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));



                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }
            }

        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ComOwned.getSrcId())) {
            ResultDTO dto = emissionController.emissionCompanyOwned(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Company Owned Vehicles");
                emissionInfo.add( Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));



                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }
            }

        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommPaid.getSrcId())) {
            ResultDTO dto = emissionController.emissionEmpCommPaid(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Employee Commuting, Paid");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }
            }
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Refri.getSrcId())) {
            ResultDTO dto = emissionController.emissionRefriLeak(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Refrigerant Leakage");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }

            }
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FireExt.getSrcId())) {
            ResultDTO dto = emissionController.emissionFireExt(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Fire Extinguishers");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OffRoad.getSrcId())) {
            ResultDTO dto = emissionController.emissionOffRoad(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Off-road Vehicles");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LPG.getSrcId())) {
            ResultDTO dto = emissionController.emissionLPGas(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("LP Gas");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }
        //bgas
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Bgas.getSrcId())) {
            ResultDTO dto = emissionController.emissionBGas(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("B Gas");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Elec.getSrcId())) {
            ResultDTO dto = emissionController.emissionElectricity(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Grid Connected Electricity");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TD.getSrcId())) {

            ResultDTO dto = emissionController.emissionTandDLoss(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Transmission and Distribution Loss");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteDisposal.getSrcId())) {
            ResultDTO dto = emissionController.emissionWasteDisposal(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Waste Disposal");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.MunWater.getSrcId())) {
            ResultDTO dto = emissionController.emissionMunWater(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Municipal Water");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirTravel.getSrcId())) {

            ResultDTO dto = emissionController.emissionAirtravel(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Business Air Travel");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId())) {
            ResultDTO dto = emissionController.emissionTransHiredNotPaid(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Vehicle Hired, Not Paid");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteTrans.getSrcId())) {
            ResultDTO dto = emissionController.emissionWasteTransport(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Waste Transport");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommNotPaid.getSrcId())) {

            ResultDTO dto = emissionController.emissionEmpCommNotPaid(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Employee Commuting, Not Paid");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredPaid.getSrcId())) {
            ResultDTO dto = emissionController.emissionTransHiredPaid(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Vehicle Hired, Paid");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TransLocPur.getSrcId())) {
            ResultDTO dto = emissionController.emissionTransportLocPurchased(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Transport Locally Purchased");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirFreight.getSrcId())) {
            ResultDTO dto = emissionController.emissionAirFreight(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Air Freight Transport");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SeaFreight.getSrcId())) {
            ResultDTO dto = emissionController.emissionSeaFreight(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Sea Freight Transport");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.BioMass.getSrcId())) {
            ResultDTO dto = emissionController.emissionBiomass(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Bio Mass");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.BioMass.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //ash trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AshTrans.getSrcId())) {
            ResultDTO dto = emissionController.emissionAshTrans(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Ash Transportation");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //forklift pertorl
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsPetrol.getSrcId())) {
            ResultDTO dto = emissionController.emissionForkliftsPetrol(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Fork-lifts Petrol");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }


        //forklifts diesel
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsDiesel.getSrcId())) {
            ResultDTO dto = emissionController.emissionForkliftsDiesel(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Fork-lifts Diesel");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //furnace oil
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FurnaceOil.getSrcId())) {
            ResultDTO dto = emissionController.emissionFurnaceOil(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Furnace Oil");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }


        //lory tran in
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransInternal.getSrcId())) {
            ResultDTO dto = emissionController.emissionLorryTransInternal(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Lorry Transportation Internal");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }


        //lorry tran ex
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransExternal.getSrcId())) {
            ResultDTO dto = emissionController.emissionLorryTransExternal(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Lorry Transportation External");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //oil gas trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OilGasTrans.getSrcId())) {
            ResultDTO dto = emissionController.emissionOilAndGasTrans(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Oil and Gas Transportation");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //paid manager vehicle
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaidManagerVehicle.getSrcId())) {
            ResultDTO dto = emissionController.emissionPaidManagerVehicle(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Paid Manager Vehicle Allowance");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //paper waste
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaperWaste.getSrcId())) {
            ResultDTO dto = emissionController.emissionPaperWaste(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Paper Waste");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //raw mat trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RawMatTrans.getSrcId())) {
            ResultDTO dto = emissionController.emissionRawMatTrans(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Raw Material Transportation Locally");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }


        //saw dust trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SawDustTrans.getSrcId())) {
            ResultDTO dto = emissionController.emissionSawDustTrans(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Saw Dust Transportation");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }

        }

        //sludge trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SludgeTrans.getSrcId())) {
            ResultDTO dto = emissionController.emissionSludgeTrans(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Sludge Transportation");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }

        //vehicle others
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehiclesOthers.getSrcId())) {
            ResultDTO dto = emissionController.emissionVehicleOthers(company.getId(), -1, company.getFyCurrentStart(), company.getFyCurrentEnd());
            if (dto != null) {
                List<String> emissionInfo = new ArrayList<>();
                emissionInfo.add("Other Vehicles");
                emissionInfo.add(Float.toString(dto.tco2e));
                emissionInfo.add(Float.toString(dto.co2));
                emissionInfo.add(Float.toString(dto.ch4));
                emissionInfo.add(Float.toHexString(dto.n2o));


                for (EmissionCategoryBean category : categories) {
                    if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getCatId() == category.getId()) {
                        catMap.get(category.getName()).add(emissionInfo);

                        break;
                    }
                }


            }
        }



        return catMap;


    }


    private List<HashMap<String, String>> getActivityData(int comId, int  branchId, int yearStart, int yearEnd) {
        List<HashMap<String, String>> outList = new ArrayList<>();
        HashMap<String, String> outMap = new LinkedHashMap<>();

        Company company = insstitutionController.findCompanyById(comId);

        if (company == null) return outList;
        List<Integer> allowedEmissionSrcs = company.getEmSources();
        Map<Integer, AllowedEmissionSrcBean> allowedEmissionSrcBeanMap = company.getAllowedEmissionSources();



        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Generator.getSrcId())) {
            ResultDTO generators = emissionController.emissionDieselGenerator(comId, branchId, yearStart, yearEnd);
            if (generators == null) {
                generators = new ResultDTO();
            }
            outMap.put("Onsite Diesel Generators (m3)", Float.toString(generators.diesel));
        }



        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Refri.getSrcId())) {
            ResultDTO refri = emissionController.emissionRefriLeak(comId, branchId, yearStart, yearEnd);
            if (refri == null) {
                refri   = new ResultDTO();
            }
            outMap.put("Refrigerant Leakages(R22) (tons)", Float.toString(refri.r22));
            outMap.put("Refrigerant Leakages(R407C)(tons)", Float.toString(refri.r407c));
            outMap.put("Refrigerant Leakages(R410A)(tons)",Float.toString(refri.r410a));
        }



        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FireExt.getSrcId())) {
            ResultDTO fireEx = emissionController.emissionFireExt(comId, branchId, yearStart, yearEnd);
            if (fireEx == null) {
                fireEx = new ResultDTO();
            }
            outMap.put("Fire Extinguisher(W Gas)(tons)", Float.toString(fireEx.wgas));
            outMap.put("Fire Extinguishers(CO2) (tons", Float.toString(fireEx.fire_co2));
            outMap.put("Fire Extinguishers(DCP) (tons)", Float.toString(fireEx.dcp));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ComOwned.getSrcId())) {
            ResultDTO comOwn = emissionController.emissionCompanyOwned(comId, branchId, yearStart, yearEnd);
            if (comOwn == null) {
                comOwn = new ResultDTO();
            }
            outMap.put("Company owned vehicles(Petrol) (m3)", Float.toString(comOwn.petrol));
            outMap.put("Company owned vehicles(Diesel) (m3)", Float.toString(comOwn.diesel));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OffRoad.getSrcId())) {
            ResultDTO offRoad = emissionController.emissionOffRoad(comId, branchId, yearStart, yearEnd);
            if (offRoad == null) {
                offRoad = new ResultDTO();
            }
            outMap.put("Off-road mobile sources and machineries (Petrol) (m3)", Float.toString(offRoad.petrol));
            outMap.put("Off-road mobile sources and machineries(Diesel) (m3)", Float.toString(offRoad.diesel));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommPaid.getSrcId())) {
            ResultDTO empCommPaid = emissionController.emissionEmpCommPaid(comId, branchId, yearStart, yearEnd);
            if (empCommPaid == null){
                empCommPaid = new ResultDTO();
            }
            outMap.put("Employee Transport, Paid By The" +
                    "Company (Petrol) (m3)", Float.toString(empCommPaid.petrol));
            outMap.put("Employee Transport, Paid By The" +
                    "Company (Diesel) (m3)", Float.toString(empCommPaid.diesel));
        }

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommNotPaid.getSrcId())) {
            ResultDTO empComNotPaid = emissionController.emissionEmpCommNotPaid(comId, branchId, yearStart, yearEnd);
            if (empComNotPaid == null){
                empComNotPaid = new ResultDTO();
            }
            outMap.put("Employee Transport, Not Paid By The" +
                    "Company (Petrol) (m3)", Float.toString(empComNotPaid.petrol));
            outMap.put("Employee Transport, Not Paid By The" +
                    "Company (Diesel) (m3)", Float.toString(empComNotPaid.diesel));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.MunWater.getSrcId())) {
            ResultDTO  mw = emissionController.emissionMunWater(comId, branchId, yearStart, yearEnd);
            if (mw == null){
                mw = new ResultDTO();
            }
            outMap.put("Municipal Water (m3)", Float.toString(mw.m3));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteDisposal.getSrcId())) {
            ResultDTO wasteDis = emissionController.emissionWasteDisposal(comId, branchId, yearStart, yearEnd);
            if (wasteDis == null) {
                wasteDis = new ResultDTO();
            }
            outMap.put("Waste Disposal(tons)", Float.toString(wasteDis.tons));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId())) {
            ResultDTO transHiredNotPaid = emissionController.emissionTransHiredNotPaid(comId, branchId, yearStart, yearEnd);
            if (transHiredNotPaid == null) {
                transHiredNotPaid = new ResultDTO();
            }
            outMap.put("Transport Hired , Not Paid (Petrol)(m3)", Float.toString(transHiredNotPaid.petrol));
            outMap.put("Transport Hired ,Not Paid (Diesel)(m3)", Float.toString(transHiredNotPaid.diesel));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredPaid.getSrcId())) {
            ResultDTO transHiredPaid = emissionController.emissionTransHiredPaid(comId, branchId, yearStart, yearEnd);
            if(transHiredPaid == null) {
                transHiredPaid = new ResultDTO();
            }
            outMap.put("Transport Hired , Paid (Petrol)(m3)", Float.toString(transHiredPaid.petrol));
            outMap.put("Transport Hired , Paid (Diesel)(m3)", Float.toString(transHiredPaid.diesel));
        }



        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleRented.getSrcId())) {
            ResultDTO transRented = emissionController.emissionTransRented(comId, branchId, yearStart, yearEnd);
            if (transRented == null){
                transRented = new ResultDTO();
            }
            outMap.put("Transport Rented (Petrol)(m3)", Float.toString(transRented.petrol));
            outMap.put("Transport Rented (Diesel)(m3)", Float.toString(transRented.diesel));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Elec.getSrcId())) {
            ResultDTO elec = emissionController.emissionElectricity(comId, branchId, yearStart, yearEnd);
            if (elec == null){
                elec = new ResultDTO();
            }
            outMap.put("Grid connected electricity (kWh)",Float.toString(elec.kwh));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirTravel.getSrcId())) {
            ResultDTO airtravel = emissionController.emissionAirtravel(comId, branchId, yearStart, yearEnd);
            if (airtravel == null){
                airtravel = new ResultDTO();
            }
            outMap.put("Business air travels (km)", Float.toString(airtravel.km));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteTrans.getSrcId())) {
            ResultDTO wasteTrans = emissionController.emissionWasteTransport(comId, branchId, yearStart, yearEnd);
            if (wasteTrans == null){
                wasteTrans =  new ResultDTO();
            }
            outMap.put("Waste Transport (Petrol) (m3)",Float.toString(wasteTrans.petrol));
            outMap.put("Waste Transport (Diesel) (m3", Float.toString(wasteTrans.diesel));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TransLocPur.getSrcId())) {
            ResultDTO transLoc = emissionController.emissionTransportLocPurchased(comId, branchId, yearStart, yearEnd);
            if (transLoc == null){
                transLoc = new ResultDTO();
            }
            outMap.put("Transport Locally Purchased (Petrol) (m3)", Float.toString(transLoc.petrol));
            outMap.put("Transport Locally Purchased (Diesel) (m3)", Float.toString(transLoc.diesel));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SeaFreight.getSrcId())) {
            ResultDTO seaFreight = emissionController.emissionSeaFreight(comId, branchId, yearStart, yearEnd);
            if (seaFreight == null) {seaFreight = new ResultDTO();}
            outMap.put("Sea Freight (tons)", Float.toString(seaFreight.tons));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirFreight.getSrcId())) {
            ResultDTO airFreight = emissionController.emissionAirFreight(comId, branchId, yearStart, yearEnd);
            if (airFreight == null) {airFreight = new ResultDTO(); }
            outMap.put("Air Freight (tons)", Float.toString(airFreight.tons));
        }


        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LPG.getSrcId())) {
            ResultDTO lpg = emissionController.emissionLPGas(comId, branchId, yearStart, yearEnd);
            if (lpg == null) {lpg = new ResultDTO(); }
            outMap.put("LP Gas (tons)", Float.toString(lpg.tons));
        }
        //bgas
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Bgas.getSrcId())) {
            ResultDTO bgas = emissionController.emissionBGas(comId, branchId, yearStart, yearEnd);
            if (bgas == null) {bgas = new ResultDTO(); }
            outMap.put("B Gas (tons)", Float.toString(bgas.tons));
        }



        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.BioMass.getSrcId())) {
            ResultDTO biomass = emissionController.emissionBiomass(comId, branchId, yearStart, yearEnd);
            if (biomass == null) {biomass = new ResultDTO(); }
            outMap.put("Bio Mass (kg)", Float.toString(biomass.tons));
        }


        //ash trans

        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AshTrans.getSrcId())) {
            ResultDTO dto= emissionController.emissionAshTrans(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Ash Transportation (Diesel) (m3)", Float.toString(dto.m3));
        }

        //fork lifts petrol
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsPetrol.getSrcId())) {
            ResultDTO dto= emissionController.emissionForkliftsPetrol(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Fork-lifts (Petrol) (m3)", Float.toString(dto.m3));
        }

        //forklifts diesel
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsDiesel.getSrcId())) {
            ResultDTO dto= emissionController.emissionForkliftsDiesel(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Fork-lifts (Diesel) (m3)", Float.toString(dto.m3));
        }
        //furnace oil
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FurnaceOil.getSrcId())) {
            ResultDTO dto= emissionController.emissionFurnaceOil(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Furnace Oil (m3)", Float.toString(dto.m3));
        }

        //lorry internal
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransInternal.getSrcId())) {
            ResultDTO dto= emissionController.emissionLorryTransInternal(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Internal Lorry Transportation (Diesel) (m3)", Float.toString(dto.m3));
        }

        //lorry external
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransExternal.getSrcId())) {
            ResultDTO dto= emissionController.emissionLorryTransExternal(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("External Lorry Transportation (Diesel) (m3)", Float.toString(dto.m3));
        }

        //oil and gas trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OilGasTrans.getSrcId())) {
            ResultDTO dto= emissionController.emissionOilAndGasTrans(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Oil and Gas Transportation (Diesel) (m3)", Float.toString(dto.m3));
        }

        //paper waste
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaperWaste.getSrcId())) {
            ResultDTO dto= emissionController.emissionPaperWaste(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Paper Waste (kg)", Float.toString(dto.tons));
        }

        //paid manager vehicle
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaidManagerVehicle.getSrcId())) {
            ResultDTO dto= emissionController.emissionPaidManagerVehicle(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Allowance Paid- Managers vehicles (Diesel) (m3)", Float.toString(dto.diesel));
            outMap.put("Allowance Paid- Managers vehicles (Petrol) (m3)", Float.toString(dto.petrol));

        }

        //raw mat trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RawMatTrans.getSrcId())) {
            ResultDTO dto= emissionController.emissionRawMatTrans(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Raw Material Transportation (Diesel) (m3)", Float.toString(dto.m3));
        }

        // saw dust trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SawDustTrans.getSrcId())) {
            ResultDTO dto= emissionController.emissionSawDustTrans(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Saw Dust Transportation (Diesel) (m3)", Float.toString(dto.m3));
        }

        //sludge trans
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SludgeTrans.getSrcId())) {
            ResultDTO dto = emissionController.emissionSludgeTrans(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Sludge Transportation (Diesel) (m3)", Float.toString(dto.m3));
        }

        //vehicle others
        if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehiclesOthers.getSrcId())) {
            ResultDTO dto = emissionController.emissionVehicleOthers(comId, branchId, yearStart, yearEnd);
            if (dto== null) {dto= new ResultDTO(); }
            outMap.put("Other Vehicles (Diesel) (m3)", Float.toString(dto.m3));
        }

















//        outMap.put("", "");










//        ResultDTO tdLoss = emissionController.emissionTandDLoss(comId, branchId, yearStart, yearEnd);
//        if (tdLoss == null){
//            tdLoss = new ResultDTO();
//        }
//        outMap.put("", "");

        outList.add(outMap);
        return outList;
    }

    private List<HashMap<String, String>> getSummaryMapList(List<GHGEmissionSummaryDTO> listDtos) {
        List<HashMap<String, String>> summaryList = new ArrayList<>();
        //Todo consider sorting if necessary
        for (GHGEmissionSummaryDTO dto: listDtos) {
            HashMap<String, String> yearSummaryMap = new LinkedHashMap<>();
            JsonObject emInfo = dto.getEmissionInfo();
            if (emInfo == null) {
                continue;
            }
            yearSummaryMap.put("Total GHG Emission", emInfo.get("total").getAsString());
            yearSummaryMap.put("Direct GHG Emission", emInfo.get("direct").getAsString());
            yearSummaryMap.put("Indirect GHG Emission", emInfo.get("indirect").getAsString());
            yearSummaryMap.put("Emission Intensity", emInfo.get("intensity").getAsString());
            yearSummaryMap.put("Emission Per-capita", emInfo.get("per_capita").getAsString());
//            yearSummaryMap.put("Emission Per Production", emInfo.get("per_prod").getAsString());
            yearSummaryMap.put("Scope 1", emInfo.get("scope_1").getAsString());
            yearSummaryMap.put("Scope 2", emInfo.get("scope_2").getAsString());
            yearSummaryMap.put("Scope 3", emInfo.get("scope_3").getAsString());
            summaryList.add(yearSummaryMap);
        }
        return summaryList;
    }

    private List<HashMap<String, String>> getDirectEmissionListMap(List<GHGEmissionSummaryDTO> listDtos) {

        List<HashMap<String, String>> directMapList = new ArrayList<>();

        for (GHGEmissionSummaryDTO dto :  listDtos) {
            JsonObject  emInfo = dto.getEmissionInfo();
            if (emInfo == null) {
                continue;
            }
            HashMap<String, String> directMap =  new LinkedHashMap<>();

            Company company = insstitutionController.findCompanyById(dto.getCompanyId());
            if (company == null) continue;
            List<Integer> allowedEmissionSrcs = company.getEmSources();
            Map<Integer, AllowedEmissionSrcBean> allowedEmissionSrcBeanMap = company.getAllowedEmissionSources();




            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleRented.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Vehicle Rented", emInfo.get("transport_rented").getAsString() );
                }



            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Generator.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Onsite Diesel Generators",emInfo.get("diesel_generators").getAsString() );
                }



            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ComOwned.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Company Owned Vehicles", emInfo.get("company_owned").getAsString() );
                }



            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Employee Commuting, Paid by The Company", emInfo.get("emp_comm_paid").getAsString() );
                }

            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Refri.getSrcId())) {

                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Refrigerant Leakages", emInfo.get("refri_leakage").getAsString() );
                }


            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FireExt.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Fire Extinguisher", emInfo.get("fire_ext").getAsString() );
                }


            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OffRoad.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Off-road mobile sources and machineries" , emInfo.get("offroad").getAsString() );
                }

            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LPG.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("LP Gas", emInfo.get("lpg").getAsString() );
                }


            }
            //bgas
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Bgas.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("B Gas", emInfo.get("bgas").getAsString() );
                }


            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Elec.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Grid Connected Electricity",emInfo.get("electricity").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TD.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Transmission and Distribution Loss", emInfo.get("t_d").getAsString());
                }


            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteDisposal.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Waste Disposal", emInfo.get("waste_disposal").getAsString());
                }


            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.MunWater.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Municipal Water", emInfo.get("mun_water").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirTravel.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Business air travels", emInfo.get("air_travel").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Vehicle Hired, Not Paid", emInfo.get("transport_hired_not_paid").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteTrans.getSrcId())) {

                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Waste Transportation", emInfo.get("waste_transport").getAsString());
                }
            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommNotPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Employee Commuting, Not Paid by The Company" , emInfo.get("emp_comm_not_paid").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Vehicle Hired, Paid", emInfo.get("transport_hired_paid").getAsString() );
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TransLocPur.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Transport Locally Purchased", emInfo.get("transport_loc_pur").getAsString() );
                }

            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirFreight.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Air Freight Transport", emInfo.get("air_freight").getAsString() );
                }
            }


            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SeaFreight.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Sea Freight Transport", emInfo.get("sea_freight").getAsString() );
                }
            }


            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.BioMass.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.BioMass.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Bio Mass", emInfo.get("bio_mass").getAsString() );
                }
            }

            //ash trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AshTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Ash Transportation", emInfo.get("ash_trans").getAsString() );
                }
            }

            //forklift pertorl
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsPetrol.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Fork-lifts (Petrol)", emInfo.get("forklifts_petrol").getAsString() );
                }
            }


            //forklifts diesel
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsDiesel.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Fork-lifts (Diesel)", emInfo.get("forklifts_diesel").getAsString() );
                }
            }

            //furnace oil
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FurnaceOil.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Furnace Oil", emInfo.get("furnace_oil").getAsString() );
                }
            }


            //lory tran in
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransInternal.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Internal Lorry Transportation", emInfo.get("lorry_trans_internal").getAsString() );
                }
            }


            //lorry tran ex
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransExternal.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("External Lorry Transportation", emInfo.get("lorry_trans_external").getAsString() );
                }
            }

            //oil gas trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OilGasTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Oil and Gas Transportation", emInfo.get("oil_gas_trans").getAsString() );
                }
            }

            //paid manager vehicle
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaidManagerVehicle.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Managers Paid Transport Allowance", emInfo.get("paid_manager_vehicles").getAsString() );
                }
            }

            //paper waste
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaperWaste.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Paper Waste", emInfo.get("paper_waste").getAsString() );
                }
            }

            //raw mat trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RawMatTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Raw Material Transportation Locally", emInfo.get("raw_mat_trans").getAsString() );
                }

            }


            //saw dust trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SawDustTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Saw Dust Transportation", emInfo.get("saw_dust_trans").getAsString() );
                }

            }

            //sludge trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SludgeTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Sludge Transportation", emInfo.get("sludge_trans").getAsString() );
                }
            }

            //vehicle others
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehiclesOthers.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                    directMap.put("Others Vehicle", emInfo.get("vehicle_others").getAsString() );
                }
            }





            directMapList.add(directMap);
        }
        return directMapList;
    }

    private List<HashMap<String, String>> getIndirectEmissionListMap(List<GHGEmissionSummaryDTO> listDtos) {
        List<HashMap<String, String >> indirectMapList = new ArrayList<>();

        for(GHGEmissionSummaryDTO dto: listDtos) {
            JsonObject emInfo = dto.getEmissionInfo();
            if (emInfo == null) {
                continue;
            }
            HashMap<String, String> indirectMap = new LinkedHashMap<>();

            Company company = insstitutionController.findCompanyById(dto.getCompanyId());
            if (company == null) continue;
            List<Integer> allowedEmissionSrcs = company.getEmSources();
            Map<Integer, AllowedEmissionSrcBean> allowedEmissionSrcBeanMap = company.getAllowedEmissionSources();

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleRented.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Vehicle Rented", emInfo.get("transport_rented").getAsString() );
                }



            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Generator.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Onsite Diesel Generators",emInfo.get("diesel_generators").getAsString() );
                }



            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ComOwned.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Company Owned Vehicles", emInfo.get("company_owned").getAsString() );
                }



            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Employee Commuting, Paid by The Company", emInfo.get("emp_comm_paid").getAsString() );
                }

            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Refri.getSrcId())) {

                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Refrigerant Leakages", emInfo.get("refri_leakage").getAsString() );
                }


            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FireExt.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Fire Extinguisher", emInfo.get("fire_ext").getAsString() );
                }


            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OffRoad.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Off-road mobile sources and machineries" , emInfo.get("offroad").getAsString() );
                }

            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LPG.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("LP Gas", emInfo.get("lpg").getAsString() );
                }


            }
            //bgas
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Bgas.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Bgas.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("B Gas", emInfo.get("bgas").getAsString() );
                }


            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.Elec.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Grid Connected Electricity",emInfo.get("electricity").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TD.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Transmission and Distribution Loss", emInfo.get("t_d").getAsString());
                }


            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteDisposal.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Waste Disposal", emInfo.get("waste_disposal").getAsString());
                }


            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.MunWater.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Municipal Water", emInfo.get("mun_water").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirTravel.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Business air travels", emInfo.get("air_travel").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Vehicle Hired, Not Paid", emInfo.get("transport_hired_not_paid").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.WasteTrans.getSrcId())) {

                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Waste Transportation", emInfo.get("waste_transport").getAsString());
                }
            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.EmpCommNotPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Employee Commuting, Not Paid by The Company" , emInfo.get("emp_comm_not_paid").getAsString());
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehicleHiredPaid.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Vehicle Hired, Paid", emInfo.get("transport_hired_paid").getAsString() );
                }

            }
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.TransLocPur.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Transport Locally Purchased", emInfo.get("transport_loc_pur").getAsString() );
                }

            }

            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AirFreight.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Air Freight Transport", emInfo.get("air_freight").getAsString() );
                }
            }


            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SeaFreight.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Sea Freight Transport", emInfo.get("sea_freight").getAsString() );
                }
            }


            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.BioMass.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.BioMass.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Bio Mass", emInfo.get("bio_mass").getAsString() );
                }
            }

            //ash trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.AshTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Ash Transportation", emInfo.get("ash_trans").getAsString() );
                }
            }

            //forklift pertorl
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsPetrol.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Fork-lifts (Petrol)", emInfo.get("forklifts_petrol").getAsString() );
                }
            }


            //forklifts diesel
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.ForkliftsDiesel.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Fork-lifts (Diesel)", emInfo.get("forklifts_diesel").getAsString() );
                }
            }

            //furnace oil
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.FurnaceOil.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Furnace Oil", emInfo.get("furnace_oil").getAsString() );
                }
            }


            //lory tran in
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransInternal.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Internal Lorry Transportation", emInfo.get("lorry_trans_internal").getAsString() );
                }
            }


            //lorry tran ex
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.LorryTransExternal.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("External Lorry Transportation", emInfo.get("lorry_trans_external").getAsString() );
                }
            }

            //oil gas trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.OilGasTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Oil and Gas Transportation", emInfo.get("oil_gas_trans").getAsString() );
                }
            }

            //paid manager vehicle
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaidManagerVehicle.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Managers Paid Transport Allowance", emInfo.get("paid_manager_vehicles").getAsString() );
                }
            }

            //paper waste
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.PaperWaste.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Paper Waste", emInfo.get("paper_waste").getAsString() );
                }
            }

            //raw mat trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.RawMatTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Raw Material Transportation Locally", emInfo.get("raw_mat_trans").getAsString() );
                }

            }


            //saw dust trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SawDustTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Saw Dust Transportation", emInfo.get("saw_dust_trans").getAsString() );
                }

            }

            //sludge trans
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.SludgeTrans.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Sludge Transportation", emInfo.get("sludge_trans").getAsString() );
                }
            }

            //vehicle others
            if (allowedEmissionSrcBeanMap.containsKey(EmissionSourceEnum.VehiclesOthers.getSrcId())) {
                if (allowedEmissionSrcBeanMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                    indirectMap.put("Others Vehicle", emInfo.get("vehicle_others").getAsString() );
                }
            }



            indirectMapList.add(indirectMap);
        }
        return indirectMapList;
    }

    public List<GHGEmissionSummaryDTO> getEmissionSummary(int comId) throws Exception {
        Company company = insstitutionController.findCompanyById(comId);
        int yStart = company.getFyCurrentStart();
        int yEnd = company.getFyCurrentEnd();

        String fy = yStart == yEnd ? "" + yStart : "" + yStart + "/" + (yEnd % 100);

        try {
            List<GHGEmissionSummaryDTO> dtos = new ArrayList<>();
            //current
            HashMap<String, String> current = emissionController.emission_tco2(comId, -1, yStart, yEnd);
            dtos.add(GHGSummaryPopulator.getInstance().fromHashMap(comId, fy, EmissionSummaryModeofEntry.Manual.mode, current));
            int y =1;
            while(true) {
                String fy_prev = company.isFinacialYear() ? ("" + (yStart - y) + "/" + ((yEnd - y ) % 100) + "") : ("" + (yStart - y) + "");
                GHGEmsissionSummary summary = null;
                try {
                    summary = ghgEmissionSummaryController.findById(comId, -1,fy_prev);
                    if (summary != null) {
                        dtos.add(GHGSummaryPopulator.getInstance().fromHashMap(comId, summary.getId().getFY(), summary.getModeOfEntry(), summary.getEmission()));

                    }
                    if(dtos.size() >= 5 || y >= 10) {
                        break;
                    }

                }catch (Exception e) {
                    logger.error(e.getMessage());
                    continue;
                }
                y++;
            }
            return dtos;

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw  new Exception(e.getMessage());
        }
    }


}
