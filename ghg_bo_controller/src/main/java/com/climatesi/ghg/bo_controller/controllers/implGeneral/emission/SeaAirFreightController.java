package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.MasterDataFactory;
import com.climatesi.ghg.api.beans.SeaAirFreightPort;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.SeaAirFrieghtPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.SeaAirFreightDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.SeaAirFreightEntry;
import com.climatesi.ghg.emission_source.api.beans.SeaAirFreightEntry;
import com.climatesi.ghg.emission_source.api.facades.ElectricityEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.ElectricityEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.SeaAirFreightEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.SeaAirFreightFacadeManager;
import com.climatesi.ghg.implGeneral.data.GridElectricityData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.implGeneral.data.TandDLossCalcData;
import com.climatesi.ghg.implGeneral.facades.SeaAirFreightPortFacadeManger;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;


@Stateless
public class SeaAirFreightController extends AbstractController {

    private static final Logger logger = Logger.getLogger(SeaAirFreightController.class);
    private SeaAirFreightFacadeManager seaAirFreightFacadeManager;
    private SeaAirFreightPortFacadeManger seaAirFreightPortFacadeManger;
    private EmissionFactorsManager emissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {
        try {
           

            seaAirFreightFacadeManager = EmsissionSourceFactory.getInstance().getSeaAirFreightFacadeManager(em);
            seaAirFreightPortFacadeManger = (SeaAirFreightPortFacadeManger) MasterDataFactory.getInstance().getSeaAirFreightPortFacade(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing electrictiy Controller", e);
        }
    }



    public ListResult<SeaAirFreightEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return seaAirFreightFacadeManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public SeaAirFreightEntry edit(SeaAirFreightDTO dto) throws GHGException {
        SeaAirFreightEntry entry;
        logger.info("###################### 65");

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                logger.info("############################# 70");
                entry = findById(dto.getId());
                SeaAirFrieghtPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                logger.info("######################## 73");
                if (entry.getIsDeleted() !=1) {
                    logger.info("######################## 75");
                    entry.setEmissionDetails(emission(entry));
                    logger.info("###################### after calculation");
                }

                //          Todo: add user id
                status = (String) seaAirFreightFacadeManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                logger.info("########################### 88");
                entry = new SeaAirFreightEntryBean();
                SeaAirFrieghtPopulator.getInstance().populate(entry, dto);
                logger.info("################## before calculate emission");
                entry.setEmissionDetails(emission(entry));
                logger.info("#################### after calculation");
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) seaAirFreightFacadeManager.addEntity(entry);
            }

            //        Todo check status and throw exceptions

        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public SeaAirFreightEntry findById(Integer id) {
        return seaAirFreightFacadeManager.getEntityByKey(id);
    }

    public HashMap<String, String> emission(SeaAirFreightEntry entry) {
        logger.info("########################3 inside emission");
        if (entry == null) {
            logger.info("#############################################################33333");
            logger.info("Entry is null");
           return new HashMap<String, String>();
        }

        float cfNauticMilesToKm = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CF_NAUTIC_KM).getValue();
        float efCO2AirFreightRange1 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_AIR_FREIGHT_RANGE1).getValue();
        float efCO2AirFreightRange2 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_AIR_FREIGHT_RANGE2).getValue();
        float efCO2AirFreightRange3 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_AIR_FREIGHT_RANGE3).getValue();
        float efCO2SeaFreight =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_SEA_FREIGHT).getValue();

        logger.info("################  Loaded Efs");

        
        float amount = entry.getFreightKg();
        SeaAirFreightPort port = seaAirFreightPortFacadeManger.getEntityByKey(entry.getPortFrom());
        if (port == null) {
            logger.info("#################  Port is null");
            return new HashMap<String, String>();
        }
        float distance = entry.getIsAirFreight() == 1 ?   port.getNauticSeaMilesFromCMB() : port.getNauticAirMilesFromCMB();


        logger.info("######### about to cacluatte");
       ResultDTO resultDTO =  entry.getIsAirFreight() == 2 ? Calculator.emissionAirFreight(
               amount,
               distance,
               cfNauticMilesToKm,
               efCO2AirFreightRange1,
               efCO2AirFreightRange2,
               efCO2AirFreightRange3
       ): Calculator.emissionSeaFreight(
               amount,
               distance,
               cfNauticMilesToKm,
               efCO2SeaFreight

       );

       if (resultDTO == null) {
           logger.info("#############################################################33333");
           logger.info("Result is null");
           return  new HashMap<>();
       }
       
        HashMap<String, String> results = new HashMap<>();
        results.put("tco2", String.format("%.5f", resultDTO.tco2e ));
        results.put("co2", String.format("%.5f",resultDTO.co2 ));
        results.put("n20", String.format("%.5f",resultDTO.n2o ));
        results.put("ch4", String.format("%.5f", resultDTO.ch4));
        results.put("quantity", String.format("%.5f",  entry.getFreightKg()));
        results.put("direct", Boolean.toString(true));
        results.put("distance", String.format("%.5f",  distance));


        results.put("cf", String.format("%.5f",  cfNauticMilesToKm));
        results.put("ef", String.format("%.5f", resultDTO.ef));

        return results;
    }
}
