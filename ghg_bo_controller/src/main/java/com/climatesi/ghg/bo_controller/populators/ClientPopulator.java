package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.ClientDTO;
import com.climatesi.ghg.business_users.api.beans.Client;
import org.jboss.logging.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClientPopulator {

    private static Logger logger = Logger.getLogger(ClientPopulator.class);
    private static ClientPopulator instance;

    public static ClientPopulator getInstance() {
        if (instance == null) {
            instance = new ClientPopulator();
        }
        return instance;
    }

    public ClientDTO populateDTO(Client client) {
        ClientDTO dto = new ClientDTO();
        try {
            dto.setId(client.getId());
            dto.setFirstName(client.getFirstName());
            dto.setLastName(client.getLastName());
            dto.setTitle(client.getTitle());
            dto.setEmail(client.getEmail());
            dto.setMobileNo(client.getMobileNo());
            dto.setTelephoneNo(client.getTelephoneNo());
            dto.setSector(client.getSector());
//            DateFormat format = new SimpleDateFormat("yyyy-MM-d");
//            dto.setAgreementStartDate(format.format(client.getAgreementStartDate()));
//            dto.setAgreementEndDate(format.format(client.getAgreementStartDate()));
            dto.setClientCode(client.getClientCode());
            dto.setUserId(client.getUserId());
            dto.setBranchId(client.getBranchId());
            dto.setCompanyId(client.getCompanyId());
            dto.setEntitlements(client.getEntitlements());
            dto.setBranch(client.getBranch());
            dto.setCompany(client.getCompany());
        } catch (NullPointerException e) {
            logger.error("Error in populating ", e);
        }
        return dto;
    }

    public Client populate(Client client, ClientDTO dto) {
        try {
            if (dto.getId() > 0) {
                client.setId(dto.getId());
                if (dto.isDeleted() == 1) {
                    client.setDeleted(dto.isDeleted());
                    return client;
                }

            }
            client.setFirstName(dto.getFirstName() == null ? client.getFirstName() : dto.getFirstName());
            client.setLastName(dto.getLastName() == null ? client.getLastName() : dto.getLastName());
            client.setTitle(dto.getTitle() == null ? client.getTitle() : dto.getTitle());
            client.setTelephoneNo(dto.getTelephoneNo() == null ? client.getTelephoneNo() : dto.getTelephoneNo());
            client.setMobileNo(dto.getMobileNo() == null ? client.getMobileNo() : dto.getMobileNo());
            client.setEmail(dto.getEmail() == null ? client.getEmail() : dto.getEmail());
            client.setSector(dto.getSector() == null ? client.getSector() : dto.getSector());
            client.setClientCode(dto.getClientCode() == null ? client.getClientCode() : dto.getClientCode());
            client.setUserId(dto.getUserId() == 0 ? client.getUserId() : dto.getUserId());
            client.setCompanyId(dto.getCompanyId() == 0 ? client.getCompanyId() : dto.getCompanyId());
            client.setBranchId(dto.getBranchId() == 0 ? client.getBranchId() : dto.getBranchId());

//            try {
////                String string = dto.getAgreementStartDate();
////                DateFormat format = new SimpleDateFormat("yyyy-MM-d");
////                Date date = format.parse(string);
////                client.setAgreementStartDate(dto.getAgreementStartDate() == null ? client.getAgreementStartDate() : date);
////                string = dto.getAgreementEndDate();
////                date = format.parse(string);
////                client.setAgreementEndDate(dto.getAgreementEndDate() == null ? client.getAgreementEndDate() : date);
//            } catch (Exception e) {
//                logger.error("Error in parsing dates", e);
//            }


            client.setBranchId(dto.getBranchId() == 0 ? client.getBranchId() : dto.getBranchId());
            client.setCompanyId(dto.getCompanyId() == 0 ? client.getCompanyId() : dto.getCompanyId());
            client.setEntitlements(dto.getEntitlements() == null ? client.getEntitlements() : dto.getEntitlements());
            client.setCompany(dto.getCompany() == null ? client.getCompany() : dto.getCompany());
            client.setBranch(dto.getBranch() == null ? client.getBranch() : dto.getBranch());

        } catch (NullPointerException e) {
            logger.error("Error in populating", e);
        }
        return client;
    }

}
