package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.ReportMetaData;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.report.core.ReportMetaDataDTO;
import com.climatesi.ghg.implGeneral.beans.ReportMetaDataBean;
import com.google.gson.JsonElement;
import org.jboss.logging.Logger;

import java.util.Map;
import java.util.Set;

public class ReportMetaDataPopulator {
    private static final Logger logger = Logger.getLogger(ReportMetaDataPopulator.class);

    private static ReportMetaDataPopulator instance;

    public static ReportMetaDataPopulator getInstance() {
        if (instance == null) {
            instance = new ReportMetaDataPopulator();
        }
        return instance;
    }

    public ReportMetaDataDTO populateDTO(ReportMetaData metaData) {
        ReportMetaDataDTO dto = new ReportMetaDataDTO();
        dto.setId(metaData.getId());
        dto.setProjectId(metaData.getProjectId());
        dto.setComName_header(metaData.getComName_header() );
        dto.setComName_address(metaData.getComName_address());
        dto.setAddr1(metaData.getAddr1());
        dto.setAddr2(metaData.getAddr2());
        dto.setDistrict(metaData.getDistrict());
        dto.setComRegNo(metaData.getComRegNo());
        dto.setEmail(metaData.getEmail());
        dto.setTelphone(metaData.getTelphone());
        dto.setFax(metaData.getFax());
        dto.setClimateSi_Name(metaData.getClimateSi_Name());
        dto.setClimateSi_addr1(metaData.getClimateSi_addr1());
        dto.setClimateSi_addr2(metaData.getClimateSi_addr2());
        dto.setClimateSi_district(metaData.getClimateSi_district());
        dto.setClimateSi_email(metaData.getClimateSi_email());
        dto.setClimaeSi_telephone(metaData.getClimaeSi_telephone());
        dto.setClimatesi_fax(metaData.getClimatesi_fax());
        dto.setYear(metaData.getYear() );
        dto.setYearStartDate(metaData.getYearStartDate() );
        dto.setYearEndDate(metaData.getYearEndDate());
        dto.setAttempt(metaData.getAttempt());
//        if (dto.getExcludedSrcAndReasons()) {
//            Set<Map.Entry<String, JsonElement>> entrySet  = dto.getExcludedSrcAndReasons().entrySet();
//            for(Map.Entry<String,JsonElement> entry : entrySet){
//                String key = entry.getKey();
//                // todo;
//                logger.info(dto.getExcludedSrcAndReasons().get(key));
//            }
//        }

        dto.setProposedDate(metaData.getProposedDate());
        dto.setDesCompany(metaData.getDesCompany());
        dto.setDesResUnit(metaData.getDesResUnit());
        dto.setDesEnvironmentalMgmntFramework(metaData.getDesEnvironmentalMgmntFramework());
        dto.setIsoStandard(metaData.getIsoStandard());
        dto.setPreviousYearISOStandard(metaData.getPreviousYearISOStandard());
        dto.setSuggestions(metaData.getSuggestions() );
        dto.setDesGHGMitigationActions(metaData.getDesGHGMitigationActions());
        dto.setNextSteps(metaData.getNextSteps());
        dto.setProducts(metaData.getProducts());
        dto.setFigEnvMgmtFramework(metaData.getFigEnvMgmtFramework());
        dto.setFigResUnit(metaData.getFigResUnit());
        dto.setSrcResUnit(metaData.getSrcResUnit());
        dto.setSrcEnvMgmtFramework(metaData.getSrcEnvMgmtFramework());
        dto.setPreviousYearEfs(metaData.getPreviousYearEfs());
        dto.setPreviousYearEmission(metaData.getPreviousYearEmission());
        dto.setPreviousYearStats(metaData.getPreviousYearStats());

        return dto;

    }

    public ReportMetaData populate(ReportMetaDataBean bean, ReportMetaDataDTO dto) {
        try {
            if (dto.getId() > 0) {
                bean.setId(dto.getId());
            }
            bean.setProjectId(dto.getProjectId() == 0 ? bean.getProjectId() : dto.getProjectId());
            bean.setComName_header(dto.getComName_header() == null ? bean.getComName_header(): dto.getComName_header());
            bean.setComName_address(dto.getComName_address() == null ? bean.getComName_address() : dto.getComName_address());
            bean.setAddr1(dto.getAddr1() == null ? bean.getAddr1() : dto.getAddr1());
            bean.setAddr2(dto.getAddr2() == null ? bean.getAddr2() : dto.getAddr2());
            bean.setDistrict(dto.getDistrict() == null ? bean.getDistrict() : dto.getDistrict());
            bean.setComRegNo(dto.getComRegNo() == null ? bean.getComRegNo() : dto.getComRegNo());
            bean.setEmail(dto.getEmail() ==  null ? bean.getEmail() : dto.getEmail());
            bean.setTelphone(dto.getTelphone() == null ? bean.getTelphone() : dto.getTelphone());
            bean.setFax(dto.getFax() == null ? bean.getFax() : dto.getFax());
            bean.setClimateSi_Name(dto.getClimateSi_Name() == null ? bean.getClimateSi_Name() : dto.getClimateSi_Name());
            bean.setClimateSi_addr1(dto.getClimateSi_addr1()== null ? bean.getClimateSi_addr1() : dto.getClimateSi_addr1());
            bean.setClimateSi_addr2(dto.getClimateSi_addr2() == null ? bean.getClimateSi_addr2() : dto.getClimateSi_addr2());
            bean.setClimateSi_district(dto.getClimateSi_district() == null ? bean.getClimateSi_district() : dto.getClimateSi_district());
            bean.setClimateSi_email(dto.getClimateSi_email() == null ? bean.getClimateSi_email() : dto.getClimateSi_email());
            bean.setClimaeSi_telephone(dto.getClimaeSi_telephone() == null ? bean.getClimaeSi_telephone(): dto.getClimaeSi_telephone());
            bean.setClimatesi_fax(dto.getClimatesi_fax()== null ? bean.getClimatesi_fax(): dto.getClimatesi_fax());
            bean.setYear(dto.getYear() == null ? bean.getYear(): dto.getYear());
            bean.setYearStartDate(dto.getYearStartDate() == null ? bean.getYearStartDate() : dto.getYearStartDate());
            bean.setYearEndDate(dto.getYearEndDate() == null ? bean.getYearEndDate() : dto.getYearEndDate());
            bean.setAttempt(dto.getAttempt());
            if (dto.getExcludedSrcAndReasons() != null) {
                Set<Map.Entry<String, JsonElement>> entrySet  = dto.getExcludedSrcAndReasons().entrySet();
                for(Map.Entry<String,JsonElement> entry : entrySet){
                    String key = entry.getKey();
                    // todo;
                    logger.info(dto.getExcludedSrcAndReasons().get(key));
                }
            }

            bean.setProposedDate(dto.getProposedDate() == null ? bean.getProposedDate() : dto.getProposedDate());
            bean.setDesCompany(dto.getDesCompany() == null ? bean.getDesCompany() : dto.getDesCompany());
            bean.setDesResUnit(dto.getDesResUnit() == null ? bean.getDesResUnit() : dto.getDesResUnit());
            bean.setDesEnvironmentalMgmntFramework(dto.getDesEnvironmentalMgmntFramework() == null ? bean.getDesEnvironmentalMgmntFramework() : dto.getDesEnvironmentalMgmntFramework());
            bean.setIsoStandard(dto.getIsoStandard() == null ? bean.getIsoStandard() : dto.getIsoStandard());
            bean.setPreviousYearISOStandard(dto.getPreviousYearISOStandard() == null ? bean.getPreviousYearISOStandard() : dto.getPreviousYearISOStandard());
            bean.setSuggestions(dto.getSuggestions() == null ? bean.getSuggestions() : dto.getSuggestions());
            bean.setDesGHGMitigationActions(dto.getDesGHGMitigationActions() ==null ? bean.getDesGHGMitigationActions() : dto.getDesGHGMitigationActions());
            bean.setNextSteps(dto.getNextSteps() == null ? bean.getNextSteps() : dto.getNextSteps());
            bean.setProducts(dto.getProducts() == null ? bean.getProducts() : dto.getProducts());
            bean.setFigEnvMgmtFramework(dto.getFigEnvMgmtFramework() == null ? bean.getFigEnvMgmtFramework() : dto.getFigEnvMgmtFramework());
            bean.setFigResUnit(dto.getFigResUnit() == null ? bean.getFigResUnit(): bean.getFigResUnit());
            bean.setSrcEnvMgmtFramework(dto.getSrcEnvMgmtFramework() == null ? bean.getSrcEnvMgmtFramework() : dto.getSrcEnvMgmtFramework());
            bean.setSrcResUnit(dto.getSrcResUnit() == null ? bean.getSrcResUnit(): dto.getSrcResUnit());
            bean.setPreviousYearEmission(dto.getPreviousYearEmission() == null ? bean.getPreviousYearEmission(): dto.getPreviousYearEmission());
            bean.setPreviousYearEfs(dto.getPreviousYearEfs() == null ? bean.getPreviousYearEfs() : dto.getPreviousYearEfs());
            bean.setPreviousYearStats(dto.getPreviousYearStats() == null ?bean.getPreviousYearStats(): dto.getPreviousYearStats());
            bean.setGr_com_direct_by_src(dto.getGr_com_direct_by_src());
            bean.setGr_result_by_src(dto.getGr_result_by_src());
            bean.setGr_pie_direct(dto.getGr_pie_direct());
            bean.setGr_pie_indirect(dto.getGr_pie_indirect());
            bean.setGr_com_ghg(dto.getGr_com_ghg());
            bean.setGr_com_direct_by_src(dto.getGr_com_direct_by_src());
            bean.setGr_com_indirect_by_src(dto.getGr_com_indirect_by_src());
            bean.setGr_percaptia(dto.getGr_percaptia());
            bean.setGr_intensity(dto.getGr_intensity());
            bean.setGr_per_production(dto.getGr_per_production());

        }catch (Exception e) {
            logger.error(e);
        }
        return bean;
    }
}
