package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LorryTransportationDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.LorryTransportationDTO;
import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class LorryTransportationPopulator {

    private static Logger logger = Logger.getLogger(LorryTransportationPopulator.class);
    private static LorryTransportationPopulator instance;



    public static LorryTransportationPopulator getInstance() {
        if (instance == null) {
            instance = new LorryTransportationPopulator();
        }
        return instance;
    }

    public LorryTransportationDTO populateDTO(LorryTransportation entry) {
        LorryTransportationDTO dto = new LorryTransportationDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setAddedDate(entry.getAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setDistance(entry.getDistance());
        dto.setFuelEconomy(entry.getFuelEconomy());
        dto.setTransportMode(entry.getTransportMode());

        dto.setNoOfTrips(entry.getNoOfTrips());
        dto.setFuelConsumption(entry.getFuelConsumption());

        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public LorryTransportation populate(LorryTransportation entry, LorryTransportationDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.getIsDeleted() == 1) {
                    entry.setIsDeleted(dto.getIsDeleted());
                    return entry;
                }
            }

            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());

            entry.setFuelEconomy(dto.getFuelEconomy());
            entry.setDistance(dto.getDistance());
            entry.setTransportMode(dto.getTransportMode());

            entry.setNoOfTrips(dto.getNoOfTrips());
            entry.setFuelConsumption(dto.getFuelConsumption());

            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
