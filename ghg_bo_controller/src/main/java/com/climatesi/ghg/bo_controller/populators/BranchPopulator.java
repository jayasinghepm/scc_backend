package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.BranchDTO;
import org.jboss.logging.Logger;

public class BranchPopulator {

    private static BranchPopulator instance;
    private Logger logger = Logger.getLogger(BranchPopulator.class);

    private BranchPopulator() {

    }

    public static BranchPopulator getInstance() {
        if (instance == null) {
            instance = new BranchPopulator();
        }
        return instance;
    }

    public BranchDTO populateDTO(Branch branch) {
        BranchDTO dto = new BranchDTO();
        try {
            dto.setId(branch.getId());
            dto.setName(branch.getName());
            dto.setAddr1(branch.getAddr1());
            dto.setAddr2(branch.getAddr2());
            dto.setDistrict(branch.getDistrict());
            dto.setCode(branch.getCode());
            dto.setUserName(branch.getUserName());
            dto.setCompanyId(branch.getCompanyId());
            dto.setCompany(branch.getCompany());
        } catch (NullPointerException e) {
            logger.error("Error in populating BranchDTO", e);
        }
        return dto;
    }

    public Branch populate(Branch branch, BranchDTO dto) {
        try {
            if (dto.getId() > 0) {
                branch.setId(dto.getId());
                if (dto.isDeleted() == 1) {
                    branch.setDeleted(dto.isDeleted());
                    return branch;
                }

            }
            branch.setName(dto.getName() == null ? branch.getName() : dto.getName());
            branch.setAddr1(dto.getAddr1() == null ? branch.getAddr1() : dto.getAddr1());
            branch.setAddr2(dto.getAddr2() == null ? branch.getAddr2() : dto.getAddr2());
            branch.setDistrict(dto.getDistrict() == null ? branch.getDistrict() : dto.getDistrict());
            branch.setCode(dto.getCode() == null ? branch.getCode() : dto.getCode());
            branch.setUserName(dto.getUserName() == null ? branch.getUserName() : dto.getUserName());
            branch.setCompanyId(dto.getCompanyId() == 0 ? branch.getCompanyId() : dto.getCompanyId());
            branch.setCompany(dto.getCompany() == null ? branch.getCompany() : dto.getCompany());
        } catch (NullPointerException e) {
            logger.error("Error in populating BranchDTO", e);
        }
        return branch;
    }
}
