package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.MunicipalWaterPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterEntry;
import com.climatesi.ghg.emission_source.api.enums.Units;
import com.climatesi.ghg.emission_source.api.facades.MunicipalWaterEntryManager;
import com.climatesi.ghg.emission_source.api.facades.MunicipalWaterMeterReadingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.MunicipalWaterEntryBean;
import com.climatesi.ghg.implGeneral.data.MunicipalWaterCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class MunicipalWaterController extends AbstractController {

    private static final Logger logger = Logger.getLogger(MunicipalWaterController.class);
    private MunicipalWaterEntryManager municipalWaterEntryManager;
    private MunicipalWaterMeterReadingEntryManager municipalWaterMeterReadingEntryManager;
    private EmissionFactorsManager emissionFactorsManager;


    @PostConstruct
    @Override
    public void initialize() {

        try {
            municipalWaterEntryManager = EmsissionSourceFactory.getInstance().getMunicipalWaterEntryManager(em);
            municipalWaterMeterReadingEntryManager = EmsissionSourceFactory.getInstance().getMunicipalWaterMeterReadingEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing MunicipalWaterController", e);
        }
    }

    public ListResult<MunicipalWaterEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return municipalWaterEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public MunicipalWaterEntry editMunicipalWaterEntry(MunicipalWaterEntryDTO dto) throws GHGException {
        MunicipalWaterEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                MunicipalWaterPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emissionMunWater(entry));

                }

                //          Todo: add user id
                status = (String) municipalWaterEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new MunicipalWaterEntryBean();
                MunicipalWaterPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emissionMunWater(entry));
                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) municipalWaterEntryManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public MunicipalWaterEntry findById(Integer id) {
        return municipalWaterEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emissionMunWater(MunicipalWaterEntry municipalWaterEntry) {

//        float consumptionLiters = 0;
        float consumptionm3 = 0;



       if (municipalWaterEntry.getUnits() == Units.LKR.unit) {
           float monServiceCharge = 0;
           float usageCharge = 75;
//           if (municipalWaterEntry.getConsumption() >= 324.8 && municipalWaterEntry.getConsumption() <=2424.8) {
//               monServiceCharge = 290;
//           }else if (municipalWaterEntry.getConsumption() >= 2828 && municipalWaterEntry.getConsumption() <= 4844 ) {
//               monServiceCharge = 575;
//           }else if (municipalWaterEntry.getConsumption() >= 5572 && municipalWaterEntry.getConsumption() <= 7588) {
//               monServiceCharge = 1150;
//           }else if (municipalWaterEntry.getConsumption() >= 7672 && municipalWaterEntry.getConsumption() <= 9688) {
//               monServiceCharge =1150;
//           }else if (municipalWaterEntry.getConsumption() >= 10544.8 && municipalWaterEntry.getConsumption() <= 18860.8) {
//               monServiceCharge = 1840;
//           }else if (municipalWaterEntry.getConsumption() >= 20104 && municipalWaterEntry.getConsumption() <= 45220) {
//               monServiceCharge = 2875;
//           }else if (municipalWaterEntry.getConsumption() >= 47236 && municipalWaterEntry.getConsumption() <= 89152) {
//               monServiceCharge = 4600;
//           }else if (municipalWaterEntry.getConsumption() >= 93744 && municipalWaterEntry.getConsumption() <= 177660) {
//               monServiceCharge = 8625;
//           }else if (municipalWaterEntry.getConsumption() >= 184184 && municipalWaterEntry.getConsumption() <= 352100) {
//               monServiceCharge = 14375;
//           }else if (municipalWaterEntry.getConsumption() >=368284 && municipalWaterEntry.getConsumption() <= 872200) {
//               monServiceCharge = 28750;
//           }else if(municipalWaterEntry.getConsumption() >= 904484 && municipalWaterEntry.getConsumption() <= 1744400) {
//               monServiceCharge = 57500;
//           }else {
//               monServiceCharge = 115000;
//           }
           if (municipalWaterEntry.getConsumption() >= 324.8 && municipalWaterEntry.getConsumption() <2424.8) {
               monServiceCharge = 290;
           }else if (municipalWaterEntry.getConsumption() >= 2424.8 && municipalWaterEntry.getConsumption() < 4844 ) {
               monServiceCharge = 575;
           }else if (municipalWaterEntry.getConsumption() >= 4844 && municipalWaterEntry.getConsumption() <7588) {
               monServiceCharge = 1150;
           }else if (municipalWaterEntry.getConsumption() >= 7588 && municipalWaterEntry.getConsumption() < 9688) {
               monServiceCharge =1150;
           }else if (municipalWaterEntry.getConsumption() >= 9688 && municipalWaterEntry.getConsumption() < 18860.8) {
               monServiceCharge = 1840;
           }else if (municipalWaterEntry.getConsumption() >= 18860.8 && municipalWaterEntry.getConsumption() < 45220) {
               monServiceCharge = 2875;
           }else if (municipalWaterEntry.getConsumption() >= 45220 && municipalWaterEntry.getConsumption() < 89152) {
               monServiceCharge = 4600;
           }else if (municipalWaterEntry.getConsumption() >= 89152 && municipalWaterEntry.getConsumption() < 177660) {
               monServiceCharge = 8625;
           }else if (municipalWaterEntry.getConsumption() >= 177660 && municipalWaterEntry.getConsumption() < 352100) {
               monServiceCharge = 14375;
           }else if (municipalWaterEntry.getConsumption() >=352100 && municipalWaterEntry.getConsumption() < 872200) {
               monServiceCharge = 28750;
           }else if(municipalWaterEntry.getConsumption() >= 872200 && municipalWaterEntry.getConsumption() <= 1744400) {
               monServiceCharge = 57500;
           }else {
               monServiceCharge = 115000;
           }
           consumptionm3 = ((municipalWaterEntry.getConsumption() / (112/100)) - monServiceCharge) / usageCharge;
       } else {
           consumptionm3 = municipalWaterEntry.getConsumption();
       }


        float ef_grid = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NATIONAL_GRID_EMISSION_FACTOR).getValue();
        float cf_mun = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CF_MUNICIPAL_WATER).getValue();
//        consumptionm3 += (consumptionLiters / 1000);
        MunicipalWaterCalcData calcData = new MunicipalWaterCalcData();
        calcData.setConsumption_m3(consumptionm3);
        calcData.setCf_municipal_water(cf_mun);
        calcData.setEf_grid_electricty(ef_grid);
        ResultDTO e1 = Calculator.emissionMunicipalWater(calcData);
        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", consumptionm3) );
        results.put("direct", Boolean.toString(false));
        results.put("ef_grid",String.format("%.5f",ef_grid) );
        results.put("cf_grid",String.format("%.5f", cf_mun) );
        results.put("unit", Integer.toString(municipalWaterEntry.getUnits()));
        return results;
    }

}
