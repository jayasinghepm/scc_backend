package com.climatesi.ghg.bo_controller;

import com.climatesi.ghg.bo_controller.controllers.implGeneral.TestController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission.*;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_calc.EmissionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors.EmissionFactorsStoreController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors.PubTransEmissionFactorsController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors.WasteDisposalFactorsControllers;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project.GHGProjectController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project.ProjectSummaryController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.insitution.InsstitutionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.master_data.MasterDataController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.mitigation.MitigationController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.notification.NotificationController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.report.ReportController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.summary_inputs.SummaryInputController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class GHGControllerFactory {

    private static Logger logger = Logger.getLogger(GHGControllerFactory.class.getName());
    private static InitialContext context;
    private static GHGControllerFactory ghgControllerFactory;


    private GHGControllerFactory() throws Exception {

    }

    public static GHGControllerFactory getInstance() throws GHGException {
        try {
            if (ghgControllerFactory == null) {
                ghgControllerFactory = new GHGControllerFactory();
                context = new InitialContext();
            }
            return ghgControllerFactory;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new GHGException("Error in getInstance of GHSControllerFactory:", ex);
        }
    }

    public TestController getTestController() throws Exception {
        try {
            Object lookupResult = context.lookup("java:module/TestController");
            if (lookupResult instanceof TestController) {
                return (TestController) lookupResult;
            } else {
                return null;
            }
        } catch(NamingException ex) {
            logger.error(ex.getMessage(), ex);
            throw new Exception("Error creating TestController Instance", ex);
        }
    }



    public ProjectSummaryController getProjectSummaryController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/ProjectSummaryController");
            if (lookupResult instanceof ProjectSummaryController) {
                return (ProjectSummaryController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating ProjectSummaryController", e);
        }

    }



    public VehicleOthersController getVehicleOthersController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/VehicleOthersController");
            if (lookupResult instanceof VehicleOthersController) {
                return (VehicleOthersController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating VehicleOthersController", e);
        }

    }


    public UserLoginController getUserLoginController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/UserLoginController");
            if (lookupResult instanceof UserLoginController) {
                return (UserLoginController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating UserLoginController", e);
        }

    }

    public AshTransportationController getAshTransportationController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/AshTransportationController");
            if (lookupResult instanceof AshTransportationController) {
                return (AshTransportationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating AshTransportationController", e);
        }

    }

    public ForkliftsController getForkliftsController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/ForkliftsController");
            if (lookupResult instanceof ForkliftsController) {
                return (ForkliftsController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating ForkliftsController", e);
        }

    }

    public FurnaceController getFurnaceController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/FurnaceController");
            if (lookupResult instanceof FurnaceController) {
                return (FurnaceController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating FurnaceController", e);
        }

    }

    public LorryTransportationController getLorryTransportationController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/LorryTransportationController");
            if (lookupResult instanceof LorryTransportationController) {
                return (LorryTransportationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating LorryTransportationController", e);
        }

    }


    public PaperWasteController getPaperWasteController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/PaperWasteController");
            if (lookupResult instanceof PaperWasteController) {
                return (PaperWasteController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating PaperWasteController", e);
        }

    }

    public PaidManagerVehicleController getPaidManagerVehicleController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/PaidManagerVehicleController");
            if (lookupResult instanceof PaidManagerVehicleController) {
                return (PaidManagerVehicleController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating PaidManagerVehicleController", e);
        }

    }

    public RawMaterialTransportController getRawMaterialTransportController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/RawMaterialTransportController");
            if (lookupResult instanceof RawMaterialTransportController) {
                return (RawMaterialTransportController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating RawMaterialTransportController", e);
        }

    }

    public OilAndGasTransportationController getOilAndGasTransportationController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/OilAndGasTransportationController");
            if (lookupResult instanceof OilAndGasTransportationController) {
                return (OilAndGasTransportationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating OilAndGasTransportationController", e);
        }

    }


    public SludgeTransportationController getSludgeTransportationController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/SludgeTransportationController");
            if (lookupResult instanceof SludgeTransportationController) {
                return (SludgeTransportationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating SludgeTransportationController", e);
        }

    }

    public SawDustTransportationController getSawDustTransportationController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/SawDustTransportationController");
            if (lookupResult instanceof SawDustTransportationController) {
                return (SawDustTransportationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating SawDustTransportationController", e);
        }

    }

    public StaffTransportationController getStaffTransportationController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/StaffTransportationController");
            if (lookupResult instanceof StaffTransportationController) {
                return (StaffTransportationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating StaffTransportationController", e);
        }

    }






    public LPGEntryController getLPGEntryController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/LPGEntryController");
            if (lookupResult instanceof LPGEntryController) {
                return (LPGEntryController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating LPGEntryController", e);
        }

    }
    public BgasEntryController getBGASEntryController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/BgasEntryController");
            if (lookupResult instanceof BgasEntryController) {
                return (BgasEntryController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating BgasEntryController", e);
        }

    }

    public BioMassEntryController getBioMassController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/BioMassEntryController");
            if (lookupResult instanceof BioMassEntryController) {
                return (BioMassEntryController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating BioMassEntryController", e);
        }

    }


    public SeaAirFreightController getSeaAirFreightController() throws GHGException {

        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/SeaAirFreightController");
            if (lookupResult instanceof SeaAirFreightController) {
                return (SeaAirFreightController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating SeaAirFreightController", e);
        }

    }


    public BusinessAirTravelController getBusinessAirTravelController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/BusinessAirTravelController");
            if (lookupResult instanceof BusinessAirTravelController) {
                return (BusinessAirTravelController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating BusinessAirTravelController", e);
        }
    }

    public ElectricityController getElectricityController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/ElectricityController");
            if (lookupResult instanceof ElectricityController) {
                return (ElectricityController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating ElectricityController", e);
        }
    }

    public FireExtingController getFireExtingController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/FireExtingController");
            if (lookupResult instanceof FireExtingController


            ) {
                return (FireExtingController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating FireExtingController", e);
        }
    }

    public GeneratorsController getGeneratorsController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/GeneratorsController");
            if (lookupResult instanceof GeneratorsController) {
                return (GeneratorsController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating GeneratorsController", e);
        }
    }

    public MunicipalWaterController getMunicipalWaterController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/MunicipalWaterController");
            if (lookupResult instanceof MunicipalWaterController) {
                return (MunicipalWaterController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating MunicipalWaterController", e);
        }
    }

    public RefrigerantsController getRefrigerantsController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/RefrigerantsController");
            if (lookupResult instanceof RefrigerantsController) {
                return (RefrigerantsController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating RefrigerantsController", e);
        }
    }

    public TransportLocalController getTransportLocalController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/TransportLocalController");
            if (lookupResult instanceof TransportLocalController) {
                return (TransportLocalController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating TransportLocalController", e);
        }
    }

    public VehicleController getVehicleController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/VehicleController");
            if (lookupResult instanceof VehicleController) {
                return (VehicleController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating VehicleController", e);
        }
    }

    public WasteDisposalController getWasteDisposalController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/WasteDisposalController");
            if (lookupResult instanceof WasteDisposalController) {
                return (WasteDisposalController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating WasteDisposalController", e);
        }
    }

    public WasteTransportController getWasteTransportController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/WasteTransportController");
            if (lookupResult instanceof WasteTransportController) {
                return (WasteTransportController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating WasteTransportController", e);
        }
    }

    public InsstitutionController getInsstitutionController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/InsstitutionController");
            if (lookupResult instanceof InsstitutionController) {
                return (InsstitutionController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating  InsstitutionController", e);
        }
    }

    public BusinessUserController getBusinessUserController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/BusinessUserController");
            if (lookupResult instanceof BusinessUserController) {
                return (BusinessUserController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating BusinessUserController", e);
        }
    }

    public MasterDataController getMasterDataController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/MasterDataController");
            if (lookupResult instanceof MasterDataController) {
                return (MasterDataController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating MasterDataController", e);
        }
    }

    public EmployeeCommutingController getEmpCommutingController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/EmployeeCommutingController");
            if (lookupResult instanceof EmployeeCommutingController) {
                return (EmployeeCommutingController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating EmployeeCommutingController", e);
        }
    }


    public EmissionFactorsStoreController getEmissionFactorsStoreController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/EmissionFactorsStoreController");
            if (lookupResult instanceof EmissionFactorsStoreController) {
                return (EmissionFactorsStoreController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating EmissionFactorsStoreController", e);
        }
    }

    public PubTransEmissionFactorsController getPubTransEmissionFactorsController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/PubTransEmissionFactorsController");
            if (lookupResult instanceof PubTransEmissionFactorsController) {
                return (PubTransEmissionFactorsController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating PubTransEmissionFactorsController", e);
        }
    }


    public WasteDisposalFactorsControllers getWasteDisposalFactorsControllers() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/WasteDisposalFactorsControllers");
            if (lookupResult instanceof WasteDisposalFactorsControllers) {
                return (WasteDisposalFactorsControllers) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating WasteDisposalFactorsControllers", e);
        }
    }

    public EmissionController getEmissionController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/EmissionController");
            if (lookupResult instanceof EmissionController) {
                return (EmissionController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating EmissionController", e);
        }
    }

    public GHGEmissionSummaryController getGHGEmissionSummaryController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/GHGEmissionSummaryController");
            if (lookupResult instanceof GHGEmissionSummaryController) {
                return (GHGEmissionSummaryController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating GHGEmissionSummaryController", e);
        }
    }

    public GHGProjectController getGHGProjectController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/GHGProjectController");
            if (lookupResult instanceof GHGProjectController) {
                return (GHGProjectController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating GHGProjectController", e);
        }
    }
    public SummaryInputController getSummaryInputController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/SummaryInputController");
            if (lookupResult instanceof SummaryInputController) {
                return (SummaryInputController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating SummaryInputController", e);
        }
    }
    public ReportController getReportController() throws GHGException {
        Object lookupResult = null;
        try {
            lookupResult = context.lookup("java:module/ReportController");
            if (lookupResult instanceof ReportController) {
                return (ReportController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating ReportController", e);
        }
    }

    public NotificationController getNotificationController() throws GHGException {
        Object lookupResult = null;
        try {
//            java:module/NotificationController
            lookupResult = context.lookup("java:module/NotificationController");
            if (lookupResult instanceof NotificationController) {
                return (NotificationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating NotificationController", e);
        }
    }
    public UserActController getUserActController() throws GHGException {
        Object lookupResult = null;
        try {
//            java:module/NotificationController
            lookupResult = context.lookup("java:module/UserActController");
            if (lookupResult instanceof UserActController) {
                return (UserActController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating UserActController", e);
        }
    }
    public MitigationController getMitigationController() throws GHGException {
        Object lookupResult = null;
        try {
//            java:module/NotificationController
            lookupResult = context.lookup("java:module/MitigationController");
            if (lookupResult instanceof MitigationController) {
                return (MitigationController) lookupResult;
            } else {
                return null;
            }
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new GHGException("Error creating MitigationController", e);
        }
    }
}
