package com.climatesi.ghg.bo_controller.controllers.implGeneral.mitigation;

import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.master_data.MasterDataController;
import com.climatesi.ghg.bo_controller.populators.MitigationProjectPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectActData;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectAnnualData;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectMonthlyActData;
import com.climatesi.ghg_mitication.api.MiticationFactory;
import com.climatesi.ghg_mitication.api.beans.MitigationProject;
import com.climatesi.ghg_mitication.api.beans.MitigationProjectMonthlyActivity;
import com.climatesi.ghg_mitication.api.facades.MitigationProjectManager;
import com.climatesi.ghg_mitication.api.facades.MitigationProjectMothlyActivityManager;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class MitigationController extends AbstractController {

    private static Logger logger = Logger.getLogger(MitigationController.class);

    private MitigationProjectManager mitigationProjectManager;
    private MitigationProjectMothlyActivityManager mitigationProjectMothlyActivityManager;


    @Override
    @PostConstruct
    public void initialize() {
        try {
            mitigationProjectManager = MiticationFactory.getInstance().getMitigationProjectFacade(em);
            mitigationProjectMothlyActivityManager = MiticationFactory.getInstance().getMitigationProjectMonthlyActivityFacade(em);
        }catch (Exception e) {
            logger.error("Failed to initialize facades", e);
        }
    }


    public MitigationProject findMitigationProjectById(int projectId) {
        return mitigationProjectManager.getEntityByKey(projectId);
    }

    public void deleteMitigationProject(int projectId) {
        //delete act data as well
        List<MitigationProjectMonthlyActivity> actDataList = mitigationProjectMothlyActivityManager.findByProjectId(projectId);
        if (actDataList != null) {
            Set<Long> ids = actDataList.stream().map(data -> data.getId()).collect(Collectors.toSet());
            deleteMitigationActDatas(ids);
        }
        mitigationProjectManager.delete(projectId);

    }

    private void deleteMitigationActDatas(Set<Long> ids) {
        for (long id : ids)  {
           mitigationProjectMothlyActivityManager.delete(id);
        }
    }

    public MitigationProject updateMitigationProject(MitigationProject project) {
         mitigationProjectManager.updateEntity(project);
         return project;
    }

    public MitigationProjectMonthlyActivity findActDataById(long id) {
        return mitigationProjectMothlyActivityManager.getEntityByKey(id);
    }

    public MitigationProjectMonthlyActivity updateMitigationActData(MitigationProjectMonthlyActivity activity) {
        mitigationProjectMothlyActivityManager.updateEntity(activity);
        return activity;
    }

    public void deleteMitigationActData(long id) {
        mitigationProjectMothlyActivityManager.delete(id);
    }

    public List<MitigationProjectActData> getActDataByCompany(int comId) {
//        List<MitigationProjectMonthlyActivity> rawData=  mitigationProjectMothlyActivityManager.getDataByCompany(comId);
//        if (rawData == null) {
//            return new ArrayList<>();
//        }
        List<MitigationProject> mitigationProjects = mitigationProjectManager.getProjectsByCompany(comId);
        if (mitigationProjects == null) {
            mitigationProjects = new ArrayList<>();
        }
        Set<Integer> projectIds = mitigationProjects.stream().map(project -> project.getProjectId()).collect(Collectors.toSet()); //rawData.stream().map(actData -> actData.getProjectId()).collect(Collectors.toSet());

        List<MitigationProjectActData> results = new ArrayList<>();


        for (Integer id : projectIds) {

            MitigationProject project = mitigationProjectManager.getEntityByKey(id);
            if (project == null) continue;

            MitigationProjectActData actData = new MitigationProjectActData();
            actData.setProjectName(project.getName());
            actData.setProjectId(project.getProjectId());
            actData.setProjectType(project.getProjectType());

            List<MitigationProjectMonthlyActivity> actDataList = mitigationProjectMothlyActivityManager.findByProjectId(id);

            Set<String> years= actDataList.stream().filter(data -> data.getProjectId() == id).map(data -> data.getYear()).sorted().collect(Collectors.toSet());
            if (years == null) {
                continue;
            }

            List<MitigationProjectAnnualData> annualDataList = new ArrayList<>();
            for (String year : years) {
                MitigationProjectAnnualData annualData = new MitigationProjectAnnualData();
                annualData.setComId(comId);
                annualData.setProjectName(project.getName());
                annualData.setProjectId(project.getProjectId());
                annualData.setYear(year);

                List<MitigationProjectMonthlyActivity> monthlyActivities  = actDataList.stream().filter(data -> data.getYear().equals(year)).sorted((left, right) ->  {
                    return left.getMonth() - right.getMonth();
                }).collect(Collectors.toList());
                if (monthlyActivities == null) {
                    monthlyActivities = new ArrayList<>();
                }
                List<MitigationProjectMonthlyActData> monthlyActDataList = monthlyActivities.stream().map(data -> {
                    MitigationProjectMonthlyActData dto = new MitigationProjectMonthlyActData();
                    dto.setValue(data.getValue());
                    dto.setEmissionSaved(data.getSavedEmission());
                    dto.setProjectId(project.getProjectId());
                    dto.setYear(year);
                    dto.setMonth(data.getMonth());

                    dto.setUnit(data.getUnit());
                    dto.setId(data.getId());

                    return dto;
                }).collect(Collectors.toList());

                annualData.setMonthlyData(monthlyActDataList);

                annualDataList.add(annualData);

            }

            actData.setAnnualData(annualDataList);

            results.add(actData);
        }

        return results;



    }

    public List<MitigationProjectDTO> getMitigationProjects(int comId) {
        List<MitigationProject> projects = mitigationProjectManager.getProjectsByCompany(comId);
        if (projects == null) {
            return new ArrayList<>();
        }
        List<MitigationProjectDTO> dtos = projects.stream().map(bean  -> MitigationProjectPopulator.getInstance().popuplateDTO(bean)).collect(Collectors.toList());

        return dtos;
    }

    public List<MitigationProjectDTO> getMitigationProjectsByBranch(int branchId) {
        List<MitigationProject> projects = mitigationProjectManager.getProjectsByBranch(branchId);
        if (projects == null) {
            return new ArrayList<>();
        }
        List<MitigationProjectDTO> dtos = projects.stream().map(bean  -> MitigationProjectPopulator.getInstance().popuplateDTO(bean)).collect(Collectors.toList());

        return dtos;
    }

    public List<MitigationProjectActData> getActDataByBranch(int branchId) {
        List<MitigationProjectMonthlyActivity> rawData=  mitigationProjectMothlyActivityManager.getDataByBranch(branchId);
        if (rawData == null) {
            return new ArrayList<>();
        }
        Set<Integer> projectIds = rawData.stream().map(actData -> actData.getProjectId()).collect(Collectors.toSet());

        List<MitigationProjectActData> results = new ArrayList<>();

        for (Integer id : projectIds) {

            MitigationProject project = mitigationProjectManager.getEntityByKey(id);
            if (project == null) continue;

            MitigationProjectActData actData = new MitigationProjectActData();
            actData.setProjectName(project.getName());
            actData.setProjectId(project.getProjectId());
            actData.setProjectType(project.getProjectType());

            Set<String> years= rawData.stream().filter(data -> data.getProjectId() == id).map(data -> data.getYear()).sorted().collect(Collectors.toSet());
            if (years == null) {
                continue;
            }

            List<MitigationProjectAnnualData> annualDataList = new ArrayList<>();
            for (String year : years) {
                MitigationProjectAnnualData annualData = new MitigationProjectAnnualData();
                annualData.setComId(branchId);
                annualData.setProjectName(project.getName());
                annualData.setProjectId(project.getProjectId());
                annualData.setYear(year);

                List<MitigationProjectMonthlyActivity> monthlyActivities  = mitigationProjectMothlyActivityManager.findByProjectIdAndSortByMonth(project.getProjectId());
                if (monthlyActivities == null) {
                    monthlyActivities = new ArrayList<>();
                }
                List<MitigationProjectMonthlyActData> monthlyActDataList = monthlyActivities.stream().map(data -> {
                    MitigationProjectMonthlyActData dto = new MitigationProjectMonthlyActData();
                    dto.setValue(data.getValue());
                    dto.setEmissionSaved(data.getSavedEmission());
                    dto.setProjectId(project.getProjectId());
                    dto.setYear(year);
                    dto.setMonth(data.getMonth());
                    dto.setUnit(data.getUnit());

                    return dto;
                }).collect(Collectors.toList());

                annualData.setMonthlyData(monthlyActDataList);

                annualDataList.add(annualData);

            }

            results.add(actData);
        }

        return results;

    }
}
