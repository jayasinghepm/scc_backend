package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;


import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.BioMassPopulator;
import com.climatesi.ghg.bo_controller.populators.ForkliftsPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ForkliftsEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.ForkLiftsEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.ForkliftsEntryManagerFacade;
import com.climatesi.ghg.emission_source.implGeneral.facades.ForkliftsEntryManagerFacade;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class ForkliftsController   extends AbstractController {

    private static final Logger logger = Logger.getLogger(ForkliftsController.class);


    private ForkliftsEntryManagerFacade forkliftsEntryManagerFacade;
    private EmissionFactorsManager emissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {

        try {
            forkliftsEntryManagerFacade = (ForkliftsEntryManagerFacade) EmsissionSourceFactory.getInstance().getForkliftsEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing MunicipalWaterController", e);
        }
    }


    public ListResult<Forklifts> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return forkliftsEntryManagerFacade.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public Forklifts edit(ForkliftsEntryDTO dto) throws GHGException {
        Forklifts entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                ForkliftsPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.getIsDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) forkliftsEntryManagerFacade.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new ForkLiftsEntryBean();
                ForkliftsPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emission(entry));
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) forkliftsEntryManagerFacade.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public Forklifts findById(Integer id) {
        return forkliftsEntryManagerFacade.getEntityByKey(id);
    }

    public HashMap<String, String> emission(Forklifts entry) {

        float consumptionLiters = entry.getFuelConsumption();
        float p_ncv = 0.043f;
        float p_efCo2 = 74.1f;
        float p_efCh4 =  0.1092f;
        float p_efN20 = 1.0335f;
        float p_density  = 0.84f;

        float d_ncv = 0.043f;
        float d_efCo2 = 69.3f;
        float d_efCh4 =  1.4f;
        float d_efN20 = 0.53f;
        float d_density  = 0.725f;

        p_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL).getValue();
        p_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_GASOLINE).getValue();
        p_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_GASOLINE).getValue();
        p_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_GASOLINE).getValue();
        p_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL).getValue();

        d_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();
        d_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_DIESEL).getValue();
        d_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_DIESEL).getValue();
        d_efN20 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_DIESEL).getValue();
        d_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();

        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();


        ResultDTO e1 =  new ResultDTO(); // avoid nul pointer exception

        if (entry.getFuleType() == 2) {
            e1 =  Calculator.emissionVehicles(
                    consumptionLiters,
                    d_ncv,
                    d_density,
                    d_efCo2,
                    d_efCh4,
                    d_efN20,
                    gwp_co2,
                    gwp_ch4,
                    gwp_n20
            );

        }else if (entry.getFuleType() == 1) {
            e1 =  Calculator.emissionVehicles(
                    consumptionLiters,
                    p_ncv,
                    p_density,
                    p_efCo2,
                    p_efCh4,
                    p_efN20,
                    gwp_co2,
                    gwp_ch4,
                    gwp_n20
            );
        }


        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", consumptionLiters * 0.001) );

        results.put("direct", Boolean.toString(false));
        results.put("isPetrol", Boolean.toString(entry.getFuleType() == 1 ? true : false));
        results.put("density",String.format("%.5f", entry.getFuleType() == 1 ? p_density : d_density) );
        results.put("ncv",String.format("%.5f", entry.getFuleType() == 1 ? p_ncv : d_ncv) );
        results.put("efCO2",String.format("%.5f", entry.getFuleType() == 1 ? p_efCo2 : d_efCo2) );
        results.put("efCH4",String.format("%.5f", entry.getFuleType() == 1 ? p_efCh4 : d_efCh4) );
        results.put("efN2O",String.format("%.5f", entry.getFuleType() == 1 ? p_efN20 : d_efN20) );
        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20) );

        results.put("te",String.format("%.5f",e1.totalEnergy) );
        return results;
    }

}
