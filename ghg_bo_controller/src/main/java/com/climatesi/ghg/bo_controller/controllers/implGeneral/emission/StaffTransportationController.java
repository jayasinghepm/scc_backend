package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;


import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.BioMassPopulator;
import com.climatesi.ghg.bo_controller.populators.StaffTransportationPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.StaffTransportationDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.StaffTransportation;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.StaffTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.BioMassEntryManagerFacade;
import com.climatesi.ghg.emission_source.implGeneral.facades.StaffTransportationEntryManagerFacade;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class StaffTransportationController   extends AbstractController {

    private static final Logger logger = Logger.getLogger(StaffTransportationController.class);


    private StaffTransportationEntryManagerFacade staffTransportationEntryManagerFacade;
    private EmissionFactorsManager emissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {

        try {
            staffTransportationEntryManagerFacade = (StaffTransportationEntryManagerFacade) EmsissionSourceFactory.getInstance().getStaffTransportationEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing Staff Transportation controller", e);
        }
    }


    public ListResult<StaffTransportation> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return staffTransportationEntryManagerFacade.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public StaffTransportation edit(StaffTransportationDTO dto) throws GHGException {
        StaffTransportation entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                StaffTransportationPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.getIsDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) staffTransportationEntryManagerFacade.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new StaffTransportationBean();
                StaffTransportationPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emission(entry));
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) staffTransportationEntryManagerFacade.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public StaffTransportation findById(Integer id) {
        return staffTransportationEntryManagerFacade.getEntityByKey(id);
    }

    public HashMap<String, String> emission(StaffTransportation entry) {

        float consumptionLiters = entry.getFuelConsumption();
//        float consumptionLiters = entry.getDistance() * entry.getFuelEconomy();
        float d_ncv = 0.043f;
        float d_efCo2 = 74.1f;
        float d_efCh4 = 0.1092f;
        float d_efN20 = 1.0335f;
        float d_density = 0.84f;

        float p_ncv = 0.043f;
        float p_efCo2 = 69.3f;
        float p_efCh4 = 0.924f;
        float p_efN20 = 0.848f;
        float p_density = 0.7525f;


        p_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL).getValue();
        p_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE).getValue();
        p_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE).getValue();
        p_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE).getValue();
        p_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL).getValue();

        d_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();
        d_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL).getValue();
        d_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL).getValue();
        d_efN20 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL).getValue();
        d_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();

        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();




        ResultDTO e1 = new ResultDTO();
        if (entry.getFuelType() == 1) {
            e1 = Calculator.emissionVehicles(
                    consumptionLiters,
                    p_ncv,
                    p_density,
                    p_efCo2,
                    p_efCh4,
                    p_efN20,
                    gwp_co2,
                    gwp_ch4,
                    gwp_n20
            );
        }else if (entry.getFuelType() == 2) {
            e1 = Calculator.emissionVehicles(
                    consumptionLiters,
                    d_ncv,
                    d_density,
                    d_efCo2,
                    d_efCh4,
                    d_efN20,
                    gwp_co2,
                    gwp_ch4,
                    gwp_n20
            );
        }
        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", consumptionLiters * 0.001) );
        results.put("direct", Boolean.toString(false)); //todo

        results.put("ncv",String.format("%.5f",entry.getFuelType() == 2 ? d_ncv : p_ncv) );
        results.put("efCO2",String.format("%.5f",entry.getFuelType() == 2 ? d_efCo2 : p_efCo2) );
        results.put("efCH4",String.format("%.5f", entry.getFuelType() == 2 ? d_efCh4 : p_efCh4) );
        results.put("efN20",String.format("%.5f", entry.getFuelType() == 2 ? d_efN20 : p_efN20) );
        results.put("density",String.format("%.5f", entry.getFuelType() == 2 ? d_density : p_density) );
        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20) );
        results.put("distance",String.format("%.5f", entry.getDistance()) );
        results.put("fuel_economy",String.format("%.5f", entry.getFuelEconomy()) );
        results.put("te",String.format("%.5f",e1.totalEnergy) );
        return results;
    }

}

