package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.LoginResponseStatus;
import com.climatesi.ghg.api.beans.AuthReply;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.AuthenticateResponseBean;
import org.jboss.logging.Logger;

public class AuthMessagePopulator {
    private static Logger logger = Logger.getLogger(AuthMessagePopulator.class);

    /**
     * Populate authentication client response using question authentication reply
     *
     * @param authReply is question module authentication reply
     * @return response message body in client protocol
     */
    public AuthenticateResponseBean populateAuthenticationResponse(AuthReply authReply) {
        logger.info("Populating authentication response message. Session Id >> " + authReply.getSessionId() + " loginStatus: " + authReply.getLoginStatus());

        AuthenticateResponseBean response = new AuthenticateResponseBean();
        response.setAuthenticationStatus(authReply.getLoginStatus());
        response.setNarration(authReply.getNarration());
        response.setSessionId(authReply.getSessionId());

        if (authReply.getLoginStatus() == LoginResponseStatus.SUCCESS.getCode()) {
            response.setUserId(authReply.getUserId());
            response.setUserName(authReply.getUserName());
            response.setUserFirstName(authReply.getFirstName());
            response.setUserLastName(authReply.getLastName());
            response.setUserType(authReply.getUserType());
            response.setBranchId(authReply.getBranchId());
            response.setCompanyId(authReply.getCompanyId());
            response.setClientId(authReply.getClientId());
            response.setAdminId(authReply.getAdminId());
            response.setDataEntryUserId(authReply.getDataEntryUserId());
            response.setCadminId(authReply.getCadminId());
            response.setIsFirstime(authReply.getIsFirstime());
            response.setCadminStatus(authReply.getCadminStatus());
            response.setCadminStatusMsg(authReply.getCadminStatusMsg());
            response.setExistActiveProject(authReply.getExistActiveProject());
            response.setDataEntryExpired(authReply.getDataEntryExpired());
            response.setProjectStatus(authReply.getProjectStatus());
            response.setCompanyName(authReply.getCompanyName());
            response.setBranchName(authReply.getBranchName());
            response.setIsMasterAdmin(authReply.getIsMasterAdmin());
            response.setPages(authReply.getPages());
            response.setEmSources(authReply.getEmSources());
            response.setAllowedEmissionSources(authReply.getAllowedEmissionSources());
            response.setDisableFormulas(authReply.getDisableFormulas());


//            Todo : uncomment after created entitilement
            if (authReply.getEntitlements() != null) {
                response.setUserEntitlementList(authReply.getEntitlements());
            }
        }

        return response;
    }

//    public CustomerAuthResponseBean populateCustomerAuthenticationResponse(CustomerAuthReply authReply) {
//        logger.info("Populating authentication response message. Session Id >> " + authReply.getSessionId());
//
//        CustomerAuthResponseBean response = new CustomerAuthResponseBean();
//        response.setAuthenticationStatus(authReply.getLoginStatus());
//        response.setNarration(authReply.getNarration());
//        response.setSessionId(authReply.getSessionId());
//        response.setCustomerId(authReply.getCustomerId());
//        response.setInstituteId(authReply.getInstId());
//        response.setCustomerNumber(authReply.getCustomerNumber());
//        response.setInstitutionCode(authReply.getInstCode());
//        response.setAnsweredPaperId(authReply.getPaperIdAnswered());
//        response.setPaperId(authReply.getCurrentPaperId());
//        response.setLastPaperAnsDate(Converter.dateToyyyyMMdd(authReply.getPaperAnsweredDate()));
//        response.setCashAccountNumber(authReply.getCashAccountNumber());
//        response.setCashBalance(authReply.getCashBalance());
//        response.setCurrency(authReply.getCurrency());
//        return response;
//    }
//
//    public CustomerAuthResponseBean populateNotRegisterAuthResponse(CustomerAuthReply authReply) {
//        logger.info("Populating authentication response for not registered customer " + authReply.getSessionId());
//
//        CustomerAuthResponseBean response = new CustomerAuthResponseBean();
//        response.setAuthenticationStatus(authReply.getLoginStatus());
//        return response;
//    }
}
