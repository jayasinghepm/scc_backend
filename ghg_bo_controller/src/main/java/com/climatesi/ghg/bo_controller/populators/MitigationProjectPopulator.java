package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.ManageMitigationProjectDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectDTO;
import com.climatesi.ghg_mitication.api.beans.MitigationProject;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectBean;
import org.jboss.logging.Logger;

public class MitigationProjectPopulator {

    private static Logger logger = Logger.getLogger(MitigationProjectPopulator.class);

    private static MitigationProjectPopulator instance;

    public static MitigationProjectPopulator getInstance() {
        if (instance == null) {
            instance = new MitigationProjectPopulator();
        }
        return instance;
    }

    public MitigationProject populate(ManageMitigationProjectDTO dto, MitigationProject project) {
        //new
        if (project == null) {
            project = new MitigationProjectBean();
            project.setBranchId(dto.getBranchId());
            project.setComId(dto.getComId());
            project.setProjectType(dto.getType());
            project.setDescription(dto.getDescription());
            project.setName(dto.getName());
            project.setCreatedAt(System.currentTimeMillis());
            project.setEmissionSaved(0);
            project.setCreatedAt(System.currentTimeMillis());


        }else  { //edit
            if (dto.getDescription() != null) {
                project.setDescription(dto.getDescription());
            }
            if (dto.getName() != null) {
                project.setName(dto.getName());

            }
            project.setUpdatedAt(System.currentTimeMillis());
        }
        return project;

    }


    public MitigationProjectDTO popuplateDTO(MitigationProject bean) {
        MitigationProjectDTO dto = new MitigationProjectDTO();
        dto.setBranchId(bean.getBranchId());
        dto.setProjectId(bean.getProjectId());
        dto.setDescription(bean.getDescription());
        dto.setName(bean.getName());
        dto.setComId(bean.getComId());
        dto.setType(bean.getProjectType());
        return dto;
    }
}
