package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.AirTravelPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.BusinessAirTravelEntry;
import com.climatesi.ghg.emission_source.api.facades.BusinessAirTravelEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.BusinessAirTravelEntryBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;

@Stateless
public class BusinessAirTravelController extends AbstractController {

    private static final Logger logger = Logger.getLogger(BusinessAirTravelController.class);
    private BusinessAirTravelEntryManager businessAirTravelEntryManager;



    @PostConstruct
    @Override
    public void initialize() {

        try {
            businessAirTravelEntryManager = EmsissionSourceFactory.getInstance().getBusinessAirTravelEntryManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing Airtravel controller", e);
        }
    }

    public ListResult<BusinessAirTravelEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return businessAirTravelEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public BusinessAirTravelEntry editWasteTransportEntry(BusinessAirTravelEntryDTO dto) throws GHGException {
        BusinessAirTravelEntry entry;

        String status = null;
        try {
            //        update
            if (dto.getAirTravelEntryId() > 0) {
                entry = findById(dto.getAirTravelEntryId());
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
                AirTravelPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id
                status = (String) businessAirTravelEntryManager.updateEntity(entry);

//         add new
            } else {
                entry = new BusinessAirTravelEntryBean();
                AirTravelPopulator.getInstance().populate(entry, dto);
                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) businessAirTravelEntryManager.addEntity(entry);
            }

//        Todo check status and throw exceptions

        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public BusinessAirTravelEntry findById(Integer id) {
        return businessAirTravelEntryManager.getEntityByKey(id);
    }
}
