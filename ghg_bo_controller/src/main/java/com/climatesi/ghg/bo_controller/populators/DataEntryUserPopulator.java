package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.DataEntryUserDTO;
import com.climatesi.ghg.business_users.api.beans.DataEntryUser;
import org.jboss.logging.Logger;

public class DataEntryUserPopulator {

    private static Logger logger = Logger.getLogger(DataEntryUserPopulator.class);
    private static DataEntryUserPopulator instance;

    public static DataEntryUserPopulator getInstance() {
        if (instance == null) {
            instance = new DataEntryUserPopulator();
        }
        return instance;
    }

    public DataEntryUserDTO populateDTO(DataEntryUser dataEntryUser) {
        DataEntryUserDTO dto = new DataEntryUserDTO();
        try {
            dto.setStatus(dataEntryUser.getStatus());
            dto.setStatusMessage(dataEntryUser.getStatusMessage());
            dto.setId(dataEntryUser.getId());
            dto.setFirstName(dataEntryUser.getFirstName());
            dto.setLastName(dataEntryUser.getLastName());
            dto.setTitle(dataEntryUser.getTitle());
            dto.setEmail(dataEntryUser.getEmail());
            dto.setTelephoneNo(dataEntryUser.getTelephoneNo());
            dto.setMobileNo(dataEntryUser.getMobileNo());
            dto.setPosition(dataEntryUser.getPosition());
            dto.setCompanyId(dataEntryUser.getCompanyId());
            dto.setBranchId(dataEntryUser.getBranchId());
            dto.setEntitlements(dataEntryUser.getEntitlements());
            dto.setUserId(dataEntryUser.getUserId());
            dto.setCompany(dataEntryUser.getCompany());
        } catch (NullPointerException e) {
            logger.error("Error in populating,", e);
        }
        return dto;
    }

    public DataEntryUser populate(DataEntryUser dataEntryUser, DataEntryUserDTO dto) {
        try {
            if (dto.getId() > 0) {
                dataEntryUser.setId(dto.getId());
                if (dto.getIsDeleted() == 1) {
                    dataEntryUser.setIsDeleted(dto.getIsDeleted());
                    return dataEntryUser;
                }

            }
            dataEntryUser.setStatus(dto.getStatus() == 0 ? dataEntryUser.getStatus() : dto.getStatus());
            dataEntryUser.setStatusMessage(dto.getStatusMessage() == null ? dataEntryUser.getStatusMessage() : dto.getStatusMessage());
            dataEntryUser.setFirstName(dto.getFirstName() == null ? dataEntryUser.getFirstName() : dto.getFirstName());
            dataEntryUser.setLastName(dto.getLastName() == null ? dataEntryUser.getLastName() : dto.getLastName());
            dataEntryUser.setTitle(dto.getTitle() == null ? dataEntryUser.getTitle() : dto.getTitle());
            dataEntryUser.setTelephoneNo(dto.getTelephoneNo() == null ? dataEntryUser.getTelephoneNo() : dto.getTelephoneNo());
            dataEntryUser.setMobileNo(dto.getMobileNo() == null ? dataEntryUser.getMobileNo() : dto.getMobileNo());
            dataEntryUser.setPosition(dto.getPosition() == null ? dataEntryUser.getPosition() : dto.getPosition());
            dataEntryUser.setBranchId(dto.getBranchId() == 0 ? dataEntryUser.getBranchId() : dto.getBranchId());
            dataEntryUser.setCompanyId(dto.getCompanyId() == 0 ? dataEntryUser.getCompanyId() : dto.getCompanyId());
            dataEntryUser.setEntitlements(dto.getEntitlements() == null ? dataEntryUser.getEntitlements() : dto.getEntitlements());
            dataEntryUser.setUserId(dto.getUserId() == 0 ? dataEntryUser.getUserId() : dto.getUserId());
            dataEntryUser.setEmail(dto.getEmail() == null ? dataEntryUser.getEmail() : dto.getEmail());
            dataEntryUser.setCompany(dto.getCompany() == null ? dataEntryUser.getCompany() : dto.getCompany());
        } catch (NullPointerException e) {
            logger.error("Error in populating", e);
        }
        return dataEntryUser;
    }

}
