package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.constants.PublicTransport_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.api.facade.PublicTransportEmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.EmployeeCommutingPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.EmpCommutingDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.EmpCommutingEntry;
import com.climatesi.ghg.emission_source.api.facades.EmployeCommutingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.EmployeeCommutingEntryBean;
import com.climatesi.ghg.implGeneral.data.CompanyOwnedVehicleCalcData;
import com.climatesi.ghg.implGeneral.data.EmpCommuPrivateCalcData;
import com.climatesi.ghg.implGeneral.data.EmpCommuPublicCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class EmployeeCommutingController extends AbstractController {

    private static Logger logger = Logger.getLogger(EmployeeCommutingController.class);
    private EmployeCommutingEntryManager employeCommutingEntryManager;
    private EmissionFactorsManager emissionFactorsManager;
    private PublicTransportEmissionFactorsManager publicTransportEmissionFactorsManager;



    @PostConstruct
    @Override
    public void initialize() {
        try {
            employeCommutingEntryManager = EmsissionSourceFactory.getInstance().getEmployeeCommEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);
            publicTransportEmissionFactorsManager = EmissionCalcFacotory.getInstance().getPublicTransportEmissionFactorsManager(em);
        } catch (Exception e) {
            logger.error("Error in intializing Employee commuting controle", e);
        }
    }

    public ListResult<EmpCommutingEntry> getFilteredResult(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return employeCommutingEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public EmpCommutingEntry findById(int id) {
        return employeCommutingEntryManager.getEntityByKey(id);
    }

    public EmpCommutingEntry editEmpCommutingEntry(EmpCommutingDTO dto) throws GHGException {
        EmpCommutingEntry entry;

        String status = null;
        try {
            //        update
            if (dto.getId() > 0) {
                entry = findById(dto.getId());
                if (entry == null) {
                    throw new GHGException("EmpCommutingEntry is not found");
                }
                EmployeeCommutingPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id
                if (entry != null) {
                    if (entry.isDeleted() !=1) {
                        entry.setEmissionDetails(getEmpCommutingEntryEmission(entry));
                    }


//                    entry.setAnnualEmission(emission);
                }
                status = (String) employeCommutingEntryManager.updateEntity(entry);

//         add new
            } else {
                entry = new EmployeeCommutingEntryBean();
                EmployeeCommutingPopulator.getInstance().populate(entry, dto);
                if (entry != null) {
                    entry.setEmissionDetails(getEmpCommutingEntryEmission(entry));
                }
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) employeCommutingEntryManager.addEntity(entry);
            }

//        Todo check status and throw exceptions

        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public HashMap<String, String> getEmpCommutingEntryEmission(EmpCommutingEntry o) {



        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();

        float ef_co2_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE).getValue();
        float ef_co2_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL).getValue();
        float ef_ch4_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL).getValue();
        float ef_ch4_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE).getValue();
        float ef_n20_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE).getValue();
        float ef_n20_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL).getValue();

        float den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();
        float net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();

        float den_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL).getValue();
        float net_cal_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL).getValue();

        float kg_co2_van = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.VAN_DIESEL).getCo2_per_km();
        float kg_co2_bus_medium = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.MEDIUM_BUS_DIESEL).getCo2_per_km();
        float kg_co2_bus = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.BUS_DIESEL).getCo2_per_km();
        float kg_co2_train = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.TRAIN).getCo2_per_km();

        EmpCommuPrivateCalcData prvCalcData = new EmpCommuPrivateCalcData();
        EmpCommuPublicCalcData pubCalcData = new EmpCommuPublicCalcData();

        float emission = 0;
        float km_own = 0;
        float km_public = 0;
        float diesel = 0;
        float petrol = 0;
        float km_com = 0;
        HashMap<String, String> results = new HashMap<>();
        try {
            // pubilc
//            up
            ResultDTO e_pub_up = null;
            switch (o.getPublicTransMode_UP()) {
                case PublicTransport_Keys.VAN_DIESEL: {
                    km_public += o.getPublicTrans_distance_UP();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_van);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_UP());
                    e_pub_up = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_van", String.format("%.20f", kg_co2_van));
                    results.put("ef_pub_up", String.format("%.20f", kg_co2_van));
                    break;
                }
                case PublicTransport_Keys.MEDIUM_BUS_DIESEL: {
                    km_public += o.getPublicTrans_distance_UP();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_bus_medium);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_UP());
                    e_pub_up = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_bus_medium", String.format("%.20f", kg_co2_bus_medium));
                    results.put("ef_pub_up", String.format("%.20f", kg_co2_bus_medium));
                    break;
                }
                case PublicTransport_Keys.BUS_DIESEL: {
                    km_public += o.getPublicTrans_distance_UP();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_bus);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_UP());
                    e_pub_up = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_bus",String.format("%.20f", kg_co2_bus));
                    results.put("ef_pub_up", String.format("%.20f", kg_co2_bus));
                    break;
                }
                case PublicTransport_Keys.TRAIN: {
                    km_public += o.getPublicTrans_distance_UP();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_train);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_UP());
                    e_pub_up = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_train", String.format("%.20f", kg_co2_train));
                    results.put("ef_pub_up",String.format("%.20f", kg_co2_train));
                    break;
                }
                default:{
                    e_pub_up = new ResultDTO();
                }
            }
            results.put("e_public_up",String.format("%.20f", e_pub_up.tco2e * o.getNoOfWorkingDays()));
            ResultDTO e_pub_down  = null;
            switch (o.getPublicTransMode_DOWN()) {
                case PublicTransport_Keys.VAN_DIESEL: {
                    km_public += o.getPublicTrans_distance_DOWN();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_van);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_DOWN());
                    e_pub_down = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_van", String.format("%.20f", kg_co2_van));
                    results.put("ef_pub_down", String.format("%.20f", kg_co2_van));
                    break;
                }
                case PublicTransport_Keys.MEDIUM_BUS_DIESEL: {
                    km_public += o.getPublicTrans_distance_DOWN();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_bus_medium);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_DOWN());
                    e_pub_down = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_bus_medium", String.format("%.20f", kg_co2_bus_medium) );
                    results.put("ef_pub_down", String.format("%.20f", kg_co2_bus_medium) );
                    break;
                }
                case PublicTransport_Keys.BUS_DIESEL: {
                    km_public += o.getPublicTrans_distance_DOWN();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_bus);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_DOWN());
                    e_pub_down = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_bus", String.format("%.20f", kg_co2_bus) );
                    results.put("ef_pub_down", String.format("%.20f", kg_co2_bus) );
                    break;
                }
                case PublicTransport_Keys.TRAIN: {
                    km_public += o.getPublicTrans_distance_DOWN();
                    pubCalcData.setEf_public_vehicle_per_passenger_kilometer(kg_co2_train);
                    pubCalcData.setUp_down_distance(o.getPublicTrans_distance_DOWN());
                    e_pub_down = Calculator.emissionPublicTransport(pubCalcData) ;
                    results.put("kg_co2_train", String.format("%.20f", kg_co2_train) );
                    results.put("ef_pub_down",  String.format("%.20f", kg_co2_train));
                    break;
                }
                default:{
                    e_pub_down = new ResultDTO();
                }
            }
            results.put("e_public_down", String.format("%.20f", e_pub_down.tco2e * o.getNoOfWorkingDays()));
//            own
            ResultDTO e_own_up = null;
            switch (o.getOwnTrans_fuelType_UP()) {
                case 1: {
                    petrol += o.getOwnTrans_fuelEconomy_UP() == 0 ? 0 : ((o.getOwnTrans_distance_UP() / o.getOwnTrans_fuelEconomy_UP())/1000);
                    prvCalcData.setNumber_of_trips(o.getNoOfWorkingDays());
                    prvCalcData.setUp_down_distance_km(o.getOwnTrans_distance_UP());
                    prvCalcData.setFuel_economy_km_l(o.getOwnTrans_fuelEconomy_UP());
                    prvCalcData.setDensity_fuel_t_m3(den_petrol);
                    prvCalcData.setNet_caloric_fuel(net_cal_petrol);
                    prvCalcData.setEf_n2o(ef_n20_petrol);
                    prvCalcData.setEf_co2(ef_co2_petrol);
                    prvCalcData.setEf_ch4(ef_ch4_petrol);
                    prvCalcData.setGwp_co2(gwp_co2);
                    prvCalcData.setGwp_ch4(gwp_ch4);
                    prvCalcData.setGwp_n2o(gwp_n20);
                    e_own_up = Calculator.emissionPrivateTransport(prvCalcData);
                    results.put("den_up", String.format("%.20f", den_petrol) );
                    results.put("net_cal_up", String.format("%.20f", net_cal_petrol) );
                    results.put("ef_n20_up",  String.format("%.20f", ef_n20_petrol));
                    results.put("ef_co2_up",  String.format("%.20f", ef_co2_petrol));
                    results.put("ef_co2_up", String.format("%.20f", ef_co2_petrol) );
                    results.put("ef_ch4_up", String.format("%.20f", ef_ch4_petrol) );
                    results.put("gwp_co2", String.format("%.20f", gwp_co2) );
                    results.put("gwp_ch4", String.format("%.20f", gwp_ch4) );
                    results.put("gwp_n20", String.format("%.20f", gwp_n20) );

                    break;
                }
                case 2: {
                    diesel += o.getOwnTrans_fuelEconomy_UP() == 0 ? 0 :((o.getOwnTrans_distance_UP() / o.getOwnTrans_fuelEconomy_UP())/1000);
                    prvCalcData.setNumber_of_trips(o.getNoOfWorkingDays());
                    prvCalcData.setUp_down_distance_km(o.getOwnTrans_distance_UP());
                    prvCalcData.setFuel_economy_km_l(o.getOwnTrans_fuelEconomy_UP());
                    prvCalcData.setGwp_co2(gwp_co2);
                    prvCalcData.setGwp_ch4(gwp_ch4);
                    prvCalcData.setGwp_n2o(gwp_n20);
                    prvCalcData.setDensity_fuel_t_m3(den_diesel);
                    prvCalcData.setNet_caloric_fuel(net_cal_diesel);
                    prvCalcData.setEf_n2o(ef_n20_diesel);
                    prvCalcData.setEf_co2(ef_co2_diesel);
                    prvCalcData.setEf_ch4(ef_ch4_diesel);
                    e_own_up = Calculator.emissionPrivateTransport(prvCalcData);
                    results.put("gwp_co2", String.format("%.20f", gwp_co2));
                    results.put("gwp_ch4", String.format("%.20f", gwp_ch4));
//                    results.put("gwp_ch4", String.format("%.20f", kg_co2_van) Float.toString(gwp_ch4));
                    results.put("den_up", String.format("%.20f", den_diesel));
                    results.put("net_cal_up", String.format("%.20f", net_cal_diesel));
                    results.put("ef_n20_up", String.format("%.20f", ef_n20_diesel));
                    results.put("ef_co2_up", String.format("%.20f", ef_co2_diesel) );
                    results.put("ef_ch4_up",String.format("%.20f", ef_ch4_diesel) );
                    break;
                }default:{
                    e_own_up = new ResultDTO();
                }
            }
            results.put("e_own_up", String.format("%.20f", e_own_up.tco2e));
            ResultDTO e_own_down = null;
            switch (o.getOwnTrans_fuelType_DOWN()) {
                case 1: {
                    petrol +=  o.getOwnTrans_fuelEconomy_DOWN() == 0 ? 0 : ((o.getOwnTrans_distance_DOWN() / o.getOwnTrans_fuelEconomy_DOWN() )/1000);
                    prvCalcData.setNumber_of_trips(o.getNoOfWorkingDays());
                    prvCalcData.setUp_down_distance_km(o.getOwnTrans_distance_DOWN());
                    prvCalcData.setFuel_economy_km_l(o.getOwnTrans_fuelEconomy_DOWN());
                    prvCalcData.setDensity_fuel_t_m3(den_petrol);
                    prvCalcData.setNet_caloric_fuel(net_cal_petrol);
                    prvCalcData.setEf_n2o(ef_n20_petrol);
                    prvCalcData.setEf_co2(ef_co2_petrol);
                    prvCalcData.setEf_ch4(ef_ch4_petrol);
                    prvCalcData.setGwp_co2(gwp_co2);
                    prvCalcData.setGwp_ch4(gwp_ch4);
                    prvCalcData.setGwp_n2o(gwp_n20);
                    e_own_down = Calculator.emissionPrivateTransport(prvCalcData);
                    results.put("den_down", String.format("%.20f", den_petrol));
                    results.put("net_cal_down",String.format("%.20f", net_cal_petrol) );
                    results.put("ef_n20_down",String.format("%.20f", ef_n20_petrol));
                    results.put("ef_co2_down",String.format("%.20f", ef_co2_petrol) );
//                    results.put("ef_co2_down",String.format("%.20f", kg_co2_van) Float.toString(ef_co2_petrol));
                    results.put("ef_ch4_down",String.format("%.20f", ef_ch4_petrol));
                    results.put("gwp_co2",String.format("%.20f", gwp_co2) );
                    results.put("gwp_ch4",String.format("%.20f", gwp_ch4) );
                    results.put("gwp_n20",String.format("%.20f", gwp_n20) );
                    break;
                }
                case 2: {
                    diesel += o.getOwnTrans_fuelEconomy_DOWN() == 0 ? 0 : ((o.getOwnTrans_distance_DOWN() / o.getOwnTrans_fuelEconomy_DOWN() )/1000);
                    prvCalcData.setNumber_of_trips(o.getNoOfWorkingDays());
                    prvCalcData.setUp_down_distance_km(o.getOwnTrans_distance_DOWN());
                    prvCalcData.setFuel_economy_km_l(o.getOwnTrans_fuelEconomy_DOWN());
                    prvCalcData.setGwp_co2(gwp_co2);
                    prvCalcData.setGwp_ch4(gwp_ch4);
                    prvCalcData.setGwp_n2o(gwp_n20);
                    prvCalcData.setDensity_fuel_t_m3(den_diesel);
                    prvCalcData.setNet_caloric_fuel(net_cal_diesel);
                    prvCalcData.setEf_n2o(ef_n20_diesel);
                    prvCalcData.setEf_co2(ef_co2_diesel);
                    prvCalcData.setEf_ch4(ef_ch4_diesel);
                    e_own_down = Calculator.emissionPrivateTransport(prvCalcData);
                    results.put("gwp_co2",String.format("%.20f", gwp_co2) );
                    results.put("gwp_n20",String.format("%.20f", gwp_n20));
                    results.put("gwp_ch4",String.format("%.20f", gwp_ch4) );
                    results.put("den_down",String.format("%.20f", den_diesel) );
                    results.put("net_cal_down",String.format("%.20f", net_cal_diesel) );
                    results.put("ef_n20_down",String.format("%.20f", ef_n20_diesel) );
                    results.put("ef_co2_down",String.format("%.20f", ef_co2_diesel));
                    results.put("ef_ch4_down",String.format("%.20f", ef_ch4_diesel) );
                    break;
                }
                default:{
                    e_own_down =new ResultDTO();
                }
            }
            results.put("e_own_down", String.format("%.20f", e_own_down.tco2e));

//            compan
            switch (o.getCompany_fuelType_UP()) {
                case 1: {

                    break;
                }
                case 2: {

                    break;
                }
            }
            switch (o.getCompany_fuelType_DOWN()) {
                case 1: {

                    break;
                }
                case 2: {

                    break;
                }
            }

            ResultDTO resultsDirectDieselCom = new ResultDTO();
            ResultDTO resultsDirectPetrolCom = new ResultDTO();
            ResultDTO resultsDirectDieselOwn= new ResultDTO();
            ResultDTO resultsDirectPetrolOwn = new ResultDTO();

            if (o.getCompanyPetrolLiters() != 0) {
                CompanyOwnedVehicleCalcData data = new CompanyOwnedVehicleCalcData();
                petrol +=  (o.getCompanyPetrolLiters()/1000);

                data.setConsumption_liters(o.getCompanyPetrolLiters());
                data.setDensity(den_petrol);
                data.setNet_caloric_value(net_cal_petrol);
                data.setEf_ch4(ef_ch4_petrol);
                data.setEf_co2(ef_co2_petrol);
                data.setEf_n2o(ef_n20_petrol);
                data.setGwp_ch4(gwp_ch4);
                data.setGwp_co2(gwp_co2);
                data.setGwp_n2o(gwp_n20);

                results.put("petrol",String.format("%.20f", petrol) );


                resultsDirectPetrolCom = Calculator.emissionCompanyOwnedVehicle(data);
            }
            if (o.getCompanyDieselLiters() != 0) {
                CompanyOwnedVehicleCalcData data = new CompanyOwnedVehicleCalcData();
                diesel += (o.getCompanyDieselLiters()/1000);

                data.setConsumption_liters(o.getCompanyDieselLiters());
                data.setDensity(den_diesel);
                data.setNet_caloric_value(net_cal_diesel);
                data.setEf_ch4(ef_ch4_diesel);
                data.setEf_co2(ef_co2_diesel);
                data.setEf_n2o(ef_n20_diesel);
                data.setGwp_ch4(gwp_ch4);
                data.setGwp_co2(gwp_co2);
                data.setGwp_n2o(gwp_n20);
                results.put("diesel",String.format("%.20f", diesel ));
                resultsDirectDieselCom= Calculator.emissionCompanyOwnedVehicle(data);
            }

            if (o.getOwnPetrolLiters() != 0) {
                CompanyOwnedVehicleCalcData data = new CompanyOwnedVehicleCalcData();
                petrol += (o.getOwnPetrolLiters()/1000);


                data.setConsumption_liters(o.getOwnPetrolLiters());
                data.setDensity(den_petrol);
                data.setNet_caloric_value(net_cal_petrol);
                data.setEf_ch4(ef_ch4_petrol);
                data.setEf_co2(ef_co2_petrol);
                data.setEf_n2o(ef_n20_petrol);
                data.setGwp_ch4(gwp_ch4);
                data.setGwp_co2(gwp_co2);
                data.setGwp_n2o(gwp_n20);
                results.put("petrol",String.format("%.20f", petrol) );

                resultsDirectPetrolOwn = Calculator.emissionCompanyOwnedVehicle(data);
            }

            if (o.getOwnDieselLiters() != 0) {
                CompanyOwnedVehicleCalcData data = new CompanyOwnedVehicleCalcData();
                diesel += (o.getOwnDieselLiters()/1000);

                data.setConsumption_liters(o.getOwnDieselLiters());
                data.setDensity(den_diesel);
                data.setNet_caloric_value(net_cal_diesel);
                data.setEf_ch4(ef_ch4_diesel);
                data.setEf_co2(ef_co2_diesel);
                data.setEf_n2o(ef_n20_diesel);
                data.setGwp_ch4(gwp_ch4);
                data.setGwp_co2(gwp_co2);
                data.setGwp_n2o(gwp_n20);

                results.put("diesel",String.format("%.20f", diesel ));
                resultsDirectDieselOwn = Calculator.emissionCompanyOwnedVehicle(data);
            }








            results.put("km_public",String.format("%.20f",km_public * o.getNoOfWorkingDays())  );
            if (!results.containsKey("diesel")) {
                results.put("diesel",String.format("%.20f", diesel * o.getNoOfWorkingDays()));
            }
            if (!results.containsKey("petrol")) {
                results.put("petrol",String.format("%.20f", petrol* o.getNoOfWorkingDays()) );
            }


            results.put("tco2",String.format("%.20f",
                    resultsDirectDieselCom.tco2e +
                            resultsDirectPetrolCom.tco2e +
                            resultsDirectPetrolOwn.tco2e +
                            resultsDirectDieselOwn.tco2e +
                            e_own_down.tco2e + e_own_up.tco2e + e_pub_up.tco2e * o.getNoOfWorkingDays() + e_pub_down.tco2e * o.getNoOfWorkingDays())  );




        } catch (NullPointerException e) {
            logger.error("Null entry", e);
        }
        return results;
    }
}
