package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.api.LoginResponseStatus;
import com.climatesi.ghg.api.UserFactory;
import com.climatesi.ghg.api.beans.AuthReply;
import com.climatesi.ghg.api.beans.AuthenticationData;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_messaging.api.beans.Header;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.authentication.AuthenticateRequestBean;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.ArrayList;

public class AuthMessageHelper {
    private static Logger logger = Logger.getLogger(AuthMessageHelper.class);

    private UserLoginController loginController;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;
//    private EntitlementController entitlementController;

    public AuthMessageHelper() {
        try {
            loginController = GHGControllerFactory.getInstance().getUserLoginController();
//            this.entitlementController = GHGControllerFactory.getInstance().getEntitlementController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
        } catch (GHGException e) {
            logger.error("Failed to initate Controllers in AuthMessage Helper >> " + e.getMessage(), e);
        }
    }

    /**
     * Send authentication request to back office and return question response
     *
     * @param message is message object of authentication request
     * @return question authentication response message
     */
    public AuthReply doLogin(AuthenticateRequestBean message, Header header, int user) {
        logger.info("[AuthMessageHelper.java:49] Message >>" + message);

        AuthenticationData authenticationData = null;
        try {
            authenticationData = UserFactory.getInstance().getEmptyAuthenticationData();
        } catch (GHGException e) {
            logger.error("[AuthMessageHelper.java:56] Failed to get dummy object.", e);
        }

        authenticationData.setUsername(message.getUserName());
        authenticationData.setPassword(message.getPassword());
        authenticationData.setChannelId(header.getChannelId());
        authenticationData.setIpAddress(header.getClientIp());
        authenticationData.setClientVersion(header.getClientVersion());

        AuthReply reply = loginController.doLoginUser(authenticationData);
        //getting user's entitlements
        if (reply.getLoginStatus() == LoginResponseStatus.SUCCESS.getCode()) {

            //  ----------------log--------------------------------------------------
            UserActivityLogBean log = null;
            try {

                UserLogin userLogin =  userLoginController.findById(user);
                if (userLogin != null) {
                    log = new UserActivityLogBean();
                    log.setUserId(user);
                    log.setDeleted(0);
                    JsonObject filter = new JsonObject();
                    JsonObject loginFilter = new JsonObject();
                    loginFilter.addProperty("value", user);
                    loginFilter.addProperty("col", 1);
                    loginFilter.addProperty("type",1);
                    filter.add("userId", loginFilter);

                    if (userLogin.getUserType() == 1) {
                        ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", filter);
                        if (result.getList().size() == 1) {
                            log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                            log.setBranchId(result.getList().get(0).getBranchId());
                            log.setCompanyId(result.getList().get(0).getCompanyId());
                        }
                    }
                    if (userLogin.getUserType() == 2) {
                        ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", filter);
                        if (result.getList().size() == 1) {
                            log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                            log.setBranchId(-1);
                            log.setCompanyId(result.getList().get(0).getCompanyId());
                        }
                    }
                    if (userLogin.getUserType() == 3) {
                        ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", filter);
                        if (result.getList().size() == 1) {
                            log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
                            log.setBranchId(-1);
                            log.setCompanyId(-1);
                        }
                    }
                }
            } catch (GHGException e) {
                logger.error(e);
            }
            if (log != null) {
                log.setActLog(log.getUsername() + " logged in.") ;
                try {
                    userActController.addActLog(log);
                } catch (GHGException e) {
                    logger.error(e);
                }
            }

            //------------------------log-----------------------------------------



            logger.info("[AuthMessageHelper.java:68] Login is success. Getting entitlements for, UserID: " + reply.getUserId());
            if (reply.getUserId() > 0) {
                ArrayList<Integer> entitlementList = null;
//                Todo: uncomment
//                entitlementList = entitlementController.getUserAllowedEntitlements(reply.getEmployee(), reply.getEmployee().getUserId());
//                if (entitlementList != null) {
//                    logger.info("[AuthMessageHelper.java:75] Fetched entitlement size: " + entitlementList.size());
//                    reply.setEntitlements(entitlementList);
//                } else {
//                    logger.info("[AuthMessageHelper.java:78] No entitlement group has assign for the customer");
//                }
            }
        }
        logger.info("[AuthMessageHelper.java:84] Retuning authentication reply message >> " + reply);
        return reply;
    }

    public ListResult<String> doLogoutAT(int userLoginId) {
        return loginController.doLogoutUser(userLoginId);
    }
}

