package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class ElectricityPopulator {

    private static Logger logger = Logger.getLogger(ElectricityPopulator.class);
    private static ElectricityPopulator instance;

    public static ElectricityPopulator getInstance() {
        if (instance == null) {

            instance = new ElectricityPopulator();
        }
        return instance;
    }

    public ElectricityEntryDTO populateDTO(ElectricityEntry entry) {
        ElectricityEntryDTO dto = new ElectricityEntryDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setEntryAddedDate(entry.getEntryAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setUnits(entry.getUnits());
        dto.setConsumption(entry.getConsumption());
        dto.setNumOfMeters(entry.getNumOfMeters());
        dto.setMeterNumbers(entry.getMeterNumbers());
        dto.setMeterNo(entry.getMeterNo());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public ElectricityEntry populate(ElectricityEntry entry, ElectricityEntryDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }

            }
            entry.setEmissionSrcId(dto.getEmissionSrcId() == 0 ? entry.getEmissionSrcId() : dto.getEmissionSrcId());
//            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId() == 0 ? entry.getBranchId() : dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId() == 0 ? entry.getCompanyId() : dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear() == null ? entry.getYear() : dto.getYear());
            entry.setUnits(dto.getUnits() == 0 ? entry.getUnits() : dto.getUnits());
            entry.setConsumption(dto.getConsumption() == 0.0 ? entry.getConsumption() : dto.getConsumption());
            entry.setNumOfMeters(dto.getNumOfMeters() == 0 ? entry.getNumOfMeters() : dto.getNumOfMeters());
            entry.setMeterNumbers(dto.getMeterNumbers() == null ? entry.getMeterNumbers() : dto.getMeterNumbers());
            entry.setMeterNo(dto.getMeterNo() == null ? entry.getMeterNo() : dto.getMeterNo());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());



        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
