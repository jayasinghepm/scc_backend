package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors.EmissionFactorsStoreController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors.PubTransEmissionFactorsController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors.WasteDisposalFactorsControllers;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_controller.populators.EmFactorsPopulator;
import com.climatesi.ghg.bo_controller.populators.PubTransPopulator;
import com.climatesi.ghg.bo_controller.populators.WasteDisFactorsPopulator;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.EmFactorDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.PubTransEmFactorDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.WasteDisFactorDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.request.*;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.response.*;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class EmissionFactorHelper {

    private static Logger logger = Logger.getLogger(EmissionFactorHelper.class);
    private EmissionFactorsStoreController emissionFactorsStoreController;
    private WasteDisposalFactorsControllers wasteDisposalFactorsControllers;
    private PubTransEmissionFactorsController pubTransEmissionFactorsController;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;

    public EmissionFactorHelper() {
        try {
            emissionFactorsStoreController = GHGControllerFactory.getInstance().getEmissionFactorsStoreController();
            wasteDisposalFactorsControllers = GHGControllerFactory.getInstance().getWasteDisposalFactorsControllers();
            pubTransEmissionFactorsController = GHGControllerFactory.getInstance().getPubTransEmissionFactorsController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
        } catch (Exception e) {
            logger.error("Error in intializing controoler", e);
        }
    }

    public ListEmFactorResMsg getEmissionFactors(ListEmFactorReqMsg msg, int user) {

        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<EmissionFactors> list = null;
        ListEmFactorResMsg res = new ListEmFactorResMsg();
        list = emissionFactorsStoreController.getFilteredResult(pageNumber, sorting, filter);
        List<EmFactorDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                EmFactorDTO dto = EmFactorsPopulator.getInstance().populateDTO((EmissionFactors) o);
                results.add(dto);
            }
        }
        res.setList(results);
        return res;
    }

    public ListWastDisFactorResMsg getWasteDisFactors(ListWasteDisFactorReqMsg msg, int user) {

        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<WasteDisposalFactors> list = null;
        ListWastDisFactorResMsg res = new ListWastDisFactorResMsg();
        list = wasteDisposalFactorsControllers.getFilteredResults(pageNumber, sorting, filter);

        List<WasteDisFactorDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                WasteDisFactorDTO dto = WasteDisFactorsPopulator.getInstance().populateDTO((WasteDisposalFactors) o);
                results.add(dto);
            }
        }
        res.setList(results);
        return res;


    }

    public ListPubTransFactorResMsg getPublicTransFactors(ListPubTransFactorReqMsg msg, int user) {

        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<PublicTransposrtEmisssionFactor> list = null;
        ListPubTransFactorResMsg res = new ListPubTransFactorResMsg();
        list = pubTransEmissionFactorsController.getFilteredResult(pageNumber, sorting, filter);

        List<PubTransEmFactorDTO> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                PubTransEmFactorDTO dto = PubTransPopulator.getInstance().populateDTO((PublicTransposrtEmisssionFactor) o);
                results.add(dto);
            }
        }
        res.setList(results);
        return res;
    }

    public ManageEmFactorsResMsg manageEmFactor(ManageEmFactorReqMsg msg, int user) {
        logger.info("Message : " + msg);
        EmissionFactors entry;
        ManageEmFactorsResMsg res = new ManageEmFactorsResMsg();
        try {
            entry = emissionFactorsStoreController.edit(msg.getDto());
            if (entry != null) {
//                //  ----------------log--------------------------------------------------
//                UserActivityLogBean log = null;
//                try {
//
//                    UserLogin userLogin =  userLoginController.findById(user);
//                    if (userLogin != null) {
//                        log = new UserActivityLogBean();
//                        log.setUserId(user);
//                        log.setDeleted(0);
//                        JsonObject filter = new JsonObject();
//                        JsonObject loginFilter = new JsonObject();
//                        loginFilter.addProperty("value", user);
//                        loginFilter.addProperty("col", 1);
//                        loginFilter.addProperty("type",1);
//                        filter.add("userId", loginFilter);
//
//                        if (userLogin.getUserType() == 1) {
//                            ListResult<Client> result =  businessUserController.getClientsFiltered(-1, "", loginFilter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(result.getList().get(0).getBranchId());
//                                log.setCompanyId(result.getList().get(0).getCompanyId());
//                            }
//                        }
//                        if (userLogin.getUserType() == 2) {
//                            ListResult<ComAdmin> result =  businessUserController.getComAdminsFiltered(-1, "", loginFilter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(-1);
//                                log.setCompanyId(result.getList().get(0).getCompanyId());
//                            }
//                        }
//                        if (userLogin.getUserType() == 3) {
//                            ListResult<Admin> result =  businessUserController.getAdminsFiltered(-1, "", loginFilter);
//                            if (result.getList().size() == 1) {
//                                log.setUsername(result.getList().get(0).getFirstName()  + " " + result.getList().get(0).getLastName());
//                                log.setBranchId(-1);
//                                log.setCompanyId(-1);
//                            }
//                        }
//                    }
//                } catch (GHGException e) {
//                    logger.error(e);
//                }
//                if (log != null) {
//                    log.setActLog("Added "+ (msg.getDto().getReportType() == 1 ? "Draft " : "Final ") + " was added by" + log.getUsername()) ;
//                    userActController.addActLog(log);
//                }
//                //------------------------log-----------------------------------------
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getId());
                res.setDto(EmFactorsPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage ", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public ManagePubTransFactorsResMsg managePubTransFactors(ManagePubTransFactorReqMsg msg, int user) {
        logger.info("Message : " + msg);
        PublicTransposrtEmisssionFactor entry;
        ManagePubTransFactorsResMsg res = new ManagePubTransFactorsResMsg();
        try {
            entry = pubTransEmissionFactorsController.edit(msg.getDto());
            if (entry != null) {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getVehicleType());
                res.setDto(PubTransPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (Exception e) {
            logger.error("Failed in manage", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    public ManageWasteDisFactorsResMsg manageWasteDisFactors(ManageWasteDisFactorReqMsg msg, int user) {
        logger.info("Message : " + msg);
        WasteDisposalFactors entry;
        ManageWasteDisFactorsResMsg res = new ManageWasteDisFactorsResMsg();
        try {
            entry = wasteDisposalFactorsControllers.edit(msg.getDto());
            if (entry != null) {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getWasteType());
                res.setDto(WasteDisFactorsPopulator.getInstance().populateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage ", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }


}
