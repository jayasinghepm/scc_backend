package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BusinessAirTravelEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.BusinessAirTravelEntry;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.HashMap;
import java.util.Map;

public class AirTravelPopulator {

    private static Logger logger = Logger.getLogger(AirTravelPopulator.class);
    private static AirTravelPopulator instance = null;

    public static AirTravelPopulator getInstance() {
        if (instance == null) {
            instance = new AirTravelPopulator();
        }
        return instance;
    }

    public BusinessAirTravelEntryDTO populateDTO(BusinessAirTravelEntry entry) {
        BusinessAirTravelEntryDTO dto = new BusinessAirTravelEntryDTO();
        dto.setAirTravelEntryId(entry.getAirTravelEntryId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setEmployeeId(entry.getEmployeeId());
        dto.setTravelYear(entry.getTravelYear());
        dto.setTravelMonth(entry.getTravelMonth());
        dto.setDepartureAirport(entry.getDepartureAirport());
        dto.setDepartureCountry(entry.geDepartureCountry());
        dto.setDestAirport(entry.getDestAirport());
        dto.setDestCountry(entry.getDestCountry());
        dto.setTransist1Port(entry.getTransist1Port());
        dto.setTransist2Port(entry.getTransist2Port());
        dto.setTransist3Port(entry.getTransist3Port());
        dto.setAirTravelWay(entry.getAirTravelWay());
        dto.setSeatClass(entry.getSeatClass());
        dto.setEntryAddedDate(entry.getEntryAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setNoOfEmps(entry.getNoOfEmps());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        dto.setDepCoun(entry.getDepCoun());
        dto.setDestCoun(entry.getDestCoun());
        dto.setDesPort(entry.getDesPort());
        dto.setDepPort(entry.getDepPort());
        dto.setTrans1(entry.getTrans1());
        dto.setTrans2(entry.getTrans2());
        dto.setTrans3(entry.getTrans3());
        dto.setSeat(entry.getSeat());
        dto.setTravelWay(entry.getTravelWay());
        dto.setDistance(entry.getDistance());


        return dto;
    }

    public BusinessAirTravelEntry populate(BusinessAirTravelEntry entry, BusinessAirTravelEntryDTO dto) {
        try {
            if (dto.getAirTravelEntryId() > 0) {
                entry.setAirTravelEntryId(dto.getAirTravelEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }

            }

//            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId() == 0 ? entry.getBranchId() : dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId() == 0 ? entry.getCompanyId() : dto.getCompanyId());
            entry.setEmployeeId(dto.getEmployeeId() == 0 ? entry.getEmployeeId() : dto.getEmployeeId());
            entry.setTravelYear(dto.getTravelYear() == null ? entry.getTravelYear() : dto.getTravelYear());
            entry.setTravelMonth(dto.getTravelMonth());
            entry.setDepartureAirport(dto.getDepartureAirport() == 0 ? entry.getDepartureAirport() : dto.getDepartureAirport());
            entry.setDepartureCountry(dto.geDepartureCountry() == 0 ? entry.geDepartureCountry() : dto.geDepartureCountry());
            entry.setDestAirport(dto.getDestAirport() == 0 ? entry.getDestAirport() : dto.getDestAirport());
            entry.setDestCountry(dto.getDestCountry() == 0 ? entry.getDestCountry() : dto.getDestCountry());
            entry.setTransist1Port(dto.getTransist1Port() == 0 ? entry.getTransist1Port() : dto.getTransist1Port());
            entry.setTransist2Port(dto.getTransist2Port() == 0 ? entry.getTransist2Port() : dto.getTransist2Port());
            entry.setTransist3Port(dto.getTransist3Port() == 0 ? entry.getTransist3Port() : dto.getTransist3Port());
            entry.setAirTravelWay(dto.getAirTravelWay() == 0 ? entry.getAirTravelWay() : dto.getAirTravelWay());
            entry.setSeatClass(dto.getSeatClass() == 0 ? entry.getSeatClass() : dto.getSeatClass());
            entry.setNoOfEmps(dto.getNoOfEmps() == 0 ? entry.getNoOfEmps() : dto.getNoOfEmps());
            entry.setCompany(dto.getCompany() == null ?  entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon() : dto.getMon());
            entry.setDepCoun(dto.getDepCoun() == null ? entry.getDepCoun() : dto.getDepCoun());
            entry.setDestCoun(dto.getDestCoun() == null ? entry.getDestCoun() : dto.getDestCoun());
            entry.setDesPort(dto.getDesPort() == null ? entry.getDesPort() : dto.getDesPort());
            entry.setDepPort(dto.getDepPort() == null ? entry.getDepPort() : dto.getDepPort());
            entry.setTrans1(dto.getTrans1() == null ? entry.getTrans1() : dto.getTrans1());
            entry.setTrans2(dto.getTrans2() == null ? entry.getTrans2() : dto.getTrans2());
            entry.setTrans3(dto.getTrans3() == null ? entry.getTrans3() : dto.getTrans3());
            entry.setSeat(dto.getSeat() == null ? entry.getSeat() : dto.getSeat());
            entry.setTravelWay(dto.getTravelWay() == null ? entry.getTravelWay() : dto.getTravelWay());
            if (dto.getEmissionDetails() != null) {
                HashMap<String, String> emissionInfo =new HashMap<>();
                for(Map.Entry<String, JsonElement> o : dto.getEmissionDetails().entrySet()){
                    emissionInfo.put(o.getKey(), dto.getEmissionDetails().get(o.getKey()).getAsString());
                }
                entry.setEmissionDetails(emissionInfo);
            }
            entry.setDistance(dto.getDistance() == 0? entry.getDistance() : dto.getDistance());

        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
