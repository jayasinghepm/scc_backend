package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.api.beans.UserActivityLog;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserActController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.UserLoginController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.insitution.InsstitutionController;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.users.BusinessUserController;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.ClientDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.core.LoginUserDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.core.UsrActLogDto;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.CreateLoginProfileBranchReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.FindLoginUserReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.ListUsrActLogsReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.request.ManageLoginUserReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response.CreateLoginProfileBranchResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response.FindLoginUserResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response.ListUsrActLogsResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.response.ManageLoginUserResMsg;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class UserLoginHelper {

    private static Logger logger = Logger.getLogger(UserLoginHelper.class);
    private UserLoginController controller;
    private UserLoginController userLoginController;
    private UserActController userActController;
    private BusinessUserController businessUserController;
    private InsstitutionController insstitutionController;



    public UserLoginHelper() {

        try {
            controller = GHGControllerFactory.getInstance().getUserLoginController();
            userLoginController = GHGControllerFactory.getInstance().getUserLoginController();
            userActController = GHGControllerFactory.getInstance().getUserActController();
            businessUserController = GHGControllerFactory.getInstance().getBusinessUserController();
            insstitutionController = GHGControllerFactory.getInstance().getInsstitutionController();
        } catch (Exception e) {
            logger.error("Error in getting instance of cotroller", e);
        }
    }

    public ManageLoginUserResMsg manageLoginUser(ManageLoginUserReqMsg msg) {
        logger.info("Message : " + msg);
        UserLogin entry;
        ManageLoginUserResMsg res = new ManageLoginUserResMsg();
        try {
            entry = controller.editUserLogin(msg.getDto());
            if (entry != null) {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
                res.setEntityKey(entry.getUserId());
                res.setDto(popoulateDTO(entry));
            } else {
                res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
                res.setNarration("Object not found in db");
            }


        } catch (GHGException e) {
            logger.error("Failed in manage UserLogin", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
            res.setNarration(e.getMessage());
        }

        return res;
    }

    private LoginUserDTO popoulateDTO(UserLogin login) {
        LoginUserDTO dto = new LoginUserDTO();
        dto.setUserId(login.getUserId());
        dto.setUserType(login.getUserType());
        dto.setUserName(login.getLoginName());
        dto.setIsFirstTime(login.getFirstTime());
        dto.setLoginStatus(login.getStatus());
        return dto;
    }

    public FindLoginUserResMsg findUserLogin(FindLoginUserReqMsg message) {
        FindLoginUserResMsg res = new FindLoginUserResMsg();
        try {
            UserLogin userLogin = controller.findById(message.getUserId());
            if (userLogin != null) {
                res.setUserId(message.getUserId());
                res.setLoginName(userLogin.getLoginName());
                res.setFailedAttempts(userLogin.getFailedLoginAttempts());
                res.setStatus(userLogin.getStatus());
                DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:SS z");
                df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
                String IST = df.format(userLogin.getLastLoggedInDate());
                res.setLastLoginDate(IST);

            }
        }catch (Exception e) {
            logger.error(e);
        }
        return res;
    }

    public Message getFilterLogs(ListUsrActLogsReqMsg msg , int user) {
        logger.info("Message : " + msg);

//        Todo:   integrate filter , sorting, pagenumber
        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);

        ListResult<UserActivityLog> list = null;
        ListUsrActLogsResMsg res = new ListUsrActLogsResMsg ();
        list = userActController.getFilteredLogs(pageNumber, sorting, filter);

        List<UsrActLogDto> results = new ArrayList();
        if (list.getList() == null) {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        } else {
            logger.info("Fetched count: " + list.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Object o : list.getList()) {
                UsrActLogDto dto = new UsrActLogDto();
                dto.setActLog(((UserActivityLogBean)o).getActLog());
                dto.setBranchId(((UserActivityLogBean)o).getBranchId());
                dto.setCompanyId(((UserActivityLogBean)o).getCompanyId());
                dto.setId(((UserActivityLogBean)o).getId());
                dto.setUserId(((UserActivityLogBean)o).getUserId());
                dto.setUsername(((UserActivityLogBean)o).getUsername());
                DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:SS z");
                df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
                String IST = df.format(((UserActivityLogBean)o).getLogTime());
                dto.setLogTime(IST);
                results.add(dto);
            }
        }
        res.setList(results);
        res.setTotalRecordCount(list.getTotalCount());
        return res;
    }

    public Message createBranchLoginProfile(CreateLoginProfileBranchReqMsg message, int user) {
        CreateLoginProfileBranchResMsg res = new CreateLoginProfileBranchResMsg();
        try {
            JsonObject filter = new JsonObject();
            JsonObject comFilter = new JsonObject();
            comFilter.addProperty("value", message.getComId());
            comFilter.addProperty("type",1);
            comFilter.addProperty("col",1);
            filter.add("companyId", comFilter);
            List<Branch> branches = insstitutionController.getBranchFiltered(-1, "", filter).getList();
            if (branches == null || branches.size() == 0) {
                res.setStatus(-1);
                return res;
            }
            HashMap<String, String> profiles = new HashMap<>();
            for(Branch b : branches) {
//                UserLogin login = new UserLoginBean();
//                login.setUserType(1);
//                login.setLoginName(b.getName());
//                login.setFirstTime(1);
//                login.setLoginPassword("password");
//                login.setStatus(1);
                JsonObject filter1 = new JsonObject();
                JsonObject branchFilter = new JsonObject();
                branchFilter.addProperty("value", b.getId());
                branchFilter.addProperty("col",1);
                branchFilter.addProperty("type",1);
                filter1.add("branchId", branchFilter);
                List<Client> clients = businessUserController.getClientsFiltered(-1, "", filter1).getList();
                 if (clients != null && clients.size() > 0) {
                     continue;
                 }
                String loginName = "";
                try {



                    LoginUserDTO loginDto = new LoginUserDTO();
                    loginDto.setIsFirstTime(2);
                    loginDto.setUserType(1);
                    loginName = b.getCompany() + " " + b.getName();
                    loginName = loginName.toLowerCase();
                    loginName = loginName.replaceAll(" ", "_");
                    loginName = loginName.replaceAll("'", "");
                    loginDto.setUserName(loginName);
                    loginDto.setPassword("password");
                    loginDto.setLoginStatus(1);
                    UserLogin login = userLoginController.editUserLogin(loginDto);

                    ClientDTO dto = new ClientDTO();
                    dto.setCompany(b.getCompany());
                    dto.setCompanyId(b.getCompanyId());
                    dto.setBranch(b.getName());
                    dto.setBranchId(b.getId());
                    dto.setUserId(login.getUserId());
                    dto.setMobileNo("Change It");
                    dto.setEmail("Change It");
                    dto.setTitle("Mr");
                    dto.setTelephoneNo("Change It");
                    dto.setLastName("Change It");
                    dto.setFirstName("Change It");
                    dto.setDeleted(0);
                    Client savedClient = businessUserController.editCliente(dto);


                    profiles.put(loginName, "password");
                }catch (Exception e) {
                    profiles.put(loginName, "#Error");
                }
            }
            JsonObject profileObj = new JsonObject();
            profiles.forEach((k,v)-> {
                profileObj.addProperty(k,v);
            });
            res.setProfiles(profileObj);

        }catch (Exception e) {
            logger.error(e);
            res.setStatus(2);
        }
        return res;
    }
}
