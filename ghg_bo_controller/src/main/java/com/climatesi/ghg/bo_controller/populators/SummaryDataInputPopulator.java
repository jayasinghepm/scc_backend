package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.CommonSummaryInputDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.ProjectDataEntryStatusDTO;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.EmissionSrcDataEntryStatusBean;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import java.util.HashMap;

public class SummaryDataInputPopulator {

    private static Logger logger = Logger.getLogger(SummaryDataInputPopulator.class);
    private static SummaryDataInputPopulator instance;

    public static SummaryDataInputPopulator getInstance() {
        if (instance == null) {
            instance = new SummaryDataInputPopulator();
        }
        return instance;
    }

    public CommonSummaryInputDTO populateDTO(HashMap<String, HashMap<String, String>> summary) {
        CommonSummaryInputDTO dto = new CommonSummaryInputDTO();
        JsonObject jan = new JsonObject();
        JsonObject feb = new JsonObject();
        JsonObject mar = new JsonObject();
        JsonObject apr = new JsonObject();
        JsonObject may = new JsonObject();
        JsonObject jun = new JsonObject();
        JsonObject jul = new JsonObject();
        JsonObject aug = new JsonObject();
        JsonObject sep = new JsonObject();
        JsonObject oct = new JsonObject();
        JsonObject nov = new JsonObject();
        JsonObject dec = new JsonObject();
        JsonObject all = new JsonObject();
        try {
            summary.get("jan").forEach((k,v)-> {
                jan.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("feb").forEach((k,v)-> {
                feb.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("mar").forEach((k,v)-> {
                mar.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("apr").forEach((k,v)-> {
                apr.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("may").forEach((k,v)-> {
                may.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("jun").forEach((k,v)-> {
                jun.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("jul").forEach((k,v)-> {
                jul.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("aug").forEach((k,v)-> {
                aug.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("sep").forEach((k,v)-> {
                sep.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("oct").forEach((k,v)-> {
                oct.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("nov").forEach((k,v)-> {
                nov.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("dec").forEach((k,v)-> {
                dec.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        try {
            summary.get("all").forEach((k,v)-> {
                all.addProperty(k,v);
            });
        }catch (NullPointerException e) {
            logger.error(e);
        }
        dto.setAll(all);
        dto.setJan(jan);
        dto.setFeb(feb);
        dto.setMar(mar);
        dto.setApr(apr);
        dto.setMay(may);
        dto.setJun(jun);
        dto.setJul(jul);
        dto.setAug(aug);
        dto.setSep(sep);
        dto.setOct(oct);
        dto.setNov(nov);
        dto.setDec(dec);

        return dto;
    }

    public ProjectDataEntryStatusDTO populateProjectStatusDTO(EmissionSrcDataEntryStatusBean bean) {
        ProjectDataEntryStatusDTO dto = new ProjectDataEntryStatusDTO();
        try {
            dto.setBranchId(bean.getId().getBranchId());
            dto.setCompanyId(bean.getId().getCompanyId());
            dto.setFy(bean.getId().getFY());
            dto.setEmissionSrc(bean.getId().getEmissionSrc());
            dto.setStatus(bean.getStatus());
        }catch (NullPointerException e) {
            logger.error(e);
        }
        return  dto;
    }
}
