package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GeneratorsEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.GeneratorsEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class GeneratorsPopulator {

    private static Logger logger = Logger.getLogger(GeneratorsPopulator.class);
    private static GeneratorsPopulator instance = null;

    public static GeneratorsPopulator getInstance() {
        if (instance == null) {
            instance = new GeneratorsPopulator();
        }
        return instance;
    }

    public GeneratorsEntryDTO populateDTO(GeneratorsEntry entry) {
        GeneratorsEntryDTO dto = new GeneratorsEntryDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setEntryAddedDate(entry.getEntryAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setFuelType(entry.getFuelType());
        dto.setUnits(entry.getUnits());
        dto.setConsumption(entry.getConsumption());
        dto.setNumOfGenerators(entry.getNumOfGenerators());
        dto.setGeneratorNumber(entry.getGeneratorNumber());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());

        return dto;
    }

    public GeneratorsEntry populate(GeneratorsEntry entry, GeneratorsEntryDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }
            }
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setFuelType(dto.getFuelType());
            entry.setUnits(dto.getUnits());
            entry.setConsumption(dto.getConsumption());
            entry.setNumOfGenerators(dto.getNumOfGenerators());
            entry.setGeneratorNumber(dto.getGeneratorNumber());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());

        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }
}
