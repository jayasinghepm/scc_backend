package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.mitigation.MitigationController;
import com.climatesi.ghg.bo_controller.populators.MitigationActDataPopulator;
import com.climatesi.ghg.bo_controller.populators.MitigationProjectPopulator;
import com.climatesi.ghg.bo_messaging.api.beans.Message;
import com.climatesi.ghg.bo_messaging.api.constants.EntityActions;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationActDataByCompany;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectActData;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.GetMitigationActDataReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.GetMitigationReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.ManageMitigationActDataReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.request.ManageMitigationProjectReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.response.GetMitigationActDataResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.response.GetMitigationResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.response.ManageMitigationActDataResMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.response.ManageMitigationProjectResMsg;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.implGeneral.data.GridElectricityData;
import com.climatesi.ghg_mitication.api.beans.MitigationProject;
import com.climatesi.ghg_mitication.api.beans.MitigationProjectMonthlyActivity;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectMonthlyActivityBean;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class MitigationHelper {

    private static Logger logger = Logger.getLogger(MitigationHelper.class);
    private MitigationController controller;

    public MitigationHelper() {
        try {
            controller = GHGControllerFactory.getInstance().getMitigationController();
        }catch (Exception e) {
            logger.error("Failed to intialize controller");
        }
    }



    public Message manageMitigationProject(ManageMitigationProjectReqMsg message, int user) {
        ManageMitigationProjectResMsg res = new ManageMitigationProjectResMsg();
        try {

            if (message.getDto() == null || message.getDto().getAction() <=0  || message.getDto().getAction() > 4) {
                throw  new Exception("Error : Empty fields");
            }
            MitigationProject saved = null;

            if (message.getDto().getAction() == EntityActions.Add.getAction()) {
                MitigationProject project = MitigationProjectPopulator.getInstance().populate(message.getDto(), null);
                saved = controller.updateMitigationProject(project);
            }else if (message.getDto().getAction() == EntityActions.Update.getAction()) {

                MitigationProject project = controller.findMitigationProjectById(message.getDto().getProjectId());

                project = MitigationProjectPopulator.getInstance().populate(message.getDto(), project);
                saved = controller.updateMitigationProject(project);
            }else if (message.getDto().getAction() == EntityActions.Delete.getAction()) {
                controller.deleteMitigationProject(message.getDto().getProjectId());
            }else {
                throw new Exception("Unknown action");
            }
            res.setProjectId(saved.getProjectId());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);



        }catch (Exception e) {
            logger.error("Failed to process request ", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        }
        return res;
    }

    public Message getMitigationProject(GetMitigationReqMsg message, int user) {
        GetMitigationResMsg res = new GetMitigationResMsg();

        try {
            if(message == null || message.getBranchId() ==0 || message.getComId() == 0) {
                throw new Exception("Failed : Empty fields");
            }

            List<MitigationProjectDTO> projects = new ArrayList<>();
            if (message.getComId() > 0) {
                projects = controller.getMitigationProjects(message.getComId());
            }else if (message.getBranchId() > 0 ) {
                projects = controller.getMitigationProjectsByBranch(message.getBranchId());
            }
            res.setProjects(projects);





            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
        }catch (Exception e) {
            logger.error("Failed to process request ", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        }
        return res;
    }

    public Message manageMitigationProjectActData(ManageMitigationActDataReqMsg message, int user) {
        ManageMitigationActDataResMsg res = new ManageMitigationActDataResMsg();
        try {
            if(message == null || message.getData() == null || message.getData().getAction() <= 0) {
                throw new Exception("Empty fields");
            }

            MitigationProjectMonthlyActivity saved = null;


            if (message.getData().getAction() == EntityActions.Add.getAction()) {
                MitigationProjectMonthlyActivity activity = MitigationActDataPopulator.getInstance().populate(message.getData(), null);

                GridElectricityData calData = new GridElectricityData();
                calData.setConsumption((float) message.getData().getValue());
                activity.setSavedEmission(Calculator.emissionGridElectricity(calData).tco2e);
                saved = controller.updateMitigationActData(activity);

            }else if (message.getData().getAction() == EntityActions.Update.getAction()) {
                MitigationProjectMonthlyActivity activity = controller.findActDataById(message.getData().getId());
                activity = MitigationActDataPopulator.getInstance().populate(message.getData(), activity);

                GridElectricityData calData = new GridElectricityData();
                calData.setConsumption((float) message.getData().getValue());
                activity.setSavedEmission(Calculator.emissionGridElectricity(calData).tco2e);
                saved = controller.updateMitigationActData(activity);

            }else if (message.getData().getAction() == EntityActions.Delete.getAction()) {
                controller.deleteMitigationActData(message.getData().getId());
            }else {
                throw new Exception("Unknown action");
            }

            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
        }catch (Exception e) {
            logger.error("Failed to process request ", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        }
        return res;

    }

    public Message getMitigationProjectActData(GetMitigationActDataReqMsg message, int user) {
        GetMitigationActDataResMsg res = new GetMitigationActDataResMsg();

        try {
            if (message == null || message.getBranchId() == 0 || message.getComId() == 0) {
                throw new Exception("Emtpty fields");
            }
            List<MitigationProjectActData> data = new ArrayList<>();
            if (message.getComId() > 0) {
               data = controller.getActDataByCompany(message.getComId());
            }
            if (message.getBranchId() > 0) {
                data = controller.getActDataByBranch(message.getBranchId());
            }
            MitigationActDataByCompany actData = new MitigationActDataByCompany();
            actData.setProjectsData(data);
            actData.setBranchId(message.getBranchId());
            actData.setComId(message.getComId());


            res.setActData(actData);
            res.setComId(message.getComId());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);

        }catch (Exception e) {
            logger.error("Failed to process request ", e);
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);

        }
        return res;

    }


}
