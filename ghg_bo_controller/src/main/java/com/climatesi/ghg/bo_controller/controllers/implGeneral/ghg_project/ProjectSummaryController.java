package com.climatesi.ghg.bo_controller.controllers.implGeneral.ghg_project;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;
import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.constants.PublicTransport_Keys;
import com.climatesi.ghg.api.constants.WasteType_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.api.facade.IncinerationFactorsManager;
import com.climatesi.ghg.api.facade.PublicTransportEmissionFactorsManager;
import com.climatesi.ghg.api.facade.WasteDisposalFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.summary_inputs.core.*;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.*;
import com.climatesi.ghg.emission_source.api.enums.*;
import com.climatesi.ghg.emission_source.api.enums.FuelTypes;
import com.climatesi.ghg.emission_source.api.facades.*;
import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;
import com.climatesi.ghg.implGeneral.facades.CompanyManagerFacade;
import com.climatesi.ghg.utility.api.enums.*;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Stateless
public class ProjectSummaryController extends AbstractController {


    private static Logger logger = Logger.getLogger(ProjectSummaryController.class);

    private BusinessAirTravelEntryManager businessAirTravelEntryManager;
    private ElectricityEntryManager electricityEntryManager;
    private EmployeCommutingEntryManager employeCommutingEntryManager;
    private FireExtingEntryManager fireExtingEntryManager;
    private GeneratorsEntryManager generatorsEntryManager;
    private MunicipalWaterEntryManager municipalWaterEntryManager;
    private RefrigerantsEntryManager refrigerantsEntryManager;
    private TransportLocalEntryManager transportLocalEntryManager;
    private VehicleEntryManager vehicleEntryManager;
    private WasteDisposalEntryManager wasteDisposalEntryManager;
    private WasteTransportEntryManager wasteTransportEntryManager;

    private BioMassEntryManager bioMassEntryManager;
    private LPGEntryEntryManager lpgEntryEntryManager;
    private AshTransportationEntryManager ashTransportationEntryManager;
    private ForkliftsEntryManager forkliftsEntryManager;
    private FurnaceEntryManager furnaceEntryManager;
    private OilAndGasTransportationEntryManager oilAndGasTransportationEntryManager;
    private LorryTransportationManager lorryTransportationManager;
    private PaperWasteManager paperWasteManager;
    private PaidManagerVehiclesEntryManager paidManagerVehiclesEntryManager;
    private RawMaterialTransportationLocalManager rawMaterialTransportationLocalManager;
    private SawDustTransportationManager sawDustTransportationManager;
    private SludgeTransportationManager sludgeTransportationManager;
    private VehicleOthersEntryManager vehicleOthersEntryManager;
    private SeaAirFreightFacade seaAirFreightFacade;
    private WasteDisposalFactorsManager  wasteDisposalFactorsManager;
    private StaffTransportationEntryManager staffTransportationEntryManager;

    private EmissionFactorsManager emissionFactorsManager;

    private CompanyManagerFacade companyManagerFacade;
    private IncinerationFactorsManager incinerationFactorsManager;
    private PublicTransportEmissionFactorsManager publicTransportEmissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {

        try {
            businessAirTravelEntryManager = EmsissionSourceFactory.getInstance().getBusinessAirTravelEntryManager(em);
            electricityEntryManager = EmsissionSourceFactory.getInstance().getElectricityEntryManager(em);
            employeCommutingEntryManager = EmsissionSourceFactory.getInstance().getEmployeeCommEntryManager(em);
            fireExtingEntryManager = EmsissionSourceFactory.getInstance().getFireExtingEntryManager(em);
            generatorsEntryManager = EmsissionSourceFactory.getInstance().getGeneratorsEntryManager(em);
            municipalWaterEntryManager = EmsissionSourceFactory.getInstance().getMunicipalWaterEntryManager(em);
            refrigerantsEntryManager = EmsissionSourceFactory.getInstance().getRefrigerantsEntryManager(em);
            transportLocalEntryManager = EmsissionSourceFactory.getInstance().getTransportLocalEntryManager(em);
            vehicleEntryManager = EmsissionSourceFactory.getInstance().getVehicleEntryManager(em);
            wasteDisposalEntryManager = EmsissionSourceFactory.getInstance().getWasteDisposalEntryManager(em);
            wasteTransportEntryManager = EmsissionSourceFactory.getInstance().getWasteTransportEntryManager(em);

            lpgEntryEntryManager = EmsissionSourceFactory.getInstance().getLPGEntryEntryManager(em);
            bioMassEntryManager = EmsissionSourceFactory.getInstance().getBioMassEntryManager(em);
            seaAirFreightFacade = EmsissionSourceFactory.getInstance().getSeaAirFreightFacadeManager(em);
            ashTransportationEntryManager = EmsissionSourceFactory.getInstance().getAshTransportationEntryManager(em);
            forkliftsEntryManager = EmsissionSourceFactory.getInstance().getForkliftsEntryManager(em);
            furnaceEntryManager = EmsissionSourceFactory.getInstance().getFurnaceEntryManager(em);
            oilAndGasTransportationEntryManager = EmsissionSourceFactory.getInstance().getOilAndGasTransportationEntryManager(em);
            lorryTransportationManager = EmsissionSourceFactory.getInstance().getLorryTransportationManager(em);
            paidManagerVehiclesEntryManager = EmsissionSourceFactory.getInstance().getPaidManagerVehiclesEntryManager(em);
            paperWasteManager = EmsissionSourceFactory.getInstance().getPaperWasteManager(em);
            rawMaterialTransportationLocalManager = EmsissionSourceFactory.getInstance().getRawMaterialTransportationLocalManager(em);
            sawDustTransportationManager = EmsissionSourceFactory.getInstance().getSawDustTransportationManager(em);
            sludgeTransportationManager = EmsissionSourceFactory.getInstance().getSludgeTransportationManager(em);
            vehicleOthersEntryManager = EmsissionSourceFactory.getInstance().getVehicleOthersEntryManager(em);
            staffTransportationEntryManager = EmsissionSourceFactory.getInstance().getStaffTransportationEntryManager(em);

            companyManagerFacade = (CompanyManagerFacade) InstitutionFactory.getInstance().getCompanyManager(em);

            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

            wasteDisposalFactorsManager = EmissionCalcFacotory.getInstance().getWasteDisposalFactorsManager(em);
            incinerationFactorsManager = EmissionCalcFacotory.getInstance().getIncinerationFactorsManager(em);
            publicTransportEmissionFactorsManager = EmissionCalcFacotory.getInstance().getPublicTransportEmissionFactorsManager(em);



        }catch (Exception e) {
            logger.error("Failed to initialize impl.facades" , e);
        }

    }



    public ProjectFullSummaryDTO getProjectSummary(int companyId) throws Exception {

        //find the project latest and year(start, end)

        //find company
        Company company = companyManagerFacade.getEntityByKey(companyId);
        if (company == null) throw new Exception("Company Cannot be null");

        int startingYear = company.getFyCurrentStart();
        int endingYear = company.getFyCurrentEnd();

        String fy = (endingYear == startingYear) ? "" + endingYear + "" : startingYear + "/" + (endingYear % 100);

        ProjectFullSummaryDTO summaryDTO = new ProjectFullSummaryDTO();

        summaryDTO.setFy(endingYear != startingYear);
        summaryDTO.setFy(fy);

        summaryDTO.setCompanyId(companyId);
        summaryDTO.setCompanyName(company.getName());
        summaryDTO.setMethodology(company.getMethodology());
        summaryDTO.setProtocol(company.getProtocol());
        summaryDTO.setExcludedReasons(company.getExcludedReasons().stream().map(reason -> {
            return  new ExcludedReasonDTO(reason.getSrcName(), reason.getReason());
        }).collect(Collectors.toList()));
        summaryDTO.setSuggestions(company.getProjectSuggestions().stream().map(suggestion -> {
            return new SuggestionDTO(suggestion.getTitle(), suggestion.getDescription());
        }).collect(Collectors.toList()));

        float directEmission = 0;
        float indirectEmission = 0;

        List<EmissionSrcDataDTO> directData = new ArrayList<>();
        List<EmissionSrcDataDTO> indirectData = new ArrayList<>();
        List<EmissionSrcDataDTO> unCategorizedData = new ArrayList<>();




        //alllowed emission sources checking
        Map<Integer, AllowedEmissionSrcBean> allowedEmissionSrcMap =  company.getAllowedEmissionSources();

        HashMap<String, EmissionSrcDataDTO> transMap = getTransportData(companyId, fy);

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.VehicleRented.getSrcId())) {



            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission +=  transMap.get("rented").getEmission();
                directData.add(transMap.get("rented"));

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehicleRented.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(transMap.get("rented"));
                indirectEmission += transMap.get("rented").getEmission() ;
            }

        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.RentedPaid.getSrcId())) {



            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.RentedPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission +=  transMap.get("rented_paid").getEmission();
                directData.add(transMap.get("rented_paid"));

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.RentedPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(transMap.get("rented_paid"));
                indirectEmission += transMap.get("rented_paid").getEmission() ;
            }

        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.RentedNotPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.RentedNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission +=  transMap.get("rented_not_paid").getEmission();
                directData.add(transMap.get("rented_not_paid"));

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.RentedNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(transMap.get("rented_not_paid"));
                indirectEmission += transMap.get("rented_not_paid").getEmission() ;
            }

        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.ComOwned.getSrcId())) {

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {

                directData.add(transMap.get("com_owned"));
                directEmission += transMap.get("com_owned").getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.ComOwned.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(transMap.get("com_owned"));
                indirectEmission += transMap.get("com_owned").getEmission();
            }

        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.OffRoad.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(transMap.get("off_road"));
                directEmission += transMap.get("off_road").getEmission();

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.OffRoad.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(transMap.get("off_road"));
                indirectEmission += transMap.get("off_road").getEmission();
            }

        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                 directData.add(transMap.get("hired_not_paid"));
                 directEmission += transMap.get("hired_not_paid").getEmission();

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehicleHiredNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(transMap.get("hired_not_paid"));
                indirectEmission += transMap.get("hired_not_paid").getEmission();
            }


        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.VehicleHiredPaid.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission += transMap.get("hired_paid").getEmission();
                directData.add(transMap.get("hired_paid"));
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehicleHiredPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectEmission += transMap.get("hired_paid").getEmission();
                indirectData.add(transMap.get("hired_paid"));
            }


        }




        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.Generator.getSrcId())) {

            EmissionSrcDataDTO genData = getGeneratorData(companyId, fy);


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(genData);
                directEmission += genData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.Generator.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {

                indirectData.add(genData);
                indirectEmission += genData.getEmission();
            }


        }

        HashMap<String, EmissionSrcDataDTO> empComData = getEmpCommData(companyId, fy);

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.EmpCommPaid.getSrcId())) {

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {

                directEmission += empComData.get("paid").getEmission();
                directData.add(empComData.get("paid"));

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.EmpCommPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectEmission +=  empComData.get("paid").getEmission();
                indirectData.add(empComData.get("paid"));

            }

        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.EmpCommNotPaid.getSrcId())) {
            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission += empComData.get("not_paid").getEmission();
                directData.add(empComData.get("not_paid"));
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.EmpCommNotPaid.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectEmission += empComData.get("not_paid").getEmission();
                indirectData.add(empComData.get("not_paid"));
            }

        }


//        --------------------------------------------------------------------------------------------------------------

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.Refri.getSrcId())) {

            EmissionSrcDataDTO refriData = getRefrigerantData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission += refriData.getEmission();
                directData.add(refriData);

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.Refri.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {

                indirectData.add(refriData);
                indirectEmission += refriData.getEmission();
            }


        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.FireExt.getSrcId())) {

            EmissionSrcDataDTO fireExtData = getFireExtinguisherData(companyId, fy);
            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {

                directEmission += fireExtData.getEmission();
                directData.add(fireExtData);
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.FireExt.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(fireExtData);
                indirectEmission += fireExtData.getEmission();
            }


        }



        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.LPG.getSrcId())) {

            EmissionSrcDataDTO lpgData = getLPGData(companyId, fy);



            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {

                directEmission += lpgData.getEmission();
                directData.add(lpgData);

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.LPG.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {

                indirectEmission += lpgData.getEmission();
                indirectData.add(lpgData);
            }

        }

        HashMap<String, EmissionSrcDataDTO> elecData = getElecData(companyId, fy);


        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.Elec.getSrcId())) {



            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(elecData.get("grid_elec"));
                directEmission += elecData.get("grid_elec").getEmission();

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.Elec.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(elecData.get("grid_elec"));
                indirectEmission += elecData.get("grid_elec").getEmission();
            }


        }
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.TD.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(elecData.get("td"));
                directEmission += elecData.get("td").getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.TD.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(elecData.get("td"));
                indirectEmission += elecData.get("td").getEmission();
            }


        }
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.WasteDisposal.getSrcId())) {

             EmissionSrcDataDTO wasteDisData = getWasteDisposalData(companyId, fy);
            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {

                directData.add(wasteDisData);
                directEmission += wasteDisData.getEmission();

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.WasteDisposal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {

                indirectData.add(wasteDisData);
                indirectEmission += wasteDisData.getEmission();
            }


        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.MunWater.getSrcId())) {

            EmissionSrcDataDTO munData = getMunWaterData(companyId, fy);


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(munData);
                directEmission += munData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.MunWater.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {

                indirectData.add(munData);
                indirectEmission += munData.getEmission();
            }


        }
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.AirTravel.getSrcId())) {

            EmissionSrcDataDTO airTravelData = getAirTravelData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission += airTravelData.getEmission();
                directData.add(airTravelData);
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.AirTravel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(airTravelData);
                indirectEmission += airTravelData.getEmission();
            }

        }

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.WasteTrans.getSrcId())) {

            EmissionSrcDataDTO wasteTransData = getWasteTransData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission += wasteTransData.getEmission();
                directData.add(wasteTransData);
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.WasteTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectEmission += wasteTransData.getEmission();
                indirectData.add(wasteTransData);
            }


        }


        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.TransLocPur.getSrcId())) {

            EmissionSrcDataDTO transLocPurData = getTransLocPurchData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directEmission += transLocPurData.getEmission();
                directData.add(transLocPurData);
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.TransLocPur.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectEmission += transLocPurData.getEmission();
                indirectData.add(transLocPurData);
            }


        }

        HashMap<String, EmissionSrcDataDTO> freightData = getFreightTransportData(companyId, fy);

        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.AirFreight.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                 directEmission += freightData.get("air").getEmission();
                 directData.add(freightData.get("air"));
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.AirFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                 indirectData.add(freightData.get("air"));
                 indirectEmission += freightData.get("air").getEmission();
            }


        }


        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.SeaFreight.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                 directData.add(freightData.get("sea"));
                 directEmission += freightData.get("sea").getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.SeaFreight.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                 indirectData.add(freightData.get("sea"));
                 indirectEmission += freightData.get("sea").getEmission();
            }


        }


        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.BioMass.getSrcId())) {
            EmissionSrcDataDTO bioMassData = getBioMassData(companyId, fy);

            unCategorizedData.add(bioMassData);
        }

        //ash trans
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.AshTrans.getSrcId())) {

            EmissionSrcDataDTO ashTransData = getAshTrans(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
               directData.add(ashTransData);
               directEmission += ashTransData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.AshTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(ashTransData);
                indirectEmission += ashTransData.getEmission();
            }


        }


        HashMap<String, EmissionSrcDataDTO>  forkliftsData = getForkliftsData(companyId, fy);

        //forklift pertorl
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.ForkliftsPetrol.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(forkliftsData.get("petrol"));
                directEmission += forkliftsData.get("petrol").getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.ForkliftsPetrol.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(forkliftsData.get("petrol"));
                indirectEmission += forkliftsData.get("petrol").getEmission();
            }


        }


        //forklifts diesel
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.ForkliftsDiesel.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(forkliftsData.get("diesel"));
                directEmission += forkliftsData.get("diesel").getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.ForkliftsDiesel.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(forkliftsData.get("diesel"));
                indirectEmission += forkliftsData.get("diesel").getEmission();
            }


        }

        //furnace oil
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.FurnaceOil.getSrcId())) {

            EmissionSrcDataDTO furnaceOilData =  getFurnaceOilData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(furnaceOilData);
                directEmission += furnaceOilData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.FurnaceOil.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(furnaceOilData);
                indirectEmission += furnaceOilData.getEmission();
            }


        }


        HashMap<String, EmissionSrcDataDTO> lorryTransData = getLorryTransData(companyId, fy);

        //lory tran in
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.LorryTransInternal.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
               directData.add(lorryTransData.get("internal"));
               directEmission += lorryTransData.get("internal").getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.LorryTransInternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(lorryTransData.get("internal"));
                indirectEmission += lorryTransData.get("internal").getEmission();
            }


        }


        //lorry tran ex
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.LorryTransExternal.getSrcId())) {


            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(lorryTransData.get("external"));
                directEmission += lorryTransData.get("external").getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.LorryTransExternal.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(lorryTransData.get("external"));
                indirectEmission += lorryTransData.get("external").getEmission();
            }


        }

        //oil gas trans
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.OilGasTrans.getSrcId())) {

            EmissionSrcDataDTO oilGasTransData = getOilGasTransData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
               directData.add(oilGasTransData);
               directEmission += oilGasTransData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.OilGasTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(oilGasTransData);
                indirectEmission += oilGasTransData.getEmission();
            }


        }

        //paid manager vehicle
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.PaidManagerVehicle.getSrcId())) {

            EmissionSrcDataDTO paidManagerVehiclData = getPaidManagerVehicleData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(paidManagerVehiclData);
                directEmission += paidManagerVehiclData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.PaidManagerVehicle.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(paidManagerVehiclData);
                indirectEmission += paidManagerVehiclData.getEmission();
            }


        }

        //paper waste
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.PaperWaste.getSrcId())) {

            EmissionSrcDataDTO paperWaste = getPaperWasteData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(paperWaste);
                directEmission += paperWaste.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.PaperWaste.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
               indirectData.add(paperWaste);
               indirectEmission += paperWaste.getEmission();
            }


        }

        //raw mat trans
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.RawMatTrans.getSrcId())) {

            EmissionSrcDataDTO rawMatTransData = getRawMatTransData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
               directData.add(rawMatTransData);
               directEmission += rawMatTransData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.RawMatTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(rawMatTransData);
                indirectEmission += rawMatTransData.getEmission();
            }


        }


        //saw dust trans
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.SawDustTrans.getSrcId())) {


            EmissionSrcDataDTO sawDustTransData = getSawDustTransData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(sawDustTransData);
                directEmission += sawDustTransData.getEmission();

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.SawDustTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
               indirectData.add(sawDustTransData);
               indirectEmission += sawDustTransData.getEmission();
            }


        }

        //staff trans
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.StaffTrans.getSrcId())) {


            EmissionSrcDataDTO staffTransData = getStaffTransData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.StaffTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(staffTransData);
                directEmission += staffTransData.getEmission();

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.StaffTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(staffTransData);
                indirectEmission += staffTransData.getEmission();
            }


        }

        //sludge trans
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.SludgeTrans.getSrcId())) {

            EmissionSrcDataDTO sludgeTransData = getSludgeTransData(companyId, fy);
            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(sludgeTransData);
                directEmission += sludgeTransData.getEmission();
            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.SludgeTrans.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
               indirectData.add(sludgeTransData);
               indirectEmission += sludgeTransData.getEmission();
            }

        }

        //vehicle others
        if (allowedEmissionSrcMap.containsKey(EmissionSourceEnum.VehiclesOthers.getSrcId())) {


            EmissionSrcDataDTO vehicleOtherData = getVehicleOthersData(companyId, fy);

            //direct
            if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Direct.getCat()) {
                directData.add(vehicleOtherData);
                directEmission += vehicleOtherData.getEmission();

            }else if (allowedEmissionSrcMap.get(EmissionSourceEnum.VehiclesOthers.getSrcId()).getDirect() == DirectCatEnum.Indirect.getCat()) {
                indirectData.add(vehicleOtherData);
                indirectEmission += vehicleOtherData.getEmission();
            }


        }


        summaryDTO.setTotalEmission(directEmission + indirectEmission);
        summaryDTO.setTotalDirectEmission(directEmission);
        summaryDTO.setTotalIndirectEmission(indirectEmission);
        summaryDTO.setIndirectSources(indirectData);
        summaryDTO.setDirectSources(directData);
        summaryDTO.setUnCategorized(unCategorizedData);

        return summaryDTO;

    }


    public HashMap<String, EmissionSrcDataDTO> getFreightTransportData(int companyId, String fy) {
        EmissionSrcDataDTO airFreight =  new EmissionSrcDataDTO();
        EmissionSrcDataDTO seaFreight = new EmissionSrcDataDTO();

        airFreight.setSrcName("Air Freight Transport");
        seaFreight.setSrcName("Sea Freight Transport");

        List<EmissionFactorDTO> efsSea = new ArrayList<>();
        List<ActivityDataDTO> actDataSea = new ArrayList<>();

        List<EmissionFactorDTO> efsAir = new ArrayList<>();
        List<ActivityDataDTO> actDataAir = new ArrayList<>();


        EmissionFactors cfNauticMilesToKm = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CF_NAUTIC_KM);
        EmissionFactors efCO2AirFreightRange1 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_AIR_FREIGHT_RANGE1);
        EmissionFactors efCO2AirFreightRange2 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_AIR_FREIGHT_RANGE2);
        EmissionFactors efCO2AirFreightRange3 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_AIR_FREIGHT_RANGE3);
        EmissionFactors efCO2SeaFreight =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CO2_EF_SEA_FREIGHT);

        EmissionFactorDTO ef_cf =  new EmissionFactorDTO();
        ef_cf.setName(cfNauticMilesToKm.getDescription());
        ef_cf.setUnitStr(cfNauticMilesToKm.getUnitStr());
        ef_cf.setUnit(cfNauticMilesToKm.getUnitId());
        ef_cf.setValue(cfNauticMilesToKm.getValue());
        efsAir.add(ef_cf);

        EmissionFactorDTO ef_air_range1 =  new EmissionFactorDTO();
        ef_air_range1.setName(efCO2AirFreightRange1.getDescription());
        ef_air_range1.setUnitStr(efCO2AirFreightRange1.getUnitStr());
        ef_air_range1.setUnit(efCO2AirFreightRange1.getUnitId());
        ef_air_range1.setValue(efCO2AirFreightRange1.getValue());
        efsAir.add(ef_air_range1);

        EmissionFactorDTO ef_air_range2 =  new EmissionFactorDTO();
        ef_air_range2.setName(efCO2AirFreightRange2.getDescription());
        ef_air_range2.setUnitStr(efCO2AirFreightRange2.getUnitStr());
        ef_air_range2.setUnit(efCO2AirFreightRange2.getUnitId());
        ef_air_range2.setValue(efCO2AirFreightRange2.getValue());
        efsAir.add(ef_air_range2);

        EmissionFactorDTO ef_air_range3 =  new EmissionFactorDTO();
        ef_air_range3.setName(efCO2AirFreightRange3.getDescription());
        ef_air_range3.setUnitStr(efCO2AirFreightRange3.getUnitStr());
        ef_air_range3.setUnit(efCO2AirFreightRange3.getUnitId());
        ef_air_range3.setValue(efCO2AirFreightRange3.getValue());
        efsAir.add(ef_air_range3);





        EmissionFactorDTO ef_sea =  new EmissionFactorDTO();
        ef_sea.setName(efCO2SeaFreight.getDescription());
        ef_sea.setUnitStr(efCO2SeaFreight.getUnitStr());
        ef_sea.setUnit(efCO2SeaFreight.getUnitId());
        ef_sea.setValue(efCO2SeaFreight.getValue());

        efsSea.add(ef_cf);
        efsSea.add(ef_sea);

        //act data
        //act data
        List<SeaAirFreightEntry> entries = seaAirFreightFacade.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emissionAirFreight = 0;
            float emissionSeaFreight = 0;

            Float [] quantitySea = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] quantityAir = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (SeaAirFreightEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;

                String emissionStr = entry.getEmissionDetails().get("tco2");


                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {


                        if (entry.getIsAirFreight() ==  1) {

                            emissionAirFreight +=emissionNum;
                            quantityAir[entry.getMonth()] += entry.getFreightKg(); //tons
                        }else {
                            emissionSeaFreight +=  emissionNum;
                            quantitySea[entry.getMonth()] += entry.getFreightKg(); //tons
                        }
                    }

                }






            }

            ActivityDataDTO seaFreightData = new ActivityDataDTO();
            ActivityDataDTO airFreightData = new ActivityDataDTO();


            seaFreightData.setUnit(UnitEnum.Tons.getId());
            seaFreightData.setUnitStr("Tons");
            seaFreightData.setValues(Arrays.asList(quantitySea));
            seaFreightData.setMetricsName("Amount");

            airFreightData.setUnit(UnitEnum.Tons.getId());
            airFreightData.setUnitStr("Tons");
            airFreightData.setValues(Arrays.asList(quantitySea));
            airFreightData.setMetricsName("Amount");

           airFreight.setEmission(emissionAirFreight);
           seaFreight.setEmission(emissionSeaFreight);

           actDataAir.add(airFreightData);
           actDataSea.add(seaFreightData);


        }



        airFreight.setActivityData(actDataAir);
        airFreight.setEmissionFactors(efsAir);

        seaFreight.setEmissionFactors(efsSea);
        seaFreight.setActivityData(actDataSea);


        HashMap<String, EmissionSrcDataDTO> data = new HashMap<>();
        data.put("air", airFreight);
        data.put("sea", seaFreight);
        return data;
    }


    public EmissionSrcDataDTO getTransLocPurchData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Transport Locally Purchased");

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        EmissionFactors ef_co2_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE);
        EmissionFactors ef_co2_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors ef_ch4_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors ef_ch4_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE);
        EmissionFactors ef_n20_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE);
        EmissionFactors ef_n20_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);

        EmissionFactors den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);

        EmissionFactors den_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);
        EmissionFactors net_cal_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);

        EmissionFactorDTO ef_p_ncv =  new EmissionFactorDTO();
        ef_p_ncv.setName(net_cal_petrol.getDescription());
        ef_p_ncv.setUnitStr(net_cal_petrol.getUnitStr());
        ef_p_ncv.setUnit(net_cal_petrol.getUnitId());
        ef_p_ncv.setValue(net_cal_petrol.getValue());

        efs.add(ef_p_ncv);

        EmissionFactorDTO ef_p_co2 =  new EmissionFactorDTO();
        ef_p_co2.setName(ef_co2_petrol.getDescription());
        ef_p_co2.setUnitStr(ef_co2_petrol.getUnitStr());
        ef_p_co2.setUnit(ef_co2_petrol.getUnitId());
        ef_p_co2.setValue(ef_co2_petrol.getValue());

        efs.add(ef_p_co2);

        EmissionFactorDTO ef_p_ch4 =  new EmissionFactorDTO();
        ef_p_ch4.setName(ef_ch4_petrol.getDescription());
        ef_p_ch4.setUnitStr(ef_ch4_petrol.getUnitStr());
        ef_p_ch4.setUnit(ef_ch4_petrol.getUnitId());
        ef_p_ch4.setValue(ef_ch4_petrol.getValue());

        efs.add(ef_p_ch4);

        EmissionFactorDTO ef_p_n20 =  new EmissionFactorDTO();
        ef_p_n20.setName(ef_n20_petrol.getDescription());
        ef_p_n20.setUnitStr(ef_n20_petrol.getUnitStr());
        ef_p_n20.setUnit(ef_n20_petrol.getUnitId());
        ef_p_n20.setValue(ef_n20_petrol.getValue());

        efs.add(ef_p_n20);

        EmissionFactorDTO ef_p_density =  new EmissionFactorDTO();
        ef_p_density.setName(den_petrol.getDescription());
        ef_p_density.setUnitStr(den_petrol.getUnitStr());
        ef_p_density.setUnit(den_petrol.getUnitId());
        ef_p_density.setValue(den_petrol.getValue());

        efs.add(ef_p_density);

        EmissionFactorDTO ef_d_ncv =  new EmissionFactorDTO();
        ef_d_ncv.setName(net_cal_diesel.getDescription());
        ef_d_ncv.setUnitStr(net_cal_diesel.getUnitStr());
        ef_d_ncv.setUnit(net_cal_diesel.getUnitId());
        ef_d_ncv.setValue(net_cal_diesel.getValue());

        efs.add(ef_d_ncv);

        EmissionFactorDTO ef_d_co2 =  new EmissionFactorDTO();
        ef_d_co2.setName(ef_co2_diesel.getDescription());
        ef_d_co2.setUnitStr(ef_co2_diesel.getUnitStr());
        ef_d_co2.setUnit(ef_co2_diesel.getUnitId());
        ef_d_co2.setValue(ef_co2_diesel.getValue());

        efs.add(ef_d_co2);

        EmissionFactorDTO ef_d_ch4 =  new EmissionFactorDTO();
        ef_d_ch4.setName(ef_ch4_diesel.getDescription());
        ef_d_ch4.setUnitStr(ef_ch4_diesel.getUnitStr());
        ef_d_ch4.setUnit(ef_ch4_diesel.getUnitId());
        ef_d_ch4.setValue(ef_ch4_diesel.getValue());

        efs.add(ef_d_ch4);

        EmissionFactorDTO ef_d_n20 =  new EmissionFactorDTO();
        ef_d_n20.setName(ef_n20_diesel.getDescription());
        ef_d_n20.setUnitStr(ef_n20_diesel.getUnitStr());
        ef_d_n20.setUnit(ef_n20_diesel.getUnitId());
        ef_d_n20.setValue(ef_n20_diesel.getValue());

        efs.add(ef_d_n20);

        EmissionFactorDTO ef_d_density =  new EmissionFactorDTO();
        ef_d_density.setName(den_diesel.getDescription());
        ef_d_density.setUnitStr(den_diesel.getUnitStr());
        ef_d_density.setUnit(den_diesel.getUnitId());
        ef_d_density.setValue(den_diesel.getValue());

        efs.add(ef_d_density);

        EmissionFactorDTO ef_gwp_co2=  new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setValue(gwp_co2.getValue());

        efs.add(ef_gwp_co2);

        EmissionFactorDTO ef_gwp_ch4 =  new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());

        efs.add(ef_gwp_ch4);

        EmissionFactorDTO ef_gwp_n20 =  new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setValue(gwp_n20.getValue());

        efs.add(ef_p_ncv);

        //act data
        List<TransportLocalEntry> entries = transportLocalEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] petrolConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};


            for (TransportLocalEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;


                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {


                        emission += emissionNum;
                    }

                }
                if (entry.getFuelType() == 1) {
                    petrolConsumption[entry.getMonth()] += entry.getFuelConsumption();
                }else {
                    dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
                }







            }

            ActivityDataDTO dieselData = new ActivityDataDTO();
            ActivityDataDTO petrolData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            petrolData.setUnit(UnitEnum.Liters.getId());
            petrolData.setUnitStr("Liters");
            petrolData.setValues(Arrays.asList(petrolConsumption));
            petrolData.setMetricsName("Petrol Consumption");



            data.setEmission(emission);

            actData.add(petrolData);
            actData.add(dieselData);


        }

        data.setEmissionFactors(efs);
        data.setActivityData(actData);



        return data;
    }

    public HashMap<String, EmissionSrcDataDTO> getTransportData(int companyId, String fy) {
        HashMap<String, EmissionSrcDataDTO> data = new HashMap<>();

        EmissionSrcDataDTO transRented = new EmissionSrcDataDTO();
        EmissionSrcDataDTO transRentedPaid = new EmissionSrcDataDTO();
        EmissionSrcDataDTO transRentedNotPaid = new EmissionSrcDataDTO();
        EmissionSrcDataDTO transHiredPaid = new EmissionSrcDataDTO();
        EmissionSrcDataDTO transHiredNotPaid = new EmissionSrcDataDTO();
        EmissionSrcDataDTO offRoad = new EmissionSrcDataDTO();
        EmissionSrcDataDTO comOwned = new EmissionSrcDataDTO();

        transRented.setSrcName("Transport Rented");
        transRentedPaid.setSrcName("Transport Rented, Paid");
        transRentedNotPaid.setSrcName("Transport Rented, Not Paid");
        transHiredPaid.setSrcName("Transport Hired, Paid");
        transHiredNotPaid.setSrcName("Transport Hired, Not Paid");
        offRoad.setSrcName("Off-road Vehicles");
        comOwned.setSrcName("Company Owned Vehicles");

        List<EmissionFactorDTO> efsRented = new ArrayList<>();
        List<ActivityDataDTO> actDataRented = new ArrayList<>();

        List<EmissionFactorDTO> efsRentedPaid = new ArrayList<>();
        List<ActivityDataDTO> actDataRentedPaid = new ArrayList<>();

        List<EmissionFactorDTO> efsRentedNotPaid = new ArrayList<>();
        List<ActivityDataDTO> actDataRentedNotPaid = new ArrayList<>();

        List<EmissionFactorDTO> efsHiredPaid = new ArrayList<>();
        List<ActivityDataDTO> actDataHiredPaid = new ArrayList<>();

        List<EmissionFactorDTO> efsHiredNotPaid = new ArrayList<>();
        List<ActivityDataDTO> actDataHiredNotPaid = new ArrayList<>();

        List<EmissionFactorDTO> efsOffRoad = new ArrayList<>();
        List<ActivityDataDTO> actDataOffRoad = new ArrayList<>();

        List<EmissionFactorDTO> efsComOwned = new ArrayList<>();
        List<ActivityDataDTO> actDataComOwned = new ArrayList<>();


        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        EmissionFactors ef_co2_m_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE);
        EmissionFactors ef_co2_m_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors ef_ch4_m_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors ef_ch4_m_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE);
        EmissionFactors ef_n20_m_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE);
        EmissionFactors ef_n20_m_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);

        EmissionFactors ef_co2_o_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_GASOLINE);
        EmissionFactors ef_co2_o_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_DIESEL);
        EmissionFactors ef_ch4_o_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_DIESEL);
        EmissionFactors ef_ch4_o_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_GASOLINE);
        EmissionFactors ef_n20_o_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_GASOLINE);
        EmissionFactors ef_n20_o_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_DIESEL);


        EmissionFactors den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);

        EmissionFactors den_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);
        EmissionFactors net_cal_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL);

        EmissionFactors price_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_DIESEL_LITER);
        EmissionFactors price_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_PETROL_LITER);

        EmissionFactors price_diesel_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_DIESEL_LITER_CALENDER);
        EmissionFactors price_petrol_cal = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_PETROL_LITER_CALENDER);

        EmissionFactorDTO ef_p_ncv = new EmissionFactorDTO();
        ef_p_ncv.setName(net_cal_petrol.getDescription());
        ef_p_ncv.setUnitStr(net_cal_petrol.getUnitStr());
        ef_p_ncv.setUnit(net_cal_petrol.getUnitId());
        ef_p_ncv.setValue(net_cal_petrol.getValue());

        efsRented.add(ef_p_ncv);

        EmissionFactorDTO ef_p_co2 = new EmissionFactorDTO();
        ef_p_co2.setName(ef_co2_m_petrol.getDescription());
        ef_p_co2.setUnitStr(ef_co2_m_petrol.getUnitStr());
        ef_p_co2.setUnit(ef_co2_m_petrol.getUnitId());
        ef_p_co2.setValue(ef_co2_m_petrol.getValue());

        efsRented.add(ef_p_co2);

        EmissionFactorDTO ef_p_ch4 = new EmissionFactorDTO();
        ef_p_ch4.setName(ef_ch4_m_petrol.getDescription());
        ef_p_ch4.setUnitStr(ef_ch4_m_petrol.getUnitStr());
        ef_p_ch4.setUnit(ef_ch4_m_petrol.getUnitId());
        ef_p_ch4.setValue(ef_ch4_m_petrol.getValue());

        efsRented.add(ef_p_ch4);

        EmissionFactorDTO ef_p_n20 = new EmissionFactorDTO();
        ef_p_n20.setName(ef_n20_m_petrol.getDescription());
        ef_p_n20.setUnitStr(ef_n20_m_petrol.getUnitStr());
        ef_p_n20.setUnit(ef_n20_m_petrol.getUnitId());
        ef_p_n20.setValue(ef_n20_m_petrol.getValue());

        efsRented.add(ef_p_n20);

        EmissionFactorDTO ef_p_density = new EmissionFactorDTO();
        ef_p_density.setName(den_petrol.getDescription());
        ef_p_density.setUnitStr(den_petrol.getUnitStr());
        ef_p_density.setUnit(den_petrol.getUnitId());
        ef_p_density.setValue(den_petrol.getValue());

        efsRented.add(ef_p_density);

        EmissionFactorDTO ef_d_ncv = new EmissionFactorDTO();
        ef_d_ncv.setName(net_cal_diesel.getDescription());
        ef_d_ncv.setUnitStr(net_cal_diesel.getUnitStr());
        ef_d_ncv.setUnit(net_cal_diesel.getUnitId());
        ef_d_ncv.setValue(net_cal_diesel.getValue());

        efsRented.add(ef_d_ncv);

        EmissionFactorDTO ef_d_co2 = new EmissionFactorDTO();
        ef_d_co2.setName(ef_co2_m_diesel.getDescription());
        ef_d_co2.setUnitStr(ef_co2_m_diesel.getUnitStr());
        ef_d_co2.setUnit(ef_co2_m_diesel.getUnitId());
        ef_d_co2.setValue(ef_co2_m_diesel.getValue());

        efsRented.add(ef_d_co2);

        EmissionFactorDTO ef_d_ch4 = new EmissionFactorDTO();
        ef_d_ch4.setName(ef_ch4_m_diesel.getDescription());
        ef_d_ch4.setUnitStr(ef_ch4_m_diesel.getUnitStr());
        ef_d_ch4.setUnit(ef_ch4_m_diesel.getUnitId());
        ef_d_ch4.setValue(ef_ch4_m_diesel.getValue());

        efsRented.add(ef_d_ch4);

        EmissionFactorDTO ef_d_n20 = new EmissionFactorDTO();
        ef_d_n20.setName(ef_n20_m_diesel.getDescription());
        ef_d_n20.setUnitStr(ef_n20_m_diesel.getUnitStr());
        ef_d_n20.setUnit(ef_n20_m_diesel.getUnitId());
        ef_d_n20.setValue(ef_n20_m_diesel.getValue());

        efsRented.add(ef_d_n20);

        EmissionFactorDTO ef_d_density = new EmissionFactorDTO();
        ef_d_density.setName(den_diesel.getDescription());
        ef_d_density.setUnitStr(den_diesel.getUnitStr());
        ef_d_density.setUnit(den_diesel.getUnitId());
        ef_d_density.setValue(den_diesel.getValue());

        efsRented.add(ef_d_density);

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setValue(gwp_co2.getValue());

        efsRented.add(ef_gwp_co2);

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());

        efsRented.add(ef_gwp_ch4);

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setValue(gwp_n20.getValue());

        efsRented.add(ef_p_ncv);

        efsHiredNotPaid = new ArrayList<>(efsRented);
        efsHiredPaid = new ArrayList<>(efsRented);
        efsComOwned = new ArrayList<>(efsRented);
        efsOffRoad = new ArrayList<>(efsRented);
        efsRentedPaid = new ArrayList<>(efsRented);
        efsRentedNotPaid = new ArrayList<>(efsRented);


        //act data
        List<VehicleEntry> entries = vehicleEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emissionRented = 0;
            float emissionRentedPaid = 0;
            float emissionRentedNotPaid = 0;
            float emissionHiredPaid = 0;
            float emissionHiredNotPaid = 0;
            float emissionComOwned = 0;
            float emissionOffRoad = 0;

            Float[] dieselConsumptionRented = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float[] petrolConsumptionRented = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float[] dieselConsumptionRentedPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float[] petrolConsumptionRentedPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float[] dieselConsumptionRentedNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float[] petrolConsumptionRentedNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float[] dieselConsumptionHiredPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float[] petrolConsumptionHiredPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float[] dieselConsumptionHiredNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float[] petrolConsumptionHiredNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float[] dieselConsumptionComOwned = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float[] petrolConsumptionComOwned = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float[] dieselConsumptionOffRoad = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float[] petrolConsumptionOffRoad = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};


            for (VehicleEntry entry : entries) {
                if (entry == null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0){
                    continue;
                }
                String emissionStr = entry.getEmissionDetails().get("tco2");

                float emission = 0f;
                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                if (entry.getVehicleModel() == VehicleTypes.AirportGroudSupportEquipment.type || entry.getVehicleModel() == VehicleTypes.ForkLifts.type ||
                        entry.getVehicleModel() == VehicleTypes.Chain_saws.type ||
                        entry.getVehicleModel() == VehicleTypes.Agriculture_Tractors.type ||
                        entry.getVehicleModel() == VehicleTypes.Other_Offroad.type) {


                    emissionOffRoad += emission;


                    if (entry.getFuelType() == FuelTypes.Diesel.type) {
                        if (entry.getUnits() == Units.liters.unit) {
                            logger.info("#########################  " + entry == null);
                            logger.info("######################### " + dieselConsumptionOffRoad.length);
                            logger.info("########################## " + entry.getMonth());
                            logger.info("#########################   " + entry.getConsumption());

                            dieselConsumptionOffRoad[entry.getMonth()] = dieselConsumptionOffRoad[entry.getMonth()] + entry.getConsumption();
                        } else if (entry.getUnits() == Units.m3.unit) {
                            dieselConsumptionOffRoad[entry.getMonth()] += (entry.getConsumption() * 1000);
                        } else if (entry.getUnits() == Units.LKR.unit) {

                        }

                    } else if (entry.getFuelType() == FuelTypes.Petrol.type) {
                        if (entry.getUnits() == Units.liters.unit) {
                            petrolConsumptionOffRoad[entry.getMonth()] += entry.getConsumption();
                        } else if (entry.getUnits() == Units.m3.unit) {
                            petrolConsumptionOffRoad[entry.getMonth()] += (entry.getConsumption() * 1000);
                        } else if (entry.getUnits() == Units.LKR.unit) {

                        }

                    }

                }
                else {
                    if (entry.getVehicleCategory() == VehicleCategory.COMPANY_OWNED.category) {

                        emissionComOwned += emission;

                        if (entry.getFuelType() == FuelTypes.Diesel.type) {
                            if (entry.getUnits() == Units.liters.unit) {
                                dieselConsumptionComOwned[entry.getMonth()] += entry.getConsumption();

                            } else if (entry.getUnits() == Units.m3.unit) {
                                dieselConsumptionComOwned[entry.getMonth()] += (entry.getConsumption() * 1000);
                            } else if (entry.getUnits() == Units.LKR.unit) {

                            }

                        } else if (entry.getFuelType() == FuelTypes.Petrol.type) {
                            if (entry.getUnits() == Units.liters.unit) {
                                petrolConsumptionComOwned[entry.getMonth()] += entry.getConsumption();

                            } else if (entry.getUnits() == Units.m3.unit) {
                                petrolConsumptionComOwned[entry.getMonth()] += (entry.getConsumption() * 1000);
                            } else if (entry.getUnits() == Units.LKR.unit) {

                            }

                        }

                        //new
                        else if (entry.getFuelType() == FuelTypes.LP_92.type) {
                            if (entry.getUnits() == Units.liters.unit) {
                                petrolConsumptionComOwned[entry.getMonth()] += entry.getConsumption();

                            } else if (entry.getUnits() == Units.m3.unit) {
                                petrolConsumptionComOwned[entry.getMonth()] += (entry.getConsumption() * 1000);
                            } else if (entry.getUnits() == Units.LKR.unit) {

                            }

                        }

                        else if (entry.getFuelType() == FuelTypes.LSD.type) {
                            if (entry.getUnits() == Units.liters.unit) {
                                dieselConsumptionComOwned[entry.getMonth()] += entry.getConsumption();

                            } else if (entry.getUnits() == Units.m3.unit) {
                                dieselConsumptionComOwned[entry.getMonth()] += (entry.getConsumption() * 1000);
                            } else if (entry.getUnits() == Units.LKR.unit) {

                            }
                        }
                    } else if (entry.getVehicleCategory() == VehicleCategory.RENTED.category) {

                        emissionRented += emission;
                        if (entry.isFuelPaidbyCompany()) {
                            emissionRentedPaid += emission;

                            if (entry.getFuelType() == FuelTypes.Diesel.type) {
                                if (entry.getUnits() == Units.liters.unit) {
                                    dieselConsumptionRentedPaid[entry.getMonth()] += entry.getConsumption();

                                } else if (entry.getUnits() == Units.m3.unit) {
                                    dieselConsumptionRentedPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                } else if (entry.getUnits() == Units.LKR.unit) {

                                }

                            } else if (entry.getFuelType() == FuelTypes.Petrol.type) {
                                if (entry.getUnits() == Units.liters.unit) {
                                    petrolConsumptionRentedPaid[entry.getMonth()] += entry.getConsumption();

                                } else if (entry.getUnits() == Units.m3.unit) {
                                    petrolConsumptionRentedPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                } else if (entry.getUnits() == Units.LKR.unit) {

                                }

                            }
                        }else {
                            emissionRentedNotPaid += emission;


                            if (entry.getFuelType() == FuelTypes.Diesel.type) {
                                if (entry.getUnits() == Units.liters.unit) {
                                    dieselConsumptionRentedNotPaid[entry.getMonth()] += entry.getConsumption();

                                } else if (entry.getUnits() == Units.m3.unit) {
                                    dieselConsumptionRentedNotPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                } else if (entry.getUnits() == Units.LKR.unit) {

                                }

                            } else if (entry.getFuelType() == FuelTypes.Petrol.type) {
                                if (entry.getUnits() == Units.liters.unit) {
                                    petrolConsumptionRentedNotPaid[entry.getMonth()] += entry.getConsumption();

                                } else if (entry.getUnits() == Units.m3.unit) {
                                    petrolConsumptionRentedNotPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                } else if (entry.getUnits() == Units.LKR.unit) {

                                }

                            }
                        }

                        if (entry.getFuelType() == FuelTypes.Diesel.type) {
                            if (entry.getUnits() == Units.liters.unit) {
                                dieselConsumptionRented[entry.getMonth()] += entry.getConsumption();

                            } else if (entry.getUnits() == Units.m3.unit) {
                                dieselConsumptionRented[entry.getMonth()] += (entry.getConsumption() * 1000);
                            } else if (entry.getUnits() == Units.LKR.unit) {

                            }

                        } else if (entry.getFuelType() == FuelTypes.Petrol.type) {
                            if (entry.getUnits() == Units.liters.unit) {
                                petrolConsumptionRented[entry.getMonth()] += entry.getConsumption();

                            } else if (entry.getUnits() == Units.m3.unit) {
                                petrolConsumptionRented[entry.getMonth()] += (entry.getConsumption() * 1000);
                            } else if (entry.getUnits() == Units.LKR.unit) {

                            }

                        }
                    } else  {
                        if (entry.getVehicleCategory() == VehicleCategory.HIRED.category) {
                            if (entry.isFuelPaidbyCompany()) {

                                emissionHiredPaid +=  emission;

                                if (entry.getFuelType() == FuelTypes.Diesel.type) {
                                    if (entry.getUnits() == Units.liters.unit) {
                                        dieselConsumptionHiredPaid[entry.getMonth()] += entry.getConsumption();

                                    } else if (entry.getUnits() == Units.m3.unit) {
                                        dieselConsumptionHiredPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                    } else if (entry.getUnits() == Units.LKR.unit) {

                                    }

                                } else if (entry.getFuelType() == FuelTypes.Petrol.type) {
                                    if (entry.getUnits() == Units.liters.unit) {
                                        petrolConsumptionHiredPaid[entry.getMonth()] += entry.getConsumption();

                                    } else if (entry.getUnits() == Units.m3.unit) {
                                        petrolConsumptionHiredPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                    } else if (entry.getUnits() == Units.LKR.unit) {

                                    }

                                }
                            } else {
                                emissionHiredNotPaid += emission;

                                if (entry.getFuelType() == FuelTypes.Diesel.type) {
                                    if (entry.getUnits() == Units.liters.unit) {
                                        dieselConsumptionHiredNotPaid[entry.getMonth()] += entry.getConsumption();

                                    } else if (entry.getUnits() == Units.m3.unit) {
                                        dieselConsumptionHiredNotPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                    } else if (entry.getUnits() == Units.LKR.unit) {

                                    }

                                } else if (entry.getFuelType() == FuelTypes.Petrol.type) {
                                    if (entry.getUnits() == Units.liters.unit) {
                                        petrolConsumptionHiredNotPaid[entry.getMonth()] += entry.getConsumption();

                                    } else if (entry.getUnits() == Units.m3.unit) {
                                        petrolConsumptionHiredNotPaid[entry.getMonth()] += (entry.getConsumption() * 1000);
                                    } else if (entry.getUnits() == Units.LKR.unit) {

                                    }

                                }
                            }
                        }
                    }
                }

            }


                ActivityDataDTO dieselRentedData = new ActivityDataDTO();
                ActivityDataDTO petrolRentedData = new ActivityDataDTO();

                ActivityDataDTO dieselRentedPaidData = new ActivityDataDTO();
                ActivityDataDTO petrolRentedPaidData = new ActivityDataDTO();

                ActivityDataDTO dieselRentedNotPaidData = new ActivityDataDTO();
                ActivityDataDTO petrolRentedNotPaidData = new ActivityDataDTO();

                ActivityDataDTO dieselHiredPaidData = new ActivityDataDTO();
                ActivityDataDTO petrolHiredPaidData = new ActivityDataDTO();

                ActivityDataDTO dieselHiredNotPaidData = new ActivityDataDTO();
                ActivityDataDTO petrolHiredNotPaidData = new ActivityDataDTO();

                ActivityDataDTO dieselComOwnedData = new ActivityDataDTO();
                ActivityDataDTO petrolComOwnedData = new ActivityDataDTO();

                ActivityDataDTO dieselOffRoadData = new ActivityDataDTO();
                ActivityDataDTO petrolOffRoadData = new ActivityDataDTO();


                dieselRentedData.setUnit(UnitEnum.Liters.getId());
                dieselRentedData.setUnitStr("Liters");
                dieselRentedData.setValues(Arrays.asList(dieselConsumptionRented));
                dieselRentedData.setMetricsName("Diesel Consumption");

                petrolRentedData.setUnit(UnitEnum.Liters.getId());
                petrolRentedData.setUnitStr("Liters");
                petrolRentedData.setValues(Arrays.asList(petrolConsumptionRented));
                petrolRentedData.setMetricsName("Petrol Consumption");


                dieselRentedPaidData.setUnit(UnitEnum.Liters.getId());
                dieselRentedPaidData.setUnitStr("Liters");
                dieselRentedPaidData.setValues(Arrays.asList(dieselConsumptionRentedPaid));
                dieselRentedPaidData.setMetricsName("Diesel Consumption");

                petrolRentedPaidData.setUnit(UnitEnum.Liters.getId());
                petrolRentedPaidData.setUnitStr("Liters");
                petrolRentedPaidData.setValues(Arrays.asList(petrolConsumptionRentedPaid));
                petrolRentedPaidData.setMetricsName("Petrol Consumption");

                dieselRentedNotPaidData.setUnit(UnitEnum.Liters.getId());
                dieselRentedNotPaidData.setUnitStr("Liters");
                dieselRentedNotPaidData.setValues(Arrays.asList(dieselConsumptionRentedNotPaid));
                dieselRentedNotPaidData.setMetricsName("Diesel Consumption");

                petrolRentedNotPaidData.setUnit(UnitEnum.Liters.getId());
                petrolRentedNotPaidData.setUnitStr("Liters");
                petrolRentedNotPaidData.setValues(Arrays.asList(petrolConsumptionRentedNotPaid));
                petrolRentedNotPaidData.setMetricsName("Petrol Consumption");



                dieselHiredPaidData.setUnit(UnitEnum.Liters.getId());
                dieselHiredPaidData.setUnitStr("Liters");
                dieselHiredPaidData.setValues(Arrays.asList(dieselConsumptionHiredPaid));
                dieselHiredPaidData.setMetricsName("Diesel Consumption");

                petrolHiredPaidData.setUnit(UnitEnum.Liters.getId());
                petrolHiredPaidData.setUnitStr("Liters");
                petrolHiredPaidData.setValues(Arrays.asList(petrolConsumptionHiredPaid));
                petrolHiredPaidData.setMetricsName("Petrol Consumption");


                dieselHiredNotPaidData.setUnit(UnitEnum.Liters.getId());
                dieselHiredNotPaidData.setUnitStr("Liters");
                dieselHiredNotPaidData.setValues(Arrays.asList(dieselConsumptionHiredNotPaid));
                dieselHiredNotPaidData.setMetricsName("Diesel Consumption");

                petrolHiredNotPaidData.setUnit(UnitEnum.Liters.getId());
                petrolHiredNotPaidData.setUnitStr("Liters");
                petrolHiredNotPaidData.setValues(Arrays.asList(petrolConsumptionHiredNotPaid));
                petrolHiredNotPaidData.setMetricsName("Petrol Consumption");

                dieselComOwnedData.setUnit(UnitEnum.Liters.getId());
                dieselComOwnedData.setUnitStr("Liters");
                dieselComOwnedData.setValues(Arrays.asList(dieselConsumptionComOwned));
                dieselComOwnedData.setMetricsName("Diesel Consumption");

                petrolComOwnedData.setUnit(UnitEnum.Liters.getId());
                petrolComOwnedData.setUnitStr("Liters");
                petrolComOwnedData.setValues(Arrays.asList(petrolConsumptionComOwned));
                petrolComOwnedData.setMetricsName("Petrol Consumption");

                dieselOffRoadData.setUnit(UnitEnum.Liters.getId());
                dieselOffRoadData.setUnitStr("Liters");
                dieselOffRoadData.setValues(Arrays.asList(dieselConsumptionOffRoad));
                dieselOffRoadData.setMetricsName("Diesel Consumption");

                petrolOffRoadData.setUnit(UnitEnum.Liters.getId());
                petrolOffRoadData.setUnitStr("Liters");
                petrolOffRoadData.setValues(Arrays.asList(petrolConsumptionOffRoad));
                petrolOffRoadData.setMetricsName("Petrol Consumption");


                actDataRented.add(petrolRentedData);
                actDataRented.add(dieselRentedData);

                actDataRentedPaid.add(petrolRentedPaidData);
                actDataRentedPaid.add(dieselRentedPaidData);

                actDataRentedNotPaid.add(petrolRentedNotPaidData);
                actDataRentedNotPaid.add(dieselRentedNotPaidData);


                actDataHiredPaid.add(petrolHiredPaidData);
                actDataHiredPaid.add(dieselHiredPaidData);

                actDataHiredNotPaid.add(petrolHiredNotPaidData);
                actDataHiredNotPaid.add(dieselHiredNotPaidData);

                actDataOffRoad.add(petrolOffRoadData);
                actDataOffRoad.add(dieselOffRoadData);

                actDataComOwned.add(petrolComOwnedData);
                actDataComOwned.add(dieselComOwnedData);


                transRented.setEmission(emissionRented);
                transRentedPaid.setEmission(emissionRentedPaid);
                transRentedNotPaid.setEmission(emissionRentedNotPaid);
                transHiredPaid.setEmission(emissionHiredPaid);
                transHiredNotPaid.setEmission(emissionHiredNotPaid);
                comOwned.setEmission(emissionComOwned);
                offRoad.setEmission(emissionOffRoad);


        }








        transRented.setEmissionFactors(efsRented);
        transRented.setActivityData(actDataRented);

        transRentedPaid.setEmissionFactors(efsRentedPaid);
        transRentedPaid.setActivityData(actDataRentedPaid);

        transRentedNotPaid.setEmissionFactors(efsRentedNotPaid);
        transRentedNotPaid.setActivityData(actDataRentedNotPaid);

        transHiredPaid.setEmissionFactors(efsHiredPaid);
        transHiredPaid.setActivityData(actDataHiredPaid);

        transHiredNotPaid.setEmissionFactors(efsHiredNotPaid);
        transHiredNotPaid.setActivityData(actDataHiredNotPaid);

        offRoad.setEmissionFactors(efsOffRoad);
        offRoad.setActivityData(actDataOffRoad);

        comOwned.setEmissionFactors(efsComOwned);
        comOwned.setActivityData(actDataComOwned);

        data.put("rented", transRented);
        data.put("hired_paid", transHiredPaid);
        data.put("hired_not_paid", transHiredNotPaid);
        data.put("off_road", offRoad);
        data.put("com_owned", comOwned);
        data.put("rented_paid", transRentedPaid);
        data.put("rented_not_paid", transRentedNotPaid);
        return data;
    }


    public EmissionSrcDataDTO getGeneratorData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Diesel Generators");

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactors den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors ef_co2_diesel_sta = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_STATIONARY_DIESEL);
        EmissionFactors ef_ch4_diesel_sta = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_STATIONARY_DIESEL);
        EmissionFactors ef_n20_diesel_sta = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N2O_STATIONARY_DIESEL);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        EmissionFactorDTO ef_ncv =  new EmissionFactorDTO();
        ef_ncv.setName(net_cal_diesel.getDescription());
        ef_ncv.setUnitStr(net_cal_diesel.getUnitStr());
        ef_ncv.setUnit(net_cal_diesel.getUnitId());
        ef_ncv.setValue(net_cal_diesel.getValue());

        efs.add(ef_ncv);


        EmissionFactorDTO ef_efco2 =  new EmissionFactorDTO();
        ef_efco2.setName(ef_co2_diesel_sta.getDescription());
        ef_efco2.setUnitStr(ef_co2_diesel_sta.getUnitStr());
        ef_efco2.setUnit(ef_co2_diesel_sta.getUnitId());
        ef_efco2.setValue(ef_co2_diesel_sta.getValue());

        efs.add(ef_efco2);

        EmissionFactorDTO ef_efch4 =  new EmissionFactorDTO();
        ef_efch4.setName(ef_ch4_diesel_sta.getDescription());
        ef_efch4.setUnitStr(ef_ch4_diesel_sta.getUnitStr());
        ef_efch4.setUnit(ef_ch4_diesel_sta.getUnitId());
        ef_efch4.setValue(ef_ch4_diesel_sta.getValue());

        efs.add(ef_efch4);

        EmissionFactorDTO ef_efn2o =  new EmissionFactorDTO();
        ef_efn2o.setName(ef_n20_diesel_sta.getDescription());
        ef_efn2o.setUnitStr(ef_n20_diesel_sta.getUnitStr());
        ef_efn2o.setUnit(ef_n20_diesel_sta.getUnitId());
        ef_efn2o.setValue(ef_n20_diesel_sta.getValue());

        efs.add(ef_efn2o);

        EmissionFactorDTO ef_density =  new EmissionFactorDTO();
        ef_density.setName(den_diesel.getDescription());
        ef_density.setUnitStr(den_diesel.getUnitStr());
        ef_density.setUnit(den_diesel.getUnitId());
        ef_density.setValue(den_diesel.getValue());

        efs.add(ef_density);


        EmissionFactorDTO ef_gwpco2 =  new EmissionFactorDTO();
        ef_gwpco2.setName(gwp_co2.getDescription());
        ef_gwpco2.setUnitStr(gwp_co2.getUnitStr());
        ef_gwpco2.setUnit(gwp_co2.getUnitId());
        ef_gwpco2.setValue(gwp_co2.getValue());

        efs.add(ef_gwpco2);

        EmissionFactorDTO ef_gwpch4 =  new EmissionFactorDTO();
        ef_gwpch4.setName(gwp_ch4.getDescription());
        ef_gwpch4.setUnitStr(gwp_ch4.getUnitStr());
        ef_gwpch4.setUnit(gwp_ch4.getUnitId());
        ef_gwpch4.setValue(gwp_ch4.getValue());

        efs.add(ef_gwpch4);

        EmissionFactorDTO ef_gwpn20 =  new EmissionFactorDTO();
        ef_gwpn20.setName(gwp_n20.getDescription());
        ef_gwpn20.setUnitStr(gwp_n20.getUnitStr());
        ef_gwpn20.setUnit(gwp_n20.getUnitId());
        ef_gwpn20.setValue(gwp_n20.getValue());

        efs.add(ef_gwpn20);

        //act data
        List<GeneratorsEntry> entries = generatorsEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};


            for (GeneratorsEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;


                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }



                dieselConsumption[entry.getMonth()] += entry.getConsumption();

            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");


            data.setEmission(emission);



            actData.add(dieselData);


        }


        data.setEmissionFactors(efs);
        data.setActivityData(actData);

        return data;
    }

    public EmissionSrcDataDTO getRefrigerantData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Refrigerant Leakage ");

        EmissionFactors gwp_r22 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R22);



        EmissionFactors r407c_ch2f2_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_PERCENT_CH2F2);
        EmissionFactors r407c_ch2f2_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_COFF_CH2F2);
        EmissionFactors r407c_cf3chf2_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_PERCENT_CF3CHF2);
        EmissionFactors r407c_cf3chf2_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_COFF_CF3CHF2);
        EmissionFactors r407c_cf3ch2f_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_PERCENT_CF3CH2F);
        EmissionFactors r407c_cf3ch2f_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R407C_W_COFF_CF3CH2F);


        EmissionFactors r410a_ch2f2_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_PERCENT_CH2F2);
        EmissionFactors r410a_chf2cf3_percent = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_PERCENT_CHF2CF3);
        EmissionFactors r410a_ch2f2_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_COFF_CH2F2);
        EmissionFactors r410a_chf2cf3_coff = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_R410A_W_COFF_CHF2CF3);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();


        EmissionFactorDTO ef_gwp_r22 =  new EmissionFactorDTO();
        ef_gwp_r22.setName(gwp_r22.getDescription());
        ef_gwp_r22.setUnitStr(gwp_r22.getUnitStr());
        ef_gwp_r22.setUnit(gwp_r22.getUnitId());
        ef_gwp_r22.setValue(gwp_r22.getValue());

        efs.add(ef_gwp_r22);

        EmissionFactorDTO ef_r407c_ch2f2_percent =  new EmissionFactorDTO();
        ef_r407c_ch2f2_percent.setName(r407c_ch2f2_percent.getDescription());
        ef_r407c_ch2f2_percent.setUnitStr(r407c_ch2f2_percent.getUnitStr());
        ef_r407c_ch2f2_percent.setUnit(r407c_ch2f2_percent.getUnitId());
        ef_r407c_ch2f2_percent.setValue(r407c_ch2f2_percent.getValue());

        efs.add(ef_r407c_ch2f2_percent);

        EmissionFactorDTO ef_r407c_ch2f2_coff =  new EmissionFactorDTO();
        ef_r407c_ch2f2_coff.setName(r407c_ch2f2_coff.getDescription());
        ef_r407c_ch2f2_coff.setUnitStr(r407c_ch2f2_coff.getUnitStr());
        ef_r407c_ch2f2_coff.setUnit(r407c_ch2f2_coff.getUnitId());
        ef_r407c_ch2f2_coff.setValue(r407c_ch2f2_coff.getValue());

        efs.add(ef_r407c_ch2f2_coff);

        EmissionFactorDTO ef_r407c_cf3chf2_percent =  new EmissionFactorDTO();
        ef_r407c_cf3chf2_percent.setName(r407c_cf3chf2_percent.getDescription());
        ef_r407c_cf3chf2_percent.setUnitStr(r407c_cf3chf2_percent.getUnitStr());
        ef_r407c_cf3chf2_percent.setUnit(r407c_cf3chf2_percent.getUnitId());
        ef_r407c_cf3chf2_percent.setValue(r407c_cf3chf2_percent.getValue());

        efs.add(ef_r407c_cf3chf2_percent);

        EmissionFactorDTO ef_r407c_cf3chf2_coff =  new EmissionFactorDTO();
        ef_r407c_cf3chf2_coff.setName(r407c_cf3chf2_coff.getDescription());
        ef_r407c_cf3chf2_coff.setUnitStr(r407c_cf3chf2_coff.getUnitStr());
        ef_r407c_cf3chf2_coff.setUnit(r407c_cf3chf2_coff.getUnitId());
        ef_r407c_cf3chf2_coff.setValue(r407c_cf3chf2_coff.getValue());

        efs.add(ef_r407c_cf3chf2_coff);

        EmissionFactorDTO ef_r407c_cf3ch2f_percent =  new EmissionFactorDTO();
        ef_r407c_cf3ch2f_percent.setName(r407c_cf3ch2f_percent.getDescription());
        ef_r407c_cf3ch2f_percent.setUnitStr(r407c_cf3ch2f_percent.getUnitStr());
        ef_r407c_cf3ch2f_percent.setUnit(r407c_cf3ch2f_percent.getUnitId());
        ef_r407c_cf3ch2f_percent.setValue(r407c_cf3ch2f_percent.getValue());

        efs.add(ef_r407c_cf3ch2f_percent);

        EmissionFactorDTO ef_r407c_cf3ch2f_coff =  new EmissionFactorDTO();
        ef_r407c_cf3ch2f_coff.setName(r407c_cf3ch2f_coff.getDescription());
        ef_r407c_cf3ch2f_coff.setUnitStr(r407c_cf3ch2f_coff.getUnitStr());
        ef_r407c_cf3ch2f_coff.setUnit(r407c_cf3ch2f_coff.getUnitId());
        ef_r407c_cf3ch2f_coff.setValue(r407c_cf3ch2f_coff.getValue());

        efs.add(ef_r407c_cf3ch2f_coff);

        EmissionFactorDTO ef_r410a_ch2f2_percent=  new EmissionFactorDTO();
        ef_r410a_ch2f2_percent.setName(r410a_ch2f2_percent.getDescription());
        ef_r410a_ch2f2_percent.setUnitStr(r410a_ch2f2_percent.getUnitStr());
        ef_r410a_ch2f2_percent.setUnit(r410a_ch2f2_percent.getUnitId());
        ef_r410a_ch2f2_percent.setValue(r410a_ch2f2_percent.getValue());

        efs.add(ef_r410a_ch2f2_percent);

        EmissionFactorDTO ef_r410a_ch2f2_coff =  new EmissionFactorDTO();
        ef_r410a_ch2f2_coff.setName(r410a_ch2f2_coff.getDescription());
        ef_r410a_ch2f2_coff.setUnitStr(r410a_ch2f2_coff.getUnitStr());
        ef_r410a_ch2f2_coff.setUnit(r410a_ch2f2_coff.getUnitId());
        ef_r410a_ch2f2_coff.setValue(r410a_ch2f2_coff.getValue());

        efs.add(ef_r410a_ch2f2_coff);

        EmissionFactorDTO ef_r410a_chf2cf3_percent =  new EmissionFactorDTO();
        ef_r410a_chf2cf3_percent.setName(r410a_chf2cf3_percent.getDescription());
        ef_r410a_chf2cf3_percent.setUnitStr(r410a_chf2cf3_percent.getUnitStr());
        ef_r410a_chf2cf3_percent.setUnit(r410a_chf2cf3_percent.getUnitId());
        ef_r410a_chf2cf3_percent.setValue(r410a_chf2cf3_percent.getValue());

        efs.add(ef_r410a_chf2cf3_percent);

        EmissionFactorDTO ef_r410a_chf2cf3_coff =  new EmissionFactorDTO();
        ef_r410a_chf2cf3_coff.setName(r410a_chf2cf3_coff.getDescription());
        ef_r410a_chf2cf3_coff.setUnitStr(r410a_chf2cf3_coff.getUnitStr());
        ef_r410a_chf2cf3_coff.setUnit(r410a_chf2cf3_coff.getUnitId());
        ef_r410a_chf2cf3_coff.setValue(r410a_chf2cf3_coff.getValue());

        efs.add(ef_r410a_chf2cf3_coff);

        //act data
        List<RefrigerantsEntry> entries = refrigerantsEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] r22Consumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] r407cConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] r410acConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (RefrigerantsEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");


                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (entry.getTypeofRefrigerent() == RefrigerantTypes.R22.type) {

                    r22Consumption[entry.getMonth()] += entry.getAmountRefilled();

                }else if (entry.getTypeofRefrigerent() == RefrigerantTypes.R407C.type) {
                    r407cConsumption[entry.getMonth()] +=  entry.getAmountRefilled();
                }else if (entry.getTypeofRefrigerent() == RefrigerantTypes.R410A.type) {
                    r410acConsumption[entry.getMonth()] +=  entry.getAmountRefilled();
                }

            }

            ActivityDataDTO r22Data = new ActivityDataDTO();
            ActivityDataDTO r407cData = new ActivityDataDTO();
            ActivityDataDTO r410aData = new ActivityDataDTO();


            r22Data.setUnit(UnitEnum.Kg.getId());
            r22Data.setUnitStr("kg");
            r22Data.setValues(Arrays.asList(r22Consumption));
            r22Data.setMetricsName("R22 Consumption");

            r407cData.setUnit(UnitEnum.Kg.getId());
            r407cData.setUnitStr("kg");
            r407cData.setValues(Arrays.asList(r407cConsumption));
            r407cData.setMetricsName("R407C Consumption");

            r410aData.setUnit(UnitEnum.Kg.getId());
            r410aData.setUnitStr("kg");
            r410aData.setValues(Arrays.asList(r410acConsumption));
            r410aData.setMetricsName("R410A Consumption");

            data.setEmission(emission);


            actData.add(r22Data);
            actData.add(r407cData);
            actData.add(r410aData);


        }


        data.setActivityData(actData);
        data.setEmissionFactors(efs);
        return data;
    }

    public EmissionSrcDataDTO getFireExtinguisherData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Fire Extinguishers");

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();



        //act data
        List<FireExtingEntry> entries = fireExtingEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] wgasConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] co2Consumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] dcpConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (FireExtingEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");


                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (entry.getFireExtinguisherType() == FireExtinguisherType.WGAS.getType()) {

                   wgasConsumption[entry.getMonth()] += (entry.getNoOfTanks() * entry.getWeight());

                }else if (entry.getFireExtinguisherType() == FireExtinguisherType.CO2.getType()) {
                    co2Consumption[entry.getMonth()] +=  (entry.getNoOfTanks() * entry.getWeight());
                }else if (entry.getFireExtinguisherType() == FireExtinguisherType.DCP.getType()) {
                    dcpConsumption[entry.getMonth()] += (entry.getNoOfTanks() * entry.getWeight());
                }

            }

            ActivityDataDTO wgasData = new ActivityDataDTO();
            ActivityDataDTO co2Data = new ActivityDataDTO();
            ActivityDataDTO dcpData = new ActivityDataDTO();


            wgasData.setUnit(UnitEnum.Kg.getId());
            wgasData.setUnitStr("kg");
            wgasData.setValues(Arrays.asList(wgasConsumption));
            wgasData.setMetricsName("WGas Consumption");

            co2Data.setUnit(UnitEnum.Kg.getId());
            co2Data.setUnitStr("kg");
            co2Data.setValues(Arrays.asList(co2Consumption));
            co2Data.setMetricsName("CO2 Consumption");

            dcpData.setUnit(UnitEnum.Kg.getId());
            dcpData.setUnitStr("kg");
            dcpData.setValues(Arrays.asList(dcpConsumption));
            dcpData.setMetricsName("DCP Consumption");

            data.setEmission(emission);


            actData.add(wgasData);
            actData.add(co2Data);
            actData.add(dcpData);


        }


        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public HashMap<String, EmissionSrcDataDTO> getEmpCommData (int companyId, String fy) {
        HashMap<String, EmissionSrcDataDTO> data = new HashMap<>();

        EmissionSrcDataDTO empCommPaid = new EmissionSrcDataDTO();
        EmissionSrcDataDTO empCommNotPaid = new EmissionSrcDataDTO();

        empCommNotPaid.setSrcName("Employee Commuting, Not paid by the company");
        empCommPaid.setSrcName("Employee Commuting, Paid by the company");

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actDataEmpCommPaid = new ArrayList<>();

        List<EmissionFactorDTO> efsEmpCommNotPaid = new ArrayList<>();
        List<ActivityDataDTO> actDataEmpCommNotPaid = new ArrayList<>();

        List<EmpCommutingEntry> entries = employeCommutingEntryManager.getCustomFiltered(-1, companyId, fy);

        Float [] pubDistancePaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
//        Float [] pubBusdistancePaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
//        Float [] pubMediumBusDistancePaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
//        Float [] railwayDistancePaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

        Float [] dieselConsumptionPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
        Float [] petrolConsumptionPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

        Float [] pubDistanceNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
//        Float [] pubBusdistanceNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
//        Float [] pubMediumBusDistanceNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
//        Float [] railwayDistanceNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

        Float [] dieselConsumptionNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
        Float [] petrolConsumptionNotPaid = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};


        if (entries != null || entries.size() > 0) {
            float emissionEmpComPaid = 0;
            float emissionEmpComNotPaid = 0;



            for (EmpCommutingEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;

                String emissionStr = entry.getEmissionDetails().get("tco2");
                float emission = 0f;

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission = emissionNum;
                    }

                }

                if (entry.isPaidByCom()) {
                    emissionEmpComPaid += emission;
                    pubDistancePaid[entry.getMonth()] += entry.getPublicTrans_distance_DOWN() + entry.getPublicTrans_distance_UP();

                    dieselConsumptionPaid[entry.getMonth()] += entry.getCompanyDieselLiters()+entry.getOwnDieselLiters() + (entry.getOwnTrans_fuelEconomy_DOWN() != 0 && entry.getOwnTrans_fuelType_DOWN() == 2 ? (entry.getOwnTrans_distance_DOWN()/ entry.getOwnTrans_fuelEconomy_DOWN()) * entry.getNoOfWorkingDays() : 0)
                            + (entry.getOwnTrans_fuelEconomy_UP() != 0  && entry.getOwnTrans_fuelType_DOWN() == 2  ? (entry.getOwnTrans_distance_UP()/entry.getOwnTrans_fuelEconomy_UP()) * entry.getNoOfWorkingDays() : 0)
                    ;
                    petrolConsumptionPaid[entry.getMonth()] += entry.getCompanyPetrolLiters()+entry.getOwnPetrolLiters() + (entry.getOwnTrans_fuelEconomy_DOWN() != 0 && entry.getOwnTrans_fuelType_DOWN() == 1 ? (entry.getOwnTrans_distance_DOWN()/ entry.getOwnTrans_fuelEconomy_DOWN()) * entry.getNoOfWorkingDays() : 0)
                            + (entry.getOwnTrans_fuelEconomy_UP() != 0  && entry.getOwnTrans_fuelType_DOWN() == 1  ? (entry.getOwnTrans_distance_UP()/entry.getOwnTrans_fuelEconomy_UP()) * entry.getNoOfWorkingDays() : 0)
                    ;

                }else {
                    emissionEmpComNotPaid +=  emission;

                    pubDistanceNotPaid[entry.getMonth()] += entry.getPublicTrans_distance_DOWN() + entry.getPublicTrans_distance_UP();

                    dieselConsumptionNotPaid[entry.getMonth()] += entry.getCompanyDieselLiters() + entry.getOwnDieselLiters()+ (entry.getOwnTrans_fuelEconomy_DOWN() != 0 && entry.getOwnTrans_fuelType_DOWN() == 2 ? (entry.getOwnTrans_distance_DOWN()/ entry.getOwnTrans_fuelEconomy_DOWN()) * entry.getNoOfWorkingDays() : 0)
                            + (entry.getOwnTrans_fuelEconomy_UP() != 0  && entry.getOwnTrans_fuelType_DOWN() == 2  ? (entry.getOwnTrans_distance_UP()/entry.getOwnTrans_fuelEconomy_UP()) * entry.getNoOfWorkingDays() : 0)
                    ;
                    petrolConsumptionNotPaid[entry.getMonth()] += entry.getCompanyPetrolLiters()+entry.getOwnPetrolLiters() + (entry.getOwnTrans_fuelEconomy_DOWN() != 0 && entry.getOwnTrans_fuelType_DOWN() == 1 ? (entry.getOwnTrans_distance_DOWN()/ entry.getOwnTrans_fuelEconomy_DOWN()) * entry.getNoOfWorkingDays() : 0)
                            + (entry.getOwnTrans_fuelEconomy_UP() != 0  && entry.getOwnTrans_fuelType_DOWN() == 1  ? (entry.getOwnTrans_distance_UP()/entry.getOwnTrans_fuelEconomy_UP()) * entry.getNoOfWorkingDays() : 0)
                    ;

                }



            }

            ActivityDataDTO dieselDataPaid = new ActivityDataDTO();
            ActivityDataDTO petrolDataPaid = new ActivityDataDTO();
            ActivityDataDTO publicDistancePaid = new ActivityDataDTO();

            ActivityDataDTO dieselDataNotPaid = new ActivityDataDTO();
            ActivityDataDTO petrolDataNotPaid = new ActivityDataDTO();
            ActivityDataDTO publicDistanceNotPaid = new ActivityDataDTO();



            dieselDataPaid.setUnit(UnitEnum.Liters.getId());
            dieselDataPaid.setUnitStr("Liters");
            dieselDataPaid.setValues(Arrays.asList(dieselConsumptionPaid));
            dieselDataPaid.setMetricsName("Diesel Consumption");

            petrolDataPaid.setUnit(UnitEnum.Liters.getId());
            petrolDataPaid.setUnitStr("Liters");
            petrolDataPaid.setValues(Arrays.asList(petrolConsumptionPaid));
            petrolDataPaid.setMetricsName("Petrol Consumption");

            publicDistancePaid.setUnit(UnitEnum.km.getId());
            publicDistancePaid.setUnitStr("km");
            publicDistancePaid.setValues(Arrays.asList(pubDistancePaid));
            publicDistancePaid.setMetricsName("Distance Travelled using Public Transport");

            dieselDataNotPaid.setUnit(UnitEnum.Liters.getId());
            dieselDataNotPaid.setUnitStr("Liters");
            dieselDataNotPaid.setValues(Arrays.asList(dieselConsumptionNotPaid));
            dieselDataNotPaid.setMetricsName("Diesel Consumption");

            petrolDataNotPaid.setUnit(UnitEnum.Liters.getId());
            petrolDataNotPaid.setUnitStr("Liters");
            petrolDataNotPaid.setValues(Arrays.asList(petrolConsumptionNotPaid));
            petrolDataNotPaid.setMetricsName("Petrol Consumption");

            publicDistanceNotPaid.setUnit(UnitEnum.km.getId());
            publicDistanceNotPaid.setUnitStr("km");
            publicDistanceNotPaid.setValues(Arrays.asList(pubDistanceNotPaid));
            publicDistanceNotPaid.setMetricsName("Distance Travelled using Public Transport");



           actDataEmpCommNotPaid.add(publicDistanceNotPaid);
           actDataEmpCommNotPaid.add(dieselDataNotPaid);
           actDataEmpCommNotPaid.add(petrolDataNotPaid);

            actDataEmpCommPaid.add(publicDistancePaid);
            actDataEmpCommPaid.add(dieselDataPaid);
            actDataEmpCommPaid.add(petrolDataPaid);


            empCommNotPaid.setEmission(emissionEmpComNotPaid);
            empCommPaid.setEmission(emissionEmpComPaid);

        }

        EmissionFactors p_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL);
        EmissionFactors p_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE);
        EmissionFactors p_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE);
        EmissionFactors  p_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE);
        EmissionFactors p_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);

        EmissionFactors d_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors d_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors d_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors  d_efN20 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors d_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);



        EmissionFactorDTO ef_p_ncv = new EmissionFactorDTO();
        ef_p_ncv.setName(p_ncv.getDescription());
        ef_p_ncv.setValue(p_ncv.getValue());
        ef_p_ncv.setUnit(p_ncv.getUnitId());
        ef_p_ncv.setUnitStr(p_ncv.getUnitStr());


        EmissionFactorDTO ef_p_efco2 = new EmissionFactorDTO();
        ef_p_efco2.setName(p_efCo2.getDescription());
        ef_p_efco2.setValue(p_efCo2.getValue());
        ef_p_efco2.setUnit(p_efCo2.getUnitId());
        ef_p_efco2.setUnitStr(p_efCo2.getUnitStr());


        EmissionFactorDTO ef_p_ch4 = new EmissionFactorDTO();
        ef_p_ch4.setName(p_efCh4.getDescription());
        ef_p_ch4.setValue(p_efCh4.getValue());
        ef_p_ch4.setUnit(p_efCh4.getUnitId());
        ef_p_ch4.setUnitStr(p_efCh4.getUnitStr());


        EmissionFactorDTO ef_p_n20 = new EmissionFactorDTO();
        ef_p_n20.setName(p_efN20.getDescription());
        ef_p_n20.setValue(p_efN20.getValue());
        ef_p_n20.setUnit(p_efN20.getUnitId());
        ef_p_n20.setUnitStr(p_efN20.getUnitStr());



        EmissionFactorDTO ef_p_density = new EmissionFactorDTO();
        ef_p_density.setName(p_density.getDescription());
        ef_p_density.setValue(p_density.getValue());
        ef_p_density.setUnit(p_density.getUnitId());
        ef_p_density.setUnitStr(p_density.getUnitStr());


        EmissionFactorDTO ef_d_ncv = new EmissionFactorDTO();
        ef_d_ncv.setName(d_density.getDescription());
        ef_d_ncv.setValue(d_density.getValue());
        ef_d_ncv.setUnit(d_density.getUnitId());
        ef_d_ncv.setUnitStr(d_density.getUnitStr());



        EmissionFactorDTO ef_d_co2 = new EmissionFactorDTO();
        ef_d_co2.setName(d_efCo2.getDescription());
        ef_d_co2.setValue(d_efCo2.getValue());
        ef_d_co2.setUnit(d_efCo2.getUnitId());
        ef_d_co2.setUnitStr(d_efCo2.getUnitStr());


        EmissionFactorDTO ef_d_ch4 = new EmissionFactorDTO();
        ef_d_ch4.setName(d_efCh4.getDescription());
        ef_d_ch4.setValue(d_efCh4.getValue());
        ef_d_ch4.setUnit(d_efCh4.getUnitId());
        ef_d_ch4.setUnitStr(d_efCh4.getUnitStr());


        EmissionFactorDTO ef_d_n20 = new EmissionFactorDTO();
        ef_d_n20.setName(d_efN20.getDescription());
        ef_d_n20.setValue(d_efN20.getValue());
        ef_d_n20.setUnit(d_efN20.getUnitId());
        ef_d_n20.setUnitStr(d_efN20.getUnitStr());


        EmissionFactorDTO ef_d_density = new EmissionFactorDTO();
        ef_d_density.setName(d_density.getDescription());
        ef_d_density.setValue(d_density.getValue());
        ef_d_density.setUnit(d_density.getUnitId());
        ef_d_density.setUnitStr(d_density.getUnitStr());


        EmissionFactorDTO ef_gwpco2 = new EmissionFactorDTO();
        ef_gwpco2.setName(gwp_co2.getDescription());
        ef_gwpco2.setValue(gwp_co2.getValue());
        ef_gwpco2.setUnit(gwp_co2.getUnitId());
        ef_gwpco2.setUnitStr(gwp_co2.getUnitStr());


        EmissionFactorDTO ef_gwpch4 = new EmissionFactorDTO();
        ef_gwpch4.setName(gwp_ch4.getDescription());
        ef_gwpch4.setValue(gwp_ch4.getValue());
        ef_gwpch4.setUnit(gwp_ch4.getUnitId());
        ef_gwpch4.setUnitStr(gwp_ch4.getUnitStr());


        EmissionFactorDTO ef_gwpn20 = new EmissionFactorDTO();
        ef_gwpn20.setName(gwp_n20.getDescription());
        ef_gwpn20.setValue(gwp_n20.getValue());
        ef_gwpn20.setUnit(gwp_n20.getUnitId());
        ef_gwpn20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_p_ncv);
        efs.add(ef_p_efco2);
        efs.add(ef_p_ch4);
        efs.add(ef_p_n20);
        efs.add(ef_p_density);
        efs.add(ef_d_ncv);
        efs.add(ef_d_co2);
        efs.add(ef_d_ch4);
        efs.add(ef_d_n20);
        efs.add(ef_d_density);
        efs.add(ef_gwpco2);
        efs.add(ef_gwpch4);
        efs.add(ef_gwpn20);

        PublicTransposrtEmisssionFactor kg_co2_van = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.VAN_DIESEL);
        PublicTransposrtEmisssionFactor kg_co2_bus_medium = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.MEDIUM_BUS_DIESEL);
        PublicTransposrtEmisssionFactor kg_co2_bus = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.BUS_DIESEL);
        PublicTransposrtEmisssionFactor kg_co2_train = publicTransportEmissionFactorsManager.getEntityByKey(PublicTransport_Keys.TRAIN);


        EmissionFactorDTO ef_kg_co2_van = new EmissionFactorDTO();
        ef_kg_co2_van.setName("Emission Factor of " + kg_co2_van.getVehicleName() );
        ef_kg_co2_van.setValue(kg_co2_van.getCo2_per_km());
        ef_kg_co2_van.setUnit(UnitEnum.kgco2perpassengerkm.getId());
        ef_kg_co2_van.setUnitStr("kgCO2e/passenger-km");
        efs.add(ef_kg_co2_van);

        EmissionFactorDTO ef_kg_co2_bus_medium = new EmissionFactorDTO();
        ef_kg_co2_bus_medium.setName("Emission Factor of " + kg_co2_bus_medium.getVehicleName() );
        ef_kg_co2_bus_medium.setValue(kg_co2_bus_medium.getCo2_per_km());
        ef_kg_co2_bus_medium.setUnit(UnitEnum.kgco2perpassengerkm.getId());
        ef_kg_co2_bus_medium.setUnitStr("kgCO2e/passenger-km");
        efs.add(ef_kg_co2_bus_medium);

        EmissionFactorDTO ef_kg_co2_bus = new EmissionFactorDTO();
        ef_kg_co2_bus.setName("Emission Factor of " + kg_co2_bus.getVehicleName() );
        ef_kg_co2_bus.setValue(kg_co2_bus.getCo2_per_km());
        ef_kg_co2_bus.setUnit(UnitEnum.kgco2perpassengerkm.getId());
        ef_kg_co2_bus.setUnitStr("kgCO2e/passenger-km");
        efs.add(ef_kg_co2_bus);

        EmissionFactorDTO ef_kg_co2_train = new EmissionFactorDTO();
        ef_kg_co2_train.setName("Emission Factor of " + kg_co2_train.getVehicleName() );
        ef_kg_co2_train.setValue(kg_co2_train.getCo2_per_km());
        ef_kg_co2_train.setUnit(UnitEnum.kgco2perpassengerkm.getId());
        ef_kg_co2_train.setUnitStr("kgCO2e/passenger-km");
        efs.add(ef_kg_co2_train);



        empCommNotPaid.setActivityData(actDataEmpCommNotPaid);
        empCommNotPaid.setEmissionFactors(efs);


        empCommPaid.setActivityData(actDataEmpCommPaid);
        empCommPaid.setEmissionFactors(efs);


        data.put("paid", empCommPaid);
        data.put("not_paid", empCommNotPaid);
        return  data;
    }

    public HashMap<String, EmissionSrcDataDTO> getElecData (int companyId, String fy) {
        HashMap<String, EmissionSrcDataDTO> data = new HashMap<>();

        EmissionSrcDataDTO td = new EmissionSrcDataDTO();
        EmissionSrcDataDTO gridElec = new EmissionSrcDataDTO();

        EmissionFactors ef_grid = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NATIONAL_GRID_EMISSION_FACTOR);
        EmissionFactors etd = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.T_AND_D_LOSS_PERCENT);

        List<EmissionFactorDTO> efsTd = new ArrayList<>();
        List<ActivityDataDTO> actDataTd = new ArrayList<>();

        List<EmissionFactorDTO> efsElec = new ArrayList<>();
        List<ActivityDataDTO> actDataElec = new ArrayList<>();

        EmissionFactorDTO ef_td =  new EmissionFactorDTO();
        ef_td.setName(etd.getDescription());
        ef_td.setUnitStr(etd.getUnitStr());
        ef_td.setUnit(etd.getUnitId());
        ef_td.setValue(etd.getValue());


        EmissionFactorDTO ef_gridElec = new EmissionFactorDTO();
        ef_gridElec.setName(ef_grid.getDescription());
        ef_gridElec.setValue(ef_grid.getValue());
        ef_gridElec.setUnit(ef_grid.getUnitId());
        ef_gridElec.setUnitStr(ef_grid.getUnitStr());

        efsTd.add(ef_td);
        efsElec.add(ef_gridElec);

        //act data
        List<ElectricityEntry> entries = electricityEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emissionTd = 0;
            float emissionElec = 0;

            Float [] electricityConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};


            for (ElectricityEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;

                String emissionStr = entry.getEmissionDetails().get("tco2");
                String emissionTDStr = entry.getEmissionDetails().get("tco2_t_d");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emissionElec += emissionNum;
                    }

                }
                if (emissionTDStr != null && !emissionTDStr.trim().isEmpty()) {
                    Float emissionTDNum = Float.parseFloat(emissionTDStr);
                    if (!emissionTDNum.isNaN() && !emissionTDNum.isInfinite()) {
                        emissionTd +=  emissionTDNum;
                    }

                }



                electricityConsumption[entry.getMonth()] += entry.getConsumption();




            }

            ActivityDataDTO elecData = new ActivityDataDTO();
            ActivityDataDTO tdData = new ActivityDataDTO();



            elecData.setUnit(UnitEnum.kWh.getId());
            elecData.setUnitStr("kWh");
            elecData.setValues(Arrays.asList(electricityConsumption));
            elecData.setMetricsName("Electricity Consumption");

            tdData.setUnit(UnitEnum.kWh.getId());
            tdData.setUnitStr("kWh");
            tdData.setValues(Arrays.asList(electricityConsumption));
            tdData.setMetricsName("Electricity Consumption");

            actDataElec.add(elecData);
            actDataTd.add(tdData);




            td.setActivityData(actDataTd);
            td.setEmissionFactors(efsTd);
            td.setEmission(emissionTd);

            gridElec.setActivityData(actDataElec);
            gridElec.setEmissionFactors(efsElec);
            gridElec.setEmission(emissionElec);


        }








        td.setSrcName("Transmission and Distribution Loss");
        gridElec.setSrcName("Grid Electricity");

        data.put("td", td);
        data.put("grid_elec", gridElec);
        return data;
    }

    public EmissionSrcDataDTO getAirTravelData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Business Air Travels");

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        List<BusinessAirTravelEntry> entries = businessAirTravelEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;


            Float [] empsBusiness = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] empsEconimic = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] empsFirstClass = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};


            for (BusinessAirTravelEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;


                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (entry.getSeatClass() == FlightClass.FirstClass.id) {
                    empsFirstClass[entry.getTravelMonth()] += entry.getNoOfEmps();
                }else  if (entry.getSeatClass() == FlightClass.BusinessClass.id) {
                    empsBusiness[entry.getTravelMonth()] += entry.getNoOfEmps();
                }else if (entry.getSeatClass() == FlightClass.EconomicClass.id) {
                    empsEconimic[entry.getTravelMonth()] += entry.getNoOfEmps();
                }




            }

            ActivityDataDTO economicClassData = new ActivityDataDTO();
            ActivityDataDTO businessClassClassData = new ActivityDataDTO();
            ActivityDataDTO firstClassData = new ActivityDataDTO();



            economicClassData.setUnit(UnitEnum.None.getId());
            economicClassData.setUnitStr("");
            economicClassData.setValues(Arrays.asList(empsEconimic));
            economicClassData.setMetricsName("No of Employees(Economic Class)");

            businessClassClassData.setUnit(UnitEnum.None.getId());
            businessClassClassData.setUnitStr("");
            businessClassClassData.setValues(Arrays.asList(empsBusiness));
            businessClassClassData.setMetricsName("No of Employees(Business Class)");

            firstClassData.setUnit(UnitEnum.None.getId());
            firstClassData.setUnitStr("");
            firstClassData.setValues(Arrays.asList(empsFirstClass));
            firstClassData.setMetricsName("No of Employees(First Class)");


            data.setEmission(emission);


            actData.add(economicClassData);
            actData.add(businessClassClassData);
            actData.add(firstClassData);


        }







        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getMunWaterData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Municipal Water");

        EmissionFactors ef_grid = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NATIONAL_GRID_EMISSION_FACTOR);
        EmissionFactors cf_mun = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.CF_MUNICIPAL_WATER);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO gridfactor = new EmissionFactorDTO();
        gridfactor.setName(ef_grid.getDescription());
        gridfactor.setValue(ef_grid.getValue());
        gridfactor.setUnit(ef_grid.getUnitId());
        gridfactor.setUnitStr(ef_grid.getUnitStr());

        EmissionFactorDTO cf = new EmissionFactorDTO();
        cf.setName(cf_mun.getDescription());
        cf.setValue(cf_mun.getValue());
        cf.setUnit(cf_mun.getUnitId());
        cf.setUnitStr(cf_mun.getUnitStr());

        efs.add(gridfactor);
        efs.add(cf);

        //act data
        List<MunicipalWaterEntry> entries = municipalWaterEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;


            Float [] consumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (MunicipalWaterEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;

                String emissionStr = entry.getEmissionDetails().get("tco2");
                String quantityStr = entry.getEmissionDetails().get("quantity");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (quantityStr != null && !quantityStr.trim().isEmpty()) {
                    Float quantityNum = Float.parseFloat(quantityStr);
                    if (!quantityNum.isNaN() && !quantityNum.isInfinite()) {
                        consumption[entry.getMonth()] += quantityNum;
                    }

                }






            }

            ActivityDataDTO waterData = new ActivityDataDTO();



            waterData.setUnit(UnitEnum.m3.getId());
            waterData.setUnitStr("Cubic Meters");
            waterData.setValues(Arrays.asList(consumption));
            waterData.setMetricsName("Water Consumption");


            data.setEmission(emission);


            actData.add(waterData);


        }




        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getWasteDisposalData (int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Waste Disposal");

        EmissionFactors ef_feedrate = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.AVG_FEED_RATE_PER_PIG);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
//        WasteDisposalFactors factors = wasteDisposalFactorsManager.getEntityByKey(wasteType);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();


        //act data
        List<WasteDisposalEntry> entries = wasteDisposalEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            HashMap<Integer, HashSet<Integer>> wasteTypesMethodMapping = new HashMap<>();

            HashMap<Integer, Float[]> amountByTypes = new HashMap<>();



            for (WasteDisposalEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");


                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }



                if (wasteTypesMethodMapping.containsKey(entry.getWasteType())) {
                    amountByTypes.get(entry.getWasteType())[entry.getMonth()] += entry.getAmountDisposed();
                    wasteTypesMethodMapping.get(entry.getWasteType()).add(entry.getDisposalMethod());

                }else {
                    Float [] amount = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
                    HashSet<Integer> disposalMethods = new HashSet<>();

                    amount[entry.getMonth()] += entry.getAmountDisposed();
                    disposalMethods.add(entry.getDisposalMethod());

                    wasteTypesMethodMapping.put(entry.getWasteType(), disposalMethods);
                    amountByTypes.put(entry.getWasteType(), amount);

                }
            }

            amountByTypes.forEach((type, amount) -> {
                ActivityDataDTO actDataWasteType = new ActivityDataDTO();

                actDataWasteType.setUnit(UnitEnum.Tons.getId());
                actDataWasteType.setUnitStr("Tons");
                actDataWasteType.setValues(Arrays.asList(amount));
                actDataWasteType.setMetricsName(getWasteTypeName(type));

                actData.add(actDataWasteType);

            });

            //loading and creation efs
            HashMap<Integer,WasteDisposalFactors> wasteFactorsMap = new HashMap<>();
            AtomicBoolean piggeryFeedingExists = new AtomicBoolean(false);
            wasteTypesMethodMapping.forEach((wasteType, disposalMethods) -> {
                WasteDisposalFactors ef = null;
                if (wasteFactorsMap.containsKey(wasteType)) {
                    ef = wasteFactorsMap.get(wasteType);

                }else {
                    ef = wasteDisposalFactorsManager.getEntityByKey(wasteType);
                    wasteFactorsMap.put(wasteType, ef);
                }


                for (Integer method: disposalMethods) {
                    if (method == WasteDispoalMethods.Incineration.method) {
                        //todo

                        EmissionFactorDTO efCarbonFraction = new EmissionFactorDTO();
                        efCarbonFraction.setName(getDisposalMethodName(method) + "-" + getWasteTypeName(wasteType) + " : " + "Carbon Fraction");
                        efCarbonFraction.setValue(incinerationFactorsManager.getEntityByKey(wasteType).getCarbon_fraction());
                        efCarbonFraction.setUnit(UnitEnum.percentage.getId());
                        efCarbonFraction.setUnitStr("%");
                        efs.add(efCarbonFraction);

//                        EmissionFactorDTO efDryMatter = new EmissionFactorDTO();
//                        efDryMatter.setName(getDisposalMethodName(method) + "-" + getWasteTypeName(wasteType) + " : " + "Dry Matter");
//                        efDryMatter.setValue(incinerationFactorsManager.getEntityByKey(wasteType).getDry_matter_per());
//                        efDryMatter.setUnit(UnitEnum.kgCO2e.getId());
//                        efDryMatter.setUnitStr("kgCO2e");
//
//                        EmissionFactorDTO efFossilFraction = new EmissionFactorDTO();
//                        efFossilFraction.setName(getDisposalMethodName(method) + "-" + getWasteTypeName(wasteType) + " : " + "Fossil Fraction");
//                        efFossilFraction.setValue(incinerationFactorsManager.getEntityByKey(wasteType).getFossil_carbon_fraction());
//                        efFossilFraction.setUnit(UnitEnum.kgCO2e.getId());
//                        efFossilFraction.setUnitStr("kgCO2e");

                        continue;
                    }
                    if (method == WasteDispoalMethods.Piggery.method) {
                        piggeryFeedingExists.set(true);
                        continue;
                    }


                    EmissionFactorDTO efDto = new EmissionFactorDTO();
                    efDto.setName(getDisposalMethodName(method) + "-" + getWasteTypeName(wasteType));
                    efDto.setValue(getDisposalValue(ef, method));
                    efDto.setUnit(UnitEnum.kgCO2e.getId());
                    efDto.setUnitStr("kgCO2e");
                    efs.add(efDto);

                }




            });

            if (piggeryFeedingExists.get()) {
                EmissionFactorDTO efDto = new EmissionFactorDTO();
                efDto.setName("Average Feed rate of a pig per day");
                efDto.setValue(emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.AVG_FEED_RATE_PER_PIG).getValue());
                efDto.setUnit(UnitEnum.Kg.getId());
                efDto.setUnitStr("kg");
                efs.add(efDto);
            }




            data.setEmission(emission);





        }




        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getWasteTransData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Waste Transportation");


        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        EmissionFactors ef_co2_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE);
        EmissionFactors ef_co2_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors ef_ch4_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors ef_ch4_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE);
        EmissionFactors ef_n20_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE);
        EmissionFactors ef_n20_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);

        EmissionFactors den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);

        EmissionFactors den_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);
        EmissionFactors net_cal_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_p_ncv = new EmissionFactorDTO();
        ef_p_ncv.setName(net_cal_petrol.getDescription());
        ef_p_ncv.setValue(net_cal_petrol.getValue());
        ef_p_ncv.setUnit(net_cal_petrol.getUnitId());
        ef_p_ncv.setUnitStr(net_cal_petrol.getUnitStr());

        EmissionFactorDTO ef_p_efco2 = new EmissionFactorDTO();
        ef_p_efco2.setName(ef_co2_petrol.getDescription());
        ef_p_efco2.setValue(ef_co2_petrol.getValue());
        ef_p_efco2.setUnit(ef_co2_petrol.getUnitId());
        ef_p_efco2.setUnitStr(ef_co2_petrol.getUnitStr());

        EmissionFactorDTO ef_p_efch4 = new EmissionFactorDTO();
        ef_p_efch4.setName(ef_ch4_petrol.getDescription());
        ef_p_efch4.setValue(ef_ch4_petrol.getValue());
        ef_p_efch4.setUnit(ef_ch4_petrol.getUnitId());
        ef_p_efch4.setUnitStr(ef_ch4_petrol.getUnitStr());

        EmissionFactorDTO ef_p_efn20 = new EmissionFactorDTO();
        ef_p_efn20.setName(ef_n20_petrol.getDescription());
        ef_p_efn20.setValue(ef_n20_petrol.getValue());
        ef_p_efn20.setUnit(ef_n20_petrol.getUnitId());
        ef_p_efn20.setUnitStr(ef_n20_petrol.getUnitStr());

        EmissionFactorDTO ef_p_density = new EmissionFactorDTO();
        ef_p_density.setName(den_petrol.getDescription());
        ef_p_density.setValue(den_petrol.getValue());
        ef_p_density.setUnit(den_petrol.getUnitId());
        ef_p_density.setUnitStr(den_petrol.getUnitStr());

        EmissionFactorDTO ef_d_ncv = new EmissionFactorDTO();
        ef_d_ncv.setName(net_cal_diesel.getDescription());
        ef_d_ncv.setValue(net_cal_diesel.getValue());
        ef_d_ncv.setUnit(net_cal_diesel.getUnitId());
        ef_d_ncv.setUnitStr(net_cal_diesel.getUnitStr());

        EmissionFactorDTO ef_d_efco2 = new EmissionFactorDTO();
        ef_d_efco2.setName(ef_co2_diesel.getDescription());
        ef_d_efco2.setValue(ef_co2_diesel.getValue());
        ef_d_efco2.setUnit(ef_co2_diesel.getUnitId());
        ef_d_efco2.setUnitStr(ef_co2_diesel.getUnitStr());

        EmissionFactorDTO ef_d_efch4 = new EmissionFactorDTO();
        ef_d_efch4.setName(ef_ch4_diesel.getDescription());
        ef_d_efch4.setValue(ef_ch4_diesel.getValue());
        ef_d_efch4.setUnit(ef_ch4_diesel.getUnitId());
        ef_d_efch4.setUnitStr(ef_ch4_diesel.getUnitStr());

        EmissionFactorDTO ef_d_efn20 = new EmissionFactorDTO();
        ef_d_efn20.setName(ef_n20_diesel.getDescription());
        ef_d_efn20.setValue(ef_n20_diesel.getValue());
        ef_d_efn20.setUnit(ef_n20_diesel.getUnitId());
        ef_d_efn20.setUnitStr(ef_n20_diesel.getUnitStr());

        EmissionFactorDTO ef_d_density = new EmissionFactorDTO();
        ef_d_density.setName(den_diesel.getDescription());
        ef_d_density.setValue(den_diesel.getValue());
        ef_d_density.setUnit(den_diesel.getUnitId());
        ef_d_density.setUnitStr(den_diesel.getUnitStr());

        EmissionFactorDTO ef_gwpco2 = new EmissionFactorDTO();
        ef_gwpco2.setName(gwp_co2.getDescription());
        ef_gwpco2.setValue(gwp_co2.getValue());
        ef_gwpco2.setUnit(gwp_co2.getUnitId());
        ef_gwpco2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwpch4 = new EmissionFactorDTO();
        ef_gwpch4.setName(gwp_ch4.getDescription());
        ef_gwpch4.setValue(gwp_ch4.getValue());
        ef_gwpch4.setUnit(gwp_ch4.getUnitId());
        ef_gwpch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwpn20 = new EmissionFactorDTO();
        ef_gwpn20.setName(gwp_n20.getDescription());
        ef_gwpn20.setValue(gwp_n20.getValue());
        ef_gwpn20.setUnit(gwp_n20.getUnitId());
        ef_gwpn20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_p_ncv);
        efs.add(ef_p_efco2);
        efs.add(ef_p_efch4);
        efs.add(ef_p_efn20);
        efs.add(ef_p_density);
        efs.add(ef_d_ncv);
        efs.add(ef_d_efco2);
        efs.add(ef_d_efch4);
        efs.add(ef_d_efn20);
        efs.add(ef_d_density);
        efs.add(ef_gwpco2);
        efs.add(ef_gwpch4);
        efs.add(ef_gwpn20);


        //act data
        List<WasteTransportEntry> entries = wasteTransportEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] petrolConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (WasteTransportEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (entry.getFuelType() == FuelTypes.Petrol.type) {
                    if (entry.getFuelEconomy() > 0) {
                        petrolConsumption[entry.getMonth()] += (entry.getDistanceTravelled()/ entry.getFuelEconomy());
                    }



                }else if (entry.getFuelType() == FuelTypes.Diesel.type) {
                    if (entry.getFuelEconomy() > 0) {
                        dieselConsumption[entry.getMonth()] += (entry.getDistanceTravelled()/ entry.getFuelEconomy());
                    }

                }

            }

            ActivityDataDTO dieselData = new ActivityDataDTO();
            ActivityDataDTO petrolData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            petrolData.setUnit(UnitEnum.Liters.getId());
            petrolData.setUnitStr("Liters");
            petrolData.setValues(Arrays.asList(petrolConsumption));
            petrolData.setMetricsName("Petrol Consumption");

            data.setEmission(emission);


            actData.add(petrolData);
            actData.add(dieselData);


        }





        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getLPGData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("LP Gas");

        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_NCV);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_EF_CO2);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_EF_CH4);
        EmissionFactors efN2o = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.LPGAS_EF_N20);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());
        efs.add(ef_ncv);

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());
        efs.add(ef_efco2);

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());
        efs.add(ef_efch4);

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN2o.getDescription());
        ef_efn20.setValue(efN2o.getValue());
        ef_efn20.setUnit(efN2o.getUnitId());
        ef_efn20.setUnitStr(efN2o.getUnitStr());
        efs.add(ef_efn20);

        EmissionFactorDTO ef_gwpco2 = new EmissionFactorDTO();
        ef_gwpco2.setName(gwp_co2.getDescription());
        ef_gwpco2.setValue(gwp_co2.getValue());
        ef_gwpco2.setUnit(gwp_co2.getUnitId());
        ef_gwpco2.setUnitStr(gwp_co2.getUnitStr());
        efs.add(ef_gwpco2);

        EmissionFactorDTO ef_gwpch4 = new EmissionFactorDTO();
        ef_gwpch4.setName(gwp_ch4.getDescription());
        ef_gwpch4.setValue(gwp_ch4.getValue());
        ef_gwpch4.setUnit(gwp_ch4.getUnitId());
        ef_gwpch4.setUnitStr(gwp_ch4.getUnitStr());
        efs.add(ef_gwpch4);

        EmissionFactorDTO ef_gwpn20 = new EmissionFactorDTO();
        ef_gwpn20.setName(gwp_n20.getDescription());
        ef_gwpn20.setValue(gwp_n20.getValue());
        ef_gwpn20.setUnit(gwp_n20.getUnitId());
        ef_gwpn20.setUnitStr(gwp_n20.getUnitStr());
        efs.add(ef_gwpn20);


        List<LPGEntry> entries = lpgEntryEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] consumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (LPGEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");


                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                consumption [entry.getMonth()] += entry.getConsumption();            }

            ActivityDataDTO bioMassData = new ActivityDataDTO();


            bioMassData.setUnit(UnitEnum.Kg.getId());
            bioMassData.setUnitStr("kg");
            bioMassData.setValues(Arrays.asList(consumption));
            bioMassData.setMetricsName("LPG Consumption");


            actData.add(bioMassData);

            data.setEmission(emission);


        }





        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getBioMassData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Bio Mass");

        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_NCV);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_CO2);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_CH4);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_N20);


        EmissionFactors other_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_NCV);
        EmissionFactors other_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_CO2);
        EmissionFactors other_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_CH4);
        EmissionFactors other_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_N20);

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());
        efs.add(ef_ncv);

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());
        efs.add(ef_efco2);

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());
        efs.add(ef_efch4);

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());
        efs.add(ef_efn20);


        EmissionFactorDTO ef_other_ncv = new EmissionFactorDTO();
        ef_other_ncv.setName(other_ncv.getDescription());
        ef_other_ncv.setValue(other_ncv.getValue());
        ef_other_ncv.setUnit(other_ncv.getUnitId());
        ef_other_ncv.setUnitStr(other_ncv.getUnitStr());
        efs.add(ef_other_ncv);

        EmissionFactorDTO ef_other_efco2 = new EmissionFactorDTO();
        ef_other_efco2.setName(other_efCo2.getDescription());
        ef_other_efco2.setValue(other_efCo2.getValue());
        ef_other_efco2.setUnit(other_efCo2.getUnitId());
        ef_other_efco2.setUnitStr(other_efCo2.getUnitStr());
        efs.add(ef_other_efco2);

        EmissionFactorDTO ef_other_efch4 = new EmissionFactorDTO();
        ef_other_efch4.setName(other_efCh4.getDescription());
        ef_other_efch4.setValue(other_efCh4.getValue());
        ef_other_efch4.setUnit(other_efCh4.getUnitId());
        ef_other_efch4.setUnitStr(other_efCh4.getUnitStr());
        efs.add(ef_other_efch4);

        EmissionFactorDTO ef_other_efn20 = new EmissionFactorDTO();
        ef_other_efn20.setName(other_efN20.getDescription());
        ef_other_efn20.setValue(other_efN20.getValue());
        ef_other_efn20.setUnit(other_efN20.getUnitId());
        ef_other_efn20.setUnitStr(other_efN20.getUnitStr());
        efs.add(ef_other_efn20);









        EmissionFactorDTO ef_gwpco2 = new EmissionFactorDTO();
        ef_gwpco2.setName(gwp_co2.getDescription());
        ef_gwpco2.setValue(gwp_co2.getValue());
        ef_gwpco2.setUnit(gwp_co2.getUnitId());
        ef_gwpco2.setUnitStr(gwp_co2.getUnitStr());
        efs.add(ef_gwpco2);

        EmissionFactorDTO ef_gwpch4 = new EmissionFactorDTO();
        ef_gwpch4.setName(gwp_ch4.getDescription());
        ef_gwpch4.setValue(gwp_ch4.getValue());
        ef_gwpch4.setUnit(gwp_ch4.getUnitId());
        ef_gwpch4.setUnitStr(gwp_ch4.getUnitStr());
        efs.add(ef_gwpch4);

        EmissionFactorDTO ef_gwpn20 = new EmissionFactorDTO();
        ef_gwpn20.setName(gwp_n20.getDescription());
        ef_gwpn20.setValue(gwp_n20.getValue());
        ef_gwpn20.setUnit(gwp_n20.getUnitId());
        ef_gwpn20.setUnitStr(gwp_n20.getUnitStr());
        efs.add(ef_gwpn20);


        List<BioMassEntry> entries = bioMassEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] consumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (BioMassEntry entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");
                String quantityStr = entry.getEmissionDetails().get("quantity");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                consumption [entry.getMonth()] += entry.getQuantity();            }

            ActivityDataDTO bioMassData = new ActivityDataDTO();


            bioMassData.setUnit(UnitEnum.Kg.getId());
            bioMassData.setUnitStr("kg");
            bioMassData.setValues(Arrays.asList(consumption));
            bioMassData.setMetricsName("Biomass Consumption");


            actData.add(bioMassData);

            data.setEmission(emission);


        }




        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getAshTrans(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Ash Transportation");


        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);


        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());

        EmissionFactorDTO ef_density = new EmissionFactorDTO();
        ef_density.setName(density.getDescription());
        ef_density.setValue(density.getValue());
        ef_density.setUnit(density.getUnitId());
        ef_density.setUnitStr(density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_ncv);
        efs.add(ef_efco2);
        efs.add(ef_efch4);
        efs.add(ef_efn20);
        efs.add(ef_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);


        //act data
        List<AshTransportation> entries = ashTransportationEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (AshTransportation entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");
                String quantityStr = entry.getEmissionDetails().get("quantity");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            data.setEmission(emission);


            actData.add(dieselData);


        }




        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public HashMap<String, EmissionSrcDataDTO> getForkliftsData(int companyId, String fy) {
        HashMap<String, EmissionSrcDataDTO> data = new HashMap<>();
        EmissionSrcDataDTO forkliftsPetrol = new EmissionSrcDataDTO();
        EmissionSrcDataDTO forkliftsDiesel = new EmissionSrcDataDTO();

        forkliftsDiesel.setSrcName("Fork-lifts, Petrol");
        forkliftsPetrol.setSrcName("Fork-lifts, Diesel");

        EmissionFactors p_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL);
        EmissionFactors p_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_GASOLINE);
        EmissionFactors p_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_GASOLINE);
        EmissionFactors p_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_GASOLINE);
        EmissionFactors p_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);

        EmissionFactors d_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors d_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_OFF_ROAD_DIESEL);
        EmissionFactors d_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_OFF_ROAD_DIESEL);
        EmissionFactors d_efN20 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_OFF_ROAD_DIESEL);
        EmissionFactors d_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efsForklifstPetrol = new ArrayList<>();
        List<ActivityDataDTO> actDataForkliftsPetrol = new ArrayList<>();

        List<EmissionFactorDTO> efsForkliftsDiesel = new ArrayList<>();
        List<ActivityDataDTO> actDataForkliftsDiesel = new ArrayList<>();


        EmissionFactorDTO ef_p_ncv = new EmissionFactorDTO();
        ef_p_ncv.setName(p_ncv.getDescription());
        ef_p_ncv.setValue(p_ncv.getValue());
        ef_p_ncv.setUnit(p_ncv.getUnitId());
        ef_p_ncv.setUnitStr(p_ncv.getUnitStr());

        EmissionFactorDTO ef_p_efco2 = new EmissionFactorDTO();
        ef_p_efco2.setName(p_efCo2.getDescription());
        ef_p_efco2.setValue(p_efCo2.getValue());
        ef_p_efco2.setUnit(p_efCo2.getUnitId());
        ef_p_efco2.setUnitStr(p_efCo2.getUnitStr());

        EmissionFactorDTO ef_p_ch4 = new  EmissionFactorDTO();
        ef_p_ch4.setName(p_efCh4.getDescription());
        ef_p_ch4.setValue(p_efCh4.getValue());
        ef_p_ch4.setUnit(p_efCh4.getUnitId());
        ef_p_ch4.setUnitStr(p_efCh4.getUnitStr());

        EmissionFactorDTO ef_p_n20 = new EmissionFactorDTO();
        ef_p_n20.setName(p_efN20.getDescription());
        ef_p_n20.setValue(p_efN20.getValue());
        ef_p_n20.setUnit(p_efN20.getUnitId());
        ef_p_n20.setUnitStr(p_efN20.getUnitStr());

        EmissionFactorDTO ef_p_density = new  EmissionFactorDTO();
        ef_p_density.setName(p_density.getDescription());
        ef_p_density.setValue(p_density.getValue());
        ef_p_density.setUnit(p_density.getUnitId());
        ef_p_density.setUnitStr(p_density.getUnitStr());

        EmissionFactorDTO ef_d_ncv = new  EmissionFactorDTO();
        ef_d_ncv.setName(d_ncv.getDescription());
        ef_d_ncv.setValue(d_ncv.getValue());
        ef_d_ncv.setUnit(d_ncv.getUnitId());
        ef_d_ncv.setUnitStr(d_ncv.getUnitStr());

        EmissionFactorDTO ef_d_efco2 = new  EmissionFactorDTO();
        ef_d_efco2.setName(d_efCo2.getDescription());
        ef_d_efco2.setValue(d_efCo2.getValue());
        ef_d_efco2.setUnit(d_efCo2.getUnitId());
        ef_d_efco2.setUnitStr(d_efCo2.getUnitStr());

        EmissionFactorDTO ef_d_efch4 = new EmissionFactorDTO();
        ef_d_efch4.setName(d_efCh4.getDescription());
        ef_d_efch4.setValue(d_efCh4.getValue());
        ef_d_efch4.setUnit(d_efCh4.getUnitId());
        ef_d_efch4.setUnitStr(d_efCh4.getUnitStr());

        EmissionFactorDTO ef_d_efn20 = new  EmissionFactorDTO();
        ef_d_efn20.setName(d_efN20.getDescription());
        ef_d_efn20.setValue(d_efN20.getValue());
        ef_d_efn20.setUnit(d_efN20.getUnitId());
        ef_d_efn20.setUnitStr(d_efN20.getUnitStr());

        EmissionFactorDTO ef_d_density = new EmissionFactorDTO();
        ef_d_density.setName(d_density.getDescription());
        ef_d_density.setValue(d_density.getValue());
        ef_d_density.setUnit(d_density.getUnitId());
        ef_d_density.setUnitStr(d_density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new  EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());



        efsForklifstPetrol.add(ef_p_ncv);
        efsForklifstPetrol.add(ef_p_efco2);
        efsForklifstPetrol.add(ef_p_n20);
        efsForklifstPetrol.add(ef_p_density);
        efsForkliftsDiesel.add(ef_d_ncv);
        efsForkliftsDiesel.add(ef_d_efco2);
        efsForkliftsDiesel.add(ef_d_efch4);
        efsForkliftsDiesel.add(ef_d_efn20);
        efsForkliftsDiesel.add(ef_d_density);
        efsForkliftsDiesel.add(ef_gwp_co2);
        efsForkliftsDiesel.add(ef_gwp_ch4);
        efsForkliftsDiesel.add(ef_gwp_n20);
        efsForklifstPetrol.add(ef_gwp_co2);
        efsForklifstPetrol.add(ef_gwp_ch4);
        efsForklifstPetrol.add(ef_gwp_n20);


        //act data
        List<Forklifts> entries = forkliftsEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emissionForkliftsPetrol = 0;
            float emissionForkliftsDiesel = 0;

            Float [] petrolConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (Forklifts entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;

                String emissionStr = entry.getEmissionDetails().get("tco2");
                float emission = 0f;

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }
                if (entry.getFuleType() == 1) {
                    emissionForkliftsPetrol +=emission;


                    petrolConsumption  [entry.getMonth()] += entry.getFuelConsumption();
                }else {
                    emissionForkliftsDiesel += emission;


                    dieselConsumption  [entry.getMonth()] += entry.getFuelConsumption();
                }

            }

            ActivityDataDTO petrolData = new ActivityDataDTO();
            ActivityDataDTO dieselData = new ActivityDataDTO();


            petrolData.setUnit(UnitEnum.Liters.getId());
            petrolData.setUnitStr("Liters");
            petrolData.setValues(Arrays.asList(petrolConsumption));
            petrolData.setMetricsName("Petrol Consumption");

            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");


           actDataForkliftsDiesel.add(dieselData);
           actDataForkliftsPetrol.add(petrolData);

           forkliftsDiesel.setEmission(emissionForkliftsDiesel);
           forkliftsPetrol.setEmission(emissionForkliftsPetrol);


        }






        forkliftsDiesel.setActivityData(actDataForkliftsDiesel);
        forkliftsDiesel.setEmissionFactors(efsForkliftsDiesel);

        forkliftsPetrol.setActivityData(actDataForkliftsPetrol);
        forkliftsPetrol.setEmissionFactors(efsForklifstPetrol);


        data.put("petrol", forkliftsPetrol);
        data.put("diesel", forkliftsDiesel);
        return data;
    }


    public EmissionSrcDataDTO getFurnaceOilData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Furnace Oil");


        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_NCV);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_EF_CO2);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_EF_CH4);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_EF_N20);
        EmissionFactors density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.FURNACE_OIL_DENSITY);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());

        EmissionFactorDTO ef_density = new EmissionFactorDTO();
        ef_density.setName(density.getDescription());
        ef_density.setValue(density.getValue());
        ef_density.setUnit(density.getUnitId());
        ef_density.setUnitStr(density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_ncv);
        efs.add(ef_efco2);
        efs.add(ef_efch4);
        efs.add(ef_efn20);
        efs.add(ef_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);


        List<FurnaceOil> entries = furnaceEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] consumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (FurnaceOil entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                 consumption [entry.getMonth()] += entry.getFuelConsumption();
            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(consumption));
            dieselData.setMetricsName("Furnace Oil Consumption");


            actData.add(dieselData);

            data.setEmission(emission);


        }





        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public HashMap<String, EmissionSrcDataDTO> getLorryTransData(int companyId, String fy) {
        HashMap<String, EmissionSrcDataDTO> data = new HashMap<>();

        EmissionSrcDataDTO internalTrans = new EmissionSrcDataDTO();
        EmissionSrcDataDTO externalTrans = new EmissionSrcDataDTO();

        internalTrans.setSrcName("Internal - Lorry Transportation");
        externalTrans.setSrcName("External - Lorry Transportation");


        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);


        List<EmissionFactorDTO> efsInternalTrans = new ArrayList<>();
        List<ActivityDataDTO> actDataInternalTrans = new ArrayList<>();

        List<EmissionFactorDTO> efsExternalTrans = new ArrayList<>();
        List<ActivityDataDTO> actDataExternalTrans = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());

        EmissionFactorDTO ef_density = new EmissionFactorDTO();
        ef_density.setName(density.getDescription());
        ef_density.setValue(density.getValue());
        ef_density.setUnit(density.getUnitId());
        ef_density.setUnitStr(density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());

        efsInternalTrans.add(ef_ncv);
        efsInternalTrans.add(ef_efco2);
        efsInternalTrans.add(ef_efch4);
        efsInternalTrans.add(ef_efn20);
        efsInternalTrans.add(ef_density);
        efsInternalTrans.add(ef_gwp_co2);
        efsInternalTrans.add(ef_gwp_ch4);
        efsInternalTrans.add(ef_gwp_n20);

        efsExternalTrans = new ArrayList<>(efsInternalTrans);


        //act data
        List<LorryTransportation> entries = lorryTransportationManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emissionInternal = 0;
            float emissionExternal = 0;

            Float [] dieselConsumptionInternal = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            Float [] dieselConsumptionExternal = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (LorryTransportation entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;

                String emissionStr = entry.getEmissionDetails().get("tco2");

                float emission = 0f;
                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (entry.getTransportMode() == LorryTransModes.Internal.getMode()) {
                    emissionInternal += emission;


                    dieselConsumptionInternal [entry.getMonth()] += entry.getFuelConsumption();
                }else {
                    emissionExternal += emission;


                    dieselConsumptionExternal [entry.getMonth()] += entry.getFuelConsumption();
                }

            }

            ActivityDataDTO dieselDataInternal = new ActivityDataDTO();
            ActivityDataDTO dieselDataExternal = new ActivityDataDTO();


            dieselDataInternal.setUnit(UnitEnum.Liters.getId());
            dieselDataInternal.setUnitStr("Liters");
            dieselDataInternal.setValues(Arrays.asList(dieselConsumptionInternal));
            dieselDataInternal.setMetricsName("Diesel Consumption");

            dieselDataExternal.setUnit(UnitEnum.Liters.getId());
            dieselDataExternal.setUnitStr("Liters");
            dieselDataExternal.setValues(Arrays.asList(dieselConsumptionExternal));
            dieselDataExternal.setMetricsName("Diesel Consumption");


            actDataExternalTrans.add(dieselDataExternal);
            actDataInternalTrans.add(dieselDataInternal);

            internalTrans.setEmission(emissionInternal);
            externalTrans.setEmission(emissionExternal);


        }







        internalTrans.setActivityData(actDataInternalTrans);
        internalTrans.setEmissionFactors(efsInternalTrans);

        externalTrans.setActivityData(actDataExternalTrans);
        externalTrans.setEmissionFactors(efsExternalTrans);


        data.put("internal", internalTrans);
        data.put("external", externalTrans);
        return data;
    }

    public EmissionSrcDataDTO getPaidManagerVehicleData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Paid Manager's Vehicle Allowance");

        EmissionFactors p_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL);
        EmissionFactors p_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE);
        EmissionFactors p_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE);
        EmissionFactors  p_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE);
        EmissionFactors p_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);

        EmissionFactors d_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors d_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors d_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors  d_efN20 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors d_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();


        EmissionFactorDTO ef_p_ncv = new EmissionFactorDTO();
        ef_p_ncv.setName(p_ncv.getDescription());
        ef_p_ncv.setValue(p_ncv.getValue());
        ef_p_ncv.setUnit(p_ncv.getUnitId());
        ef_p_ncv.setUnitStr(p_ncv.getUnitStr());


        EmissionFactorDTO ef_p_efco2 = new EmissionFactorDTO();
        ef_p_efco2.setName(p_efCo2.getDescription());
        ef_p_efco2.setValue(p_efCo2.getValue());
        ef_p_efco2.setUnit(p_efCo2.getUnitId());
        ef_p_efco2.setUnitStr(p_efCo2.getUnitStr());


        EmissionFactorDTO ef_p_ch4 = new EmissionFactorDTO();
        ef_p_ch4.setName(p_efCh4.getDescription());
        ef_p_ch4.setValue(p_efCh4.getValue());
        ef_p_ch4.setUnit(p_efCh4.getUnitId());
        ef_p_ch4.setUnitStr(p_efCh4.getUnitStr());


        EmissionFactorDTO ef_p_n20 = new EmissionFactorDTO();
        ef_p_n20.setName(p_efN20.getDescription());
        ef_p_n20.setValue(p_efN20.getValue());
        ef_p_n20.setUnit(p_efN20.getUnitId());
        ef_p_n20.setUnitStr(p_efN20.getUnitStr());



        EmissionFactorDTO ef_p_density = new EmissionFactorDTO();
        ef_p_density.setName(p_density.getDescription());
        ef_p_density.setValue(p_density.getValue());
        ef_p_density.setUnit(p_density.getUnitId());
        ef_p_density.setUnitStr(p_density.getUnitStr());


        EmissionFactorDTO ef_d_ncv = new EmissionFactorDTO();
        ef_d_ncv.setName(d_density.getDescription());
        ef_d_ncv.setValue(d_density.getValue());
        ef_d_ncv.setUnit(d_density.getUnitId());
        ef_d_ncv.setUnitStr(d_density.getUnitStr());



        EmissionFactorDTO ef_d_co2 = new EmissionFactorDTO();
        ef_d_co2.setName(d_efCo2.getDescription());
        ef_d_co2.setValue(d_efCo2.getValue());
        ef_d_co2.setUnit(d_efCo2.getUnitId());
        ef_d_co2.setUnitStr(d_efCo2.getUnitStr());


        EmissionFactorDTO ef_d_ch4 = new EmissionFactorDTO();
        ef_d_ch4.setName(d_efCh4.getDescription());
        ef_d_ch4.setValue(d_efCh4.getValue());
        ef_d_ch4.setUnit(d_efCh4.getUnitId());
        ef_d_ch4.setUnitStr(d_efCh4.getUnitStr());


        EmissionFactorDTO ef_d_n20 = new EmissionFactorDTO();
        ef_d_n20.setName(d_efN20.getDescription());
        ef_d_n20.setValue(d_efN20.getValue());
        ef_d_n20.setUnit(d_efN20.getUnitId());
        ef_d_n20.setUnitStr(d_efN20.getUnitStr());


        EmissionFactorDTO ef_d_density = new EmissionFactorDTO();
        ef_d_density.setName(d_density.getDescription());
        ef_d_density.setValue(d_density.getValue());
        ef_d_density.setUnit(d_density.getUnitId());
        ef_d_density.setUnitStr(d_density.getUnitStr());


        EmissionFactorDTO ef_gwpco2 = new EmissionFactorDTO();
        ef_gwpco2.setName(gwp_co2.getDescription());
        ef_gwpco2.setValue(gwp_co2.getValue());
        ef_gwpco2.setUnit(gwp_co2.getUnitId());
        ef_gwpco2.setUnitStr(gwp_co2.getUnitStr());


        EmissionFactorDTO ef_gwpch4 = new EmissionFactorDTO();
        ef_gwpch4.setName(gwp_ch4.getDescription());
        ef_gwpch4.setValue(gwp_ch4.getValue());
        ef_gwpch4.setUnit(gwp_ch4.getUnitId());
        ef_gwpch4.setUnitStr(gwp_ch4.getUnitStr());


        EmissionFactorDTO ef_gwpn20 = new EmissionFactorDTO();
        ef_gwpn20.setName(gwp_n20.getDescription());
        ef_gwpn20.setValue(gwp_n20.getValue());
        ef_gwpn20.setUnit(gwp_n20.getUnitId());
        ef_gwpn20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_p_ncv);
        efs.add(ef_p_efco2);
        efs.add(ef_p_ch4);
        efs.add(ef_p_n20);
        efs.add(ef_p_density);
        efs.add(ef_d_ncv);
        efs.add(ef_d_co2);
        efs.add(ef_d_ch4);
        efs.add(ef_d_n20);
        efs.add(ef_d_density);
        efs.add(ef_gwpco2);
        efs.add(ef_gwpch4);
        efs.add(ef_gwpn20);


        //act data
        List<PaidManagerVehicles> entries = paidManagerVehiclesEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;
            Float [] petrolConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (PaidManagerVehicles entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (entry.getFuleType() == 1) {
                    petrolConsumption[entry.getMonth()] += entry.getFuelConsumption();
                }else {
                    dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
                }
            }
            ActivityDataDTO petrolData = new ActivityDataDTO();
            ActivityDataDTO dieselData = new ActivityDataDTO();

            petrolData.setUnit(UnitEnum.Liters.getId());
            petrolData.setUnitStr("Liters");
            petrolData.setValues(Arrays.asList(petrolConsumption));
            petrolData.setMetricsName("Petrol Consumption");

            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            actData.add(petrolData);
            actData.add(dieselData);

            data.setEmission(emission);


        }








        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getPaperWasteData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Paper Waste");

        EmissionFactors ef = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PAPER_WASTE_EF_CO2) ;

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_pw = new EmissionFactorDTO();
        ef_pw.setName(ef.getDescription());
        ef_pw.setValue(ef.getValue());
        ef_pw.setUnit(ef.getUnitId());
        ef_pw.setUnitStr(ef.getUnitStr());

        efs.add(ef_pw);

        //act data
        List<PaperWaste> entries = paperWasteManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] wastage = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (PaperWaste entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                wastage [entry.getMonth()] += entry.getQuantity() ;
            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Kg.getId());
            dieselData.setUnitStr("kg");
            dieselData.setValues(Arrays.asList(wastage));
            dieselData.setMetricsName("Paper Waste");

            data.setEmission(emission);


            actData.add(dieselData);


        }




        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return data;
    }

    public EmissionSrcDataDTO getOilGasTransData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Oil and Gas Transportation");


        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);


        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());

        EmissionFactorDTO ef_density = new EmissionFactorDTO();
        ef_density.setName(density.getDescription());
        ef_density.setValue(density.getValue());
        ef_density.setUnit(density.getUnitId());
        ef_density.setUnitStr(density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_ncv);
        efs.add(ef_efco2);
        efs.add(ef_efch4);
        efs.add(ef_efn20);
        efs.add(ef_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);


        //act data
        List<OilAndGasTransportation> entries = oilAndGasTransportationEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (OilAndGasTransportation entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            data.setEmission(emission);


            actData.add(dieselData);


        }





        data.setActivityData(actData);
        data.setEmissionFactors(efs);

        return  data;
    }

    public EmissionSrcDataDTO getRawMatTransData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Raw Material Transportation");

        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();


        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());

        EmissionFactorDTO ef_density = new EmissionFactorDTO();
        ef_density.setName(density.getDescription());
        ef_density.setValue(density.getValue());
        ef_density.setUnit(density.getUnitId());
        ef_density.setUnitStr(density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_ncv);
        efs.add(ef_efco2);
        efs.add(ef_efch4);
        efs.add(ef_efn20);
        efs.add(ef_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);

        //act data
        List<RawMaterialTransporationLocal> entries = rawMaterialTransportationLocalManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (RawMaterialTransporationLocal entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            data.setEmission(emission);

            actData.add(dieselData);


        }




        data.setActivityData(actData);
        data.setEmissionFactors(efs);



        return data;
    }

    public EmissionSrcDataDTO getSludgeTransData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Sludge Transportation");


        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());

        EmissionFactorDTO ef_density = new EmissionFactorDTO();
        ef_density.setName(density.getDescription());
        ef_density.setValue(density.getValue());
        ef_density.setUnit(density.getUnitId());
        ef_density.setUnitStr(density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_ncv);
        efs.add(ef_efco2);
        efs.add(ef_efch4);
        efs.add(ef_efn20);
        efs.add(ef_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);


        //act data
        List<SludgeTransportation> entries = sludgeTransportationManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (SludgeTransportation entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");


            actData.add(dieselData);

            data.setEmission(emission);


        }




        data.setActivityData(actData);
        data.setEmissionFactors(efs);


        return data;
    }

    public EmissionSrcDataDTO getSawDustTransData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Saw Dust Transportation");

        EmissionFactors ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);
        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);

        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_ncv = new EmissionFactorDTO();
        ef_ncv.setName(ncv.getDescription());
        ef_ncv.setValue(ncv.getValue());
        ef_ncv.setUnit(ncv.getUnitId());
        ef_ncv.setUnitStr(ncv.getUnitStr());

        EmissionFactorDTO ef_efco2 = new EmissionFactorDTO();
        ef_efco2.setName(efCo2.getDescription());
        ef_efco2.setValue(efCo2.getValue());
        ef_efco2.setUnit(efCo2.getUnitId());
        ef_efco2.setUnitStr(efCo2.getUnitStr());

        EmissionFactorDTO ef_efch4 = new EmissionFactorDTO();
        ef_efch4.setName(efCh4.getDescription());
        ef_efch4.setValue(efCh4.getValue());
        ef_efch4.setUnit(efCh4.getUnitId());
        ef_efch4.setUnitStr(efCh4.getUnitStr());

        EmissionFactorDTO ef_efn20 = new EmissionFactorDTO();
        ef_efn20.setName(efN20.getDescription());
        ef_efn20.setValue(efN20.getValue());
        ef_efn20.setUnit(efN20.getUnitId());
        ef_efn20.setUnitStr(efN20.getUnitStr());

        EmissionFactorDTO ef_density = new EmissionFactorDTO();
        ef_density.setName(density.getDescription());
        ef_density.setValue(density.getValue());
        ef_density.setUnit(density.getUnitId());
        ef_density.setUnitStr(density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());

        efs.add(ef_ncv);
        efs.add(ef_efco2);
        efs.add(ef_efch4);
        efs.add(ef_efn20);
        efs.add(ef_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);

        //act data
        List<SawDustTransportation> entries = sawDustTransportationManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;

            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (SawDustTransportation entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }


                    dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
            }

            ActivityDataDTO dieselData = new ActivityDataDTO();


            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");


            actData.add(dieselData);

            data.setEmission(emission);

        }





        data.setActivityData(actData);
        data.setEmissionFactors(efs);


        return data;
    }

    public EmissionSrcDataDTO getStaffTransData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Staff Transportation");

        EmissionFactors p_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL);
        EmissionFactors p_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE);
        EmissionFactors p_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE);
        EmissionFactors p_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE);
        EmissionFactors p_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);

        EmissionFactors d_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors d_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors d_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors d_efN20 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors d_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);


        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_p_ncv = new EmissionFactorDTO();
        ef_p_ncv.setName(p_ncv.getDescription());
        ef_p_ncv.setValue(p_ncv.getValue());
        ef_p_ncv.setUnit(p_ncv.getUnitId());
        ef_p_ncv.setUnitStr(p_ncv.getUnitStr());

        EmissionFactorDTO ef_p_efco2 = new EmissionFactorDTO();
        ef_p_efco2.setName(p_efCo2.getDescription());
        ef_p_efco2.setValue(p_efCo2.getValue());
        ef_p_efco2.setUnit(p_efCo2.getUnitId());
        ef_p_efco2.setUnitStr(p_efCo2.getUnitStr());

        EmissionFactorDTO ef_p_ch4 = new  EmissionFactorDTO();
        ef_p_ch4.setName(p_efCh4.getDescription());
        ef_p_ch4.setValue(p_efCh4.getValue());
        ef_p_ch4.setUnit(p_efCh4.getUnitId());
        ef_p_ch4.setUnitStr(p_efCh4.getUnitStr());

        EmissionFactorDTO ef_p_n20 = new EmissionFactorDTO();
        ef_p_n20.setName(p_efN20.getDescription());
        ef_p_n20.setValue(p_efN20.getValue());
        ef_p_n20.setUnit(p_efN20.getUnitId());
        ef_p_n20.setUnitStr(p_efN20.getUnitStr());

        EmissionFactorDTO ef_p_density = new  EmissionFactorDTO();
        ef_p_density.setName(p_density.getDescription());
        ef_p_density.setValue(p_density.getValue());
        ef_p_density.setUnit(p_density.getUnitId());
        ef_p_density.setUnitStr(p_density.getUnitStr());

        EmissionFactorDTO ef_d_ncv = new  EmissionFactorDTO();
        ef_d_ncv.setName(d_ncv.getDescription());
        ef_d_ncv.setValue(d_ncv.getValue());
        ef_d_ncv.setUnit(d_ncv.getUnitId());
        ef_d_ncv.setUnitStr(d_ncv.getUnitStr());

        EmissionFactorDTO ef_d_efco2 = new  EmissionFactorDTO();
        ef_d_efco2.setName(d_efCo2.getDescription());
        ef_d_efco2.setValue(d_efCo2.getValue());
        ef_d_efco2.setUnit(d_efCo2.getUnitId());
        ef_d_efco2.setUnitStr(d_efCo2.getUnitStr());

        EmissionFactorDTO ef_d_efch4 = new EmissionFactorDTO();
        ef_d_efch4.setName(d_efCh4.getDescription());
        ef_d_efch4.setValue(d_efCh4.getValue());
        ef_d_efch4.setUnit(d_efCh4.getUnitId());
        ef_d_efch4.setUnitStr(d_efCh4.getUnitStr());

        EmissionFactorDTO ef_d_efn20 = new  EmissionFactorDTO();
        ef_d_efn20.setName(d_efN20.getDescription());
        ef_d_efn20.setValue(d_efN20.getValue());
        ef_d_efn20.setUnit(d_efN20.getUnitId());
        ef_d_efn20.setUnitStr(d_efN20.getUnitStr());

        EmissionFactorDTO ef_d_density = new EmissionFactorDTO();
        ef_d_density.setName(d_density.getDescription());
        ef_d_density.setValue(d_density.getValue());
        ef_d_density.setUnit(d_density.getUnitId());
        ef_d_density.setUnitStr(d_density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new  EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());



        efs.add(ef_p_ncv);
        efs.add(ef_p_efco2);
        efs.add(ef_p_n20);
        efs.add(ef_p_density);
        efs.add(ef_d_ncv);
        efs.add(ef_d_efco2);
        efs.add(ef_d_efch4);
        efs.add(ef_d_efn20);
        efs.add(ef_d_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);


        //act data
        List<StaffTransportation> entries = staffTransportationEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;
            Float [] petrolConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (StaffTransportation entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }

                if (entry.getFuelType() == 1) {
                    petrolConsumption[entry.getMonth()] += entry.getFuelConsumption();
                }else {
                    dieselConsumption [entry.getMonth()] += entry.getFuelConsumption();
                }
            }
            ActivityDataDTO petrolData = new ActivityDataDTO();
            ActivityDataDTO dieselData = new ActivityDataDTO();

            petrolData.setUnit(UnitEnum.Liters.getId());
            petrolData.setUnitStr("Liters");
            petrolData.setValues(Arrays.asList(petrolConsumption));
            petrolData.setMetricsName("Petrol Consumption");

            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            actData.add(petrolData);
            actData.add(dieselData);



            data.setEmission(emission);

        }






        data.setActivityData(actData);
        data.setEmissionFactors(efs);
        return data;
    }

    public EmissionSrcDataDTO getVehicleOthersData(int companyId, String fy) {
        EmissionSrcDataDTO data = new EmissionSrcDataDTO();
        data.setSrcName("Other Vehicles");

        EmissionFactors p_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_PETROL);
        EmissionFactors p_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE);
        EmissionFactors p_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE);
        EmissionFactors p_efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE);
        EmissionFactors p_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL);

        EmissionFactors d_ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL);
        EmissionFactors d_efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL);
        EmissionFactors d_efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL);
        EmissionFactors d_efN20 =  emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL);
        EmissionFactors d_density = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL);

        EmissionFactors gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL);
        EmissionFactors gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL);
        EmissionFactors gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL);


        List<EmissionFactorDTO> efs = new ArrayList<>();
        List<ActivityDataDTO> actData = new ArrayList<>();

        EmissionFactorDTO ef_p_ncv = new EmissionFactorDTO();
        ef_p_ncv.setName(p_ncv.getDescription());
        ef_p_ncv.setValue(p_ncv.getValue());
        ef_p_ncv.setUnit(p_ncv.getUnitId());
        ef_p_ncv.setUnitStr(p_ncv.getUnitStr());

        EmissionFactorDTO ef_p_efco2 = new EmissionFactorDTO();
        ef_p_efco2.setName(p_efCo2.getDescription());
        ef_p_efco2.setValue(p_efCo2.getValue());
        ef_p_efco2.setUnit(p_efCo2.getUnitId());
        ef_p_efco2.setUnitStr(p_efCo2.getUnitStr());

        EmissionFactorDTO ef_p_ch4 = new  EmissionFactorDTO();
        ef_p_ch4.setName(p_efCh4.getDescription());
        ef_p_ch4.setValue(p_efCh4.getValue());
        ef_p_ch4.setUnit(p_efCh4.getUnitId());
        ef_p_ch4.setUnitStr(p_efCh4.getUnitStr());

        EmissionFactorDTO ef_p_n20 = new EmissionFactorDTO();
        ef_p_n20.setName(p_efN20.getDescription());
        ef_p_n20.setValue(p_efN20.getValue());
        ef_p_n20.setUnit(p_efN20.getUnitId());
        ef_p_n20.setUnitStr(p_efN20.getUnitStr());

        EmissionFactorDTO ef_p_density = new  EmissionFactorDTO();
        ef_p_density.setName(p_density.getDescription());
        ef_p_density.setValue(p_density.getValue());
        ef_p_density.setUnit(p_density.getUnitId());
        ef_p_density.setUnitStr(p_density.getUnitStr());

        EmissionFactorDTO ef_d_ncv = new  EmissionFactorDTO();
        ef_d_ncv.setName(d_ncv.getDescription());
        ef_d_ncv.setValue(d_ncv.getValue());
        ef_d_ncv.setUnit(d_ncv.getUnitId());
        ef_d_ncv.setUnitStr(d_ncv.getUnitStr());

        EmissionFactorDTO ef_d_efco2 = new  EmissionFactorDTO();
        ef_d_efco2.setName(d_efCo2.getDescription());
        ef_d_efco2.setValue(d_efCo2.getValue());
        ef_d_efco2.setUnit(d_efCo2.getUnitId());
        ef_d_efco2.setUnitStr(d_efCo2.getUnitStr());

        EmissionFactorDTO ef_d_efch4 = new EmissionFactorDTO();
        ef_d_efch4.setName(d_efCh4.getDescription());
        ef_d_efch4.setValue(d_efCh4.getValue());
        ef_d_efch4.setUnit(d_efCh4.getUnitId());
        ef_d_efch4.setUnitStr(d_efCh4.getUnitStr());

        EmissionFactorDTO ef_d_efn20 = new  EmissionFactorDTO();
        ef_d_efn20.setName(d_efN20.getDescription());
        ef_d_efn20.setValue(d_efN20.getValue());
        ef_d_efn20.setUnit(d_efN20.getUnitId());
        ef_d_efn20.setUnitStr(d_efN20.getUnitStr());

        EmissionFactorDTO ef_d_density = new EmissionFactorDTO();
        ef_d_density.setName(d_density.getDescription());
        ef_d_density.setValue(d_density.getValue());
        ef_d_density.setUnit(d_density.getUnitId());
        ef_d_density.setUnitStr(d_density.getUnitStr());

        EmissionFactorDTO ef_gwp_co2 = new  EmissionFactorDTO();
        ef_gwp_co2.setName(gwp_co2.getDescription());
        ef_gwp_co2.setValue(gwp_co2.getValue());
        ef_gwp_co2.setUnit(gwp_co2.getUnitId());
        ef_gwp_co2.setUnitStr(gwp_co2.getUnitStr());

        EmissionFactorDTO ef_gwp_ch4 = new EmissionFactorDTO();
        ef_gwp_ch4.setName(gwp_ch4.getDescription());
        ef_gwp_ch4.setValue(gwp_ch4.getValue());
        ef_gwp_ch4.setUnit(gwp_ch4.getUnitId());
        ef_gwp_ch4.setUnitStr(gwp_ch4.getUnitStr());

        EmissionFactorDTO ef_gwp_n20 = new EmissionFactorDTO();
        ef_gwp_n20.setName(gwp_n20.getDescription());
        ef_gwp_n20.setValue(gwp_n20.getValue());
        ef_gwp_n20.setUnit(gwp_n20.getUnitId());
        ef_gwp_n20.setUnitStr(gwp_n20.getUnitStr());



        efs.add(ef_p_ncv);
        efs.add(ef_p_efco2);
        efs.add(ef_p_n20);
        efs.add(ef_p_density);
        efs.add(ef_d_ncv);
        efs.add(ef_d_efco2);
        efs.add(ef_d_efch4);
        efs.add(ef_d_efn20);
        efs.add(ef_d_density);
        efs.add(ef_gwp_co2);
        efs.add(ef_gwp_ch4);
        efs.add(ef_gwp_n20);


        //act data
        List<VehicleOthers> entries = vehicleOthersEntryManager.getCustomFiltered(-1, companyId, fy);
        if (entries != null || entries.size() > 0) {
            float emission = 0;
            Float [] petrolConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};
            Float [] dieselConsumption = new Float[]{0f,0f,0f,0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,0f};

            for (VehicleOthers entry : entries) {
                if (entry ==  null || entry.getEmissionDetails() == null || entry.getEmissionDetails().size() == 0) continue;
                String emissionStr = entry.getEmissionDetails().get("tco2");

                if (emissionStr != null && !emissionStr.trim().isEmpty()) {
                    Float emissionNum = Float.parseFloat(emissionStr);
                    if (!emissionNum.isInfinite() && !emissionNum.isNaN()) {
                        emission += emissionNum;
                    }

                }
                if (entry.getFuelType() == 1) {
                    if (entry.getFuelEconomy() > 0 ) {
                        petrolConsumption[entry.getMonth()] += entry.getDistance() /entry.getFuelEconomy();
                    }

                }else {
                    if (entry.getFuelEconomy() > 0 ) {
                        dieselConsumption [entry.getMonth()] += entry.getDistance() /entry.getFuelEconomy();
                    }

                }
            }
            ActivityDataDTO petrolData = new ActivityDataDTO();
            ActivityDataDTO dieselData = new ActivityDataDTO();

            petrolData.setUnit(UnitEnum.Liters.getId());
            petrolData.setUnitStr("Liters");
            petrolData.setValues(Arrays.asList(petrolConsumption));
            petrolData.setMetricsName("Petrol Consumption");

            dieselData.setUnit(UnitEnum.Liters.getId());
            dieselData.setUnitStr("Liters");
            dieselData.setValues(Arrays.asList(dieselConsumption));
            dieselData.setMetricsName("Diesel Consumption");

            actData.add(petrolData);
            actData.add(dieselData);



            data.setEmission(emission);

        }






        data.setActivityData(actData);
        data.setEmissionFactors(efs);
        return data;
    }



    private String getWasteTypeName(int wasteType) {
        switch (wasteType) {
            case WasteType_Keys
                    .AVG_CONSTRUCTION: {
                return "Average construction";
            }
            case WasteType_Keys
                    .SOILS: {
                return "Soils";
            }
            case WasteType_Keys
                    .WOOD: {
                return "Wood";
            }
            case WasteType_Keys
                    .BOOKS: {
                return "Books";
            }
            case WasteType_Keys
                    .GLASS: {
                return "Glass";
            }
            case WasteType_Keys
                    .CLOTHING: {
                return "Clothing";
            }
            case WasteType_Keys
                    .MUNICIPAL_WASTE: {
                return "Municipal Waste";
            }
            case WasteType_Keys
                    .ORGANIC_FOOD_DRINK_WASTE: {
                return "Organic: food and drink waste";
            }
            case WasteType_Keys
                    .ORGANIC_GARDEN_WASTE: {
                return "Organic: garden waste";
            }
            case WasteType_Keys
                    .ORGANIC_MIXED_FOOD_GARDEN_WASTE: {
                return "Organic: mixed food and garden waste";
            }
            case WasteType_Keys
                    .COMMERCIAL_INDUSTRIAL_WASTE: {
                return "Commercial and industrial waste";
            }
            case WasteType_Keys
                    .WEEE: {

                return "WEEE";
            }
            case WasteType_Keys
                    .WEEE_FRIDGES_FREEZERS: {

                return "WEEE - fridges and freezers";
            }
            case WasteType_Keys
                    .BATTERIES: {

                return "Batteries";
            }
            case WasteType_Keys
                    .METAL: {

                return "Metals";
            }
            case WasteType_Keys
                    .PLASTICS: {

                return "Plastics";
            }
            case WasteType_Keys
                    .PAPER_BOARD: {

                return "Paper and board";
            }
            default:
                return "";


    }

    }



    private String getDisposalMethodName(int disposalMethod) {

        if (disposalMethod ==  WasteDispoalMethods.Reuse.method) {
            return "Reuse";
        }else if (disposalMethod == WasteDispoalMethods.OpenLoop.method) {
            return "Open Loop";
        }
        else if (disposalMethod == WasteDispoalMethods.ClosedLoop.method) {
            return "Closed Loop";
        }
        else if (disposalMethod == WasteDispoalMethods.Combustion.method) {
            return "Combustion";
        }
        else if (disposalMethod == WasteDispoalMethods.Composting.method) {
            return "Composting";
        }
        else if (disposalMethod == WasteDispoalMethods.LandFill.method ) {
            return "Land Fill";
        }
        else if (disposalMethod ==  WasteDispoalMethods.Anaerobic_Digestion.method) {
            return "Anaerobic Digestion";
        }
        else if (disposalMethod == WasteDispoalMethods.Piggery.method) {
            return "Piggery Feed";
        }
        else if (disposalMethod == WasteDispoalMethods.Incineration.method) {
            return "Incineration";
        }
        return "";

    }

    private float getDisposalValue(WasteDisposalFactors ef, int disposalMethod) {

        if (disposalMethod ==  WasteDispoalMethods.Reuse.method) {
            return ef.getReuse();
        }else if (disposalMethod == WasteDispoalMethods.OpenLoop.method) {
            return ef.getOpen_loop();
        }
        else if (disposalMethod == WasteDispoalMethods.ClosedLoop.method) {
            return ef.getClosed_loop();
        }
        else if (disposalMethod == WasteDispoalMethods.Combustion.method) {
            return ef.getCombustion();
        }
        else if (disposalMethod == WasteDispoalMethods.Composting.method) {
            return ef.getComposting();
        }
        else if (disposalMethod == WasteDispoalMethods.LandFill.method ) {
            return ef.getLandfill();
        }
        else if (disposalMethod ==  WasteDispoalMethods.Anaerobic_Digestion.method) {
            return ef.getAnaerobic_digestion();
        }
//        else if (disposalMethod == WasteDispoalMethods.Piggery.method) {
//            return "Piggery Feed";
//        }
//        else if (disposalMethod == WasteDispoalMethods.Incineration.method) {
//            return "Incineration";
//        }
        return 0;

    }




}
