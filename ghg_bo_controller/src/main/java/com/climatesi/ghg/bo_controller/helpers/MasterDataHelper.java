package com.climatesi.ghg.bo_controller.helpers;

import com.climatesi.ghg.api.beans.Airport;
import com.climatesi.ghg.api.beans.Country;
import com.climatesi.ghg.bo_controller.GHGControllerFactory;
import com.climatesi.ghg.bo_controller.controllers.implGeneral.master_data.MasterDataController;
import com.climatesi.ghg.bo_messaging.api.constants.HeaderConstants;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.core.AirportDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.core.CountryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.request.AirportListReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.request.CountryListReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.response.AirportListResponseMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.master_data.response.CountryListResponseMsg;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class MasterDataHelper {

    private static Logger logger = Logger.getLogger(MasterDataHelper.class);
    private MasterDataController controller;

    public MasterDataHelper() {
        try {
            controller = GHGControllerFactory.getInstance().getMasterDataController();

        } catch (Exception e) {
            logger.error("Error in intialing controllers");
        }
    }


    public CountryListResponseMsg getCountries(CountryListReqMsg msg, int user) {
        logger.info("Message : " + msg);

        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);


        CountryListResponseMsg res = new CountryListResponseMsg();
        List<CountryDTO> results = new ArrayList<>();
        ListResult<Country> listResult = controller.getCountries(pageNumber, sorting, filter);

        if (listResult != null && listResult.getList() != null) {
            logger.info("Fetched count: " + listResult.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Country c : listResult.getList()) {
                CountryDTO dto = new CountryDTO();
                dto.setId(c.getId());
                dto.setName(c.getName());
                results.add(dto);
            }
        } else {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        }
        res.setList(results);
        return res;
    }

    public AirportListResponseMsg getAirports(AirportListReqMsg msg, int user) {
        logger.info("Message : " + msg);
        List<AirportDTO> results = new ArrayList<>();

        Object filter = msg.getFilterModel();
        Object sorting = msg.getSortModel();
        int pageNumber = msg.getPageNumber();
        logger.info("filter : " + filter + "  sorting : " + sorting + "pageNumber: " + pageNumber);


        AirportListResponseMsg res = new AirportListResponseMsg();

        ListResult<Airport> listResult = controller.getAirports(pageNumber, sorting, filter);

        if (listResult != null && listResult.getList() != null) {
            logger.info("Fetched count: " + listResult.getList().size());
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_SUCCESS_GENERAL);
            for (Airport c : listResult.getList()) {
                AirportDTO dto = new AirportDTO();
                dto.setId(c.getId());
                dto.setName(c.getName());
                results.add(dto);
            }
        } else {
            res.setActionStatus(HeaderConstants.RESPONSE_HEADER_STATUS_FAILED_GENERAL);
        }
        res.setList(results);
        return res;

    }
}
