package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.ManageMitigationActivityData;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.mitigation_activity.core.MitigationProjectActData;
import com.climatesi.ghg_mitication.api.beans.MitigationProjectMonthlyActivity;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectMonthlyActivityBean;
import org.jboss.logging.Logger;

public class MitigationActDataPopulator {

    private static Logger logger  = Logger.getLogger(MitigationActDataPopulator.class);

    private static MitigationActDataPopulator instance;

    public static MitigationActDataPopulator getInstance() {
        if (instance == null) {
            instance =  new MitigationActDataPopulator();
        }
        return instance;
    }

    public MitigationProjectMonthlyActivity populate(ManageMitigationActivityData dto, MitigationProjectMonthlyActivity bean) {

        if (bean == null) { //new

            bean = new MitigationProjectMonthlyActivityBean();
            bean.setMonth(dto.getMonth());
            bean.setYear(dto.getYear());
            bean.setProjectId(dto.getProjectId());
            bean.setSavedEmission(0);
            bean.setUnit(dto.getUnit());
            bean.setValue(dto.getValue());
            bean.setUpdatedAt(System.currentTimeMillis());


        }else { //update
           bean.setMonth(dto.getMonth());
           bean.setValue(dto.getValue());

        }
        return bean;
    }
}
