package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.ElectricityPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.ElectricityEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.climatesi.ghg.emission_source.api.facades.ElectricityEntryManager;
import com.climatesi.ghg.emission_source.api.facades.ElectricityMeterReadingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.ElectricityEntryBean;
import com.climatesi.ghg.implGeneral.data.GridElectricityData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.implGeneral.data.TandDLossCalcData;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class ElectricityController extends AbstractController {

    private static final Logger logger = Logger.getLogger(ElectricityController.class);
    private ElectricityEntryManager electricityEntryManager;
    private ElectricityMeterReadingEntryManager electricityMeterReadingEntryManager;
    private EmissionFactorsManager emissionFactorsManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

            electricityEntryManager = EmsissionSourceFactory.getInstance().getElectricityEntryManager(em);
            electricityMeterReadingEntryManager = EmsissionSourceFactory.getInstance().getElectricityMeterReadingEntryManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing electrictiy Controller", e);
        }
    }

    public ListResult<ElectricityEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return electricityEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public ElectricityEntry editElectricityEntry(ElectricityEntryDTO dto) throws GHGException {
        ElectricityEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                ElectricityPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emissionElectricity(entry));
                }

                //          Todo: add user id
                status = (String) electricityEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new ElectricityEntryBean();
                ElectricityPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emissionElectricity(entry));
                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) electricityEntryManager.addEntity(entry);
            }

            //        Todo check status and throw exceptions

        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public ElectricityEntry findById(Integer id) {
        return electricityEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emissionElectricity(ElectricityEntry entry) {
        if (entry == null) {
            new HashMap<String, String>();
        }
        float consumption = entry.getConsumption();
//
//        int year_c = Calendar.getInstance().get(Calendar.YEAR);
//        String year_s = entry.getYear();
//        int year =Integer.parseInt(year_s);
//
//        if(year == year_c){
//
//        }
//        else if(year == year_c-1){
//
//        }
//        else if(year == year_c-2){
//
//        }


        float ef_grid = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NATIONAL_GRID_EMISSION_FACTOR).getValue();
        float etd = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.T_AND_D_LOSS_PERCENT).getValue();

        GridElectricityData gridElectricityData = new GridElectricityData();
        gridElectricityData.setConsumption(consumption);
        gridElectricityData.setEf_grid_electricity(ef_grid);
        ResultDTO  e1 = Calculator.emissionGridElectricity(gridElectricityData);

        TandDLossCalcData tandDLossCalcData = new TandDLossCalcData();
        tandDLossCalcData.setConsumption(consumption/1000f);
        tandDLossCalcData.setEf_grid_electricity(ef_grid);
        tandDLossCalcData.setT_d_percent(etd);
        ResultDTO e2 = Calculator.emissionTandDLoss(tandDLossCalcData);

        HashMap<String, String> results = new HashMap<>();
        results.put("tco2", String.format("%.5f", e1.tco2e ));
        results.put("co2", String.format("%.5f",e1.co2 ));
        results.put("n20", String.format("%.5f",e1.n2o ));
        results.put("ch4", String.format("%.5f", e1.ch4));
        results.put("quantity", String.format("%.5f", consumption ));
        results.put("direct", Boolean.toString(false));
        results.put("gridEmissionFactor", String.format("%.5f", ef_grid));
        results.put("tandDPerc", String.format("%.5f", etd ));
        results.put("tco2_t_d",String.format("%.5f", e2.tco2e));
        return results;
    }

}
