package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.facades.BranchManager;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.facades.*;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

@Stateless
public class SummaryInputsController extends AbstractController {

    private static Logger logger = Logger.getLogger(SummaryInputsController.class);
    private MunicipalWaterEntryManager municipalWaterEntryManager;
    private ElectricityEntryManager electricityEntryManager;
    private GeneratorsEntryManager generatorsEntryManager;
    private RefrigerantsEntryManager refrigerantsEntryManager;
    private FireExtingEntryManager fireExtingEntryManager;
    private WasteTransportEntryManager wasteTransportEntryManager;
    private EmployeCommutingEntryManager employeCommutingEntryManager;
    private VehicleEntryManager vehicleEntryManager;
    private WasteDisposalEntryManager wasteDisposalEntryManager;
    private TransportLocalEntryManager transportLocalEntryManager;
    private CompanyManager companyManager;
    private BranchManager branchManager;

    @PostConstruct
    @Override
    public void initialize() {
        try {
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            transportLocalEntryManager = EmsissionSourceFactory.getInstance().getTransportLocalEntryManager(em);
            municipalWaterEntryManager = EmsissionSourceFactory.getInstance().getMunicipalWaterEntryManager(em);
            electricityEntryManager = EmsissionSourceFactory.getInstance().getElectricityEntryManager(em);
            generatorsEntryManager = EmsissionSourceFactory.getInstance().getGeneratorsEntryManager(em);
            refrigerantsEntryManager = EmsissionSourceFactory.getInstance().getRefrigerantsEntryManager(em);
            fireExtingEntryManager = EmsissionSourceFactory.getInstance().getFireExtingEntryManager(em);
            wasteTransportEntryManager = EmsissionSourceFactory.getInstance().getWasteTransportEntryManager(em);
            employeCommutingEntryManager = EmsissionSourceFactory.getInstance().getEmployeeCommEntryManager(em);
            vehicleEntryManager = EmsissionSourceFactory.getInstance().getVehicleEntryManager(em);
            wasteDisposalEntryManager = EmsissionSourceFactory.getInstance().getWasteDisposalEntryManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);

        } catch (Exception e) {
            logger.error("Error in intializing Emission Calc Controller", e);
        }
    }

}
