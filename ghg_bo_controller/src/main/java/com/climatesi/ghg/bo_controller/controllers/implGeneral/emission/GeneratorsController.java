package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.GeneratorsPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.GeneratorsEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.GeneratorsEntry;
import com.climatesi.ghg.emission_source.api.enums.FuelTypes;
import com.climatesi.ghg.emission_source.api.enums.Units;
import com.climatesi.ghg.emission_source.api.facades.GeneratorsEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.GeneratorsEntryBean;
import com.climatesi.ghg.implGeneral.data.DiselGeneratorCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class GeneratorsController extends AbstractController {

    private static final Logger logger = Logger.getLogger(GeneratorsController.class);
    private GeneratorsEntryManager generatorsEntryManager;
    private EmissionFactorsManager emissionFactorsManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            generatorsEntryManager = EmsissionSourceFactory.getInstance().getGeneratorsEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing GeneratorController", e);
        }
    }

    public ListResult<GeneratorsEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return generatorsEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public GeneratorsEntry editGeneratorsEntry(GeneratorsEntryDTO dto) throws GHGException {
        GeneratorsEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {

                entry = findById(dto.getEntryId());
                GeneratorsPopulator.getInstance().populate(entry, dto);
                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emissionDieselGenerators(entry));

                }
                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id
                status = (String) generatorsEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new GeneratorsEntryBean();
                entry.setEntryAddedDate(new Date());
                GeneratorsPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emissionDieselGenerators(entry));

//          Todo: add user id
                status = (String) generatorsEntryManager.addEntity(entry);
            }
            // Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public GeneratorsEntry findById(Integer id) {
        return generatorsEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emissionDieselGenerators(GeneratorsEntry o) {

        float consumptionDiesel_m3 = 0;
        float consumptionDiesel_liters = 0;
        float consumptionDiesel_ruppee = 0;
        float consumptionPetrol_m3 = 0;
        float consumptionPetrol_liters = 0;
        float consumptionPetrol_rupee = 0;


        if (o != null) {
            if (o.getFuelType() == FuelTypes.Petrol.type) {
                if (o.getUnits() == Units.m3.unit) {
                    consumptionPetrol_m3 += o.getConsumption();
                } else if (o.getUnits() == Units.liters.unit) {
                    consumptionPetrol_liters += o.getConsumption();
                } else if (o.getUnits() == Units.LKR.unit) {
                    consumptionPetrol_rupee += o.getConsumption();
                }
            } else if (o.getFuelType() == FuelTypes.Diesel.type) {
                if (o.getUnits() == Units.m3.unit) {
                    consumptionDiesel_m3 += o.getConsumption();
                } else if (o.getUnits() == Units.liters.unit) {
                    consumptionDiesel_liters += o.getConsumption();
                } else if (o.getUnits() == Units.LKR.unit) {
                    consumptionDiesel_ruppee += o.getConsumption();
                }
            }
        } else {
            logger.error("Error in  null entry ");
        }

        float price_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_DIESEL_LITER).getValue();
        //Diesl conversion
        consumptionDiesel_liters += (consumptionDiesel_m3 * 1000); // Todo : add ruppe conversion
        consumptionDiesel_liters += (consumptionDiesel_ruppee / price_diesel);

        DiselGeneratorCalcData diselGeneratorCalcData = new DiselGeneratorCalcData();
        float den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();
        float net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();
        float ef_co2_diesel_sta = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_STATIONARY_DIESEL).getValue();
        float ef_ch4_diesel_sta = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_STATIONARY_DIESEL).getValue();
        float ef_n20_diesel_sta = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N2O_STATIONARY_DIESEL).getValue();
        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();

        diselGeneratorCalcData.setConsumption(consumptionDiesel_liters);
        diselGeneratorCalcData.setGwp_CO2(gwp_co2);
        diselGeneratorCalcData.setGwp_CH4(gwp_ch4);
        diselGeneratorCalcData.setGwp_N2O(gwp_n20);
        diselGeneratorCalcData.setDensity(den_diesel);
        diselGeneratorCalcData.setNetCaloricValue(net_cal_diesel);
        diselGeneratorCalcData.setEf_CO2_s_n(ef_co2_diesel_sta);
        diselGeneratorCalcData.setEf_CH4_s_n(ef_ch4_diesel_sta);
        diselGeneratorCalcData.setEf_N20_s_n(ef_n20_diesel_sta);

        // Todo add petrol consuption

        ResultDTO emission = Calculator.emissionGenerators(diselGeneratorCalcData);
        float te_dg = (consumptionDiesel_liters / 1000) * den_diesel * net_cal_diesel;

        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", emission.tco2e) );
        results.put("co2",String.format("%.5f", emission.co2) );
        results.put("n20",String.format("%.5f", emission.n2o));
        results.put("ch4",String.format("%.5f", emission.ch4) );
        results.put("quantity",String.format("%.5f", consumptionDiesel_liters ));
        results.put("direct", Boolean.toString(true));
        results.put("density_diesel",String.format("%.5f", den_diesel) );
        results.put("net_caloric",String.format("%.5f", net_cal_diesel) );
        results.put("ef_co2",String.format("%.5f", ef_co2_diesel_sta) );
        results.put("ef_ch4",String.format("%.5f", ef_ch4_diesel_sta) );
        results.put("ef_n20", Float.toString(ef_n20_diesel_sta));
//        results.put("ef_n20", Float.toString(ef_n20_diesel_sta));
        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20) );
        results.put("te_dg",String.format("%.5f", te_dg));
        return results;
    }

}
