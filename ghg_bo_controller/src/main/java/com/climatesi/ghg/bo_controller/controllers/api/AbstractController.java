package com.climatesi.ghg.bo_controller.controllers.api;

import com.climatesi.ghg_mitication.api.beans.MitigationProject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractController {

    @PersistenceContext(unitName = "bo_db")
    protected EntityManager em;

    public abstract void initialize();

    
}
