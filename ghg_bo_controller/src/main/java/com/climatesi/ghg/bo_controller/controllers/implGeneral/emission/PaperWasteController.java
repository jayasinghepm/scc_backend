package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;


import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.BioMassPopulator;
import com.climatesi.ghg.bo_controller.populators.PaperWastePopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.PaperWasteDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.PaperWasteEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.PaperWasteEntryManagerFacade;
import com.climatesi.ghg.emission_source.implGeneral.facades.PaperWasteEntryManagerFacade;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class PaperWasteController   extends AbstractController {

    private static final Logger logger = Logger.getLogger(PaperWasteController.class);


    private PaperWasteEntryManagerFacade paperWasteEntryManagerFacade;

    private EmissionFactorsManager emissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {

        try {
            paperWasteEntryManagerFacade = (PaperWasteEntryManagerFacade) EmsissionSourceFactory.getInstance().getPaperWasteManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing MunicipalWaterController", e);
        }
    }


    public ListResult<PaperWaste> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return paperWasteEntryManagerFacade.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public PaperWaste edit(PaperWasteDTO dto) throws GHGException {
        PaperWaste entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                PaperWastePopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.getIsDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) paperWasteEntryManagerFacade.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new PaperWasteEntryBean();
                PaperWastePopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emission(entry));
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) paperWasteEntryManagerFacade.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public PaperWaste findById(Integer id) {
        return paperWasteEntryManagerFacade.getEntityByKey(id);
    }

    public HashMap<String, String> emission(PaperWaste entry) {

        float tons = entry.getQuantity() / 1000;
        //float ef = 21.354f * 0.001f;
        float ef ;


        ef = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PAPER_WASTE_EF_CO2).getValue() * 0.001f;


        ResultDTO e1 = Calculator.emissionPaperWaste(tons, ef);


        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", tons) );
        results.put("ef",String.format("%.5f", ef) );
        results.put("direct", Boolean.toString(false));



        return results;
    }

}
