package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.WebNotification;
import com.climatesi.ghg.bo_messaging.implGeneral.ws_message.WebNotificationDTO;
import com.climatesi.ghg.implGeneral.beans.WebNotificationBean;
import org.jboss.logging.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class NotificationPopulator {

    private static NotificationPopulator instance;

    private Logger logger = Logger.getLogger(NotificationPopulator.class);

    private NotificationPopulator() {


    }

    public static NotificationPopulator getInstance() {
        if (instance == null) {
            instance = new NotificationPopulator();
        }
        return instance;
    }

    public WebNotificationDTO populateDTO(WebNotification bean) {
        WebNotificationDTO dto = new WebNotificationDTO();
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:SS z");
            df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
            String IST = df.format(bean.getAddedDate());
            dto.setAddedTime(IST);
        }catch (Exception e) {
            logger.error(e);
        }

        dto.setMessage(bean.getMessage());
        dto.setRead(bean.getRead() +1);
        dto.setLink(bean.getLink());
        dto.setId(bean.getId());
        dto.setUserId(bean.getUserId());
        return dto;
    }

    public WebNotificationBean populate(WebNotificationBean bean, WebNotificationDTO dto) {
        if (dto.getId() > 0) {
            bean.setId(dto.getId());
            if (dto.getRead() == 2) {
                bean.setRead(1);
            }
        }
        else {
            bean.setAddedDate(new Date());
            bean.setDeleted(0);
            bean.setRead(0);
        }
            bean.setMessage(dto.getMessage());
            bean.setLink(dto.getLink());
            bean.setUserId(dto.getUserId());
        return bean;
    }
}
