package com.climatesi.ghg.bo_controller.controllers.implGeneral.master_data;

import com.climatesi.ghg.api.MasterDataFactory;
import com.climatesi.ghg.api.beans.Airport;
import com.climatesi.ghg.api.beans.Country;
import com.climatesi.ghg.api.facades.AirportFacade;
import com.climatesi.ghg.api.facades.CountryFacade;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.implGeneral.StringMatcher;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Stateless
public class MasterDataController extends AbstractController {

    private static Logger logger = Logger.getLogger(MasterDataController.class);
    private AirportFacade airportManager;
    private CountryFacade countryManager;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            airportManager = MasterDataFactory.getInstance().getAirportFacade(em);
            countryManager = MasterDataFactory.getInstance().getCountryFacade(em);
        } catch (Exception e) {
            logger.error("Error in intializing MasterData Controller", e);
        }
    }

    public ListResult<Country> getCountries(int pageNum, Object sorting, Object filter) {
        return countryManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public ListResult<Airport> getAirports(int pageNum, Object sorting, Object filter) {
        ListResult<Airport> airportListResult = airportManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
        List<Airport> newlist =new ArrayList<>();
        if (filter != null && !filter.equals("") && airportListResult != null ){
            JsonParser parser = new JsonParser();
            JsonObject filterModelObj = parser.parse(new Gson().toJson(filter)).getAsJsonObject();
            JsonObject queryObject = filterModelObj.getAsJsonObject("searchName");
            if (queryObject != null) {
                String query = queryObject.get("value").getAsString();
                Collections.sort(airportListResult.getList(),(a1, a2) -> {
                    int a1_score = StringMatcher.getMatchScore(query, a1.getSearchName());
                    int a2_score = StringMatcher.getMatchScore(query, a2.getSearchName());
                    if (a1_score == a2_score) {
                        return 0;
                    }
                    else if (a1_score > a2_score) {
                        return 1;
                    }else {
                        return -1;
                    }
                });
                List<Airport> smallList = new ArrayList<>();
                if (airportListResult.getList().size() > 100) {
                   smallList = airportListResult.getList().subList(0, 100);
                    airportListResult.setList(smallList);
                }
            }
        }


        return airportListResult;

    }
}
