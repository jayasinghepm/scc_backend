package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.WasteTransportPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.WasteTransportEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.WasteTransportEntry;
import com.climatesi.ghg.emission_source.api.enums.FuelTypes;
import com.climatesi.ghg.emission_source.api.facades.WasteTransportEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.WasteTransportEntryBean;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.implGeneral.data.WasteTransportCalcData;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class WasteTransportController extends AbstractController {
    private static final Logger logger = Logger.getLogger(WasteTransportController.class);
    private WasteTransportEntryManager wasteTransportEntryManager;
    private EmissionFactorsManager emissionFactorsManager;



    @PostConstruct
    @Override
    public void initialize() {
        try {
            wasteTransportEntryManager = EmsissionSourceFactory.getInstance().getWasteTransportEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

        } catch (GHGException e) {
            logger.error("Error in intializing Waste Transport controller", e);
        }
    }

    public ListResult<WasteTransportEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return wasteTransportEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public WasteTransportEntry editWasteTransportEntry(WasteTransportEntryDTO dto) throws GHGException {
        WasteTransportEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                entry = WasteTransportPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id

                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emissionWasteTransport(entry));
                    entry.setWasteTons(dto.getLoadingCapacity());

                }


                status = (String) wasteTransportEntryManager.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {

                entry = new WasteTransportEntryBean();
                entry.setWasteTons(dto.getLoadingCapacity());
                WasteTransportPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emissionWasteTransport(entry));

                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) wasteTransportEntryManager.addEntity(entry);
            }
            // todo;
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public WasteTransportEntry findById(Integer id) {
        return wasteTransportEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emissionWasteTransport(WasteTransportEntry o) {
        ResultDTO emission = null;

        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();

        float ef_co2_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE).getValue();
        float ef_co2_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL).getValue();
        float ef_ch4_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL).getValue();
        float ef_ch4_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE).getValue();
        float ef_n20_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE).getValue();
        float ef_n20_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL).getValue();

        float den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();
        float net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();

        float den_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL).getValue();
        float net_cal_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();

        float petrol_cons = 0;
        float diesel_cons = 0;

        WasteTransportCalcData calcData = new WasteTransportCalcData();
        HashMap<String, String> results = new HashMap<>();

        if (o != null) {
            if (o.getFuelType() == FuelTypes.Petrol.type) {
                petrol_cons += ((o.getWasteTons() / o.getLoadingCapacity()) * o.getNoOfTurns() * o.getDistanceTravelled() / o.getFuelEconomy());
                calcData.setDensity_fuel_t_m3(den_petrol);
                calcData.setWaste_tonnes(o.getWasteTons());
                calcData.setLoading_capacity_tonnes(o.getLoadingCapacity());
                calcData.setUp_down_distance_km(o.getDistanceTravelled());
                calcData.setNo_of_turns(o.getNoOfTurns());//edit by pasindu
                calcData.setFuel_economy_km_l(o.getFuelEconomy());
                calcData.setNet_caloric_fuel(net_cal_petrol);
                calcData.setEf_co2(ef_co2_petrol);
                calcData.setEf_ch4(ef_ch4_petrol);
                calcData.setEf_n2o(ef_n20_petrol);
                calcData.setGwp_co2(gwp_co2);
                calcData.setGwp_ch4(gwp_ch4);
                calcData.setGwp_n2o(gwp_n20);

                emission = Calculator.emissionWasteTransport(calcData);
                results.put("tco2",String.format("%.5f", emission.tco2e ) );
                results.put("co2",String.format("%.5f", emission.co2 ) );
                results.put("n20",String.format("%.5f", emission.n2o) );
                results.put("ch4",String.format("%.5f", emission.ch4 ) );
                results.put("quantity",String.format("%.5f", petrol_cons ) );
                results.put("direct", Boolean.toString(false));
                results.put("gwp_co2",String.format("%.5f", gwp_co2 ) );
                results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
                results.put("gwp_n20",String.format("%.5f", gwp_n20 ) );
                results.put("ef_co2",String.format("%.5f", ef_co2_petrol ) );
                results.put("ef_ch4",String.format("%.5f", ef_ch4_petrol ) );
                results.put("ef_n20",String.format("%.5f", ef_n20_petrol ) );
                results.put("den",String.format("%.5f", den_petrol) );
                results.put("net_cal",String.format("%.5f", net_cal_petrol ) );

            } else if (o.getFuelType() == FuelTypes.Diesel.type) {
                diesel_cons += ((o.getWasteTons() / o.getLoadingCapacity()) * o.getNoOfTurns() * o.getDistanceTravelled() / o.getFuelEconomy());
                calcData.setDensity_fuel_t_m3(den_diesel);
                calcData.setWaste_tonnes(o.getWasteTons());
                calcData.setLoading_capacity_tonnes(o.getLoadingCapacity());
                calcData.setUp_down_distance_km(o.getDistanceTravelled());
                calcData.setFuel_economy_km_l(o.getFuelEconomy());
                calcData.setNo_of_turns(o.getNoOfTurns());//edit by pasindu
                calcData.setNet_caloric_fuel(net_cal_diesel);
                calcData.setEf_co2(ef_co2_diesel);
                calcData.setEf_ch4(ef_ch4_diesel);
                calcData.setEf_n2o(ef_n20_diesel);
                calcData.setGwp_co2(gwp_co2);
                calcData.setGwp_ch4(gwp_ch4);
                calcData.setGwp_n2o(gwp_n20);

                emission = Calculator.emissionWasteTransport(calcData);

                results.put("tco2",String.format("%.5f", emission.tco2e ) );
                results.put("co2",String.format("%.5f", emission.co2) );
                results.put("n20",String.format("%.5f", emission.n2o) );
                results.put("ch4",String.format("%.5f", emission.ch4 ) );
                results.put("quantity",String.format("%.5f", diesel_cons ));
                results.put("gwp_co2",String.format("%.5f", gwp_co2 ) );
                results.put("gwp_ch4",String.format("%.5f", gwp_ch4 ) );
                results.put("gwp_n20",String.format("%.5f", gwp_n20) );
                results.put("ef_co2",String.format("%.5f", ef_co2_diesel ) );
                results.put("ef_ch4",String.format("%.5f", ef_ch4_diesel) );
                results.put("ef_n20",String.format("%.5f", ef_n20_diesel ) );
                results.put("den",String.format("%.5f", den_diesel) );
                results.put("net_cal",String.format("%.5f", net_cal_diesel ) );
            } else {
                // todo:
            }
        } else {
            logger.error("Error null entry");
        }


        return results;
    }


}
