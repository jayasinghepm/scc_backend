package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;
import com.climatesi.ghg.api.facade.PublicTransportEmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.PubTransPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.PubTransEmFactorDTO;
import com.climatesi.ghg.implGeneral.PublicTransportEmissionFactorsBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

@Stateless
public class PubTransEmissionFactorsController extends AbstractController {
    private static final Logger logger = Logger.getLogger(PubTransEmissionFactorsController.class);
    private PublicTransportEmissionFactorsManager manager;

    @PostConstruct
    @Override
    public void initialize() {
        try {
            manager = EmissionCalcFacotory.getInstance().getPublicTransportEmissionFactorsManager(em);
        } catch (Exception e) {
            logger.error("Error in intializing controllers");
        }
    }

    public ListResult<PublicTransposrtEmisssionFactor> getFilteredResult(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return manager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public PublicTransposrtEmisssionFactor edit(PubTransEmFactorDTO dto) throws Exception {
        PublicTransposrtEmisssionFactor entry;

        String status = null;
        try {
//        update
            if (dto.getVehicleType() > 0) {
                entry = findById(dto.getVehicleType());
                if (entry == null) {
                    throw new GHGException("Branch is not found");
                }

                PubTransPopulator.getInstance().populate(entry, dto);
//                entry.se(new Date());
                //          Todo: add user id
                status = (String) manager.updateEntity(entry);

//         add new
            } else {
                entry = new PublicTransportEmissionFactorsBean();
                PubTransPopulator.getInstance().populate(entry, dto);
//                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) manager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public PublicTransposrtEmisssionFactor findById(Integer id) {
        return manager.getEntityByKey(id);
    }

    public ListResult<PublicTransposrtEmisssionFactor> getFilteredResults(int pageNum, Object sorting, Object filter) {
        return manager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

}
