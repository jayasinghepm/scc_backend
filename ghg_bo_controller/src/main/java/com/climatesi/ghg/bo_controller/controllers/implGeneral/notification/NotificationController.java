package com.climatesi.ghg.bo_controller.controllers.implGeneral.notification;

import com.climatesi.ghg.api.NotificationFactory;
import com.climatesi.ghg.api.beans.WebNotification;
import com.climatesi.ghg.api.facade.WebNotificationManger;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.NotificationPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.ws_message.WebNotificationDTO;
import com.climatesi.ghg.implGeneral.beans.WebNotificationBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class NotificationController extends AbstractController {

    private static Logger logger = Logger.getLogger(NotificationController.class);
    private WebNotificationManger webNotificationManger;


    @PostConstruct
    @Override
    public void initialize() {
        try {
            webNotificationManger = NotificationFactory.getInstance().getWebNotificationManger(em);
        }catch (Exception e) {
            logger.error(e);
        }
    }

    public WebNotification editNotification(WebNotificationDTO dto ) throws Exception {
        WebNotification entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                logger.info("Updating notifications: " + dto.getMessage());
                entry = webNotificationManger.getEntityByKey(dto.getId());
                if (entry == null) {
                    throw new GHGException("Notification is not found");
                }

                NotificationPopulator.getInstance().populate((WebNotificationBean) entry, dto);
//                entry.se(new Date());
                //          Todo: add user id
                status = (String) webNotificationManger.updateEntity(entry);


//         add new
            } else {
                logger.info("Creating notifications: " + dto.getMessage());
                entry = new WebNotificationBean();

                NotificationPopulator.getInstance().populate((WebNotificationBean) entry, dto);
//                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) webNotificationManger.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public List<WebNotification> getNotifications(int userId) {
        JsonObject filter = new JsonObject();
        JsonObject userFilter = new JsonObject();
        userFilter.addProperty("type", 1);
        userFilter.addProperty("value", userId);
        userFilter.addProperty("col", 1);
        filter.add("userId", userFilter);

        JsonObject readFilter = new JsonObject();
        readFilter.addProperty("type", 1);
        readFilter.addProperty("value", 0);
        readFilter.addProperty("col", 1);
        filter.add("read", readFilter);

        JsonObject sorter = new JsonObject();
        JsonObject timeSorter = new JsonObject();
        timeSorter.addProperty("dir", "DESC");
        sorter.add("addedDate", timeSorter);

        int pg = -1;

        ListResult<WebNotification> listResult = webNotificationManger.getPaginatedEntityListByFilter(pg, sorter, filter);
        List<WebNotification> list = listResult != null ? listResult.getList() : new ArrayList<>();
        if (list.size() < 5) {
            // todo: find read 5 notificaions;
        }

        return list;

    }
}
