package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission_factors;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.api.facade.WasteDisposalFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.WasteDisFactorsPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission_factors.core.WasteDisFactorDTO;
import com.climatesi.ghg.implGeneral.WasteDisposalFactorsBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

@Stateless
public class WasteDisposalFactorsControllers extends AbstractController {

    private static final Logger logger = Logger.getLogger(WasteDisposalFactorsControllers.class);
    private WasteDisposalFactorsManager manager;

    @PostConstruct
    @Override
    public void initialize() {
        try {
            manager = EmissionCalcFacotory.getInstance().getWasteDisposalFactorsManager(em);
        } catch (Exception e) {
            logger.error("Error in intializing controllers");
        }
    }

    public WasteDisposalFactors findById(int id) {
        return manager.getEntityByKey(id);
    }

    public WasteDisposalFactors edit(WasteDisFactorDTO dto) throws GHGException {
        WasteDisposalFactors entry;

        String status = null;
        try {
//        update
            if (dto.getWasteType() > 0) {
                entry = findById(dto.getWasteType());
                if (entry == null) {
                    throw new GHGException("Branch is not found");
                }

                WasteDisFactorsPopulator.getInstance().populate(entry, dto);
//                entry.se(new Date());
                //          Todo: add user id
                status = (String) manager.updateEntity(entry);

//         add new
            } else {
                entry = new WasteDisposalFactorsBean();
                WasteDisFactorsPopulator.getInstance().populate(entry, dto);
//                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) manager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public ListResult<WasteDisposalFactors> getFilteredResults(int pageNum, Object sorting, Object filter) {
        return manager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }
}
