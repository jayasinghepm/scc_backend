package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.TransportLocalPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.TransportLocalEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.TransportLocalEntry;
import com.climatesi.ghg.emission_source.api.enums.FuelTypes;
import com.climatesi.ghg.emission_source.api.facades.TransportLocalEntryManager;
import com.climatesi.ghg.emission_source.api.facades.WasteTransportEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.TransportLocalEntryBean;
import com.climatesi.ghg.implGeneral.data.CompanyOwnedVehicleCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class TransportLocalController extends AbstractController {
    private static final Logger logger = Logger.getLogger(TransportLocalController.class);
    private TransportLocalEntryManager transportLocalEntryManager;
    private WasteTransportEntryManager wasteTransportEntryManager;
    private EmissionFactorsManager emissionFactorsManager;


    @PostConstruct
    @Override
    public void initialize() {

        try {
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);

            transportLocalEntryManager = EmsissionSourceFactory.getInstance().getTransportLocalEntryManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing Tranport local controller", e);
        }

    }

    public ListResult<TransportLocalEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return transportLocalEntryManager.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public TransportLocalEntry editTransportLocalEntry(TransportLocalEntryDTO dto) throws GHGException {
        TransportLocalEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                TransportLocalPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());

                if (entry.isDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }
                //          Todo: add user id
                status = (String) transportLocalEntryManager.updateEntity(entry);

                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new TransportLocalEntryBean();
                TransportLocalPopulator.getInstance().populate(entry, dto);
                entry.setEntryAddedDate(new Date());
                entry.setEmissionDetails(emission(entry));

//          Todo: add user id
                status = (String) transportLocalEntryManager.addEntity(entry);
            }
            // todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public TransportLocalEntry findById(Integer id) {
        return transportLocalEntryManager.getEntityByKey(id);
    }

    public HashMap<String, String> emission(TransportLocalEntry o) {
        HashMap<String, String> results = new HashMap<>();
        float diesel = 0;
        float petrol = 0;

        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();

        float ef_co2_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_GASOLINE).getValue();
        float ef_co2_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CO2_MOBILE_DIESEL).getValue();
        float ef_ch4_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_DIESEL).getValue();
        float ef_ch4_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_CH4_MOBILE_GASOLINE).getValue();
        float ef_n20_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_GASOLINE).getValue();
        float ef_n20_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.EF_N20_MOBILE_DIESEL).getValue();

        float den_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_DIESEL).getValue();
        float net_cal_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();

        float den_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.DENSITY_FUEL_PETROL).getValue();
        float net_cal_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.NET_CALORIC_DIESEL).getValue();

        float price_diesel = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_DIESEL_LITER).getValue();
        float price_petrol = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.PRICE_PETROL_LITER).getValue();
        float consumption = 0;
        ResultDTO emission = null;

        if (o != null) {
            CompanyOwnedVehicleCalcData calcData = new CompanyOwnedVehicleCalcData();
//            consumption =   o.getDistanceTravelled() /o.getFuelEconomy();
            consumption =   o.getFuelConsumption();
            if (o.getFuelType() == FuelTypes.Petrol.type) {
                petrol += (consumption);
                calcData.setConsumption_liters(consumption);
                calcData.setDensity(den_petrol);
                calcData.setNet_caloric_value(net_cal_petrol);
                calcData.setEf_co2(ef_co2_petrol);
                calcData.setEf_ch4(ef_ch4_petrol);
                calcData.setEf_n2o(ef_n20_petrol);
                calcData.setGwp_co2(gwp_co2);
                calcData.setGwp_ch4(gwp_ch4);
                calcData.setGwp_n2o(gwp_n20);

                emission = Calculator.emissionCompanyOwnedVehicle(calcData);
                results.put("ef_co2",String.format("%.5f", ef_co2_petrol) );
                results.put("ef_ch4",String.format("%.5f", ef_ch4_petrol) );
                results.put("ef_n20",String.format("%.5f", ef_n20_petrol));
                results.put("den",String.format("%.5f", den_petrol) );
                results.put("net_cal",String.format("%.5f", net_cal_petrol));
            } else if (o.getFuelType() == FuelTypes.Diesel.type) {
                diesel +=(consumption);
                calcData.setConsumption_liters(consumption);
                calcData.setDensity(den_diesel);
                calcData.setNet_caloric_value(net_cal_diesel);
                calcData.setEf_co2(ef_co2_diesel);
                calcData.setEf_ch4(ef_ch4_diesel);
                calcData.setEf_n2o(ef_n20_diesel);
                calcData.setGwp_co2(gwp_co2);
                calcData.setGwp_ch4(gwp_ch4);
                calcData.setGwp_n2o(gwp_n20);
                emission = Calculator.emissionCompanyOwnedVehicle(calcData);
                results.put("ef_co2",String.format("%.5f", ef_co2_diesel) );
                results.put("ef_ch4",String.format("%.5f", ef_ch4_diesel));
                results.put("ef_n20",String.format("%.5f", ef_n20_diesel) );
                results.put("den",String.format("%.5f", den_diesel) );
                results.put("net_cal",String.format("%.5f", net_cal_diesel) );
            }

        }
        results.put("tco2",String.format("%.5f", emission.tco2e) );
        results.put("co2",String.format("%.5f", emission.co2)) ;
        results.put("n20",String.format("%.5f", emission.n2o) );
        results.put("ch4", String.format("%.5f",emission.ch4));
        results.put("diesel",String.format("%.5f", diesel) );
        results.put("petrol",String.format("%.5f", petrol) );
        results.put("quantity",String.format("%.5f", consumption) );
        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20 ));
        return results;
    }

}
