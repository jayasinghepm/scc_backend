package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterEntryDTO;
import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterEntry;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

public class MunicipalWaterPopulator {

    private static Logger logger = Logger.getLogger(MunicipalWaterPopulator.class);
    private static MunicipalWaterPopulator instance;

    public static MunicipalWaterPopulator getInstance() {
        if (instance == null) {
            instance = new MunicipalWaterPopulator();
        }
        return instance;
    }

    public MunicipalWaterEntryDTO populateDTO(MunicipalWaterEntry entry) {
        MunicipalWaterEntryDTO dto = new MunicipalWaterEntryDTO();
        dto.setEntryId(entry.getEntryId());
        dto.setEmissionSrcId(entry.getEmissionSrcId());
        dto.setAddedBy(entry.getAddedBy());
        dto.setBranchId(entry.getBranchId());
        dto.setCompanyId(entry.getCompanyId());
        dto.setEntryAddedDate(entry.getEntryAddedDate());
        dto.setLastUpdatedDate(entry.getLastUpdatedDate());
        dto.setMonth(entry.getMonth());
        dto.setYear(entry.getYear());
        dto.setUnits(entry.getUnits());
        dto.setConsumption(entry.getConsumption());
        dto.setNumOfMeters(entry.getNumOfMeters());
        dto.setMeterNumbers(entry.getMeterNumbers());
        dto.setMeterNo(entry.getMeterNo());
        JsonObject jsonObject = new JsonObject();
        if (entry.getEmissionDetails() != null) {
            entry.getEmissionDetails().forEach((k, v) -> {
                jsonObject.addProperty(k, v);
            });
            dto.setEmissionDetails(jsonObject);
        }
        dto.setCompany(entry.getCompany());
        dto.setBranch(entry.getBranch());
        dto.setMon(entry.getMon());
        return dto;
    }

    public MunicipalWaterEntry populate(MunicipalWaterEntry entry, MunicipalWaterEntryDTO dto) {
        try {
            if (dto.getEntryId() > 0) {
                entry.setEntryId(dto.getEntryId());
                if (dto.isDeleted() == 1) {
                    entry.setDeleted(dto.isDeleted());
                    return entry;
                }
            }
            entry.setMeterNo(dto.getMeterNo());
            entry.setEmissionSrcId(dto.getEmissionSrcId());
            entry.setAddedBy(dto.getAddedBy());
            entry.setBranchId(dto.getBranchId());
            entry.setCompanyId(dto.getCompanyId());
            entry.setMonth(dto.getMonth());
            entry.setYear(dto.getYear());
            entry.setUnits(dto.getUnits());
            entry.setConsumption(dto.getConsumption());
            entry.setNumOfMeters(dto.getNumOfMeters());
            entry.setMeterNumbers(dto.getMeterNumbers());
            entry.setCompany(dto.getCompany() == null ? entry.getCompany() : dto.getCompany());
            entry.setBranch(dto.getBranch() == null ? entry.getBranch() : dto.getBranch());
            entry.setMon(dto.getMon() == null ? entry.getMon(): dto.getMon());
        } catch (NullPointerException e) {
            logger.error("Some fields null ", e);
        }

        return entry;
    }


}
