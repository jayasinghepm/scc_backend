package com.climatesi.ghg.bo_controller.controllers.implGeneral;

import com.climatesi.ghg_mitication.api.ProjectFactory;
import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg_mitication.api.facade.ProejctManager;
import at.favre.lib.crypto.bcrypt.BCrypt;
import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.LoginResponseStatus;
import com.climatesi.ghg.api.UserFactory;
import com.climatesi.ghg.api.beans.*;
import com.climatesi.ghg.api.facades.*;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.user.core.LoginUserDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.ws_message.WebNotificationDTO;
import com.climatesi.ghg.business_users.api.BusinessUserFactory;
import com.climatesi.ghg.business_users.api.beans.Admin;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.business_users.api.beans.DataEntryUser;
import com.climatesi.ghg.business_users.api.enums.UserTypes;
import com.climatesi.ghg.business_users.api.facades.AdminManager;
import com.climatesi.ghg.business_users.api.facades.CadminManager;
import com.climatesi.ghg.business_users.api.facades.ClientManager;
import com.climatesi.ghg.business_users.api.facades.DataEntryUserManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.AuthReplyBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserLoginBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.LoginStatusMaster;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class UserLoginController extends AbstractController {
    private static Logger logger = Logger.getLogger(UserLoginController.class.getName());

    private UserLoginManager userLoginManager;
    private UserSessionManager userSessionManager;
    private UserManager userManager;
    private UserLoginHistoryManager userLoginHistoryManager;
    private ClientManager clientManager;
    private AdminManager adminManager;
    private CadminManager cadminManager;
    private DataEntryUserManager dataEntryUserManager;
    private ProejctManager proejctManager;
    private CompanyManager companyManager;

    @Inject
    private Event<WebNotificationDTO> notificationSrcEvent;

    private static HashMap<Integer, Long> userLastReceivedRequestTimeHashMap = new HashMap<Integer, Long>();

    @PostConstruct
    @Override
    public void initialize() {

        try {
            userLoginManager = UserFactory.getInstance().getUserLoginManager(em);
            userManager = UserFactory.getInstance().getUserManager(em);
            userSessionManager = UserFactory.getInstance().getUserSessionManager(em);
            userLoginHistoryManager = UserFactory.getInstance().getUserLoginHistoryManager(em);
            clientManager = BusinessUserFactory.getInstance().getClientManager(em);
            adminManager = BusinessUserFactory.getInstance().getAdminManager(em);
            cadminManager = BusinessUserFactory.getInstance().getCadminManager(em);
            proejctManager = ProjectFactory.getInstance().getProejctManager(em);
            dataEntryUserManager = BusinessUserFactory.getInstance().getDataEntryUserManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);

        } catch (GHGException e) {
            logger.error("Failed to initialize UserLoginController=> ",e);
        }
    }

    public UserLogin editUserLogin(LoginUserDTO dto) throws GHGException {
        UserLogin entry;

        String status = null;
        try {
//        update
            if (dto.getUserId() > 0) {
                entry = userLoginManager.getEntityByKey(dto.getUserId());
                if (entry == null) {
                    throw new GHGException("UserLogin  is not found");
                }
//                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id
                populate(entry, dto);
                if (dto.getPassword() != null) {
                    logger.info("On line 96########################################################################");
                    String hashpw = BCrypt.withDefaults().hashToString(12, dto.getPassword().toCharArray());
                    entry.setLoginPassword(hashpw);
                }

                logger.info("on line 101 #######################################################################");
                status = (String) userLoginManager.updateEntity(entry);


//         add new
            } else {
                entry = new UserLoginBean();
                populate(entry, dto);
//                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                String hashpw = BCrypt.withDefaults().hashToString(12, dto.getPassword().toCharArray());
                entry.setLoginPassword(hashpw);
                status = (String) userLoginManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public AuthReply doLoginUser(AuthenticationData authData ) {
        logger.info("doLoginUserMethod( " + authData + " ");

        AuthReply reply = new AuthReplyBean();
//        set failed code defaultly
        reply.setLoginStatus(LoginResponseStatus.FAIL.getCode());

        UserLogin userLogin = userLoginManager.getLoginByName(authData.getUsername());

        if (userLogin == null) {
            logger.error("UserLoginController: User Not found " + authData.getUsername());
            reply.setLoginStatus(LoginResponseStatus.INVALID_LOGIN_NAME.getCode());
            reply.setNarration("Invalid Username or Password");
            return reply;
        }

        Integer loginStatus = userLogin.getStatus();
        if (loginStatus == null) {
            logger.error("UserLoginController:  Login status is NULL. Reset to Pending.");
            userLogin.setStatus(0);
            loginStatus = 0;

        }

        LoginStatusMaster loginStatusEnum = LoginStatusMaster.getEnum(loginStatus);

        String rejectMsg = "";
        boolean isValidLoginStatus = false;
        //check the status of the user login
        logger.info("Check the user login account status ...........");
        switch (loginStatusEnum) {
            case PENDING:
                rejectMsg = "Account not APPROVED";
                break;
            case LOCKED:
                rejectMsg = "Account is LOCKED";
                break;
            case SUSPENDED:
                rejectMsg = "Account is SUSPENDED";
                break;
            default:
                isValidLoginStatus = true;
        }
        if (!isValidLoginStatus) {
            logger.error("[UserLoginController.java:116] Invalid Login Status, loginStatus: "+loginStatusEnum.name());

            reply.setLoginStatus(LoginResponseStatus.INVALID_LOGIN_STATUS.getCode());
            reply.setNarration(rejectMsg);
            userLogin.setLastLoggedInDate(new Date());
            userLoginManager.updateUserLogin(userLogin);
            updateUserLoginHistory(reply,userLogin,authData);

            return reply;
        }else{
            logger.info("Active user account hence proceed with the login");
        }


        //endregion
        //check the user password with the one in DB
        BCrypt.Result pwCheck = BCrypt.verifyer().verify(authData.getPassword().toCharArray(), userLogin.getLoginPassword());
        if (!pwCheck.verified) {
            logger.error("[UserLoginController.java:130] Incorrect password");

            reply.setLoginStatus(LoginResponseStatus.INVALID_PASSWORD.getCode());
            reply.setNarration("Invalid username or password");

            int failedAttempts = userLogin.getFailedLoginAttempts();
            failedAttempts++;

            if (failedAttempts >= 5) {  //todo this needed to te taken from inst level config
                userLogin.setStatus(LoginStatusMaster.LOCKED.getCode());
            }
            userLogin.setFailedLoginAttempts(failedAttempts);
            userLogin.setLastLoggedInDate(new Date());
            userLoginManager.updateEntity(userLogin);
            updateUserLoginHistory(reply,userLogin,authData);
            return reply;
        }else{
            logger.info("Entered password matching with one at DB");
        }

        //Verify Password Expired
        if (userLogin.getPasswordExpiryDate() != null && userLogin.getPasswordExpiryDate().before(new Date())) {
            logger.error("[UserLoginController.java:150] Password has expired");
            reply.setLoginStatus(LoginResponseStatus.PASSWORD_EXPIRED.getCode());
            reply.setNarration("Password has expired");
            userLogin.setLastLoggedInDate(new Date());
            userLoginManager.updateEntity(userLogin);
            updateUserLoginHistory(reply,userLogin,authData);
            return reply;
        }else{
            logger.info("Password has not expired ....");
        }


//        //Todo :pass correct user ID here
//        User user = userManager.getEntityByKey((int) userLogin.getUserId());
//        if (user == null) {
//            //Note : Ideally this code segment is unreachable
//            logger.error("[UserLoginController.java:164] User not found");
//            reply.setLoginStatus(LoginResponseStatus.USER_NOT_FOUND.getCode());
//            reply.setNarration("Failed to Load User");
//            userLogin.setLastLoggedInDate(new Date());
//            userLoginManager.updateEntity(userLogin);
//            updateUserLoginHistory(reply,userLogin,authData);
//            return reply;
//        }

        reply.setUserId(userLogin.getUserId());
        reply.setUserType(userLogin.getUserType());
        reply.setFirstime(userLogin.getFirstTime());
        switch (userLogin.getUserType()) {
            case UserTypes.CLIENT: {
                JsonObject object = new JsonObject();
                JsonObject obj = new JsonObject();
                obj.addProperty("type", 1);
                obj.addProperty("value", userLogin.getUserId());
                obj.addProperty("col", 1);
                object.add("userId", obj);
                ListResult<Client> result = clientManager.getPaginatedEntityListByFilter(0, "", object);
                if (result.getList() != null && result.getList().size() == 1) {


                    Client c = result.getList().get(0);
                    Company company = companyManager.getEntityByKey(c.getCompanyId());
                    if (company != null) {
                        reply.setPages(company.getPages());
                        reply.setEmSources(company.getEmSources());
                    }

                    reply.setBranchId(c.getBranchId());
                    reply.setCompanyId(c.getCompanyId());
                    reply.setEntitlements(c.getEntitlements());
                    reply.setClientId(c.getId());
                    reply.setFirstName(c.getFirstName());
                    reply.setLastName(c.getLastName());
                    reply.setBranchName(c.getBranch());
                    reply.setCompanyName(c.getCompany());
                    reply.setAllowedEmissionSources(company.getAllowedEmissionSources());


                    // Find projects
                    JsonObject filter = new JsonObject();
                    JsonObject comId = new JsonObject();
                    comId.addProperty("type", 1);
                    comId.addProperty("value", c.getCompanyId());
                    comId.addProperty("col", 1);
                    filter.add("companyId", comId);
                    ListResult<Project> projects = proejctManager.getPaginatedEntityListByFilter(-1, "", filter);
                    if (projects.getList() != null && projects.getList().size() > 0) {
                        for (Project p : projects.getList()) {

                            if (p.getStatus() <= 7 && p.getStatus() >= 4 ) {
                                boolean isExp = false;
                                if (p.getExtendedDate() != null) {
                                    isExp = p.getExtendedDate().before(new Date());
                                }else {
                                    isExp = p.getEndDate().before(new Date());
                                }
                                reply.setDataEntryExpired(isExp ? 1: 0);

                                reply.setProjectStatus(p.getStatus());
                                reply.setExistActiveProject(1);

                                reply.setDisableFormulas(p.getDisableFormula());
                                break;
                            }

                        }
                        if (reply.getExistActiveProject() != 1) {
                            reply.setExistActiveProject(0);
                        }
                    }

                } else {
                    // Todo: errro
                    reply.setNarration("User not found");
                    reply.setLoginStatus(LoginResponseStatus.FAIL.getCode());
                }
                break;
            }
            case UserTypes.CADMIN: {
                JsonObject object = new JsonObject();
                JsonObject obj = new JsonObject();
                obj.addProperty("type", 1);
                obj.addProperty("value", userLogin.getUserId());
                obj.addProperty("col", 1);
                object.add("userId", obj);
                ListResult<ComAdmin> result = cadminManager.getPaginatedEntityListByFilter(0, "", object);
                if (result.getList() != null && result.getList().size() == 1) {
                    ComAdmin c = result.getList().get(0);

                    Company company = companyManager.getEntityByKey(c.getCompanyId());
                    if (company != null) {
                        reply.setPages(company.getPages());
                        reply.setEmSources(company.getEmSources());
                    }

                    reply.setBranchId(c.getBranchId());
                    reply.setCompanyId(c.getCompanyId());
                    reply.setEntitlements(c.getEntitlements());
                    reply.setCadminId(c.getId());
                    reply.setFirstName(c.getFirstName());
                    reply.setLastName(c.getLastName());
                    reply.setCadminStatus(c.getStatus());
                    reply.setCadminStatusMsg(c.getStatusMessage());
                    reply.setCompanyName(c.getCompany());
                    reply.setAllowedEmissionSources(company.getAllowedEmissionSources());

                    JsonObject filter = new JsonObject();
                    JsonObject comId = new JsonObject();
                    comId.addProperty("type", 1);
                    comId.addProperty("value", c.getCompanyId());
                    comId.addProperty("col", 1);
                    filter.add("companyId", comId);
                    ListResult<Project> projects = proejctManager.getPaginatedEntityListByFilter(-1, "", filter);
                    if (projects.getList() != null && projects.getList().size() > 0) {
                        for (Project p : projects.getList()) {

                            if (p.getStatus() <= 7) {
                                reply.setProjectStatus(p.getStatus());
                                reply.setExistActiveProject(1);

                                boolean isExp = false;
                                if (p.getExtendedDate() != null) {
                                    isExp = p.getExtendedDate().before(new Date());
                                }else {
                                    isExp = p.getEndDate().before(new Date());
                                }
                                reply.setDataEntryExpired(isExp ? 1: 0);
                                reply.setDisableFormulas(p.getDisableFormula());

                                break;
                            }

                        }
                        if (reply.getExistActiveProject() != 1) {
                            reply.setExistActiveProject(0);
                        }
                    }

                } else {
                    // Todo: errro
                    reply.setNarration("User not found");
                    reply.setLoginStatus(LoginResponseStatus.FAIL.getCode());
                }
                break;
            }
            case UserTypes.DATA_ENTRY_USER: {
                JsonObject object = new JsonObject();
                JsonObject obj = new JsonObject();
                obj.addProperty("type", 1);
                obj.addProperty("value", userLogin.getUserId());
                obj.addProperty("col", 1);
                object.add("userId", obj);
                ListResult<DataEntryUser> result = dataEntryUserManager.getPaginatedEntityListByFilter(0, "", object);
                if (result.getList() != null && result.getList().size() == 1) {
                    DataEntryUser c = result.getList().get(0);

                    Company company = companyManager.getEntityByKey(c.getCompanyId());
                    if (company != null) {
                        reply.setPages(company.getPages());
                        reply.setEmSources(company.getEmSources());
                    }

                    reply.setBranchId(c.getBranchId());
                    reply.setCompanyId(c.getCompanyId());
                    reply.setEntitlements(c.getEntitlements());
                    reply.setDataEntryUserId(c.getId());
                    reply.setFirstName(c.getFirstName());
                    reply.setLastName(c.getLastName());
                    reply.setCadminStatus(c.getStatus());
                    reply.setCadminStatusMsg(c.getStatusMessage());
                    reply.setCompanyName(c.getCompany());
                    reply.setAllowedEmissionSources(company.getAllowedEmissionSources());

                    JsonObject filter = new JsonObject();
                    JsonObject comId = new JsonObject();
                    comId.addProperty("type", 1);
                    comId.addProperty("value", c.getCompanyId());
                    comId.addProperty("col", 1);
                    filter.add("companyId", comId);
                    ListResult<Project> projects = proejctManager.getPaginatedEntityListByFilter(-1, "", filter);
                    if (projects.getList() != null && projects.getList().size() > 0) {
                        for (Project p : projects.getList()) {

                            if (p.getStatus() <= 7) {
                                reply.setProjectStatus(p.getStatus());
                                reply.setExistActiveProject(1);

                                boolean isExp = false;
                                if (p.getExtendedDate() != null) {
                                    isExp = p.getExtendedDate().before(new Date());
                                } else {
                                    isExp = p.getEndDate().before(new Date());
                                }
                                reply.setDataEntryExpired(isExp ? 1 : 0);
                                reply.setDisableFormulas(p.getDisableFormula());

                                break;
                            }

                        }
                        if (reply.getExistActiveProject() != 1) {
                            reply.setExistActiveProject(0);
                        }
                    }

                } else {
                    // Todo: errro
                    reply.setNarration("User not found");
                    reply.setLoginStatus(LoginResponseStatus.FAIL.getCode());
                }

                break;
            }
            case UserTypes.ADMIN: {
                JsonObject object = new JsonObject();
                JsonObject obj = new JsonObject();
                obj.addProperty("type", 1);
                obj.addProperty("value", userLogin.getUserId());
                obj.addProperty("col", 1);
                object.add("userId", obj);

//                WebNotificationDTO not = new WebNotificationDTO();
//                not.setMessage("jsljfsflsfjsf");
//                not.setUserId(userLogin.getUserId());
//                not.setRead(1);
//                notificationSrcEvent.fire(not);

                reply.setIsMasterAdmin(userLogin.getIsMasterAdmin());



                ListResult<Admin> result = adminManager.getPaginatedEntityListByFilter(0, "", object);
                if (result.getList() != null && result.getList().size() == 1) {
                    Admin c = result.getList().get(0);
                    reply.setAdminId(c.getId());
                    reply.setEntitlements(c.getEntitlements());
                    reply.setFirstName(c.getFirstName());
                    reply.setLastName(c.getLastName());

                } else {
                    // Todo: errro
                    reply.setNarration("User not found");
                    reply.setLoginStatus(LoginResponseStatus.FAIL.getCode());
                }
                break;
            }
        }

        updateUserLoginHistory(reply,userLogin,authData);


//        logger.info("[UserLoginController.java:179] Employee Name >> " + loggedUser.getFirstName() + " " + loggedUser.getLastName() + " , USerID");

        //Remove existing user sessions
        ListResult<String> removedSessions = removeSessionsForUser(userLogin.getUserId());

        String u01SessionId = getNewSessionId();

        Date loggedDate = new Date();

        //Add session to U01
        UserSession u01 = userSessionManager.getDummyEntity();
        u01.setUserSessionId(u01SessionId);
        u01.setChannelId(authData.getChannelId());
        u01.setIpAddress(authData.getIpAddress());
        u01.setVersion(authData.getClientVersion());
        u01.setUserLoginTime(loggedDate);
        u01.setUserLoginTime(loggedDate);
        u01.setLoginId(userLogin.getUserId());
        userSessionManager.addEntity(u01);

        //Update login info
        //  userLogin.setLastLoginTime(loggedDate);
        userLogin.setLastLoggedInDate(loggedDate);
        userLogin.setFailedLoginAttempts(0);
        userLoginManager.updateEntity(userLogin);

        reply.setLoginStatus(LoginResponseStatus.SUCCESS.getCode());
        reply.setNarration("Login Success");
        // todo: sent client , admin, client admin details
//        reply.setUser(loggedUser);
        reply.setSessionId(u01SessionId);
        reply.setRemovedSessions(removedSessions.getList());
        reply.setUserName(authData.getUsername());

        //adding the user session, for pulse validations
        this.addOrUpdateUserLastKnownRequestTime(userLogin.getUserId(), System.currentTimeMillis());
        return reply;



}

    public void addOrUpdateUserLastKnownRequestTime(int userLoginId, Long lastKnownReceivedRequestTime) {

        logger.info("[UserLoginController.java:282] Adding user last received time to map, User loginID: " + userLoginId + " time: " + lastKnownReceivedRequestTime);
        if (userLastReceivedRequestTimeHashMap.put(userLoginId, lastKnownReceivedRequestTime) == null) {
            logger.info("[UserLoginController.java:284] User's last known request time added");
        } else {
            logger.info("[UserLoginController.java:286] User's last known request time updated");
        }
    }

    public ListResult<String> doLogoutUser(int userLoginId) {
        return removeSessionsForUser(userLoginId);
    }

    private String getNewSessionId() {
        StringBuilder ses = new StringBuilder();

        ses.append("BO"); //2

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        ses.append(dateFormat.format(date)); //16

        ses.append(Long.toString(System.currentTimeMillis(), 10)); //10

        SecureRandom random = new SecureRandom();
        BigInteger randInt = new BigInteger(130, random);
        ses.append(randInt.toString(32).toUpperCase()); //32

        return ses.toString(); //2 + 16 + 10 + 32
    }

    private ListResult<String> removeSessionsForUser(int userLoginId) {
        ListResult<String> removedSessions = new ListResult<>();
        logger.info("[UserLoginController.java:255] Remove existing user sessions, userLoginId: " + userLoginId);
        JsonObject object = new JsonObject();
        JsonObject obj = new JsonObject();
        obj.addProperty("type", 1);
        obj.addProperty("value", userLoginId);
        obj.addProperty("col", 1);
        object.add("loginId", obj);

        ListResult<UserSession> preSessions = userSessionManager.getEntityListByFilter(object);

        if (preSessions.getList() != null && !preSessions.getList().isEmpty()) {
            logger.info("[UserLoginController.java:259] previous active session count >> " + preSessions.getList().size());

            for (UserSession preSession : preSessions.getList()) {
                //Mark to remove those entries from cache
                removedSessions.getList().add(preSession.getUserSessionId());

                //Remove those entries from database
                userSessionManager.removeUserSession(preSession);
            }
        } else {
            logger.info("[UserLoginController.java:269] Not found any previous active sessions");
        }

        return removedSessions;
    }

    public  UserLogin findById(int userId) throws GHGException {
        UserLogin entry;
        try {
           entry = userLoginManager.getUserLoginProfileByKey(userId);
        }catch (Exception e) {
            logger.error(e);
            throw new GHGException("");
        }
        return entry;
    }

    private void updateUserLoginHistory(AuthReply authReply, UserLogin userLogin, AuthenticationData authenticationData){
        UserLoginHistory userLoginHistory = userLoginHistoryManager.getEmptyUserLoginHistoryBean();
        userLoginHistory.setConnectIp(authenticationData.getIpAddress());
        userLoginHistory.setLoginEmail(userLogin.getLoginEmail());
        userLoginHistory.setLoginName(userLogin.getLoginName());
        userLoginHistory.setLoginStatus(authReply.getLoginStatus());
        userLoginHistory.setLoginTime(new Date());
        userLoginHistory.setRejectReason(authReply.getNarration());
        userLoginHistory.setSessionId(authReply.getSessionId());
        userLoginHistory.setUserId(String.valueOf(userLogin.getUserId()));
        userLoginHistory.setVersion(authenticationData.getClientVersion());
        userLoginHistoryManager.addUserLoginHistory(userLoginHistory);
    }


    private UserLogin populate(UserLogin login, LoginUserDTO dto) {

        if (dto.getUserName() != null ) {
            login.setLoginName(dto.getUserName());
        }
        if (dto.getIsFirstTime() == 0) {

        }else if (dto.getIsFirstTime() == 1) {
            login.setFirstTime(0);
        }else if(dto.getIsFirstTime() ==2) {
            login.setFirstTime(1);
            login.setFailedLoginAttempts(0);
            login.setUserType(dto.getUserType());
        }
//        login.setLoginName(dto.getUserName() == null ? login.getLoginName() : dto.getUserName());
//        login.setFirstTime(dto.getIsFirstTime());

        if(dto.getPassword() != null) {
            login.setLoginPassword(dto.getPassword());
        }
        if (dto.getLoginStatus() != 0) {
            login.setStatus(dto.getLoginStatus());
        }

//        login.setStatus(dto.getLoginStatus());
        if (dto.getUserId() > 0) {
            login.setUserId(dto.getUserId());
        }
        return login;
    }

}
