package com.climatesi.ghg.bo_controller.controllers.implGeneral.emission;

import com.climatesi.ghg.api.EmissionCalcFacotory;
import com.climatesi.ghg.api.constants.EmissionFactors_Keys;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.BioMassPopulator;
import com.climatesi.ghg.bo_controller.populators.MunicipalWaterPopulator;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.BioMassEntryDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.emission.core.MunicipalWaterEntryDTO;
import com.climatesi.ghg.calc.Calculator;
import com.climatesi.ghg.emission_source.api.EmsissionSourceFactory;
import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.enums.Units;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.BioMassEntryManagerFacade;
import com.climatesi.ghg.implGeneral.data.MunicipalWaterCalcData;
import com.climatesi.ghg.implGeneral.data.ResultDTO;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.BioMassTypeEnum;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.HashMap;

@Stateless
public class BioMassEntryController  extends AbstractController {

    private static final Logger logger = Logger.getLogger(BioMassEntryController.class);


    private BioMassEntryManagerFacade bioMassEntryManagerFacade;
    private EmissionFactorsManager emissionFactorsManager;

    @PostConstruct
    @Override
    public void initialize() {

        try {
            bioMassEntryManagerFacade = (BioMassEntryManagerFacade) EmsissionSourceFactory.getInstance().getBioMassEntryManager(em);
            emissionFactorsManager = EmissionCalcFacotory.getInstance().getEmissionFactorsManager(em);
        } catch (GHGException e) {
            logger.error("Error in intializing MunicipalWaterController", e);
        }
    }


    public ListResult<BioMassEntry> getFiteredReslt(int pageNum, Object sortingCriteria, Object filterCriteria) {
        return bioMassEntryManagerFacade.getPaginatedEntityListByFilter(pageNum, sortingCriteria, filterCriteria);
    }

    public BioMassEntry edit(BioMassEntryDTO dto) throws GHGException {
        BioMassEntry entry;

        String status = null;
        try {
//        update
            if (dto.getEntryId() > 0) {
                entry = findById(dto.getEntryId());
                BioMassPopulator.getInstance().populate(entry, dto);
                entry.setLastUpdatedDate(new Date());
                if (entry.getIsDeleted() !=1) {
                    entry.setEmissionDetails(emission(entry));

                }

                //          Todo: add user id
                status = (String) bioMassEntryManagerFacade.updateEntity(entry);
                if (entry == null) {
                    throw new GHGException("WasteTransportEntry is not found");
                }
//         add new
            } else {
                entry = new BioMassEntryBean();
                BioMassPopulator.getInstance().populate(entry, dto);
                entry.setEmissionDetails(emission(entry));
                entry.setAddedDate(new Date());
//          Todo: add user id
                status = (String) bioMassEntryManagerFacade.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }

    public BioMassEntry findById(Integer id) {
        return bioMassEntryManagerFacade.getEntityByKey(id);
    }

    public HashMap<String, String> emission(BioMassEntry entry) {

        float quantity = entry.getQuantity() * 0.001f;
        float ncv = 0.0156f;
        float efCo2 = 112f;
        float efCh4 =  0.84f;
        float efN20 = 1.06f;

        if (entry.getBiomassType() == BioMassTypeEnum.WoodChips.getType() ||
            entry.getBiomassType() == BioMassTypeEnum.SawDust.getType()
        ) {
            ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_NCV).getValue();
            efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_CO2).getValue();
            efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_CH4).getValue();
            efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.BIOMASS_EF_N20).getValue();
        }else {
            ncv = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.OTHER_BIOMASS_NCV).getValue();
            efCo2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.OTHER_BIOMASS_EF_CO2).getValue();
            efCh4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.OTHER_BIOMASS_EF_CH4).getValue();
            efN20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.OTHER_BIOMASS_EF_N20).getValue();
        }


        float gwp_co2 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CO2_STATIONARY_DIESEL).getValue();
        float gwp_ch4 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_CH4_STATIONARY_DIESEL).getValue();
        float gwp_n20 = emissionFactorsManager.getEntityByKey(EmissionFactors_Keys.GWP_N20_STATIONARY_DIESEL).getValue();



        ResultDTO e1 = Calculator.emissionBioMass(
                quantity,
                ncv,
                efCo2,
                efCh4,
                efN20,
                gwp_co2,
                gwp_ch4,
                gwp_n20
        );
        HashMap<String, String> results = new HashMap<>();
        results.put("tco2",String.format("%.5f", e1.tco2e) );
        results.put("co2",String.format("%.5f", e1.co2));
        results.put("n20",String.format("%.5f", e1.n2o) );
        results.put("ch4",String.format("%.5f", e1.ch4) );
        results.put("quantity",String.format("%.5f", quantity) );
        results.put("ncv",String.format("%.5f", ncv) );
        results.put("efCO2",String.format("%.5f", efCo2) );
        results.put("efCH4",String.format("%.5f",efCh4) );
        results.put("efN2O",String.format("%.5f", efN20) );
        results.put("gwp_co2",String.format("%.5f", gwp_co2) );
        results.put("gwp_ch4",String.format("%.5f", gwp_ch4) );
        results.put("gwp_n20",String.format("%.5f", gwp_n20) );
        results.put("te",String.format("%.5f",e1.totalEnergy) );

        results.put("direct", Boolean.toString(false));

        return results;
    }

}
