package com.climatesi.ghg.bo_controller.controllers.implGeneral.insitution;

import com.climatesi.ghg_mitication.api.ProjectFactory;
import com.climatesi.ghg_mitication.api.ProjectStatus;
import com.climatesi.ghg_mitication.api.beans.Project;
import com.climatesi.ghg_mitication.api.facade.ProejctManager;
import com.climatesi.ghg.api.InstitutionFactory;
import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.facades.BranchManager;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.bo_controller.controllers.api.AbstractController;
import com.climatesi.ghg.bo_controller.populators.BranchPopulator;
import com.climatesi.ghg.bo_controller.populators.CompanyPopulator;
import com.climatesi.ghg.bo_messaging.api.constants.EntityActions;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.BranchDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.request.ManageEmissionCategoryReqMsg;
import com.climatesi.ghg.bo_messaging.implGeneral.ws_message.WebNotificationDTO;
import com.climatesi.ghg.business_users.api.BusinessUserFactory;
import com.climatesi.ghg.business_users.api.beans.Client;
import com.climatesi.ghg.business_users.api.beans.ComAdmin;
import com.climatesi.ghg.business_users.api.facades.CadminManager;
import com.climatesi.ghg.business_users.api.facades.ClientManager;
import com.climatesi.ghg.business_users.api.facades.DataEntryUserManager;
import com.climatesi.ghg.emission_source.api.enums.DataEntryStatus;
import com.climatesi.ghg.emission_source.api.facades.EmissionSrcDataEntryStatusManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.ProjectDataEntryStatusIdBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.EmissionSrcDataEntryStatusBean;
import com.climatesi.ghg.implGeneral.BranchBean;
import com.climatesi.ghg.implGeneral.CompanyBean;
import com.climatesi.ghg.implGeneral.EmissionCategoryBean;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class InsstitutionController extends AbstractController {

    private static Logger logger = Logger.getLogger(InsstitutionController.class);
    private BranchManager branchManager;
    private CompanyManager companyManager;
    private ClientManager clientManager;
    private CadminManager cadminManager;
    private ProejctManager proejctManager;
    private DataEntryUserManager dataEntryUserManager;
    private EmissionSrcDataEntryStatusManager emissionSrcDataEntryStatusManager;


    @Inject
    private Event<WebNotificationDTO> notificationSrcEvent;

    @PostConstruct
    @Override
    public void initialize() {
        try {
            proejctManager = ProjectFactory.getInstance().getProejctManager(em);
            cadminManager = BusinessUserFactory.getInstance().getCadminManager(em);
            clientManager = BusinessUserFactory.getInstance().getClientManager(em);
            branchManager = InstitutionFactory.getInstance().getBranchManager(em);
            companyManager = InstitutionFactory.getInstance().getCompanyManager(em);
            dataEntryUserManager = BusinessUserFactory.getInstance().getDataEntryUserManager(em);


        } catch (Exception e) {
            logger.error("Error in intializing Institution Controller", e);

        }
    }


    public ListResult<Branch> getBranchFiltered(int pageNum, Object sorting, Object filter) {
        return branchManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public ListResult<Company> getCompanyFiltered(int pageNum, Object sorting, Object filter) {

        return companyManager.getPaginatedEntityListByFilter(pageNum, sorting, filter);
    }

    public Branch findBranchById(Integer id) {
        return branchManager.getEntityByKey(id);
    }

    public Company findCompanyById(Integer id) {
        return companyManager.getEntityByKey(id);
    }

    public Branch editBranch(BranchDTO dto) throws GHGException {
        Branch entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findBranchById(dto.getId());
                if (entry == null) {
                    throw new GHGException("Branch is not found");
                }

                BranchPopulator.getInstance().populate(entry, dto);
//                entry.se(new Date());
                if (dto.isDeleted() == 1) {
                    deleteBranch(dto.getId());
                    return entry;
                }
                //          Todo: add user id
                status = (String) branchManager.updateEntity(entry);
//                if ((status == null || !status.equals("-1")) && dto.isDeleted() == 1 && entry.isDeleted() == 0) {
//
//
//                    // delete clients
//                    JsonObject filObjClients = new JsonObject();
//                    JsonObject fillClients = new JsonObject();
//                    fillClients.addProperty("type", 1);
//                    fillClients.addProperty("value", dto.getCompanyId());
//                    fillClients.addProperty("col", 1);
//                    filObjClients.add("companyId", fillClients);
//
//                    List<Client> delClients = clientManager.getPaginatedEntityListByFilter(-1, "", filObjClients).getList();
//                    for (Client c : delClients) {
//                        c.setDeleted(1);
//                        clientManager.updateEntity(c);
//                    }
//
//                }

//         add new
            } else {
                entry = new BranchBean();
                BranchPopulator.getInstance().populate(entry, dto);


//                entry.setEntryAddedDate(new Date());
//          Todo: add user id
                status = (String) branchManager.addEntity(entry);

                if (entry.getId() != 0 ) {



                }
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }
    private void initBranchDataEntryStatus(Branch entry) {
        Company company = companyManager.getEntityByKey(entry.getCompanyId());
        String fy = company.isFinacialYear() ? company.getFyCurrentStart() + "/" + (company.getFyCurrentEnd()%100) : "" + company.getFyCurrentEnd();

        JsonObject comFilter = new JsonObject();
        JsonObject filter = new JsonObject();
        filter.addProperty("type", 1);
        filter.addProperty("col", 1);
        filter.addProperty("value", entry.getCompanyId());
        comFilter.add("companyId", filter);
        List<Project> projects = proejctManager.getPaginatedEntityListByFilter(-1, "", comFilter).getList();
        if (projects != null ) {
            for (Project p : projects) {
                if (p.getStatus() <= ProjectStatus.DATA_ENTRY) {
                    for (int src = 1; src < 14; src++) {
                        EmissionSrcDataEntryStatusBean bean = new EmissionSrcDataEntryStatusBean();
                        ProjectDataEntryStatusIdBean id = new ProjectDataEntryStatusIdBean();
                        id.setEmissionSrc(src);
                        id.setFY(fy);
                        id.setCompanyId(entry.getCompanyId());
                        id.setBranchId(entry.getId());
                        bean.setStatus(DataEntryStatus.PENDING.status);
                        bean.setId(id);
                        emissionSrcDataEntryStatusManager.addEntity(bean);
                    }
                    break;
                }
            }
        }
    }

    public Company updateMenus(Company company)  throws GHGException{
        Company entry;

        String status = null;
        try {
//        update


                status = (String) companyManager.updateEntity(company);

        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return company;
    }

    public Company editCompany(CompanyDTO dto) throws GHGException {
        Company entry;

        String status = null;
        try {
//        update
            if (dto.getId() > 0) {
                entry = findCompanyById(dto.getId());
                if (entry == null) {
                    throw new GHGException("Company is not found");
                }
                if(dto.isDeleted() == 1) {
                    deleteCompany(dto.getId());
                    return entry;
                }
                CompanyPopulator.getInstance().populate(entry, dto);
//                entry.setLastUpdatedDate(new Date());
                //          Todo: add user id

                status = (String) companyManager.updateEntity(entry);
//                if ((status == null || !status.equals("-1")) && dto.isDeleted() == 1 && entry.isDeleted() == 0) {
//
////                            softDeleteCompany(dto.getId());
//
//                }


//         add new
            } else {
                entry = new CompanyBean();
                CompanyPopulator.getInstance().populate(entry, dto);
//                entry.set
//          Todo: add user id
                status = (String) companyManager.addEntity(entry);
            }
//         Todo:
        } catch (Exception e) {
            logger.error("Error in updating Entry", e);
            throw new GHGException(e.getMessage());
        }

        return entry;
    }


    public void softDeleteCompany(int comId) {
        // delete projects

                    JsonObject filObjProject = new JsonObject();
                    JsonObject filProj = new JsonObject();
                    filProj.addProperty("type", 1);
                    filProj.addProperty("value", comId);
                    filProj.addProperty("col", 1);
                    filObjProject.add("companyId", filProj);
                    List<Project> delProjects = proejctManager.getPaginatedEntityListByFilter(-1, "", filObjProject).getList();
                    for (Project p : delProjects) {
                        p.setDeleted(1);
                        proejctManager.updateEntity(p);
                    }

//
//                    // delete branches
                    JsonObject filObjBranch = new JsonObject();
                    JsonObject filBranch = new JsonObject();
                    filBranch.addProperty("type", 1);
                    filBranch.addProperty("value", comId);
                    filBranch.addProperty("col", 1);
                    filObjBranch.add("companyId", filBranch);
                    List<Branch> delBranches = branchManager.getPaginatedEntityListByFilter(-1, "", filObjBranch).getList();
                    for (Branch b : delBranches) {
                        b.setDeleted(1);
                        branchManager.updateEntity(b);
                    }
//                    // delete clients
                    JsonObject filObjClients = new JsonObject();
                    JsonObject fillClients = new JsonObject();
                    fillClients.addProperty("type", 1);
                    fillClients.addProperty("value", comId);
                    fillClients.addProperty("col", 1);
                    filObjClients.add("companyId", fillClients);

                    List<Client> delClients = clientManager.getPaginatedEntityListByFilter(-1, "", filObjClients).getList();
                    for (Client c : delClients) {
                        c.setDeleted(1);
                        clientManager.updateEntity(c);
                    }

//
//                    // delete cadmins
                    JsonObject filObjCadmins = new JsonObject();
                    JsonObject filCadmins = new JsonObject();
                    filCadmins.addProperty("type", 1);
                    filCadmins.addProperty("value", comId);
                    filCadmins.addProperty("col", 1);
                    filObjCadmins.add("companyId", filBranch);

                    List<ComAdmin> delCadmins = cadminManager.getPaginatedEntityListByFilter(-1, "", filObjCadmins).getList();
                    for (ComAdmin c : delCadmins) {
                        c.setDeleted(1);
                        cadminManager.updateEntity(c);
                    }
    }

    public int deleteCompany(int comId)  {
        logger.info("deleteCompany()");
        try {
//
//            JsonObject clientFilter = new JsonObject();
//            JsonObject clientFilterObj = new JsonObject();
//            clientFilterObj.addProperty("type",1);
//            clientFilterObj.addProperty("value", comId);
//            clientFilterObj.addProperty("col", 1);
//            clientFilter.add("companyId", clientFilterObj);
//
//            List<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", clientFilter).getList();
//            logger.info("Delete No of Clients: " + clients.size());
//            for(Client c : clients) {
//                clientManager.deleteClient(c.getId());
//            }
//
//            JsonObject cadminFilter = new JsonObject();
//            JsonObject cadminFilterObj = new JsonObject();
//            cadminFilterObj.addProperty("type",1 );
//            cadminFilterObj.addProperty("value", comId);
//            cadminFilterObj.addProperty("col", 1);
//            cadminFilter.add("companyId", cadminFilterObj);
//
//            List<ComAdmin> cadmins = cadminManager.getPaginatedEntityListByFilter(-1, "", cadminFilter).getList();
//            logger.info("Delete No of Cadmins: " + cadmins.size());
//            for(ComAdmin c : cadmins) {
//                cadminManager.deleteCadmin(c.getId());
//            }
//
//            JsonObject deuFilter = new JsonObject();
//            JsonObject deuFilterObj = new JsonObject();
//            deuFilterObj.addProperty("value", 1);
//            deuFilterObj.addProperty("type",1);
//            deuFilterObj.addProperty("col",1);
//            deuFilter.add("companyId", deuFilterObj);
//
//            List<DataEntryUser> dataEntryUsers = dataEntryUserManager.getPaginatedEntityListByFilter(-1, "", deuFilter).getList();
//            logger.info("Delete No of Dataentry users: " + dataEntryUsers.size());
//            for(DataEntryUser d : dataEntryUsers) {
//                dataEntryUserManager.deleteDataEntryUser(d.getId());
//            }

            return companyManager.deleteCompany(comId);
        }catch (Exception e) {
            logger.error(e);
        }

        return 0;
    }

    public int deleteBranch(int branchId) {
        try {
            JsonObject clientFilter = new JsonObject();
            JsonObject clientFilterObj = new JsonObject();
            clientFilterObj.addProperty("type",1);
            clientFilterObj.addProperty("value", branchId);
            clientFilterObj.addProperty("col", 1);
            clientFilter.add("branchId", clientFilterObj);

            List<Client> clients = clientManager.getPaginatedEntityListByFilter(-1, "", clientFilter).getList();
            for(Client client: clients) {
                clientManager.deleteClient(client.getId());
            }

            return branchManager.deleteBranch(branchId);
        }catch(Exception e) {
            logger.error(e);
        }

        return 0;
    }


    public EmissionCategoryBean updateEmissionCategory(ManageEmissionCategoryReqMsg msg) {
       Company company = findCompanyById(msg.getDto().getComId());

       if (company == null) return null;

        if (msg.getAction() == EntityActions.Add.getAction() ){
            EmissionCategoryBean category = new EmissionCategoryBean();
            List<EmissionCategoryBean> categories = company.getEmissionCategories();
            int id = 1;

            if (categories ==null || categories.size() == 0) {
                categories = new ArrayList<>();
            }else {
                 id = Collections.max(categories.stream().map(cat -> cat.getId()).collect(Collectors.toList())) + 1;
            }
            category.setId(id);
            category.setName(msg.getDto().getName());
            category.setComId(msg.getDto().getComId());
            categories.add(category);

            company.setEmissionCategories(categories);
            companyManager.updateEntity(company);
            return category;

        }
        if (msg.getAction() == EntityActions.Update.getAction()) {
            EmissionCategoryBean category = null;
            List<EmissionCategoryBean> categories = company.getEmissionCategories();
            if (categories !=null || categories.size() > 0) {
                EmissionCategoryBean categoryBean = categories.stream().filter(cat -> cat.getId() == msg.getDto().getId()).findFirst().orElse(null);
                if (categoryBean != null) {
                    categoryBean.setName(msg.getDto().getName());
                    company.setEmissionCategories(categories);
                    companyManager.updateEntity(company);

                    return categoryBean;
                }


            }
        }
        if (msg.getAction() == EntityActions.Delete.getAction()) {
            EmissionCategoryBean category = null;
            List<EmissionCategoryBean> categories = company.getEmissionCategories();
            if (categories != null) {
                EmissionCategoryBean categoryBean = categories.stream().filter(cat -> cat.getId() == msg.getDto().getId()).findFirst().orElse(null);
                categories.remove(categoryBean);
                company.setEmissionCategories(categories);
                companyManager.updateEntity(company);
                return categoryBean;
            }
        }

        return  null;
    }


    public Company updateSuggestionsAndExcludedReasons(Company company) throws Exception {
        try {
            companyManager.updateEntity(company);
        }catch (Exception e) {
            logger.error("Failed to update suggestions ", e);
            throw  new Exception("Failed to update ");
        }
        return company;
    }
}
