package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.business_users.core.EmployeeDTO;
import com.climatesi.ghg.business_users.api.beans.Employee;
import org.jboss.logging.Logger;

public class EmployeePopulator {

    private final static Logger logger = Logger.getLogger(EmployeePopulator.class);
    private static EmployeePopulator instance;

    private EmployeePopulator() {
    }

    public static EmployeePopulator getInstance() {
        if (instance == null) {
            instance = new EmployeePopulator();
        }
        return instance;
    }

    public EmployeeDTO populateDTO(Employee employee) {
        EmployeeDTO dto = new EmployeeDTO();

        try {
            dto.setId(employee.getId());
            dto.setEmpNo(employee.getEmpNo());
            dto.setBranchId(employee.getBranchId());
            dto.setComId(employee.getComId());
            dto.setName(employee.getName());

        } catch (NullPointerException e) {
            logger.error("Error in populating", e);
        }
        return dto;
    }

    public Employee populate(Employee employee, EmployeeDTO dto) {
        try {
            if (dto.getId() > 0) {
                employee.setId(dto.getId());
                if (employee.isDeleted() == 1) {
                    employee.setDeleted(dto.isDeleted());
                    return employee;
                }
            }
            employee.setName(dto.getName() == null ? employee.getName() : dto.getName());
            employee.setEmpNo(dto.getEmpNo() == null ? employee.getEmpNo() : dto.getEmpNo());
            employee.setBranchId(dto.getBranchId() == 0 ? employee.getBranchId() : dto.getBranchId());
            employee.setComId(dto.getComId() == 0 ? employee.getComId() : dto.getComId());
        } catch (NullPointerException e) {
            logger.error("ERroi in populating", e);
        }
        return employee;
    }
}
