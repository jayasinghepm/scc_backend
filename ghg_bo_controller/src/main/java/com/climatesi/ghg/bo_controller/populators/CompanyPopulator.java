package com.climatesi.ghg.bo_controller.populators;

import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.bo_messaging.implGeneral.beans.message.institution.core.CompanyDTO;
import org.jboss.logging.Logger;

public class CompanyPopulator {

    private final static Logger logger = Logger.getLogger(CompanyPopulator.class);
    private static CompanyPopulator instance;

    public static CompanyPopulator getInstance() {
        if (instance == null) {
            instance = new CompanyPopulator();
        }
        return instance;
    }

    public CompanyDTO popoulateDTO(Company company, int isLogoRequired) {
        CompanyDTO dto = new CompanyDTO();
        try {
            dto.setId(company.getId());
            dto.setName(company.getName());
            dto.setCode(company.getCode());
            dto.setSector(company.getSector());
            dto.setEntitlements(company.getEntitlements());
            dto.setNoOfBranches(company.getNoOfBranches());
            dto.setNoOfEmployees(company.getNoOfEmployees());
            dto.setAnnumRevFY(company.getAnnumRevFY());
            dto.setBaseYear(company.getBaseYear());
            dto.setYearEmission(company.getYearEmission());
            dto.setAddr1(company.getAddr1());
            dto.setAddr2(company.getAddr2());
            dto.setDistrict(company.getDistrict());
            if (isLogoRequired == 1) {
                dto.setLogo(company.getLogo());
            }


            dto.setFyCurrent(company.getFyCurrent());
            dto.setFyMonthStart(company.getFyMonthStart());
            dto.setFyMonthEnd(company.getFyMonthEnd());
            dto.setFyCurrentEnd(company.getFyCurrentEnd());
            dto.setFyCurrentStart(company.getFyCurrentStart());
            dto.setTargetGHG(company.getTargetGHG());
            dto.setPages(company.getPages());
            dto.setEmSources(company.getEmSources());
            if (company.isFinacialYear()) {
                dto.setFinancialYear(2);
            } else {
                dto.setFinancialYear(1);
            }
            dto.setAllowedEmissionSources(company.getAllowedEmissionSources());
            dto.setEmissionCategories(company.getEmissionCategories());


        } catch (NullPointerException e) {
            logger.error("Error in populating", e);
        }
        return dto;
    }

    public Company populate(Company company, CompanyDTO dto) {
        try {
            if (dto.getId() > 0) {
                company.setId(dto.getId());
//                if (dto.isDeleted() == 1) {
//                    company.setDeleted(dto.isDeleted());
//                    return company;
//                }

            }
            company.setCode(dto.getCode() == null ? company.getCode() : dto.getCode());
            company.setName(dto.getName() == null ? company.getName() : dto.getName());
            company.setSector(dto.getSector() == null ? company.getSector() : dto.getSector());
            company.setEntitlements(dto.getEntitlements() == null ? company.getEntitlements() : dto.getEntitlements());
            company.setAnnumRevFY(dto.getAnnumRevFY() == 0 ? company.getAnnumRevFY() : dto.getAnnumRevFY());
            company.setBaseYear(dto.getBaseYear() == null ? company.getBaseYear() : dto.getBaseYear());
//            company.setAnnumRevFY(dto.getAnnumRevFY());
            company.setYearEmission(dto.getYearEmission() == null ? company.getYearEmission() : dto.getYearEmission());
            company.setNoOfBranches(dto.getNoOfBranches() == 0 ? company.getNoOfBranches() : dto.getNoOfBranches());
            company.setNoOfEmployees(dto.getNoOfEmployees() == 0 ? company.getNoOfEmployees() : dto.getNoOfEmployees());
            company.setAddr1(dto.getAddr1() == null ? company.getAddr1() : dto.getAddr1());
            company.setAddr2(dto.getAddr2() == null ? company.getAddr2() : dto.getAddr2());
            company.setDistrict(dto.getDistrict() == null ? company.getDistrict() : dto.getDistrict());
            company.setLogo(dto.getLogo() == null ? company.getLogo() : dto.getLogo());
            company.setFyCurrent(dto.getFyCurrent() == null ? company.getFyCurrent() : dto.getFyCurrent());
            company.setFyMonthEnd(dto.getFyMonthEnd());
            company.setFyMonthStart(dto.getFyMonthStart());
            company.setFyCurrentEnd(dto.getFyCurrentEnd() == 0 ? company.getFyCurrentEnd() : dto.getFyCurrentEnd());
            company.setFyCurrentStart(dto.getFyCurrentStart() == 0 ? company.getFyCurrentStart() : dto.getFyCurrentStart());
            if(dto.getPages() != null) {
                company.setPages(dto.getPages());
            }
            if(dto.getEmSources() != null) {
                company.setEmSources(dto.getEmSources());
            }
            if (dto.isFinancialYear() == 0) {
            } else if (dto.isFinancialYear() == 1) {
                company.setFinacialYear(false);
            } else if (dto.isFinancialYear() == 2) {
                company.setFinacialYear(true);
            }
            company.setTargetGHG(dto.getTargetGHG() == 0 ? company.getTargetGHG() : dto.getTargetGHG());
        } catch (NullPointerException e) {
            logger.error("Error in populating", e);
        }
        return company;
    }
}
