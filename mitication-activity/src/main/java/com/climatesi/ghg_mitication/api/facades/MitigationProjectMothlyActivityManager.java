package com.climatesi.ghg_mitication.api.facades;

import com.climatesi.ghg.utility.api.facade.DataEntryManager;
import com.climatesi.ghg_mitication.api.beans.MitigationProjectMonthlyActivity;

import java.util.List;

public interface MitigationProjectMothlyActivityManager extends DataEntryManager<MitigationProjectMonthlyActivity, Long> {

    List<MitigationProjectMonthlyActivity> findByProjectId(int projectId);

    void delete(long id);

    List<MitigationProjectMonthlyActivity> getDataByCompany(int comId);

    List<MitigationProjectMonthlyActivity> findByProjectIdAndSortByMonth(int projectId);

    List<MitigationProjectMonthlyActivity> getDataByBranch(int branchId);
}
