package com.climatesi.ghg_mitication.api;

import com.climatesi.ghg_mitication.impl.facades.MitigationProjectFacade;
import com.climatesi.ghg_mitication.impl.facades.MitigationProjectMonthlyActivityFacade;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class MiticationFactory {

    private static Logger logger = Logger.getLogger(MiticationFactory.class);
    private static MiticationFactory instance;
    private MitigationProjectFacade mitigationProjectFacade;
    private MitigationProjectMonthlyActivityFacade mitigationProjectMonthlyActivityFacade;


    public MiticationFactory() {

    }

    public static MiticationFactory getInstance() {
        if (instance == null) {
            instance = new MiticationFactory();
        }
        return instance;
    }

    public MitigationProjectFacade getMitigationProjectFacade(EntityManager em) {
        if (mitigationProjectFacade == null) {
            mitigationProjectFacade = new MitigationProjectFacade();
            mitigationProjectFacade.injectEntityManager(em);
        }

        return mitigationProjectFacade;
    }



    public MitigationProjectMonthlyActivityFacade getMitigationProjectMonthlyActivityFacade(EntityManager em) {
        if (mitigationProjectMonthlyActivityFacade == null) {
            mitigationProjectMonthlyActivityFacade = new MitigationProjectMonthlyActivityFacade();
            mitigationProjectMonthlyActivityFacade.injectEntityManager(em);
        }
        return mitigationProjectMonthlyActivityFacade;
    }


}
