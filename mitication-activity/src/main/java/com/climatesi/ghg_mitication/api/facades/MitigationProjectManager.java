package com.climatesi.ghg_mitication.api.facades;

import com.climatesi.ghg.utility.api.facade.DataEntryManager;
import com.climatesi.ghg_mitication.api.beans.MitigationProject;

import java.util.List;

public interface MitigationProjectManager extends DataEntryManager<MitigationProject, Integer> {
    void delete(int projectId);

    List<MitigationProject> getProjectsByCompany(int comId);

    List<MitigationProject> getProjectsByBranch(int branchId);
}
