package com.climatesi.ghg_mitication.api.enums;

public enum MiticationActivityTypes {
    Solar(1),
    BioMass(2),
    BioGass(3);

    private int type;

    MiticationActivityTypes(int type) {
        type = type;
    }

    public int getType() {
        return type;
    }
}
