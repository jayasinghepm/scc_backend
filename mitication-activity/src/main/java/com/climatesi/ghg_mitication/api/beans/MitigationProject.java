package com.climatesi.ghg_mitication.api.beans;

public interface MitigationProject {


    public int getProjectId() ;

    public void setProjectId(int projectId);

    public int getProjectType() ;


    public void setProjectType(int projectType);

    public int getComId();

    public void setComId(int comId) ;

    public int getBranchId() ;

    public void setBranchId(int branchId) ;

    public String getDescription() ;

    public void setDescription(String description);

    public long getUpdatedAt() ;

    public void setUpdatedAt(long updatedAt) ;

    public long getCreatedAt() ;

    public void setCreatedAt(long createdAt) ;

    public double getEmissionSaved() ;

    public void setEmissionSaved(double emissionSaved) ;

    public String getName() ;

    public void setName(String name) ;

    public int getIsDeleted();

    public void setIsDeleted(int isDeleted) ;
}
