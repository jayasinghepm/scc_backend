package com.climatesi.ghg_mitication.api.beans;

public interface MitigationProjectMonthlyActivity {


    public long getId() ;

    public void setId(long id) ;

    public int getProjectId();

    public void setProjectId(int projectId) ;

    public int getMonth() ;

    public void setMonth(int month) ;

    public String getYear() ;

    public void setYear(String year) ;

    public double getValue();

    public void setValue(double value) ;

    public int getUnit() ;

    public void setUnit(int unit) ;

    public double getSavedEmission();

    public void setSavedEmission(double savedEmission) ;

    public long getUpdatedAt();

    public void setUpdatedAt(long updatedAt) ;

    public int getIsDeleted();

    public void setIsDeleted(int isDeleted);
}
