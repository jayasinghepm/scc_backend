package com.climatesi.ghg_mitication.impl.beans;

import com.climatesi.ghg_mitication.api.beans.MitigationProjectMonthlyActivity;

import javax.persistence.*;


@Entity
@Table(name = "MITIGATION_PROJECT_MONTHLY_ACTIVITY")
@NamedQueries({
        @NamedQuery(name = "MitigationProjectMonthlyActivityBean.findById",
                query = "SELECT i FROM MitigationProjectMonthlyActivityBean i WHERE i.id = :id"),
})
public class MitigationProjectMonthlyActivityBean implements MitigationProjectMonthlyActivity {


    @Id
    @GeneratedValue(generator = "mitigationProjectMonthlyActivityIdSeq")
    @SequenceGenerator(name = "mitigationProjectMonthlyActivityIdSeq", sequenceName = "MITIGATION_PROJECT_MONTHLY_ACTIVITY_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    private long id;

    private int projectId;

    private int month;

    private String year;

    private double value;

    private int unit;

    private double savedEmission ; // tco2e

    private long updatedAt;

    private int isDeleted;

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getSavedEmission() {
        return savedEmission;
    }

    public void setSavedEmission(double savedEmission) {
        this.savedEmission = savedEmission;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }
}
