package com.climatesi.ghg_mitication.impl.facades;

import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg_mitication.api.beans.MitigationProject;
import com.climatesi.ghg_mitication.api.facades.MitigationProjectManager;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectBean;
import com.climatesi.ghg_mitication.impl.persistence.MitigationProjectDAO;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

public class MitigationProjectFacade implements MitigationProjectManager {

    private static Logger logger = Logger.getLogger(MitigationProjectFacade.class);

    private MitigationProjectDAO dao;

    @Override
    public ListResult<MitigationProject> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<MitigationProject> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public MitigationProject getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<MitigationProject> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<MitigationProject> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return null;
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new MitigationProjectDAO(entityManager);
    }

    @Override
    public Object addEntity(MitigationProject entity) {
        try {
            return dao.insert((MitigationProjectBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding BEan ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(MitigationProject entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(MitigationProject entity) {

        try {
            return dao.update((MitigationProjectBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding BranchBEan ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(MitigationProject entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(MitigationProject entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(MitigationProject entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(MitigationProject entity) {
        return null;
    }

    @Override
    public MitigationProject getDummyEntity() {
        return null;
    }

    @Override
    public void delete(int projectId) {
        dao.delete(projectId);
    }

    @Override
    public List<MitigationProject> getProjectsByCompany(int comId) {
        JsonObject filter = new JsonObject();
        JsonObject comFilter = new JsonObject();
//        JsonObject comFilterAttrValue = new JsonObject();
//        JsonObject comFilterAttrType = new JsonObject();
//        JsonObject comFilterAttrCol = new JsonObject();
//
//        comFilterAttrCol.addProperty("");

        filter.add("comId", comFilter);
        comFilter.addProperty("value", comId);
        comFilter.addProperty("col",1 );
        comFilter.addProperty("type", 1);

        List<MitigationProject> projects = (List<MitigationProject>) dao.findPaginatedSearchResults(filter,new JsonObject(), -1).getList();
        if (projects != null) {
            return projects.stream().filter(project -> {
                return project.getComId() == comId;
            }).collect(Collectors.toList());
        }
        return dao.findByCompany(comId);
    }

    @Override
    public List<MitigationProject> getProjectsByBranch(int branchId) {
        return dao.findByBranch(branchId);
    }

}