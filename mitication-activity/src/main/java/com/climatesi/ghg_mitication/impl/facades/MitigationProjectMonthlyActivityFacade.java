package com.climatesi.ghg_mitication.impl.facades;

import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg_mitication.api.beans.MitigationProjectMonthlyActivity;
import com.climatesi.ghg_mitication.api.facades.MitigationProjectMothlyActivityManager;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectBean;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectMonthlyActivityBean;
import com.climatesi.ghg_mitication.impl.persistence.MitigationProjectMonthlyActivityDAO;
import com.google.gson.JsonObject;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;


public class MitigationProjectMonthlyActivityFacade implements MitigationProjectMothlyActivityManager {

    private Logger logger = Logger.getLogger(MitigationProjectMonthlyActivityFacade.class);
    private MitigationProjectMonthlyActivityDAO dao;


    @Override
    public ListResult<MitigationProjectMonthlyActivity> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<MitigationProjectMonthlyActivity> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public MitigationProjectMonthlyActivity getEntityByKey(Long id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<MitigationProjectMonthlyActivity> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<MitigationProjectMonthlyActivity> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return null;
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        if (dao == null) {
            dao = new MitigationProjectMonthlyActivityDAO(entityManager);
        }
    }

    @Override
    public Object addEntity(MitigationProjectMonthlyActivity entity) {
        try {
            return dao.insert((MitigationProjectMonthlyActivityBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding BEan ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(MitigationProjectMonthlyActivity entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(MitigationProjectMonthlyActivity entity) {

        try {
            return dao.update((MitigationProjectMonthlyActivityBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding BranchBEan ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(MitigationProjectMonthlyActivity entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(MitigationProjectMonthlyActivity entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(MitigationProjectMonthlyActivity entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(MitigationProjectMonthlyActivity entity) {
        return null;
    }

    @Override
    public MitigationProjectMonthlyActivity getDummyEntity() {
        return null;
    }

    @Override
    public List<MitigationProjectMonthlyActivity> findByProjectId(int projectId) {
        return dao.findByProjectId(projectId);
    }

    @Override
    public void delete(long id) {
        dao.delete(id);
    }

    @Override
    public List<MitigationProjectMonthlyActivity> getDataByCompany(int comId) {

//        List<MitigationProjectMonthlyActivity> data = dao.findAll().getList();
//        if (data != null) {
//            return data.stream().filter(actData -> {
//                return actData
//            }).collect(Collectors.toList());
//        }

        return dao.findByCompany(comId);
    }

    public List<MitigationProjectMonthlyActivity> getDataByProjectId(int projectId) {
        JsonObject filter = new JsonObject();
        JsonObject projectFilter = new JsonObject();
//        JsonObject comFilterAttrValue = new JsonObject();
//        JsonObject comFilterAttrType = new JsonObject();
//        JsonObject comFilterAttrCol = new JsonObject();
//
//        comFilterAttrCol.addProperty("");

        filter.add("projectId", projectFilter);
        projectFilter.addProperty("value", projectId);
        projectFilter.addProperty("col",1 );
        projectFilter.addProperty("type", 1);

//        List<MitigationProjectMonthlyActivity> data = dao.findAll().getList();
//        if (data != null) {
//            return data.stream().filter(actData -> {
//                return actData.get
//            }).collect(Collectors.toList());
//        }
//
//        return dao.findByCompany(comId);
        return dao.findPaginatedSearchResults(filter, new JsonObject(), -1).getList();

    }

    @Override
    public List<MitigationProjectMonthlyActivity> findByProjectIdAndSortByMonth(int projectId) {
        List<MitigationProjectMonthlyActivity> actData =  dao.findAll().getList();
        if (actData != null) {
            return actData.stream().filter(data -> {
                return data.getProjectId() == projectId;
            }).collect(Collectors.toList());
        }
        return dao.findByProjectIdAndSortByMonth(projectId);
    }

    @Override
    public List<MitigationProjectMonthlyActivity> getDataByBranch(int branchId) {
        return dao.findByBranch(branchId);
    }
}
