package com.climatesi.ghg_mitication.impl.beans;

import com.climatesi.ghg_mitication.api.beans.MitigationProject;

import javax.persistence.*;

@Entity
@Table(name = "MITIGATION_PROJECT")
@NamedQueries({
        @NamedQuery(name = "MitigationProjectBean.findById",
                query = "SELECT i FROM MitigationProjectBean i WHERE i.projectId = :projectId"),
})
public class MitigationProjectBean  implements MitigationProject {

    @Id
    @GeneratedValue(generator = "mitigationProjectIdSeq")
    @SequenceGenerator(name = "mitigationProjectIdSeq", sequenceName = "MITIGATION_PROJECT_ID_SEQ", initialValue = 1, allocationSize = 1)
    private int projectId;

    private int projectType;

    private int comId;

    private int branchId;

    @Lob
    private String description;

    private long updatedAt;

    private long createdAt;

    private double emissionSaved; //tco2e

    private String name;

    private int isDeleted;

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getProjectType() {
        return projectType;
    }

    public void setProjectType(int projectType) {
        this.projectType = projectType;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public double getEmissionSaved() {
        return emissionSaved;
    }

    public void setEmissionSaved(double emissionSaved) {
        this.emissionSaved = emissionSaved;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
