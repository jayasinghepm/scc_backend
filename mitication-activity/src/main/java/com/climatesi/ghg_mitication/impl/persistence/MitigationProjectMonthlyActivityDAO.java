package com.climatesi.ghg_mitication.impl.persistence;

import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.climatesi.ghg_mitication.api.beans.MitigationProjectMonthlyActivity;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectMonthlyActivityBean;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MitigationProjectMonthlyActivityDAO extends AbstractDAO<MitigationProjectMonthlyActivityBean, MitigationProjectMonthlyActivity> {

    private static Logger logger = Logger.getLogger(MitigationProjectMonthlyActivityDAO.class.getName());
    private EntityManager em;

    public MitigationProjectMonthlyActivityDAO(EntityManager em) {
        super(em, MitigationProjectMonthlyActivityBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("MITIGATION_PROJECT_MONTHLY_ACTIVITY", "id", "MitigationProjectMonthlyActivityBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public long delete(long id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from MitigationProjectMonthlyActivityBean o where o.id = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }

    @Override
    public Map<String, Object> getAddSpParams(MitigationProjectMonthlyActivityBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(MitigationProjectMonthlyActivityBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(MitigationProjectMonthlyActivityBean entityImpl) throws GHGException {
        return null;
    }

    public List<MitigationProjectMonthlyActivity> findByProjectId(int projectId) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Select o  from MitigationProjectMonthlyActivityBean o where o.projectId = :projectId");
            query.setParameter("projectId", projectId);
            return query.getResultList();

//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<MitigationProjectMonthlyActivity> findByCompany(int comId) {
        try {
//            em.getTransaction().begin();
            Query query = em.createNativeQuery("select actData  from MitigationProjectMonthlyActivityBean actData join (select project MitigationProjectBean project where project.comId = :comId) on actData.projectId = project.projectId");
            query.setParameter("comId", comId);
            return query.getResultList();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<MitigationProjectMonthlyActivity> findByProjectIdAndSortByMonth(int projectId) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Select o  from MitigationProjectMonthlyActivityBean o where o.projectId = :projectId ORDER BY o.month");
            query.setParameter("projectId", projectId);
            return query.getResultList();

//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<MitigationProjectMonthlyActivity> findByBranch(int branchId) {
        try {
//            em.getTransaction().begin();
            Query query = em.createNativeQuery("select actData  from MitigationProjectMonthlyActivityBean actData join (select project MitigationProjectBean project where project.branchId = :branchId) on actData.projectId = project.projectId");
            query.setParameter("branchId", branchId);
            return query.getResultList();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }
}
