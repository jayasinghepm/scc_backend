package com.climatesi.ghg_mitication.impl.persistence;

import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.climatesi.ghg_mitication.api.beans.MitigationProject;
import com.climatesi.ghg_mitication.impl.beans.MitigationProjectBean;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MitigationProjectDAO extends AbstractDAO<MitigationProjectBean, MitigationProject> {

private static Logger logger = Logger.getLogger(MitigationProjectDAO.class.getName());
private EntityManager em;

public MitigationProjectDAO(EntityManager em) {
        super(em, MitigationProjectBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("MITIGATION_PROJECT", "projectId", "MitigationProjectBean");
        meta.setKeyPropertyName("projectId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);

    }


    public int delete(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from MitigationProjectBean o where o.projectId = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }

    @Override
    public Map<String, Object> getAddSpParams(MitigationProjectBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(MitigationProjectBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(MitigationProjectBean entityImpl) throws GHGException {
        return null;
    }

    public List<MitigationProject> findByCompany(int comId) {
        try {
//            em.getTransaction().begin();

            Query query = em.createQuery("select  o from MitigationProjectBean o where (o.comId = :comId)", MitigationProjectBean.class);
            query.setParameter("comId", comId);

            List <MitigationProject> results = query.getResultList();
//            em.getTransaction().commit();
            return results;
        }catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }

    }

    public List<MitigationProject> findByBranch(int branchId) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("select o from MitigationProjectBean o where o.branchId = :id");
            query.setParameter("id", branchId);

            return query.getResultList();
        }catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }

    }
}
