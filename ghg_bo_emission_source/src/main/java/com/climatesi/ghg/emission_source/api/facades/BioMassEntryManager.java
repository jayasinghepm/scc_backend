package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface BioMassEntryManager  extends DataEntryManager<BioMassEntry, Integer> {

    public List<BioMassEntry> getCustomFiltered(int branchId, int companyId, String fy);
}

