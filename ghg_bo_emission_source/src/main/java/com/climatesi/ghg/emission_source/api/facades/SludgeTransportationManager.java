package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.SludgeTransportation;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface SludgeTransportationManager extends DataEntryManager<SludgeTransportation, Integer> {

    public List<SludgeTransportation> getCustomFiltered(int branchId, int companyId, String fy);
}


