package com.climatesi.ghg.emission_source.api.enums;

public enum AirTravelTripWay {
    ONE_WAY_TRIP(1),
    TWO_WAY_TRIP(2);

    public final Integer way;

    private AirTravelTripWay(int way) {
        this.way = way;
    }
}
