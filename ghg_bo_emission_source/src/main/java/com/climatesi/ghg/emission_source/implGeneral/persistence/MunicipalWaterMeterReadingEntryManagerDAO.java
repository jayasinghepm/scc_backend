package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterMeterReadingEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.MunicipalWaterMeterReadingEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class MunicipalWaterMeterReadingEntryManagerDAO extends AbstractDAO<MunicipalWaterMeterReadingEntryBean, MunicipalWaterMeterReadingEntry> {

    private static Logger logger = Logger.getLogger(MunicipalWaterMeterReadingEntryManagerDAO.class);
    private static EntityManager em;


    public MunicipalWaterMeterReadingEntryManagerDAO(EntityManager em) {
        super(em, MunicipalWaterMeterReadingEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_MUNICIPAL_WATER_METER_READING_ENTRY", "MUNICIPAL_WATER_METER_READING_ID", "MunicipalWaterMeterReadingEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public MunicipalWaterMeterReadingEntry updateMunicipalWaterMeterReadingEntry(MunicipalWaterMeterReadingEntry entry) {
        logger.info("updateMunicipalWaterMeterReadingEntry(" + entry + ")");
        MunicipalWaterMeterReadingEntry updated = this.em.merge(entry);
        return  updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(MunicipalWaterMeterReadingEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(MunicipalWaterMeterReadingEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(MunicipalWaterMeterReadingEntryBean entityImpl) throws GHGException {
        return null;
    }
}
