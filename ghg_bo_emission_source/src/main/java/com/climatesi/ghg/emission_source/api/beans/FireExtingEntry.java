package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface FireExtingEntry {

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getFireExtEntryId();

    public void setFireExtEntryId(int id);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();

    public void setYear(String year);

    public float getWeight();

    public void setWeight(float weight);

    public float getAmountRefilled();

    public void setAmountRefilled(float amountRefilled);

    public int getFireExtinguisherType();

    public void setFireExtinguisherType(int type);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public int getNoOfTanks();

    public void setNoOfTanks(int noOfTanks);

    public int getFireExtType();

    public void setFireExtType(int fireExtType);

    public int getIsDeleted();

    public void setIsDeleted(int isDeleted);

    public String getCompany();

    public void setCompany(String company);

    public String getBranch();

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon) ;

    public String getFireExt();

    public void setFireExt(String fireExt);

}
