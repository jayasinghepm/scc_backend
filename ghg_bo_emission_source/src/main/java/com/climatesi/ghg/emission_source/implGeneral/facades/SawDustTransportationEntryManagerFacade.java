package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.SawDustTransportation;
import com.climatesi.ghg.emission_source.api.beans.SawDustTransportation;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.SawDustTransportationManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.SawDustTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.SawDustTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.SawDustTransportationEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class SawDustTransportationEntryManagerFacade  implements SawDustTransportationManager {

    private Logger logger = Logger.getLogger(SawDustTransportationEntryManagerFacade.class);
    private SawDustTransportationEntryManagerDAO dao;

    @Override
    public Object addEntity(SawDustTransportation entity) {
        try {
            return dao.insert((SawDustTransportationBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding SawDustTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(SawDustTransportation entity, int userID) {
        try {
            return dao.insert((SawDustTransportationBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding SawDustTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(SawDustTransportation entity) {
        try {
            return dao.update((SawDustTransportationBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  SawDustTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(SawDustTransportation entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(SawDustTransportation entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(SawDustTransportation entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(SawDustTransportation entity) {
        return null;
    }

    @Override
    public SawDustTransportation getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<SawDustTransportation> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<SawDustTransportation> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public SawDustTransportation getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<SawDustTransportation> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<SawDustTransportation> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new SawDustTransportationEntryManagerDAO(entityManager);
    }

    @Override
    public List<SawDustTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}


