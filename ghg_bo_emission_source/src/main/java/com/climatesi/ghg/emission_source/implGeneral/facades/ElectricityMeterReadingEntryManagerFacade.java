package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.ElectricityMeterReadingEntry;
import com.climatesi.ghg.emission_source.api.facades.ElectricityMeterReadingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.ElectricityMeterReadingEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.ElectricityMeterReadingEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class ElectricityMeterReadingEntryManagerFacade implements ElectricityMeterReadingEntryManager {

    private static Logger logger = Logger.getLogger(ElectricityMeterReadingEntryManagerFacade.class);
    private ElectricityMeterReadingEntryManagerDAO electricityMeterReadingEntryManagerDAO;

    @Override
    public Object addEntity(ElectricityMeterReadingEntry entity) {
        try {
            return electricityMeterReadingEntryManagerDAO.insert((ElectricityMeterReadingEntryBean)entity);
        } catch (GHGException e) {
            logger.error("Issue occurred while adding electricityMeterReadingEntry");
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(ElectricityMeterReadingEntry entity, int userID) {
        try {
            return electricityMeterReadingEntryManagerDAO.insert((ElectricityMeterReadingEntryBean)entity, userID);
        } catch (GHGException e) {
            logger.error("Issue occurred while adding electricityMeterReadingEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(ElectricityMeterReadingEntry entity) {
        try {
            return electricityMeterReadingEntryManagerDAO.update((ElectricityMeterReadingEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Issue occurred while updating electricityMeterReadingEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(ElectricityMeterReadingEntry entity, int userID) {
        try {
            return electricityMeterReadingEntryManagerDAO.update((ElectricityMeterReadingEntryBean) entity, userID);
        } catch (GHGException e) {
            logger.error("Issue occurred while updating electricityMeterReadingEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public boolean setStatusOfEntity(ElectricityMeterReadingEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(ElectricityMeterReadingEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(ElectricityMeterReadingEntry entity) {
        return null;
    }

    @Override
    public ElectricityMeterReadingEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<ElectricityMeterReadingEntry> getAllEntityList() {
        return electricityMeterReadingEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<ElectricityMeterReadingEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public ElectricityMeterReadingEntry getEntityByKey(Integer id) {
        return electricityMeterReadingEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<ElectricityMeterReadingEntry> getEntityListByFilter(Object filterCriteria) {
        return electricityMeterReadingEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<ElectricityMeterReadingEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return electricityMeterReadingEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        electricityMeterReadingEntryManagerDAO = new ElectricityMeterReadingEntryManagerDAO(entityManager);
    }
}
