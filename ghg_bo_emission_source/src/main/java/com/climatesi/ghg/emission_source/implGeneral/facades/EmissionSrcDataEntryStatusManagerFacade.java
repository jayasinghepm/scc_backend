package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.summary.EmissionSrcDataEntryStatus;
import com.climatesi.ghg.emission_source.api.facades.EmissionSrcDataEntryStatusManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.EmployeeCommutingEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.ProjectDataEntryStatusIdBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.EmissionSrcDataEntryStatusBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.EmissionSrcDataEntryStatusManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class EmissionSrcDataEntryStatusManagerFacade implements EmissionSrcDataEntryStatusManager {

    private static Logger logger = Logger.getLogger(EmissionSrcDataEntryStatusManagerFacade.class);
    private EmissionSrcDataEntryStatusManagerDAO dao;

    @Override
    public Object addEntity(EmissionSrcDataEntryStatus entity) {
        try {
            return dao.insert((EmissionSrcDataEntryStatusBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding EmissionSrcDataEntryStatusBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(EmissionSrcDataEntryStatus entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(EmissionSrcDataEntryStatus entity) {
        try {
            return dao.update((EmissionSrcDataEntryStatusBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while updating EmissionSrcDataEntryStatusBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(EmissionSrcDataEntryStatus entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(EmissionSrcDataEntryStatus entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(EmissionSrcDataEntryStatus entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(EmissionSrcDataEntryStatus entity) {
        return null;
    }

    @Override
    public EmissionSrcDataEntryStatus getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<EmissionSrcDataEntryStatus> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<EmissionSrcDataEntryStatus> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public EmissionSrcDataEntryStatus getEntityByKey(ProjectDataEntryStatusIdBean id) {
        return dao.findByKey(id);
    }


    @Override
    public ListResult<EmissionSrcDataEntryStatus> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<EmissionSrcDataEntryStatus> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);

    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new EmissionSrcDataEntryStatusManagerDAO(entityManager);
    }

    @Override
    public int deleteDataEntryStatues(int comId, String fy) {
        try {
            return dao.deleteDataEntryStatus(comId, fy);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }


}
