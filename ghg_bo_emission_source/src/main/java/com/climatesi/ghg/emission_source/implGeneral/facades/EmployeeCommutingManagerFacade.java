package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.EmpCommutingEntry;
import com.climatesi.ghg.emission_source.api.facades.EmployeCommutingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.EmployeeCommutingEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.EmployeeCommutingEntryDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class EmployeeCommutingManagerFacade implements EmployeCommutingEntryManager {

    private static Logger logger = Logger.getLogger(EmployeeCommutingManagerFacade.class);
    private EmployeeCommutingEntryDAO dao;


    @Override
    public Object addEntity(EmpCommutingEntry entity) {
        try {
            return dao.insert((EmployeeCommutingEntryBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding EmployeeCommutingEntryBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(EmpCommutingEntry entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(EmpCommutingEntry entity) {
        try {
            return dao.update((EmployeeCommutingEntryBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding EmployeeCommutingEntryBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(EmpCommutingEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(EmpCommutingEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(EmpCommutingEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(EmpCommutingEntry entity) {
        return null;
    }

    @Override
    public EmpCommutingEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<EmpCommutingEntry> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<EmpCommutingEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public EmpCommutingEntry getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<EmpCommutingEntry> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<EmpCommutingEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new EmployeeCommutingEntryDAO(entityManager);
    }

    @Override
    public List<EmpCommutingEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}
