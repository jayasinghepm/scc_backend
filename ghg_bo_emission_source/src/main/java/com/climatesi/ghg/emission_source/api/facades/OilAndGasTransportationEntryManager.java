package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.OilAndGasTransportation;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface OilAndGasTransportationEntryManager extends DataEntryManager<OilAndGasTransportation, Integer> {

    public List<OilAndGasTransportation> getCustomFiltered(int branchId, int companyId, String fy);
}


