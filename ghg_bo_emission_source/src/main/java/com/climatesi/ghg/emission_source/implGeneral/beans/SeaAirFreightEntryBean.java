package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.SeaAirFreightEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;


@Entity
@Table(name = "SEA_AIR_FREIGHT_ENTRY")
@NamedQueries({
        @NamedQuery(name = "SeaAirFreightEntryBean.findById",
                query = "SELECT i FROM SeaAirFreightEntryBean i WHERE i.id = :id"),
})
public class SeaAirFreightEntryBean  implements SeaAirFreightEntry {

    @Id
    @GeneratedValue(generator = "seaAirFreightEntryIdSeq")
    @SequenceGenerator(name = "seaAirFreightEntryIdSeq", sequenceName = "SEA_AIR_FREIGHT_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "SEA_AIR_FREIGHT_ENTRY_ID")
    private int id;

    private int portFrom;

    private int isAirFreight;

    private float freightKg;

    private int containerType; // 1-40ft 2-20ft 3-14.5



    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name = "YEAR")
    private String year;


    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

//    @Lob
//    @Column(name = "EMISSION_DETAILS")
//    @Convert(converter = HashMapConverter.class)
//    private HashMap<String, String> emissionDetails;

    @Column(name = "IS_DELETED")
    private int isDeleted;


    private String company;

    private String branch;

    private String mon;

    private String portFromText;

    public int getId() {
        return id;
    }

    public String getPortFromText() {
        return portFromText;
    }

    public void setPortFromText(String portFromText) {
        this.portFromText = portFromText;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortFrom() {
        return portFrom;
    }

    public void setPortFrom(int portFrom) {
        this.portFrom = portFrom;
    }

    public int getIsAirFreight() {
        return isAirFreight;
    }

    public void setIsAirFreight(int isAirFreight) {
        this.isAirFreight = isAirFreight;
    }

    public float getFreightKg() {
        return freightKg;
    }

    public void setFreightKg(float freightKg) {
        this.freightKg = freightKg;
    }

    public int getContainerType() {
        return containerType;
    }

    public void setContainerType(int containerType) {
        this.containerType = containerType;
    }

    public int getEmissionSrcId() {
        return emissionSrcId;
    }

    public void setEmissionSrcId(int emissionSrcId) {
        this.emissionSrcId = emissionSrcId;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }
}
