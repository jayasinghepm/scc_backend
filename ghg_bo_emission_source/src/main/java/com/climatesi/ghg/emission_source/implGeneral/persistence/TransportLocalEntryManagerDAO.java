package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.TransportLocalEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.TransportLocalEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TransportLocalEntryManagerDAO extends AbstractDAO<TransportLocalEntryBean, TransportLocalEntry> {

    private Logger logger = Logger.getLogger(TransportLocalEntryManagerDAO.class);
    private EntityManager em;

    public TransportLocalEntryManagerDAO(EntityManager em) {
        super(em, TransportLocalEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_TRANSPORT_LOCAL_ENTRY", "TRANSPORT_LOCAL_ENTRY_ID", "TransportLocalEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(TransportLocalEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(TransportLocalEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(TransportLocalEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<TransportLocalEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from TransportLocalEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted)", TransportLocalEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from TransportLocalEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", TransportLocalEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<TransportLocalEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<TransportLocalEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
