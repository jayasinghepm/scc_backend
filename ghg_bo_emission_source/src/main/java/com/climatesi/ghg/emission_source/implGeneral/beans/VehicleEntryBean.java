package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.VehicleEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_VEHICLE_ENTRY")
@NamedQueries({
        @NamedQuery(name = "VehicleEntryBean.findById",
                query = "SELECT i FROM VehicleEntryBean i WHERE i.entryId = :entryId"),
})
public class VehicleEntryBean implements VehicleEntry {

    @Id
    @GeneratedValue(generator = "vehicleEntryIdSeq")
    @SequenceGenerator(name = "vehicleEntryIdSeq", sequenceName = "VEHICLE_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "VEHICLE_ENTRY_ID")
    private int entryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name= "YEAR")
    private String year;


    @Column(name = "FUEL_TYPE")
    private int fuleType;

    @Column(name = "UNITS")
    private int units;

    @Column(name = "FUEL_CONSUMPTION")
    private float consumption;

    private float fuelEconomy;

    private float distance;

    @Column(name = "VEHICLE_MODEL")
    private int vehicleModel;

    @Column(name = "VEHICLE_CATEGORY")
    private int vehicleCategory;

    @Column(name = "VEHICLE_NUMBER")
    private String vehicleNumber;

    @Column(name = "IS_FUEL_PAID_BY_COMPANY")
    private boolean isFuelPaidbyCompany;

    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    private String model;

    private String category;

    private String fuel;

    public float getFuelEconomy() {
        return fuelEconomy;
    }

    public void setFuelEconomy(float fuelEconomy) {
        this.fuelEconomy = fuelEconomy;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getFuleType() {
        return fuleType;
    }

    public void setFuleType(int fuleType) {
        this.fuleType = fuleType;
    }

    public void setVehicleModel(int vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public void setFuelPaidbyCompany(boolean fuelPaidbyCompany) {
        isFuelPaidbyCompany = fuelPaidbyCompany;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }


    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    @Override
    public int getEntryId() {
        return this.entryId ;
    }

    @Override
    public void setEntryId(int id) {
        this.entryId  = id;
    }

    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate ;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String getYear() {
        return this.year;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }


    @Override
    public int getFuelType() {
        return this.fuleType;
    }

    @Override
    public void setFuelType(int type) {
        this.fuleType = type;
    }

    @Override
    public float getConsumption() {
        return this.consumption;
    }

    @Override
    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }

    @Override
    public int getUnits() {
        return this.units;
    }

    @Override
    public void setUnits(int units) {
        this.units = units;
    }


    @Override
    public boolean isFuelPaidbyCompany() {
        return this.isFuelPaidbyCompany;
    }

    @Override
    public void setIsFuelPaidbyCompany(boolean value) {
        this.isFuelPaidbyCompany = value;
    }

    @Override
    public int getVehicleModel() {
        return this.vehicleModel;
    }

    @Override
    public void setVehiclModel(int model) {
        this.vehicleModel = model;
    }

    @Override
    public int getVehicleCategory() {
        return this.vehicleCategory;
    }

    @Override
    public void setVehicleCategory(int category) {
        this.vehicleCategory = category;
    }



    @Override
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    @Override
    public void setVehicleNumber(String number) {
        this.vehicleNumber = number;
    }

    @Override
    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    @Override
    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }
}
