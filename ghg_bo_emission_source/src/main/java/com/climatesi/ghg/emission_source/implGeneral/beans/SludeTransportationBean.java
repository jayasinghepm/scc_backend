package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.SludgeTransportation;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;


@Entity
@Table(name = "EMISS_SLUDGE_TRANS_ENTRY")
@NamedQueries({
        @NamedQuery(name = "SludeTransportationBean.findById",
                query = "SELECT i FROM SludeTransportationBean i WHERE i.entryId = :entryId"),
})
public class SludeTransportationBean  implements SludgeTransportation {


    @Id
    @GeneratedValue(generator = "sludgeTransEntryIdSeq")
    @SequenceGenerator(name = "sludgeTransEntryIdSeq", sequenceName = "SLUDGE_TRANS_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "ENTRY_ID")
    private int entryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name= "YEAR")
    private String year;

    private int fuleType;//pasindu

    private float distance;

    private float fuelEconomy;

    private float noOfTrips;



    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;



    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    private String branch;

    private String mon;




    private float fuelConsumption; //liters

    public float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getEmissionSrcId() {
        return emissionSrcId;
    }

    public void setEmissionSrcId(int emissionSrcId) {
        this.emissionSrcId = emissionSrcId;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }
    //pasindu edited
    public int getFuleType() {
        return fuleType;
    }

    public void setFuleType(int fuleType) {
        this.fuleType = fuleType;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getFuelEconomy() {
        return fuelEconomy;
    }

    public void setFuelEconomy(float fuelEconomy) {
        this.fuelEconomy = fuelEconomy;
    }

    public float getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(float noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }



    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }
}
