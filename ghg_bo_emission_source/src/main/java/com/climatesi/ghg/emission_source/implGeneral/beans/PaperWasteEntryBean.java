package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;


@Entity
@Table(name = "EMISS_PAPER_WASTE_ENTRY")
@NamedQueries({
        @NamedQuery(name = "PaperWasteEntryBean.findById",
                query = "SELECT i FROM PaperWasteEntryBean i WHERE i.entryId = :entryId"),
})
public class PaperWasteEntryBean  implements PaperWaste {

    @Id
    @GeneratedValue(generator = "paperWasteEntryIdSeq")
    @SequenceGenerator(name = "paperWasteEntryIdSeq", sequenceName = "PAPER_WASTE_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "ENTRY_ID")
    private int entryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name = "YEAR")
    private String year;

//    @Column(name = "DISPOSAL_METHOD")
//    private int disposalMethod;
//
//    private String disposal;


    private float quantity;//kg


    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;


    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    private String branch;

    private String mon;


    //New -added
//    public String getDisposal() {
//        return disposal;
//    }
//
//    public void setDisposal(String disposal) {
//        this.disposal = disposal;
   // }
//----------

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getEmissionSrcId() {
        return emissionSrcId;
    }

    public void setEmissionSrcId(int emissionSrcId) {
        this.emissionSrcId = emissionSrcId;
    }

    public int getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(int addedBy) {
        this.addedBy = addedBy;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }


//    public int getDisposalMethod() {
//        return this.disposalMethod;
//    }
//
//
//    public void setDisposalMethod(int disposalMethod) {
//        this.disposalMethod = disposalMethod;
//    }

}