package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.TransportLocalEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface TransportLocalEntryManager extends DataEntryManager<TransportLocalEntry, Integer> {

    public List<TransportLocalEntry> getCustomFiltered(int branchId, int companyId, String fy);

}
