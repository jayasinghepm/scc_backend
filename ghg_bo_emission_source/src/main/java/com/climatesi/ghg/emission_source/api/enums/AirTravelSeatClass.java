package com.climatesi.ghg.emission_source.api.enums;

public enum AirTravelSeatClass {

    FIRST_CLASS(1),
    BUSINESS_CLASS(2),
    ECONOMIC_CLASS(3);

    public final int seatClass;
    private AirTravelSeatClass(int seatClass) {
        this.seatClass = seatClass;
    }

}
