package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.WasteDisposalEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_WASTE_DISPOSAL_ENTRY")
@NamedQueries({
        @NamedQuery(name = "WasteDisposalEntryBean.findById",
                query = "SELECT i FROM WasteDisposalEntryBean i WHERE i.entryId = :entryId"),
})
public class WasteDisposalEntryBean implements WasteDisposalEntry {

    @Id
    @GeneratedValue(generator = "wasteDisposalEntryIdSeq")
    @SequenceGenerator(name = "wasteDisposalEntryIdSeq", sequenceName = "WASTE_DISPOSAL_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "WASTE_DISPOSAL_ENTRY_ID")
    private int entryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name= "YEAR")
    private String year;

    @Column(name = "UNITS")
    private int units;

    @Column(name= "AMOUNT_DISPOSED")
    private float amountDisposed;

    @Column(name= "DISPOSAL_METHOD")
    private int disposalMethod;

    @Column(name= "WASTE_TYPE")
    private int wasteType;

    @Column(name= "SUB_CONTRACTOR_NAME")
    private String subContractorName;

    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    private String branch;

    private String mon;


    private String disposal;

    private String waste;

    public String getDisposal() {
        return disposal;
    }

    public void setDisposal(String disposal) {
        this.disposal = disposal;
    }


    public String getWaste() {
        return waste;
    }

    public void setWaste(String waste) {
        this.waste = waste;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public void setWasteType(int wasteType) {
        this.wasteType = wasteType;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }


    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    @Override
    public int getEntryId() {
        return this.entryId ;
    }

    @Override
    public void setEntryId(int id) {
        this.entryId  = id;
    }

    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate ;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String getYear() {
        return this.year;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public float getAmountDisposed() {
        return this.amountDisposed;
    }

    @Override
    public void setAmountDisposed(float amount) {
        this.amountDisposed = amount;
    }

    @Override
    public int getDisposalMethod() {
        return this.disposalMethod;
    }

    @Override
    public void setDisposalMethod(int disposalMethod) {
        this.disposalMethod = disposalMethod;
    }

    @Override
    public int getWasteType() {
        return this.wasteType;
    }

    @Override
    public void setWasteype(int type) {
        this.wasteType = type;
    }

    @Override
    public String getSubContractorName() {
        return this.subContractorName;
    }

    @Override
    public void setSubContractorName(String name) {
        this.subContractorName = name;
    }

    @Override
    public int getUnits() {
        return this.units;
    }

    @Override
    public void setUnits(int units) {
        this.units = units;
    }

    @Override
    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    @Override
    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }
}
