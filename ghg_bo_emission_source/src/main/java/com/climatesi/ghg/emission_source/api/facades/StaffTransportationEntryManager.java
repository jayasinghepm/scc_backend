package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.SawDustTransportation;
import com.climatesi.ghg.emission_source.api.beans.StaffTransportation;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface StaffTransportationEntryManager  extends DataEntryManager<StaffTransportation, Integer> {

    public List<StaffTransportation> getCustomFiltered(int branchId, int companyId, String fy);
}

