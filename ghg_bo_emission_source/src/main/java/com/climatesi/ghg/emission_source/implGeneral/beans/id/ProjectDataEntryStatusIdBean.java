package com.climatesi.ghg.emission_source.implGeneral.beans.id;

import com.climatesi.ghg.emission_source.api.beans.id.ProjectDataEntryStatusId;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ProjectDataEntryStatusIdBean  implements ProjectDataEntryStatusId {

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "FINANCIAL_YEAR")
    private String fy;

    @Column(name = "EM_SRC_")
    private int emissionSrc;

    public int getEmissionSrc() {
        return emissionSrc;
    }

    public void setEmissionSrc(int emissionSrc) {
        this.emissionSrc = emissionSrc;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public String getFY() {
        return this.fy;
    }

    @Override
    public void setFY(String fy) {
        this.fy = fy;
    }
}
