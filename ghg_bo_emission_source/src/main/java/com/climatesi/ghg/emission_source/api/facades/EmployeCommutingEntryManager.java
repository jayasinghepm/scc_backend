package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.EmpCommutingEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface EmployeCommutingEntryManager extends DataEntryManager<EmpCommutingEntry, Integer> {

    public List<EmpCommutingEntry> getCustomFiltered(int branchId, int companyId, String fy);

}
