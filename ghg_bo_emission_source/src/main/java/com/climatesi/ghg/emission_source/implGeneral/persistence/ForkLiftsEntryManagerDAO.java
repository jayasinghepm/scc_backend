package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.climatesi.ghg.emission_source.implGeneral.beans.ForkLiftsEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.ForkLiftsEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ForkLiftsEntryManagerDAO extends AbstractDAO<ForkLiftsEntryBean, Forklifts> {
    private static Logger logger = Logger.getLogger(ForkLiftsEntryManagerDAO.class.getName());
    private EntityManager em;

    public ForkLiftsEntryManagerDAO(EntityManager em) {
        super(em, ForkLiftsEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_FORK_LIFTS_ENTRY", "ENTRY_ID", "ForkLiftsEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public Forklifts update(Forklifts entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        Forklifts updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(ForkLiftsEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(ForkLiftsEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(ForkLiftsEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<Forklifts> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from ForkLiftsEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", ForkLiftsEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from ForkLiftsEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", ForkLiftsEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<Forklifts> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<Forklifts>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

