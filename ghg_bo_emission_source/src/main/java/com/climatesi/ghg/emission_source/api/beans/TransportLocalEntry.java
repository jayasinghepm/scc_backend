package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface TransportLocalEntry {

    public float getWeight();

    public void setWeight(float weight);

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getEntryId();

    public void setEntryId(int id);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();

    public void setYear(String year);

    public String getMaterialType();

    public void setMaterialype(String type);

    public String getSubContractorName();

    public void setSubContractorName(String name);

    public int getFuelType();

    public void setFuelType(int type);

    public int getVehicleType();

    public void setVehicleType(int type);

    public float getDistanceTravelled();

    public void setDistanceTravelled(float distanceTravelled);

    public float getFuelEconomy();

    public void setFuelEconomy(float economy);

    public float getLoadingCapacity();

    public void setLoadingCapacity(float capacity);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public String getCompany();

    public void setCompany(String company) ;

    public String getBranch();

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon);

    public String getFuel();

    public void setFuel(String fuel);

    public String getVehicle();

    public void setVehicle(String vehicle);


    public float getNoOfTrips();

    public void setNoOfTrips(float noOfTrips) ;

    public float getFuelConsumption() ;

    public void setFuelConsumption(float fuelConsumption) ;

}
