package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.id.GHGEmissionSummaryId;
import com.climatesi.ghg.emission_source.api.beans.summary.GHGEmsissionSummary;
import com.climatesi.ghg.emission_source.api.facades.GHGEmissionSummaryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.GHGEmissionSummaryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.GHGEmissionSummaryDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class GHGEmissionSummaryManagerFacade implements GHGEmissionSummaryManager {

    private static Logger logger = Logger.getLogger(GHGEmissionSummaryManagerFacade.class);
    private GHGEmissionSummaryDAO dao;

    @Override
    public Object addEntity(GHGEmsissionSummary entity) {
        try {
            return dao.insert((GHGEmissionSummaryBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding GHGEmsissionSummary ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    public int delete(int comId, String fy) {
        try {
            return dao.deleteGHGEmissionSummary(comId, fy);
        }catch (Exception e) {
            logger.error(e);
        }
        return  0;
    }

    @Override
    public GHGEmsissionSummary find(int companyId, String year, int modeOfEntry) {
        return dao.find(companyId, year, modeOfEntry);
    }

    @Override
    public Object addEntity(GHGEmsissionSummary entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(GHGEmsissionSummary entity) {
        try {
            return dao.update((GHGEmissionSummaryBean) entity);
        } catch (Exception e) {
            logger.error("Error occured  while updating GHGEmissionSummaryBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(GHGEmsissionSummary entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(GHGEmsissionSummary entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(GHGEmsissionSummary entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(GHGEmsissionSummary entity) {
        return null;
    }

    @Override
    public GHGEmsissionSummary getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<GHGEmsissionSummary> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<GHGEmsissionSummary> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public GHGEmsissionSummary getEntityByKey(GHGEmissionSummaryId id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<GHGEmsissionSummary> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<GHGEmsissionSummary> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new GHGEmissionSummaryDAO(entityManager);
    }
}
