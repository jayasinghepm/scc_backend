package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.RefrigerantsEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_REFRIGERANTS")
@NamedQueries({
        @NamedQuery(name = "RefrigerantsEntryBean.findById",
                query = "SELECT i FROM RefrigerantsEntryBean i WHERE i.refrigerantsEntryId = :refrigerantsEntryId"),
})
public class RefrigerantsEntryBean implements RefrigerantsEntry {

    @Id
    @GeneratedValue(generator = "refrigerantsEntryIdSeq")
    @SequenceGenerator(name = "refrigerantsEntryIdSeq", sequenceName = "REFRIGERANTS_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "REFRI_ENTRY_ID")
    private int refrigerantsEntryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name= "YEAR")
    private String year;

    @Column(name = "AMOUNT_REFILLED")
    private float amountRefilled;

    @Column(name = "REFRIGERENT_TYPE")
    private int refrigerentType;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

    private String company;

    private String branch;

    private String mon;


    private String refrigerent;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getRefrigerent() {
        return refrigerent;
    }

    public void setRefrigerent(String refrigerent) {
        this.refrigerent = refrigerent;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getRefrigerentType() {
        return refrigerentType;
    }

    public void setRefrigerentType(int refrigerentType) {
        this.refrigerentType = refrigerentType;
    }

    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }




    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate ;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String getYear() {
        return this.year;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }


    @Override
    public int getRefrigerantsEntryId() {
        return this.refrigerantsEntryId;
    }

    @Override
    public void setRefrigerantsEntryId(int id) {

    }

    @Override
    public int getTypeofRefrigerent() {
        return this.refrigerentType;
    }

    @Override
    public void setTypeofRefrigerent(int type) {
        this.refrigerentType = type;
    }

    @Override
    public float getAmountRefilled() {
        return this.amountRefilled;
    }

    @Override
    public void setAmountRefilled(float amountRefilled) {
        this.amountRefilled = amountRefilled;
    }

}
