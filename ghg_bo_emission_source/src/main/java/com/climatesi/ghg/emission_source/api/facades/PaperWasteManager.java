package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface PaperWasteManager extends DataEntryManager<PaperWaste, Integer> {

    public List<PaperWaste> getCustomFiltered(int branchId, int companyId, String fy);
}


