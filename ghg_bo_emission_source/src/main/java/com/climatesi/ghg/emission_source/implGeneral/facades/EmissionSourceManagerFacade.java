package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.EmissionSource;
import com.climatesi.ghg.emission_source.api.facades.EmissionSourceManager;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class EmissionSourceManagerFacade implements EmissionSourceManager {

    private static Logger logger = Logger.getLogger(EmissionSourceManagerFacade.class);


    @Override
    public Object addEntity(EmissionSource entity) {
        return null;
    }

    @Override
    public Object addEntity(EmissionSource entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(EmissionSource entity) {
        return null;
    }

    @Override
    public Object updateEntity(EmissionSource entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(EmissionSource entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(EmissionSource entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(EmissionSource entity) {
        return null;
    }

    @Override
    public EmissionSource getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<EmissionSource> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<EmissionSource> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public EmissionSource getEntityByKey(Integer id) {
        return null;
    }

    @Override
    public ListResult<EmissionSource> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<EmissionSource> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return null;
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {

    }
}
