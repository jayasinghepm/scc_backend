package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.emission_source.implGeneral.beans.LorryTransporationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.LorryTransporationBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LorryTransportationEntryManagerDAO extends AbstractDAO<LorryTransporationBean, LorryTransportation> {
    private static Logger logger = Logger.getLogger(LorryTransportationEntryManagerDAO.class.getName());
    private EntityManager em;

    public LorryTransportationEntryManagerDAO(EntityManager em) {
        super(em, LorryTransporationBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_LORRY_TRANS_ENTRY", "ENTRY_ID", "LorryTransporationBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public LorryTransportation update(LorryTransportation entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        LorryTransportation updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(LorryTransporationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(LorryTransporationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(LorryTransporationBean entityImpl) throws GHGException {
        return null;
    }

    public List<LorryTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from LorryTransporationBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", LorryTransporationBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from LorryTransporationBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", LorryTransporationBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<LorryTransportation> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<LorryTransportation>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

