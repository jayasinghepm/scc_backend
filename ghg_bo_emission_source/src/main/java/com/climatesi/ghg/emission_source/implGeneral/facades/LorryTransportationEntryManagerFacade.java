package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.LorryTransportationManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.LorryTransporationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.LorryTransporationBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.LorryTransportationEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class LorryTransportationEntryManagerFacade  implements LorryTransportationManager {

    private Logger logger = Logger.getLogger(LorryTransportationEntryManagerFacade.class);
    private LorryTransportationEntryManagerDAO dao;

    @Override
    public Object addEntity(LorryTransportation entity) {
        try {
            return dao.insert((LorryTransporationBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding LorryTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(LorryTransportation entity, int userID) {
        try {
            return dao.insert((LorryTransporationBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding LorryTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(LorryTransportation entity) {
        try {
            return dao.update((LorryTransporationBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  LorryTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(LorryTransportation entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(LorryTransportation entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(LorryTransportation entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(LorryTransportation entity) {
        return null;
    }

    @Override
    public LorryTransportation getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<LorryTransportation> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<LorryTransportation> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public LorryTransportation getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<LorryTransportation> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<LorryTransportation> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new LorryTransportationEntryManagerDAO(entityManager);
    }

    @Override
    public List<LorryTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

