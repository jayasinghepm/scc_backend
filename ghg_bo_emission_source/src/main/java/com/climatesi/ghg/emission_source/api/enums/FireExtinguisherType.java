package com.climatesi.ghg.emission_source.api.enums;

// Todo:  add types
public enum FireExtinguisherType {

      WGAS(1),
      CO2(2),
      DCP(3);

      private int type;

      FireExtinguisherType(int type) {
          this.type = type;
      }

    public int getType() {
        return type;
    }
}
