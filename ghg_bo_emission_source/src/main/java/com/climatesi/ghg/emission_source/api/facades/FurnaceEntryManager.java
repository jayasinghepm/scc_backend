package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.FurnaceOil;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface FurnaceEntryManager extends DataEntryManager<FurnaceOil, Integer> {

    public List<FurnaceOil> getCustomFiltered(int branchId, int companyId, String fy);
}


