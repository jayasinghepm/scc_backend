package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.api.facades.LPGEntryEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.LPGEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.LPGEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.LPGEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class LPGEntryManagerFacade implements LPGEntryEntryManager {

    private Logger logger = Logger.getLogger(LPGEntryManagerFacade.class);
    private LPGEntryManagerDAO dao;

    @Override
    public Object addEntity(LPGEntry entity) {
        try {
            return dao.insert((LPGEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding LPGEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(LPGEntry entity, int userID) {
        try {
            return dao.insert((LPGEntryBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding LPGEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(LPGEntry entity) {
        try {
            return dao.update((LPGEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  LPGEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(LPGEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(LPGEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(LPGEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(LPGEntry entity) {
        return null;
    }

    @Override
    public LPGEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<LPGEntry> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<LPGEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public LPGEntry getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<LPGEntry> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<LPGEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new LPGEntryManagerDAO(entityManager);
    }

    @Override
    public List<LPGEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}
