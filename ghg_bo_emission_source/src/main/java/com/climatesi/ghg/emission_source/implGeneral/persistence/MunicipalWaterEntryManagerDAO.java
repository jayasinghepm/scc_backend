package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.MunicipalWaterEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MunicipalWaterEntryManagerDAO extends AbstractDAO<MunicipalWaterEntryBean, MunicipalWaterEntry> {

    private static Logger logger = Logger.getLogger(MunicipalWaterEntryManagerDAO.class);
    private static EntityManager em;


    public MunicipalWaterEntryManagerDAO(EntityManager em) {
        super(em, MunicipalWaterEntryBean.class);
        this.em =em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_MUNICIPAL_WATER_ENTRY","MUNICIPAL_WATER_ENTRY_ID", "MunicipalWaterEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public MunicipalWaterEntry updateMunicipalWaterEntry(MunicipalWaterEntry entry) {
        logger.info("updateMunicipalWaterEntry" + entry + ")");
        MunicipalWaterEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(MunicipalWaterEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(MunicipalWaterEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(MunicipalWaterEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<MunicipalWaterEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from MunicipalWaterEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted)", MunicipalWaterEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from MunicipalWaterEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", MunicipalWaterEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<MunicipalWaterEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<MunicipalWaterEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
