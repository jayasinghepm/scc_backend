package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.RawMaterialTransporationLocal;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface RawMaterialTransportationLocalManager extends DataEntryManager<RawMaterialTransporationLocal, Integer> {

    public List<RawMaterialTransporationLocal> getCustomFiltered(int branchId, int companyId, String fy);
}


