package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.summary.GHGEmsissionSummary;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.GHGEmissionSummaryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class GHGEmissionSummaryDAO extends AbstractDAO<GHGEmissionSummaryBean, GHGEmsissionSummary> {
    private static Logger logger = Logger.getLogger(GHGEmissionSummaryDAO.class);
    private static EntityManager em;

    public GHGEmissionSummaryDAO(EntityManager em) {
        super(em, GHGEmissionSummaryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_GHG_SUMMARY", "FINANCIAL_YEAR", "GHGEmissionSummaryBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public int deleteGHGEmissionSummary(int comId, String fy) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from GHGEmissionSummaryBean l where l.id.companyId = :comId and l.id.fy = : fy");
            query.setParameter("comId", comId);
            query.setParameter("fy", fy);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return comId;
    }
    public GHGEmsissionSummary find(int companyId, String year, int modeOfEntry) {

        Query query = null;
        GHGEmsissionSummary summary = null;

            query = em.createQuery("select o from GHGEmissionSummaryBean o where ( o.id.fy = :year and o.id.companyId = :companyId)", GHGEmissionSummaryBean.class);
//            query.setParameter("modeOfEntry", modeOfEntry); o.modeOfEntry = :modeOfEntry and
            query.setParameter("year", year);
            query.setParameter("companyId", companyId);



        try {
            Object resultView = query.getSingleResult();
            summary = (GHGEmsissionSummary) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return summary;
    }

    @Override
    public Map<String, Object> getAddSpParams(GHGEmissionSummaryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(GHGEmissionSummaryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(GHGEmissionSummaryBean entityImpl) throws GHGException {
        return null;
    }
}
