package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;

public interface MunicipalWaterMeterReadingEntry {

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getEntryId();

    public void setEntryId(int id);

    public String getMeterNumber();

    public float getReadingForMonth();

    public void setReadingForMonth(float reading);

    public void setMeterNumber(String number);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public int getMonth();

    public void setMonth(int month);

    public int getYear();

    public void setYear(int year);
}
