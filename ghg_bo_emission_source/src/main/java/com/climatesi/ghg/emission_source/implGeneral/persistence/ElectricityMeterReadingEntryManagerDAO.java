package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.ElectricityMeterReadingEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.ElectricityMeterReadingEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class ElectricityMeterReadingEntryManagerDAO extends AbstractDAO<ElectricityMeterReadingEntryBean, ElectricityMeterReadingEntry> {

    private static Logger logger = Logger.getLogger(ElectricityMeterReadingEntryManagerDAO.class.getName());
    private EntityManager em;

    public ElectricityMeterReadingEntryManagerDAO(EntityManager em) {
        super(em, ElectricityMeterReadingEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_ELECTRICITY_METER_READING_ENTRY", "ELEC_METER_READING_ID", "ElectricityMeterReadingEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public ElectricityMeterReadingEntry updateElectricityMeterReadingEntry(ElectricityMeterReadingEntry entry) {
        ElectricityMeterReadingEntry updated = em.merge(entry);
        return updated;
    }



    @Override
    public Map<String, Object> getAddSpParams(ElectricityMeterReadingEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(ElectricityMeterReadingEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(ElectricityMeterReadingEntryBean entityImpl) throws GHGException {
        return null;
    }
}
