package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.AshTransportation;
import com.climatesi.ghg.emission_source.api.beans.AshTransportation;
import com.climatesi.ghg.emission_source.api.facades.AshTransportationEntryManager;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.AshTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.AshTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.AshTransportationEntryManagerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class AshTransportationEntryManagerFacade  implements AshTransportationEntryManager {

    private Logger logger = Logger.getLogger(AshTransportationEntryManagerFacade.class);
    private AshTransportationEntryManagerDAO dao;

    @Override
    public Object addEntity(AshTransportation entity) {
        try {
            return dao.insert((AshTransportationBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding AshTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(AshTransportation entity, int userID) {
        try {
            return dao.insert((AshTransportationBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding AshTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(AshTransportation entity) {
        try {
            return dao.update((AshTransportationBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  AshTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(AshTransportation entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(AshTransportation entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(AshTransportation entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(AshTransportation entity) {
        return null;
    }

    @Override
    public AshTransportation getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<AshTransportation> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<AshTransportation> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public AshTransportation getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<AshTransportation> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<AshTransportation> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new AshTransportationEntryManagerDAO(entityManager);
    }

    @Override
    public List<AshTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

