package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans. SeaAirFreightEntry;
import com.climatesi.ghg.emission_source.api.beans.SeaAirFreightEntry;
import com.climatesi.ghg.emission_source.api.facades.RefrigerantsEntryManager;
import com.climatesi.ghg.emission_source.api.facades.SeaAirFreightFacade;
import com.climatesi.ghg.emission_source.implGeneral.beans.SeaAirFreightEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.SeaAirFreightEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.SeaAirFreightEntryDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class SeaAirFreightFacadeManager  implements SeaAirFreightFacade {

    private static Logger logger = Logger.getLogger(RefrigerantsEntryManagerFacade.class);
    private static SeaAirFreightEntryDAO dao;

    @Override
    public Object addEntity(SeaAirFreightEntry entity) {
        try {
            return dao.insert((SeaAirFreightEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding  SeaAirFreightEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity( SeaAirFreightEntry entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity( SeaAirFreightEntry entity) {
        try {
            return dao.update((SeaAirFreightEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating  SeaAirFreightEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity( SeaAirFreightEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity( SeaAirFreightEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity( SeaAirFreightEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus( SeaAirFreightEntry entity) {
        return null;
    }

    @Override
    public  SeaAirFreightEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult< SeaAirFreightEntry> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult< SeaAirFreightEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public  SeaAirFreightEntry getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult< SeaAirFreightEntry> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult< SeaAirFreightEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new SeaAirFreightEntryDAO(entityManager);
    }

    @Override
    public List< SeaAirFreightEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}
