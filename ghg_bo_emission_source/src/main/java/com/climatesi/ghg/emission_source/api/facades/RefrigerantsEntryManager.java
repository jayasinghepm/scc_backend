package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.RefrigerantsEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface RefrigerantsEntryManager extends DataEntryManager<RefrigerantsEntry, Integer> {

    public List<RefrigerantsEntry> getCustomFiltered(int branchId, int companyId, String fy);

}
