package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.TransportLocalEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_TRANSPORT_LOCAL_ENTRY")
@NamedQueries({
        @NamedQuery(name = "TransportLocalEntryBean.findById",
                query = "SELECT i FROM TransportLocalEntryBean i WHERE i.entryId = :entryId"),
})
public class TransportLocalEntryBean implements TransportLocalEntry {


    @Id
    @GeneratedValue(generator = "transportLocalEntryIdSeq")
    @SequenceGenerator(name = "transportLocalEntryIdSeq", sequenceName = "TRANSPORT_LOCAL_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "TRANSPORT_LOCAL_ENTRY_ID")
    private int entryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name = "YEAR")
    private String year;

    @Column(name= "MATERIAL_TYPE")
    private String materialType;

    @Column(name= "SUB_CONTRACTOR_NAME")
    private String subContractorName;

    @Column(name = "FUEL_TYPE")
    private int fuleType;

    @Column(name = "VEHICLE_TYPE")
    private int vehicleType;

    @Column(name = "FUEL_ECONOMY")
    private float fuelEconomy;

    @Column(name = "LOADING_CAPACITY")
    private float loadingCapacity;

    @Column(name = "DISTANCE_TRAVELLED")
    private float distanceTravelled;

    private float noOfTrips = 1;

    private float fuelConsumption; //liters

    private float weight; //tons

    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

//    @Lob
//    @Column(name = "EMISSION_DETAILS")
//    @Convert(converter = HashMapConverter.class)
//    private HashMap<String, String> emissionDetails;

    @Column(name = "IS_DELETED")
    private int isDeleted;


    private String company;

    private String branch;

    private String mon;


    private String fuel;

    private String vehicle;

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(float noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public int getFuleType() {
        return fuleType;
    }

    public void setFuleType(int fuleType) {
        this.fuleType = fuleType;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    @Override
    public int getEntryId() {
        return this.entryId;
    }

    @Override
    public void setEntryId(int id) {
        this.entryId = id;
    }

    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String getYear() {
        return this.year;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String getMaterialType() {
        return this.materialType;
    }

    @Override
    public void setMaterialype(String type) {
        this.materialType = type;
    }

    @Override
    public String getSubContractorName() {
        return this.subContractorName;
    }

    @Override
    public void setSubContractorName(String name) {
        this.subContractorName = name;
    }


    @Override
    public int getFuelType() {
        return this.fuleType;
    }

    @Override
    public void setFuelType(int type) {
        this.fuleType = type;
    }


    @Override
    public int getVehicleType() {
        return this.vehicleType;
    }

    @Override
    public void setVehicleType(int type) {
        this.vehicleType = type;
    }

    @Override
    public float getDistanceTravelled() {
        return this.distanceTravelled;
    }

    @Override
    public void setDistanceTravelled(float distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    @Override
    public float getFuelEconomy() {
        return this.fuelEconomy;
    }

    @Override
    public void setFuelEconomy(float economy) {
        this.fuelEconomy = economy;
    }

    @Override
    public float getLoadingCapacity() {
        return this.loadingCapacity;
    }

    @Override
    public void setLoadingCapacity(float capacity) {
        this.loadingCapacity = capacity;
    }

    @Override
    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    @Override
    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }
}
