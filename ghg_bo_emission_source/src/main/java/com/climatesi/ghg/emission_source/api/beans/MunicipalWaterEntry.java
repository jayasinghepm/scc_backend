package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface MunicipalWaterEntry {

    public int isDeleted();

    public void setDeleted(int deleted);


    public String getMeterNo();

    public void setMeterNo(String meterNo);

    public int getEntryId();

    public void setEntryId(int id);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();

    public void setYear(String year);

    public int getNumOfMeters();

    public void setNumOfMeters(int meters);

    public List<String> getMeterNumbers();

    public void setMeterNumbers(List<String> meterNumbers);

    public float getConsumption();

    public void setConsumption(float consumption);

    public int getUnits();

    public void setUnits(int units);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public String getCompany();
    public void setCompany(String company);

    public String getBranch() ;

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon);



}
