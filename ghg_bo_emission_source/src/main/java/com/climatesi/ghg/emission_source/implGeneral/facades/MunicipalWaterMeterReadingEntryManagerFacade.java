package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterMeterReadingEntry;
import com.climatesi.ghg.emission_source.api.facades.MunicipalWaterMeterReadingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.MunicipalWaterMeterReadingEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.MunicipalWaterMeterReadingEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class MunicipalWaterMeterReadingEntryManagerFacade implements MunicipalWaterMeterReadingEntryManager {

    private static Logger logger = Logger.getLogger(MunicipalWaterMeterReadingEntryManagerFacade.class);
    private static MunicipalWaterMeterReadingEntryManagerDAO municipalWaterMeterReadingEntryManagerDAO;

    @Override
    public Object addEntity(MunicipalWaterMeterReadingEntry entity) {
        try {
            return municipalWaterMeterReadingEntryManagerDAO.insert((MunicipalWaterMeterReadingEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Issue occurred while adding MunicipalWaterMeterReadingEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(MunicipalWaterMeterReadingEntry entity, int userID) {
        try {
            return municipalWaterMeterReadingEntryManagerDAO.insert((MunicipalWaterMeterReadingEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Issue occurred while adding MunicipalWaterMeterReadingEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(MunicipalWaterMeterReadingEntry entity) {
        try {
            return municipalWaterMeterReadingEntryManagerDAO.update((MunicipalWaterMeterReadingEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Issue occurred while updating MunicipalWaterMeterReadingEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(MunicipalWaterMeterReadingEntry entity, int userID) {
        try {
            return municipalWaterMeterReadingEntryManagerDAO.update((MunicipalWaterMeterReadingEntryBean) entity, userID);
        } catch (GHGException e) {
            logger.error("Issue occurred while updating MunicipalWaterMeterReadingEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public boolean setStatusOfEntity(MunicipalWaterMeterReadingEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(MunicipalWaterMeterReadingEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(MunicipalWaterMeterReadingEntry entity) {
        return null;
    }

    @Override
    public MunicipalWaterMeterReadingEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<MunicipalWaterMeterReadingEntry> getAllEntityList() {
        return municipalWaterMeterReadingEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<MunicipalWaterMeterReadingEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return municipalWaterMeterReadingEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public MunicipalWaterMeterReadingEntry getEntityByKey(Integer id) {
        return municipalWaterMeterReadingEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<MunicipalWaterMeterReadingEntry> getEntityListByFilter(Object filterCriteria) {
        return municipalWaterMeterReadingEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<MunicipalWaterMeterReadingEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return municipalWaterMeterReadingEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, 0);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        municipalWaterMeterReadingEntryManagerDAO = new MunicipalWaterMeterReadingEntryManagerDAO(entityManager);
    }
}
