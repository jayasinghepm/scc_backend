package com.climatesi.ghg.emission_source.api.beans.id;

import java.io.Serializable;

public interface ProjectDataEntryStatusId extends Serializable {
    public int getCompanyId();
    public void setCompanyId(int id);
    public int getBranchId();
    public void setBranchId(int id);

    public String getFY();

    public void setFY(String fy);


    public int getEmissionSrc();

    public void setEmissionSrc(int emissionSrc);
}
