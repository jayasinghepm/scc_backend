package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.SawDustTransportation;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface SawDustTransportationManager extends DataEntryManager<SawDustTransportation, Integer> {

    public List<SawDustTransportation> getCustomFiltered(int branchId, int companyId, String fy);
}

