package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.FireExtingEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_FIRE_EXTINGUISHER")
@NamedQueries({
        @NamedQuery(name = "FireExtingEntryBean.findById",
                query = "SELECT i FROM FireExtingEntryBean i WHERE i.fireExtEntryId = :fireExtEntryId"),
})
public class FireExtingEntryBean implements FireExtingEntry {

    @Id
    @GeneratedValue(generator = "fireExtEntryIdSeq")
    @SequenceGenerator(name = "fireExtEntryIdSeq", sequenceName = "FIRE_EXT_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "FIRE_EXT_ENTRY_ID")
    private int fireExtEntryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name= "YEAR")
    private String year;

    @Column(name = "WEIGHT")
    private float weight;

    @Column(name = "AMOUNT_REFILLED")
    private float amountRefilled;

    @Column(name = "FIRE_EXT_TYPE")
    private int fireExtType;


    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

    @Column(name = "NO_OF_TANKS")
    private int noOfTanks;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    private String fireExt;

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getFireExtType() {
        return fireExtType;
    }

    public void setFireExtType(int fireExtType) {
        this.fireExtType = fireExtType;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getFireExt() {
        return fireExt;
    }

    public void setFireExt(String fireExt) {
        this.fireExt = fireExt;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    @Override
    public int getFireExtEntryId() {
        return this.fireExtEntryId;
    }

    @Override
    public void setFireExtEntryId(int id) {
        this.fireExtEntryId = id;
    }

    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate ;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String getYear() {
        return this.year;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public float getWeight() {
        return this.weight;
    }

    @Override
    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public float getAmountRefilled() {
        return this.amountRefilled;
    }

    @Override
    public void setAmountRefilled(float amountRefilled) {
        this.amountRefilled = amountRefilled;
    }

    @Override
    public int getFireExtinguisherType() {
        return this.fireExtType;
    }

    @Override
    public void setFireExtinguisherType(int type) {
        this.fireExtType = type;
    }

    @Override
    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    @Override
    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public int getNoOfTanks() {
        return noOfTanks;
    }

    public void setNoOfTanks(int noOfTanks) {
        this.noOfTanks = noOfTanks;
    }
}
