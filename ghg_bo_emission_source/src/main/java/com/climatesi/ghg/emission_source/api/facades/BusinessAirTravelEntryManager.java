package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BusinessAirTravelEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface BusinessAirTravelEntryManager extends DataEntryManager<BusinessAirTravelEntry, Integer> {

    public List<BusinessAirTravelEntry> getCustomFiltered(int branchId, int companyId, String fy);


}
