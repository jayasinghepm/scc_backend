package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface SeaAirFreightEntry {


    public int getId();

    public void setId(int id);

    public String getPortFromText() ;

    public void setPortFromText(String portFromText);

    public int getPortFrom();

    public void setPortFrom(int portFrom);

    public int getIsAirFreight();

    public void setIsAirFreight(int isAirFreight);

    public float getFreightKg();

    public void setFreightKg(float freightKg);
    public int getContainerType();

    public void setContainerType(int containerType);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int emissionSrcId);

    public int getAddedBy();

    public void setAddedBy(int addedBy);
   

    public int getBranchId();

    public void setBranchId(int branchId);
    public int getCompanyId();

    public void setCompanyId(int companyId);

    public Date getAddedDate();

    public void setAddedDate(Date addedDate);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date lastUpdatedDate);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();

    public void setYear(String year);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public int getIsDeleted();

    public void setIsDeleted(int isDeleted);

    public String getCompany();

    public void setCompany(String company);

    public String getBranch();

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon);

}
