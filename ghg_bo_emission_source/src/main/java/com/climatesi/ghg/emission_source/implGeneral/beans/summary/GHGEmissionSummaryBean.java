package com.climatesi.ghg.emission_source.implGeneral.beans.summary;

import com.climatesi.ghg.emission_source.api.beans.summary.GHGEmsissionSummary;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.GHGEmissionSummaryIdBean;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter1;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_GHG_SUMMARY")
@NamedQueries({
        @NamedQuery(name = "GHGEmissionSummaryBean.findById",
                query = "SELECT i FROM GHGEmissionSummaryBean i WHERE i.id = :id"),
})
public class GHGEmissionSummaryBean implements GHGEmsissionSummary {


    @Id
    private GHGEmissionSummaryIdBean id;

    @Lob
    @Column(name = "EMISSION_INFO")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String,String> emission;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    @Column(name="MODE_OF_ENTRY")
    private int modeOfEntry;

    public int getModeOfEntry() {
        return modeOfEntry;
    }

    public void setModeOfEntry(int modeOfEntry) {
        this.modeOfEntry = modeOfEntry;
    }

    @Override
    public GHGEmissionSummaryIdBean getId() {
        return id;
    }

    @Override
    public void setId(GHGEmissionSummaryIdBean id) {
        this.id = id;
    }


    public HashMap<String,String> getEmission() {
        return emission;
    }

    public void setEmission(HashMap<String,String> emission) {
        this.emission = emission;
    }

    @Override
    public Date getAddedDate() {
        return addedDate;
    }

    @Override
    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    @Override
    public int getIsDeleted() {
        return isDeleted;
    }

    @Override
    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
