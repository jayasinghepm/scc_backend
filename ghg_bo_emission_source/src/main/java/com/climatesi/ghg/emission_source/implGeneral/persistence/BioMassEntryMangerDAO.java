package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BioMassEntryMangerDAO extends AbstractDAO<BioMassEntryBean, BioMassEntry> {
    private static Logger logger = Logger.getLogger(BioMassEntryMangerDAO.class.getName());
    private EntityManager em;

    public BioMassEntryMangerDAO(EntityManager em) {
        super(em, BioMassEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_BIO_MASS_ENTRY", "EMISS_BIO_MASS_ENTRY_ID", "BioMassEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public BioMassEntry update(BioMassEntry entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        BioMassEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(BioMassEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(BioMassEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(BioMassEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<BioMassEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from BioMassEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", BioMassEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from BioMassEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", BioMassEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<BioMassEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<BioMassEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

