package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.SludgeTransportation;
import com.climatesi.ghg.emission_source.api.beans.SludgeTransportation;
import com.climatesi.ghg.emission_source.implGeneral.beans.SludeTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.SludeTransportationBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SludgeTransporationEntryManagerDAO extends AbstractDAO<SludeTransportationBean, SludgeTransportation> {
    private static Logger logger = Logger.getLogger(SludgeTransporationEntryManagerDAO.class.getName());
    private EntityManager em;

    public SludgeTransporationEntryManagerDAO(EntityManager em) {
        super(em, SludeTransportationBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_SLUDGE_TRANS_ENTRY", "ENTRY_ID", "SludeTransportationBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public SludgeTransportation update(SludgeTransportation entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        SludgeTransportation updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(SludeTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(SludeTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(SludeTransportationBean entityImpl) throws GHGException {
        return null;
    }

    public List<SludgeTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from SludeTransportationBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", SludeTransportationBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from SludeTransportationBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", SludeTransportationBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<SludgeTransportation> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<SludgeTransportation>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

