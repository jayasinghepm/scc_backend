package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.LorryTransportation;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface LorryTransportationManager extends DataEntryManager<LorryTransportation, Integer> {

    public List<LorryTransportation> getCustomFiltered(int branchId, int companyId, String fy);
}


