package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface WasteDisposalEntry {

    public int isDeleted();

    public void setDeleted(int deleted);


    public int getEntryId();

    public void setEntryId(int id);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();

    public void setYear(String year);


    public float getAmountDisposed();

    public void setAmountDisposed(float amount);

    public int getDisposalMethod();

    public void setDisposalMethod(int disposalMethod);

    public int getWasteType();

    public void setWasteype(int type);

    public String getSubContractorName();

    public void setSubContractorName(String name);

    public int getUnits();

    public void setUnits(int units);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public void setWasteType(int wasteType);

    public int getIsDeleted();

    public void setIsDeleted(int isDeleted);

    public String getCompany() ;

    public void setCompany(String company);

    public String getBranch();

    public void setBranch(String branch) ;

    public String getMon();

    public void setMon(String mon);

    public String getDisposal();

    public void setDisposal(String disposal);


    public String getWaste();

    public void setWaste(String waste);

}
