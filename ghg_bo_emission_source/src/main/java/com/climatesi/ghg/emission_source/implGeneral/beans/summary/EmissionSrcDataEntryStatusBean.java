package com.climatesi.ghg.emission_source.implGeneral.beans.summary;

import com.climatesi.ghg.emission_source.api.beans.summary.EmissionSrcDataEntryStatus;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.ProjectDataEntryStatusIdBean;

import javax.persistence.*;

@Entity
@Table(name = "SUMM_INPUTS_STATUS")
@NamedQueries({
        @NamedQuery(name = "EmissionSrcDataEntryStatusBean.findById",
                query = "SELECT i FROM EmissionSrcDataEntryStatusBean i WHERE i.id = :id"),
})
public class EmissionSrcDataEntryStatusBean implements EmissionSrcDataEntryStatus {

    @Id
    private ProjectDataEntryStatusIdBean id;



    @Column(name = "STATUS")
    private int status;

    public ProjectDataEntryStatusIdBean getId() {
        return id;
    }

    public void setId(ProjectDataEntryStatusIdBean id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
