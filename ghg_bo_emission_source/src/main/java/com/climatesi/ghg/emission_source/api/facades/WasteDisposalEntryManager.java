package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.WasteDisposalEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface WasteDisposalEntryManager extends DataEntryManager<WasteDisposalEntry, Integer> {

    public List<WasteDisposalEntry> getCustomFiltered(int branchId, int companyId, String fy);

}
