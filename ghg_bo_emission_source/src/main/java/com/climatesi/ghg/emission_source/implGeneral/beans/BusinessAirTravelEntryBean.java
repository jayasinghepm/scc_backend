package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.BusinessAirTravelEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_BUSINESS_AIR_TRAVEL")
@NamedQueries({
        @NamedQuery(name = "BusinessAirTravelEntryBean.findById",
                query = "SELECT i FROM BusinessAirTravelEntryBean i WHERE i.airTravelEntryId = :airTravelEntryId"),
})
public class BusinessAirTravelEntryBean implements BusinessAirTravelEntry {

    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Id
    @GeneratedValue(generator = "airTravelEntryIdSeq")
    @SequenceGenerator(name = "airTravelEntryIdSeq", sequenceName = "AIR_TRAVEL_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "AIR_TRAVEL_ENTRY_ID")
    private int airTravelEntryId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "EMP_ID")
    private int empId;

    @Column(name = "TRAVEL_YEAR")
    private String travelYear;

    @Column(name = "TRAVEL_MONTH")
    private int travelMonth;

    @Column(name = "DEP_AIRPORT")
    private int depAirport;

    @Column(name = "DEP_COUNTRY")
    private int depCountry;

    @Column(name = "DEST_AIRPORT")
    private int destAirport;

    @Column(name = "DEST_COUNTRY")
    private int destCountry;

    @Column(name = "TRANSIST_1")
    private int transist1;

    @Column(name = "TRANSIST_2")
    private int transist2;

    @Column(name = "TRANSIST_3")
    private int transist3;

    @Column(name = "AIR_TRAVEL_WAY")
    private int airTravelway;

    @Column(name = "SEAT_CLASS")
    private int seatClass;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;


    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

    @Column(name = "NO_OF_EMPLOYEES")
    private int noOfEmps;

    @Column(name = "IS_DELETED")
    private int isDeleted;


    private String company;

    private String branch;

    private String mon;

    private String depCoun;

    private String destCoun;

    private String desPort;
    private String depPort;

    private String trans1;

    private String trans2;

    private String trans3;

    private String seat;

    private String travelWay;

    private float distance;


    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }


    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getDepAirport() {
        return depAirport;
    }

    public void setDepAirport(int depAirport) {
        this.depAirport = depAirport;
    }

    public int getDepCountry() {
        return depCountry;
    }

    public void setDepCountry(int depCountry) {
        this.depCountry = depCountry;
    }

    public int getTransist1() {
        return transist1;
    }

    public void setTransist1(int transist1) {
        this.transist1 = transist1;
    }

    public int getTransist2() {
        return transist2;
    }

    public void setTransist2(int transist2) {
        this.transist2 = transist2;
    }

    public int getTransist3() {
        return transist3;
    }

    public void setTransist3(int transist3) {
        this.transist3 = transist3;
    }

    public int getAirTravelway() {
        return airTravelway;
    }

    public void setAirTravelway(int airTravelway) {
        this.airTravelway = airTravelway;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getDepCoun() {
        return depCoun;
    }

    public void setDepCoun(String depCoun) {
        this.depCoun = depCoun;
    }

    public String getDestCoun() {
        return destCoun;
    }

    public void setDestCoun(String destCoun) {
        this.destCoun = destCoun;
    }

    public String getDesPort() {
        return desPort;
    }

    public void setDesPort(String desPort) {
        this.desPort = desPort;
    }

    public String getDepPort() {
        return depPort;
    }

    public void setDepPort(String depPort) {
        this.depPort = depPort;
    }

    public String getTrans1() {
        return trans1;
    }

    public void setTrans1(String trans1) {
        this.trans1 = trans1;
    }

    public String getTrans2() {
        return trans2;
    }

    public void setTrans2(String trans2) {
        this.trans2 = trans2;
    }

    public String getTrans3() {
        return trans3;
    }

    public void setTrans3(String trans3) {
        this.trans3 = trans3;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getTravelWay() {
        return travelWay;
    }

    public void setTravelWay(String travelWay) {
        this.travelWay = travelWay;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    @Override
    public int getEmissionSrcId() {
       return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAirTravelEntryId() {
        return this.airTravelEntryId;
    }

    @Override
    public void setAirTravelEntryId(int id) {
        this.airTravelEntryId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }

    @Override
    public int getEmployeeId() {
        return this.empId;
    }

    @Override
    public void setEmployeeId(int id) {
        this.empId = id;
    }

    @Override
    public String getTravelYear() {
        return this.travelYear;
    }

    @Override
    public void setTravelYear(String year) {
        this.travelYear = year;
    }

    @Override
    public int getTravelMonth() {
        return this.travelMonth;
    }

    @Override
    public void setTravelMonth(int month) {
        this.travelMonth = month;
    }

    @Override
    public int getDepartureAirport() {
        return this.depAirport;
    }

    @Override
    public void setDepartureAirport(int port) {
        this.depAirport = port;
    }

    @Override
    public int geDepartureCountry() {
        return this.depCountry;
    }

    @Override
    public void setDepartureCountry(int country) {
        this.depCountry = country;
    }

    @Override
    public int getDestCountry() {
        return this.destCountry;
    }

    @Override
    public void setDestCountry(int country) {
        this.destCountry = country;
    }

    @Override
    public int getTransist1Port() {
        return transist1;
    }

    @Override
    public void setTransist1Port(int port) {
        this.transist1 = port;
    }

    @Override
    public int getTransist2Port() {
        return this.transist2;
    }

    @Override
    public void setTransist2Port(int port) {
        this.transist2 = port;
    }

    @Override
    public int getTransist3Port() {
        return this.transist3;
    }

    @Override
    public void setTransist3Port(int port) {
        this.transist3 = port;
    }

    @Override
    public int getAirTravelWay() {
        return this.airTravelway;
    }

    @Override
    public void setAirTravelWay(int way) {
        this.airTravelway = way;
    }

    @Override
    public int getSeatClass() {
        return this.seatClass;
    }

    @Override
    public void setSeatClass(int seatClass) {
        this.seatClass = seatClass;
    }

    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate ;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    public int getDestAirport() {
        return destAirport;
    }

    public void setDestAirport(int destAirport) {
        this.destAirport = destAirport;
    }

    @Override
    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    @Override
    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }

    public int getNoOfEmps() {
        return noOfEmps;
    }

    public void setNoOfEmps(int noOfEmps) {
        this.noOfEmps = noOfEmps;
    }


}
