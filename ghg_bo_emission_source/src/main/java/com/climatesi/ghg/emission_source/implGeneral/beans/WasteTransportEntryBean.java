package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.WasteTransportEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "EMISS_WASTE_TRANSPORT_ENTRY")
@NamedQueries({
        @NamedQuery(name = "WasteTransportEntryBean.findById",
                query = "SELECT i FROM WasteTransportEntryBean i WHERE i.entryId = :entryId"),
})
public class WasteTransportEntryBean implements WasteTransportEntry {

    @Id
    @GeneratedValue(generator = "wasteTransportEntryIdSeq")
    @SequenceGenerator(name = "wasteTransportEntryIdSeq", sequenceName = "WASTE_TRANSPORT_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "WASTE_TRANSPORT_ENTRY_ID")
    private int entryId;

    //-------edit by pasindu---------
    @Column(name = "noOfTurns")
    private float noOfTurns;
    //-------edit by pasindu---------


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name = "YEAR")
    private String year;

    @Column(name= "WASTE_TYPE")
    private int wasteType;

    @Column(name= "SUB_CONTRACTOR_NAME")
    private String subContractorName;

    @Column(name = "FUEL_TYPE")
    private int fuleType;

    @Column(name = "VEHICLE_TYPE")
    private int vehicleType;

    @Column(name = "FUEL_ECONOMY")
    private float fuelEconomy;

    @Column(name = "LOADING_CAPACITY")
    private float loadingCapacity;

    @Column(name = "DISTANCE_TRAVELLED")
    private float distanceTravelled;

    @Column(name = "WASTE_TONS")
    private float wasteTons;

   // private float noOfTurns;

    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    private String waste;

    private String fuel;

    private String vehicle;

   /* public float getNoOfTurns() {
        return noOfTurns;
    }

    public void setNoOfTurns(float noOfTurns) {
        this.noOfTurns = noOfTurns;
    } */

    public void setWasteType(int wasteType) {
        this.wasteType = wasteType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getWaste() {
        return waste;
    }

    public void setWaste(String waste) {
        this.waste = waste;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }


    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    //-----edit by pasindu------
    @Override
    public void setNoOfTurns(float noOfTurns) {
        this.noOfTurns = noOfTurns;
    }

    @Override
    public float getNoOfTurns() {
        return this.noOfTurns;
    }
    //-----edit by pasindu------

    @Override
    public int getEntryId() {
        return this.entryId;
    }

    @Override
    public void setEntryId(int id) {
        this.entryId = id;
    }

    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String getYear() {
        return this.year;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public int getWasteType() {
        return this.wasteType;
    }

    @Override
    public void setWasteype(int type) {
        this.wasteType = type;
    }

    @Override
    public String getSubContractorName() {
        return this.subContractorName;
    }

    @Override
    public void setSubContractorName(String name) {
        this.subContractorName = name;
    }


    @Override
    public int getFuelType() {
        return this.fuleType;
    }

    @Override
    public void setFuelType(int type) {
        this.fuleType = type;
    }


    @Override
    public int getVehicleType() {
        return this.vehicleType;
    }

    @Override
    public void setVehicleType(int type) {
        this.vehicleType = type;
    }

    @Override
    public float getDistanceTravelled() {
        return this.distanceTravelled;
    }

    @Override
    public void setDistanceTravelled(float distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    @Override
    public float getFuelEconomy() {
        return this.fuelEconomy;
    }

    @Override
    public void setFuelEconomy(float economy) {
        this.fuelEconomy = economy;
    }

    @Override
    public float getLoadingCapacity() {
        return this.loadingCapacity;
    }

    @Override
    public void setLoadingCapacity(float capacity) {
        this.loadingCapacity = capacity;
    }

    @Override
    public float getWasteTons() {
        return this.wasteTons;
    }

    @Override
    public void setWasteTons(float weight) {
        this.wasteTons = weight;
    }

    @Override
    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    @Override
    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }
}
