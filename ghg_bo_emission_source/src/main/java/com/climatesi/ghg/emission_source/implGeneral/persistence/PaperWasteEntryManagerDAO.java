package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.emission_source.implGeneral.beans.PaperWasteEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.PaperWasteEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PaperWasteEntryManagerDAO extends AbstractDAO<PaperWasteEntryBean, PaperWaste> {
    private static Logger logger = Logger.getLogger(ElectricityEntryManagerDAO.class.getName());
    private EntityManager em;

    public PaperWasteEntryManagerDAO(EntityManager em) {
        super(em, PaperWasteEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_PAPER_WASTE_ENTRY", "ENTRY_ID", "PaperWasteEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public PaperWaste update(PaperWaste entry) {
        logger.info("update(" + entry + ")");
        PaperWaste updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(PaperWasteEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(PaperWasteEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(PaperWasteEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<PaperWaste> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from PaperWasteEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", PaperWasteEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from PaperWasteEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", PaperWasteEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<PaperWaste> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<PaperWaste>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
