package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.EmissionSource;
import com.climatesi.ghg.emission_source.implGeneral.beans.EmissionSrcBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class EmissionSourceManagerDAO extends AbstractDAO<EmissionSrcBean, EmissionSource> {

    private static Logger logger = Logger.getLogger(EmissionSourceManagerDAO.class);
    private EntityManager em;

    public EmissionSourceManagerDAO(EntityManager em) {
        super(em, EmissionSrcBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISSION_SRC", "EMISSION_SRC_ID", "EmissionSrcBean");
        meta.setKeyPropertyName("emissionSrcId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public EmissionSource updateEmissionSource(EmissionSource entry) {
        EmissionSource updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(EmissionSrcBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(EmissionSrcBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(EmissionSrcBean entityImpl) throws GHGException {
        return null;
    }
}
