package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.StaffTransportation;
import com.climatesi.ghg.emission_source.api.beans.StaffTransportation;
import com.climatesi.ghg.emission_source.api.facades.SludgeTransportationManager;
import com.climatesi.ghg.emission_source.api.facades.StaffTransportationEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.StaffTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.StaffTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.SludgeTransporationEntryManagerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.StaffTransportationEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class StaffTransportationEntryManagerFacade  implements StaffTransportationEntryManager {

    private Logger logger = Logger.getLogger(StaffTransportationEntryManagerFacade.class);
    private StaffTransportationEntryManagerDAO dao;

    @Override
    public Object addEntity(StaffTransportation entity) {
        try {
            return dao.insert((StaffTransportationBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding StaffTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(StaffTransportation  entity, int userID) {
        try {
            return dao.insert((StaffTransportationBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding StaffTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(StaffTransportation  entity) {
        try {
            return dao.update((StaffTransportationBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  StaffTransportation  ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(StaffTransportation  entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(StaffTransportation  entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(StaffTransportation  entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(StaffTransportation  entity) {
        return null;
    }

    @Override
    public StaffTransportation  getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<StaffTransportation > getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<StaffTransportation > getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public StaffTransportation  getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<StaffTransportation > getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<StaffTransportation > getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new StaffTransportationEntryManagerDAO(entityManager);
    }

    @Override
    public List<StaffTransportation > getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}