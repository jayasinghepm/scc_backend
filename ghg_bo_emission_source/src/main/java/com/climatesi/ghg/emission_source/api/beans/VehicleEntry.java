package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface VehicleEntry {


    public float getFuelEconomy() ;

    public void setFuelEconomy(float fuelEconomy);

    public float getDistance() ;

    public void setDistance(float distance);

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getEntryId();

    public void setEntryId(int id);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();

    public void setYear(String year);

    public boolean isFuelPaidbyCompany();

    public void setIsFuelPaidbyCompany(boolean value);

    public int getVehicleModel();

    public void setVehiclModel(int model);

    public int getVehicleCategory();

    public void setVehicleCategory(int category);

    public int getFuelType();

    public void setFuelType(int type);

    public String getVehicleNumber();

    public void setVehicleNumber(String number);

    public float getConsumption();

    public void setConsumption(float consumption);

    public int getUnits();

    public void setUnits(int units);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public String getModel();

    public void setModel(String model);

    public String getCategory();

    public void setCategory(String category);

    public String getFuel();

    public void setFuel(String fuel);

    public Date getAddedDate();

    public void setAddedDate(Date addedDate);

    public int getFuleType();

    public void setFuleType(int fuleType);

    public void setVehicleModel(int vehicleModel);

    public void setFuelPaidbyCompany(boolean fuelPaidbyCompany);

    public int getIsDeleted();

    public void setIsDeleted(int isDeleted);

    public String getCompany();

    public void setCompany(String company);

    public String getBranch();

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon);

}
