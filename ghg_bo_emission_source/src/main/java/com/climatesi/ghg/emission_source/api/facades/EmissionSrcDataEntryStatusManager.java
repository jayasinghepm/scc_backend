package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BusinessAirTravelEntry;
import com.climatesi.ghg.emission_source.api.beans.summary.EmissionSrcDataEntryStatus;
import com.climatesi.ghg.emission_source.implGeneral.beans.id.ProjectDataEntryStatusIdBean;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface EmissionSrcDataEntryStatusManager  extends DataEntryManager<EmissionSrcDataEntryStatus, ProjectDataEntryStatusIdBean> {

    int deleteDataEntryStatues(int comId, String fy);

}
