package com.climatesi.ghg.emission_source.api.enums;

public enum DataEntryStatus {

    PENDING(1),
    COMPLETED(2),
    NA(3);

    public final int status;
    private DataEntryStatus(int status) { this.status = status;}
}
