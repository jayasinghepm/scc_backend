package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.WasteDisposalEntry;
import com.climatesi.ghg.emission_source.api.facades.WasteDisposalEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.WasteDisposalEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.WasteDisposalEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class WasteDisposalEntryManagerFacade implements WasteDisposalEntryManager {

    private static Logger logger = Logger.getLogger(WasteDisposalEntryManagerFacade.class);
    private WasteDisposalEntryManagerDAO wasteDisposalEntryManagerDAO;

    @Override
    public Object addEntity(WasteDisposalEntry entity) {
        try {
            return wasteDisposalEntryManagerDAO.insert((WasteDisposalEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding wasteDisposalEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(WasteDisposalEntry entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(WasteDisposalEntry entity) {
        try {
            return wasteDisposalEntryManagerDAO.update((WasteDisposalEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating wasteDisposalEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(WasteDisposalEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(WasteDisposalEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(WasteDisposalEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(WasteDisposalEntry entity) {
        return null;
    }

    @Override
    public WasteDisposalEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<WasteDisposalEntry> getAllEntityList() {

        return wasteDisposalEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<WasteDisposalEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return wasteDisposalEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public WasteDisposalEntry getEntityByKey(Integer id) {
        return wasteDisposalEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<WasteDisposalEntry> getEntityListByFilter(Object filterCriteria) {
        return wasteDisposalEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<WasteDisposalEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return wasteDisposalEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        wasteDisposalEntryManagerDAO = new WasteDisposalEntryManagerDAO(entityManager);
    }

    @Override
    public List<WasteDisposalEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return wasteDisposalEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
