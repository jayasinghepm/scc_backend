package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.BGASEntry;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.BgasEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.LPGEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BGASEntryManagerDAO extends AbstractDAO<BgasEntryBean, BGASEntry> {
    private static Logger logger = Logger.getLogger(BGASEntryManagerDAO.class.getName());
    private EntityManager em;

    public BGASEntryManagerDAO(EntityManager em) {
        super(em, BgasEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_BGAS_ENTRY", "LPG_BGAS_ID", "BgasEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public BGASEntry update(BGASEntry entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        BGASEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(BgasEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(BgasEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(BgasEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<BGASEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from BgasEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", BgasEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from BgasEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", BgasEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<BGASEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<BGASEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

