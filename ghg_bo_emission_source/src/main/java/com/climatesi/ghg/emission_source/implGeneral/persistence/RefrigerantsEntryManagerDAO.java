package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.RefrigerantsEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.RefrigerantsEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RefrigerantsEntryManagerDAO extends AbstractDAO<RefrigerantsEntryBean, RefrigerantsEntry> {

    private static Logger logger = Logger.getLogger(RefrigerantsEntryManagerDAO.class);
    private static EntityManager em;

    public RefrigerantsEntryManagerDAO(EntityManager em) {
        super(em, RefrigerantsEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_REFRIGERANTS", "REFRI_ENTRY_ID", "RefrigerantsEntryBean");
        meta.setKeyPropertyName("refrigerantsEntryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public RefrigerantsEntry updateRefrigerantsEntry(RefrigerantsEntry entry) {
        logger.info("updateRefrigerantsEntry(" + entry + ")");
        RefrigerantsEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(RefrigerantsEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(RefrigerantsEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(RefrigerantsEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<RefrigerantsEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from RefrigerantsEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted)", RefrigerantsEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from RefrigerantsEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", RefrigerantsEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<RefrigerantsEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<RefrigerantsEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
