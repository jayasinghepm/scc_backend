package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.FurnaceOil ;
import com.climatesi.ghg.emission_source.api.beans.FurnaceOil;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.FurnaceEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.FurnaceOilEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.FurnaceOilEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.FurnaceEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class FurnaceEntryManagerFacade  implements FurnaceEntryManager {

    private Logger logger = Logger.getLogger(FurnaceEntryManagerFacade.class);
    private FurnaceEntryManagerDAO dao;

    @Override
    public Object addEntity(FurnaceOil entity) {
        try {
            return dao.insert((FurnaceOilEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding FurnaceOil ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(FurnaceOil  entity, int userID) {
        try {
            return dao.insert((FurnaceOilEntryBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding FurnaceOil ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(FurnaceOil  entity) {
        try {
            return dao.update((FurnaceOilEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  FurnaceOil  ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(FurnaceOil  entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(FurnaceOil  entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(FurnaceOil  entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(FurnaceOil  entity) {
        return null;
    }

    @Override
    public FurnaceOil  getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<FurnaceOil > getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<FurnaceOil > getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public FurnaceOil  getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<FurnaceOil > getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<FurnaceOil > getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new FurnaceEntryManagerDAO(entityManager);
    }

    @Override
    public List<FurnaceOil > getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

