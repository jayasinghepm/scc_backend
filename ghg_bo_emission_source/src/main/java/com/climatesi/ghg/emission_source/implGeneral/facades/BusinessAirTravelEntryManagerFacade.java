package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.BusinessAirTravelEntry;
import com.climatesi.ghg.emission_source.api.facades.BusinessAirTravelEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.BusinessAirTravelEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BusinessAirTravelEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class BusinessAirTravelEntryManagerFacade implements BusinessAirTravelEntryManager {

    private static Logger logger = Logger.getLogger(BusinessAirTravelEntryManagerFacade.class);
    private BusinessAirTravelEntryManagerDAO businessAirTravelEntryManagerDAO;

    @Override
    public Object addEntity(BusinessAirTravelEntry entity) {
        try {
            return  businessAirTravelEntryManagerDAO.insert((BusinessAirTravelEntryBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding BusinessAirTravelEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }

    }

    @Override
    public Object addEntity(BusinessAirTravelEntry entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(BusinessAirTravelEntry entity) {
       try {
           return  businessAirTravelEntryManagerDAO.update((BusinessAirTravelEntryBean) entity);
       } catch (Exception e) {
           logger.error("Error occured  while updating BusinessAirTravelEntry ", e);
           return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
       }
    }

    @Override
    public Object updateEntity(BusinessAirTravelEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(BusinessAirTravelEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(BusinessAirTravelEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(BusinessAirTravelEntry entity) {
        return null;
    }

    @Override
    public BusinessAirTravelEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<BusinessAirTravelEntry> getAllEntityList() {
        return businessAirTravelEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<BusinessAirTravelEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return businessAirTravelEntryManagerDAO.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public BusinessAirTravelEntry getEntityByKey(Integer id) {
        return businessAirTravelEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<BusinessAirTravelEntry> getEntityListByFilter(Object filterCriteria) {
        return businessAirTravelEntryManagerDAO.findPaginatedSearchResults(null, null, 0);
    }

    @Override
    public ListResult<BusinessAirTravelEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return businessAirTravelEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        businessAirTravelEntryManagerDAO = new BusinessAirTravelEntryManagerDAO(entityManager);
    }

    @Override
    public List<BusinessAirTravelEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return businessAirTravelEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
