package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface MunicipalWaterEntryManager extends DataEntryManager<MunicipalWaterEntry, Integer> {

    public List<MunicipalWaterEntry> getCustomFiltered(int branchId, int companyId, String fy);

}
