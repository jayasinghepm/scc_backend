package com.climatesi.ghg.emission_source.api.enums;

public enum RefrigerantTypes {

    R22(1),
    R407C(2),
    R410A(3),
    HFC134A(4);

    public final int type;

    private RefrigerantTypes(int type) {
        this.type = type;
    }

}
