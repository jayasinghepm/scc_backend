package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterEntry;
import com.climatesi.ghg.emission_source.api.facades.MunicipalWaterEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.MunicipalWaterEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.MunicipalWaterEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class MunicipalWaterEntryManagerFacade implements MunicipalWaterEntryManager {

    private static Logger logger = Logger.getLogger(MunicipalWaterEntryManagerFacade.class);
    private static MunicipalWaterEntryManagerDAO municipalWaterEntryManagerDAO;

    @Override
    public Object addEntity(MunicipalWaterEntry entity) {
        try {
            return municipalWaterEntryManagerDAO.insert((MunicipalWaterEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding MunicipalWaterEntryBean", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(MunicipalWaterEntry entity, int userID) {
        try {
            return municipalWaterEntryManagerDAO.insert((MunicipalWaterEntryBean) entity, userID);
        } catch (GHGException e) {
            logger.error("Error in adding MunicipalWaterEntryBean", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(MunicipalWaterEntry entity) {
        try {
            return municipalWaterEntryManagerDAO.update((MunicipalWaterEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating MunicipalWaterEntryBean", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(MunicipalWaterEntry entity, int userID) {
        try {
            return municipalWaterEntryManagerDAO.update((MunicipalWaterEntryBean) entity, userID);
        } catch (GHGException e) {
            logger.error("Error in updating MunicipalWaterEntryBean", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public boolean setStatusOfEntity(MunicipalWaterEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(MunicipalWaterEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(MunicipalWaterEntry entity) {
        return null;
    }

    @Override
    public MunicipalWaterEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<MunicipalWaterEntry> getAllEntityList() {
        return municipalWaterEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<MunicipalWaterEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return municipalWaterEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public MunicipalWaterEntry getEntityByKey(Integer id) {
        return municipalWaterEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<MunicipalWaterEntry> getEntityListByFilter(Object filterCriteria) {
        return municipalWaterEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<MunicipalWaterEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return municipalWaterEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        municipalWaterEntryManagerDAO = new MunicipalWaterEntryManagerDAO(entityManager);
    }

    @Override
    public List<MunicipalWaterEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return municipalWaterEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
