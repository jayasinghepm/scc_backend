package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.VehicleOthers;
import com.climatesi.ghg.emission_source.api.beans.WasteDisposalEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface VehicleOthersEntryManager extends DataEntryManager<VehicleOthers, Integer> {

    public List<VehicleOthers> getCustomFiltered(int branchId, int companyId, String fy);

}
