package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface BusinessAirTravelEntry {

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public int getAirTravelEntryId();

    public void setAirTravelEntryId(int id);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public int getEmployeeId();

    public void setEmployeeId(int id);

    public String getTravelYear();

    public void setTravelYear(String year);

    public int getTravelMonth();

    public void setTravelMonth(int month);

    public int getDepartureAirport();

    public void setDepartureAirport(int port);

    public int getDestAirport();

    public void setDestAirport(int port);

    public int geDepartureCountry();

    public void setDepartureCountry(int country);

    public int getDestCountry();

    public void setDestCountry(int country);

    public int getTransist1Port();

    public void setTransist1Port(int port);

    public int getTransist2Port();

    public void setTransist2Port(int port);

    public int getTransist3Port();

    public void setTransist3Port(int port);

    public int getAirTravelWay();

    public void setAirTravelWay(int way);

    public int getSeatClass();

    public void setSeatClass(int seatClass);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public int getNoOfEmps();

    public void setNoOfEmps(int noOfEmps);

    public int getEmpId();
    public void setEmpId(int empId) ;
    public int getDepAirport();

    public void setDepAirport(int depAirport);
    public int getDepCountry();

    public void setDepCountry(int depCountry);
    public int getTransist1();
    public void setTransist1(int transist1);

    public int getTransist2();

    public void setTransist2(int transist2);
    public int getTransist3() ;
    public void setTransist3(int transist3) ;
    public int getAirTravelway();
    public void setAirTravelway(int airTravelway) ;

    public Date getAddedDate();

    public void setAddedDate(Date addedDate);
    public int getIsDeleted() ;

    public void setIsDeleted(int isDeleted) ;

    public String getCompany() ;
    public void setCompany(String company);
    public String getBranch();

    public void setBranch(String branch) ;
    public String getMon();

    public void setMon(String mon) ;
    public String getDepCoun() ;

    public void setDepCoun(String depCoun);

    public String getDestCoun();

    public void setDestCoun(String destCoun);
    public String getDesPort();

    public void setDesPort(String desPort);

    public String getDepPort() ;

    public void setDepPort(String depPort) ;

    public String getTrans1() ;

    public void setTrans1(String trans1);

    public String getTrans2();

    public void setTrans2(String trans2);

    public String getTrans3() ;

    public void setTrans3(String trans3 );

    public String getSeat();

    public void setSeat(String seat) ;

    public String getTravelWay();

    public void setTravelWay(String travelWay);

    public float getDistance();

    public void setDistance(float distance);
}
