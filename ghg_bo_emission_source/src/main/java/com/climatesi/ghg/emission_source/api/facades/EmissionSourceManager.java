package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.EmissionSource;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface EmissionSourceManager extends DataEntryManager<EmissionSource, Integer> {
}
