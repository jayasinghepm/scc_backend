package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.SawDustTransportation;
import com.climatesi.ghg.emission_source.api.beans.SawDustTransportation;
import com.climatesi.ghg.emission_source.implGeneral.beans.SawDustTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.SawDustTransportationBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SawDustTransportationEntryManagerDAO extends AbstractDAO<SawDustTransportationBean, SawDustTransportation> {
    private static Logger logger = Logger.getLogger(SawDustTransportationEntryManagerDAO.class.getName());
    private EntityManager em;

    public SawDustTransportationEntryManagerDAO(EntityManager em) {
        super(em, SawDustTransportationBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_SAW_DUST_TRANS_ENTRY", "ENTRY_ID", "SawDustTransportationBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public SawDustTransportation update(SawDustTransportation entry) {
        logger.info("update(" + entry + ")");
        SawDustTransportation updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(SawDustTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(SawDustTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(SawDustTransportationBean entityImpl) throws GHGException {
        return null;
    }

    public List<SawDustTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from SawDustTransportationBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", SawDustTransportationBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from SawDustTransportationBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", SawDustTransportationBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<SawDustTransportation> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<SawDustTransportation>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

