package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.BGASEntry;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.api.facades.BGASEntryEntryManager;
import com.climatesi.ghg.emission_source.api.facades.LPGEntryEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.BgasEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.LPGEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BGASEntryManagerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.LPGEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class BGASEntryManagerFacade implements BGASEntryEntryManager {

    private Logger logger = Logger.getLogger(BGASEntryManagerFacade.class);
    private BGASEntryManagerDAO dao;

    @Override
    public Object addEntity(BGASEntry entity) {
        try {
            return dao.insert((BgasEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding LPGEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(BGASEntry entity, int userID) {
        try {
            return dao.insert((BgasEntryBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding LPGEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(BGASEntry entity) {
        try {
            return dao.update((BgasEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  LPGEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(BGASEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(BGASEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(BGASEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(BGASEntry entity) {
        return null;
    }

    @Override
    public BGASEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<BGASEntry> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<BGASEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public BGASEntry getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<BGASEntry> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<BGASEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new BGASEntryManagerDAO(entityManager);
    }

    @Override
    public List<BGASEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}
