package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.RawMaterialTransporationLocal;
import com.climatesi.ghg.emission_source.api.beans.RawMaterialTransporationLocal;
import com.climatesi.ghg.emission_source.implGeneral.beans.RawMaterialTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.RawMaterialTransportationBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RawMaterialTransportationEntryDAO extends AbstractDAO<RawMaterialTransportationBean, RawMaterialTransporationLocal> {
    private static Logger logger = Logger.getLogger(RawMaterialTransportationEntryDAO.class.getName());
    private EntityManager em;

    public RawMaterialTransportationEntryDAO(EntityManager em) {
        super(em, RawMaterialTransportationBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_RAW_MAT_LOCAL_TRANS_ENTRY", "ENTRY_ID", "RawMaterialTransportationBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public RawMaterialTransporationLocal update(RawMaterialTransporationLocal entry) {
        logger.info("update(" + entry + ")");
        RawMaterialTransporationLocal updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(RawMaterialTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(RawMaterialTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(RawMaterialTransportationBean entityImpl) throws GHGException {
        return null;
    }

    public List<RawMaterialTransporationLocal> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from RawMaterialTransportationBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", RawMaterialTransportationBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from RawMaterialTransportationBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", RawMaterialTransportationBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<RawMaterialTransporationLocal> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<RawMaterialTransporationLocal>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

