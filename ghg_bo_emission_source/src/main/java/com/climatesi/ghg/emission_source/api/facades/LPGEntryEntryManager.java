package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface LPGEntryEntryManager extends DataEntryManager<LPGEntry, Integer> {

    public List<LPGEntry> getCustomFiltered(int branchId, int companyId, String fy);
}
