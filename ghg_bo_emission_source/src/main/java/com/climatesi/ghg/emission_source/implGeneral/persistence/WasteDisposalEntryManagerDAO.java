package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.WasteDisposalEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.WasteDisposalEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WasteDisposalEntryManagerDAO extends AbstractDAO<WasteDisposalEntryBean, WasteDisposalEntry> {

    private Logger logger = Logger.getLogger(WasteDisposalEntryManagerDAO.class);
    private EntityManager em;

    public WasteDisposalEntryManagerDAO(EntityManager em) {
        super(em, WasteDisposalEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_WASTE_DISPOSAL_ENTRY", "WASTE_DISPOSAL_ENTRY_ID", "WasteDisposalEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public WasteDisposalEntry updateWasteDisposalEntry(WasteDisposalEntry entry) {
        logger.info("updateWasteDisposalEntry" + entry + ")");
        WasteDisposalEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(WasteDisposalEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(WasteDisposalEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(WasteDisposalEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<WasteDisposalEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from WasteDisposalEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted)", WasteDisposalEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from WasteDisposalEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", WasteDisposalEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<WasteDisposalEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<WasteDisposalEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
