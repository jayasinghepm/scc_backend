package com.climatesi.ghg.emission_source.api.enums;

public enum FuelTypes {
    Petrol(1),//LP_95
    Diesel(2),//LAD

    LP_92(3),
    LSD(4),;





    public final int type;

    private FuelTypes(int fuelType) {
        this.type = fuelType;
    }
}
