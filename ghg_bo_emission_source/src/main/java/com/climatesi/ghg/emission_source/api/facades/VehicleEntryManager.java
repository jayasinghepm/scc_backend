package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.VehicleEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface VehicleEntryManager extends DataEntryManager<VehicleEntry, Integer> {

    public List<VehicleEntry> getCustomFiltered(int branchId, int companyId, String fy);
}
