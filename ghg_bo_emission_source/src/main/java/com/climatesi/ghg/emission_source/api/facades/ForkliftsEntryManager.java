package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface ForkliftsEntryManager extends DataEntryManager<Forklifts, Integer> {

    public List<Forklifts> getCustomFiltered(int branchId, int companyId, String fy);
}


