package com.climatesi.ghg.emission_source.api.enums;

public enum VehicleTypes {
    Lorry(1),
    Tractor(2),
    Van(3),
    Jeep(4),
    Car(5),
    Prime_Move(6),
    Bike(7),
    Threewheel(8),
    Bus(9),
    Train(10),
    Walking(11),
    Cycling(12),
    Free_Ride(13),
    Agriculture_Tractors(14),
    Chain_saws(15),
    ForkLifts(16),
    AirportGroudSupportEquipment(17),
    Other_Offroad(18),
    Other(19);
    public final int type;

    private VehicleTypes(int type) {
        this.type = type;
    }
}
