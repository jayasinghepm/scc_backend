package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.api.beans.LPGEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.LPGEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.LPGEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LPGEntryManagerDAO extends AbstractDAO<LPGEntryBean, LPGEntry> {
    private static Logger logger = Logger.getLogger(LPGEntryManagerDAO.class.getName());
    private EntityManager em;

    public LPGEntryManagerDAO(EntityManager em) {
        super(em, LPGEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_LPG_ENTRY", "LPG_ENTRY_ID", "LPGEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public LPGEntry update(LPGEntry entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        LPGEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(LPGEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(LPGEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(LPGEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<LPGEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from LPGEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", LPGEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from LPGEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", LPGEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<LPGEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<LPGEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

