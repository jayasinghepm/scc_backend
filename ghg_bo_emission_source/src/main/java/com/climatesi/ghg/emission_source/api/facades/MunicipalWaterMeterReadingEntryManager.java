package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterMeterReadingEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface MunicipalWaterMeterReadingEntryManager extends DataEntryManager<MunicipalWaterMeterReadingEntry, Integer> {
}
