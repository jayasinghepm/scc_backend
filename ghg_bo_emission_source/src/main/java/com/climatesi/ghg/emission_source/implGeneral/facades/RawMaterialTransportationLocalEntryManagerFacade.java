package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.RawMaterialTransporationLocal;
import com.climatesi.ghg.emission_source.api.beans.RawMaterialTransporationLocal;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.RawMaterialTransportationLocalManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.RawMaterialTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.RawMaterialTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.RawMaterialTransportationEntryDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class RawMaterialTransportationLocalEntryManagerFacade implements RawMaterialTransportationLocalManager {

    private Logger logger = Logger.getLogger(RawMaterialTransportationLocalEntryManagerFacade.class);
    private RawMaterialTransportationEntryDAO dao;

    @Override
    public Object addEntity(RawMaterialTransporationLocal entity) {
        try {
            return dao.insert((RawMaterialTransportationBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding RawMaterialTransporationLocal", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(RawMaterialTransporationLocal entity, int userID) {
        try {
            return dao.insert((RawMaterialTransportationBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding RawMaterialTransporationLocal", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(RawMaterialTransporationLocal entity) {
        try {
            return dao.update((RawMaterialTransportationBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  RawMaterialTransporationLocal ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(RawMaterialTransporationLocal entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(RawMaterialTransporationLocal entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(RawMaterialTransporationLocal entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(RawMaterialTransporationLocal entity) {
        return null;
    }

    @Override
    public RawMaterialTransporationLocal getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<RawMaterialTransporationLocal> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<RawMaterialTransporationLocal> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public RawMaterialTransporationLocal getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<RawMaterialTransporationLocal> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<RawMaterialTransporationLocal> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new RawMaterialTransportationEntryDAO(entityManager);
    }

    @Override
    public List<RawMaterialTransporationLocal> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

