package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;

import com.climatesi.ghg.emission_source.api.beans.OilAndGasTransportation;
import com.climatesi.ghg.emission_source.implGeneral.beans.OilAndGasTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.OilAndGasTransportationBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OilAndGasTransportationEntryManagerDAO extends AbstractDAO<OilAndGasTransportationBean, OilAndGasTransportation> {
    private static Logger logger = Logger.getLogger(OilAndGasTransportationEntryManagerDAO.class.getName());
    private EntityManager em;

    public OilAndGasTransportationEntryManagerDAO(EntityManager em) {
        super(em, OilAndGasTransportationBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_OIL_GAS_TRANS_ENTRY", "ENTRY_ID", "OilAndGasTransportationBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public ElectricityEntry update(ElectricityEntry entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        ElectricityEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(OilAndGasTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(OilAndGasTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(OilAndGasTransportationBean entityImpl) throws GHGException {
        return null;
    }

    public List<OilAndGasTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from OilAndGasTransportationBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", OilAndGasTransportationBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from OilAndGasTransportationBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", OilAndGasTransportationBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<OilAndGasTransportation> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<OilAndGasTransportation>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

