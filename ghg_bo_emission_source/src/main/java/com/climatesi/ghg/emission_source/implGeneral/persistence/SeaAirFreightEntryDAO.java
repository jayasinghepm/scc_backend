package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans. SeaAirFreightEntry;
import com.climatesi.ghg.emission_source.api.beans.SeaAirFreightEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.SeaAirFreightEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.SeaAirFreightEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SeaAirFreightEntryDAO extends AbstractDAO<SeaAirFreightEntryBean, SeaAirFreightEntry> {

    private static Logger logger = Logger.getLogger( SeaAirFreightEntryDAO.class.getName());
    private EntityManager em;

    public SeaAirFreightEntryDAO(EntityManager em) {
        super(em, SeaAirFreightEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("SEA_AIR_FREIGHT_ENTRY", "SEA_AIR_FREIGHT_ENTRY_ID", "SeaAirFreightEntryBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public  SeaAirFreightEntry update( SeaAirFreightEntry entry) {
        logger.info("updateBusinessAirTravel(" + entry + ")");
         SeaAirFreightEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(SeaAirFreightEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(SeaAirFreightEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(SeaAirFreightEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List< SeaAirFreightEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from SeaAirFreightEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted)", SeaAirFreightEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from SeaAirFreightEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", SeaAirFreightEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List< SeaAirFreightEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List< SeaAirFreightEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }
}
