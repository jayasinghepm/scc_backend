package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.FireExtingEntry;
import com.climatesi.ghg.emission_source.api.facades.FireExtingEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.FireExtingEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.FireExtingEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class FireExtingEntryManagerFacade implements FireExtingEntryManager {
    private static Logger logger = Logger.getLogger(FireExtingEntryManagerFacade.class);
    private FireExtingEntryManagerDAO fireExtingEntryManagerDAO;

    @Override
    public Object addEntity(FireExtingEntry entity) {
        try {
            return fireExtingEntryManagerDAO.insert((FireExtingEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding FireExtingEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(FireExtingEntry entity, int userID) {
        try {
            return fireExtingEntryManagerDAO.insert((FireExtingEntryBean) entity, userID);
        } catch (GHGException e) {
            logger.error("Error in adding FireExtingEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(FireExtingEntry entity) {
        try {
            return fireExtingEntryManagerDAO.update((FireExtingEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating FireExtingEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(FireExtingEntry entity, int userID) {
        try {
            return fireExtingEntryManagerDAO.update((FireExtingEntryBean) entity, userID);
        } catch (GHGException e) {
            logger.error("Error in updating FireExtingEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public boolean setStatusOfEntity(FireExtingEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(FireExtingEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(FireExtingEntry entity) {
        return null;
    }

    @Override
    public FireExtingEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<FireExtingEntry> getAllEntityList() {
        return fireExtingEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<FireExtingEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return fireExtingEntryManagerDAO.findPaginatedSearchResults(null, sortingProperty, pageNumber);
    }

    @Override
    public FireExtingEntry getEntityByKey(Integer id) {
        return fireExtingEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<FireExtingEntry> getEntityListByFilter(Object filterCriteria) {
        return fireExtingEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<FireExtingEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return fireExtingEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        fireExtingEntryManagerDAO = new FireExtingEntryManagerDAO(entityManager);
    }

    @Override
    public List<FireExtingEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return fireExtingEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
