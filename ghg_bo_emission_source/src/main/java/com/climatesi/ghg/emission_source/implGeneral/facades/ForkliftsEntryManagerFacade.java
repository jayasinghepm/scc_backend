package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.Forklifts ;
import com.climatesi.ghg.emission_source.api.beans.Forklifts;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.ForkliftsEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.ForkLiftsEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.ForkLiftsEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.ForkLiftsEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class ForkliftsEntryManagerFacade  implements ForkliftsEntryManager {

    private Logger logger = Logger.getLogger(ForkliftsEntryManagerFacade.class);
    private ForkLiftsEntryManagerDAO dao;

    @Override
    public Object addEntity(Forklifts entity) {
        try {
            return dao.insert((ForkLiftsEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding Forklifts ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(Forklifts  entity, int userID) {
        try {
            return dao.insert((ForkLiftsEntryBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding Forklifts ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(Forklifts  entity) {
        try {
            return dao.update((ForkLiftsEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  Forklifts  ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(Forklifts  entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Forklifts  entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Forklifts  entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Forklifts  entity) {
        return null;
    }

    @Override
    public Forklifts  getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Forklifts > getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<Forklifts > getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public Forklifts  getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<Forklifts > getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<Forklifts > getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new ForkLiftsEntryManagerDAO(entityManager);
    }

    @Override
    public List<Forklifts > getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

