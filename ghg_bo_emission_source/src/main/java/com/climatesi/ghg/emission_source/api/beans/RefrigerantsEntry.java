package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface RefrigerantsEntry {

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public int getAddedBy();

    public void setAddedBy(int clientId);

    public int getBranchId();

    public void setBranchId(int id);

    public int getCompanyId();

    public void setCompanyId(int id);

    public Date getEntryAddedDate();

    public void setEntryAddedDate(Date date);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date date);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();

    public void setYear(String year);

    public int getRefrigerantsEntryId();

    public void setRefrigerantsEntryId(int id);

    public int getTypeofRefrigerent();

    public void setTypeofRefrigerent(int type);

    public float getAmountRefilled();

    public void setAmountRefilled(float amountRefilled);

    public int getRefrigerentType();

    public void setRefrigerentType(int refrigerentType);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public String getCompany();

    public void setCompany(String company);

    public String getBranch();

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon);

    public String getRefrigerent();

    public void setRefrigerent(String refrigerent);
}
