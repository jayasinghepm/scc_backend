package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.MunicipalWaterMeterReadingEntry;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "EMISS_MUNICIPAL_WATER_METER_READING_ENTRY")
@NamedQueries({
        @NamedQuery(name = "MunicipalWaterMeterReadingEntryBean.findById",
                query = "SELECT i FROM MunicipalWaterMeterReadingEntryBean i WHERE i.entryId = :entryId"),
})
public class MunicipalWaterMeterReadingEntryBean implements MunicipalWaterMeterReadingEntry {

    @Id
    @GeneratedValue(generator = "municipalMeterReadingEntryIdSeq")
    @SequenceGenerator(name = "municipalMeterReadingEntryIdSeq", sequenceName = "MUNCI_WATER_METER_READING_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "MUNICIPAL_WATER_METER_READING_ID")
    private int entryId;


    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name= "YEAR")
    private int year;

    @Column(name="READING_MONTH")
    private float readingMonth;

    @Column(name = "METER_NUMBER")
    private String meterNumber;

    @Override
    public int isDeleted() {
        return 0;
    }

    @Override
    public void setDeleted(int deleted) {

    }

    @Override
    public int getEntryId() {
        return this.entryId ;
    }

    @Override
    public void setEntryId(int id) {
        this.entryId  = id;
    }

    @Override
    public String getMeterNumber() {
        return this.meterNumber;
    }

    @Override
    public float getReadingForMonth() {
        return this.readingMonth;
    }

    @Override
    public void setReadingForMonth(float reading) {
        this.readingMonth = reading;
    }

    @Override
    public void setMeterNumber(String number) {
        this.meterNumber = number;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate ;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public int getYear() {
        return this.year;
    }

    @Override
    public void setYear(int year) {
        this.year = year;
    }


}
