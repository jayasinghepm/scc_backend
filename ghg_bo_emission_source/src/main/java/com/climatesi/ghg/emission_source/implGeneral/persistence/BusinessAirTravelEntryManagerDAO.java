package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.BusinessAirTravelEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.BusinessAirTravelEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BusinessAirTravelEntryManagerDAO extends AbstractDAO<BusinessAirTravelEntryBean, BusinessAirTravelEntry> {

    private static Logger logger = Logger.getLogger(BusinessAirTravelEntryManagerDAO.class.getName());
    private EntityManager em;

    public BusinessAirTravelEntryManagerDAO(EntityManager em) {
        super(em, BusinessAirTravelEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_BUSINESS_AIR_TRAVEL", "AIR_TRAVEL_ENTRY_ID", "BusinessAirTravelEntryBean");
        meta.setKeyPropertyName("airTravelEntryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public BusinessAirTravelEntry updateBusinessAirTravel(BusinessAirTravelEntry entry) {
        logger.info("updateBusinessAirTravel(" + entry + ")");
        BusinessAirTravelEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(BusinessAirTravelEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(BusinessAirTravelEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(BusinessAirTravelEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<BusinessAirTravelEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from BusinessAirTravelEntryBean o where (o.branchId = :branchId and o.travelYear = :fy and o.isDeleted = :isDeleted)", BusinessAirTravelEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from BusinessAirTravelEntryBean o where (o.companyId = :companyId and o.travelYear = :fy and o.isDeleted = :isDeleted)", BusinessAirTravelEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<BusinessAirTravelEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<BusinessAirTravelEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }
}
