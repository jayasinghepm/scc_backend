package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface EmpCommutingEntry {


    public int getId();


    public void setId(int id);


    public int getCompanyId();


    public void setCompanyId(int companyId);


    public int getBranchId();


    public void setBranchId(int branchId);


    public String getEmpId();


    public void setEmpId(String empId);

    public int getNoEmissionMode_UP();

    public void setNoEmissionMode_UP(int noEmissionMode_UP);

    public int getNoEmissionMode_DOWN();

    public void setNoEmissionMode_DOWN(int noEmissionMode_DOWN);

    public float getNoEmission_distance_UP();

    public void setNoEmission_distance_UP(float noEmission_distance_UP);

    public float getNoEmission_distance_DOWN();

    public void setNoEmission_distance_DOWN(float noEmission_distance_DOWN);

    public int getPublicTransMode_UP();

    public void setPublicTransMode_UP(int publicTransMode_UP);

    public int getPublicTransMode_DOWN();

    public void setPublicTransMode_DOWN(int publicTransMode_DOWN);

    public float getPublicTrans_distance_UP();

    public void setPublicTrans_distance_UP(float publicTrans_distance_UP);

    public float getPublicTrans_distance_DOWN();

    public void setPublicTrans_distance_DOWN(float publicTrans_distance_DOWN);

    public int getOwnTransMode_UP();

    public void setOwnTransMode_UP(int ownTransMode_UP);

    public int getOwnTransMode_DOWN();

    public void setOwnTransMode_DOWN(int ownTransMode_DOWN);

    public float getOwnTrans_distance_UP();

    public void setOwnTrans_distance_UP(float ownTrans_distance_UP);

    public float getOwnTrans_distance_DOWN();

    public void setOwnTrans_distance_DOWN(float ownTrans_distance_DOWN);

    public float getOwnTrans_fuelEconomy_UP();

    public void setOwnTrans_fuelEconomy_UP(float ownTrans_fuelEconomy_UP);

    public float getOwnTrans_fuelEconomy_DOWN();

    public void setOwnTrans_fuelEconomy_DOWN(float ownTrans_fuelEconomy_DOWN);

    public int getCompanyFuelMode_UP();

    public void setCompanyFuelMode_UP(int companyFuelMode_UP);

    public int getCompanyFuelMode_DOWN();

    public void setCompanyFuelMode_DOWN(int companyFuelMode_DOWN);

    public float getCompanyFuel_distance_UP();

    public void setCompanyFuel_distance_UP(float companyFuel_distance_UP);

    public float getCompanyFuel_distance_DOWN();

    public void setCompanyFuel_distance_DOWN(float companyFuel_distance_DOWN);

    public float getCompanyFuel_fuelEconomy_UP();

    public void setCompanyFuel_fuelEconomy_UP(float companyFuel_fuelEconomy_UP);

    
    public float getAnnualEmission();


    public void setAnnualEmission(float annualEmission);


    public Date getAddedDate();


    public void setAddedDate(Date addedDate);


    public Date getLastUpdatedDate();


    public void setLastUpdatedDate(Date lastUpdatedDate);


    public int getMonth();


    public void setMonth(int month);


    public String getYear();


    public void setYear(String year);

    public int getCompany_fuelType_UP();

    public void setCompany_fuelType_UP(int company_fuelType_UP);

    public int getCompany_fuelType_DOWN();

    public void setCompany_fuelType_DOWN(int company_fuelType_DOWN);

    public int getOwnTrans_fuelType_UP();

    public void setOwnTrans_fuelType_UP(int ownTrans_fuelType_UP);

    public int getOwnTrans_fuelType_DOWN();

    public void setOwnTrans_fuelType_DOWN(int ownTrans_fuelType_DOWN);


    public boolean isPaidByCom();


    public void setPaidByCom(boolean paidByCom);


    public int getNoOfWorkingDays();


    public void setNoOfWorkingDays(int noOfWorkingDays);

    public int isDeleted();

    public void setDeleted(int deleted);

    public boolean isTwoWay();

    public void setTwoWay(boolean twoWay);

    public float getCompanyFuel_fuelEconomy_DOWN();

    public void setCompanyFuel_fuelEconomy_DOWN(float companyFuel_fuelEconomy_DOWN);


    public HashMap<String, String> getEmissionDetails();


    public void setEmissionDetails(HashMap<String, String> emissionDetails);

    public String getCompany();

    public void setCompany(String company);

    public String getBranch();

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon);

    public String getTwoWay();

    public void setTwoWay(String twoWay);

    public String getPaid();

    public void setPaid(String paid);


    public float getCompanyPetrolLiters();

    public void setCompanyPetrolLiters(float companyPetrolLiters);

    public float getCompanyDieselLiters();

    public void setCompanyDieselLiters(float companyDieselLiters) ;

    public float getOwnPetrolLiters() ;

    public void setOwnPetrolLiters(float ownPetrolLiters);

    public float getOwnDieselLiters();

    public void setOwnDieselLiters(float ownDieselLiters);

}
