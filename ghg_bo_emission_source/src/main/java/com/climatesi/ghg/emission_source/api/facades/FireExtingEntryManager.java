package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.FireExtingEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface FireExtingEntryManager extends DataEntryManager<FireExtingEntry, Integer> {

    public List<FireExtingEntry> getCustomFiltered(int branchId, int companyId, String fy);

}
