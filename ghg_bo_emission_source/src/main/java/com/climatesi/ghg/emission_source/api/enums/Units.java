package com.climatesi.ghg.emission_source.api.enums;

public enum
Units {
    m3(1),
    kg(2),
    kWh(3),
    liters(4),
    tons(5),
    LKR(6);

    public final int unit;

    private Units(int unit) {
        this.unit = unit;
    }
}
