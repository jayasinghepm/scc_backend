package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.PaidManagerVehicles;
import com.climatesi.ghg.emission_source.api.beans.PaidManagerVehicles;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.PaidManagerVehiclesEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.PaidManagerVehicleEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.PaidManagerVehicleEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.PaidManagerEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class PaidManagerVehicleEntryManagerFacade  implements PaidManagerVehiclesEntryManager {

    private Logger logger = Logger.getLogger(PaidManagerVehicleEntryManagerFacade.class);
    private PaidManagerEntryManagerDAO dao;

    @Override
    public Object addEntity(PaidManagerVehicles entity) {
        try {
            return dao.insert((PaidManagerVehicleEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding PaidManagerVehicles", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(PaidManagerVehicles entity, int userID) {
        try {
            return dao.insert((PaidManagerVehicleEntryBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding PaidManagerVehicles", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(PaidManagerVehicles entity) {
        try {
            return dao.update((PaidManagerVehicleEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  PaidManagerVehicles ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(PaidManagerVehicles entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(PaidManagerVehicles entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(PaidManagerVehicles entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(PaidManagerVehicles entity) {
        return null;
    }

    @Override
    public PaidManagerVehicles getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<PaidManagerVehicles> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<PaidManagerVehicles> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public PaidManagerVehicles getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<PaidManagerVehicles> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<PaidManagerVehicles> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new PaidManagerEntryManagerDAO(entityManager);
    }

    @Override
    public List<PaidManagerVehicles> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

