package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.climatesi.ghg.emission_source.api.facades.ElectricityEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.ElectricityEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.ElectricityEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class ElectricityEntryManagerFacade implements ElectricityEntryManager {

    private Logger logger = Logger.getLogger(ElectricityEntryManagerFacade.class);
    private ElectricityEntryManagerDAO electricityEntryManagerDAO;

    @Override
    public Object addEntity(ElectricityEntry entity) {
        try {
            return electricityEntryManagerDAO.insert((ElectricityEntryBean)entity);
        } catch (GHGException e) {
            logger.error("Error in adding ElectricityEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(ElectricityEntry entity, int userID) {
       try {
           return electricityEntryManagerDAO.insert((ElectricityEntryBean)entity, userID);
       } catch (Exception e) {
           logger.error("Error in adding ElectricityEntry", e);
           return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
       }
    }

    @Override
    public Object updateEntity(ElectricityEntry entity) {
        try {
            return electricityEntryManagerDAO.update((ElectricityEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  ElectricityEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(ElectricityEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(ElectricityEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(ElectricityEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(ElectricityEntry entity) {
        return null;
    }

    @Override
    public ElectricityEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<ElectricityEntry> getAllEntityList() {
        return electricityEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<ElectricityEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return electricityEntryManagerDAO.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public ElectricityEntry getEntityByKey(Integer id) {
        return electricityEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<ElectricityEntry> getEntityListByFilter(Object filterCriteria) {
        return electricityEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<ElectricityEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return electricityEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        electricityEntryManagerDAO = new ElectricityEntryManagerDAO(entityManager);
    }

    @Override
    public List<ElectricityEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return electricityEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
