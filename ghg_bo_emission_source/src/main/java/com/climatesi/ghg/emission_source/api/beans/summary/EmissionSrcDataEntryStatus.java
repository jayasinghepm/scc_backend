package com.climatesi.ghg.emission_source.api.beans.summary;

import com.climatesi.ghg.emission_source.implGeneral.beans.id.ProjectDataEntryStatusIdBean;

public interface EmissionSrcDataEntryStatus {

    public ProjectDataEntryStatusIdBean getId();

    public void setId(ProjectDataEntryStatusIdBean id);

    public int getStatus();

    public void setStatus(int status);

}
