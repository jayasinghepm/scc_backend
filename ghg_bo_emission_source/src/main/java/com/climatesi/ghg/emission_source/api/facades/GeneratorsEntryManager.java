package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.GeneratorsEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface GeneratorsEntryManager extends DataEntryManager<GeneratorsEntry, Integer> {

    public List<GeneratorsEntry> getCustomFiltered(int branchId, int companyId, String fy);
}
