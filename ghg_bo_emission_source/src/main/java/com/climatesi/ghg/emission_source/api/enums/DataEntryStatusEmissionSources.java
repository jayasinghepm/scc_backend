package com.climatesi.ghg.emission_source.api.enums;

public enum DataEntryStatusEmissionSources {
    ELEC(1),
    MUN_WATER(2),
    REFRI(3),
    FIRE_EXT(4),
    WASTE_TRANS(5),
    COM_OWNED(6),
    EMP_COMM(7),
    AIR_TRAVEL(8),
    RENTED(9),
    HIRED(10),
    GENERATOR(11),
    WASTE_DIS(12),
    TRANS_LOC_PUR(13);

    public final  int src;
    private DataEntryStatusEmissionSources(int src) {this.src = src;}

}
