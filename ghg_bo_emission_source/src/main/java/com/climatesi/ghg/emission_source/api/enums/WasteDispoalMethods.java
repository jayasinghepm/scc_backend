package com.climatesi.ghg.emission_source.api.enums;

public enum WasteDispoalMethods {

    Reuse(1),
    OpenLoop(2),
    ClosedLoop(3),
    Combustion(4),
    Composting(5),
    LandFill(6),
    Anaerobic_Digestion(7),
    Piggery(8),
    Incineration(9);


    public final int method;

    private WasteDispoalMethods(int method) {
        this.method = method;
    }
}
