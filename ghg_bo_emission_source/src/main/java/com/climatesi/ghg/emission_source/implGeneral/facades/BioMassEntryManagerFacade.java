package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.LPGEntryEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.BioMassEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.LPGEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class BioMassEntryManagerFacade implements BioMassEntryManager {

    private Logger logger = Logger.getLogger(BioMassEntryManagerFacade.class);
    private BioMassEntryMangerDAO dao;

    @Override
    public Object addEntity(BioMassEntry entity) {
        try {
            return dao.insert((BioMassEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding BioMassEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(BioMassEntry entity, int userID) {
        try {
            return dao.insert((BioMassEntryBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding BioMassEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(BioMassEntry entity) {
        try {
            return dao.update((BioMassEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  BioMassEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(BioMassEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(BioMassEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(BioMassEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(BioMassEntry entity) {
        return null;
    }

    @Override
    public BioMassEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<BioMassEntry> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<BioMassEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public BioMassEntry getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<BioMassEntry> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<BioMassEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new BioMassEntryMangerDAO(entityManager);
    }

    @Override
    public List<BioMassEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

