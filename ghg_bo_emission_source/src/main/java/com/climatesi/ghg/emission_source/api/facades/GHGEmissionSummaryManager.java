package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.id.GHGEmissionSummaryId;
import com.climatesi.ghg.emission_source.api.beans.summary.GHGEmsissionSummary;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface GHGEmissionSummaryManager extends DataEntryManager<GHGEmsissionSummary, GHGEmissionSummaryId> {

    int delete(int comId, String fy);

     GHGEmsissionSummary find(int companyId, String year, int modeOfEntry);
}
