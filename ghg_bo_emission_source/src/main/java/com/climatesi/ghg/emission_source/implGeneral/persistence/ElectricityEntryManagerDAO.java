package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.ElectricityEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ElectricityEntryManagerDAO extends AbstractDAO<ElectricityEntryBean, ElectricityEntry> {
    private static Logger logger = Logger.getLogger(ElectricityEntryManagerDAO.class.getName());
    private EntityManager em;

    public ElectricityEntryManagerDAO(EntityManager em) {
        super(em, ElectricityEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_ELECTRICITY_ENTRY", "ELECTRICITY_ENTRY_ID", "ElectricityEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public ElectricityEntry updateElectricityEntry(ElectricityEntry entry) {
        logger.info("updateElectricityEntry(" + entry + ")");
        ElectricityEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(ElectricityEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(ElectricityEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(ElectricityEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<ElectricityEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from ElectricityEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", ElectricityEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from ElectricityEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", ElectricityEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<ElectricityEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<ElectricityEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
