package com.climatesi.ghg.emission_source.api.enums;

public enum VehicleCategory {

    COMPANY_OWNED(1),
    RENTED(2),
    HIRED(3);

    public final int category;

    private VehicleCategory(int category) {
        this.category = category;
    }
}
