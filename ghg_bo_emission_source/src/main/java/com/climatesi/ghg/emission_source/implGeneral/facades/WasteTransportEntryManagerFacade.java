package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.WasteTransportEntry;
import com.climatesi.ghg.emission_source.api.facades.WasteTransportEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.WasteTransportEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.WasteTransportEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class WasteTransportEntryManagerFacade implements WasteTransportEntryManager {

    private static Logger logger = Logger.getLogger(WasteTransportEntryManagerFacade.class);
    private static WasteTransportEntryManagerDAO wasteTransportEntryManagerDAO;

    @Override
    public Object addEntity(WasteTransportEntry entity) {
        try {
            return wasteTransportEntryManagerDAO.insert((WasteTransportEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding wasteTransportEntryManage ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(WasteTransportEntry entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(WasteTransportEntry entity) {
        try {
            return wasteTransportEntryManagerDAO.update((WasteTransportEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating wasteTransportEntryManage ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(WasteTransportEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(WasteTransportEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(WasteTransportEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(WasteTransportEntry entity) {
        return null;
    }

    @Override
    public WasteTransportEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<WasteTransportEntry> getAllEntityList() {
        return wasteTransportEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<WasteTransportEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return wasteTransportEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public WasteTransportEntry getEntityByKey(Integer id) {
        return wasteTransportEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<WasteTransportEntry> getEntityListByFilter(Object filterCriteria) {
        return wasteTransportEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<WasteTransportEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return wasteTransportEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        wasteTransportEntryManagerDAO = new WasteTransportEntryManagerDAO(entityManager);
    }

    @Override
    public List<WasteTransportEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return wasteTransportEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
