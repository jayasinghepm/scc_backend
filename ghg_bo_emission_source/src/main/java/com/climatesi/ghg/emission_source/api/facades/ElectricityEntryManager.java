package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface ElectricityEntryManager extends DataEntryManager<ElectricityEntry, Integer> {

    public List<ElectricityEntry> getCustomFiltered(int branchId, int companyId, String fy);
}
