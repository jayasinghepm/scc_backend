package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.OilAndGasTransportation;
import com.climatesi.ghg.emission_source.api.beans.OilAndGasTransportation;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.OilAndGasTransportationEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.OilAndGasTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.OilAndGasTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.OilAndGasTransportationEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class OilAndGasTransportationEntryManagerFacade  implements OilAndGasTransportationEntryManager {

    private Logger logger = Logger.getLogger(OilAndGasTransportationEntryManagerFacade.class);
    private OilAndGasTransportationEntryManagerDAO dao;

    @Override
    public Object addEntity(OilAndGasTransportation entity) {
        try {
            return dao.insert((OilAndGasTransportationBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding OilAndGasTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(OilAndGasTransportation entity, int userID) {
        try {
            return dao.insert((OilAndGasTransportationBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding OilAndGasTransportation", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(OilAndGasTransportation entity) {
        try {
            return dao.update((OilAndGasTransportationBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  OilAndGasTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(OilAndGasTransportation entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(OilAndGasTransportation entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(OilAndGasTransportation entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(OilAndGasTransportation entity) {
        return null;
    }

    @Override
    public OilAndGasTransportation getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<OilAndGasTransportation> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<OilAndGasTransportation> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public OilAndGasTransportation getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<OilAndGasTransportation> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<OilAndGasTransportation> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new OilAndGasTransportationEntryManagerDAO(entityManager);
    }

    @Override
    public List<OilAndGasTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

