package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.SludgeTransportation ;
import com.climatesi.ghg.emission_source.api.beans.SludgeTransportation;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.SludgeTransportationManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.SludeTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.SludeTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.SludgeTransporationEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class SludgeTransportationEntryManagerFacade  implements SludgeTransportationManager {

    private Logger logger = Logger.getLogger(SludgeTransportationEntryManagerFacade.class);
    private SludgeTransporationEntryManagerDAO dao;

    @Override
    public Object addEntity(SludgeTransportation entity) {
        try {
            return dao.insert((SludeTransportationBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding SludgeTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(SludgeTransportation  entity, int userID) {
        try {
            return dao.insert((SludeTransportationBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding SludgeTransportation ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(SludgeTransportation  entity) {
        try {
            return dao.update((SludeTransportationBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  SludgeTransportation  ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(SludgeTransportation  entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(SludgeTransportation  entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(SludgeTransportation  entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(SludgeTransportation  entity) {
        return null;
    }

    @Override
    public SludgeTransportation  getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<SludgeTransportation > getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<SludgeTransportation > getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public SludgeTransportation  getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<SludgeTransportation > getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<SludgeTransportation > getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new SludgeTransporationEntryManagerDAO(entityManager);
    }

    @Override
    public List<SludgeTransportation > getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}


