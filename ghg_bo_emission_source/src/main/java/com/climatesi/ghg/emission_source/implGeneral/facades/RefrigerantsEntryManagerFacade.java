package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.RefrigerantsEntry;
import com.climatesi.ghg.emission_source.api.facades.RefrigerantsEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.RefrigerantsEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.RefrigerantsEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class RefrigerantsEntryManagerFacade implements RefrigerantsEntryManager {

    private static Logger logger = Logger.getLogger(RefrigerantsEntryManagerFacade.class);
    private static RefrigerantsEntryManagerDAO refrigerantsEntryManagerDAO;

    @Override
    public Object addEntity(RefrigerantsEntry entity) {
        try {
            return refrigerantsEntryManagerDAO.insert((RefrigerantsEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding refrigerantsEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(RefrigerantsEntry entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(RefrigerantsEntry entity) {
        try {
            return refrigerantsEntryManagerDAO.update((RefrigerantsEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating refrigerantsEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(RefrigerantsEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(RefrigerantsEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(RefrigerantsEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(RefrigerantsEntry entity) {
        return null;
    }

    @Override
    public RefrigerantsEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<RefrigerantsEntry> getAllEntityList() {
        return refrigerantsEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<RefrigerantsEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return refrigerantsEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public RefrigerantsEntry getEntityByKey(Integer id) {
        return refrigerantsEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<RefrigerantsEntry> getEntityListByFilter(Object filterCriteria) {
        return refrigerantsEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<RefrigerantsEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return refrigerantsEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        refrigerantsEntryManagerDAO = new RefrigerantsEntryManagerDAO(entityManager);
    }

    @Override
    public List<RefrigerantsEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return refrigerantsEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
