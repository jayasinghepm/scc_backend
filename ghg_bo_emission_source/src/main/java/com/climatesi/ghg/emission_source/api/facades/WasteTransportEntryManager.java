package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.WasteTransportEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface WasteTransportEntryManager extends DataEntryManager<WasteTransportEntry, Integer> {

    public List<WasteTransportEntry> getCustomFiltered(int branchId, int companyId, String fy);
}
