package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.EmissionSource;

import javax.persistence.*;

@Entity
@Table(name = "EMISSION_SRC")
@NamedQueries({
        @NamedQuery(name = "EmissionSrcBean.findById",
                query = "SELECT i FROM EmissionSrcBean i WHERE i.emissionSrcId = :emissionSrcId"),
})
public class EmissionSrcBean implements EmissionSource {

    @Id
    @GeneratedValue(generator = "emissionSrcIdSeq")
    @SequenceGenerator(name = "emissionSrcIdSeq", sequenceName = "EMISSION_SRC_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "EMISSION_NAME")
    private String emissionSrcName;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public String getEmissionSrcName() {
        return this.emissionSrcName;
    }

    @Override
    public void setEmissionSrcName(String srcName) {
        this.emissionSrcName = srcName;
    }

    @Override
    public String toString() {
        return "EmissionSrcBean{" +
                "emissionSrcId=" + emissionSrcId +
                ", emissionSrcName='" + emissionSrcName + '\'' +
                '}';
    }


}
