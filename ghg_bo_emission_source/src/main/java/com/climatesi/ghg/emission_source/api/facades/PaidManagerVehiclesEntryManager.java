package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.emission_source.api.beans.PaidManagerVehicles;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface PaidManagerVehiclesEntryManager extends DataEntryManager<PaidManagerVehicles, Integer> {

    public List<PaidManagerVehicles> getCustomFiltered(int branchId, int companyId, String fy);
}

