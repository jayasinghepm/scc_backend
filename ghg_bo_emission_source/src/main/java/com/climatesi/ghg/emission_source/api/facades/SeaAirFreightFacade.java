package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.SeaAirFreightEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface SeaAirFreightFacade extends DataEntryManager<SeaAirFreightEntry, Integer> {

public List<SeaAirFreightEntry> getCustomFiltered(int branchId, int companyId, String fy);

        }
