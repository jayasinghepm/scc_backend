package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.PaperWaste ;
import com.climatesi.ghg.emission_source.api.beans.PaperWaste;
import com.climatesi.ghg.emission_source.api.facades.BioMassEntryManager;
import com.climatesi.ghg.emission_source.api.facades.PaperWasteManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.PaperWasteEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.PaperWasteEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.BioMassEntryMangerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.PaperWasteEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class PaperWasteEntryManagerFacade  implements PaperWasteManager {

    private Logger logger = Logger.getLogger(PaperWasteEntryManagerFacade.class);
    private PaperWasteEntryManagerDAO dao;

    @Override
    public Object addEntity(PaperWaste entity) {
        try {
            return dao.insert((PaperWasteEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding PaperWaste ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(PaperWaste  entity, int userID) {
        try {
            return dao.insert((PaperWasteEntryBean)entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding PaperWaste ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(PaperWaste  entity) {
        try {
            return dao.update((PaperWasteEntryBean)entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  PaperWaste  ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(PaperWaste  entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(PaperWaste  entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(PaperWaste  entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(PaperWaste  entity) {
        return null;
    }

    @Override
    public PaperWaste  getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<PaperWaste > getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<PaperWaste > getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public PaperWaste  getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<PaperWaste > getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<PaperWaste > getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new PaperWasteEntryManagerDAO(entityManager);
    }

    @Override
    public List<PaperWaste > getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }
}

