package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.ElectricityMeterReadingEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface ElectricityMeterReadingEntryManager extends DataEntryManager<ElectricityMeterReadingEntry, Integer> {
}
