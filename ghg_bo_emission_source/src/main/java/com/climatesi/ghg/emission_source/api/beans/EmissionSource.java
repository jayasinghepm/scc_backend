package com.climatesi.ghg.emission_source.api.beans;

public interface EmissionSource {

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int id);

    public String getEmissionSrcName();

    public void setEmissionSrcName(String srcName);

}
