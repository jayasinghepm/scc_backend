package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.AshTransportation;
import com.climatesi.ghg.emission_source.api.beans.BioMassEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface AshTransportationEntryManager extends DataEntryManager<AshTransportation, Integer> {

    public List<AshTransportation> getCustomFiltered(int branchId, int companyId, String fy);
}

