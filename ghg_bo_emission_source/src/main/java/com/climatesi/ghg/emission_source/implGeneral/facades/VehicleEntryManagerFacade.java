package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.VehicleEntry;
import com.climatesi.ghg.emission_source.api.facades.VehicleEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.VehicleEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.VehicleEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class VehicleEntryManagerFacade implements VehicleEntryManager {

    private static Logger logger = Logger.getLogger(VehicleEntryManagerFacade.class);
    private VehicleEntryManagerDAO vehicleEntryManagerDAO;

    @Override
    public Object addEntity(VehicleEntry entity) {
        try {
            return vehicleEntryManagerDAO.insert((VehicleEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding  vehicleEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;

        }
    }

    @Override
    public Object addEntity(VehicleEntry entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(VehicleEntry entity) {
        try {
            return vehicleEntryManagerDAO.update((VehicleEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating  vehicleEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;

        }
    }

    @Override
    public Object updateEntity(VehicleEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(VehicleEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(VehicleEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(VehicleEntry entity) {
        return null;
    }

    @Override
    public VehicleEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<VehicleEntry> getAllEntityList() {

        return vehicleEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<VehicleEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {

        return vehicleEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public VehicleEntry getEntityByKey(Integer id) {
        return vehicleEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<VehicleEntry> getEntityListByFilter(Object filterCriteria) {
        return vehicleEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<VehicleEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return vehicleEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        vehicleEntryManagerDAO = new VehicleEntryManagerDAO(entityManager);
    }

    @Override
    public List<VehicleEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return vehicleEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
