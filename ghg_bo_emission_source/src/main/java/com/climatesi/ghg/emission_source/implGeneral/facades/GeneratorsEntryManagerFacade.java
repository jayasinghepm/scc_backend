package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.GeneratorsEntry;
import com.climatesi.ghg.emission_source.api.facades.GeneratorsEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.GeneratorsEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.GeneratorsEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class GeneratorsEntryManagerFacade implements GeneratorsEntryManager {

    private Logger logger = Logger.getLogger(GeneratorsEntryManagerFacade.class);
    private GeneratorsEntryManagerDAO generatorsEntryManagerDAO;

    @Override
    public Object addEntity(GeneratorsEntry entity) {
        try {
            return generatorsEntryManagerDAO.insert((GeneratorsEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding generatorsEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(GeneratorsEntry entity, int userID) {
        try {
            return generatorsEntryManagerDAO.insert((GeneratorsEntryBean) entity, userID);
        } catch (GHGException e) {
            logger.error("Error in adding generatorsEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(GeneratorsEntry entity) {
        try {
            return generatorsEntryManagerDAO.update((GeneratorsEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating generatorsEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(GeneratorsEntry entity, int userID) {
        try {
            return generatorsEntryManagerDAO.update((GeneratorsEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating generatorsEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public boolean setStatusOfEntity(GeneratorsEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(GeneratorsEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(GeneratorsEntry entity) {
        return null;
    }

    @Override
    public GeneratorsEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<GeneratorsEntry> getAllEntityList() {
        return generatorsEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<GeneratorsEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return generatorsEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public GeneratorsEntry getEntityByKey(Integer id) {
        return generatorsEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<GeneratorsEntry> getEntityListByFilter(Object filterCriteria) {
        return generatorsEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<GeneratorsEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return generatorsEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        generatorsEntryManagerDAO = new GeneratorsEntryManagerDAO(entityManager);
    }

    @Override
    public List<GeneratorsEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return generatorsEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
