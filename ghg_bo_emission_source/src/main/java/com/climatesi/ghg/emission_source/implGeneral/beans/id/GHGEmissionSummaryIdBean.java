package com.climatesi.ghg.emission_source.implGeneral.beans.id;

import com.climatesi.ghg.emission_source.api.beans.id.GHGEmissionSummaryId;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class GHGEmissionSummaryIdBean implements GHGEmissionSummaryId {

    @Column(name = "COMPANY_ID")
    private int companyId;


    @Column(name = "FINANCIAL_YEAR")
    private String fy;


    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }



    @Override
    public String getFY() {
        return this.fy;
    }

    @Override
    public void setFY(String fy) {
        this.fy = fy;
    }
}
