package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.FurnaceOil;
import com.climatesi.ghg.emission_source.api.beans.FurnaceOil;
import com.climatesi.ghg.emission_source.implGeneral.beans.FurnaceOilEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.FurnaceOilEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FurnaceEntryManagerDAO extends AbstractDAO<FurnaceOilEntryBean, FurnaceOil> {
    private static Logger logger = Logger.getLogger(FurnaceEntryManagerDAO.class.getName());
    private EntityManager em;

    public FurnaceEntryManagerDAO(EntityManager em) {
        super(em, FurnaceOilEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_ELECTRICITY_ENTRY", "ELECTRICITY_ENTRY_ID", "FurnaceOilEntryBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public FurnaceOil update(FurnaceOil entry) {
        logger.info("updateEntry(" + entry + ")");
        FurnaceOil updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(FurnaceOilEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(FurnaceOilEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(FurnaceOilEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<FurnaceOil> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from FurnaceOilEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", FurnaceOilEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from FurnaceOilEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", FurnaceOilEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<FurnaceOil> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<FurnaceOil>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}

