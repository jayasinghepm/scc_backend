package com.climatesi.ghg.emission_source.api.facades;

import com.climatesi.ghg.emission_source.api.beans.BGASEntry;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import java.util.List;

public interface BGASEntryEntryManager extends DataEntryManager<BGASEntry, Integer> {

    public List<BGASEntry> getCustomFiltered(int branchId, int companyId, String fy);
}
