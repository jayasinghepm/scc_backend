package com.climatesi.ghg.emission_source.api.enums;

public enum EmissionSummaryModeofEntry {
    Manual(1),
    Auto(2);

    public final int mode;

    private EmissionSummaryModeofEntry(int mode) {
        this.mode = mode;
    }
}

