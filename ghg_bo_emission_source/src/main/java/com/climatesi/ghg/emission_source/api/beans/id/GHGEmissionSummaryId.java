package com.climatesi.ghg.emission_source.api.beans.id;

import java.io.Serializable;

public interface GHGEmissionSummaryId extends Serializable {


    public int getCompanyId();
    public void setCompanyId(int id);

    public String getFY();

    public void setFY(String fy);
}
