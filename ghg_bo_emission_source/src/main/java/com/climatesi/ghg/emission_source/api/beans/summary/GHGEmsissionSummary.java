package com.climatesi.ghg.emission_source.api.beans.summary;

import com.climatesi.ghg.emission_source.implGeneral.beans.id.GHGEmissionSummaryIdBean;

import java.util.Date;
import java.util.HashMap;

public interface GHGEmsissionSummary {

    public GHGEmissionSummaryIdBean getId();

    public void setId(GHGEmissionSummaryIdBean id);

    public HashMap<String, String> getEmission();

    public void setEmission(HashMap<String, String> emission);


    public Date getAddedDate();


    public void setAddedDate(Date addedDate);


    public int getIsDeleted();


    public void setIsDeleted(int isDeleted);

    public int getModeOfEntry();

    public void setModeOfEntry(int modeOfEntry);
}
