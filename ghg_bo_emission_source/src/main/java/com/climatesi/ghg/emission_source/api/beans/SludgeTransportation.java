package com.climatesi.ghg.emission_source.api.beans;

import java.util.Date;
import java.util.HashMap;

public interface SludgeTransportation {


    public int getEntryId();

    public void setEntryId(int entryId);

    public int getEmissionSrcId();

    public void setEmissionSrcId(int emissionSrcId);

    public int getAddedBy();

    public void setAddedBy(int addedBy);

    public int getBranchId();

    public void setBranchId(int branchId);

    public int getCompanyId();

    public void setCompanyId(int companyId);

    public Date getAddedDate();

    public void setAddedDate(Date addedDate);

    public Date getLastUpdatedDate();

    public void setLastUpdatedDate(Date lastUpdatedDate);

    public int getMonth();

    public void setMonth(int month);

    public String getYear();
        
    public void setYear(String year);

    public float getDistance();

    public void setDistance(float distance);

    public float getFuelEconomy();

    public void setFuelEconomy(float fuelEconomy);

    public float getNoOfTrips();

    public void setNoOfTrips(float noOfTrips) ;

    public int getFuleType();

    public void setFuleType(int fuleType);

    public HashMap<String, String> getEmissionDetails();

    public void setEmissionDetails(HashMap<String, String> emissionDetails);


    public int getIsDeleted();

    public void setIsDeleted(int isDeleted);

    public String getCompany();

    public void setCompany(String company);

    public String getBranch();

    public void setBranch(String branch);

    public String getMon();

    public void setMon(String mon);


    public float getFuelConsumption();

    public void setFuelConsumption(float fuelConsumption);



}
