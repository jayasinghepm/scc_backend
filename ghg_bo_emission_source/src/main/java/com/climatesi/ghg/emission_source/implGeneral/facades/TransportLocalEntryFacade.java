package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.TransportLocalEntry;
import com.climatesi.ghg.emission_source.api.facades.TransportLocalEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.TransportLocalEntryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.TransportLocalEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class TransportLocalEntryFacade implements TransportLocalEntryManager {

    private static Logger logger = Logger.getLogger(TransportLocalEntryFacade.class);
    private TransportLocalEntryManagerDAO transportLocalEntryManagerDAO;

    @Override
    public Object addEntity(TransportLocalEntry entity) {
        try {
            return transportLocalEntryManagerDAO.insert((TransportLocalEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding transportLocalEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;

        }
    }

    @Override
    public Object addEntity(TransportLocalEntry entity, int userID) {
        try {
            return transportLocalEntryManagerDAO.insert((TransportLocalEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding transportLocalEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;

        }
    }

    @Override
    public Object updateEntity(TransportLocalEntry entity) {
        try {
            return transportLocalEntryManagerDAO.update((TransportLocalEntryBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating transportLocalEntry", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;

        }
    }

    @Override
    public Object updateEntity(TransportLocalEntry entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(TransportLocalEntry entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(TransportLocalEntry entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(TransportLocalEntry entity) {
        return null;
    }

    @Override
    public TransportLocalEntry getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<TransportLocalEntry> getAllEntityList() {
        return transportLocalEntryManagerDAO.findAll();
    }

    @Override
    public ListResult<TransportLocalEntry> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return transportLocalEntryManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public TransportLocalEntry getEntityByKey(Integer id) {
        return transportLocalEntryManagerDAO.findByKey(id);
    }

    @Override
    public ListResult<TransportLocalEntry> getEntityListByFilter(Object filterCriteria) {

        return transportLocalEntryManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<TransportLocalEntry> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return transportLocalEntryManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        transportLocalEntryManagerDAO = new TransportLocalEntryManagerDAO(entityManager);
    }

    @Override
    public List<TransportLocalEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        return transportLocalEntryManagerDAO.getCustomFiltered(branchId, companyId, fy);
    }
}
