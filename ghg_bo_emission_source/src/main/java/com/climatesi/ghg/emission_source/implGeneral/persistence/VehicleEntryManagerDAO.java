package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.VehicleEntry;
import com.climatesi.ghg.emission_source.implGeneral.beans.VehicleEntryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VehicleEntryManagerDAO extends AbstractDAO<VehicleEntryBean, VehicleEntry> {

    private Logger logger = Logger.getLogger(VehicleEntryManagerDAO.class);
    private EntityManager em;

    public VehicleEntryManagerDAO(EntityManager em) {
        super(em, VehicleEntryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_VEHICLE_ENTRY", "VEHICLE_ENTRY_ID", "VehicleEntryBean");
        meta.setKeyPropertyName("entryId");
        super.setDaoMeta(meta);
    }

    public VehicleEntry updateVehicleEntry(VehicleEntry entry) {
        logger.info("updateVehicleEntry(" + entry + ")");
        VehicleEntry updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(VehicleEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(VehicleEntryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(VehicleEntryBean entityImpl) throws GHGException {
        return null;
    }

    public List<VehicleEntry> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from VehicleEntryBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted)", VehicleEntryBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from VehicleEntryBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", VehicleEntryBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<VehicleEntry> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<VehicleEntry>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
