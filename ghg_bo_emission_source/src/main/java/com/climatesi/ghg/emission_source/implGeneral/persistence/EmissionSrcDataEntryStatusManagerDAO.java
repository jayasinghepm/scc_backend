package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.summary.EmissionSrcDataEntryStatus;
import com.climatesi.ghg.emission_source.implGeneral.beans.summary.EmissionSrcDataEntryStatusBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

public class EmissionSrcDataEntryStatusManagerDAO extends AbstractDAO<EmissionSrcDataEntryStatusBean, EmissionSrcDataEntryStatus> {
    private static Logger logger = Logger.getLogger(EmissionSrcDataEntryStatusManagerDAO.class);
    private EntityManager em;


    public EmissionSrcDataEntryStatusManagerDAO(EntityManager em) {
        super(em, EmissionSrcDataEntryStatusBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("SUMM_INPUTS_STATUS", "id", "EmissionSrcDataEntryStatusBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public int deleteDataEntryStatus(int comId, String fy) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from EmissionSrcDataEntryStatusBean l where l.id.companyId = :comId and l.id.fy = :fy");
            query.setParameter("comId", comId);
            query.setParameter("fy", fy);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return comId;
    }

    @Override
    public Map<String, Object> getAddSpParams(EmissionSrcDataEntryStatusBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(EmissionSrcDataEntryStatusBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(EmissionSrcDataEntryStatusBean entityImpl) throws GHGException {
        return null;
    }
}
