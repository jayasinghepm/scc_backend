package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.emission_source.api.beans.VehicleOthers;
import com.climatesi.ghg.emission_source.api.facades.AshTransportationEntryManager;
import com.climatesi.ghg.emission_source.api.facades.VehicleOthersEntryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.VehicleOthersBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.VehicleOthersBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.AshTransportationEntryManagerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.VehicleEntryManagerDAO;
import com.climatesi.ghg.emission_source.implGeneral.persistence.VehicleOthersEntryManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.List;

public class VehicleOthersEntryManagerFacade implements VehicleOthersEntryManager {

    private Logger logger = Logger.getLogger(VehicleOthersEntryManagerFacade.class);
    private VehicleOthersEntryManagerDAO dao;

    @Override
    public Object addEntity(VehicleOthers entity) {
        try {
            return dao.insert((VehicleOthersBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding VehicleOthers", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(VehicleOthers entity, int userID) {
        try {
            return dao.insert((VehicleOthersBean) entity, userID);
        } catch (Exception e) {
            logger.error("Error in adding VehicleOthers", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(VehicleOthers entity) {
        try {
            return dao.update((VehicleOthersBean) entity, PersistMode.JPA_PERSIST);
        } catch (GHGException e) {
            logger.error("Error in updating  VehicleOthers ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(VehicleOthers entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(VehicleOthers entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(VehicleOthers entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(VehicleOthers entity) {
        return null;
    }

    @Override
    public VehicleOthers getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<VehicleOthers> getAllEntityList() {
        return dao.findAll();
    }

    @Override
    public ListResult<VehicleOthers> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return dao.findPaginatedSearchResults(pageNumber, sortingProperty, 0);
    }

    @Override
    public VehicleOthers getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<VehicleOthers> getEntityListByFilter(Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<VehicleOthers> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new VehicleOthersEntryManagerDAO(entityManager);
    }

    @Override
    public List<VehicleOthers> getCustomFiltered(int branchId, int companyId, String fy) {
        return dao.getCustomFiltered(branchId, companyId, fy);
    }


}