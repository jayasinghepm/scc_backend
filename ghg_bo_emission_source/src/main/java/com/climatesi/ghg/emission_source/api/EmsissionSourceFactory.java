package com.climatesi.ghg.emission_source.api;

import com.climatesi.ghg.emission_source.api.facades.*;
import com.climatesi.ghg.emission_source.implGeneral.facades.*;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class EmsissionSourceFactory {

    private static Logger logger = Logger.getLogger(EmsissionSourceFactory.class.getName());

    private static EmsissionSourceFactory emsissionSourceFactory;
    private static BusinessAirTravelEntryManager businessAirTravelEntryManager;
    private static ElectricityEntryManager electricityEntryManager;
    private static EmissionSourceManager emissionSourceManager;
    private static FireExtingEntryManager fireExtingEntryManager;
    private static GeneratorsEntryManager generatorsEntryManager;
    private static MunicipalWaterEntryManager municipalWaterEntryManager;
    private static MunicipalWaterMeterReadingEntryManager municipalWaterMeterReadingEntryManager;
    private static RefrigerantsEntryManager refrigerantsEntryManager;
    private static TransportLocalEntryManager transportLocalEntryManager;
    private static VehicleEntryManager vehicleEntryManager;
    private static WasteDisposalEntryManager wasteDisposalEntryManager;
    private static WasteTransportEntryManager wasteTransportEntryManager;
    private static ElectricityMeterReadingEntryManager electricityMeterReadingEntryManager;
    private static EmployeCommutingEntryManager employeCommutingEntryManager;
    private static GHGEmissionSummaryManager ghgEmissionSummaryManager;
    private static EmissionSrcDataEntryStatusManager emissionSrcDataEntryStatusManager;
    private static SeaAirFreightFacadeManager seaAirFreightFacadeManager;
    private static BioMassEntryManager bioMassEntryManager;
    private static LPGEntryEntryManager lpgEntryEntryManager;
    private static BGASEntryEntryManager bgasEntryEntryManager;//pasindu

    private static AshTransportationEntryManager ashTransportationEntryManager;
    private static ForkliftsEntryManager forkliftsEntryManager;
    private static FurnaceEntryManager furnaceEntryManager;
    private static LorryTransportationManager lorryTransportationManager;
    private static PaidManagerVehiclesEntryManager paidManagerVehiclesEntryManager;
    private static PaperWasteManager paperWasteManager;
    private static OilAndGasTransportationEntryManager oilAndGasTransportationEntryManager;
    private static RawMaterialTransportationLocalManager rawMaterialTransportationLocalManager;
    private static SawDustTransportationManager sawDustTransportationManager;
    private static SludgeTransportationManager sludgeTransportationManager;
    private static StaffTransportationEntryManager staffTransportationEntryManager;
    private static VehicleOthersEntryManager vehicleOthersEntryManager;


    private EmsissionSourceFactory() {

    }

    public static EmsissionSourceFactory getInstance() throws GHGException {
        try {
            if (emsissionSourceFactory == null) {
                emsissionSourceFactory = new EmsissionSourceFactory();
            }
            return emsissionSourceFactory;
        }catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new GHGException("Error in get instance of QuestionFactory", ex);
        }
    }

    public AshTransportationEntryManager getAshTransportationEntryManager(EntityManager em) {
        if (ashTransportationEntryManager == null) {
            ashTransportationEntryManager = new AshTransportationEntryManagerFacade();
            ashTransportationEntryManager.injectEntityManager(em);
        }
        return ashTransportationEntryManager;
    }

    public ForkliftsEntryManager getForkliftsEntryManager(EntityManager em) {
        if (forkliftsEntryManager == null) {
            forkliftsEntryManager = new ForkliftsEntryManagerFacade();
            forkliftsEntryManager.injectEntityManager(em);
        }
        return forkliftsEntryManager;
    }

    public FurnaceEntryManager getFurnaceEntryManager(EntityManager em) {
        if (furnaceEntryManager == null) {
            furnaceEntryManager = new FurnaceEntryManagerFacade();
            furnaceEntryManager.injectEntityManager(em);
        }
        return furnaceEntryManager;
    }

    public LorryTransportationManager getLorryTransportationManager(EntityManager em) {
        if (lorryTransportationManager == null) {
            lorryTransportationManager = new LorryTransportationEntryManagerFacade();
            lorryTransportationManager.injectEntityManager(em);
        }
        return lorryTransportationManager;
    }

    public VehicleOthersEntryManager getVehicleOthersEntryManager(EntityManager em) {
        if (vehicleOthersEntryManager == null) {
            vehicleOthersEntryManager = new VehicleOthersEntryManagerFacade();
            vehicleOthersEntryManager.injectEntityManager(em);
        }
        return vehicleOthersEntryManager;
    }

    public OilAndGasTransportationEntryManager getOilAndGasTransportationEntryManager(EntityManager em) {
        if (oilAndGasTransportationEntryManager == null) {
            oilAndGasTransportationEntryManager = new OilAndGasTransportationEntryManagerFacade();
            oilAndGasTransportationEntryManager.injectEntityManager(em);
        }
        return oilAndGasTransportationEntryManager;
    }

    public PaidManagerVehiclesEntryManager getPaidManagerVehiclesEntryManager(EntityManager em) {
        if (paidManagerVehiclesEntryManager == null) {
            paidManagerVehiclesEntryManager = new PaidManagerVehicleEntryManagerFacade();
            paidManagerVehiclesEntryManager.injectEntityManager(em);
        }
        return paidManagerVehiclesEntryManager;
    }

    public PaperWasteManager getPaperWasteManager(EntityManager em) {
        if (paperWasteManager == null) {
            paperWasteManager = new PaperWasteEntryManagerFacade();
            paperWasteManager.injectEntityManager(em);
        }
        return paperWasteManager;
    }

    public RawMaterialTransportationLocalManager getRawMaterialTransportationLocalManager(EntityManager em) {
        if (rawMaterialTransportationLocalManager == null) {
            rawMaterialTransportationLocalManager = new RawMaterialTransportationLocalEntryManagerFacade();
            rawMaterialTransportationLocalManager.injectEntityManager(em);
        }
        return rawMaterialTransportationLocalManager;
    }

    public SawDustTransportationManager getSawDustTransportationManager(EntityManager em) {
        if (sawDustTransportationManager == null) {
            sawDustTransportationManager = new SawDustTransportationEntryManagerFacade();
            sawDustTransportationManager.injectEntityManager(em);
        }
        return sawDustTransportationManager;
    }

    public SludgeTransportationManager getSludgeTransportationManager(EntityManager em) {
        if (sludgeTransportationManager == null) {
            sludgeTransportationManager = new SludgeTransportationEntryManagerFacade();
            sludgeTransportationManager.injectEntityManager(em);
        }
        return sludgeTransportationManager;
    }



    public StaffTransportationEntryManager getStaffTransportationEntryManager(EntityManager em) {
        if (staffTransportationEntryManager == null) {
            staffTransportationEntryManager = new StaffTransportationEntryManagerFacade();
            staffTransportationEntryManager.injectEntityManager(em);
        }
        return staffTransportationEntryManager;
    }

    public LPGEntryEntryManager getLPGEntryEntryManager(EntityManager em) {
        if (lpgEntryEntryManager == null) {
            lpgEntryEntryManager = new LPGEntryManagerFacade();
            lpgEntryEntryManager.injectEntityManager(em);
        }
        return lpgEntryEntryManager;
    }





    public BioMassEntryManager getBioMassEntryManager(EntityManager em) {
        if (bioMassEntryManager == null) {
            bioMassEntryManager = new BioMassEntryManagerFacade();
            bioMassEntryManager.injectEntityManager(em);
        }
        return bioMassEntryManager;
    }



    public SeaAirFreightFacadeManager getSeaAirFreightFacadeManager(EntityManager em) {
        if (seaAirFreightFacadeManager == null) {
            seaAirFreightFacadeManager = new SeaAirFreightFacadeManager();
            seaAirFreightFacadeManager.injectEntityManager(em);
        }
        return seaAirFreightFacadeManager;
    }

    public BusinessAirTravelEntryManager getBusinessAirTravelEntryManager(EntityManager em) {
        if (businessAirTravelEntryManager == null) {
            businessAirTravelEntryManager = new BusinessAirTravelEntryManagerFacade();
            businessAirTravelEntryManager.injectEntityManager(em);
        }
        return businessAirTravelEntryManager;
    }

    public ElectricityEntryManager getElectricityEntryManager(EntityManager em) {
        if (electricityEntryManager == null) {
            electricityEntryManager = new ElectricityEntryManagerFacade();
            electricityEntryManager.injectEntityManager(em);
        }
        return electricityEntryManager;
    }

    public ElectricityMeterReadingEntryManager getElectricityMeterReadingEntryManager(EntityManager em) {
        if (electricityMeterReadingEntryManager == null) {
            electricityMeterReadingEntryManager = new ElectricityMeterReadingEntryManagerFacade();
            electricityMeterReadingEntryManager.injectEntityManager(em);
        }
        return electricityMeterReadingEntryManager;
    }

    public EmissionSourceManager getEmissionSourceManager(EntityManager em) {
        if (emissionSourceManager == null) {
            emissionSourceManager = new EmissionSourceManagerFacade();
            emissionSourceManager.injectEntityManager(em);
        }
        return emissionSourceManager;
    }

    public FireExtingEntryManager getFireExtingEntryManager(EntityManager em) {
        if (fireExtingEntryManager == null) {
            fireExtingEntryManager = new FireExtingEntryManagerFacade();
            fireExtingEntryManager.injectEntityManager(em);
        }
        return fireExtingEntryManager;
    }

    public GeneratorsEntryManager getGeneratorsEntryManager(EntityManager em) {
        if (generatorsEntryManager == null) {
            generatorsEntryManager = new GeneratorsEntryManagerFacade();
            generatorsEntryManager.injectEntityManager(em);
        }
        return generatorsEntryManager;
    }

    public MunicipalWaterEntryManager getMunicipalWaterEntryManager(EntityManager em) {
        if (municipalWaterEntryManager == null) {
            municipalWaterEntryManager = new MunicipalWaterEntryManagerFacade();
            municipalWaterEntryManager.injectEntityManager(em);
        }
        return municipalWaterEntryManager;
    }

    public MunicipalWaterMeterReadingEntryManager getMunicipalWaterMeterReadingEntryManager(EntityManager em) {
        if (municipalWaterMeterReadingEntryManager == null) {
            municipalWaterMeterReadingEntryManager = new MunicipalWaterMeterReadingEntryManagerFacade();
            municipalWaterMeterReadingEntryManager.injectEntityManager(em);
        }
        return  municipalWaterMeterReadingEntryManager;
    }

    public RefrigerantsEntryManager getRefrigerantsEntryManager(EntityManager em) {
        if (refrigerantsEntryManager == null) {
            refrigerantsEntryManager = new RefrigerantsEntryManagerFacade();
            refrigerantsEntryManager.injectEntityManager(em);
        }
        return refrigerantsEntryManager;
    }

    public TransportLocalEntryManager getTransportLocalEntryManager(EntityManager em) {
        if (transportLocalEntryManager == null) {
            transportLocalEntryManager = new TransportLocalEntryFacade();
            transportLocalEntryManager.injectEntityManager(em);
        }
        return transportLocalEntryManager;
    }

    public VehicleEntryManager getVehicleEntryManager(EntityManager em) {
        if (vehicleEntryManager == null) {
            vehicleEntryManager = new VehicleEntryManagerFacade();
            vehicleEntryManager.injectEntityManager(em);
        }
        return vehicleEntryManager;
    }

    public WasteDisposalEntryManager getWasteDisposalEntryManager(EntityManager em) {
        if (wasteDisposalEntryManager == null) {
            wasteDisposalEntryManager = new WasteDisposalEntryManagerFacade();
            wasteDisposalEntryManager.injectEntityManager(em);
        }
        return wasteDisposalEntryManager;
    }

    public WasteTransportEntryManager getWasteTransportEntryManager(EntityManager em) {
        if (wasteTransportEntryManager == null) {
            wasteTransportEntryManager = new WasteTransportEntryManagerFacade();
            wasteTransportEntryManager.injectEntityManager(em);
        }
        return  wasteTransportEntryManager;
    }

    public EmployeCommutingEntryManager getEmployeeCommEntryManager(EntityManager em) {
        if (employeCommutingEntryManager == null) {
            employeCommutingEntryManager = new EmployeeCommutingManagerFacade();
            employeCommutingEntryManager.injectEntityManager(em);
        }
        return employeCommutingEntryManager;

    }

    public GHGEmissionSummaryManager getGHGEmissionSummaryManager(EntityManager em) {
        if (ghgEmissionSummaryManager == null) {
            ghgEmissionSummaryManager = new GHGEmissionSummaryManagerFacade();
            ghgEmissionSummaryManager.injectEntityManager(em);
        }
        return ghgEmissionSummaryManager;
    }

    public EmissionSrcDataEntryStatusManager getEmissionSrcDataEntryManager(EntityManager em) {
        if (emissionSrcDataEntryStatusManager == null){
            emissionSrcDataEntryStatusManager = new EmissionSrcDataEntryStatusManagerFacade();
            emissionSrcDataEntryStatusManager.injectEntityManager(em);
        }
        return emissionSrcDataEntryStatusManager;
    }

    public BGASEntryEntryManager getBGASEntryEntryManager(EntityManager em) {
        if (bgasEntryEntryManager == null) {
            bgasEntryEntryManager = new BGASEntryManagerFacade();
            bgasEntryEntryManager.injectEntityManager(em);
        }
        return bgasEntryEntryManager;
    }

}

