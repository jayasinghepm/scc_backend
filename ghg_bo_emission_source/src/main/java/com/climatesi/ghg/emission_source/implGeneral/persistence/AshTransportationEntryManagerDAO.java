package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.AshTransportation;
import com.climatesi.ghg.emission_source.api.beans.AshTransportation;
import com.climatesi.ghg.emission_source.implGeneral.beans.AshTransportationBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.AshTransportationBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AshTransportationEntryManagerDAO extends AbstractDAO<AshTransportationBean, AshTransportation> {
    private static Logger logger = Logger.getLogger(AshTransportationEntryManagerDAO.class.getName());
    private EntityManager em;

    public AshTransportationEntryManagerDAO(EntityManager em) {
        super(em, AshTransportationBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_ASH_TRANS_ENTRY", "ENTRY_ID", "AshTransportationBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public AshTransportation update(AshTransportation entry) {
        logger.info("update(" + entry + ")");
        AshTransportation updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(AshTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(AshTransportationBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(AshTransportationBean entityImpl) throws GHGException {
        return null;
    }

    public List<AshTransportation> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from AshTransportationBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted) ", AshTransportationBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from AshTransportationBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", AshTransportationBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<AshTransportation> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<AshTransportation>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}
