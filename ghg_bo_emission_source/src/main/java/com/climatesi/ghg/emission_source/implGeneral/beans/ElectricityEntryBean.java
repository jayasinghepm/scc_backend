package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.emission_source.api.beans.ElectricityEntry;
import com.climatesi.ghg.utility.implGeneral.persistence.HashMapConverter;
import com.climatesi.ghg.utility.implGeneral.persistence.StringListConverter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Entity
@Table(name = "EMISS_ELECTRICITY_ENTRY")
@NamedQueries({
        @NamedQuery(name = "ElectricityEntryBean.findById",
                query = "SELECT i FROM ElectricityEntryBean i WHERE i.entryId = :entryId"),
})
public class ElectricityEntryBean implements ElectricityEntry {
    @Id
    @GeneratedValue(generator = "electricityEntryIdSeq")
    @SequenceGenerator(name = "electricityEntryIdSeq", sequenceName = "ELECTRICITY_ENTRY_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "ELECTRICITY_ENTRY_ID")
    private int entryId;


    @Column(name = "EMISSION_SRC_ID")
    private int emissionSrcId;

    @Column(name = "ADDED_BY")
    private int addedBy;

    @Column(name = "BRANCH_ID")
    private int branchId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "ADDED_DATE")
    private Date addedDate;

    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "MONTH")
    private int month;

    @Column(name= "YEAR")
    private String year;

    @Column(name = "UNITS")
    private int units;

    @Column(name = "ELECTRICITY_CONSUMPTION")
    private float consumption;

    @Column(name = "NUMS_OF_METERS")
    private int numOfMeters;

    @Column(name = "METER_NO")
    private String meterNo;

    @Column(name = "METER_NUMBERS")
    @Convert(converter = StringListConverter.class)
    private List<String> meterNumbers = new ArrayList<>();


    @Lob
    @Column(name = "EMISSION_DETAILS")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, String> emissionDetails;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    private String branch;

    private String mon;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    @Override
    public int getEntryId() {
        return this.entryId ;
    }

    @Override
    public void setEntryId(int id) {
        this.entryId  = id;
    }

    @Override
    public int getEmissionSrcId() {
        return this.emissionSrcId;
    }

    @Override
    public void setEmissionSrcId(int id) {
        this.emissionSrcId = id;
    }

    @Override
    public int getAddedBy() {
        return this.addedBy;
    }

    @Override
    public void setAddedBy(int clientId) {
        this.addedBy = clientId;
    }

    @Override
    public int getBranchId() {
        return this.branchId;
    }

    @Override
    public void setBranchId(int id) {
        this.branchId = id;
    }

    @Override
    public int getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(int id) {
        this.companyId = id;
    }


    @Override
    public Date getEntryAddedDate() {
        return this.addedDate;
    }

    @Override
    public void setEntryAddedDate(Date date) {
        this.addedDate = date;
    }

    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate ;
    }

    @Override
    public void setLastUpdatedDate(Date date) {
        this.lastUpdatedDate = date;
    }

    @Override
    public int getMonth() {
        return this.month;
    }

    @Override
    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String getYear() {
        return this.year;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }


    @Override
    public int getNumOfMeters() {
        return this.numOfMeters;
    }

    @Override
    public void setNumOfMeters(int meters) {
        this.numOfMeters = meters;
    }

    @Override
    public List<String> getMeterNumbers() {
        return this.meterNumbers;
    }

    @Override
    public void setMeterNumbers(List<String> meterNumbers) {
        this.meterNumbers = meterNumbers;
    }

    @Override
    public float getConsumption() {
        return this.consumption;
    }

    @Override
    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }

    @Override
    public int getUnits() {
        return this.units;
    }

    @Override
    public void setUnits(int units) {
        this.units = units;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public HashMap<String, String> getEmissionDetails() {
        return emissionDetails;
    }

    public void setEmissionDetails(HashMap<String, String> emissionDetails) {
        this.emissionDetails = emissionDetails;
    }


}
