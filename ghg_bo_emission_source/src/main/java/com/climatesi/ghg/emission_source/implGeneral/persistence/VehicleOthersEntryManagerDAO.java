package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.emission_source.api.beans.VehicleOthers;
import com.climatesi.ghg.emission_source.api.beans.VehicleOthers;
import com.climatesi.ghg.emission_source.implGeneral.beans.VehicleOthersBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.VehicleOthersBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VehicleOthersEntryManagerDAO extends AbstractDAO<VehicleOthersBean, VehicleOthers> {

    private Logger logger = Logger.getLogger(VehicleOthersEntryManagerDAO.class);
    private EntityManager em;

    public VehicleOthersEntryManagerDAO(EntityManager em) {
        super(em, VehicleOthersBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("EMISS_VEHICLE_OTHERS_ENTRY", "ENTRY_ID", "VehicleOthersBean");
        meta.setKeyPropertyName("entryId");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public VehicleOthers update(VehicleOthers entry) {
        logger.info("update" + entry + ")");
        VehicleOthers updated = em.merge(entry);
        return updated;
    }

    @Override
    public Map<String, Object> getAddSpParams(VehicleOthersBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(VehicleOthersBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(VehicleOthersBean entityImpl) throws GHGException {
        return null;
    }

    public List<VehicleOthers> getCustomFiltered(int branchId, int companyId, String fy) {
        logger.info("Getting Custom filter for calculations");
        Query query = null;

        if (branchId != -1) {
            query = em.createQuery("select o from VehicleOthersBean o where (o.branchId = :branchId and o.year = :fy and o.isDeleted = :isDeleted)", VehicleOthersBean.class);
            query.setParameter("branchId", branchId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }
        if (companyId != -1) {
            query = em.createQuery("select o from VehicleOthersBean o where (o.companyId = :companyId and o.year = :fy and o.isDeleted = :isDeleted)", VehicleOthersBean.class);
            query.setParameter("companyId", companyId);
            query.setParameter("fy", fy);
            query.setParameter("isDeleted", 0);
        }

        List<VehicleOthers> results = new ArrayList<>();
        try {
            Object resultView = query.getResultList();
            results = (List<VehicleOthers>) resultView;
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }

        return results;
    }

}