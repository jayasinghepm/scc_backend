package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.AuthenticationData;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

import javax.persistence.EntityManager;

public interface UserLoginManager extends DataEntryManager<UserLogin, Integer> {

    /**
     * Injects Entity Manager implementation into database interact layer. Actual database interaction done via this entity manager.
     *
     * @param entityManager is the created entity manager object in transaction handling layer
     */
    void injectEntityManager(EntityManager entityManager);

    /**
     * Get user login entry by comparing login name or alias
     *
     * @param name is user entered user name. This can be user name or alias
     * @return matching first login entry
     */
    UserLogin getLoginByName(String name);


    int deleteLoginProfile(int loginId);

    /**
     * get the number of available similar login names or aliases
     *
     * @param loginName login name or alias
     * @return number of results found
     */
    int validateLoginName(String loginName);

    /**
     * Update modified user login details in database
     *
     * @param userLogin is object with updated information
     * @return
     */
    boolean updateUserLogin(UserLogin userLogin);

    /**
     * @return AuthenticationData implementation with null properties
     */
    AuthenticationData getEmptyAuthParameterObject();


    Object addUserLoginProfile(UserLogin userLoginProfile);

    Object updateUserLoginProfile(UserLogin userLoginProfile);

    ListResult<UserLogin> getAllUserLoginProfileList();

    UserLogin getDummyUserLoginProfile();

    UserLogin getUserLoginProfileByKey(int id);


}