package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.User;
import com.climatesi.ghg.api.beans.UserActivityLog;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface UserActLogManager extends DataEntryManager<UserActivityLog, Long> {
}
