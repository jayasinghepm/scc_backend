package com.climatesi.ghg.api.beans;

import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;

import java.util.List;
import java.util.Map;

public interface AuthReply {

    public int getDisableFormulas();

    public void setDisableFormulas(int disableFormulas);




    public Map<Integer, AllowedEmissionSrcBean> getAllowedEmissionSources();
    public void setAllowedEmissionSources(Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources);

    public int getIsMasterAdmin();

    public void setIsMasterAdmin(int isMasterAdmin);

    public String getBranchName();

    public void setBranchName(String branchName);

    public String getCompanyName();

    public void setCompanyName(String companyName);

    public int getProjectStatus();

    public void setProjectStatus(int projectStatus);

    String getUserName();

    void setUserName(String userName);

    Integer getLoginStatus();

    void setLoginStatus(Integer loginStatus);

    String getNarration();

    void setNarration(String narration);

    String getSessionId();

    void setSessionId(String sessionId);

    public int getExistActiveProject();

    public void setExistActiveProject(int existActiveProject);

    List<String> getRemovedSessions();

    void setRemovedSessions(List<String> removedSessions);

    List<String> getEntitlements();

    void setEntitlements(List<String> entitlements);

    public int getUserType();

    public void setUserType(int userType);


    public int getUserId();

    public void setUserId(int userId);

    public int getClientId();

    public void setClientId(int clientId);

    public int getAdminId();

    public void setAdminId(int adminId);

    public int getCadminId();

    public void setCadminId(int cadminId);

    public int isFirstime();

    public void setFirstime(int firstime);

    public int getCompanyId();

    public void setCompanyId(int companyId);

    public int getBranchId();

    public void setBranchId(int branchId);

    public int getIsFirstime();

    public void setIsFirstime(int isFirstime);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public int getCadminStatus();

    public void setCadminStatus(int cadminStatus);

    public String getCadminStatusMsg();

    public void setCadminStatusMsg(String cadminStatusMsg);

    public int getDataEntryExpired();

    public void setDataEntryExpired(int dataEntryExpired);

    public int getDataEntryUserId();

    public void setDataEntryUserId(int dataEntryUserId);

    public List<Integer> getPages();

    public void setPages(List<Integer> pages) ;

    public List<Integer> getEmSources() ;

    public void setEmSources(List<Integer> emSources) ;


}
