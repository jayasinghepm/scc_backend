package com.climatesi.ghg.api;

public enum LoginResponseStatus {
    FAIL(0),
    SUCCESS(1),
    INVALID_LOGIN_NAME(2),
    PASSWORD_EXPIRED(3),
    INVALID_PASSWORD(4),
    USER_NOT_FOUND(5),
    INVALID_LOGIN_STATUS(6),
    INVALID_INPUT_PARAMETERS(7),
    INACTIVE_ACCOUNT(8),
    PENDING(9),
    LOCKED(10),
    SUSPENDED(11),
    NOT_REGISTERED(12),
    REGISTRATION_PENDING(13);

    private int code;

    LoginResponseStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static LoginResponseStatus getEnum(int code) {
        switch (code) {
            case 1:
                return SUCCESS;
            case 2:
                return INVALID_LOGIN_NAME;
            case 3:
                return PASSWORD_EXPIRED;
            case 4:
                return INVALID_PASSWORD;
            case 5:
                return USER_NOT_FOUND;
            case 6:
                return INVALID_LOGIN_STATUS;
            case 7:
                return INVALID_INPUT_PARAMETERS;
            case 8:
                return INACTIVE_ACCOUNT;
            case 9:
                return PENDING;
            case 10:
                return LOCKED;
            case 11:
                return SUSPENDED;
            case 12:
                return NOT_REGISTERED;
            case 13:
                return REGISTRATION_PENDING;
            default:
                return FAIL;
        }
    }
}
