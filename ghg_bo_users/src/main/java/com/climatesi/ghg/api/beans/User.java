package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface User {

    public int isDeleted();

    public void setDeleted(int deleted);


    int getUserId();

    void setUserId(int userId);

    String getFirstName();

    void setFirstName(String firstName);

    String getLastName();

    void setLastName(String lastName);

    int getInstitutionId();

    void setInstitutionId(int institutionId);

    String getInstitutionCode();

    void setInstitutionCode(String institutionCode);

    int getUserType();

    void setUserType(int userType);

    int getUserStatus();

    int getCreatedBy();

    void setCreatedBy(int createdBy);

    Date getCreatedDate();

    void setCreatedDate(Date createdDate);

    int getEditedBy();

    Date getEditedDate();

    void setEditedDate(Date editedDate);

    void setEditedBy(int editedBy);

    void setUserStatus(int userStatus);

    String getOfficeTele();

    void setOfficeTele(String officeTele);

    String getMobile();

    void setMobile(String mobile);

    String getEmail();

    void setEmail(String email);

    String getAddress();

    void setAddress(String address);


}

