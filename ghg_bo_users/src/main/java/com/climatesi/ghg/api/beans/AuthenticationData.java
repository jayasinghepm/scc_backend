package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface AuthenticationData {

    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getIpAddress();

    void setIpAddress(String ipAddress);

    Integer getChannelId();

    void setChannelId(Integer channelId);

    String getClientVersion();

    void setClientVersion(String clientVersion);

    Date getLoginTime();

    void setLoginTime(Date loginTime);
}
