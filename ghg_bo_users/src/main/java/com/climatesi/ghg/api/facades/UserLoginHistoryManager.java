package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.UserLoginHistory;

import javax.persistence.EntityManager;

public interface UserLoginHistoryManager {

    void injectEntityManager(EntityManager entityManager);

    void addUserLoginHistory(UserLoginHistory userLoginHistory);

    UserLoginHistory getEmptyUserLoginHistoryBean();
}
