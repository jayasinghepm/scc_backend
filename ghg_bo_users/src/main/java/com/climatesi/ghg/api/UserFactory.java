package com.climatesi.ghg.api;

import com.climatesi.ghg.api.beans.AuthenticationData;
import com.climatesi.ghg.api.facades.*;
import com.climatesi.ghg.emission_source.implGeneral.beans.AuthenticationDataBean;
import com.climatesi.ghg.emission_source.implGeneral.facades.*;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class UserFactory {

    private static Logger logger = Logger.getLogger(UserFactory.class);
    private static UserFactory userFactory;
    private static UserLoginManager userLoginManager;
    private static UserManager userManager;
    private static UserSessionManager userSessionManager;
    private static UserLoginHistoryManager userLoginHistoryManager;
    private static UserActLogManager userActLogManager;

    private UserFactory() {
        //Left intentionally
    }

    public static UserFactory getInstance() throws GHGException {
        try {
            if (userFactory == null) {
                userFactory = new UserFactory();
            }
            return userFactory;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new GHGException("Error in get instance of UserFactory", ex);
        }
    }


    public UserLoginManager getUserLoginManager(EntityManager entityManager) throws GHGException {

        if (userLoginManager == null) {
            userLoginManager = new UserLoginManagerFacade();
        }

        if (entityManager != null) {
            userLoginManager.injectEntityManager(entityManager);
        }
        return userLoginManager;
    }

   
    public UserLoginManager getLoginManagerInstance() throws GHGException {
        return userLoginManager;
    }

    public UserManager getUserManager(EntityManager entityManager) throws GHGException {
        if (userManager == null) {
            userManager = new UserManagerFacade();
        }
        if (entityManager != null) {
            userManager.injectEntityManager(entityManager);
        }
        return userManager;
    }

    public UserManager getUserManagerInstance() throws GHGException {
        return userManager;
    }

    public UserSessionManager getUserSessionManager(EntityManager entityManager) throws GHGException {
       if (userSessionManager == null) {
           userSessionManager = new UserSessionManagerFacade();
       }

       if (entityManager != null) {
           userSessionManager.injectEntityManager(entityManager);
       }
       return userSessionManager;
    }

    public UserLoginHistoryManager getUserLoginHistoryManager(EntityManager entityManager) throws GHGException {
        if (userLoginHistoryManager == null) {
            userLoginHistoryManager = new UserLoginHistoryManagerFacade();
        }
        if (entityManager != null) {
            userLoginHistoryManager.injectEntityManager(entityManager);
        }
        return userLoginHistoryManager;
    }

    public UserActLogManager getUserActLogManager(EntityManager em) throws GHGException {
        if (userActLogManager == null) {
            userActLogManager = new UserActLogManagerFacade();
            userActLogManager.injectEntityManager(em);
        }
        return userActLogManager;
    }

    public AuthenticationData getEmptyAuthenticationData() {
        return new AuthenticationDataBean();
    }
}
