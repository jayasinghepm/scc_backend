package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface UserActivityLog {

    public int isDeleted();

    public void setDeleted(int deleted);


    public long getId();
    public void setId(long id);

    public String getUsername() ;

    public void setUsername(String username);

    public int getUserId();

    public void setUserId(int userId);

    public int getCompanyId();

    public void setCompanyId(int companyId);

    public int getBranchId();

    public void setBranchId(int branchId);

    public String getActLog();

    public void setActLog(String actLog);

    public Date getLogTime();

    public void setLogTime(Date logTime);
}
