package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.User;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

// Todo: considering long userId
public interface UserManager extends DataEntryManager<User, Integer> {
}
