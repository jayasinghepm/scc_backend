package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface UserLoginHistory {

    public int isDeleted();

    public void setDeleted(int deleted);

    long getLoginHistoryId();

    void setLoginHistoryId(long loginHistoryId);

    String getUserId();

    void setUserId(String userId);

    String getLoginName();

    void setLoginName(String loginName);

    String getLoginEmail();

    void setLoginEmail(String loginEmail);

    String getVersion();

    void setVersion(String version);

    Date getLoginTime();

    void setLoginTime(Date loginTime);

    int getLoginStatus();

    void setLoginStatus(int loginStatus);

    String getConnectIp();

    void setConnectIp(String connectIp);

    String getSessionId();

    void setSessionId(String sessionId);

    String getRejectReason();

    void setRejectReason(String rejectReason);
}
