package com.climatesi.ghg.api.exceptions;

public class UserAccountException extends Exception {
    /**
     * Instantiates a new Model portfolio management exception.
     *
     * @param ex the ex
     */
    public UserAccountException(Exception ex) {
        super(ex);
    }

    /**
     * Instantiates a new Model portfolio management exception.
     *
     * @param message the message
     * @param ex      the ex
     */
    public UserAccountException(String message, Exception ex) {
        super(message, ex);
    }

}
