package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface UserSession {

    public int isDeleted();

    public void setDeleted(int deleted);

    String getUserSessionId();

    void setUserSessionId(String userSessionId);

    int getLoginId();

    void setLoginId(int loginId);

    String getIpAddress();

    void setIpAddress(String ip);

    Integer getChannelId();

    void setChannelId(Integer channelId);

    String getVersion();

    void setVersion(String version);

    Date getUserLoginTime();

    void setUserLoginTime(Date userLoginTime);

    Date getLastUpdated();

    void setLastUpdated(Date lastUpdated);


}
