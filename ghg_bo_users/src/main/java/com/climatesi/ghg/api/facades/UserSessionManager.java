package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.UserSession;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface UserSessionManager extends DataEntryManager<UserSession, String> {

    void removeUserSession(UserSession userSession);
}
