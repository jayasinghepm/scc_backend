package com.climatesi.ghg.api.beans;

import java.util.Date;

public interface UserLogin {

    public int getIsMasterAdmin();
    public void setIsMasterAdmin(int isMasterAdmin);

    public int isDeleted();

    public void setDeleted(int deleted);

    int getUserId();

    void setUserId(int userId);

    String getLoginName();

    void setLoginName(String loginName);

    String getLoginEmail();

    void setLoginEmail(String email);

    String getLoginPassword();

    void setLoginPassword(String password);

    int getStatus();

    void setStatus(int status);

    int getFailedLoginAttempts();

    void setFailedLoginAttempts(int failedAttemptCount);

    Date getPasswordExpiryDate();

    void setPasswordExpiryDate(Date passwordExpDate);

    int getFirstTime();

    void setFirstTime(int firstTime);

    Date getLastLoggedInDate();

    void setLastLoggedInDate(Date lastLoginDate);

    Date getFirstLoginDate();

    void setFirstLoginDate(Date firstLoginDate);

    int getUserType();

    void setUserType(int userType);
}
