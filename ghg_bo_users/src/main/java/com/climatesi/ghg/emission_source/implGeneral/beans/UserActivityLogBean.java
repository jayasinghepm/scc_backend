package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.api.beans.UserActivityLog;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "USR_ACT_LOGS")
@NamedQueries({
        @NamedQuery(name = "UserActivityLogBean.findById",
                query = "SELECT us FROM UserActivityLogBean us WHERE us.id = :id"),
})
public class UserActivityLogBean implements UserActivityLog {


    @Id
    @GeneratedValue(generator = "logIdSeq")
    @SequenceGenerator(name = "logIdSeq", sequenceName = "LOG_ID_SEQ", initialValue = 1, allocationSize = 1)
    private long id;
    private String username; // fname + lname
    private int userId;
    private int companyId;
    private int branchId;
    private String actLog;
    private Date logTime;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getActLog() {
        return actLog;
    }

    public void setActLog(String actLog) {
        this.actLog = actLog;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }
}
