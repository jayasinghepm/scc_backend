package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.api.beans.User;
import com.climatesi.ghg.api.facades.UserManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.UserManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class UserManagerFacade implements UserManager {
    private final static Logger logger = Logger.getLogger(UserManagerFacade.class);
    private UserManagerDAO userManagerDAO;

    /**
     * Used to get all entity list
     *
     * @return ListResult object with all entity list contains in database
     */
    @Override
    public ListResult<User> getAllEntityList() {
            return userManagerDAO.findAll();
            }

    /**
     * Used to get all paginated entity list
     *
     * @param pageNumber      is the number of page to display data
     * @param sortingProperty is the property which is required to sort before pagination
     * @return ResultList object with entity list which need to display in given page after sorting according to given sorting property
     */
    @Override
    public ListResult<User> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
            return null;
            }

    /**
     * Used to get filtered results for given custom filter criteria
     *
     * @param filterCriteria is the query string to pass to filter expected results
     * @return ListResult object with all entity list retrieved from database after appending given filter criteria to where clause
     */
    @Override
    public ListResult<User> getEntityListByFilter(Object filterCriteria) {
        return this.userManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
            }

    /**
     * Used to get filtered paginated results for given custom filter criteria
     *
     * @param pageNumber      is expected page number of display data
     * @param sortingProperty is the property which is required to sort before pagination
     * @param filterCriteria  is the query string to pass to filter expected results. This should be a valid sql where condition.
     * @return ListResult object with paginated entity list retrieved from database after appending given filter criteria to where clause
     */
    @Override
    public ListResult<User> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return this.userManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
            }

    /**
     * Used to get entity for given primary key
     *
     * @param id is the primary key value of entity table
     * @return entity which is relevant for given primary key. return null if there is no entity with given key.
     */
    @Override
    public User getEntityByKey(Integer id) {
            return userManagerDAO.findByKey(id);
            }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
            userManagerDAO = new UserManagerDAO(entityManager);

            }

    /**
     * Insert entity information into database
     *
     * @param entity contains new entity information
     * @return added entity meta information. Most probably this may contains the auto generated private key information.
     */
    @Override
    public Object addEntity(User entity) {
            try {
            return userManagerDAO.insert((UserBean) entity);
            } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
            }
            }

    /**
     * Insert entity information into database
     *
     * @param entity contains new entity information
     * @param userID the user changed the entity
     * @return added entity meta information. Most probably this may contains the auto generated private key information.
     */
    @Override
    public Object addEntity(User entity, int userID) {
            return null;
            }

    /**
     * Update entity information in database
     *
     * @param entity contains updated information which is required to persist
     * @return update action related information.
     */
    @Override
    public Object updateEntity(User entity) {
            try {
            return userManagerDAO.update((UserBean) entity, PersistMode.JPA_PERSIST);
            } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
            }
            }

    /**
     * Update entity information in database
     *
     * @param entity contains updated information which is required to persist
     * @param userID the user changed the entity
     * @return update action related information.
     */
    @Override
    public Object updateEntity(User entity, int userID) {
            return null;
            }

    /**
     * Change the approval status of master data entity
     *
     * @param entity entity concrete implementation object
     * @return true if change status action is success. otherwise it is false
     */
    @Override
    public boolean setStatusOfEntity(User entity) {
            return false;
            }

    /**
     * Change the approval status of master data entity
     *
     * @param entity entity concrete implementation object
     * @param userID the user changed the entity
     * @return true if change status action is success. otherwise it is false
     */
    @Override
    public boolean setStatusOfEntity(User entity, int userID) {
            return false;
            }

    /**
     * Change the approval status of master data entity
     * This setStatus method uses for change the status of entities which is return and out param from DB to app server
     *
     * @param entity entity concrete implementation object
     * @return status change actions
     */
    @Override
    public Object setStatus(User entity) {
            try {
            return this.userManagerDAO.statusChange((UserBean) entity);
            } catch (Exception e) {
            logger.error("Issue occurred while setting up status Commission Group");
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_CHANGE_STATUS;
            }
            }

    /**
     * Used to get a created dummy entity object
     *
     * @return dummy object without any property values
     */
    @Override
    public User getDummyEntity() {
            return new UserBean();
            }
        }