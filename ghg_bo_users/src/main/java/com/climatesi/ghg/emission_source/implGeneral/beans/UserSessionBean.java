package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.api.beans.UserSession;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USR_USER_SESSIONS")
@NamedQueries({
        @NamedQuery(name = "UserSessionBean.findById",
                query = "SELECT us FROM UserSessionBean us WHERE us.userSessionId = :userSessionId"),
})
public class UserSessionBean implements UserSession {

    @Id
    @Column(name = "SESSION_ID")
    private String userSessionId;

    @Column(name = "LOGIN_ID")
    private int loginId;

    @Column(name = "CLIENT_IP")
    private String ipAddress;

    @Column(name = "CHANNEL_ID")
    private Integer channelId;

    @Column(name = "CLIENT_VERSION")
    private String version;

    @Column(name = "LOGIN_TIME")
    private Date userLoginTime;

    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    @Override
    public String getUserSessionId() {
        return userSessionId;
    }

    @Override
    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }

    @Override
    public Integer getChannelId() {
        return channelId;
    }

    @Override
    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    @Override
    public Date getUserLoginTime() {
        return userLoginTime;
    }

    @Override
    public void setUserLoginTime(Date userLoginTime) {
        this.userLoginTime = userLoginTime;
    }

    @Override
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String getIpAddress() {
        return ipAddress;
    }

    @Override
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public int getLoginId() {
        return loginId;
    }

    @Override
    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "UserSessionBean{" +
                "userSessionId='" + userSessionId + '\'' +
                ", channelId='" + channelId + '\'' +
                ", userLoginTime=" + userLoginTime +
                ", lastUpdated=" + lastUpdated +
                ", IP='" + ipAddress + '\'' +
                ", loginId=" + loginId +
                ", version='" + version + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserSessionBean)) {
            return false;
        }

        UserSessionBean that = (UserSessionBean) o;

        if (getUserSessionId() != null ? !getUserSessionId().equals(that.getUserSessionId()) : that.getUserSessionId() != null) {
            return false;
        }
        if (getChannelId() != null ? !getChannelId().equals(that.getChannelId()) : that.getChannelId() != null) {
            return false;
        }
        if (getIpAddress() != null ? !getIpAddress().equals(that.getIpAddress()) : that.getIpAddress() != null) {
            return false;
        }
        if (getLoginId() != 0 ? getLoginId() != that.getLoginId() : that.getLoginId() != 0) {
            return false;
        }

        return getVersion() != null ? getVersion().equals(that.getVersion()) : that.getVersion() == null;

    }

    @Override
    public int hashCode() {
        int result = getUserSessionId() != null ? getUserSessionId().hashCode() : 0;
        result = 31 * result + (getChannelId() != null ? getChannelId().hashCode() : 0);
        result = 31 * result + (getIpAddress() != null ? getIpAddress().hashCode() : 0);
        result = 31 * result + (getLoginId() != 0 ? getLoginId() : 0);
        result = 31 * result + (getVersion() != null ? getVersion().hashCode() : 0);
        return result;
    }
}
