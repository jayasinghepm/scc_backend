package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.api.beans.UserActivityLog;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserLoginBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class UserActLogDAO extends AbstractDAO<UserActivityLogBean, UserActivityLog> {

    private EntityManager em;
    private static Logger logger = Logger.getLogger(UserActivityLog.class);

    public UserActLogDAO(EntityManager em) {
        super(em, UserActivityLogBean.class);
        this.em = em;
        DataEntryInfo userMeta = new DataEntryInfo("USR_ACT_LOGS", "id", "UserActivityLogBean");
        userMeta.setKeyPropertyName("id");
        userMeta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(userMeta);
    }

    @Override
    public Map<String, Object> getAddSpParams(UserActivityLogBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(UserActivityLogBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(UserActivityLogBean entityImpl) throws GHGException {
        return null;
    }
}
