package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.api.beans.User;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class UserManagerDAO extends AbstractDAO<UserBean, User> {
    private static Logger logger = Logger.getLogger(UserManagerDAO.class);

    public UserManagerDAO(EntityManager em) {
        super(em, UserBean.class);

        DataEntryInfo userMeta = new DataEntryInfo("USR_USER", "USER_ID", "UserBean");
        userMeta.setKeyPropertyName("userId");
        userMeta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(userMeta);
    }

    @Override
    public Map<String, Object> getAddSpParams(UserBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(UserBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(UserBean entityImpl) throws GHGException {
        return null;
    }
}
