package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.api.beans.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USR_USER")
@NamedQueries({
        @NamedQuery(name = "UserBean.findById",
                query = "SELECT i FROM UserBean i WHERE i.userId = :userId"),
})
public class UserBean implements User {
    @Id
//    @GeneratedValue(generator = "userIdSeq")
    @SequenceGenerator(name = "userIdSeq", sequenceName = "USER_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "USER_ID")
    private int userId;
    @Column(name = "FIRST_NAME", columnDefinition = "VARCHAR(255)")
    private String firstName;
    @Column(name = "LAST_NAME", columnDefinition = "VARCHAR(255)")
    private String lastName;
    @Column(name = "INST_ID")
    private int institutionId;
    @Column(name = "INST_CODE", columnDefinition = "VARCHAR(255)")
    private String institutionCode;
    @Column(name = "USER_TYPE")
    private int userType;
    @Column(name = "USER_STATUS")
    private int userStatus;
    @Column(name = "CREATED_BY")
    private int createdBy;
    @Column(name = "CREATED_DATE", columnDefinition = "DATE")
    private Date createdDate;
    @Column(name = "EDITED_BY")
    private int editedBy;
    @Column(name = "EDITED_DATE", columnDefinition = "DATE")
    private Date editedDate;
    @Column(name = "OFFICE_TELE", columnDefinition = "VARCHAR(255)")
    private String officeTele;
    @Column(name = "MOBILE", columnDefinition = "VARCHAR(255)")
    private String mobile;
    @Column(name = "EMAIL", columnDefinition = "VARCHAR(255)")
    private String email;
    @Column(name = "ADDRESS", columnDefinition = "VARCHAR(255)")
    private String address;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    
    public int getUserId() {
        return userId;
    }

    
    public void setUserId(int userId) {
        this.userId = userId;
    }

    
    public String getFirstName() {
        return firstName;
    }

    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    
    public String getLastName() {
        return lastName;
    }

    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    
    public int getInstitutionId() {
        return institutionId;
    }

    
    public void setInstitutionId(int institutionId) {
        this.institutionId = institutionId;
    }

    
    public String getInstitutionCode() {
        return institutionCode;
    }

    
    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    
    public int getUserType() {
        return userType;
    }

    
    public void setUserType(int userType) {
        this.userType = userType;
    }

    
    public int getUserStatus() {
        return userStatus;
    }

    
    public int getCreatedBy() {
        return createdBy;
    }

    
    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    
    public Date getCreatedDate() {
        return createdDate;
    }

    
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    
    public int getEditedBy() {
        return editedBy;
    }

    
    public void setEditedBy(int editedBy) {
        this.editedBy = editedBy;
    }

    
    public Date getEditedDate() {
        return editedDate;
    }

    
    public void setEditedDate(Date editedDate) {
        this.editedDate = editedDate;
    }

    
    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    
    public String getOfficeTele() {
        return officeTele;
    }

    
    public void setOfficeTele(String officeTele) {
        this.officeTele = officeTele;
    }

    
    public String getMobile() {
        return mobile;
    }

    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    
    public String getEmail() {
        return email;
    }

    
    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getAddress() {
        return address;
    }

    
    public void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        return "UserBean{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", institutionId=" + institutionId +
                ", institutionCode=" + institutionCode +
                ", userType=" + userType +
                ", userStatus='" + userStatus + '\'' +
                ", officeTele=" + officeTele +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", editedBy='" + editedBy + '\'' +
                ", editedDate='" + editedDate + '\'' +
                '}';
    }
}
