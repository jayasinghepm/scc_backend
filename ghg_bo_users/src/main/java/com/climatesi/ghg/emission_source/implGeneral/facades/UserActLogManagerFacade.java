package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.api.beans.UserActivityLog;
import com.climatesi.ghg.api.facades.UserActLogManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserActivityLogBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserLoginBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.UserActLogDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class UserActLogManagerFacade implements UserActLogManager {
    private UserActLogDAO dao;
    private static Logger logger = Logger.getLogger(UserActLogManagerFacade.class);

    @Override
    public Object addEntity(UserActivityLog entity) {
        try {
            return dao.insert((UserActivityLogBean) entity);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(UserActivityLog entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(UserActivityLog entity) {
        return null;
    }

    @Override
    public Object updateEntity(UserActivityLog entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(UserActivityLog entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(UserActivityLog entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(UserActivityLog entity) {
        return null;
    }

    @Override
    public UserActivityLog getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<UserActivityLog> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<UserActivityLog> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public UserActivityLog getEntityByKey(Long id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<UserActivityLog> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<UserActivityLog> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        if (dao == null) {
            dao = new UserActLogDAO(entityManager);
        }
    }
}
