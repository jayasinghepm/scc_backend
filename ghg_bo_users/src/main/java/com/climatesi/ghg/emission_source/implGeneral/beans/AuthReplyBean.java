package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.api.beans.AuthReply;
import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthReplyBean implements AuthReply {
    private Integer loginStatus;
    private String narration;
    private String sessionId;

    private int userId;
    private int clientId;
    private int adminId;
    private int cadminId;
    private int dataEntryUserId;
    private int isFirstime;
    private int companyId;
    private int branchId;

    private String firstName;
    private String lastName;
    private int userType;

    private int cadminStatus;
    private String cadminStatusMsg;
    private List<String> entitlements;

    private int existActiveProject;

    private int dataEntryExpired;

    private int projectStatus;

    private String branchName;
    private String companyName;

    private int isMasterAdmin;

    private List<Integer> pages;

    private List<Integer> emSources;

    private Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources ;

    private int disableFormulas ;


    public int getDisableFormulas() {
        return disableFormulas;
    }

    public void setDisableFormulas(int disableFormulas) {
        this.disableFormulas = disableFormulas;
    }

    public Map<Integer, AllowedEmissionSrcBean> getAllowedEmissionSources() {
        return allowedEmissionSources;
    }

    public void setAllowedEmissionSources(Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources) {
        this.allowedEmissionSources = allowedEmissionSources;
    }

    public List<Integer> getPages() {
        return pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }

    public List<Integer> getEmSources() {
        return emSources;
    }

    public void setEmSources(List<Integer> emSources) {
        this.emSources = emSources;
    }

    public int getDataEntryUserId() {
        return dataEntryUserId;
    }

    public void setDataEntryUserId(int dataEntryUserId) {
        this.dataEntryUserId = dataEntryUserId;
    }

    public int getIsMasterAdmin() {
        return isMasterAdmin;
    }

    public void setIsMasterAdmin(int isMasterAdmin) {
        this.isMasterAdmin = isMasterAdmin;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(int projectStatus) {
        this.projectStatus = projectStatus;
    }

    public int getDataEntryExpired() {
        return dataEntryExpired;
    }

    public void setDataEntryExpired(int dataEntryExpired) {
        this.dataEntryExpired = dataEntryExpired;
    }

    public int getExistActiveProject() {
        return existActiveProject;
    }

    public void setExistActiveProject(int existActiveProject) {
        this.existActiveProject = existActiveProject;
    }

    public int getCadminStatus() {
        return cadminStatus;
    }

    public void setCadminStatus(int cadminStatus) {
        this.cadminStatus = cadminStatus;
    }

    public String getCadminStatusMsg() {
        return cadminStatusMsg;
    }

    public int getIsFirstime() {
        return isFirstime;
    }

    public void setIsFirstime(int isFirstime) {
        this.isFirstime = isFirstime;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private List<String> removedSessions;
    private String userName;

    public void setCadminStatusMsg(String cadminStatusMsg) {
        this.cadminStatusMsg = cadminStatusMsg;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public int getCadminId() {
        return cadminId;
    }

    public void setCadminId(int cadminId) {
        this.cadminId = cadminId;
    }

    public int isFirstime() {
        return isFirstime;
    }

    public void setFirstime(int firstime) {
        isFirstime = firstime;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }


    @Override
    public String getUserName() {
        return this.userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public Integer getLoginStatus() {
        return loginStatus;
    }

    @Override
    public void setLoginStatus(Integer loginStatus) {
        this.loginStatus = loginStatus;
    }

    @Override
    public String getNarration() {
        return narration;
    }

    @Override
    public void setNarration(String narration) {
        this.narration = narration;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }



    @Override
    public List<String> getRemovedSessions() {
        return removedSessions;
    }

    @Override
    public void setRemovedSessions(List<String> removedSessions) {
        this.removedSessions = removedSessions;
    }

    @Override
    public List<String> getEntitlements() {
        return entitlements;
    }

    @Override
    public void setEntitlements(List<String> entitlements) {
        this.entitlements = entitlements;
    }
}

