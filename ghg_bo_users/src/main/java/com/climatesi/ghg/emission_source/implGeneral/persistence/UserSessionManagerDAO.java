package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.api.beans.UserSession;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserSessionBean;
import com.climatesi.ghg.utility.api.dao.AbstractViewOnlyDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class UserSessionManagerDAO extends AbstractViewOnlyDAO<UserSessionBean, UserSession> {
    private static Logger logger = Logger.getLogger(UserSessionManagerDAO.class);
    private EntityManager em;

    /**
     * This constructor calls the list of generic methods which are common to all modules.
     * i.e, Common DAO methods in MubasherDAO are accessed through this.
     *
     * @param em
     */
    public UserSessionManagerDAO(EntityManager em) {
        super(em, UserSessionBean.class);

        this.em = em;

        logger.debug("Creating DataEntryInfo entry with JPA_PERSIST mode");

        DataEntryInfo userSessionMeta = new DataEntryInfo("USR_USER_SESSIONS", "SESSION_ID", "UserSessionBean");
        userSessionMeta.setKeyPropertyName("userSessionId");
        userSessionMeta.setAddMode(PersistMode.JPA_PERSIST);
        userSessionMeta.setEditMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(userSessionMeta);
    }

    /**
     * Delete current user session from database
     *
     * @param session is the session which is going to remove
     */
    public void removeSession(UserSessionBean session) {
        logger.info("Delete session >> " + session.getUserSessionId());
        em.remove(session);
    }

}

