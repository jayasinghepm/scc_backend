package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.api.beans.AuthenticationData;
import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.api.exceptions.UserAccountException;
import com.climatesi.ghg.api.facades.UserLoginManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.AuthenticationDataBean;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserLoginBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.UserLoginManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class UserLoginManagerFacade implements UserLoginManager {

    private static Logger logger = Logger.getLogger(UserLoginManagerFacade.class);
    private UserLoginManagerDAO userLoginManagerDAO;



    @Override
    public Object addUserLoginProfile(UserLogin userLoginProfile) {
        try {
            return userLoginManagerDAO.insert((UserLoginBean) userLoginProfile);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateUserLoginProfile(UserLogin userLoginProfile) {
        try {
            return userLoginManagerDAO.update((UserLoginBean) userLoginProfile);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public ListResult<UserLogin> getAllUserLoginProfileList() {
        return userLoginManagerDAO.findAll();
    }

    @Override
    public UserLogin getDummyUserLoginProfile() {
        return new UserLoginBean();
    }

    @Override
    public UserLogin getUserLoginProfileByKey(int id) {
        return userLoginManagerDAO.findByKey(id);
    }

    /**
     * Used to get all entity list
     *
     * @return ListResult object with all entity list contains in database
     */
    @Override
    public ListResult<UserLogin> getAllEntityList() {
        return userLoginManagerDAO.findAll();
    }

    /**
     * Used to get all paginated entity list
     *
     * @param pageNumber      is the number of page to display data
     * @param sortingProperty is the property which is required to sort before pagination
     * @return ResultList object with entity list which need to display in given page after sorting according to given sorting property
     */
    @Override
    public ListResult<UserLogin> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    /**
     * Used to get filtered results for given custom filter criteria
     *
     * @param filterCriteria is the query string to pass to filter expected results
     * @return ListResult object with all entity list retrieved from database after appending given filter criteria to where clause
     */
    @Override
    public ListResult<UserLogin> getEntityListByFilter(Object filterCriteria) {
        return this.userLoginManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    /**
     * Used to get filtered paginated results for given custom filter criteria
     *
     * @param pageNumber      is expected page number of display data
     * @param sortingProperty is the property which is required to sort before pagination
     * @param filterCriteria  is the query string to pass to filter expected results. This should be a valid sql where condition.
     * @return ListResult object with paginated entity list retrieved from database after appending given filter criteria to where clause
     */
    @Override
    public ListResult<UserLogin> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return this.userLoginManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, 0);
    }


    /**
     * Used to get entity for given primary key
     *
     * @param id is the primary key value of entity table
     * @return entity which is relevant for given primary key. return null if there is no entity with given key.
     */
    @Override
    public UserLogin getEntityByKey(Integer id) {
        return userLoginManagerDAO.findByKey(id);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        userLoginManagerDAO = new UserLoginManagerDAO(entityManager);
    }

    @Override
    public UserLogin getLoginByName(String nameOrAlias) {
        UserLogin login = null;
        try {
            login = userLoginManagerDAO.getUserByNameOrAlias(nameOrAlias);
        } catch (UserAccountException e) {
            logger.error("[UserLoginManagerFacade.java:80] Failed to retrieve login details for given login name or alias", e);
        }
        return login;
    }

    @Override
    public int deleteLoginProfile(int loginId) {
        try {
            return userLoginManagerDAO.deleteLogin(loginId);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }

    /**
     * get the number of available similar login names or aliases
     *
     * @param loginNameOrAlias given login name or alias
     * @return number of results found
     */
    @Override
    public int validateLoginName(String loginNameOrAlias) {

        int result = 0;
        try {
            result = userLoginManagerDAO.getNumberOfUserByNameOrAlias(loginNameOrAlias);
        } catch (UserAccountException e) {
            logger.error("[UserLoginManagerFacade.java:99] Failed to retrieve similar login names or aliases for given login name or alias", e);
        }
        return result;
    }

    @Override
    public boolean updateUserLogin(UserLogin userLogin) {
        return userLoginManagerDAO.updateUserLogin(userLogin);
    }

    @Override
    public AuthenticationData getEmptyAuthParameterObject() {
        return new AuthenticationDataBean();
    }


    /**
     * Insert entity information into database
     *
     * @param entity contains new entity information
     * @return added entity meta information. Most probably this may contains the auto generated private key information.
     */
    @Override
    public Object addEntity(UserLogin entity) {
        try {
            return userLoginManagerDAO.insert((UserLoginBean) entity);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    /**
     * Insert entity information into database
     *
     * @param entity contains new entity information
     * @param userID the user changed the entity
     * @return added entity meta information. Most probably this may contains the auto generated private key information.
     */
    @Override
    public Object addEntity(UserLogin entity, int userID) {
        return null;
    }

    /**
     * Update entity information in database
     *
     * @param entity contains updated information which is required to persist
     * @return update action related information.
     */
    @Override
    public Object updateEntity(UserLogin entity) {
        try {
            return userLoginManagerDAO.update((UserLoginBean) entity);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    /**
     * Update entity information in database
     *
     * @param entity contains updated information which is required to persist
     * @param userID the user changed the entity
     * @return update action related information.
     */
    @Override
    public Object updateEntity(UserLogin entity, int userID) {
        try {
            return userLoginManagerDAO.update((UserLoginBean) entity);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    /**
     * Change the approval status of master data entity
     *
     * @param entity entity concrete implementation object
     * @return true if change status action is success. otherwise it is false
     */
    @Override
    public boolean setStatusOfEntity(UserLogin entity) {
        return false;
    }

    /**
     * Change the approval status of master data entity
     *
     * @param entity entity concrete implementation object
     * @param userID the user changed the entity
     * @return true if change status action is success. otherwise it is false
     */
    @Override
    public boolean setStatusOfEntity(UserLogin entity, int userID) {
        return false;
    }

    /**
     * Change the approval status of master data entity
     * This setStatus method uses for change the status of entities which is return and out param from DB to app server
     *
     * @param entity entity concrete implementation object
     * @return status change actions
     */
    @Override
    public Object setStatus(UserLogin entity) {
        try {
            return this.userLoginManagerDAO.statusChange((UserLoginBean) entity);
        } catch (Exception e) {
            logger.error("Issue occurred while setting up status Commission Group");
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_CHANGE_STATUS;
        }
    }

    /**
     * Used to get a created dummy entity object
     *
     * @return dummy object without any property values
     */
    @Override
    public UserLogin getDummyEntity() {
        return new UserLoginBean();
    }
}

