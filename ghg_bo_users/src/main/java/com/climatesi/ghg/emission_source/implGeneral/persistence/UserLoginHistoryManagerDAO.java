package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.api.beans.UserLoginHistory;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserLoginHistoryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class UserLoginHistoryManagerDAO extends AbstractDAO<UserLoginHistoryBean, UserLoginHistory> {

    private static Logger logger = Logger.getLogger(UserLoginHistoryManagerDAO.class);

    public UserLoginHistoryManagerDAO(EntityManager em) {
        super(em, UserLoginHistoryBean.class);
        DataEntryInfo userLoginHistoryMeta = new DataEntryInfo("USR_USER_LOGIN_HISTORY", "LOGIN_TIME", "UserLoginHistoryBean");
        userLoginHistoryMeta.setKeyPropertyName("LOGIN_HISTORY_ID");
        userLoginHistoryMeta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(userLoginHistoryMeta);
    }

    @Override
    public Map<String, Object> getAddSpParams(UserLoginHistoryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(UserLoginHistoryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(UserLoginHistoryBean entityImpl) throws GHGException {
        return null;
    }
}

