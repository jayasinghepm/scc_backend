package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.api.beans.UserLoginHistory;
import com.climatesi.ghg.api.facades.UserLoginHistoryManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserLoginHistoryBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.UserLoginHistoryManagerDAO;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class UserLoginHistoryManagerFacade implements UserLoginHistoryManager {
    private final static Logger logger = Logger.getLogger(UserLoginHistoryManagerFacade.class);
    private UserLoginHistoryManagerDAO userLoginHistoryManagerDAO;

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        userLoginHistoryManagerDAO = new UserLoginHistoryManagerDAO(entityManager);

    }

    @Override
    public void addUserLoginHistory(UserLoginHistory userLoginHistory) {
        try {
            userLoginHistoryManagerDAO.insert((UserLoginHistoryBean) userLoginHistory);
        } catch (GHGException e) {
            logger.error("Issue in updating user login history "+e.getMessage(),e);
        }
    }

    @Override
    public UserLoginHistory getEmptyUserLoginHistoryBean(){
        return new UserLoginHistoryBean();
    }
}

