package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.api.beans.UserLoginHistory;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USR_USER_LOGIN_HISTORY")
@NamedQueries({
        @NamedQuery(name = "UserLoginHistoryBean.findById",
                query = "SELECT i FROM UserLoginHistoryBean i WHERE i.loginHistoryId = :loginHistoryId"),
})
public class UserLoginHistoryBean implements UserLoginHistory {
    @Id
    @Column(name = "LOGIN_HISTORY_ID")
    @GeneratedValue(generator = "usrLoginHisIdSeq")
    @SequenceGenerator(name = "usrLoginHisIdSeq", sequenceName = "USR_LOGIN_HIS_ID_SEQ",initialValue = 1, allocationSize =1)
    private long loginHistoryId;
    @Column(name = "USER_ID", columnDefinition = "VARCHAR(100)")
    private String userId;
    @Column(name = "LOGIN_NAME", columnDefinition = "VARCHAR(100)")
    private String loginName;
    @Column(name = "LOGIN_EMAIL", columnDefinition = "VARCHAR(100)")
    private String loginEmail;
    @Column(name = "VERSION", columnDefinition = "VARCHAR(100)")
    private String version;
    @Column(name = "LOGIN_TIME")
    private Date loginTime;
    @Column(name = "LOGIN_STATUS", columnDefinition = "TINYINT(1)")
    private int loginStatus;
    @Column(name = "CONNECTION_IP", columnDefinition = "VARCHAR(100)")
    private String connectIp;
    @Column(name = "SESSION_ID", columnDefinition = "VARCHAR(100)")
    private String sessionId;
    @Column(name = "REJECT_REASON", columnDefinition = "TEXT")
    private String rejectReason;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public long getLoginHistoryId() {
        return loginHistoryId;
    }

    public void setLoginHistoryId(long loginHistoryId) {
        this.loginHistoryId = loginHistoryId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Override
    public String getLoginEmail() {
        return this.loginEmail;
    }

    @Override
    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }



    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public int getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(int loginStatus) {
        this.loginStatus = loginStatus;
    }

    @Override
    public String getConnectIp() {
        return connectIp;
    }

    @Override
    public void setConnectIp(String connectIp) {
        this.connectIp = connectIp;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String getRejectReason() {
        return rejectReason;
    }

    @Override
    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }
}
