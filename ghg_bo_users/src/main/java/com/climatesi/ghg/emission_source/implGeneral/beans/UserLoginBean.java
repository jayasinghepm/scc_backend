package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.api.beans.UserLogin;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "USR_LOGIN_PROFILE")
@NamedQueries({
        @NamedQuery(name = "UserLoginBean.findById",
                query = "SELECT i FROM UserLoginBean i WHERE i.userId = :userId"),
})
public class UserLoginBean implements UserLogin {

    @Id
    @GeneratedValue(generator = "userIdSeq")
    @SequenceGenerator(name = "userIdSeq", sequenceName = "USER_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "USER_ID")
    private int userId;
    @Column(name = "LOGIN_NAME", unique = true)
    private String loginName;
    @Column(name = "LOGIN_EMAIL")
    private String loginEmail;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "STATUS")
    private int status;
    @Column(name = "FAILED_ATTEMPT_COUNT")
    private int failedAttemptCount;
    @Column(name = "PASSWORD_EXP_DATE")
    private Date passwordExpDate;
    @Column(name = "IS_FIRST_TIME")
    private int firstTime;
    @Column(name = "LAST_LOGIN_DATE")
    private Date lastLoginDate;
    @Column(name = "FIRST_LOGIN_DATE")
    private Date firstLoginDate;
    @Column(name="USER_TYPE")
    private int userType;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    @Column(name="IS_MASTER_ADMIN")
    private int isMasterAdmin;

    public UserLoginBean(){
        this.isMasterAdmin = 0;
        //by default
    }

    public int getIsMasterAdmin() {
        return isMasterAdmin;
    }

    public void setIsMasterAdmin(int isMasterAdmin) {
        this.isMasterAdmin = isMasterAdmin;
    }

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String getLoginName() {
        return loginName;
    }

    @Override
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Override
    public String getLoginEmail() {
        return this.loginEmail;
    }

    @Override
    public void setLoginEmail(String email) {
        this.loginEmail = email;
    }



    @Override
    public String getLoginPassword() {
        return password;
    }

    @Override
    public void setLoginPassword(String password) {
        this.password = password;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int getFailedLoginAttempts() {
        return failedAttemptCount;
    }

    @Override
    public void setFailedLoginAttempts(int failedAttemptCount) {
        this.failedAttemptCount = failedAttemptCount;
    }

    @Override
    public Date getPasswordExpiryDate() {
        return passwordExpDate;
    }

    @Override
    public void setPasswordExpiryDate(Date passwordExpDate) {
        this.passwordExpDate = passwordExpDate;
    }

    @Override
    public int getFirstTime() {
        return firstTime;
    }

    @Override
    public void setFirstTime(int firstTime) {
        this.firstTime = firstTime;
    }

    @Override
    public Date getLastLoggedInDate() {
        return lastLoginDate;
    }

    @Override
    public void setLastLoggedInDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    @Override
    public Date getFirstLoginDate() {
        return firstLoginDate;
    }

    @Override
    public void setFirstLoginDate(Date firstLoginDate) {
        this.firstLoginDate = firstLoginDate;
    }

    @Override
    public int getUserType() {
        return this.userType;
    }

    @Override
    public void setUserType(int userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "UserLoginBean{" +
                "userId=" + userId +
                ", loginName='" + loginName + '\'' +
                ", loginEmail='" + loginEmail + '\'' +
                ", password=" + password +
                ", status=" + status +
                ", userType=" + userType +
                ", failedAttemptCount=" + failedAttemptCount +
                ", passwordExpDate='" + passwordExpDate + '\'' +
                ", firstTime=" + firstTime +
                ", lastLoginDate='" + lastLoginDate + '\'' +
                ", firstLoginDate='" + firstLoginDate + '\'' +
                '}';
    }

}
