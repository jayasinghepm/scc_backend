package com.climatesi.ghg.emission_source.implGeneral.facades;

import com.climatesi.ghg.api.beans.UserSession;
import com.climatesi.ghg.api.facades.UserSessionManager;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserSessionBean;
import com.climatesi.ghg.emission_source.implGeneral.persistence.UserSessionManagerDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;


public class UserSessionManagerFacade implements UserSessionManager {

    private static Logger logger = Logger.getLogger(UserSessionManagerFacade.class);
    private UserSessionManagerDAO userSessionManagerDAO;

    @Override
    public String addEntity(UserSession userSession) {
        try {
            return userSessionManagerDAO.insert((UserSessionBean) userSession);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(UserSession entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(UserSession userSession) {
        try {
            return userSessionManagerDAO.update((UserSessionBean) userSession);
        } catch (Exception e) {
            logger.error(e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(UserSession entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(UserSession userSession) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(UserSession entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(UserSession entity) {
        return null;
    }

    @Override
    public UserSession getDummyEntity() {
        return new UserSessionBean();
    }

    @Override
    public ListResult<UserSession> getAllEntityList() {
        return userSessionManagerDAO.findAll();
    }

    @Override
    public ListResult<UserSession> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return userSessionManagerDAO.findPaginatedSearchResults("", sortingProperty, pageNumber);
    }

    @Override
    public ListResult<UserSession> getEntityListByFilter(Object filterCriteria) {
        return userSessionManagerDAO.findPaginatedSearchResults(filterCriteria, "", 0);
    }

    @Override
    public ListResult<UserSession> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return userSessionManagerDAO.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public UserSession getEntityByKey(String userSessionId) {
        return userSessionManagerDAO.findByKey(userSessionId);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        userSessionManagerDAO = new UserSessionManagerDAO(entityManager);
    }

    @Override
    public void removeUserSession(UserSession userSession) {
        userSessionManagerDAO.removeSession((UserSessionBean) userSession);
    }
}

