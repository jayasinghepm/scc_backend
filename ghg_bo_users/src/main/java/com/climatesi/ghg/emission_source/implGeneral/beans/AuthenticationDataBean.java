package com.climatesi.ghg.emission_source.implGeneral.beans;

import com.climatesi.ghg.api.beans.AuthenticationData;

import java.util.Date;

public class AuthenticationDataBean implements AuthenticationData {
    private String username;
    private String password;
    private String ipAddress;
    private Integer channelId;
    private String clientVersion;
    private Date loginTime;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getIpAddress() {
        return ipAddress;
    }

    @Override
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public Integer getChannelId() {
        return channelId;
    }

    @Override
    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    @Override
    public String getClientVersion() {
        return clientVersion;
    }

    @Override
    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    @Override
    public Date getLoginTime() {
        return loginTime;
    }

    @Override
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }
}
