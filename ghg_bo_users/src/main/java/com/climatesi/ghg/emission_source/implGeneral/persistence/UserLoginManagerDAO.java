package com.climatesi.ghg.emission_source.implGeneral.persistence;

import com.climatesi.ghg.api.beans.UserLogin;
import com.climatesi.ghg.api.exceptions.UserAccountException;
import com.climatesi.ghg.emission_source.implGeneral.beans.UserLoginBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Map;

public class UserLoginManagerDAO extends AbstractDAO<UserLoginBean, UserLogin> {
    private static Logger logger = Logger.getLogger(UserLoginManagerDAO.class);
    private EntityManager em;

    public UserLoginManagerDAO(EntityManager em) {
        super(em, UserLoginBean.class);
        this.em = em;
        DataEntryInfo userMeta = new DataEntryInfo("USR_LOGIN_PROFILE", "USER_ID", "UserLoginBean");
        userMeta.setKeyPropertyName("userId");
        userMeta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(userMeta);
    }


    public UserLogin getUserByNameOrAlias(String nameOrAlias) throws UserAccountException {
        logger.info("Getting the user login detail matching with the given login name or alias >>" + nameOrAlias);

        Query searchByLoginQuery = em.createQuery("select l from UserLoginBean l where l.loginName = :uname or l.loginEmail = :uname");
        searchByLoginQuery.setParameter("uname", nameOrAlias);

        UserLogin userLogin = null;
        try {
            userLogin = (UserLoginBean) searchByLoginQuery.getSingleResult();
            logger.info("Return the user >>" + userLogin);
        } catch (NoResultException nre) {
            logger.info("User not found for given login name or login alias.", nre);
        }

        return userLogin;
    }

    /**
     * To check for any given login name or login Aliases if a similar login name or alias is exists.
     *
     * @param nameOrAlias given login name or alias
     * @return the number of results found
     * @throws UserAccountException
     */
    public int getNumberOfUserByNameOrAlias(String nameOrAlias) throws UserAccountException {

        int foundMatchingLoginAliases = 0;
        logger.info("Getting the number of login detail matching with the given login name or alias >>" + nameOrAlias);

        Query searchByLoginQuery = em.createQuery("select l from UserLoginBean l where l.loginName = :uname or l.loginAlias = :uname");
        searchByLoginQuery.setParameter("uname", nameOrAlias);

        try {
            foundMatchingLoginAliases = searchByLoginQuery.getResultList().size();
            logger.info("available similar login or aliases >>" + foundMatchingLoginAliases);
        } catch (NoResultException nre) {
            logger.info("User not found for given login name or login alias.", nre);
        }

        return foundMatchingLoginAliases;
    }



    public int deleteLogin(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from UserLoginBean l where l.userId = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
       return id;
    }

    public boolean updateUserLogin(UserLogin loginInfo) {
        UserLogin mLogin = em.merge(loginInfo);
        return mLogin == null;
    }

    @Override
    public Map<String, Object> getAddSpParams(UserLoginBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(UserLoginBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(UserLoginBean entityImpl) throws GHGException {
        return null;
    }
}
