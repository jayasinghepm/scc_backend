package com.climatesi.ghg.utility.implGeneral.persistence;

import org.jboss.logging.Logger;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HashMapStrngListStringConverter implements AttributeConverter<HashMap<String, List<String>>, String> {
   private static Logger logger = Logger.getLogger(HashMapStrngListStringConverter.class);

    @Override
    public String convertToDatabaseColumn(HashMap<String, List<String>> stringListHashMap) {
        if(stringListHashMap == null || stringListHashMap.size() == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringListHashMap.forEach((k,v) -> {
            stringBuilder.append(k);
            stringBuilder.append(":");
//            stringBuilder.append("[");
            v.forEach(s -> {
                stringBuilder.append(",");
            });
//            stringBuilder.append("]");
            stringBuilder.append("-");
        });
        return stringBuilder.toString();
    }

    @Override
    public HashMap<String, List<String>> convertToEntityAttribute(String s) {
        if (s == null || s.equals("")) {
            return  new HashMap<>();
        }
        String [] values = s.split("-");
        HashMap<String, List<String>> map = new HashMap<>();
        try {
            for(int i = 0 ; i < values.length; i++) {
                String [] obj = values[i].split(":");
                String key = obj[0];
                String value = obj[1];
                String [] listStr = value.split("");
                List<String> list = new ArrayList<>();
                for (int j =0 ; j < listStr.length; j++) {
                    list.add(listStr[j]);
                }
                map.put(key, list);
            }
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return map;
    }
}
