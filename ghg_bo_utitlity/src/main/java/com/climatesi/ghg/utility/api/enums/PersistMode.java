package com.climatesi.ghg.utility.api.enums;

public enum PersistMode {
    JPA_PERSIST(1), //Use Direct Persistence API using JPA entity
    DB_SP_CALL(2); //Call database stored procedures

    int id;

    private PersistMode(int id) {
        this.id = id;
    }

    public static PersistMode getEnum(int id) {
        if (1 == id) {
            return JPA_PERSIST;
        } else {
            return DB_SP_CALL;
        }
    }
}
