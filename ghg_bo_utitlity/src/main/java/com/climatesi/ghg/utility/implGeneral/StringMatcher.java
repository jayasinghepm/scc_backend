package com.climatesi.ghg.utility.implGeneral;

public class StringMatcher {


    public static int getMatchScore(String query, String matcher) {
        query = query.toLowerCase();
        matcher = matcher.toLowerCase();
        int score = 0;

        for(int i = 0; i < query.length(); i++) {
            if(matcher.length()  > i){
                if (query.charAt(i) == matcher.charAt(i)) {
                    score ++;
                }else {
                    break;
                }
            }else {
                break;
            }
        }
        return score;
    }

    public static int getMatchScoreText(String query, String text) {
        String [] words = text.split(" ");
        int score = 0;
        for(String w : words) {
            score +=  (StringMatcher.getMatchScore(query, w) * 5);
        }
        return score;
    }
}
