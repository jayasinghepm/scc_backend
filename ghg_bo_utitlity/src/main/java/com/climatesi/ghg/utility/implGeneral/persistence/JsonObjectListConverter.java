package com.climatesi.ghg.utility.implGeneral.persistence;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class JsonObjectListConverter implements AttributeConverter<JsonObject, String> {
    @Override
    public String convertToDatabaseColumn(JsonObject jsonObject) {
        if (jsonObject == null) {
            return "";
        }

        return new Gson().toJson(jsonObject);
    }

    @Override
    public JsonObject convertToEntityAttribute(String s) {
        if (s == null || s.equals("")) {
            return new JsonObject();
        }
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(s).getAsJsonObject();
        return jsonObject;
    }


}
