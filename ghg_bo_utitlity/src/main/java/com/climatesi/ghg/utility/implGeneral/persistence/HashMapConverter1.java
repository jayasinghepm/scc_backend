package com.climatesi.ghg.utility.implGeneral.persistence;

import org.jboss.logging.Logger;

import javax.persistence.AttributeConverter;
import java.util.HashMap;

public class HashMapConverter1 implements AttributeConverter<HashMap<String, Object>, String> {
    private static final Logger logger = Logger.getLogger(HashMapConverter1.class);

    @Override
    public String convertToDatabaseColumn(HashMap<String, Object> stringObjectHashMap) {
        if (stringObjectHashMap == null || stringObjectHashMap.size() == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringObjectHashMap.forEach((k, v) -> {
            if (v instanceof String) {
                stringBuilder.append(k + "-" + v + ",");
            }
            if (v instanceof HashMap) {
                stringBuilder.append(k + "-");

                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("{");
                ((HashMap<String, String>) v).forEach((s, t) -> {
                    stringBuilder1.append(s + "-" + t + ",");
                });
                stringBuilder1.append("}");
                stringBuilder.append(",");
            }
        });
        stringBuilder.append("}");
        return null;
    }

    @Override
    public HashMap<String, Object> convertToEntityAttribute(String s) {
        if (s == null || s.equals("")) {
            return new HashMap<>();
        }
        String[] values = s.split(",");
        HashMap<String, Object> map = new HashMap<>();
        try {
            for (int i = 0; i < values.length; i++) {
                String key = values[i].split("-")[0];
                String value = values[i].split("-")[1];
                if (value.contains("{") && value.contains("}")) {
                    String mpStr = value.substring(1, value.length() - 2);
                    String[] mpValues = mpStr.split(",");
                    HashMap<String, String> insideMap = new HashMap<>();
                    for (int j = 0; j < mpValues.length; j++) {
                        String k = mpValues[j].split("-")[0];
                        String v = mpValues[j].split("-")[1];
                        insideMap.put(k, v);
                    }
                    map.put(key, insideMap);
                } else {
                    map.put(key, value);
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return map;
    }
}
