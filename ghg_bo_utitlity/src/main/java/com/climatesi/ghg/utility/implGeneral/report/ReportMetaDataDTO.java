package com.climatesi.ghg.utility.implGeneral.report;

import com.google.gson.JsonObject;

import java.util.List;

public class ReportMetaDataDTO {

    private int id;

    private int projectId;


    private String comName_header;


    private String comName_address;


    private String addr1;


    private String addr2;


    private String district;


    private String comRegNo;


    private String email;

    private String telphone;

    private String fax;

    private String climateSi_Name;


    private String climateSi_addr1;


    private String climateSi_addr2;


    private String climateSi_district;

    private String climateSi_email;

    private String climaeSi_telephone;

    private String climatesi_fax;

    private String year;

    private String yearStartDate;

    private String yearEndDate;

    private int attempt;



    private JsonObject excludedSrcReasons;

    private String proposedDate;


    private String desCompany;


    private String desResUnit;


    private String desEnvironmentalMgmntFramework;

    private String isoStandard;


    private String previousYearISOStandard;



    private List<String> suggestions;


    private String desGHGMitigationActions;



    private List<String> nextSteps;



    private List<String> products;

    private String figEnvMgmtFramework;

    private String figResUnit;

    private String srcEnvMgmtFramework;

    private String srcResUnit;

    private JsonObject previousYearEmission;

    private JsonObject previousYearEfs;

    private String gr_dir_v_indir;
    private String gr_result_by_src;
    private String gr_pie_direct;
    private String gr_pie_indirect;
    private String gr_com_ghg;
    private String gr_com_direct_by_src;
    private String gr_com_indirect_by_src;
    private String gr_percaptia;
    private String gr_intensity;
    private String gr_per_production;
    private JsonObject previousYearStats;


    private int num_emps;
    private float revenue;
    private float prod_ton;

    public int getNum_emps() {
        return num_emps;
    }

    public void setNum_emps(int num_emps) {
        this.num_emps = num_emps;
    }

    public float getRevenue() {
        return revenue;
    }

    public void setRevenue(float revenue) {
        this.revenue = revenue;
    }

    public float getProd_ton() {
        return prod_ton;
    }

    public void setProd_ton(float prod_ton) {
        this.prod_ton = prod_ton;
    }

    public String getGr_dir_v_indir() {
        return gr_dir_v_indir;
    }

    public void setGr_dir_v_indir(String gr_dir_v_indir) {
        this.gr_dir_v_indir = gr_dir_v_indir;
    }

    public String getGr_result_by_src() {
        return gr_result_by_src;
    }

    public void setGr_result_by_src(String gr_result_by_src) {
        this.gr_result_by_src = gr_result_by_src;
    }

    public String getGr_pie_direct() {
        return gr_pie_direct;
    }

    public void setGr_pie_direct(String gr_pie_direct) {
        this.gr_pie_direct = gr_pie_direct;
    }

    public String getGr_pie_indirect() {
        return gr_pie_indirect;
    }

    public void setGr_pie_indirect(String gr_pie_indirect) {
        this.gr_pie_indirect = gr_pie_indirect;
    }

    public String getGr_com_ghg() {
        return gr_com_ghg;
    }

    public void setGr_com_ghg(String gr_com_ghg) {
        this.gr_com_ghg = gr_com_ghg;
    }

    public String getGr_com_direct_by_src() {
        return gr_com_direct_by_src;
    }

    public void setGr_com_direct_by_src(String gr_com_direct_by_src) {
        this.gr_com_direct_by_src = gr_com_direct_by_src;
    }

    public String getGr_com_indirect_by_src() {
        return gr_com_indirect_by_src;
    }

    public void setGr_com_indirect_by_src(String gr_com_indirect_by_src) {
        this.gr_com_indirect_by_src = gr_com_indirect_by_src;
    }

    public String getGr_percaptia() {
        return gr_percaptia;
    }

    public void setGr_percaptia(String gr_percaptia) {
        this.gr_percaptia = gr_percaptia;
    }

    public String getGr_intensity() {
        return gr_intensity;
    }

    public void setGr_intensity(String gr_intensity) {
        this.gr_intensity = gr_intensity;
    }

    public String getGr_per_production() {
        return gr_per_production;
    }

    public void setGr_per_production(String gr_per_production) {
        this.gr_per_production = gr_per_production;
    }

    public JsonObject getPreviousYearStats() {
        return previousYearStats;
    }

    public void setPreviousYearStats(JsonObject previousYearStats) {
        this.previousYearStats = previousYearStats;
    }

    public String getFigEnvMgmtFramework() {
        return figEnvMgmtFramework;
    }

    public void setFigEnvMgmtFramework(String figEnvMgmtFramework) {
        this.figEnvMgmtFramework = figEnvMgmtFramework;
    }

    public String getFigResUnit() {
        return figResUnit;
    }

    public void setFigResUnit(String figResUnit) {
        this.figResUnit = figResUnit;
    }

    public String getSrcEnvMgmtFramework() {
        return srcEnvMgmtFramework;
    }

    public void setSrcEnvMgmtFramework(String srcEnvMgmtFramework) {
        this.srcEnvMgmtFramework = srcEnvMgmtFramework;
    }

    public String getSrcResUnit() {
        return srcResUnit;
    }

    public void setSrcResUnit(String srcResUnit) {
        this.srcResUnit = srcResUnit;
    }

    public JsonObject getPreviousYearEmission() {
        return previousYearEmission;
    }

    public void setPreviousYearEmission(JsonObject previousYearEmission) {
        this.previousYearEmission = previousYearEmission;
    }

    public JsonObject getPreviousYearEfs() {
        return previousYearEfs;
    }

    public void setPreviousYearEfs(JsonObject previousYearEfs) {
        this.previousYearEfs = previousYearEfs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getComName_header() {
        return comName_header;
    }

    public void setComName_header(String comName_header) {
        this.comName_header = comName_header;
    }

    public String getComName_address() {
        return comName_address;
    }

    public void setComName_address(String comName_address) {
        this.comName_address = comName_address;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getComRegNo() {
        return comRegNo;
    }

    public void setComRegNo(String comRegNo) {
        this.comRegNo = comRegNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getClimateSi_Name() {
        return climateSi_Name;
    }

    public void setClimateSi_Name(String climateSi_Name) {
        this.climateSi_Name = climateSi_Name;
    }

    public String getClimateSi_addr1() {
        return climateSi_addr1;
    }

    public void setClimateSi_addr1(String climateSi_addr1) {
        this.climateSi_addr1 = climateSi_addr1;
    }

    public String getClimateSi_addr2() {
        return climateSi_addr2;
    }

    public void setClimateSi_addr2(String climateSi_addr2) {
        this.climateSi_addr2 = climateSi_addr2;
    }

    public String getClimateSi_district() {
        return climateSi_district;
    }

    public void setClimateSi_district(String climateSi_district) {
        this.climateSi_district = climateSi_district;
    }

    public String getClimateSi_email() {
        return climateSi_email;
    }

    public void setClimateSi_email(String climateSi_email) {
        this.climateSi_email = climateSi_email;
    }

    public String getClimaeSi_telephone() {
        return climaeSi_telephone;
    }

    public void setClimaeSi_telephone(String climaeSi_telephone) {
        this.climaeSi_telephone = climaeSi_telephone;
    }

    public String getClimatesi_fax() {
        return climatesi_fax;
    }

    public void setClimatesi_fax(String climatesi_fax) {
        this.climatesi_fax = climatesi_fax;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYearStartDate() {
        return yearStartDate;
    }

    public void setYearStartDate(String yearStartDate) {
        this.yearStartDate = yearStartDate;
    }

    public String getYearEndDate() {
        return yearEndDate;
    }

    public void setYearEndDate(String yearEndDate) {
        this.yearEndDate = yearEndDate;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public JsonObject getExcludedSrcAndReasons() {
        return excludedSrcReasons;
    }

    public void setExcludedSrcAndReasons(JsonObject excludedSrcReasons) {
        this.excludedSrcReasons = excludedSrcReasons;
    }

    public String getProposedDate() {
        return proposedDate;
    }

    public void setProposedDate(String proposedDate) {
        this.proposedDate = proposedDate;
    }

    public String getDesCompany() {
        return desCompany;
    }

    public void setDesCompany(String desCompany) {
        this.desCompany = desCompany;
    }

    public String getDesResUnit() {
        return desResUnit;
    }

    public void setDesResUnit(String desResUnit) {
        this.desResUnit = desResUnit;
    }

    public String getDesEnvironmentalMgmntFramework() {
        return desEnvironmentalMgmntFramework;
    }

    public void setDesEnvironmentalMgmntFramework(String desEnvironmentalMgmntFramework) {
        this.desEnvironmentalMgmntFramework = desEnvironmentalMgmntFramework;
    }

    public String getIsoStandard() {
        return isoStandard;
    }

    public void setIsoStandard(String isoStandard) {
        this.isoStandard = isoStandard;
    }

    public String getPreviousYearISOStandard() {
        return previousYearISOStandard;
    }

    public void setPreviousYearISOStandard(String previousYearISOStandard) {
        this.previousYearISOStandard = previousYearISOStandard;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    public String getDesGHGMitigationActions() {
        return desGHGMitigationActions;
    }

    public void setDesGHGMitigationActions(String desGHGMitigationActions) {
        this.desGHGMitigationActions = desGHGMitigationActions;
    }

    public List<String> getNextSteps() {
        return nextSteps;
    }

    public void setNextSteps(List<String> nextSteps) {
        this.nextSteps = nextSteps;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }
}
