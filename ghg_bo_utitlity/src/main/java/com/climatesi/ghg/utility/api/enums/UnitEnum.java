package com.climatesi.ghg.utility.api.enums;

public enum UnitEnum {

    Liters(1),
    Kg(2),
    Tons(3),
    m3(4),
    None(-1),
    kWh(5),
    kgCO2e(6),
    percentage(7),
    kgco2perpassengerkm(8),
    km(9),
    ;

    private int id;

    public int getId() {
        return id;
    }

    UnitEnum(int id) {this.id = id;}
}
