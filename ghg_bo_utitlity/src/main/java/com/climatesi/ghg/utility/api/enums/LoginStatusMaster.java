package com.climatesi.ghg.utility.api.enums;

public enum LoginStatusMaster {
    UNKNOWN(-1),
    PENDING(0),
    ACTIVE(1),
    LOCKED(2),
    SUSPENDED(3);

    private int code;

    LoginStatusMaster(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static LoginStatusMaster getEnum(int code) {
        switch (code) {
            case 0:
                return PENDING;
            case 1:
                return ACTIVE;
            case 2:
                return LOCKED;
            case 3:
                return SUSPENDED;
            default:
                return UNKNOWN;
        }
    }
}
