package com.climatesi.ghg.utility.api.enums;

public enum LorryTransModes {

    Internal(1),
    External(2);

    int mode;

    LorryTransModes(int mode) {
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }
}
