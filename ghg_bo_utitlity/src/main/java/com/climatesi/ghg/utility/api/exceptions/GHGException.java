package com.climatesi.ghg.utility.api.exceptions;

public class GHGException extends Exception {

    public GHGException() {
        super();
    }

    public GHGException(Exception ex) {
        super(ex);
    }

    public GHGException(String message) {
        super(message);
    }

    public GHGException(String message, Exception ex) {
        super(message, ex);
    }
}
