package com.climatesi.ghg.utility.api.enums;

public enum DirectCatEnum {

    Direct(1),
    Indirect(2),
    None(3);

    private int cat;

    DirectCatEnum(int cat) {
        this.cat = cat;
    }

    public int getCat() {
        return cat;
    }
}
