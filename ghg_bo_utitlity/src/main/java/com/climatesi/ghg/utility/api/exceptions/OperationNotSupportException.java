package com.climatesi.ghg.utility.api.exceptions;

public class OperationNotSupportException extends GHGException {
    public OperationNotSupportException(String message) {
        super(message);
    }
}
