package com.climatesi.ghg.utility.api.enums;

public enum FuelTypes {

    Petrol (1),
    Diesel(2);

    private int fuel;

    FuelTypes(int fuel) {
        this.fuel = fuel;
    }

    public int getFuel() {
        return fuel;
    }
}
