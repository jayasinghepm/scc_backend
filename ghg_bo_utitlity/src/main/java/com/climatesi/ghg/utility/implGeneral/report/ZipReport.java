package com.climatesi.ghg.utility.implGeneral.report;

import org.jboss.logging.Logger;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.climatesi.ghg.utility.implGeneral.report.WriteTex.*;

public class ZipReport {

    private static final Logger logger = Logger.getLogger(ZipReport.class);

    public static String getZipReport() {
        return "";
    }

    public static File writeImage(String base64, String fileName) {
        BufferedImage image = null;
        byte[] imageByte = null;

        Base64.Decoder decoder = Base64.getDecoder();
        try {

            imageByte = decoder.decode(base64.split(",")[1]);
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        String mimeType = null;
        String fileExtension = null;
        try {
            mimeType = URLConnection.guessContentTypeFromStream(bis);
            String delimiter="[/]";
            String[] tokens = mimeType.split(delimiter);
            fileExtension = tokens[1];
        }catch (IOException e) {
            logger.error(e);
        }
        try {
            image = ImageIO.read(bis);
            bis.close();
        } catch (IOException e) {
            logger.error(e);
            return  null;
        }


        File outputfile = new File(fileName + "." + fileExtension);
        try {
            ImageIO.write(image, fileExtension, outputfile);
        } catch (IOException e) {
            logger.error(e);
            return null;
        }
        return outputfile;
    }

    public static void writeStructure(String url) {
        StringBuilder structureTex = new StringBuilder();
        structureTex.append("\\usepackage{titlesec} \\usepackage{graphicx} % Required for including pictures\n" +
                "\\graphicspath{{images/}} % Specifies the directory where pictures are stored\n" +
                "\n" +
                "\\usepackage{tikz} % Required for drawing custom shapes\n" +
                "\\usepackage[english]{babel} % English language/hyphenation\n" +
                "\\usepackage{enumitem} % Customize lists\n" +
                "\\setlist{nolistsep} % Reduce spacing between bullet points and numbered lists\n" +
                "\n" +
                "\\usepackage{booktabs} % Required for nicer horizontal rules in tables\n" +
                "\n" +
                "\\usepackage{eso-pic} % Required for specifying an image background in the title page\n" +
                "\n" +
                "\\usepackage{titletoc} % Required for manipulating the table of contents\n" +
                "\\contentsmargin{0cm} % Removes the default margin");

        PrintWriter pw = null;
        try {
            pw = new PrintWriter( url +".tex", "UTF-8");
            pw.println(structureTex.toString());
            pw.close();
        } catch (FileNotFoundException e) {
           logger.error(e);
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        }


    }

    public static void writeMainTex(String url) {
        StringBuilder mainBuilder = new StringBuilder();
        mainBuilder.append("\\documentclass[fleqn, 16pt,a4paper]{book}\n" +
                "\n" +
                "\\usepackage[top=3cm,bottom=2.5cm,left=2.5cm,right=2.5cm,headsep=10pt,a4paper, headheight=5cm]{geometry}\n" +
                "\n" +
                "% Font Settings\n" +
                "\\usepackage{avant} % Use the Avantgarde font for headings\n" +
                "\\usepackage{mathptmx} % Use the Adobe Times Roman as the default text font together with math symbols from the Sym\u00ADbol, Chancery and Com\u00ADputer Modern fonts\n" +
                "\\usepackage{microtype} % Slightly tweak font spacing for aesthetics\n" +
                "\\usepackage[utf8]{inputenc} % Required for including letters with accents\n" +
                "\\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs\n" +
                "\n" +
                " \\usepackage{amsmath,amsthm, amssymb, latexsym}\n" +
                "\n" +
                "% Index\n" +
                "\\usepackage{calc} % For simpler calculation - used for spacing the index letter headings correctly\n" +
                "\\usepackage{makeidx} % Required to make an index\n" +
                "\\makeindex % Tells LaTeX to create the files required for indexing\n" +
                "\\usepackage{verbatim}\n" +
                "\n" +
                "\\usepackage[export]{adjustbox}\n" +
                "\n" +
                "\\usepackage{graphicx}\n" +
                "\\usepackage{tabulary}\n" +
                "\n" +
                "\\usepackage{seqsplit}\n" +
                "\n" +
                "\\input{structure} % Insert the commands.tex file which contains the majority of the structure behind the template\n" +
                "\n" +
                "\\usepackage{xcolor}\n" +
                "\\usepackage{fancyhdr}\n" +
                "\\renewcommand{\\headrulewidth}{0.5pt}\n" +
                "\\renewcommand{\\headrule}{\\hbox to\\headwidth{\\color{red}\\leaders\\hrule height \\headrulewidth\\hfill}}\n" +
                "\\renewcommand{\\footrulewidth}{0.5pt}\n" +
                "\\renewcommand{\\footrule}{\\hbox to\\headwidth{\\color{red}\\leaders\\hrule height \\footrulewidth\\hfill}}\n" +
                "\\pagestyle{fancy}\n" +
                "\\lhead{\\vspace{1.5 \\baselineskip }Greenhouse Gas Inventory Report – Company Name}\n" +
                "\\rhead{\\includegraphics[width=3cm]{images/company_logo}}\n" +
                "\\rfoot{\\includegraphics[width=2cm]{images/favicon}}\n" +
                "\\cfoot{\\vspace{-2.5\\baselineskip}Climate Smart Initiatives (Pvt) Ltd }\n" +
                "\\fancyfoot[l]{\\vspace{-2.5\\baselineskip}\\thepage}\n" +
                "\n" +
                "% Redefine the plain page style\n" +
                "\\fancypagestyle{plain}{%\n" +
                "%   \\fancyhf{}%\n" +
                "%   \\fancyfoot[C]{\\thepage\\ of \\pageref{LastPage}}%\n" +
                "%   \\renewcommand{\\headrulewidth}{0pt}% Line at the header invisible\n" +
                "%   \\renewcommand{\\footrulewidth}{0.4pt}% Line at the footer visible\n" +
                "\\lhead{\\vspace{-2.5 \\baselineskip}Greenhouse Gas Inventory Report – Company Name}\n" +
                "\\rhead{\\includegraphics[width=3cm]{images/company_logo}}\n" +
                "\\fancyfoot[r]{\\includegraphics[width=2cm]{images/favicon}}\n" +
                "\\cfoot{\\vspace{-2.5\\baselineskip}Climate Smart Initiatives (Pvt) Ltd }\n" +
                "\\fancyfoot[l]{\\vspace{-2.5\\baselineskip}\\thepage}\n" +
                "}\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\\begin{document}\n" +
                "\\begingroup\n" +
                "\\let\\cleardoublepage\\clearpage\n" +
                "\n" +
                "\\thispagestyle{empty}\n" +
                "% \\begingroup\n" +
                "%\n" +
                "% \\AddToShipoutPicture*{\\put(15,200){\\includegraphics[scale=1, width=20cm,height=40cm,keepaspectratio]{images/home}}} % Image background\n" +
                "% \\endgroup\n" +
                "% \\begin{titlepage}\n" +
                "\\includegraphics[width=.3\\textwidth,right]{images/company_logo}\n" +
                "\n" +
                "\\begingroup\n" +
                "\\vspace{1cm}\n" +
                "\\begin{center}\n" +
                "\\textsc{\\huge GREENHOUSE GAS INVENTORY REPORT}\n" +
                "\\end{center}\n" +
                "\\begin{center}\n" +
                "\\textsc{\\Large Commercial Bank}\n" +
                "\\end{center}\n" +
                "\\begin{center}\n" +
                "\\textsc{\\Large FINANCIAL YEAR 2018/19} \n" +
                "\\end{center}\n" +
                "\\vspace{1cm}\n" +
                "\\endgroup\n" +
                "\n" +
                "\\includegraphics[scale=1, width=18cm,height=13cm, trim={2cm -3cm 0 0},  center]{images/home}\n" +
                "\n" +
                "\\includegraphics[width=0.2  \\textwidth,center,  trim={0 0 0 0}]{images/favicon}\n" +
                "\\endgroup\n" +
                "% \\end{titlepage}\n" +
                "\\pagestyle{fancy}\n" +
                "\n" +
                "\\newpage\n" +
                "\n" +
                "\\noindent\n" +
                "{\\Large This report was prepared for the sole purpose of the following authority: }\\newline \n" +
                "\\begin{center}\n" +
                "    \\includegraphics[height=4cm]{images/company_logo}\n" +
                "\\end{center}\n" +
                "\n" +
                "\\vspace{0.5cm}\n" +
                "\\begin{center}\n" +
                "    \\begingroup\n" +
                "      {\\large Commercial Bank \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large No-23,Lane,HeadOffice,Colombo \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Registration No: PV 19991e \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large E-mail: info@combank.com \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Telephone:  +94 (011)2 786 477\\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Fax: +94(011)2786477 \\hfill}  \\par  \\vspace{.2cm}\n" +
                "    \\endgroup\n" +
                "\\end{center}\n" +
                "\n" +
                "\n" +
                "\\vspace{0.5cm}\n" +
                "\\noindent\n" +
                "{\\Large Assessment was carried out and the report was prepared by: }\n" +
                "\\newline\n" +
                "\\begin{center}\n" +
                "    \\includegraphics[height=4cm]{images/favicon}\n" +
                "\\end{center}\n" +
                "\n" +
                "\\vspace{0.5cm}\n" +
                "\\begin{center}\n" +
                "    \\begingroup\n" +
                "      {\\large Climate Smart Initiatives (Pvt) Ltd  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large  No. 559D, D.P. Wijesinghe Mawatha, Pelawatta,\n" +
                "Battaramulla \\hfill}  \\par  \\vspace{.2cm}\n" +
                "% fold address\n" +
                "% -----------------------------------------------\n" +
                "      {\\large Registration No: PV 119397  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large E-mail: info@climatesi.com \n" +
                " \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Telephone: +94 (011)2 786 477, +94(0 71)3 750 113\\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Fax: +94(011)2786477  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "    \\endgroup\n" +
                "\\end{center}\n" +
                "\\vspace{.5cm}\n" +
                "\\noindent\n" +
                "{\\large Reporting period: Financial Year 2018/19 }\n" +
                "\\newpage\n" +
                "\\section*{EXECUTIVE SUMMARY}\n" +
                "\\paragraph{Greenhouse gas emissions and the resultant global warming is an overwhelming\n" +
                "environmental issue which was caused due to adding of greenhouse gases to the atmosphere\n" +
                "since the industrial revolution. Reducing atmospheric emissions which result GHG effect is\n" +
                "now a major task for the industries to become more environmentally friendly and conscious. \n" +
                "}\n" +
                "\n" +
                "\\paragraph{Company Name has decided to measure its atmospheric GHG emissions for the fourth\n" +
                "attempt to become more environmentally responsible and conscious business. }\n" +
                "\n" +
                "\\paragraph{This report details the quantification of greenhouse gas (GHG) emissions for Company Name\n" +
                "(Company Name) for the financial year (FY) 2018/19 (1st April 2018 – 31st March 2019) and\n" +
                "provides recommendations to reduce GHG emissions. The boundary of this assessment is\n" +
                "Company Address. This study quantifies and reports the organization (al level greenhouse gas\n" +
                "(GHG) emissions based on data received from Company Name in accordance with ISO 14064-\n" +
                "1-2018 and United Nations Intergovernmental Panel on Climate Change (IPCC) Fifth\n" +
                "Assessment Report (AR5). All GHG emissions were reported as tonne of CO2 equivalent\n" +
                "(tCO2e).}\n" +
                "\n" +
                "\\paragraph{Reporting of GHG emissions was more comprehensive as it covered range of indirect\n" +
                "emissions which has multiple emission sources not within the control of the organization in\n" +
                "addition to direct emissions. Data collection from Company Name was also quite complete,\n" +
                "but there is still room for improvement for indirect emissions data collection and reporting. }\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "% \\newline{}\n" +
                "% \\text{ \\Large Commercial Bank} \n" +
                "% \\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large No-1,323, jlsjfs, j2lj} \\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large Registration No: PV 19991} \\newline{} \n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large E-mail: info@combank.com}\\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large Telephone:  +94 (011)2 786 477}\\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large Fax: +94(011)2786477}\n" +
                "\n" +
                "~\\vfill\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "% \\pagestyle{fancy}\n" +
                "\\chapter{ljsflsjf}\n" +
                "\\tableofcontents\n" +
                "\\pagestyle{fancy}\n" +
                "\\chapter{Summary}\n" +
                "\\section{jlsjfslfj}\n" +
                "\\chapter{sjlfjsf}\n" +
                "% \\addcontentsline{toc}{chapter}{Introduction} \\markboth{INTRODUCTION}{} \n" +
                "\n" +
                "\n" +
                "\\end{document}");

        try {
            PrintWriter pw = new PrintWriter(url + ".tex", "UTF-8");
            pw.println(mainBuilder.toString());
            pw.close();
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        }

    }

    public static void writeLatex(
            String url,
            GetReportZipReqMsg message,
            boolean isFy,
            int yBegin,
            int yEnd,
            String baseYear,
            HashMap<String, String> emissionInfo,
            HashMap<String, Object> tableData) {
        StringBuilder builder = new StringBuilder();
        isFy = yBegin != yEnd;

        // preamble
        builder.append(WriteTex.genPreamble(message.getMetadata().getComName_header()));
        builder.append("\n");

        // front page

        String fy = ( yBegin == yEnd ? yBegin + "" : yBegin + "/" + (yEnd % 100));
        String startDate = "";
        String endDate = "";
        if (isFy){
            startDate = "1st April " + yBegin;
            endDate = "31st March " + yEnd;
        }else {
            startDate = "1st January " + yBegin;
            endDate = "31st March " + yEnd;
        }

        builder.append(WriteTex.genFrontPage(
                message.getMetadata().getComName_address(),
                isFy,
                fy));

        builder.append("\n");
        builder.append(genSecondPage(
                message.getMetadata().getComName_address(),
                message.getMetadata().getAddr1(),
                message.getMetadata().getAddr2(),
                message.getMetadata().getDistrict(),
                message.getMetadata().getEmail(),
                message.getMetadata().getTelphone(),
                message.getMetadata().getFax(),
                message.getMetadata().getComRegNo(),
                isFy,
                fy
        ));
        builder.append("\n");


        builder.append(genExecSummary(
                message.getMetadata().getComName_header(),
                message.getMetadata().getComName_address(),
                attempt(message.getMetadata().getAttempt()),
                isFy,
                fy,
                message.getMetadata().getYearStartDate(),
                message.getMetadata().getYearEndDate(),
                message.getMetadata().getAddr1() + ", " + message.getMetadata().getAddr2() + ", " + message.getMetadata().getDistrict(),
                emissionInfo.get("total"),
                emissionInfo.get("per_capita"),
                emissionInfo.get("intensity"),
                true,
                emissionInfo.get("prod")
        ));

        builder.append("\n");
        builder.append(genTableOfCon());

        builder.append("\n");
        builder.append(genGloassary());
        builder.append("\n");

        builder.append(genIntroduction(
                message.getMetadata().getComName_header(),
                message.getMetadata().getComName_address(),
                message.getMetadata().getProposedDate(),
                isFy,
                fy,
                startDate,
                endDate,
                message.getMetadata().getDesCompany(),
                message.getMetadata().getSrcResUnit(),
                message.getMetadata().getDesEnvironmentalMgmntFramework(),
                message.getMetadata().getSrcEnvMgmtFramework(),
                message.getMetadata().getDesResUnit(),
                isFy,
                baseYear,
                attempt(message.getMetadata().getAttempt())
        ));
        builder.append("\n");

        builder.append(genBoundaries(
                message.getMetadata().getComName_header(),
                message.getMetadata().getAddr1() + ", " + message.getMetadata().getAddr2() + ", " + message.getMetadata().getDistrict()));

        builder.append("\n");

        builder.append(genChapterQuantification(
                attempt(message.getMetadata().getAttempt()),
                message.getMetadata().getComName_header(),
                isFy,
                fy
        ));
        builder.append("\n");
        builder.append("\n");
        builder.append(genActivityData((List<HashMap<String, String>>) tableData.get("activity")));
        builder.append("\n");
        builder.append(genEmissionFactors());
        builder.append("\\section{Uncertainties }");
        ;
        builder.append("\\section{Exclusions}\n" +
                "\\newline\n" +
                "\\vspace{.2cm}\n");
        builder.append(genExclusions((List<HashMap<String, String>>) tableData.get("exclusion")));
        builder.append("\n");
        builder.append("\n \\chapter{RESULTS: GREENHOUSE GAS INVENTORY EVALUATION }");
        builder.append("\n");
        builder.append(ghgInv(
                (List<List<String>>)tableData.get("cat2"),
                (List<List<String>>)tableData.get("cat3"),
                (List<List<String>>)tableData.get("cat4"),
                (List<String>)tableData.get("td_cat"),
                "Category 2: Indirect\n" +
                        "Emissions from\n" +
                        "imported energy ",
                "category 4: Indirect\n" +
                        "emissions from\n" +
                        "services Used by\n" +
                        "the organization",
                "Category 6: Indirect\n" +
                        "emissions from\n" +
                        "other sources ",
                "Indirect emissions from T \\& D Loss from Imported\n" +
                        "Electricity",
                (List<List<String>>)tableData.get("gas_direct"),
                (List<String>)tableData.get("gas_direct_total"),
                (List<String>) tableData.get("gas_indirect_total"),
                (List<String>)tableData.get("gas_total"),
                (isFy ? fy: yBegin +"")

        ));
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append(genDirectGHGEmission(
                (List<HashMap<String, String>>) tableData.get("direct"),
                isFy,
                fy,
                message.getMetadata().getComName_header()

        ));
        builder.append("\n");
        builder.append("\n");
        builder.append(genInDirectGHGEmission(
                (List<HashMap<String, String>>) tableData.get("indirect"),
                isFy,
                fy,
                message.getMetadata().getComName_header()
        ));
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append(genChapterComparision(
                (List<List<String>>)tableData.get("ghg_com_indirect"),
                (List<List<String>>)tableData.get("ghg_com_direc"),
                (List<String>)tableData.get("ghg_com_years"),
                (List<String>)tableData.get("ghg_com_t_dir"),
                (List<String>)tableData.get("ghg_com_t_indir"),
                (List<String>)tableData.get("ghg_com_t_total"),
                (List<List<String>>)tableData.get("efstats"),
                isFy,
                fy,
                message.metadata.getIsoStandard(),
                message.metadata.getPreviousYearISOStandard()


        ));
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append(genChapterConclusion(
                message.metadata.getComName_header(),
                emissionInfo.get("total"),
                isFy,
                fy,
                message.metadata.getProd_ton(),
                message.metadata.getProducts(),
                Float.parseFloat(emissionInfo.get("prod")),
                message.metadata.getSuggestions(),
                message.metadata.getDesGHGMitigationActions(),
                message.metadata.getNextSteps()

        ));
        builder.append("\\newline");
        builder.append("\\newline");

        builder.append("\n");
        builder.append("\\end{document}");


        try {
            PrintWriter pw = new PrintWriter(url + ".tex", "UTF-8");
            pw.println(builder.toString());
            pw.close();
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        }
    }

    public static void zip(String zipUrl) {
        String out = zipUrl +".zip";
        String src = zipUrl;
        List<String> fileList = new ArrayList<>();

        addZipFiles(new File(zipUrl), fileList, src);
        zipIt(out, fileList, src);



    }


    private static void zipIt(String zipFile, List<String> fileList, String src) {
        byte[] buffer = new byte[1024];
        String source = new File(src).getName();
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            logger.info("Output to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file : fileList) {
                logger.info("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(src + File.separator + file);
                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();
            logger.info("Folder successfully compressed");

        } catch (IOException ex) {
            logger.error(ex);
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }

    private static void addZipFiles(File node, List<String> fileList, String src) {
        if (node.isFile()) {
            fileList.add(node.toString().substring(src.length() + 1, node.toString().length()));
        }

        if (node.isDirectory()){
            String [] subnode = node.list();
            for (String fileName: subnode) {
                addZipFiles(new File(node, fileName), fileList, src);
            }
        }
    }


    private static  String attempt(int attempt) {
        String rtn = "";
        switch (attempt) {
            case 1: {
                rtn = "First";
                break;
            }
            case 2: {
                rtn = "Second";
                break;
            }
            case 3: {
                rtn = "Third";
                break;
            }
            case 4: {
                rtn = "Fourth";
                break;
            }
            case 5: {
                rtn = "Fifth";
                break;
            }
            case 6: {
                rtn = "Sixth";
                break;
            }
            case 7: {
                rtn = "Seventh";
                break;
            }
            case 8: {
                rtn = "Eighth";
                break;
            }
            case 9: {
                rtn = "Ninth";
                break;
            }
            case 10: {
                rtn = "Tenth";
                break;
            }
            default: {
                rtn = attempt + "th";

            }
        }
        return rtn;
    }


    public static String genChapterQuantification(
            String attempt,
            String comName,
            boolean isFy,
            String fy
    ) {
        StringBuilder builder = new StringBuilder();
        builder.append("\\chapter{ QUANTIFICATION OF GHG EMISSIONS AND REMOVALS1}");
        builder.append("\n");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("This is the " + attempt + " " +(isFy ? "FY " : "Year") + " for which the GHG emissions were quantified in accordance with ISO\n" +
                "14064-1 and the Greenhouse Gas Protocol Corporate Accounting and Reporting Standard\n" +
                "(January, 2010). This leads to a more complete reporting of " + comName +"’s GHG emissions.\n" +
                "All GHG sources were identified in consultation with "+ comName + ". The identified,\n" +
                "categorized emission sources can be seen in section 2.2 (“Reporting Boundaries”) of this\n" +
                "report.\n" +
                "\\vspace{0.2cm}\n" +
                "\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Methodology}\n" +
                "The main greenhouse gases are carbon dioxide, methane and nitrous oxide. Carbon dioxide is\n" +
                "associated with electricity and fuel consumption. Methane and Nitrous oxide are formed in\n" +
                "small quantities from the combustion of diesel and petrol. As per international protocol, all the greenhouse gases (GHGs) are converted to carbon dioxide using global warming potentials (GWP) as per the IPCC 5th assessment report (C02 -1 , CH4 -28, N20- 265). GHG reporting is done as Carbon Dioxide equivalent (CO2e). \n" +
                "\\vspace{0.2cm}\n" +
                "\\newline\n" +
                "All calculations were done based on GHG activity data multiplied by an appropriate GHG\n" +
                "emission factor. Unless stated otherwise, all emission factors were obtained from the Intergovernmental Panel on Climate Change (IPCC). This approach makes it easier to compare the carbon footprint calculated in this report with other similar reports. \n" +
                "\\vspace{0.2cm}\n" +
                "\\newline\n" +
                "Collection of activity data was done primarily through the invoices, log books utility bills, ERP system and employee surveys. All these Hard copies of GHG records will be kept at least for 2 years before archiving. To avoid the double counting data discrepancy and patterns of changes were monitored. When uncertainties arise sample, surveys were carried out to get the real image. \n" +
                "\\vspace{0.2cm}\n" +
                "\\newline");
        builder.append("\n");
        builder.append( comName +  " may decide to get the overall footprint verified by an independent third-party\n" +
                "verification body, and get the assurance that its carbon footprint for " + (isFy ? "FY " + fy : fy) + " is not\n" +
                "materially misstated.");

        builder.append("\n");

        return builder.toString();
    }

    public static String genChapterComparision( List<List<String>> indirect,
                                                List<List<String>> direct,
                                                List<String> years,
                                                List<String> tDirect,
                                                List<String>tIndirect,
                                                List<String> totals,
                                                List<List<String>> efStats,
                                                boolean isFy,
                                                String year,
                                                String prevIso,
                                                String currentIso

                                                ) {




        StringBuilder builder = new StringBuilder();
        builder.append("\n \n");
        builder.append("\\chapter{Comparison of CFP report of " + (isFy ? "FY " + year : "Year " + year));
        builder.append(" with previous " + (isFy ? "Financial years "  : "Years ") + "(");
        for (String y : years) {
            if (!y.equals(year)) {
                builder.append( y);
                builder.append(" , ");
            }


        }
        builder.append(") CFP Report");
        builder.append("}");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Comparison of Whole Organizational GHG emissions}");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append(
                ghgComparision(
                        indirect,
                        direct,
                        years,
                        tDirect,
                        tIndirect,
                        totals
                )
        );

        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Comparison of the organizational Carbon Footprint over the Financial Years}");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,5) {\\includegraphics[width=12cm]{images/com_ghg_overyear}};\n" +
                "\\end{tikzpicture}\\caption{Comparison of overall emissions } \\label{fig:fig1}\\end{figure}");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Comparison of direct GHG emissions over the Financial Years}");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,5) {\\includegraphics[width=12cm]{images/direct_over_year}};\n" +
                "\\end{tikzpicture}\\caption{ Comparison of direct emissions by the financial year } \\label{fig:fig1}\\end{figure}");

        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Comparison of Indirect GHG emissions over the financial years}");
        builder.append("\\newline");
        builder.append("\\newline");

        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,5) {\\includegraphics[width=12cm]{images/indirect_over_year}};\n" +
                "\\end{tikzpicture}\\caption{Comparison of Indirect emissions by the financial year } \\label{fig:fig1}\\end{figure}");


        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Comparisons of Emission Factors & other Constant with previous years }");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append(efComparision(efStats, years));
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\\begin{itemize}\n" +
                "  \\item ");
        builder.append("The GHG inventory has been done for the ");
        builder.append( isFy ? "financial year ": "year ");
        builder.append(year);
        builder.append("according to " + currentIso);
        builder.append("and for the previous year it was done using the GHG protocol and " + prevIso);
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Comparisons of per capita emissions, emission intensity and per ton of\n" +
                "production emissions with previous years }");
        builder.append("\\newline");
        builder.append("\\newline");

        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,5) {\\includegraphics[width=12cm]{images/capita}};\n" +
                "\\end{tikzpicture}\\caption{Per capita emissions over the finance years } \\label{fig:fig1}\\end{figure}");
        builder.append("\\newline");
        builder.append("\\newline");

        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,5) {\\includegraphics[width=12cm]{images/intensity}};\n" +
                "\\end{tikzpicture}\\caption{Emission intensity over the financial years  } \\label{fig:fig1}\\end{figure}");
        builder.append("\\newline");
        builder.append("\\newline");

        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,5) {\\includegraphics[width=12cm]{images/per_prod}};\n" +
                "\\end{tikzpicture}\\caption{Emissions per ton of production over the financial years } \\label{fig:fig1}\\end{figure}");
        builder.append("\\newline");
        builder.append("\\newline");


        return builder.toString();

    }

    public static String ghgComparision( List<List<String>> indirect,
                                         List<List<String>> direct,
                                         List<String> years,
                                         List<String> tDirect,
                                         List<String>tIndirect,
                                         List<String> totals
    )
    {
        StringBuilder builder = new StringBuilder();
        int cols = years.size();
        float width = 0;
        if (cols < 5) {
            width =  2.1f;
        }else {
            width = 1.4f;
        }
        builder.append("\\begin{table}[]\n" +
                "    \\centering\n" +
                "     \\caption{ Comparison of GHG inventories over financial years}\n" +
                "    \\label{tab:my_label}\n" +
                "   \\begin{longtable}{?p{5.8cm}?");
        for (String year : years) {
            builder.append("p{" +width +"cm}");
        }

        builder.append("?}\n" +
                "\\rowcolor[RGB]{147, 183, 190}\n" +
                "{\\parafont Source} & \\multicolumn{ " +(cols) + "}{c|}{\\parafont GHG Emissions (tCO2e)}\\\\\n" +
                "\\rowcolor[RGB]{147, 183, 190}");
        builder.append("{} ");
        for (String y: years) {
            builder.append("& {" + y + "}");
        }
//        builder.append("\\\\");
        builder.append("\n \\hline\n");
        for (int i = 0; i < direct.size(); i++) {
            builder.append("\n");
            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }
            for (int j= 0; j <direct.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + direct.get(i).get(j));
                }else {
                    builder.append(direct.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");


        }

//        builder.append("\\\\");
        builder.append("\n \\hline \n");
//        builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
        builder.append("\\hline \n");
        builder.append("\\textbf{Total direct GHG emissions} ");
        for (int i = 0; i < cols; i++) {
            builder.append("& \\textbf{" + tDirect.get(i) + "}");
        }
        builder.append("\\\\");
        builder.append("\n \\hline \n");

        for (int i = 0; i < indirect.size(); i++) {
            builder.append("\n");
//            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
//            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
//            }
            for (int j= 0; j < indirect.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + indirect.get(i).get(j));
                }else {
                    builder.append(indirect.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");
        }
//        builder.append("\\\\");
        builder.append("\n \\hline \n");
//        builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
//        builder.append("\\hline \n");
        builder.append("\\textbf{Total Indirect GHG emissions} ");
        for (int i = 0; i < cols; i++) {
            builder.append("& \\textbf{" + tIndirect.get(i) + "}");
        }
//        builder.append("\\\\");
        builder.append("\n \\hline \n");

        builder.append("\\textbf{Total Emissions} ");

        for (int i = 0; i < cols; i++) {
            builder.append("& \\textbf{" + totals.get(i) + "}");
        }
        builder.append("\\\\");
        builder.append("\n \\hline \n");

        builder.append("\\hline \n");
        builder.append("\\end{longtable}");
        builder.append("\\end{table}");

        return builder.toString();

    }

    public static String genChapterConclusion(
            String comName,
            String total,
            boolean isfy,
            String year,
            float tons_prods,
            List<String> prooducts,
            float per_prod,
            List<String> suggestions,
            String desMitigation,
            List<String> nextSteps
    ) {
        StringBuilder builder = new StringBuilder();
        builder.append("\\chapter{ CONCLUSION \\& RECOMENDATIONS }");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("Carbon footprint measurement and reporting was conducted in accordance with ISO\n" +
                "14064:2018. The resultant carbon footprint of " + comName +" for " + (isfy ? "FY ": "Year ") + year+ " is " + total  +" tonnes\n" +
                "of CO2e. ");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline");
       builder.append("\n");
       builder.append("\n");

        if (prooducts.size() > 0) {
            builder.append("Total production for the same year was" +  tons_prods +  " tonnes of Name of the product (if applicable). The\n" +
                    "carbon footprint per tonne of production was determined to be " +per_prod + " tonnes CO\\textsubscript{2}e (considering\n" +
                    "direct and indirect emissions). ");
            builder.append("\\vspace{0.2cm}\n" +
                    "\\newline");
            builder.append("\n");
            builder.append("\n");
        }



        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");


        builder.append("We believe that this assessment and future carbon audits will complement the image of\n" +
                comName +" and will provide a sound basis for continuing the participating programmes\n" +
                "such as Carbon Disclosure Project and United Nations Carbon Neutral Now. In addition, we\n" +
                "also assume that this assessment will help " + comName + " in identifying emission reduction\n" +
                "opportunities.");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline");
        builder.append("\n");
        builder.append("\n");

        builder.append("\\newline");
        builder.append("\\newline");

        builder.append("\\section{ Recommendations" +
                "Information Management System }");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");

        builder.append("Company Name could improve the information management of carbon footprint by:");
        builder.append("\\newline");
        builder.append("\\begin{itemize}");
        for(String suggestion: suggestions) {
            builder.append("\\item " + suggestion);
            builder.append("\\vspace{0.2cm}");
        }
        builder.append("\\end{itemize}");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Ongoing GHG Mitigation and Removal Enhancement Projects}");
        builder.append("\\newline");
        builder.append("\\newline");

        builder.append( comName + " complements its commitment to carbon neutrality through several ongoing\n" +
                "initiatives.");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append(desMitigation);
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\n");
        builder.append("\n");
        builder.append("\\section{Next Steps}");
        builder.append("\\newline");
        builder.append("\\newline");
        builder.append("\\begin{itemize}");

        for(String next : nextSteps) {
            builder.append("\\item " + next);
            builder.append("\\vspace{0.2cm}");
        }
        return builder.toString();
    }
}
