package com.climatesi.ghg.utility.api.enums;

// Todo: have to change
public enum EntityID {
    CUSTOMER(1),
    USER(2),
    CURRENCY(3),
    COUNTRY(4),
    CITY(5),
    INSTITUTION(6),
    DEPARTMENT(7),
    UNKNOWN(-1);

    int id;

    private EntityID(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public static EntityID getEnum(int id) {
        switch (id) {
            case 1:
                return CUSTOMER;
            case 2:
                return USER;
            case 3:
                return CURRENCY;
            case 4:
                return COUNTRY;
            case 5:
                return CITY;
            case 6:
                return INSTITUTION;
            case 7:
                return DEPARTMENT;
            default:
                return UNKNOWN;
        }
    }
}