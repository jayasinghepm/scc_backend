package com.climatesi.ghg.utility.api.enums;

public enum BioMassTypeEnum {

    WoodChips(1),
    SawDust(2),
    OtherSolid(3);

    private int type;

    BioMassTypeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
