package com.climatesi.ghg.utility.api.dao;

import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.climatesi.ghg.utility.api.exceptions.OperationNotSupportException;

import javax.persistence.EntityManager;
import java.util.Map;

public class AbstractViewOnlyDAO<C,I> extends AbstractDAO<C, I> {

    public AbstractViewOnlyDAO(EntityManager em, Class<C> entityImplClass) {
        super(em, entityImplClass);
    }

    @Override
    public Map<String, Object> getAddSpParams(C entityImpl) throws GHGException {
        throw new OperationNotSupportException("Does not allowed to add via this API");
    }

    @Override
    public Map<String, Object> getEditSpParams(C entityImpl) throws GHGException {
        throw new OperationNotSupportException("Does not allowed to edit via this API");
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(C entityImpl) throws GHGException {
        throw new OperationNotSupportException("Does not allowed to change status via this API");
    }
}
