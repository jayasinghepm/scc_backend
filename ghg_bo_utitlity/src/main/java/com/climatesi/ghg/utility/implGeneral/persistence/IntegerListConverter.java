package com.climatesi.ghg.utility.implGeneral.persistence;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Converter
public class IntegerListConverter implements AttributeConverter<List<Integer>, String> {

    @Override
    public String convertToDatabaseColumn(List<Integer> list) {
        if (list == null) {
            return "";
        }
        List<String> stringList = new ArrayList<>();
        list.forEach(i -> {
            stringList.add(i.toString());
        });
        return String.join(",", stringList);


    }

    @Override
    public List<Integer> convertToEntityAttribute(String joined) {
        if (joined == null || joined.equals("")) {
            return new ArrayList<>();
        }
        List<String> stringList = new ArrayList<>(Arrays.asList(joined.split(",")));
        List<Integer> integers = new ArrayList<>();
        stringList.forEach(s -> {
            integers.add(Integer.parseInt(s));
        });
        return integers;
    }

}

