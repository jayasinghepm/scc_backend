package com.climatesi.ghg.utility.implGeneral.report;

import org.jboss.logging.Logger;

import java.awt.geom.FlatteningPathIterator;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class WriteTex {

    public static Logger logger = Logger.getLogger(WriteTex.class);

    public static String writeStructure(String url) {
        StringBuilder structureTex = new StringBuilder();
        structureTex.append("\\usepackage{titlesec} \\usepackage{graphicx} % Required for including pictures\n" +
                "\\graphicspath{{images/}} % Specifies the directory where pictures are stored\n" +
                "\n" +
                "\\usepackage{tikz} % Required for drawing custom shapes\n" +
                "\\usepackage[english]{babel} % English language/hyphenation\n" +
                "\\usepackage{enumitem} % Customize lists\n" +
                "\\setlist{nolistsep} % Reduce spacing between bullet points and numbered lists\n" +
                "\n" +
                "\\usepackage{booktabs} % Required for nicer horizontal rules in tables\n" +
                "\n" +
                "\\usepackage{eso-pic} % Required for specifying an image background in the title page\n" +
                "\n" +
                "\\usepackage{titletoc} % Required for manipulating the table of contents\n" +
                "\\contentsmargin{0cm} % Removes the default margin");

        return structureTex.toString();


    }

    public static String genPreamble(String comName) {
        StringBuilder builder = new StringBuilder();

        builder.append("\\documentclass[fleqn, 20pt,a4paper]{book}\n" +
                "\n" +
                "\\usepackage[top=3cm,bottom=2.5cm,left=2.5cm,right=2.5cm,headsep=10pt,a4paper, headheight=5cm]{geometry}\n" +
                "\n" +
                "% Font Settings\n" +
                "\\usepackage{avant} % Use the Avantgarde font for headings\n" +
                "\\usepackage{mathptmx} % Use the Adobe Times Roman as the default text font together with math symbols from the Sym\u00ADbol, Chancery and Com\u00ADputer Modern fonts\n" +
                "\\usepackage{microtype} % Slightly tweak font spacing for aesthetics\n" +
                "\\usepackage[utf8]{inputenc} % Required for including letters with accents\n" +
                "\\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs\n" +
                "\\usepackage{tabularx} % in the preamble\n" +
                " \\usepackage{amsmath,amsthm, amssymb, latexsym}\n" +
                " \\usepackage{tabularx}\n" +
                " \n" +
                " \\usepackage[table]{xcolor}\n" +
                " \\usepackage{longtable}\n" +
                " \\usepackage{tabu}\n" +
                "\\usepackage{array}\n" +
                "\\newcolumntype{?}{!{\\vrule width 1pt}}\n" +
                "\\usepackage{multirow}\n" +
                "%  \n" +
                "\\usepackage{caption}\n" +
                "\n" +
                "\n" +
                "% Index\n" +
                "\\usepackage{calc} % For simpler calculation - used for spacing the index letter headings correctly\n" +
                "\\usepackage{makeidx} % Required to make an index\n" +
                "\\makeindex % Tells LaTeX to create the files required for indexing\n" +
                "\\usepackage{verbatim}\n" +
                "\n" +
                "\\usepackage[export]{adjustbox}\n" +
                "\n" +
                "\\usepackage{graphicx}\n" +
                "\\usepackage{tabulary}\n" +
                "\n" +
                "\\usepackage{seqsplit}\n" +
                "\n" +
                "\\makeatletter\n" +
                "\\def\\@makechapterhead#1{%\n" +
                "  \\vspace*{20\\p@}%\n" +
                "  {\\parindent \\z@ \\raggedright \\normalfont\n" +
                "    \\interlinepenalty\\@M\n" +
                "    \\Huge\\bfseries  \\thechapter.\\quad #1\\par\\nobreak\n" +
                "    \\vskip 40\\p@\n" +
                "  }}\n" +
                "\\makeatother\n" +
                "\n" +
                "\n" +
                "\\usepackage{fontspec}\n" +
                "\\setmainfont[Ligatures=TeX]{Arial}\n" +
                "\\setsansfont[Ligatures=TeX]{Georgia}\n" +
                "\n" +
                "\n" +
                "\n" +
                "\\input{structure} % Insert the commands.tex file which contains the majority of the structure behind the template\n\n");

        builder.append(genHeaderFooter(comName));

        builder.append("\\usepackage{array}   %% tables content alignment\n" +
                "\\newcolumntype{L}[1]{>{\\raggedright\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}m{#1}}\n" +
                "\\newcolumntype{C}[1]{>{\\centering\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}m{#1}}\n" +
                "\\newcolumntype{R}[1]{>{\\raggedleft\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}m{#1}}\n" +
                "\n" +
                "\n" +
                "\n" +
                "\\newcommand{\\bigsize}{\\fontsize{16pt}{20pt}\\selectfont}\n" +
                "\\newcommand{\\parafont}{\\fontsize{12pt}{14pt}\\selectfont}\n" +
                "\n" +
                "\\newcolumntype{b}{X}\n" +
                "\\newcolumntype{s}{>{\\hsize=.8\\hsize}X}\n" +
                "\n" +
                "\\usepackage{hyperref}\n" +
                "\\catcode`\\_=12\n" +
                "\\usepackage{makecell}"+
                "\\usepackage{ltablex}\n" +
                "\n");

        return builder.toString();
    }

    public static void writeMainTex(String url) {
        StringBuilder mainBuilder = new StringBuilder();
        mainBuilder.append("\\documentclass[fleqn, 16pt,a4paper]{book}\n" +
                "\n" +
                "\\usepackage[top=3cm,bottom=2.5cm,left=2.5cm,right=2.5cm,headsep=10pt,a4paper, headheight=5cm]{geometry}\n" +
                "\n" +
                "% Font Settings\n" +
                "\\usepackage{avant} % Use the Avantgarde font for headings\n" +
                "\\usepackage{mathptmx} % Use the Adobe Times Roman as the default text font together with math symbols from the Sym\u00ADbol, Chancery and Com\u00ADputer Modern fonts\n" +
                "\\usepackage{microtype} % Slightly tweak font spacing for aesthetics\n" +
                "\\usepackage[utf8]{inputenc} % Required for including letters with accents\n" +
                "\\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs\n" +
                "\n" +
                " \\usepackage{amsmath,amsthm, amssymb, latexsym}\n" +
                "\n" +
                "% Index\n" +
                "\\usepackage{calc} % For simpler calculation - used for spacing the index letter headings correctly\n" +
                "\\usepackage{makeidx} % Required to make an index\n" +
                "\\makeindex % Tells LaTeX to create the files required for indexing\n" +
                "\\usepackage{verbatim}\n" +
                "\n" +
                "\\usepackage[export]{adjustbox}\n" +
                "\n" +
                "\\usepackage{graphicx}\n" +
                "\\usepackage{tabulary}\n" +
                "\n" +
                "\\usepackage{seqsplit}\n" +
                "\n" +
                "\\input{structure} % Insert the commands.tex file which contains the majority of the structure behind the template\n" +
                "\n" +
                "\\usepackage{xcolor}\n" +
                "\\usepackage{fancyhdr}\n" +
                "\\renewcommand{\\headrulewidth}{0.5pt}\n" +
                "\\renewcommand{\\headrule}{\\hbox to\\headwidth{\\color{red}\\leaders\\hrule height \\headrulewidth\\hfill}}\n" +
                "\\renewcommand{\\footrulewidth}{0.5pt}\n" +
                "\\renewcommand{\\footrule}{\\hbox to\\headwidth{\\color{red}\\leaders\\hrule height \\footrulewidth\\hfill}}\n" +
                "\\pagestyle{fancy}\n" +
                "\\lhead{\\vspace{1.5 \\baselineskip }Greenhouse Gas Inventory Report – Company Name}\n" +
                "\\rhead{\\includegraphics[width=3cm]{images/company_logo}}\n" +
                "\\rfoot{\\includegraphics[width=2cm]{images/favicon}}\n" +
                "\\cfoot{\\vspace{-2.5\\baselineskip}Climate Smart Initiatives (Pvt) Ltd }\n" +
                "\\fancyfoot[l]{\\vspace{-2.5\\baselineskip}\\thepage}\n" +
                "\n" +
                "% Redefine the plain page style\n" +
                "\\fancypagestyle{plain}{%\n" +
                "%   \\fancyhf{}%\n" +
                "%   \\fancyfoot[C]{\\thepage\\ of \\pageref{LastPage}}%\n" +
                "%   \\renewcommand{\\headrulewidth}{0pt}% Line at the header invisible\n" +
                "%   \\renewcommand{\\footrulewidth}{0.4pt}% Line at the footer visible\n" +
                "\\lhead{\\vspace{-2.5 \\baselineskip}Greenhouse Gas Inventory Report – Company Name}\n" +
                "\\rhead{\\includegraphics[width=3cm]{images/company_logo}}\n" +
                "\\fancyfoot[r]{\\includegraphics[width=2cm]{images/favicon}}\n" +
                "\\cfoot{\\vspace{-2.5\\baselineskip}Climate Smart Initiatives (Pvt) Ltd }\n" +
                "\\fancyfoot[l]{\\vspace{-2.5\\baselineskip}\\thepage}\n" +
                "}\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\\usepackage{hyperref}\n" +
                "\\catcode`\\_=12\n" +
                "\\usepackage{makecell}"+
                "\\begin{document}\n" +
                "\\begingroup\n" +
                "\\let\\cleardoublepage\\clearpage\n" +
                "\n" +
                "\\thispagestyle{empty}\n" +
                "% \\begingroup\n" +
                "%\n" +
                "% \\AddToShipoutPicture*{\\put(15,200){\\includegraphics[scale=1, width=20cm,height=40cm,keepaspectratio]{images/home}}} % Image background\n" +
                "% \\endgroup\n" +
                "% \\begin{titlepage}\n" +
                "\\includegraphics[width=.3\\textwidth,right]{images/company_logo}\n" +
                "\n" +
                "\\begingroup\n" +
                "\\vspace{1cm}\n" +
                "\\begin{center}\n" +
                "\\textsc{\\huge GREENHOUSE GAS INVENTORY REPORT}\n" +
                "\\end{center}\n" +
                "\\begin{center}\n" +
                "\\textsc{\\Large Commercial Bank}\n" +
                "\\end{center}\n" +
                "\\begin{center}\n" +
                "\\textsc{\\Large FINANCIAL YEAR 2018/19} \n" +
                "\\end{center}\n" +
                "\\vspace{1cm}\n" +
                "\\endgroup\n" +
                "\n" +
                "\\includegraphics[scale=1, width=18cm,height=13cm, trim={2cm -3cm 0 0},  center]{images/home}\n" +
                "\n" +
                "\\includegraphics[width=0.2  \\textwidth,center,  trim={0 0 0 0}]{images/favicon}\n" +
                "\\endgroup\n" +
                "% \\end{titlepage}\n" +
                "\\pagestyle{fancy}\n" +
                "\n" +
                "\\newpage\n" +
                "\n" +
                "\\noindent\n" +
                "{\\Large This report was prepared for the sole purpose of the following authority: }\\newline \n" +
                "\\begin{center}\n" +
                "    \\includegraphics[height=4cm]{images/company_logo}\n" +
                "\\end{center}\n" +
                "\n" +
                "\\vspace{0.5cm}\n" +
                "\\begin{center}\n" +
                "    \\begingroup\n" +
                "      {\\large Commercial Bank \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large No-23,Lane,HeadOffice,Colombo \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Registration No: PV 19991e \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large E-mail: info@combank.com \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Telephone:  +94 (011)2 786 477\\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Fax: +94(011)2786477 \\hfill}  \\par  \\vspace{.2cm}\n" +
                "    \\endgroup\n" +
                "\\end{center}\n" +
                "\n" +
                "\n" +
                "\\vspace{0.5cm}\n" +
                "\\noindent\n" +
                "{\\Large Assessment was carried out and the report was prepared by: }\n" +
                "\\newline\n" +
                "\\begin{center}\n" +
                "    \\includegraphics[height=4cm]{images/favicon}\n" +
                "\\end{center}\n" +
                "\n" +
                "\\vspace{0.5cm}\n" +
                "\\begin{center}\n" +
                "    \\begingroup\n" +
                "      {\\large Climate Smart Initiatives (Pvt) Ltd  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large  No. 559D, D.P. Wijesinghe Mawatha, Pelawatta,\n" +
                "Battaramulla \\hfill}  \\par  \\vspace{.2cm}\n" +
                "% fold address\n" +
                "% -----------------------------------------------\n" +
                "      {\\large Registration No: PV 119397  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large E-mail: info@climatesi.com \n" +
                " \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Telephone: +94 (011)2 786 477, +94(0 71)3 750 113\\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Fax: +94(011)2786477  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "    \\endgroup\n" +
                "\\end{center}\n" +
                "\\vspace{.5cm}\n" +
                "\\noindent\n" +
                "{\\large Reporting period: Financial Year 2018/19 }\n" +
                "\\newpage\n" +
                "\\section*{EXECUTIVE SUMMARY}\n" +
                "\\paragraph{Greenhouse gas emissions and the resultant global warming is an overwhelming\n" +
                "environmental issue which was caused due to adding of greenhouse gases to the atmosphere\n" +
                "since the industrial revolution. Reducing atmospheric emissions which result GHG effect is\n" +
                "now a major task for the industries to become more environmentally friendly and conscious. \n" +
                "}\n" +
                "\n" +
                "\\paragraph{Company Name has decided to measure its atmospheric GHG emissions for the fourth\n" +
                "attempt to become more environmentally responsible and conscious business. }\n" +
                "\n" +
                "\\paragraph{This report details the quantification of greenhouse gas (GHG) emissions for Company Name\n" +
                "(Company Name) for the financial year (FY) 2018/19 (1st April 2018 – 31st March 2019) and\n" +
                "provides recommendations to reduce GHG emissions. The boundary of this assessment is\n" +
                "Company Address. This study quantifies and reports the organization (al level greenhouse gas\n" +
                "(GHG) emissions based on data received from Company Name in accordance with ISO 14064-\n" +
                "1-2018 and United Nations Intergovernmental Panel on Climate Change (IPCC) Fifth\n" +
                "Assessment Report (AR5). All GHG emissions were reported as tonne of CO2 equivalent\n" +
                "(tCO2e).}\n" +
                "\n" +
                "\\paragraph{Reporting of GHG emissions was more comprehensive as it covered range of indirect\n" +
                "emissions which has multiple emission sources not within the control of the organization in\n" +
                "addition to direct emissions. Data collection from Company Name was also quite complete,\n" +
                "but there is still room for improvement for indirect emissions data collection and reporting. }\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "% \\newline{}\n" +
                "% \\text{ \\Large Commercial Bank} \n" +
                "% \\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large No-1,323, jlsjfs, j2lj} \\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large Registration No: PV 19991} \\newline{} \n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large E-mail: info@combank.com}\\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large Telephone:  +94 (011)2 786 477}\\newline{}\n" +
                "% \\vspace{.5cm}\n" +
                "% \\text{\\Large Fax: +94(011)2786477}\n" +
                "\n" +
                "~\\vfill\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "% \\pagestyle{fancy}\n" +
                "\\chapter{ljsflsjf}\n" +
                "\\tableofcontents\n" +
                "\\pagestyle{fancy}\n" +
                "\\chapter{Summary}\n" +
                "\\section{jlsjfslfj}\n" +
                "\\chapter{sjlfjsf}\n" +
                "% \\addcontentsline{toc}{chapter}{Introduction} \\markboth{INTRODUCTION}{} \n" +
                "\n" +
                "\n" +
                "\\end{document}");

        try {
            PrintWriter pw = new PrintWriter(url + ".tex", "UTF-8");
            pw.println(mainBuilder.toString());
            pw.close();
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        }

    }


    public static String genBoundaries(
            String comName,
            String addr
    ) {
        StringBuilder builder = new StringBuilder();
        builder.append("\\chapter{BOUNDARIES }\n" +
                "The first step in calculation of carbon footprint is to set the boundary. This is important as it determines which sources and sinks of the organization must be included in the footprint calculation and which are to be excluded. ");
        builder.append("\\section{Setting up the organizational boundaries}\n" +
                "ISO 14064-1:2018 standard allows the setting of organizational boundaries on either the\n" +
                "control approach or the equity shareholding approach. According to the control approach, all\n" +
                "emissions and removals from the facilities over which it has financial or operational control should be accounted. According to the shareholding approach; emissions of the entities in which the organization has a share must be counted in proportionate to the shareholding.");
        builder.append("\\subsection{Control approach}");
        builder.append("Under the control approach, each entity accounts for 100 percent of the GHG emissions from\n" +
                "operations over which it has control. It does not account for GHG emissions from operations\n" +
                "where it has an interest but has no control. Control can be defined either financial or\n" +
                "operational terms. When using the control approach to consolidate GHG emissions,\n" +
                "companies shall choose between either the operational control or financial control criteria.");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("In accordance with the ISO standard, the organizational boundaries were drawn around all\n" +
                "the business operations controlled by " + comName + ". Organizational boundary chosen for\n" +
                "calculating the carbon footprint was " + addr + ". ");
        builder.append("\\section{Reporting boundaries}\n" +
                "Under the reporting boundaries organization shall establish and document the direct and\n" +
                "indirect GHG emissions and removals associated with the organization’s operations. ");
        builder.append("\\subsection{ Direct GHG emissions }\n" +
                "The direct GHG emissions should be quantified and reported separately for all GHGs in tonne\n" +
                "of CO2e.");
        builder.append("Indirect GHG emissions accure as a consequence of the activities of the company, by sources\n" +
                "owned or controlled by the company. ");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline");
        builder.append("Indirect GHG emissions are emissions that accure as a consequence of the activities of the\n" +
                "company but at sources owned or controlled by another company or person. These indirect\n" +
                "emissions have been selected according to the materiality to the company operations. Within\n" +
                "the operational boundary of " + comName +", the emissions associated with the following\n" +
                "activities were quantified and reported.");

//        todo: add figure

        return builder.toString();
    }

    public static String genIntroduction(
            String comName,
            String comNameFull,
            String proposedDate,
            boolean fy,
            String year,
            String yearStart,
            String yearEnd,
            String desCom,
            String srcSus,
            String desEnv,
            String srcEnv,
            String desResUnit,
            boolean baseFy,
            String baseYear,
            String attempt

    ) {
        StringBuilder builder = new StringBuilder();
        builder.append("\\newpage\n" +
                "\n" +
                "\\chapter{INTRODUCTION}");
        builder.append("Rapid rise of GHGs in the atmosphere with the industrial revolution and present bad industrial\n" +
                "practices cause temperature increase of the globe in a way which is not accepted by the\n" +
                "nature. Global warming negatively affects the environment in different ways such as\n" +
                "degradation and loss of biodiversity, melting of glaciers and sea level rise, extinction of species\n" +
                "and cause a huge risk to human wellbeing.");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("Quantification of GHGs released by an industry is now appeared as the best practice to take\n" +
                "the corrective actions to mitigate the climate change. Quantification of carbon footprint\n" +
                "provides number of key benefits to the organization:");
        builder.append("\\begin{center}\n" +
                "    \\begin{itemize}\n" +
                " \\renewcommand{\\labelitemi}{\\scriptsize$\\blacksquare$}\n" +
                " \\item  Get a good understanding about its impacts on climate change \n" +
                " \\vspace{0.2cm}\n" +
                " \\item  Develop key performance indicators for carbon emissions management and energy\n" +
                "use \n" +
                " \\vspace{0.2cm}\n" +
                " \\item  Maintain a higher rank among other competitive industries showing its commitment\n" +
                "towards sustainable business \n" +
                " \\vspace{0.2cm}\n" +
                " \\item  Meet stakeholders demand to address the imperative corporate responsibility of\n" +
                "environmental conservation\n" +
                " \\vspace{0.2cm}\n" +
                " \\item  Develop carbon management plan to make real emission reduction through supply\n" +
                "chain and production\n" +
                " \\vspace{0.2cm}\n" +
                "\\end{itemize}\n" +
                "\\end{center}\n");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("This report quantifies and reports the GHG emissions under the operational control of\n" +
                " " + comNameFull + " (“ " +comName + "”)and it has been prepared and submitted in line with\n" +
                "Climate Smart Initiatives (Pvt) Ltd (“ClimateSI”)’s proposal dated " + proposedDate + " and various discussions\n" +
                "held between " + comName + " and ClimateSI.");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("The operational carbon footprint for the " + (fy ? "financial year": "year") + year+ " (" + yearStart + " - " + yearEnd + ")" +
                " is covered in this report."
        );
        builder.append( (fy ? "Financial year ": "Year ") + " carbon footprint provides a more recent picture\n" +
                "of the carbon performance of the company and can be compared with the carbon footprint\n" +
                "of other companies. The " +  (fy ? " financial year ": " year ") + "carbon footprint is calculated based on the activity data\n" +
                "provided by " + comName + ". ");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("Principles of the ISO standard (ISO 14064-1 - “Specification with guidance at the organizational\n" +
                "level for the quantification and reporting of greenhouse gas emissions and removals”) were\n" +
                "applied while quantifying and reporting the greenhouse gas (GHG) emissions.");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("\\section{Introduction to the organization}\n" +
                desCom);
        builder.append("\\section{Persons Responsible}\n" +
                "\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture}\n" +
                "\\node [] at (8,4) {\\includegraphics[width=12cm]{images/sus_gov_stru}};\n" +
                "\\end{tikzpicture}\n" +
                "\\caption{Sustainability Governance Structure}\n" +
                "\\label{fig:fig3}");
        builder.append("\\captionsetup{font={footnotesize,bf,it}}\n" +
                "   \\caption*{ " + srcSus + "}\n" +
                "\\end{figure}\n" +
                "\\newline\n");
        builder.append(desResUnit);
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("\\section{Dissemination policy}\n" +
                comName +" has a comprehensive Environmental Management Policy and Environmental\n" +
                "Management system which addresses different areas of priority using well organized\n" +
                "environmental management framework.");
        builder.append("\\newline\n" +
                "\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture}\n" +
                "\\node [] at (8,4) {\\includegraphics[width=12cm]{images/env_frm}};\n" +
                "\\end{tikzpicture}\n" +
                "\\caption{ Environmental Management Framework }\n" +
                "\\label{fig:fig4}\n" +
                "\\captionsetup{font={footnotesize,bf,it}}");
        builder.append(" \\caption*{" + srcEnv +"}");
        builder.append("\\end{figure}\n" +
                "\\newline\n");
        builder.append(desEnv);

        builder.append("\\section{ Purpose of the Report}");
        builder.append("This GHG Inventory quantifies " +comName + "’s total Greenhouse Gas (GHG) emissions for\n" +
                "the" + (fy ? "FY " : "Year ") + year+ ", by accurately measuring the GHG emissions associated with its operations. ");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append(" The main purpose of this report is to figure out the amount of GHG emissions during the\n" +
                "financial year and observe the trends in GHG emissions over the past financial years. Also, this\n" +
                "detail assessment helps the company to set emission and emission removal targets for the\n" +
                "next financial year or years to reduce their emissions.");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("\\section{Reporting period and frequency of reporting }");
        builder.append("This report quantifies the GHG emissions resulted in the " + (fy ? "financial year ": "year ") +  year + "(" + yearStart +
                "- " + yearEnd + "). Organization does this annually as a best practice in moving towards\n" +
                "sustainability. ");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("\\section{Base Year}");
        builder.append("As this is the " + attempt  + " attempt to quantify the carbon footprint taking into account all direct and\n" +
                "indirect emissions in line with ISO 14064-1 standard, " +  (baseFy ? "financial year " : "year ") + baseYear + " is considered\n" +
                "as the base year of GHG Inventory preparation. ");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("\\section{Data and information (List of GHGs account) }");
        builder.append("Greenhouse Gas emissions assessment quantifies the total greenhouse gases (GHGs)\n" +
                "produced directly and indirectly from an organization’s activities within a specified time\n" +
                "frame. It quantifies all seven greenhouse gases where applicable and measures in units of\n" +
                "carbon dioxide equivalent, or CO2e. The seven gases are Carbon Dioxide (CO2), Methane (CH4)\n" +
                "Nitrous Oxide (N2O), Hydrofluorocarbons (HFCs), Sulphur hexafluoride (SF6), Perfluorocarbons\n" +
                "(PFCs) and Nitrogen Trifluoride (NF3), which are identified by the Kyoto Protocol as most\n" +
                "harmful gasses that have major impact on climate change and compulsory to report under\n" +
                "ISO 14064-1:2018.");
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");


        return builder.toString();
    }



    public static String genFrontPage(String comName, boolean fy , String year) {
        StringBuilder builder = new StringBuilder();
        builder.append("\\begin{document}\n" +
                "\\begingroup\n" +
                "\\let\\cleardoublepage\\clearpage\n" +
                "\n" +
                "\\thispagestyle{empty}\n" +
                "\\includegraphics[width=.3\\textwidth,right]{images/company_logo}\n" +
                "\n" +
                "\\begingroup\n" +
                "\\vspace{1cm}\n" +
                "\\begin{center}\n" +
                "\\textsc{\\huge GREENHOUSE GAS INVENTORY REPORT}\n" +
                "\\end{center}\n" +
                "\\begin{center}");
        builder.append("\\textsc{\\Large " + comName + "}\n" +
                "\\end{center}\n" +
                "\\begin{center}");
        builder.append("\\textsc{\\Large" + (fy ? "  FINANCIAL YEAR  ": "  YEAR  " ) +  year + "} \n" +
                "\\end{center}\n" +
                "\\vspace{1cm}\n" +
                "\\endgroup");
        builder.append("\\includegraphics[scale=1, width=18cm,height=13cm, trim={2cm -3cm 0 0},  center]{images/home}\n");
        builder.append("\\includegraphics[width=0.2  \\textwidth,center,  trim={0 0 0 0}]{images/favicon}\n" +
                "\\endgroup\n" +
                "\\pagestyle{fancy}\n" +
                "\n" +
                "\\newpage");
        return builder.toString();
    }

    public static String genSecondPage(
            String comName,
            String addr1,
            String addr2,
            String district,
            String email,
            String telephone,
            String fax,
            String regNo,
            boolean fy,
            String year
    ) {
        StringBuilder builder = new StringBuilder();

        builder.append("\\noindent\n" +
                "{\\Large This report was prepared for the sole purpose of the following authority: }\\newline \n" +
                "\\begin{center}\n" +
                "    \\includegraphics[height=4cm]{images/company_logo}\n" +
                "\\end{center}\n" +
                "\n" +
                "\\vspace{0.5cm}");
        builder.append("\\begin{center}\n" +
                "    \\begingroup");
        builder.append(" {\\large  " + comName + " \\hfill}  \\par  \\vspace{.2cm}");
        builder.append("{\\large " + addr1 + "," + addr2 + "," + district + " \\hfill}  \\par  \\vspace{.2cm}");
        builder.append("{\\large Registration No: " +  regNo + " \\hfill}  \\par  \\vspace{.2cm}");
        builder.append("{\\large E-mail: " + email + " \\hfill}  \\par  \\vspace{.2cm}");
        builder.append("{\\large Telephone: " + telephone + " \\hfill}  \\par  \\vspace{.2cm}");
        builder.append(" {\\large Fax: " + fax + " \\hfill}  \\par  \\vspace{.2cm}");
        builder.append(" \\endgroup\n" +
                "\\end{center}");
        builder.append("\\vspace{0.5cm}\n" +
                "\\noindent\n" +
                "{\\Large Assessment was carried out and the report was prepared by: }\n" +
                "\\newline\n" +
                "\\begin{center}\n" +
                "    \\includegraphics[height=4cm]{images/favicon}\n" +
                "\\end{center}\n" +
                "\n" +
                "\\vspace{0.5cm}\n" +
                "\\begin{center}\n" +
                "    \\begingroup\n" +
                "      {\\large Climate Smart Initiatives (Pvt) Ltd  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large  No. 559D, D.P. Wijesinghe Mawatha, Pelawatta,\n" +
                "Battaramulla \\hfill}  \\par  \\vspace{.2cm}");
        builder.append(" {\\large Registration No: PV 119397  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large E-mail: info@climatesi.com \n" +
                " \\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Telephone: +94 (011)2 786 477, +94(0 71)3 750 113\\hfill}  \\par  \\vspace{.2cm}\n" +
                "      {\\large Fax: +94(011)2786477  \\hfill}  \\par  \\vspace{.2cm}\n" +
                "    \\endgroup\n" +
                "\\end{center}\n" +
                "\\vspace{.5cm}\n" +
                "\\noindent");
        builder.append("{\\large Reporting period: " + (fy ? " Financial Year " : " Year ") +  year + " }");
        builder.append("\\newpage");


        return builder.toString();
    }

    public static String genExecSummary(
            String comName,
            String comNameFull,
            String attempt,
            boolean fy,
            String year,
            String yearStart,
            String yearEnd,
            String address,
            String emission,
            String perCapita,
            String intensity,
            boolean isProdAvailable,
            String prod


    ) {
        StringBuilder builder = new StringBuilder();
        builder.append("\\section*{EXECUTIVE SUMMARY}\n" +
                " Greenhouse gas emissions and the resultant global warming is an overwhelming"+
                " environmental issue which was caused due to adding of greenhouse gases to the" +
                " atmosphere since the industrial revolution. Reducing atmospheric emissions which" +
                " result GHG effect is now a major task for the industries to become more environmentally friendly and conscious.\n" +
                " \\vspace{0.2cm}\n" +
                " \\newline\n");
        builder.append(comName + " has decided to measure its atmospheric GHG emissions for " +  attempt+" \n" +
                "attempt to become more environmentally responsible and conscious business. \n" +
                "\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("This report details the quantification of greenhouse gas (GHG) emissions for " + comName + "(" +comNameFull +") for the " + (fy ? " financial year(FY) ": " year ") + year + " ( " + yearStart + " – " + yearEnd + ")");
        builder.append("and provides recommendations to reduce GHG emissions. The boundary of this assessment is ");
        builder.append(address);
        builder.append(". This study quantifies and reports the organization (al level greenhouse gas\n" +
                "(GHG) emissions based on data received from " + comName + " in accordance with ISO 14064-\n" +
                "1-2018 and United Nations Intergovernmental Panel on Climate Change (IPCC) Fifth\n" +
                "Assessment Report (AR5). All GHG emissions were reported as tonne of CO\\textsubscript{2} equivalent\n" +
                "(tCO\\textsubscript{2e}).");
        builder.append("\\vspace{0.2cm}\n" +
                " \\newline\n");
        builder.append("Reporting of GHG emissions was more comprehensive as it covered range of indirect\n" +
                "emissions which has multiple emission sources not within the control of the organization in addition to direct emissions. Data collection from " + comName + " was also quite complete,but there is still room for improvement for indirect emissions data collection and reporting. \n" +
                "\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,4) {\\includegraphics[width=12cm]{images/tot_dir_in}};\n" +
                "\\end{tikzpicture}");
        builder.append("\\caption{Emissions for " + (fy ? "FY " : " ") + year +  " } \\label{fig:fig1}");
        builder.append("\\end{figure}");
        builder.append("As Figure \\ref{fig:fig1} illustrates, the total carbon footprint of " +  comName + ", for the year " + year + " is " +  emission + " tCO\\textsubscript{2e}.");
        builder.append("\\vspace{0.2cm}\n" +
                " \\newline\n");
        String s = "The carbon footprint per tonne of production was determined to\n" +
                "be " + prod  +" tonnes CO\\textsubscript{2e} (considering direct and indirect emissions).";
        builder.append("For the " + (fy ? "Financial year ": "Year ") + year + " per capita emission and emission intensity are " + perCapita + " tCO\\textsubscript{2e} and\n" +
                intensity +" tCO\\textsubscript{2e}/LKR respectively." + (isProdAvailable ? s : "") );
        builder.append("\\vspace{0.2cm}\n" +
                "\\newline\n");
        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,5) {\\includegraphics[width=12cm]{images/all_src}};\n" +
                "\\end{tikzpicture}");
        builder.append("\\caption{GHG Emissions by source in " + comName +" } \\label{fig:fig2}\n" +
                "\\end{figure}");
        builder.append("\n \\newpage\n");
        return builder.toString();
    }

    public static String ghgInv( List<List<String>> cat2, // cat 2
                                 List<List<String>> cat3, // cat 4
                                 List<List<String>> cat4, // cat 6
                                 List<String> td_gases, // td
                                 String cat2text, //cat2
                                 String cat3text, // cate 4
                                 String cat4text, // cat 6
                                 String cat5text, // td_cat
                                 List<List<String>> direct,
                                 List<String> tDirect,
                                 List<String>tIndirect,
                                 List<String> totals,
                                 String year
    ) {
        StringBuilder builder = new StringBuilder();
        builder.append(
                "    \\begin{center}\n" +

                        "   \\begin{longtable}{?p{3.4cm}?p{4cm}?p{1.5cm}p{1.5cm}p{1.5cm}p{1.5cm}?}"
        );
        builder.append("\n");
        builder.append("\n");
        builder.append("  \n    \\caption{  Organizational GHG Inventory " + year +  "}\n"  +
                "           \\label{tab:my_label}\n \\hline");
        builder.append("\n" +
                "\\rowcolor[RGB]{147, 183, 190}\n" +
                "\\multicolumn{ " + 6 + "}{?c?}{\\parafont GHG emissions " + year +" }\\\\\n" +
                "\\hline" +
                "\\rowcolor[RGB]{147, 183, 190} \n");
        builder.append("{} & {\\parafont Emission Source } & {\\parafont tCO\\textsubscript{2}e} & {\\parafont CO\\textsubscript{2}} & {\\parafont CH\\textsubscript{4}} & {\\parafont N\\textsubscript{2}O} \\\\ ");


//        builder.append("\\\\");
        builder.append("\n \\hline\n");
        for (int i = 0; i < direct.size(); i++) {
            builder.append("\n");
            if (i == 0) {
                builder.append("\\multirow{" + direct.size() + "}{*}{} &");
            }else {
                builder.append("&");
            }
            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }
            for (int j= 0; j <direct.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + direct.get(i).get(j));
                }else {
                    builder.append(direct.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");


        }

//        builder.append("\\\\");
        builder.append("\n \\hline \n");
//        builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
        builder.append("\\hline \n");
        builder.append("\\multicolumn{ 2}{?c?}{\\textbf{ Total direct GHG emissions }}");
//        builder.append("\\textbf{Total direct GHG emissions} ");
        for (int i = 0; i < 4; i++) {
            builder.append("& \\textbf{" + tDirect.get(i) + "}");
        }
        builder.append("\\\\");
//        builder.append("\n \\hline \n");

        builder.append("\n \\hline\n");
        for (int i = 0; i < cat2.size(); i++) {
            builder.append("\n");
            if (i == 0) {
                builder.append("\\multirow{" + cat2.size() + "}{=}{\\textbf{" );
                for(String s : cat2text.split(" ")) {
                    builder.append(s);
                    builder.append(" \\newline ");
                }

                builder.append("}} &");
            } else {
                builder.append(" & ");
            }
            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }
            for (int j= 0; j <cat2.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + cat2.get(i).get(j));
                }else {
                    builder.append(cat2.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");

        }

        builder.append("\n \\hline\n");
        for (int i = 0; i < cat3.size(); i++) {
            builder.append("\n");
            if (i == 0) {
                builder.append("\\multirow{" + cat3.size() + "}{=}{\\textbf{" );
                for(String s : cat3text.split(" ")) {
                    builder.append(s);
                    builder.append(" \\newline ");
                }
                builder.append( "}} &");
            } else {
                builder.append(" & ");
            }
            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }
            for (int j= 0; j <cat3.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + cat3.get(i).get(j));
                }else {
                    builder.append(cat3.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");

        }

        builder.append("\n \\hline\n");
        for (int i = 0; i < cat4.size(); i++) {
            builder.append("\n");
            if (i == 0) {
                builder.append("\\multirow{" + cat4.size() + "}{=}{\\textbf{" );
                for(String s : cat2text.split(" ")) {
                    builder.append(s);
                    builder.append("\\newline");
                }
                builder.append( "}} &");
            } else {
                builder.append(" & ");
            }
            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }
            for (int j= 0; j <cat4.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + cat4.get(i).get(j));
                }else {
                    builder.append(cat4.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");

        }
        if (td_gases.size() > 0) {
            builder.append("\n \\hline\n");
            builder.append("\\multicolumn{ 2}{?p{5cm}?}{\\textbf{ " + cat5text +  " }}");
            for (int i = 0; i < td_gases.size(); i++) {
                builder.append("  & ");
                builder.append(td_gases.get(i));
            }
            builder.append("\\\\");
        }


        builder.append("\n \\hline\n");

        builder.append("\n \\hline \n");
//        builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
//        builder.append("\\hline \n");
        builder.append("\\multicolumn{ 2}{?c?}{\\textbf{Total Indirect GHG emissions }}");
//        builder.append("\\textbf{Total Indirect GHG emissions} ");
        for (int i = 0; i <4; i++) {
            builder.append("& \\textbf{" + tIndirect.get(i) + "}");
        }
//        builder.append("\\\\");
        builder.append("\\\\");
        builder.append("\n \\hline \n");
//        builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
        builder.append("\\hline \n");

        builder.append("\\multicolumn{ 2}{?c?}{\\textbf{Total Emissions }}");
//        builder.append("\\textbf{Total Emissions} ");

        for (int i = 0; i < 4; i++) {
            builder.append("& \\textbf{" + totals.get(i) + "}");
        }
        builder.append("\\\\");
        builder.append("\n \\hline \n");

        builder.append("\\hline \n");
        builder.append("\\end{longtable}");
        builder.append("\n");
        builder.append("\\end{center}");

        return builder.toString();

    }


    public static String ghgComparision( List<List<String>> indirect,
                                         List<List<String>> direct,
                                         List<String> years,
                                         List<String> tDirect,
                                         List<String>tIndirect,
                                         List<String> totals
    ) {
        StringBuilder builder = new StringBuilder();
        int cols = years.size();
        float width = 0;
        if (cols < 5) {
            width =  2.1f;
        }else {
            width = 1.4f;
        }
        builder.append("\\begin{table}[]\n" +
                "    \\centering\n" +
                "     \\caption{ Comparison of GHG inventories over financial years}\n" +
                "    \\label{tab:my_label}\n" +
                "   \\begin{longtable}{?p{5.8cm}?");
        for (String year : years) {
            builder.append("p{" +width +"cm}");
        }

        builder.append("?}\n" +
                "\\rowcolor[RGB]{147, 183, 190}\n" +
                "{\\parafont Source} & \\multicolumn{ " +(cols) + "}{c|}{\\parafont GHG Emissions (tCO2e)}\\\\\n" +
                "\\rowcolor[RGB]{147, 183, 190}");
        builder.append("{} ");
        for (String y: years) {
            builder.append("& {" + y + "}");
        }
//        builder.append("\\\\");
        builder.append("\n \\hline\n");
        for (int i = 0; i < direct.size(); i++) {
            builder.append("\n");
            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
            }
            for (int j= 0; j <direct.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + direct.get(i).get(j));
                }else {
                    builder.append(direct.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");


        }

//        builder.append("\\\\");
        builder.append("\n \\hline \n");
//        builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
        builder.append("\\hline \n");
        builder.append("\\textbf{Total direct GHG emissions} ");
        for (int i = 0; i < cols; i++) {
            builder.append("& \\textbf{" + tDirect.get(i) + "}");
        }
        builder.append("\\\\");
        builder.append("\n \\hline \n");

        for (int i = 0; i < indirect.size(); i++) {
            builder.append("\n");
//            if (i % 2 == 0) {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
//            }else {
//                builder.append("\n \\rowcolor[RGB]{255, 255, 255} \n");
//            }
            for (int j= 0; j < indirect.get(i).size(); j++) {
                if (j != 0) {
                    builder.append(" & " + indirect.get(i).get(j));
                }else {
                    builder.append(indirect.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n");
        }
//        builder.append("\\\\");
        builder.append("\n \\hline \n");
//        builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
//        builder.append("\\hline \n");
        builder.append("\\textbf{Total Indirect GHG emissions} ");
        for (int i = 0; i < cols; i++) {
            builder.append("& \\textbf{" + tIndirect.get(i) + "}");
        }
//        builder.append("\\\\");
        builder.append("\n \\hline \n");

        builder.append("\\textbf{Total Emissions} ");

        for (int i = 0; i < cols; i++) {
            builder.append("& \\textbf{" + totals.get(i) + "}");
        }
        builder.append("\\\\");
        builder.append("\n \\hline \n");

        builder.append("\\hline \n");
        builder.append("\\end{longtable}");
        builder.append("\\end{table}");

        return builder.toString();

    }

    public static String efComparision(List<List<String>> data, List<String> years) {
        StringBuilder builder = new StringBuilder();
        int cols = data.get(0).size();
        float width = 0;
        if (cols < 5) {
            width =  2.1f;
        }else {
            width = 1.4f;
        }
        builder.append("\\begin{table}[]\n" +
                "    \\centering\n" +
                "     \\caption{Comparison Emission Factors}\n" +
                "    \\label{tab:my_label}\n" +
                "   \\begin{longtable}{?p{5.8cm}?");
        for (String year : years) {
            builder.append("p{" +width +"cm}");
        }

        builder.append("?}\n" +
                "\\rowcolor[RGB]{147, 183, 190}\n" +
                "{\\parafont Source} & \\multicolumn{ " +(cols -1) + "}{p{ " + width * (cols-1) +"cm}?}{\\parafont Emission Factor and Other Constants}\\\\\n" +
                "\\rowcolor[RGB]{147, 183, 190}");
        builder.append("{} ");
        for (String y: years) {
            builder.append("& {" + y + "}");
        }
//        builder.append("\\\\");
        builder.append("\n \\hline\n");
        for (int i = 0; i < data.size(); i++) {
            if (i % 2 == 0) {
                builder.append("\\rowcolor[RGB]{255, 255, 255} \n");
            }else {
                builder.append("\\rowcolor[RGB]{229, 230, 255} \n");
            }
            for (int j= 0; j < cols; j++) {
                if (j != 0) {
                    builder.append(" & " + data.get(i).get(j));
                }else {
                    builder.append(data.get(i).get(j));
                }
            }
            builder.append("\\\\");
            builder.append("\n \\hline \n");
        }
        builder.append("\\hline \n");
        builder.append("\\end{longtable}");
        builder.append("\\end{table}");

        return builder.toString();
    }


    public static String genTableOfCon() {
        StringBuilder  builder = new StringBuilder();
        builder.append("\\tableofcontents\n" +
                "\\let\\cleardoublepage\\clearpage\n" +
                "\\listoffigures\n" +
                "\\let\\cleardoublepage\\clearpage\n" +
                "\\listoftables\n" +
                "\\let\\cleardoublepage\\clearpage\n" +
                "\\newpage \n");
        return builder.toString();
    }

    public static String genExclusions(List<HashMap<String, String>> data) {
        StringBuilder builder = new StringBuilder();
//        table header
        builder.append("\\begin{longtable}{?p{7cm}?p{8.1cm}?}\n" +
                " \\caption{Excluded Emission Sources and Reasons}\n" +
                " \\label{tab:tab2} \\hline\n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont Exclusion from the Organization\n" +
                "Boundary} & {\\parafont  Reason} \\\\ \\cline{1pt}{2-3} \n" +
                " \n");

        int i = 0;
        for(HashMap<String, String> rowMap : data) {
            if (i % 2 == 0) {
//                builder.append("\\hline\n");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{255, 255, 255}\n");
                builder.append("{" +  rowMap.get("s") + "} &");
                builder.append("{" +  rowMap.get("r") + "} \\\\");
                builder.append("\n");
            }else {
//                builder.append("\\hline");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{229, 230, 255}\n");
                builder.append("{" +  rowMap.get("s") + "} &");
                builder.append("{" +  rowMap.get("r") + "} \\\\");
                builder.append("\n");
            }

            i++;
        }
        builder.append("\\hline \n");
        builder.append(" \\end{longtable}");

        return builder.toString();
    }


    public static String genHeaderFooter(String comName) {
        StringBuilder builder = new StringBuilder();
        builder.append("\\usepackage{xcolor}\n" +
                "\\usepackage{fancyhdr}");
        builder.append("\\renewcommand{\\headrulewidth}{0.5pt}\n" +
                "\\renewcommand{\\headrule}{\\hbox to\\headwidth{\\color{red}\\leaders\\hrule height \\headrulewidth\\hfill}}\n" +
                "\\renewcommand{\\footrulewidth}{0.5pt}\n" +
                "\\renewcommand{\\footrule}{\\vspace{.4cm}\\hbox to\\headwidth{\\color{red}\\leaders\\hrule height \\footrulewidth\\hfill}}");
        builder.append("\\pagestyle{fancy}");
        builder.append("\\lhead{\\vspace{1.5 \\baselineskip }Greenhouse Gas Inventory Report – " + comName + "}");
        builder.append("\\rhead{\\includegraphics[width=3cm]{images/company_logo}}\n" +
                "\\rfoot{\\includegraphics[width=2cm]{images/favicon}}\n" +
                "\\cfoot{\\vspace{1\\baselineskip}Climate Smart Initiatives (Pvt) Ltd }\n" +
                "\\fancyfoot[l]{\\vspace{-2.5\\baselineskip}\\thepage}");
        builder.append("\\fancypagestyle{plain}{%");
        builder.append("\\lhead{\\vspace{-2.5 \\baselineskip}Greenhouse Gas Inventory Report – " + comName + "}");
        builder.append("\\rhead{\\includegraphics[width=3cm]{images/company_logo}}\n" +
                "\\fancyfoot[r]{\\includegraphics[width=2cm]{images/favicon}}\n" +
                "\\cfoot{\\vspace{-2.5\\baselineskip}Climate Smart Initiatives (Pvt) Ltd }\n" +
                "\\fancyfoot[l]{\\vspace{-2.5\\baselineskip}\\thepage}\n" +
                "}");
        return builder.toString();
    }

    public static String genGloassary() {
        StringBuilder builder = new StringBuilder();
        builder.append("\\chapter*{Glossary of Terms}\n" +
                "\n" +
                "\\setlength\\extrarowheight{10pt}\n" +
                "\\begin{table}[h]\n" +
                "\\begin{tabularx}{165mm}{sb}\n" +
                "% \\hline\n" +
                "\\rowcolor[RGB]{147, 183, 190}\n" +
                "\\text{\\bigsize GHG Emission Source}  & \\text{\\bigsize Description} \\\\ \n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{\\parafont On-site diesel generators}  & {\\parafont Direct emission source attributed to use of diesel in generators.} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{\\parafont Fire extinguishers} & {\\parafont Direct emission source attributed to use of fire extinguishers } \\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{\\parafont Refrigerant leakages} & {\\parafont Direct emission source attributed to Refrigerant leakage} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{\\parafont Company owned Vehicles} & {\\parafont Direct emission source attributed to company owned, prime\n" +
                "movers, delivery vans, three-wheelers, passenger buses, vans and\n" +
                "lorries}\\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{\\parafont Off-road mobile sources\n" +
                "and machineriesCompany Owned\n" +
                "forklifts} & {\\parafont Direct emission source attributed to use of off-road mobile\n" +
                "sources and machineries like forklifts} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{\\parafont Employee transport\n" +
                "paid by the company}  & {\\parafont Direct emission source attributed to employee owned diesel and\n" +
                "petrol vehicles which receive company fuel.} \\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{\\parafont Grid connected electricity} \n" +
                "& {\\parafont Indirect emission source attributed to use of electricity\n" +
                "transmitted and distributed by Ceylon electricity board} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{\\parafont Business air travel} & {\\parafont Indirect emission source attributed to the business air travels} \\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{\\parafont Employee commuting not paid by the company} \n" +
                "& {\\parafont Indirect emission source attributed to diesel and petrol consumption of the employee owned vehicles which don't receive company fuel} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{\\parafont Municipal water} & {\\parafont Indirect emission source attributed to municipal water\n" +
                "consumption}\\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255} \n" +
                "{\\parafont Waste disposal}  & {\\parafont Indirect emission source attributed to the disposal of waste}\\\\\n" +
                "\n" +
                "% \\rowcolor[RGB]{229, 230, 255}\n" +
                "\\end{tabularx}\n" +
                "% \\caption{Data-Sets Used in Researches} \\label{table:tab1}\n" +
                "\\end{table}\n" +
                "\n" +
                "\\newpage\n" +
                "\n" +
                "\\begin{table}[h]\n" +
                "\\begin{tabularx}{165mm}{sb}\n" +
                "% \\hline\n" +
                "\\rowcolor[RGB]{147, 183, 190}\n" +
                "\\text{\\bigsize GHG Emission Source}  & \\text{\\bigsize Description} \\\\ \n" +
                "\\rowcolor[RGB]{255, 255, 255} \n" +
                "{\\parafont Transmission and Distribution (T and D) loss}  & {\\parafont Indirect emission source attributed to the transmission and\n" +
                "distribution loss of electricity}\\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{\\parafont Waste transport}  & {\\parafont Indirect emission source attributed to waste transport} \\\\\n" +
                "\n" +
                "\\end{tabularx}\n" +
                "% \\caption{Data-Sets Used in Researches} \\label{table:tab1}\n" +
                "\\end{table}");

        return builder.toString();
    }

    public static String genActivityData(
            List<HashMap<String, String>> data
    ) {
        StringBuilder builder = new StringBuilder();
//        table header
        builder.append("\\begin{longtable}{?p{6cm}?p{2.8cm}p{2.5cm}?p{3cm}?}\n" +
                " \\caption{Activity data used for quantifying GHGs}\n" +
                " \\label{tab:tab1} \\hline\n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont Category} &  \\multicolumn{2}{|c?}{\\parafont Activity Data} & {\\parafont  Reference} \\\\ \\cline{1pt}{2-3} \n" +
                " \n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont } & {\\parafont  Quantity} & {\\parafont  Unit} & {\\parafont  Data Source}\\\\");

        int i = 0;
        for(HashMap<String, String> rowMap : data) {
            if (i % 2 == 0) {
//                builder.append("\\hline\n");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{255, 255, 255}\n");
                builder.append("{" +  rowMap.get("c") + "} &");
                builder.append("{" +  rowMap.get("q") + "} &");
                builder.append("{" +  rowMap.get("u") + "} &");
                builder.append("{" +  rowMap.get("d") + "} \\\\");
                builder.append("\n");
            }else {
//                builder.append("\\hline");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{229, 230, 255}\n");
                builder.append("{" +  rowMap.get("c") + "} &");
                builder.append("{" +  rowMap.get("q") + "} &");
                builder.append("{" +  rowMap.get("u") + "} &");
                builder.append("{" +  rowMap.get("d") + "} \\\\");
                builder.append("\n");
            }

            i++;
        }
        builder.append("\\hline \n");
        builder.append(" \\end{longtable}");

        return builder.toString();
    }

    public static String genEmissionFactorsData(
            List<HashMap<String, String>> data
    ){
        StringBuilder builder = new StringBuilder();
//        table header
        builder.append("\\begin{longtable}{?p{7cm}?p{2cm}p{2cm}?p{3.3cm}?}\n" +
                " \\caption{Emission factors and other constants used for quantifying GHGs}\n" +
                " \\label{tab:tab2} \\hline\n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont Fuel} &  \\multicolumn{2}{|c|}{\\parafont Emission Factor and\n" +
                "constant} & {\\parafont  Reference} \\\\ \\cline{1pt}{2-3} \n" +
                " \n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont } & {\\parafont  Quantity} & {\\parafont  Unit} & {\\parafont  Data Source}\\\\");

        int i = 0;
        for(HashMap<String, String> rowMap : data) {
            if (i % 2 == 0) {
//                builder.append("\\hline\n");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{255, 255, 255}\n");
                builder.append("{" +  rowMap.get("f") + "} &");
                builder.append("{" +  rowMap.get("q") + "} &");
                builder.append("{" +  rowMap.get("u") + "} &");
                builder.append("{" +  rowMap.get("d") + "} \\\\");
                builder.append("\n");
            }else {
//                builder.append("\\hline");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{229, 230, 255}\n");
                builder.append("{" +  rowMap.get("f") + "} &");
                builder.append("{" +  rowMap.get("q") + "} &");
                builder.append("{" +  rowMap.get("u") + "} &");
                builder.append("{" +  rowMap.get("d") + "} \\\\");
                builder.append("\n");
            }

            i++;
        }
        builder.append("\\hline \n");
        builder.append(" \\end{longtable}");

        return builder.toString();
    }

    public static String genDirectGHGEmission(
            List<HashMap<String, String>> data,
            boolean fy,
            String year,
            String comName
    ) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        builder.append("\\section{ Direct GHG emissions}");
        builder.append("Direct on-site greenhouse gas emissions are due to ");
        float total = 0;
        float max = 0;
        String maxSecond = "";
        String maxThird = "";
        String maxSrc ="";
        List<Float> vals = new ArrayList<>();

        for(HashMap<String, String> row : data) {
            builder.append(row.get("s"));
            builder.append(", ");
            total += Float.parseFloat(row.get("v"));
            vals.add(Float.parseFloat(row.get("v")));
            if (Float.parseFloat(row.get("v")) > max) {
                maxSrc = row.get("s");
                max = Float.parseFloat(row.get("v"));
            }
        }
        Collections.sort(vals, Collections.reverseOrder());
        if (vals.size() > 3) {
            for(HashMap<String, String> row : data) {
                if (vals.get(1) == Float.parseFloat(row.get("v"))){
                    maxSecond = row.get("s");
                }
                if (vals.get(2) == Float.parseFloat(row.get("v"))) {
                    maxThird = row.get("s");
                }

            }
        }
        builder.append(" by ");
        builder.append(comName);

//        table header
        builder.append("\\begin{longtable}{?p{10cm}?p{5.1cm}?}\n" +
                " \\caption{Direct emissions for " + (fy ? "FY " + year : "Year " + year) + " (tonne CO\\textsubscript{2}e)}\n" +
                " \\label{tab:tab2} \\hline\n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont Source\n} & {\\parafont  Emissions\n"+
                "(tCO\\textsubscript{2}e) \n} \\\\ \\cline{1pt}{2-3} \n" +
                " \n");

        int i = 0;
        for(HashMap<String, String> rowMap : data) {
            if (i % 2 == 0) {
//                builder.append("\\hline\n");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{255, 255, 255}\n");
                builder.append("{" +  rowMap.get("s") + "} &");
                builder.append("{" +  rowMap.get("v") + "} \\\\");
                builder.append("\n");
            }else {
//                builder.append("\\hline");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{229, 230, 255}\n");
                builder.append("{" +  rowMap.get("s") + "} &");
                builder.append("{" +  rowMap.get("v") + "} \\\\");
                builder.append("\n");
            }

            i++;
        }
        builder.append("\\hline \n");
        builder.append(" \\end{longtable}");

        builder.append("\n");

        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,4) {\\includegraphics[width=12cm]{images/pie_direct}};\n" +
                "\\end{tikzpicture}\n" +
                "\\caption{Direct emissions by source for " + ( fy ? "FY " : "Year " ) + year + " } \\label{fig:fig1}\n" +
                "\\end{figure}");

        builder.append("\n");
        builder.append("Total direct emissions of ");
        builder.append(comName);
        builder.append("are ");
        builder.append((max/total) * 100);
        builder.append("%");
        builder.append("tCO\\textsubscript{2}.");
        builder.append(maxSrc + " by the\n" +
                "company, which account "+  (max/total * 100) + "\\% of the total direct emissions, is the largest emission source.\n" +
                "Second and third emission sources are " + maxSecond + " and " + maxThird + ". \n");

        return builder.toString();
    }

    public static String genInDirectGHGEmission(
            List<HashMap<String, String>> data,
            boolean fy,
            String year,
            String comName
    ) {
        StringBuilder builder = new StringBuilder();

        float total = 0;
        float max = 0;
        String maxSrc ="";

        String others = "";
        for(HashMap<String, String> row : data) {

            if (!row.get("s").equals("Grid Connected Electricity") && !row.get("s").equals("Transmission and Distribution Loss")){
                others += row.get("s") + ", ";
            }
            total += Float.parseFloat(row.get("v"));
            if (Float.parseFloat(row.get("v")) > max) {
                maxSrc = row.get("s");
                max = Float.parseFloat(row.get("v"));
            }
        }


        builder.append("\\section{ Indirect GHG emissions}");
        builder.append("Indirect emissions are sometimes difficult to assign to a specific operation, therefore, the\n" +
                "reporting method for different sources may vary. ");
        builder.append("\\newline");
        builder.append(comName + "’s operations use the electricity from Ceylon Electricity Board, which is the\n" +
                "national grid. The National Grid consists of coal, large hydro, diesel, and non-conventional\n" +
                "renewable energy sources. The emission factor used to calculate emissions associated with\n" +
                "electricity consumption is 0.568 tCO2e/MWh. " +comName + "’s " + maxSrc +" \n" +
                "consumption accounts for the top indirect emissions. In addition," +others  +".");

//        table header
        builder.append("\\begin{longtable}{?p{10cm}?p{5.1cm}?}\n" +
                " \\caption{Indirect emissions for " + (fy ? "FY " + year : "Year " + year) + " (tonne CO\\textsubscript{2}e)}\n" +
                " \\label{tab:tab2} \\hline\n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont Source\n} & {\\parafont  Emissions\n"+
                "(tCO\\textsubscript{2}e) \n} \\\\ \\cline{1pt}{2-3} \n" +
                " \n");

        int i = 0;
        for(HashMap<String, String> rowMap : data) {
            if (i % 2 == 0) {
//                builder.append("\\hline\n");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{255, 255, 255}\n");
                builder.append("{" +  rowMap.get("s") + "} &");
                builder.append("{" +  rowMap.get("v") + "} \\\\");
                builder.append("\n");
            }else {
//                builder.append("\\hline");
                builder.append("\n");
                builder.append("\\rowcolor[RGB]{229, 230, 255}\n");
                builder.append("{" +  rowMap.get("s") + "} &");
                builder.append("{" +  rowMap.get("v") + "} \\\\");
                builder.append("\n");
            }

            i++;
        }
        builder.append("\\hline \n");
        builder.append(" \\end{longtable}");

        builder.append("\n");

        builder.append("\\begin{figure}[h]\n" +
                "\\centering\n" +
                "\\begin{tikzpicture} \\draw [help lines] (0,0) -- (0, 10) -- ( 16.0,10) -- ( 16.0,0) -- (0,0);\n" +
                "\\node [] at (8,4) {\\includegraphics[width=12cm]{images/pie_indirect}};\n" +
                "\\end{tikzpicture}\n" +
                "\\caption{ Indirect Emissions by source  for " + ( fy ? "FY " : "Year " ) + year + " } \\label{fig:fig1}\n" +
                "\\end{figure}");

        builder.append("\n");

        builder.append("\\newline");
        return builder.toString();
    }


    public static String genEmissionFactors() {
        StringBuilder builder = new StringBuilder();
        builder.append("\\n");
        builder.append("\\section{Emission factors and other constants }\n" +
                "\n" +
                "\\begin{longtable}{?p{6cm}?p{2.8cm}p{2.5cm}?p{3cm}?}\n" +
                " \\caption{Emission factors and other constants used for quantifying GHGs}\n" +
                " \\label{tab:tab2} \\hline\n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont Fuel} &  \\multicolumn{2}{?c?}{\\parafont Emission Factor and\n" +
                "constant} & {\\parafont  Reference} \\\\ \\cline{1pt}{2-3} \n" +
                " \n" +
                " \\rowcolor[RGB]{147, 183, 190}\n" +
                " {\\parafont } & {\\parafont  Quantity} & {\\parafont  Unit} & {\\parafont  Data Source}\\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
//                "{jlsjfsjfsjfjfsll} &{102020} &{tCO\\textsubscript{2}e/TJ} &\\href{http://www.ipccnggip.iges.or.jp/public/2006gl/pdf/2_Volum\n" +
//                "e2/V2_2_Ch2_Stationary_Combustion.pdf\n" +
//                "}{view} \\\\\n" +
//                "\n" +
//                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Diesel (Mobile\n" +
                "Combustion) } &{75.24} &{tCO\\textsubscript{2}e/TJ} &\n" +
                "\\href{http://www.ipccnggip.iges.or.jp/public/2006gl/pdf/2_Volum\n" +
                "e2/V2_3_Ch3_Mobile_Combustion.pdf}{view}\\\\\n" +
                "\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Petrol (off-road\n" +
                "machineries)} &{74.44} &{tCO\\textsubscript{2}e/TJ} & \\href{https://www.ipcc.ch/meetings/session25/d\n" +
                "oc4a4b/vol2.pdf}{view} \\\\\n" +
                "\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{Diesel (Off-road\n" +
                "machineries) } &{81.79} &{tCO\\textsubscript{2}e/TJ} & \\href{https://www.ipcc.ch/meetings/session25/d\n" +
                "oc4a4b/vol2.pdf}{view} \\\\\n" +
                "\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Petrol (Mobile\n" +
                "Combustion) } &{71.07} &{tCO\\textsubscript{2}e/TJ} & \\href{http://www.ipccnggip.iges.or.jp/public/2006gl/pdf/2_Volum\n" +
                "e2/V2_3_Ch3_Mobile_Combustion.pdf\n" +
                "}{view\n" +
                "} \\\\\n" +
                "\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{Refrigerant\n" +
                "Leakage (R 22)} &{1,760} &{GWP} & \\href{https://www.ipcc.ch/pdf/assessmentreport/ar5/wg1/WG1AR5_Chapter08_FINAL\n" +
                ".pdf\n" +
                "}{view} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Municipal Water} &{0.35} &{kWh/m \\textsuperscript{3} } &{Climate Change secretariat CFP manual\n" +
                "} \\\\\n" +
                "\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{National Grid\n" +
                "Emission Factor} &{0.5845} &{Kg CO\\textsubscript{2}e/kWh } &\\href{http://www.energy.gov.lk/en/}{view} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Transmission and\n" +
                "Distribution Loss} &{9.63} &{\\%} & \\href{http://www.ceb.lk/publications/#tab1439804992508-1\n" +
                "}{view} \\\\\n" +
                "\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{Business air\n" +
                "travels \n" +
                "} &{} &{ } &\\href{https://www.icao.int/environmentalprotection/CarbonOffset/Pages/default.asp\n" +
                "x\n" +
                "}{view} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Fuel economy of\n" +
                "Bike } & {50} &{km/l} & {Clean Air Asia \\& opinion of based on sector\n" +
                "expert\n" +
                "} \\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{Fuel economy of\n" +
                "Three Wheel } & {35} &{km/l} & {Clean Air Asia \\& opinion of based on sector\n" +
                "expert\n" +
                "} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Fuel economy of\n" +
                "Car } & {13} &{km/l} & {Clean Air Asia \\& opinion of based on sector\n" +
                "expert\n" +
                "} \\\\\n" +
                "\\rowcolor[RGB]{255, 255, 255}\n" +
                "{Fuel economy of\n" +
                "Hybrid car } & {22} &{km/l} & {Clean Air Asia \\& opinion of based on sector\n" +
                "expert\n" +
                "} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Fuel economy of\n" +
                "Bus(diesel) } & {0.0162032 } &{kgCO\\textsubscript{2}e/passenger km } & {Indian GHG Programme} \\\\\n" +
                "\\rowcolor[RGB]{229, 230, 255}\n" +
                "{Fuel economy of\n" +
                "Railway } & { 0.0079760  } &{kgCO\\textsubscript{2}e/passenger km } & {Indian GHG Programme} \\\\\n" +
                "\\hline \n" +
                " \\end{longtable}");

        return builder.toString();
    }
}
