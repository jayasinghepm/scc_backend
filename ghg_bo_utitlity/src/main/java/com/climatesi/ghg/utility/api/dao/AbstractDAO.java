package com.climatesi.ghg.utility.api.dao;

import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.EntityID;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import com.climatesi.ghg.utility.api.exceptions.GHGRuntimeException;
import com.climatesi.ghg.utility.api.exceptions.OperationNotSupportException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
//import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.jpa.criteria.CriteriaQueryImpl;
import org.jboss.logging.Logger;

import javax.persistence.*;
import java.util.*;

public abstract class AbstractDAO<C, I> {
    private static Logger logger = Logger.getLogger(AbstractDAO.class.getName());
    private static final Gson GSON = new Gson();

    protected EntityManager entityManager;
    private DataEntryInfo daoMeta;

    private Class<C> beanClass;
    private String implClassName;

    private static final String AUDIT_SP = "pkg_common_audit.sp_add";
    private static final String STATUS_SP = "pkg_common_audit.sp_add";

    //region Constructor

    public AbstractDAO(EntityManager em, Class<C> entityImplClass) {
        this.entityManager = em;
        this.beanClass = entityImplClass;

        implClassName = beanClass.getSimpleName();
        logger.info("Entity implementation class name >> " + implClassName);
    }

    //endregion

    //region Getters and Setters

    public DataEntryInfo getDaoMeta() {
        return daoMeta;
    }

    protected void setDaoMeta(DataEntryInfo daoMeta) {
        this.daoMeta = daoMeta;
    }

    //endregion

    //region Abstract Methods

    public abstract Map<String, Object> getAddSpParams(C entityImpl) throws GHGException;

    public abstract Map<String, Object> getEditSpParams(C entityImpl) throws GHGException;

    public abstract Map<String, Object> getChangeStatusSpParams(C entityImpl) throws GHGException;

    //endregion

    /**
     * Store an instance of JPA entity class in database
     *
     * @param entityImpl JPA entity implementation
     * @return insert action related information. For procedure calls, this is the OUT parameter value
     * @throws Exception
     */
    public String insert(C entityImpl) throws GHGException {
        try {
            return insert(entityImpl, -1);
        } catch (OperationNotSupportException e) {
            logger.error("Add operation not support. >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }
    }

    public String insert(C entityImpl, int userID) throws GHGException {
        try {
            Map<String, Object> addPara = null;

            if (daoMeta.getAddMode() == PersistMode.DB_SP_CALL) {
                addPara = getAddSpParams(entityImpl);
            }
            if (addPara == null) {
                addPara = new HashMap<>();
            }
            return insert(entityImpl, daoMeta.getAddSpJpaName(), addPara, daoMeta.hasAddOutPara());
        } catch (OperationNotSupportException e) {
            logger.error("Add operation not support. >> " + e.getMessage(), e);
            throw new GHGException(e);
        }
    }

    public String insert(C entityImpl, String spName, Map<String, Object> paramMap, boolean hasOutPara) throws GHGException {
        String result = ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        try {
            return insert(entityImpl, spName, paramMap, hasOutPara, -1);
        } catch (OperationNotSupportException e) {
            logger.error("Add operation not support. >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }
    }


    public String insert(C entityImpl, String spName, Map<String, Object> paramMap, boolean hasOutPara, int userID) throws GHGException {
        String result = ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        try {
            logger.info("Inserting entity to database >> " + entityImpl.toString());
            logger.info("Persistence Mode >> " + daoMeta.getAddMode());

            switch (daoMeta.getAddMode()) {
                case JPA_PERSIST:
                    entityManager.persist(entityImpl); //Persist takes an entity instance, adds it to the context and makes that instance managed
                    logger.info("Entity added to database using JPA.");
                    result = ApplicationConstants.DEFAULT_SUCCESS_RESPONSE_FOR_JPA_EXECUTE;
                    break;
                case DB_SP_CALL:
                    result = DBManager.getInstance().executeNamedProcedure(entityManager, spName, paramMap, hasOutPara);
                    logger.info("Entity added to database using NamedStoredProcedure.");
                    break;
                default:
                    logger.error("Unsupported Persistence Mode");
                    break;
            }
            if (userID != -1 && daoMeta.getEntityID() != null) { // checking whether there is a valid user Id to Set Created by audit log
                PersistenceUnitUtil util = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
                Object key = util.getIdentifier(entityImpl);
                DBManager.getInstance().executeNamedProcedure(entityManager, this.AUDIT_SP, getAuditSpParams(userID, 1, (int) key), false);
            }
        } catch (Exception e) {
            logger.error("Failed to insert entity to database >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }

        return result;
    }

    public String insert(C entityImpl, PersistMode persistMode) throws GHGException {

        String result = ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        try {
            logger.info("Inserting entity in database >> " + entityImpl.toString());
            logger.info("Persistence Mode >> " + persistMode);

            switch (persistMode) {
                case JPA_PERSIST:
                    entityManager.persist(entityImpl); //Persist takes an entity instance, adds it to the context and makes that instance managed
                    logger.info("Entity added to database using JPA.");
                    result = ApplicationConstants.DEFAULT_SUCCESS_RESPONSE_FOR_JPA_EXECUTE;
                    break;
                case DB_SP_CALL:
                    result = DBManager.getInstance().executeNamedProcedure(entityManager, daoMeta.getAddSpJpaName(), getAddSpParams(entityImpl), daoMeta.hasAddOutPara());
                    logger.info("Entity added to database using NamedStoredProcedure.");
                    break;
                default:
                    logger.error("Unsupported Persistence Mode");
                    break;
            }
        } catch (OperationNotSupportException e) {
            logger.error("Add operation not support. >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        } catch (Exception e) {
            logger.error("Failed to add entity in database >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }

        return result;
    }

    /**
     * Update JPA entity store in database
     *
     * @param entityImpl JPA entity implementation with updated properties
     * @return update action related information. For procedure call and has the OUT parameter, this is the OUT parameter value
     * @throws GHGException
     */
    public String update(C entityImpl) throws GHGException {
        return update(entityImpl, daoMeta.getEditMode());
    }

    public String update(C entityImpl, int userID) throws GHGException {
        return update(entityImpl, daoMeta.getEditMode(), userID);
    }

    public String update(C entityImpl, PersistMode persistMode) throws GHGException {
        String result = ApplicationConstants.DEFAULT_FAILED_CODE_FOR_JPA_EXECUTE;
        return update(entityImpl, persistMode, -1);
    }

    public String update(C entityImpl, PersistMode persistMode, int userID) throws GHGException {
        String result = ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        try {
            logger.info("Updating entity in database >> " + entityImpl.toString());
            logger.info("Persistence Mode >> " + persistMode);

            switch (persistMode) {
                case JPA_PERSIST:
                    C entityResponse = entityManager.merge(entityImpl); //Merge creates a new instance of your entity, copies the state from the supplied entity, and makes the new copy managed. The instance you pass in will not be managed

                    if (entityResponse == null) {
                        logger.error("Failed to update entity in database using JPA.");
                        result = ApplicationConstants.DEFAULT_FAILED_CODE_FOR_JPA_EXECUTE;
                    } else {
                        logger.info("Entity updated in database using JPA.");
                        logger.info(entityResponse);
                        result = ApplicationConstants.DEFAULT_SUCCESS_RESPONSE_FOR_JPA_EXECUTE;
                    }
                    break;
                case DB_SP_CALL:
                    result = DBManager.getInstance().executeNamedProcedure(entityManager, daoMeta.getEditSpJpaName(), getEditSpParams(entityImpl), daoMeta.hasEditOutPara());
                    logger.info("Entity edited to database using NamedStoredProcedure.");
                    break;
                default:
                    logger.error("Unsupported Persistence Mode");
                    break;
            }

            if (userID != -1 && daoMeta.getEntityID() != null) { // checking whether there is a valid user Id to Set Created by audit log
                PersistenceUnitUtil util = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
                Object key = util.getIdentifier(entityImpl);
                DBManager.getInstance().executeNamedProcedure(entityManager, this.AUDIT_SP, getAuditSpParams(userID, 1, (int) key), false);
            }
        } catch (OperationNotSupportException e) {
            logger.error("Edit operation not support. >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        } catch (Exception e) {
            logger.error("Failed to update entity in database >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }

        return result;
    }


    private static String getFilter(String filterKey, int filterType) {
        switch (filterType) {
            case 1: { // equals
                return " ( o." + filterKey + " = :" + filterKey + " ) ";
            }
            case 2: { // <
                return " ( o." + filterKey + " < :" + filterKey + " ) ";
            }
            case 3: { // >
                return " ( o." + filterKey + " > :" + filterKey + " ) ";
            }
            case 4: { // like
                return " ( o." + filterKey + " LIKE :" + filterKey + " ) ";
            }
            case 5: { // not equal
                return " ( o." + filterKey + " != :" + filterKey + " ) ";
            }

        }
        return "";
    }

    /**
     * Used to get the entity related to primary key
     *
     * @param searchKey
     * @return
     */
    public I findByKey(Object searchKey) {
        I entity = null;

        try {
            logger.info("Find entity by key >> " + searchKey);

            Object result = DBManager.getInstance().getEntityByNamedQuery(entityManager, daoMeta.getFindByKeyQueryJpaName(), daoMeta.getKeyPropertyName(), searchKey);
            entity = (I) result;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }

        logger.info("Return entity by key >> " + entity);

        return entity;
    }

    /**
     * Used to change the approval status of master data records
     *
     * @param entityImpl PA entity implementation with updated properties
     * @return true if status change is success
     * @throws GHGException
     */
    public boolean changeStatus(C entityImpl, int userID) throws GHGException {
        boolean isStatusChangeSuccess = false;
        String result;
        try {
            switch (daoMeta.getAddMode()) {
                case JPA_PERSIST:
                    result = update(entityImpl);
                    isStatusChangeSuccess = result.equalsIgnoreCase(ApplicationConstants.DEFAULT_SUCCESS_RESPONSE_FOR_JPA_EXECUTE);
                    if (userID != -1 && daoMeta.getEntityID() != null) { // checking whether there is a valid user Id to Set Created by audit log

                        PersistenceUnitUtil util = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
                        Object key = util.getIdentifier(entityImpl);
                        DBManager.getInstance().executeNamedProcedure(entityManager, this.STATUS_SP, getAuditSpParams(userID, 1, (int) key), false);
                    }
                    break;
                case DB_SP_CALL:
                    logger.info("Change status of entity in database >> " + entityImpl.toString());
                    result = DBManager.getInstance().executeNamedProcedure(entityManager, daoMeta.getStatusChangeSpJpaName(), getChangeStatusSpParams(entityImpl), false);
                    logger.info("Change status result >> " + result);

                    isStatusChangeSuccess = String.valueOf(ApplicationConstants.DEFAULT_SUCCESS_RESPONSE_FOR_SP_EXECUTE).equals(result);
                    break;
                default:
                    logger.error("Unsupported Persistence Mode");
                    break;
            }

        } catch (OperationNotSupportException e) {
            logger.error("Status change not support. >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        } catch (Exception e) {
            logger.error("Failed to change status of entity in database >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }

        return isStatusChangeSuccess;
    }

    /**
     * Used to change the approval status of master data records
     *
     * @param entityImpl PA entity implementation with updated properties
     * @return true if status change is success
     * @throws GHGException
     */
    public boolean changeStatus(C entityImpl) throws GHGException {

        try {
            return changeStatus(entityImpl, -1);
        } catch (OperationNotSupportException e) {
            logger.error("Status change not support. >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        } catch (Exception e) {
            logger.error("Failed to change status of entity in database >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }
    }


    /**
     * Used to change the approval status of master data records
     *
     * @param entityImpl PA entity implementation with updated properties
     * @return status change action related information. this is the OUT parameter value
     * @throws GHGException
     */

    public String statusChange(C entityImpl) throws GHGException {

        String result;
        try {
            logger.info("Updating entity in database >> " + entityImpl.toString());

            result = DBManager.getInstance().executeNamedProcedure(entityManager, daoMeta.getStatusChangeSpJpaName(), getChangeStatusSpParams(entityImpl), true);

            if (result == null) {
                logger.error("Failed to change the status in database using NamedStoredProcedure.");
                result = ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
            } else {
                logger.info(" changed status in the database using NamedStoredProcedure.");
            }

        } catch (OperationNotSupportException e) {
            logger.error("Status change not support. >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        } catch (Exception e) {
            logger.error("Failed to change status of entity in database >> " + e.getMessage(), e);
            throw new GHGRuntimeException(e);
        }

        return result;
    }

    /**
     * Use to get all entities
     *
     * @return all ListResult object with list of entities
     */
    public ListResult<I> findAll() {
        JsonObject sortObject = new JsonObject();
        JsonObject defaultSortObj = new JsonObject();
        defaultSortObj.addProperty("dir", "asc");

        sortObject.add(daoMeta.getDefaultSortColumn(), defaultSortObj);
        return findPaginatedSearchResults(0, sortObject, 0);
    }

    public Map<String, Object> getAuditSpParams(int userID, int action, int key) {

        logger.info("Populate add sp parameters for common Log >> User ID:" + userID + " Entity:" + EntityID.getEnum(daoMeta.getEntityID().getId()) + " Action:" + action + " Key:" + key);
        Map<String, Object> addParamMap = new HashMap<>();

        addParamMap.put("entity_id", daoMeta.getEntityID().getId());
        addParamMap.put("action", action);
        addParamMap.put("key", key);
        addParamMap.put("user_id", userID);
        return addParamMap;
    }


    /**
     * Gets next sequence id.
     *
     * @return the next sequence id
     */
    public Object getNextSequenceId() {
        Object result = null;
        try {
            result = DBManager.getInstance().executeGetNextSequenceIdProcedure(entityManager, daoMeta.getSequenceGeneratorSpJpaName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        logger.info("Return object >> " + result);

        return result;
    }

    /**
     * Use to get all entities
     *
     * @return all ListResult object with list of entities
     */
//    public ListResult<I> findAll( int instId) {
//        String filterCriteria = "INST_ID ="+instId;
//        return findPaginatedSearchResults(0, daoMeta.getDefaultSortColumn(), filterCriteria);
//    }
    public ListResult<I> findPaginatedSearchResults(Object filterModel, Object sortModel, int pageNumber) {

//
        HashMap<String, Integer> filterTypes = new HashMap<>();
        HashMap<String, Integer> filterSql = new HashMap<>(); // equal, < , > , like
//        filterTypes.put(0, 1); // filter type (equal, long)


//        ListResult<I> entityListResult = new ListResult<>();
        JsonParser parser = new JsonParser();
        int offset;
        int limit;
//        offset = (pageNumber == 0) ? 0 : ((pageNumber - 1) * ApplicationConstants.RECORDS_PER_PAGE) - 1;
        limit = ApplicationConstants.RECORDS_PER_PAGE;
        if (pageNumber < 0) {
            offset = 0;
            limit = 9999;
        } else {
            offset = pageNumber == 0 ? 0 : (pageNumber * ApplicationConstants.RECORDS_PER_PAGE) - 1;
            limit = ApplicationConstants.RECORDS_PER_PAGE;
        }



        String sort = GSON.toJson(sortModel);
        logger.info("Sort model :" + sort);


        String sortQuery = "";
        String filterQuery = "SELECT o FROM " + daoMeta.getImplClassName() + " o " + "where (o.isDeleted =:deleted)";
        String countSql = "select count(1) from " + daoMeta.getImplClassName() + " o " + "where (o.isDeleted =:deleted)";

        String sortKey = "";
        String sortDir = "";
        HashMap<String, String> params = new HashMap<>();

        if (!sortModel.equals("") && sortModel != null) {


            JsonObject sortModelObj = parser.parse(sort).getAsJsonObject();
            for (Map.Entry<String, JsonElement> element : sortModelObj.entrySet()) {

                sortKey = element.getKey();
                JsonObject sortObj = element.getValue().getAsJsonObject();
                for (Map.Entry<String, JsonElement> sortElement : sortObj.entrySet()) {
                    if (sortElement.getKey().equals("dir")) {
                        sortDir = sortElement.getValue().getAsString();
                    }
                }
            }
            if (!sortKey.equals("") && sortKey != null && sortDir != null && !sortDir.equals("")) {
                sortQuery = " ORDER BY o." + sortKey + " " + sortDir;
            }
            logger.info("Sort query : " + sortQuery);
        } else {
            sortQuery = " ORDER BY o." + daoMeta.getKeyPropertyName() + " " + "ASC";
        }

        if (!filterModel.equals("") && filterModel != null) {
            JsonObject filterModelObj = parser.parse(GSON.toJson(filterModel)).getAsJsonObject();
            filterQuery = "SELECT o FROM " + daoMeta.getImplClassName() + " o " + "where (o.isDeleted =:deleted) AND  ( ";
            countSql = "select count(1) from " + daoMeta.getImplClassName() + " o " + "where (o.isDeleted =:deleted) AND (";
            Set<String> filterStrings = new HashSet<>();


            String filterKey = "";
            int filterType = -1;
            String filterVal = "";
            int filterColType = 0;  // 1 = int , 2 = long, 3 = string 4 = booolean
            for (Map.Entry<String, JsonElement> element : filterModelObj.entrySet()) {

                filterKey = element.getKey();
                JsonObject filterObj = element.getValue().getAsJsonObject();
                for (Map.Entry<String, JsonElement> filterElement : filterObj.entrySet()) {
                    if (filterElement.getKey().equals("type")) {
                        filterType = filterElement.getValue().getAsInt();
                    }
                    if (filterElement.getKey().equals("value")) {
                        filterVal = filterElement.getValue().getAsString();
                    }
                    if (filterElement.getKey().equals("col")) {
                        filterColType = filterElement.getValue().getAsInt();
                    }
                }
                if (!filterKey.equals("") && filterKey != null && !filterVal.equals("") && filterVal != null && filterType != -1) {
                    filterStrings.add(getFilter(filterKey, filterType));
                    filterSql.put(filterKey, filterType);
                    params.put(filterKey, filterVal);
                    filterTypes.put(filterKey, filterColType);
                }

            }

            int index = 0;
            for (String s : filterStrings) {

                if (index != 0 && index <= filterStrings.size() - 1) {
                    filterQuery += " AND ";
                    countSql += " AND ";
                }
                filterQuery += s;
                countSql += s;
                index++;
            }

            filterQuery += " ) ";
            countSql += " ) ";
        } else {

        }


        filterQuery += sortQuery;
//        filterQuery += " limit " + offset + " , " + limit;  no support in hql


        logger.info("Filter Query : " + filterQuery);

        Query listQuery = entityManager.createQuery(filterQuery, beanClass);

        //count
        Query countQuery = entityManager.createQuery(countSql,Long.class);


        countQuery.setParameter("deleted", 0);

        listQuery.setMaxResults(limit);
        listQuery.setFirstResult(offset);
        listQuery.setParameter("deleted", 0);
//        listQuery.setP

        params.forEach((k, v) -> {
            switch (filterTypes.get(k)) {
                case 1: {// int
                    listQuery.setParameter(k, Integer.parseInt(v.indexOf('.') != -1 ? v.substring(0, v.indexOf('.')) : v));
                    countQuery.setParameter(k, Integer.parseInt(v.indexOf('.') != -1 ? v.substring(0, v.indexOf('.')) : v));
                    break;
                }
                case 2: {// long
                    listQuery.setParameter(k, (Object) (new java.lang.Long(Long.parseLong(v.indexOf('.') != -1 ? v.substring(0, v.indexOf('.')) : v))));
                    countQuery.setParameter(k, (Object) (new java.lang.Long(Long.parseLong(v.indexOf('.') != -1 ? v.substring(0, v.indexOf('.')) : v))));
                    break;
                }
                case 3: { // string
                    if(filterSql.get(k) == 4) {
                        listQuery.setParameter(k, "%" +v + "%");
                        countQuery.setParameter(k, "%" +v + "%");
                    }else {
                        listQuery.setParameter(k, v);
                        countQuery.setParameter(k, v);
                    }

                    break;
                }
                case 4: {
                    listQuery.setParameter(k, Boolean.parseBoolean(v));
                    countQuery.setParameter(k, Boolean.parseBoolean(v));
                    break;
                }
                case 5: { // float
                    listQuery.setParameter(k, Float.parseFloat(v));
                    countQuery.setParameter(k, Float.parseFloat(v));
                    break;
                }
            }
//            listQuery.setParameter(k, (String) v);
        });

        try {

            Object resultView = listQuery.getResultList();

            logger.info("Results for query >> " + resultView);

            List<I> response = (List<I>) resultView;

            logger.info("Results after cast for query >> " + response);

            Long count = (Long) countQuery.getSingleResult();


            return new ListResult<I>(response, count.intValue());
        } catch (Exception e) {
            logger.error("Error in executing query", e);
        }




        return new ListResult<>();
    }

    /**
     * Execute given view by considering given filter criteria using common sp in data base. Statement sort result set according to given sort order and return only given index related records only.
     *
     * @param em               is the entity manager
     * @param viewName         is the executing base view name
     * @param sortColumn       order the result set using this field. Should be available in view output column list
     * @param filterCriteria   is the basic conditions available in Sql WHERE condition block
     * @param firstRecordIndex used to filter result ser which is ROWNUM is greater than this index
     * @param lastRecordIndex  used to filter result ser which is ROWNUM is less than this index
     * @return result set after executing the view by applying filter criteria and paging criteria
     */
    private ListResult<I> executeBaseProcedure(EntityManager em, String viewName, String sortColumn, String filterCriteria, int firstRecordIndex, int lastRecordIndex) {
        try {
            logger.info("Execute Common SP  >> " + em + "|viewName:" + viewName + "|sortColumn:" + sortColumn + "|filterCriteria:" + filterCriteria + "|firstRecordIndex:" + firstRecordIndex + "|lastRecordIndex:" + lastRecordIndex);

            StoredProcedureQuery query = em.createStoredProcedureQuery("sp_get_list_" + viewName, beanClass);

            if (query == null) {
                logger.error("Failed to generate StoredProcedureQuery for Common SP >> pkg_dc_common.sp_show_range");
                return new ListResult<>(Collections.emptyList(), -1); // TODO [check] return -1 as count when an exception
            }

            logger.info("Define parameters of CommonSP StoredProcedureQuery.");

            query.registerStoredProcedureParameter("p_filter", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("p_sortColumn", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("p_count", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("p_offset", Integer.class, ParameterMode.IN);

            logger.info("Assign parameter values of CommonSP StoredProcedureQuery.");

            query.setParameter("p_filter", viewName);
            query.setParameter("p_sortColumn", sortColumn);
            query.setParameter("p_count", lastRecordIndex - firstRecordIndex);
            query.setParameter("p_offset", firstRecordIndex - 1);

            logger.info("Ready to execute common sp");

            query.execute();


            logger.info("Executed common sp");

            int totalNoOfRows;

//            Object resultingRowCount = query.getOutputParameterValue("prows");
//            if (resultingRowCount == null) {
//
//                logger.error("prows Out parameter is null");
//                totalNoOfRows = -1;
//            } else {
//                logger.info("Total number of results for query >> " + resultingRowCount);
//                totalNoOfRows = (int) resultingRowCount;
//            }

//            logger.info("Total number of results (integer value) for query >> " + totalNoOfRows);

            Object resultView = query.getResultList();

            logger.info("Results for query >> " + resultView);

            List<I> response = (List<I>) resultView;

            logger.info("Results after cast for query >> " + response);

            return new ListResult<I>(response, ((List<I>) resultView).size());

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ListResult<>(Collections.emptyList(), -1); // TODO [check] return -1 as count when an exception
        }
    }
}
