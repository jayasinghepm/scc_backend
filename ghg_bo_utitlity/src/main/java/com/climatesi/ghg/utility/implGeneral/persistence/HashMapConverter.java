package com.climatesi.ghg.utility.implGeneral.persistence;

import org.jboss.logging.Logger;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashMap;

@Converter
public class HashMapConverter implements AttributeConverter<HashMap<String, String>, String> {

    private static Logger logger = Logger.getLogger(HashMapConverter.class);


    @Override
    public String convertToDatabaseColumn(HashMap<String, String> stringStringHashMap) {
        if (stringStringHashMap == null || stringStringHashMap.size() == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringStringHashMap.forEach((k, v) -> {
//           jsonObject.addProperty(k,v);
            stringBuilder.append(k + "-" + v + ",");
        });
        return stringBuilder.toString();
    }

    @Override
    public HashMap<String, String> convertToEntityAttribute(String s) {
        if (s == null || s.equals("")) {
            return new HashMap<>();
        }
        String[] values = s.split(",");
        HashMap<String, String> map = new HashMap<>();
        try {
            for (int i = 0; i < values.length; i++) {
                String key = values[i].split("-")[0];
                String value = values[i].split("-")[1];
                map.put(key, value);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return map;
    }
}
