package com.climatesi.ghg.utility.api.enums;

public enum FlightClass {

    FirstClass(1),
    BusinessClass(2),
    EconomicClass (3);

    public int id ;

    FlightClass(int id) {this.id = id;}
}
