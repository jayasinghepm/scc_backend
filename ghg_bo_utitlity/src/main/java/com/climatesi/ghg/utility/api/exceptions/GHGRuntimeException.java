package com.climatesi.ghg.utility.api.exceptions;

public class GHGRuntimeException extends RuntimeException {
    public GHGRuntimeException() {
        super();
    }

    public GHGRuntimeException(Exception ex) {
        super(ex);
    }

    public GHGRuntimeException(String message) {
        super(message);
    }

    public GHGRuntimeException(String message, Exception ex) {
        super(message, ex);
    }
}
