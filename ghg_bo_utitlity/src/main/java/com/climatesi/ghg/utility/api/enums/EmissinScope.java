package com.climatesi.ghg.utility.api.enums;

public enum EmissinScope {

    Scope1(1),
    Scope2(2),
    Scope3(3);

    private int scope;

    EmissinScope(int scope) {
        this.scope = scope;
    }

    public int getScope() {
        return scope;
    }
}
