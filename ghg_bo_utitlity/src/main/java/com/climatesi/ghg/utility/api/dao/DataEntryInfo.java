package com.climatesi.ghg.utility.api.dao;

import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.enums.EntityID;
import com.climatesi.ghg.utility.api.enums.PersistMode;

public class DataEntryInfo {
    private String viewName;
    private String defaultSortColumn;
    private String outParameterName = ApplicationConstants.DEFAULT_SP_OUT_PARA_NAME;

    private String addSpJpaName;
    private PersistMode addMode = PersistMode.JPA_PERSIST;
    private boolean hasAddOutPara = true;

    private String editSpJpaName;
    private PersistMode editMode = PersistMode.JPA_PERSIST;
    private boolean hasEditOutPara = false;

    private String keyPropertyName;
    private String findByKeyQueryJpaName;
    private String statusChangeSpJpaName;
    private String sequenceGeneratorSpJpaName;
    private String implClassName;

    private EntityID entityID;

    //region Constructor

    public DataEntryInfo(String viewName, String defaultSortColumn, String implClassName) {

        this(viewName, defaultSortColumn, implClassName + ".AddSP", implClassName + ".EditSP", implClassName + ".findById", implClassName + ".ChangeStatusSP",implClassName + ".getNextSeqIdSP");
        this.implClassName = implClassName;
    }

    public DataEntryInfo(String viewName, String defaultSortColumn) {
        this(viewName, defaultSortColumn, "", "", "", "","");
    }

    public DataEntryInfo(String viewName, String defaultSortColumn, String addSpJpaName, String editSpJpaName, String findByKeyQueryJpaName, String statusChangeSpJpaName, String sequenceGeneratorSpJpaName) {
        this.viewName = viewName;
        this.defaultSortColumn = defaultSortColumn;
        this.addSpJpaName = addSpJpaName;
        this.editSpJpaName = editSpJpaName;
        this.findByKeyQueryJpaName = findByKeyQueryJpaName;
        this.statusChangeSpJpaName = statusChangeSpJpaName;
        this.sequenceGeneratorSpJpaName=sequenceGeneratorSpJpaName;
    }

    //endregion

    //region Getters and Setters

    public String getViewName() {
        return viewName;
    }

    public String getDefaultSortColumn() {
        return defaultSortColumn;
    }

    public String getAddSpJpaName() {
        return addSpJpaName;
    }

    public String getEditSpJpaName() {
        return editSpJpaName;
    }

    public String getOutParameterName() {
        return outParameterName;
    }

    public void setOutParameterName(String outParameterName) {
        this.outParameterName = outParameterName;
    }

    public PersistMode getAddMode() {
        return addMode;
    }

    public void setAddMode(PersistMode addMode) {
        this.addMode = addMode;
    }

    public boolean hasAddOutPara() {
        return hasAddOutPara;
    }

    public void setAddOutPara(boolean hasAddOutPara) {
        this.hasAddOutPara = hasAddOutPara;
    }

    public PersistMode getEditMode() {
        return editMode;
    }

    public void setEditMode(PersistMode editMode) {
        this.editMode = editMode;
    }

    public boolean hasEditOutPara() {
        return hasEditOutPara;
    }

    public void setEditOutPara(boolean hasEditOutPara) {
        this.hasEditOutPara = hasEditOutPara;
    }

    public String getKeyPropertyName() {
        return keyPropertyName;
    }

    public void setKeyPropertyName(String keyPropertyName) {
        this.keyPropertyName = keyPropertyName;
    }

    public String getFindByKeyQueryJpaName() {
        return findByKeyQueryJpaName;
    }

    public String getStatusChangeSpJpaName() {
        return statusChangeSpJpaName;
    }

    public String getSequenceGeneratorSpJpaName() {
        return sequenceGeneratorSpJpaName;
    }
//    //endregion
    public EntityID getEntityID(){
        return entityID;
    }

    public void setEntityID(EntityID entityID){
        this.entityID=entityID;
    }

    public String getImplClassName() {
        return implClassName;
    }
    //endregion

}
