package com.climatesi.ghg.utility.api.constants;

// Todo : Have to update this file
public class ApplicationConstants {

    private ApplicationConstants() {

    }

    //PreFIx for the ejb
    public static final String BO_JNDI_PREFIX = "";
    //default record count for a page
    public static final int RECORDS_PER_PAGE = 20;
    //Default output param name of a Stored Procedure
    public static final String DEFAULT_SP_OUT_PARA_NAME = "pkey";
    public static final String DEFAULT_GET_NEXT_SEQ_OUT_PARA_NAME = "id";
    public static final int ERROR_JPA_SP_EXECUTE = -1;
    public static final int DEFAULT_SUCCESS_RESPONSE_FOR_SP_EXECUTE = 1;

    public static final String DEFAULT_SUCCESS_RESPONSE_FOR_JPA_EXECUTE = "1";
    public static final String DEFAULT_FAILED_CODE_FOR_JPA_EXECUTE = "-1";

    public static final String DEFAULT_SUCCESS_CODE_FOR_ADD_EDIT = "1|Success";
    public static final String DEFAULT_FAILED_CODE_FOR_ADD_EDIT = "-1|Failed";

    public static final String DEFAULT_SUCCESS_CODE_FOR_CHANGE_STATUS = "1|Success";
    public static final String DEFAULT_FAILED_CODE_FOR_CHANGE_STATUS = "-1|Failed";

    //Use this user id for execute unit tests
    public static final int DEFAULT_TEST_USER_EMPLOYEE_ID = 1;

    public static final String DEFAULT_DATE_FORMAT = "YYYY-MM-DD"; //eg: "2010-10-2"

    public static final int FIRST_INDEX = 1;

    public static final int RANDOM_PASSWORD_LENGTH = 8;

}
