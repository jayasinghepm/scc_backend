package com.climatesi.ghg.utility.api.facade;

public interface DataEntryManager<E, K> extends DataEntityManager<E, K> {
    /**
     * Insert entity information into database
     *
     * @param entity contains new entity information
     * @return added entity meta information. Most probably this may contains the auto generated private key information.
     */
    Object addEntity(E entity);

    /**
     * Insert entity information into database
     *
     * @param entity contains new entity information
     * @param userID the user changed the entity
     * @return added entity meta information. Most probably this may contains the auto generated private key information.
     */
    Object addEntity(E entity,int userID);

    /**
     * Update entity information in database
     *
     * @param entity contains updated information which is required to persist
     * @return update action related information.
     */
    Object updateEntity(E entity);

    /**
     * Update entity information in database
     *
     * @param entity contains updated information which is required to persist
     * @param userID the user changed the entity
     * @return update action related information.
     */
    Object updateEntity(E entity,int userID);

    /**
     * Change the approval status of master data entity
     *
     * @param entity entity concrete implementation object
     * @return true if change status action is success. otherwise it is false
     */
    boolean setStatusOfEntity(E entity);

    /**
     * Change the approval status of master data entity
     *
     * @param entity entity concrete implementation object
     * @param userID the user changed the entity
     * @return true if change status action is success. otherwise it is false
     */
    boolean setStatusOfEntity(E entity,int userID);

    /**
     * Change the approval status of master data entity
     * This setStatus method uses for change the status of entities which is return and out param from DB to app server
     *
     * @param entity entity concrete implementation object
     * @return status change actions
     */

    Object setStatus(E entity);

    /**
     * Used to get a created dummy entity object
     *
     * @return dummy object without any property values
     */
    E getDummyEntity();
}
