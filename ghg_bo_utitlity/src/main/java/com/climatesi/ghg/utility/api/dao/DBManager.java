package com.climatesi.ghg.utility.api.dao;

import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import org.jboss.logging.Logger;


import java.util.Map;
import javax.persistence.*;

public class DBManager {
    private static DBManager instance = null;
    private static Logger logger = Logger.getLogger(DBManager.class);

    private DBManager() {
        //Left empty intentionally
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }

        return instance;
    }

    /**
     * Execute given named stored procedure
     *
     * @param em         Entity Manager instance for JPA entity implementation
     * @param jpaSpName  is the name property value of the NamedStoredProcedureQuery object..look at your bean in genImpl
     * @param parameters HashMap of procedure input parameter list.
     * @param hasOutPara indicate is it have an OUT parameter. If this is true, OUT parameter should be ApplicationConstants.DEFAULT_SP_OUT_PARA_NAME
     * @return
     */
    public String executeNamedProcedure(EntityManager em, String jpaSpName, Map<String, Object> parameters, boolean hasOutPara) {
        try {
            logger.info("Execute Named Stored Procedure  >> " + em + "|" + jpaSpName + "|" + parameters);

            StoredProcedureQuery query = em.createNamedStoredProcedureQuery(jpaSpName);

            if (query == null) {
                logger.error("Failed to GET Named Stored Procedure >> " + jpaSpName);
                return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
            }

            logger.info("Assign" + parameters.size() + " parameter(s) to " + jpaSpName);

            parameters.keySet().forEach(i -> query.setParameter(i, parameters.get(i)));

            logger.info("Ready to execute named stored procedure");

            query.execute();

            logger.info("Executed named stored procedure");

            String returnStr;

            if (hasOutPara) {
                //TODO : Pass OUT Parameter name as a parameter. Meta data default value is this. Can be override.
                Object result = query.getOutputParameterValue(ApplicationConstants.DEFAULT_SP_OUT_PARA_NAME);
                if (result == null) {
                    logger.error("Out parameter is null");
                    return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
                }

                returnStr = result.toString();
            } else {
                returnStr = String.valueOf(ApplicationConstants.DEFAULT_SUCCESS_RESPONSE_FOR_SP_EXECUTE);
            }

            logger.info("SP OUT Parameter Value >> " + returnStr);

            return returnStr;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
        }
    }

    public String executeProcedure(EntityManager em, String spName, Map<String, Object> inParamsandValues, boolean hasOutPara) {
        try {
            logger.info("Execute Stored Procedure  >> " + em + "|" + spName + "|" + inParamsandValues);

            StoredProcedureQuery query = em.createStoredProcedureQuery(spName);

            logger.info("Assign" + inParamsandValues.size() + " parameter(s) to " + spName);
            inParamsandValues.keySet().forEach(i -> query.registerStoredProcedureParameter(i, inParamsandValues.get(i).getClass(), ParameterMode.IN));
            if (hasOutPara){
                query.registerStoredProcedureParameter(ApplicationConstants.DEFAULT_SP_OUT_PARA_NAME, String.class, ParameterMode.OUT);
            }

            inParamsandValues.keySet().forEach(i -> query.setParameter(i, inParamsandValues.get(i)));

            logger.info("Ready to execute the stored procedure: " + spName);

            query.execute();

            logger.info("Executed named stored procedure");

            String returnValue = String.valueOf(ApplicationConstants.DEFAULT_SUCCESS_RESPONSE_FOR_SP_EXECUTE);

            if(hasOutPara) {
                Object result = query.getOutputParameterValue(ApplicationConstants.DEFAULT_SP_OUT_PARA_NAME);
                if (result == null) {
                    logger.error("Out parameter is null");
                    return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
                }

                returnValue = result.toString();
            }

            return returnValue;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
        }
    }

    /**
     * Execute single entity return JPA named query
     *
     * @param em               Entity Manager instance for JPA entity implementation
     * @param jpaQueryName     is the name of NamedQuery
     * @param keyPropertyName  name of key mapping property
     * @param keyPropertyValue searching key value
     * @return entity object which is primary key is equal to given input
     */
    public Object getEntityByNamedQuery(EntityManager em, String jpaQueryName, String keyPropertyName, Object keyPropertyValue) {
        Object response = null;

        try {
            logger.info("Execute Named Query  >> " + em + "|" + jpaQueryName + "|" + keyPropertyName + "|" + keyPropertyValue);

            Query queryForFilter = em.createNamedQuery(jpaQueryName);
            if (queryForFilter == null) {
                logger.error("Failed to generate JPA named query for get entity by key");
                return null;
            }

            queryForFilter = queryForFilter.setParameter(keyPropertyName, keyPropertyValue);

            try {
                response = queryForFilter.getSingleResult();
            } catch (NoResultException e) {
                logger.error("No entity found for key >> " + keyPropertyName);
                return null;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return response;
    }

    /**
     * Execute get next sequence id procedure object.
     *
     * @param em       Entity Manager instance for JPA entity implementation
     * @param jpaSpName  is the name of NamedQuery
     * @return the sequence number return from the stored procedure
     */
    public Object executeGetNextSequenceIdProcedure(EntityManager em, String jpaSpName) {
        try {
            logger.info("Execute Named Stored Procedure to get next sequence number  >> " + em + "|" + jpaSpName);

            StoredProcedureQuery query = em.createNamedStoredProcedureQuery(jpaSpName);

            if (query == null) {
                logger.error("Failed to GET Named Stored Procedure >> " + jpaSpName);
                return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
            }
            logger.info("Ready to execute stored procedure to get next sequence >> " + query.toString());
            query.execute();
            logger.info("Executed named stored procedure");

            Object result = query.getOutputParameterValue(ApplicationConstants.DEFAULT_GET_NEXT_SEQ_OUT_PARA_NAME);
            if (result == null) {
                logger.error("Out parameter is null");
                return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
            }
            return result;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return String.valueOf(ApplicationConstants.ERROR_JPA_SP_EXECUTE);
        }
    }
}

