package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface CompanyManager extends DataEntryManager<Company, Integer> {

    int deleteCompany(int id);
}


