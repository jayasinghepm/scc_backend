package com.climatesi.ghg.api.beans;

import com.climatesi.ghg.implGeneral.AllowedEmissionSrcBean;
import com.climatesi.ghg.implGeneral.EmissionCategoryBean;
import com.climatesi.ghg.implGeneral.ExcludedReasonBean;
import com.climatesi.ghg.implGeneral.ProjectSuggestionBean;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface Company {

    public Set<ProjectSuggestionBean> getProjectSuggestions() ;

    public void setProjectSuggestions(Set<ProjectSuggestionBean> projectSuggestions) ;

    public Set<ExcludedReasonBean> getExcludedReasons();

    public void setExcludedReasons(Set<ExcludedReasonBean> excludedReasons) ;

    public String getMethodology();

    public void setMethodology(String methodology);

    public String getProtocol() ;

    public void setProtocol(String protocol) ;

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getId();

    public void setId(int id);

    public String getName();

    public void setName(String name);

    public String getSector();

    public void setSector(String sector);

    public String getCode();

    public void setCode(String code);

    public List<Integer> getEntitlements();

    public void setEntitlements(List<Integer> entitlements);

    public JsonObject getYearEmission();

    public void setYearEmission(JsonObject yearEmission);

    public int getNoOfEmployees();

    public void setNoOfEmployees(int noOfEmployees);

    public int getNoOfBranches();

    public void setNoOfBranches(int noOfBranches);

    public float getAnnumRevFY();

    public void setAnnumRevFY(float annumRevFY);

    public String getBaseYear();

    public void setBaseYear(String baseYear);

    public String getAddr1();

    public void setAddr1(String addr1);

    public String getAddr2();

    public void setAddr2(String addr2);

    public String getDistrict();

    public void setDistrict(String district);

    public String getLogo();

    public void setLogo(String logo);

    public int getFyMonthStart();

    public void setFyMonthStart(int fyMonthStart);

    public int getFyMonthEnd();

    public void setFyMonthEnd(int fyMonthEnd);

    public String getFyCurrent();

    public void setFyCurrent(String fyCurrent);


    public int getFyCurrentStart();

    public void setFyCurrentStart(int fyCurrentStart);

    public int getFyCurrentEnd();

    public void setFyCurrentEnd(int fyCurrentEnd);

    public boolean isFinacialYear();

    public void setFinacialYear(boolean finacialYear);

    public float getTargetGHG();

    public void setTargetGHG(float targetGHG);

    public List<Integer> getPages() ;

    public void setPages(List<Integer> pages) ;

    public List<Integer> getEmSources() ;

    public void setEmSources(List<Integer> emSources) ;



    public Map<Integer, AllowedEmissionSrcBean> getAllowedEmissionSources();

    public void setAllowedEmissionSources(Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources) ;

    public List<EmissionCategoryBean> getEmissionCategories();

    public void setEmissionCategories(List<EmissionCategoryBean> emissionCategories) ;

}


