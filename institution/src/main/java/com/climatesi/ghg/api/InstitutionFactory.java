package com.climatesi.ghg.api;

import com.climatesi.ghg.api.facades.BranchManager;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.implGeneral.facades.BranchManagerFacade;
import com.climatesi.ghg.implGeneral.facades.CompanyManagerFacade;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class InstitutionFactory {

    private static Logger logger = Logger.getLogger(InstitutionFactory.class);
    private static InstitutionFactory instance;

    private BranchManager branchManager;
    private CompanyManager companyManager;


    private InstitutionFactory() {

    }

    public static InstitutionFactory getInstance() {
        if (instance == null) {
            instance = new InstitutionFactory();
        }
        return instance;
    }

    public BranchManager getBranchManager(EntityManager em) {
        if (branchManager == null) {
            branchManager = new BranchManagerFacade();
            branchManager.injectEntityManager(em);
        }
        return branchManager;
    }

    public CompanyManager getCompanyManager(EntityManager em) {
        if (companyManager == null) {
            companyManager = new CompanyManagerFacade();
            companyManager.injectEntityManager(em);
        }
        return companyManager;
    }


}
