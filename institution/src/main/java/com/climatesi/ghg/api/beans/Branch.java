package com.climatesi.ghg.api.beans;

public interface Branch {

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getCompanyId();

    public void setCompanyId(int companyId);

    public int getId();

    public void setId(int id);

    public String getName();

    public void setName(String name);

    public String getAddr1();

    public void setAddr1(String addr1);

    public String getAddr2();

    public void setAddr2(String addr2);

    public String getDistrict();

    public void setDistrict(String district);

    public String getCode();

    public void setCode(String code);

    public String getUserName();

    public void setUserName(String userName);

    public String getCompany();

    public void setCompany(String company);
}
