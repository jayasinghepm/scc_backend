package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface BranchManager extends DataEntryManager<Branch, Integer> {

    int deleteBranch(int id);

}
