package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.implGeneral.CompanyBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class CompanyDAO extends AbstractDAO<CompanyBean, Company> {

    private static Logger logger = Logger.getLogger(CompanyDAO.class);
    private EntityManager em;

    public CompanyDAO(EntityManager em) {
        super(em, CompanyBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("COMPANY", "COM_ID", "CompanyBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);

    }


    public int deleteCompany(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from CompanyBean l where l.id = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
            logger.info("No of Deleted Companies: " + rows);
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }

    @Override
    public Map<String, Object> getAddSpParams(CompanyBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(CompanyBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(CompanyBean entityImpl) throws GHGException {
        return null;
    }
}
