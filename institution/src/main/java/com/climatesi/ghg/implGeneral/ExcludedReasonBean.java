package com.climatesi.ghg.implGeneral;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

@Embeddable
public class ExcludedReasonBean {

//    private int comId;

    private String srcName;

    @Lob
    private String reason;


//    public int getComId() {
//        return comId;
//    }
//
//    public void setComId(int comId) {
//        this.comId = comId;
//    }

    public String getSrcName() {
        return srcName;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
