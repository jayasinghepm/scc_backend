package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.utility.implGeneral.persistence.IntegerListConverter;
import com.climatesi.ghg.utility.implGeneral.persistence.JsonObjectListConverter;
import com.climatesi.ghg.utility.implGeneral.persistence.StringListConverter;
import com.google.gson.JsonObject;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.*;


@Entity
@Table(name = "COMPANY")
@NamedQueries({
        @NamedQuery(name = "CompanyBean.findById",
                query = "SELECT i FROM CompanyBean i WHERE i.id = :id"),
})
public class CompanyBean implements Company {


    @Id
    @GeneratedValue(generator = "comIdSeq")
    @SequenceGenerator(name = "comIdSeq", sequenceName = "COM_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "COM_ID")
    private int id;

    @Column(name = "NAME", unique = true)
    private String name;

    @Column(name = "SECTOR")
    private String sector;

    @Column(name = "CODE", unique = true)
    private String code;

    @Column(name = "ENTILEMENTS")
    @Convert(converter = IntegerListConverter.class)
    private List<Integer> entitlements;

    @Column(name = "YEAR_EMISSION")
    @Convert(converter = JsonObjectListConverter.class)
    private JsonObject yearEmission;

    @Column(name = "NO_OF_EMPS")
    private int noOfEmployees;

    @Column(name = "NO_OF_BRANCHES")
    private int noOfBranches;

    @Column(name = "ANNUAL_REV_FY")
    private float annumRevFY;

    @Column(name = "BASE_YEAR_GHG_PREPARATION")
    private String baseYear;
    @Column(name = "ADDR1")
    private String addr1;
    @Column(name = "ADDR2")
    private String addr2;
    @Column(name = "DISTRICT")
    private String district;
    @Lob
    @Column(name = "LOGO")
    private String logo;
    @Column(name = "FY_MON_START")
    private int fyMonthStart;
    @Column(name = "FY_MONTH_END")
    private int fyMonthEnd;

    @Column(name = "FY_CURRENT")
    private String fyCurrent;

    @Column(name = "FY_CURRNET_START")
    private int fyCurrentStart;

    @Column(name = "FY_CURRENT_END")
    private int fyCurrentEnd;


    @Column(name = "IS_DELETED")
    private int isDeleted;

    @Column(name = "IS_FINANCIAL_YEAR")
    private boolean isFinacialYear;

    @Column(name = "PAGES")
    @Convert(converter = IntegerListConverter.class)
    private List<Integer> pages = new ArrayList<>();

    @Column(name = "EM_SOURCES")
    @Convert(converter = IntegerListConverter.class)
    private List<Integer> emSources = new ArrayList<>();




    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "em_src_mapping",
            joinColumns = {@JoinColumn(name = "com_id", referencedColumnName = "COM_ID")},
            inverseJoinColumns = {@JoinColumn(name = "em_src_fk", referencedColumnName = "id")})
    @MapKeyColumn(name = "srcId")
    private Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources = new HashMap<>();


    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "em_categories", joinColumns = @JoinColumn(name = "COM_ID"))
    @Column(name = "category")
    private List<EmissionCategoryBean> emissionCategories = new ArrayList<>();


    @ElementCollection()
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = "project_suggestions", joinColumns = @JoinColumn(name = "COM_ID"))
    @Column(name = "company_project_suggestion")
    private Set<ProjectSuggestionBean> projectSuggestions = new HashSet<>();


    @ElementCollection()
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = "src_exclulded_reason", joinColumns = @JoinColumn(name = "COM_ID"))
    @Column(name = "company_project_src_excluded_reason")
    private Set<ExcludedReasonBean> excludedReasons = new HashSet<>();


    public CompanyBean() {
    }

    private String methodology;

    private String protocol;


    public float targetGHG;

    public String getMethodology() {
        return methodology;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Set<ProjectSuggestionBean> getProjectSuggestions() {
        return projectSuggestions;
    }

    public void setProjectSuggestions(Set<ProjectSuggestionBean> projectSuggestions) {
        this.projectSuggestions = projectSuggestions;
    }

    public Set<ExcludedReasonBean> getExcludedReasons() {
        return excludedReasons;
    }

    public void setExcludedReasons(Set<ExcludedReasonBean> excludedReasons) {
        this.excludedReasons = excludedReasons;
    }

    public Map<Integer, AllowedEmissionSrcBean> getAllowedEmissionSources() {
        return allowedEmissionSources;
    }

    public void setAllowedEmissionSources(Map<Integer, AllowedEmissionSrcBean> allowedEmissionSources) {
        this.allowedEmissionSources = allowedEmissionSources;
    }

    public List<EmissionCategoryBean> getEmissionCategories() {
        return emissionCategories;
    }

    public void setEmissionCategories(List<EmissionCategoryBean> emissionCategories) {
        this.emissionCategories = emissionCategories;
    }

    public float getTargetGHG() {
        return targetGHG;
    }

    public void setTargetGHG(float targetGHG) {
        this.targetGHG = targetGHG;
    }

    public int getFyCurrentStart() {
        return fyCurrentStart;
    }

    public void setFyCurrentStart(int fyCurrentStart) {
        this.fyCurrentStart = fyCurrentStart;
    }

    public int getFyCurrentEnd() {
        return fyCurrentEnd;
    }

    public void setFyCurrentEnd(int fyCurrentEnd) {
        this.fyCurrentEnd = fyCurrentEnd;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getFyMonthStart() {
        return fyMonthStart;
    }

    public void setFyMonthStart(int fyMonthStart) {
        this.fyMonthStart = fyMonthStart;
    }

    public int getFyMonthEnd() {
        return fyMonthEnd;
    }

    public void setFyMonthEnd(int fyMonthEnd) {
        this.fyMonthEnd = fyMonthEnd;
    }

    public String getFyCurrent() {
        return fyCurrent;
    }

    public void setFyCurrent(String fyCurrent) {
        this.fyCurrent = fyCurrent;
    }

    public List<Integer> getEntitlements() {
        return entitlements;
    }

    public void setEntitlements(List<Integer> entitlements) {
        this.entitlements = entitlements;
    }

    public JsonObject getYearEmission() {
        return yearEmission;
    }

    public void setYearEmission(JsonObject yearEmission) {
        this.yearEmission = yearEmission;
    }

    public int getNoOfEmployees() {
        return noOfEmployees;
    }

    public void setNoOfEmployees(int noOfEmployees) {
        this.noOfEmployees = noOfEmployees;
    }

    public int getNoOfBranches() {
        return noOfBranches;
    }

    public void setNoOfBranches(int noOfBranches) {
        this.noOfBranches = noOfBranches;
    }

    public float getAnnumRevFY() {
        return annumRevFY;
    }

    public void setAnnumRevFY(float annumRevFY) {
        this.annumRevFY = annumRevFY;
    }

    public String getBaseYear() {
        return baseYear;
    }

    public void setBaseYear(String baseYear) {
        this.baseYear = baseYear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int isDeleted() {
        return isDeleted;
    }

    @Override
    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public boolean isFinacialYear() {
        return isFinacialYear;
    }

    public void setFinacialYear(boolean finacialYear) {
        isFinacialYear = finacialYear;
    }

    public List<Integer> getPages() {
        return pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }

    public List<Integer> getEmSources() {
        return emSources;
    }

    public void setEmSources(List<Integer> emSources) {
        this.emSources = emSources;
    }
}
