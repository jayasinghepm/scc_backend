package com.climatesi.ghg.implGeneral.facades;

import com.climatesi.ghg.api.beans.Company;
import com.climatesi.ghg.api.facades.CompanyManager;
import com.climatesi.ghg.implGeneral.CompanyBean;
import com.climatesi.ghg.implGeneral.persistence.CompanyDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class CompanyManagerFacade implements CompanyManager {

    private Logger logger = Logger.getLogger(CompanyManagerFacade.class);
    private CompanyDAO dao;

    @Override
    public Object addEntity(Company entity) {
        try {
            return dao.insert((CompanyBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding company Bean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(Company entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Company entity) {
        try {
            return dao.update((CompanyBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding company Bean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(Company entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Company entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Company entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Company entity) {
        return null;
    }

    @Override
    public Company getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Company> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Company> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Company getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<Company> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Company> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new CompanyDAO(entityManager);
    }

    @Override
    public int deleteCompany(int id) {
        try {
            return dao.deleteCompany(id);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
