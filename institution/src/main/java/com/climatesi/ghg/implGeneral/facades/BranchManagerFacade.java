package com.climatesi.ghg.implGeneral.facades;

import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.api.facades.BranchManager;
import com.climatesi.ghg.implGeneral.BranchBean;
import com.climatesi.ghg.implGeneral.persistence.BranchDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class BranchManagerFacade implements BranchManager {

    private static Logger logger = Logger.getLogger(BranchManagerFacade.class);
    private BranchDAO dao;

    @Override
    public Object addEntity(Branch entity) {
        try {
            return dao.insert((BranchBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding BranchBEan ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }

    }

    @Override
    public Object addEntity(Branch entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Branch entity) {
        try {
            return dao.update((BranchBean) entity, PersistMode.JPA_PERSIST);
        } catch (Exception e) {
            logger.error("Error occured  while adding BranchBEan ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(Branch entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Branch entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Branch entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Branch entity) {
        return null;
    }

    @Override
    public Branch getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Branch> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Branch> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Branch getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<Branch> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Branch> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new BranchDAO(entityManager);
    }

    @Override
    public int deleteBranch(int id) {
        try {
            return dao.deleteBranch(id);
        }catch (Exception e) {
            logger.error(e);
        }
        return 0;
    }
}
