package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.Branch;

import javax.persistence.*;

@Entity
@Table(name = "BRANCH")
@NamedQueries({
        @NamedQuery(name = "BranchBean.findById",
                query = "SELECT i FROM BranchBean i WHERE i.id = :id"),
})
public class BranchBean implements Branch {

    @Id
    @GeneratedValue(generator = "branchIdSeq")
    @SequenceGenerator(name = "branchIdSeq", sequenceName = "BRANCH_ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "BRANCH_ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ADDR1")
    private String addr1;

    @Column(name = "ADDR2")
    private String addr2;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "CODE")
    private String code;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    private String company;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Override
    public int isDeleted() {
        return isDeleted;
    }

    @Override
    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }
}
