package com.climatesi.ghg.implGeneral;


import institution.src.main.java.com.climatesi.ghg.implGeneral.AllowedEmissionSrcIdBean;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "ALLOWED_EM_SOURCES")
public class AllowedEmissionSrcBean {

    @Id
    private String id; //comId-srcId

    private int srcId;

    private String srcName;

    private int comId;

    //1-direct, 2-indirect  3 ; not selected
    private int direct;

    private int catId;

    private int scopeId;

    public int getScopeId() {
        return scopeId;
    }

    public void setScopeId(int scopeId) {
        this.scopeId = scopeId;
    }

    public int getSrcId() {
        return srcId;
    }

    public void setSrcId(int srcId) {
        this.srcId = srcId;
    }

    public String getSrcName() {
        return srcName;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public int getDirect() {
        return direct;
    }

    public void setDirect(int direct) {
        this.direct = direct;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
