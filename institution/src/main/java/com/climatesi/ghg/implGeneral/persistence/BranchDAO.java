package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.Branch;
import com.climatesi.ghg.implGeneral.BranchBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class BranchDAO extends AbstractDAO<BranchBean, Branch> {

    private static Logger logger = Logger.getLogger(BranchDAO.class);
    private EntityManager em;

    public BranchDAO(EntityManager em) {
        super(em, BranchBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("BRANCH", "BRANCH_ID", "BranchBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    public int deleteBranch(int id) {
        try {
//            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from BranchBean l where l.id = :id");
            query.setParameter("id", id);
            int rows = query.executeUpdate();
//            em.getTransaction().commit();
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }

    @Override
    public Map<String, Object> getAddSpParams(BranchBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(BranchBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(BranchBean entityImpl) throws GHGException {
        return null;
    }
}
