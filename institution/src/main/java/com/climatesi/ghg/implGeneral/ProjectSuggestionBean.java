package com.climatesi.ghg.implGeneral;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

@Embeddable
public class ProjectSuggestionBean {

//    private int comId;

    private String title;

    @Lob
    private String description;

//    public int getComId() {
//        return comId;
//    }
//
//    public void setComId(int comId) {
//        this.comId = comId;
//    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
