package com.climatesi.ghg.implGeneral.data;

public class WasteDisposalCalcData {

    private float weight_waste;
    private float co2_waste_method;

    public float getWeight_waste() {
        return weight_waste;
    }

    public void setWeight_waste(float weight_waste) {
        this.weight_waste = weight_waste;
    }


    public float getCo2_waste_method() {
        return co2_waste_method;
    }

    public void setCo2_waste_method(float co2_waste_method) {
        this.co2_waste_method = co2_waste_method;
    }
}
