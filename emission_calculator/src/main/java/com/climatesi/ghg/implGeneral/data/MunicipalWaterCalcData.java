package com.climatesi.ghg.implGeneral.data;

public class MunicipalWaterCalcData {

    private float consumption_m3;
    private float ef_grid_electricty;
    private float cf_municipal_water;

    public float getConsumption_m3() {
        return consumption_m3;
    }

    public void setConsumption_m3(float consumption_m3) {
        this.consumption_m3 = consumption_m3;
    }

    public float getEf_grid_electricty() {
        return ef_grid_electricty;
    }

    public void setEf_grid_electricty(float ef_grid_electricty) {
        this.ef_grid_electricty = ef_grid_electricty;
    }

    public float getCf_municipal_water() {
        return cf_municipal_water;
    }

    public void setCf_municipal_water(float cf_municipal_water) {
        this.cf_municipal_water = cf_municipal_water;
    }
}
