package com.climatesi.ghg.implGeneral.data;

public class PiggeryCalcData {

    private float feed_rate;

    private float weight;

    private float num_of_days;

    private float gwp_ch4;

    public float getFeed_rate() {
        return feed_rate;
    }

    public void setFeed_rate(float feed_rate) {
        this.feed_rate = feed_rate;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getNum_of_days() {
        return num_of_days;
    }

    public void setNum_of_days(float num_of_days) {
        this.num_of_days = num_of_days;
    }

    public float getGwp_ch4() {
        return gwp_ch4;
    }

    public void setGwp_ch4(float gwp_ch4) {
        this.gwp_ch4 = gwp_ch4;
    }
}
