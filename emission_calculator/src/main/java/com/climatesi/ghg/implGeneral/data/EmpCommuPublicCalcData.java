package com.climatesi.ghg.implGeneral.data;

public class EmpCommuPublicCalcData {

    private float up_down_distance;
    private float ef_public_vehicle_per_passenger_kilometer;

    public float getUp_down_distance() {
        return up_down_distance;
    }

    public void setUp_down_distance(float up_down_distance) {
        this.up_down_distance = up_down_distance;
    }

    public float getEf_public_vehicle_per_passenger_kilometer() {
        return ef_public_vehicle_per_passenger_kilometer;
    }

    public void setEf_public_vehicle_per_passenger_kilometer(float ef_public_vehicle_per_passenger_kilometer) {
        this.ef_public_vehicle_per_passenger_kilometer = ef_public_vehicle_per_passenger_kilometer;
    }
}
