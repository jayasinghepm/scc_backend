package com.climatesi.ghg.implGeneral.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ResultDTO {

    public float tco2e;
    public float co2;
    public float ch4;
    public float n2o;

    public float diesel;
    public float petrol;
    public float r22;
    public float r407c;
    public float r410a;
    public float wgas;
    public float fire_co2;
    public float dcp;
    public float km;
    public float m3;
    public float tons;
    public float kwh;
    public float totalEnergy;

    public float ef;

    public ResultDTO() {
        this.tco2e = 0;
        this.co2 = 0;
        this.ch4 = 0;
        this.n2o = 0;
        this.n2o = 0;
        this.diesel = 0;
        this.petrol = 0;
        this.r22 = 0;
        this.r410a = 0;
        this.r407c = 0;
        this.wgas = 0;
        this.fire_co2 = 0;
        this.dcp = 0;
        this.km = 0;
        this.m3 = 0;
        this.tons = 0;
        this.kwh = 0;
    }


    public ResultDTO(float tco2e, float  co2,float ch4, float n2o) {
        this.tco2e = tco2e;
        this.co2 = co2;
        this.ch4 = ch4;
        this.n2o = n2o;

    }


    public float getTotalEnergy() {
        return totalEnergy;
    }

    public void setTotalEnergy(float totalEnergy) {
        this.totalEnergy = totalEnergy;
    }

    public JsonObject toJson() {
        JsonObject obj = new JsonObject();
        obj.addProperty("tco2", tco2e );
        obj.addProperty("co2", co2);
        obj.addProperty("ch4", ch4);
        obj.addProperty("diesel", diesel);
        obj.addProperty("petrol", petrol);
        obj.addProperty("r22", r22);
        obj.addProperty("r407c", r407c);
        obj.addProperty("r410a", r410a);
        obj.addProperty("wgas", wgas);
        obj.addProperty("fire_co2",fire_co2);
        obj.addProperty("dcp",dcp);
        obj.addProperty("km",km);
        obj.addProperty("m3",m3);
        obj.addProperty("tons",tons);
        obj.addProperty("kwh",kwh);
        return obj;
    }
}
