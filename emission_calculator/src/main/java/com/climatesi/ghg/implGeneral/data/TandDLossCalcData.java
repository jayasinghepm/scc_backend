package com.climatesi.ghg.implGeneral.data;

public class TandDLossCalcData {

    private float t_d_percent;

    private float ef_grid_electricity;
    private float consumption;

    public float getT_d_percent() {
        return t_d_percent;
    }

    public void setT_d_percent(float t_d_percent) {
        this.t_d_percent = t_d_percent;
    }

    public float getEf_grid_electricity() {
        return ef_grid_electricity;
    }

    public void setEf_grid_electricity(float ef_grid_electricity) {
        this.ef_grid_electricity = ef_grid_electricity;
    }

    public float getConsumption() {
        return consumption;
    }

    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }
}
