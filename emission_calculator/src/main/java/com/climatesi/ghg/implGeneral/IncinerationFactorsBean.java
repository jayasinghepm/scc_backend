package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.IncinerationFactor;

import javax.persistence.*;

@Entity
@Table(name = "CONST_INCINERATION_FACTORS")
@NamedQueries({
        @NamedQuery(name = "IncinerationFactorsBean.findById",
                query = "SELECT i FROM IncinerationFactorsBean i WHERE i.id = :id"),
})
public class IncinerationFactorsBean implements IncinerationFactor {

    @Id
    @Column(name = "ID")
    private int id;

    private float dry_matter_per;

    private float carbon_fraction;

    private float fossil_carbon_fraction;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getDry_matter_per() {
        return dry_matter_per;
    }

    public void setDry_matter_per(float dry_matter_per) {
        this.dry_matter_per = dry_matter_per;
    }

    public float getCarbon_fraction() {
        return carbon_fraction;
    }

    public void setCarbon_fraction(float carbon_fraction) {
        this.carbon_fraction = carbon_fraction;
    }

    public float getFossil_carbon_fraction() {
        return fossil_carbon_fraction;
    }

    public void setFossil_carbon_fraction(float fossil_carbon_fraction) {
        this.fossil_carbon_fraction = fossil_carbon_fraction;
    }
}
