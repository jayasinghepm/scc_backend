package com.climatesi.ghg.implGeneral.data;

public class GWP_R407C {

    private float weight;
    private float ch2f2_percent;
    private float ch2f2_coff;
    private float cf3chf2_percent;
    private float cf3chf2_coff;
    private float cf3ch2f_percent;
    private float cf3ch2f_coff;

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getCh2f2_percent() {
        return ch2f2_percent;
    }

    public void setCh2f2_percent(float ch2f2_percent) {
        this.ch2f2_percent = ch2f2_percent;
    }

    public float getCh2f2_coff() {
        return ch2f2_coff;
    }

    public void setCh2f2_coff(float ch2f2_coff) {
        this.ch2f2_coff = ch2f2_coff;
    }

    public float getCf3chf2_percent() {
        return cf3chf2_percent;
    }

    public void setCf3chf2_percent(float cf3chf2_percent) {
        this.cf3chf2_percent = cf3chf2_percent;
    }

    public float getCf3chf2_coff() {
        return cf3chf2_coff;
    }

    public void setCf3chf2_coff(float cf3chf2_coff) {
        this.cf3chf2_coff = cf3chf2_coff;
    }

    public float getCf3ch2f_percent() {
        return cf3ch2f_percent;
    }

    public void setCf3ch2f_percent(float cf3ch2f_percent) {
        this.cf3ch2f_percent = cf3ch2f_percent;
    }

    public float getCf3ch2f_coff() {
        return cf3ch2f_coff;
    }

    public void setCf3ch2f_coff(float cf3ch2f_coff) {
        this.cf3ch2f_coff = cf3ch2f_coff;
    }
}
