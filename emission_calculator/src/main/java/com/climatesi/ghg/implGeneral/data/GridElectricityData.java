package com.climatesi.ghg.implGeneral.data;

public class GridElectricityData {

    private float consumption;
    private float ef_grid_electricity;

    public float getConsumption() {
        return consumption;
    }

    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }

    public float getEf_grid_electricity() {
        return ef_grid_electricity;
    }

    public void setEf_grid_electricity(float ef_grid_electricity) {
        this.ef_grid_electricity = ef_grid_electricity;
    }
}
