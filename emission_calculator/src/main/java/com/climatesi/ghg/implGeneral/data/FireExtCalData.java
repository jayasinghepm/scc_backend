package com.climatesi.ghg.implGeneral.data;

public class FireExtCalData {

    private float weight_per_tank;
    private float number_of_tanks;

    public float getWeight_per_tank() {
        return weight_per_tank;
    }

    public void setWeight_per_tank(float weight_per_tank) {
        this.weight_per_tank = weight_per_tank;
    }

    public float getNumber_of_tanks() {
        return number_of_tanks;
    }

    public void setNumber_of_tanks(float number_of_tanks) {
        this.number_of_tanks = number_of_tanks;
    }
}
