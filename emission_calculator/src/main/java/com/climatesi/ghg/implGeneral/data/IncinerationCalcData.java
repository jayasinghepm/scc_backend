package com.climatesi.ghg.implGeneral.data;

public class IncinerationCalcData {

    private float weightwaste;

    private float dry_content_percen;

    private float carbon_fraction;

    private float fossil_carbon_fraction;

    private float weight_solid_waste;

    private float ef_ch4;
    private float ef_n20;
    private float oxidation_factor;


    public float getWeightwaste() {
        return weightwaste;
    }

    public void setWeightwaste(float weightwaste) {
        this.weightwaste = weightwaste;
    }

    public float getDry_content_percen() {
        return dry_content_percen;
    }

    public void setDry_content_percen(float dry_content_percen) {
        this.dry_content_percen = dry_content_percen;
    }

    public float getCarbon_fraction() {
        return carbon_fraction;
    }

    public void setCarbon_fraction(float carbon_fraction) {
        this.carbon_fraction = carbon_fraction;
    }

    public float getFossil_carbon_fraction() {
        return fossil_carbon_fraction;
    }

    public void setFossil_carbon_fraction(float fossil_carbon_fraction) {
        this.fossil_carbon_fraction = fossil_carbon_fraction;
    }

    public float getWeight_solid_waste() {
        return weight_solid_waste;
    }

    public void setWeight_solid_waste(float weight_solid_waste) {
        this.weight_solid_waste = weight_solid_waste;
    }

    public float getEf_ch4() {
        return ef_ch4;
    }

    public void setEf_ch4(float ef_ch4) {
        this.ef_ch4 = ef_ch4;
    }

    public float getEf_n20() {
        return ef_n20;
    }

    public void setEf_n20(float ef_n20) {
        this.ef_n20 = ef_n20;
    }

    public float getOxidation_factor() {
        return oxidation_factor;
    }

    public void setOxidation_factor(float oxidation_factor) {
        this.oxidation_factor = oxidation_factor;
    }
}
