package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;

import javax.persistence.*;

@Entity
@Table(name = "CONST_PUBLIC_TRANSPORT_FACTORS")
@NamedQueries({
        @NamedQuery(name = "PublicTransportEmissionFactorsBean.findById",
                query = "SELECT i FROM PublicTransportEmissionFactorsBean i WHERE i.vehicleType = :vehicleType"),
})
public class PublicTransportEmissionFactorsBean implements PublicTransposrtEmisssionFactor {

    @Id
    @GeneratedValue(generator = "vehicleTypeSeq")
    @SequenceGenerator(name = "vehicleTypeSeq", sequenceName = "VEHICLE_TYPE_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "VEHICLE_TYPE")
    private int vehicleType;
    @Column(name = "VEHICLE_NAME")
    private String vehicleName;
    @Column(name = "G_PER_KM")
    private float g_per_km;
    @Column(name = "CO2_PER_KM")
    private float co2_per_km;
    @Column(name = "ASSUMPTION")
    private String assumption;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    public int getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(int vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public float getG_per_km() {
        return g_per_km;
    }

    public void setG_per_km(float g_per_km) {
        this.g_per_km = g_per_km;
    }

    public float getCo2_per_km() {
        return co2_per_km;
    }

    public void setCo2_per_km(float co2_per_km) {
        this.co2_per_km = co2_per_km;
    }

    public String getAssumption() {
        return assumption;
    }

    public void setAssumption(String assumption) {
        this.assumption = assumption;
    }

    @Override
    public int isDeleted() {
        return this.isDeleted;
    }

    @Override
    public void setDeleted(int deleted) {
        this.isDeleted = deleted;
    }
}
