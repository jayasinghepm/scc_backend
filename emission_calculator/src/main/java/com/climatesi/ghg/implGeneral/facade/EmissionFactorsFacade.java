package com.climatesi.ghg.implGeneral.facade;

import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.implGeneral.EmissionFactorsStoreBean;
import com.climatesi.ghg.persistence.EmissionFactorsDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class EmissionFactorsFacade implements EmissionFactorsManager {


    private static Logger logger = Logger.getLogger(EmissionFactorsFacade.class);
    private EmissionFactorsDAO dao;


    @Override
    public Object addEntity(EmissionFactors entity) {
        try {
            return dao.insert((EmissionFactorsStoreBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding EmissionFactorsStoreBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(EmissionFactors entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(EmissionFactors entity) {
        try {
            return dao.update((EmissionFactorsStoreBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating FireExtingEntry ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(EmissionFactors entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(EmissionFactors entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(EmissionFactors entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(EmissionFactors entity) {
        return null;
    }

    @Override
    public EmissionFactors getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<EmissionFactors> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<EmissionFactors> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public EmissionFactors getEntityByKey(Integer id) {
        return this.dao.findByKey(id);
    }

    @Override
    public ListResult<EmissionFactors> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<EmissionFactors> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new EmissionFactorsDAO(entityManager);
    }
}
