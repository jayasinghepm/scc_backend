package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.WasteDisposalFactors;

import javax.persistence.*;

@Entity
@Table(name = "CONST_WASTE_DISPOSAL_FACTORS")
@NamedQueries({
        @NamedQuery(name = "WasteDisposalFactorsBean.findById",
                query = "SELECT i FROM WasteDisposalFactorsBean i WHERE i.wasteType = :wasteType"),
})
public class WasteDisposalFactorsBean implements WasteDisposalFactors {

    @Id
    @GeneratedValue(generator = "wasteDisSeq")
    @SequenceGenerator(name = "wasteDisSeq", sequenceName = "WASTE_TYPE_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "WASTE_TYPE")
    private int wasteType;

    @Column(name = "WASTE_NAME")
    private String wasteName;

    @Column(name = "UNITS")
    private String units;

    @Column(name = "REUSE")
    private float reuse;
    @Column(name = "OPEN_LOOP")
    private float open_loop;
    @Column(name = "CLOSED_LOOP")
    private float closed_loop;
    @Column(name = "COMBUSTION")
    private float combustion;
    @Column(name = "COMPOSTING")
    private float composting;
    @Column(name = "LAND_FIL")
    private float landfill;
    @Column(name = "ANAEROBIC_DIGESTION")
    private float anaerobic_digestion;

    @Column(name = "IS_DELETED")
    private int isDeleted;



    public int getWasteType() {
        return wasteType;
    }

    public void setWasteType(int wasteType) {
        this.wasteType = wasteType;
    }

    public String getWasteName() {
        return wasteName;
    }

    public void setWasteName(String wasteName) {
        this.wasteName = wasteName;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public float getReuse() {
        return reuse;
    }

    public void setReuse(float reuse) {
        this.reuse = reuse;
    }

    public float getOpen_loop() {
        return open_loop;
    }

    public void setOpen_loop(float open_loop) {
        this.open_loop = open_loop;
    }

    public float getClosed_loop() {
        return closed_loop;
    }

    public void setClosed_loop(float closed_loop) {
        this.closed_loop = closed_loop;
    }

    public float getCombustion() {
        return combustion;
    }

    public void setCombustion(float combustion) {
        this.combustion = combustion;
    }

    public float getComposting() {
        return composting;
    }

    public void setComposting(float composting) {
        this.composting = composting;
    }

    public float getLandfill() {
        return landfill;
    }

    public void setLandfill(float landfill) {
        this.landfill = landfill;
    }

    public float getAnaerobic_digestion() {
        return anaerobic_digestion;
    }

    public void setAnaerobic_digestion(float anaerobic_digestion) {
        this.anaerobic_digestion = anaerobic_digestion;
    }

    @Override
    public int isDeleted() {
        return this.isDeleted;
    }

    @Override
    public void setDeleted(int deleted) {
        this.isDeleted = deleted;
    }
}
