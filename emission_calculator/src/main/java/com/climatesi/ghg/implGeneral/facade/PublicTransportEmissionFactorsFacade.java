package com.climatesi.ghg.implGeneral.facade;

import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;
import com.climatesi.ghg.api.facade.PublicTransportEmissionFactorsManager;
import com.climatesi.ghg.persistence.PublicTransportEmissionFactorsDAO;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class PublicTransportEmissionFactorsFacade implements PublicTransportEmissionFactorsManager {

    private static Logger logger = Logger.getLogger(PublicTransportEmissionFactorsFacade.class);
    private PublicTransportEmissionFactorsDAO dao;

    @Override
    public Object addEntity(PublicTransposrtEmisssionFactor entity) {
        return null;
    }

    @Override
    public Object addEntity(PublicTransposrtEmisssionFactor entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(PublicTransposrtEmisssionFactor entity) {
        return null;
    }

    @Override
    public Object updateEntity(PublicTransposrtEmisssionFactor entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(PublicTransposrtEmisssionFactor entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(PublicTransposrtEmisssionFactor entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(PublicTransposrtEmisssionFactor entity) {
        return null;
    }

    @Override
    public PublicTransposrtEmisssionFactor getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<PublicTransposrtEmisssionFactor> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<PublicTransposrtEmisssionFactor> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public PublicTransposrtEmisssionFactor getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<PublicTransposrtEmisssionFactor> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<PublicTransposrtEmisssionFactor> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new PublicTransportEmissionFactorsDAO(entityManager);
    }
}
