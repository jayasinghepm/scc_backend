package com.climatesi.ghg.implGeneral.data;

public class DiselGeneratorCalcData {

    private float consumption;
    private float density;
    private float netCaloricValue;
    private float ef_CO2_s_n;
    private float ef_CH4_s_n;
    private float ef_N20_s_n;
    private float gwp_CO2;
    private float gwp_CH4;
    private float gwp_N2O;

    public float getConsumption() {
        return consumption;
    }

    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public float getNetCaloricValue() {
        return netCaloricValue;
    }

    public void setNetCaloricValue(float netCaloricValue) {
        this.netCaloricValue = netCaloricValue;
    }

    public float getEf_CO2_s_n() {
        return ef_CO2_s_n;
    }

    public void setEf_CO2_s_n(float ef_CO2_s_n) {
        this.ef_CO2_s_n = ef_CO2_s_n;
    }

    public float getEf_CH4_s_n() {
        return ef_CH4_s_n;
    }

    public void setEf_CH4_s_n(float ef_CH4_s_n) {
        this.ef_CH4_s_n = ef_CH4_s_n;
    }

    public float getEf_N20_s_n() {
        return ef_N20_s_n;
    }

    public void setEf_N20_s_n(float ef_N20_s_n) {
        this.ef_N20_s_n = ef_N20_s_n;
    }

    public float getGwp_CO2() {
        return gwp_CO2;
    }

    public void setGwp_CO2(float gwp_CO2) {
        this.gwp_CO2 = gwp_CO2;
    }

    public float getGwp_CH4() {
        return gwp_CH4;
    }

    public void setGwp_CH4(float gwp_CH4) {
        this.gwp_CH4 = gwp_CH4;
    }

    public float getGwp_N2O() {
        return gwp_N2O;
    }

    public void setGwp_N2O(float gwp_N2O) {
        this.gwp_N2O = gwp_N2O;
    }
}
