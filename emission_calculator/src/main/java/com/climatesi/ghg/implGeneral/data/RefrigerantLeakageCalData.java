package com.climatesi.ghg.implGeneral.data;

public class RefrigerantLeakageCalData {

    private float gfp;
    private float weight; // tons

    public float getGfp() {
        return gfp;
    }

    public void setGfp(float gfp) {
        this.gfp = gfp;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
