package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.EmissionFactors;

import javax.persistence.*;

@Entity
@Table(name = "CONST_EMISSION_FACTORS")
@NamedQueries({
        @NamedQuery(name = "EmissionFactorsStoreBean.findById",
                query = "SELECT i FROM EmissionFactorsStoreBean i WHERE i.id = :id"),
})
public class EmissionFactorsStoreBean implements EmissionFactors {

    @Id
    @Column(name = "ID")
    private int id;

    private String description;

    private float value;

    private String reference;


    private int isDeleted;

    private int unitId;

    private String unitStr;

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    @Override
    public String getUnitStr() {
        return unitStr;
    }

    public void setUnitStr(String unitStr) {
        this.unitStr = unitStr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public int isDeleted() {
        return this.isDeleted;
    }

    @Override
    public void setDeleted(int deleted) {
        this.isDeleted = deleted;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
