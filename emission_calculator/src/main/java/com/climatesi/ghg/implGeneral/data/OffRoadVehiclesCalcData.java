package com.climatesi.ghg.implGeneral.data;

public class OffRoadVehiclesCalcData {

    private float consumption_liters;
    private float density;
    private float net_caloric_value;
    private float ef_co2;
    private float ef_ch4;
    private float ef_n2o;
    private float gwp_co2;
    private float gwp_ch4;
    private float gwp_n2o;

    public float getConsumption_liters() {
        return consumption_liters;
    }

    public void setConsumption_liters(float consumption_liters) {
        this.consumption_liters = consumption_liters;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public float getNet_caloric_value() {
        return net_caloric_value;
    }

    public void setNet_caloric_value(float net_caloric_value) {
        this.net_caloric_value = net_caloric_value;
    }

    public float getEf_co2() {
        return ef_co2;
    }

    public void setEf_co2(float ef_co2) {
        this.ef_co2 = ef_co2;
    }

    public float getEf_ch4() {
        return ef_ch4;
    }

    public void setEf_ch4(float ef_ch4) {
        this.ef_ch4 = ef_ch4;
    }

    public float getEf_n2o() {
        return ef_n2o;
    }

    public void setEf_n2o(float ef_n2o) {
        this.ef_n2o = ef_n2o;
    }

    public float getGwp_co2() {
        return gwp_co2;
    }

    public void setGwp_co2(float gwp_co2) {
        this.gwp_co2 = gwp_co2;
    }

    public float getGwp_ch4() {
        return gwp_ch4;
    }

    public void setGwp_ch4(float gwp_ch4) {
        this.gwp_ch4 = gwp_ch4;
    }

    public float getGwp_n2o() {
        return gwp_n2o;
    }

    public void setGwp_n2o(float gwp_n2o) {
        this.gwp_n2o = gwp_n2o;
    }
}
