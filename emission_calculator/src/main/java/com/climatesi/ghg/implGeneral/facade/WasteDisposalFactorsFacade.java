package com.climatesi.ghg.implGeneral.facade;

import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.api.facade.WasteDisposalFactorsManager;
import com.climatesi.ghg.persistence.WasteDisposalFactorsDAO;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class WasteDisposalFactorsFacade implements WasteDisposalFactorsManager {

    private static Logger logger = Logger.getLogger(WasteDisposalFactorsFacade.class);
    private WasteDisposalFactorsDAO dao;

    @Override
    public Object addEntity(WasteDisposalFactors entity) {
        return null;
    }

    @Override
    public Object addEntity(WasteDisposalFactors entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(WasteDisposalFactors entity) {
        return null;
    }

    @Override
    public Object updateEntity(WasteDisposalFactors entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(WasteDisposalFactors entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(WasteDisposalFactors entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(WasteDisposalFactors entity) {
        return null;
    }

    @Override
    public WasteDisposalFactors getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<WasteDisposalFactors> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<WasteDisposalFactors> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public WasteDisposalFactors getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<WasteDisposalFactors> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<WasteDisposalFactors> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return this.dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new WasteDisposalFactorsDAO(entityManager);
    }
}
