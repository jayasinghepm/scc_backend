package com.climatesi.ghg.implGeneral.facade;

import com.climatesi.ghg.api.beans.IncinerationFactor;
import com.climatesi.ghg.api.facade.IncinerationFactorsManager;
import com.climatesi.ghg.implGeneral.EmissionFactorsStoreBean;
import com.climatesi.ghg.implGeneral.IncinerationFactorsBean;
import com.climatesi.ghg.persistence.EmissionFactorsDAO;
import com.climatesi.ghg.persistence.IncinerationFactorsDAO;
import com.climatesi.ghg.utility.api.constants.ApplicationConstants;
import com.climatesi.ghg.utility.api.data.ListResult;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class IncinerationFactorsFacade implements IncinerationFactorsManager {

    private static Logger logger = Logger.getLogger(IncinerationFactorsFacade.class);
    private IncinerationFactorsDAO dao;

    @Override
    public Object addEntity(IncinerationFactor entity) {
        try {
            return dao.insert((IncinerationFactorsBean) entity);
        } catch (GHGException e) {
            logger.error("Error in adding EmissionFactorsStoreBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object addEntity(IncinerationFactor entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(IncinerationFactor entity) {
        try {
            return dao.update((IncinerationFactorsBean) entity);
        } catch (GHGException e) {
            logger.error("Error in updating IncinerationFactorsBean ", e);
            return ApplicationConstants.DEFAULT_FAILED_CODE_FOR_ADD_EDIT;
        }
    }

    @Override
    public Object updateEntity(IncinerationFactor entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(IncinerationFactor entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(IncinerationFactor entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(IncinerationFactor entity) {
        return null;
    }

    @Override
    public IncinerationFactor getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<IncinerationFactor> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<IncinerationFactor> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public IncinerationFactor getEntityByKey(Integer id) {
        return this.dao.findByKey(id);
    }

    @Override
    public ListResult<IncinerationFactor> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<IncinerationFactor> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new IncinerationFactorsDAO(entityManager);
    }
}
