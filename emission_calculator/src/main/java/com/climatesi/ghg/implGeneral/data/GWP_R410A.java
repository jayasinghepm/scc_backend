package com.climatesi.ghg.implGeneral.data;

public class GWP_R410A {

    private float weight;
    private float ch2f2_percent;
    private float ch2f2_coff;
    private float chf2cf3_percent;
    private float chf2cf3_coff;

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getCh2f2_percent() {
        return ch2f2_percent;
    }

    public void setCh2f2_percent(float ch2f2_percent) {
        this.ch2f2_percent = ch2f2_percent;
    }

    public float getCh2f2_coff() {
        return ch2f2_coff;
    }

    public void setCh2f2_coff(float ch2f2_coff) {
        this.ch2f2_coff = ch2f2_coff;
    }

    public float getChf2cf3_percent() {
        return chf2cf3_percent;
    }

    public void setChf2cf3_percent(float chf2cf3_percent) {
        this.chf2cf3_percent = chf2cf3_percent;
    }

    public float getChf2cf3_coff() {
        return chf2cf3_coff;
    }

    public void setChf2cf3_coff(float chf2cf3_coff) {
        this.chf2cf3_coff = chf2cf3_coff;
    }
}
