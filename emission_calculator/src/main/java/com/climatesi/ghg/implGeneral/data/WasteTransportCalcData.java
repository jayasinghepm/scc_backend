package com.climatesi.ghg.implGeneral.data;

public class WasteTransportCalcData {


    private float waste_tonnes;
    private float loading_capacity_tonnes;
    private float up_down_distance_km;
    private float fuel_economy_km_l;
    private float density_fuel_t_m3;
    private float net_caloric_fuel;
    private float ef_co2;
    private float ef_ch4;
    private float ef_n2o;
    private float gwp_co2;
    private float gwp_ch4;
    private float gwp_n2o;
    private float no_of_turns;//new


    //noOfTurns
    //----edit by pasindu (missing noOfTurrns)-----
    public float getNo_of_turns() {
        return no_of_turns;
    }

    public void setNo_of_turns(float no_of_turns) {
        this.no_of_turns = no_of_turns;
    }
    //----edit by pasindu-----


    public float getWaste_tonnes() {
        return waste_tonnes;
    }

    public void setWaste_tonnes(float waste_tonnes) {
        this.waste_tonnes = waste_tonnes;
    }

    public float getLoading_capacity_tonnes() {
        return loading_capacity_tonnes;
    }

    public void setLoading_capacity_tonnes(float loading_capacity_tonnes) {
        this.loading_capacity_tonnes = loading_capacity_tonnes;
    }

    public float getUp_down_distance_km() {
        return up_down_distance_km;
    }

    public void setUp_down_distance_km(float up_down_distance_km) {
        this.up_down_distance_km = up_down_distance_km;
    }

    public float getFuel_economy_km_l() {
        return fuel_economy_km_l;
    }

    public void setFuel_economy_km_l(float fuel_economy_km_l) {
        this.fuel_economy_km_l = fuel_economy_km_l;
    }

    public float getDensity_fuel_t_m3() {
        return density_fuel_t_m3;
    }

    public void setDensity_fuel_t_m3(float density_fuel_t_m3) {
        this.density_fuel_t_m3 = density_fuel_t_m3;
    }

    public float getNet_caloric_fuel() {
        return net_caloric_fuel;
    }

    public void setNet_caloric_fuel(float net_caloric_fuel) {
        this.net_caloric_fuel = net_caloric_fuel;
    }

    public float getEf_co2() {
        return ef_co2;
    }

    public void setEf_co2(float ef_co2) {
        this.ef_co2 = ef_co2;
    }

    public float getEf_ch4() {
        return ef_ch4;
    }

    public void setEf_ch4(float ef_ch4) {
        this.ef_ch4 = ef_ch4;
    }

    public float getEf_n2o() {
        return ef_n2o;
    }

    public void setEf_n2o(float ef_n2o) {
        this.ef_n2o = ef_n2o;
    }

    public float getGwp_co2() {
        return gwp_co2;
    }

    public void setGwp_co2(float gwp_co2) {
        this.gwp_co2 = gwp_co2;
    }

    public float getGwp_ch4() {
        return gwp_ch4;
    }

    public void setGwp_ch4(float gwp_ch4) {
        this.gwp_ch4 = gwp_ch4;
    }

    public float getGwp_n2o() {
        return gwp_n2o;
    }

    public void setGwp_n2o(float gwp_n2o) {
        this.gwp_n2o = gwp_n2o;
    }
}
