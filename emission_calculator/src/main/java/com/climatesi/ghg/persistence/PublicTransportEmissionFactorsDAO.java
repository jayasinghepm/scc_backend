package com.climatesi.ghg.persistence;

import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;
import com.climatesi.ghg.implGeneral.PublicTransportEmissionFactorsBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class PublicTransportEmissionFactorsDAO extends AbstractDAO<PublicTransportEmissionFactorsBean, PublicTransposrtEmisssionFactor> {

    private static Logger logger = Logger.getLogger(PublicTransportEmissionFactorsDAO.class);
    private static EntityManager em;

    public PublicTransportEmissionFactorsDAO(EntityManager em) {
        super(em, PublicTransportEmissionFactorsBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("CONST_PUBLIC_TRANSPORT_FACTORS", "VEHICLE_TYPE", "PublicTransportEmissionFactorsBean");
        meta.setKeyPropertyName("vehicleType");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }


    @Override
    public Map<String, Object> getAddSpParams(PublicTransportEmissionFactorsBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(PublicTransportEmissionFactorsBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(PublicTransportEmissionFactorsBean entityImpl) throws GHGException {
        return null;
    }
}
