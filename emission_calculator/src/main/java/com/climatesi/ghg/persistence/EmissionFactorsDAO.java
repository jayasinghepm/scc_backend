package com.climatesi.ghg.persistence;

import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.implGeneral.EmissionFactorsStoreBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class EmissionFactorsDAO extends AbstractDAO<EmissionFactorsStoreBean, EmissionFactors> {
    private static Logger logger = Logger.getLogger(EmissionFactorsDAO.class);
    private static EntityManager em;


    public EmissionFactorsDAO(EntityManager em) {
        super(em, EmissionFactorsStoreBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("CONST_EMISSION_FACTORS", "ID", "EmissionFactorsStoreBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(EmissionFactorsStoreBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(EmissionFactorsStoreBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(EmissionFactorsStoreBean entityImpl) throws GHGException {
        return null;
    }
}
