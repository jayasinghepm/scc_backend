package com.climatesi.ghg.persistence;

import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.api.beans.IncinerationFactor;
import com.climatesi.ghg.implGeneral.EmissionFactorsStoreBean;
import com.climatesi.ghg.implGeneral.IncinerationFactorsBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;

import javax.persistence.EntityManager;
import java.util.Map;

public class IncinerationFactorsDAO  extends AbstractDAO<IncinerationFactorsBean, IncinerationFactor> {
    private static EntityManager em;

    public IncinerationFactorsDAO(EntityManager em) {
        super(em, IncinerationFactorsBean.class);
        DataEntryInfo meta = new DataEntryInfo("CONST_INCINERATION_FACTORS", "ID", "IncinerationFactorsBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(IncinerationFactorsBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(IncinerationFactorsBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(IncinerationFactorsBean entityImpl) throws GHGException {
        return null;
    }
}
