package com.climatesi.ghg.persistence;

import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.implGeneral.WasteDisposalFactorsBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class WasteDisposalFactorsDAO extends AbstractDAO<WasteDisposalFactorsBean, WasteDisposalFactors> {
    private static Logger logger = Logger.getLogger(WasteDisposalFactorsDAO.class);
    private static EntityManager em;

    public WasteDisposalFactorsDAO(EntityManager em) {
        super(em, WasteDisposalFactorsBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("CONST_WASTE_DISPOSAL_FACTORS", "WASTE_TYPE", "WasteDisposalFactorsBean");
        meta.setKeyPropertyName("wasteType");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(WasteDisposalFactorsBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(WasteDisposalFactorsBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(WasteDisposalFactorsBean entityImpl) throws GHGException {
        return null;
    }
}
