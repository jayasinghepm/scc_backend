package com.climatesi.ghg.api.beans;

public interface WasteDisposalFactors {

    public int getWasteType();

    public void setWasteType(int wasteType);

    public String getWasteName();

    public void setWasteName(String wasteName);

    public String getUnits();

    public void setUnits(String units);

    public float getReuse();

    public void setReuse(float reuse);

    public float getOpen_loop();

    public void setOpen_loop(float open_loop);

    public float getClosed_loop();

    public void setClosed_loop(float closed_loop);

    public float getCombustion();

    public void setCombustion(float combustion);

    public float getComposting();

    public void setComposting(float composting);

    public float getLandfill();

    public void setLandfill(float landfill);

    public float getAnaerobic_digestion();

    public int isDeleted();

    public void setDeleted(int deleted);

    public void setAnaerobic_digestion(float anaerobic_digestion);
}
