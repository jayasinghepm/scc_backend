package com.climatesi.ghg.api.constants;

public class EmissionFactors_Keys {

    public final static int DENSITY_FUEL_DIESEL = 1;
    public final static int DENSITY_FUEL_PETROL = 2;
    public final static int NET_CALORIC_DIESEL = 3;
    public final static int NET_CALORIC_PETROL = 4;
    public final static int EF_CO2_STATIONARY_DIESEL = 5;
    public final static int EF_CH4_STATIONARY_DIESEL = 6;
    public final static int EF_N2O_STATIONARY_DIESEL = 7;
    public final static int GWP_CO2_STATIONARY_DIESEL = 8;
    public final static int GWP_CH4_STATIONARY_DIESEL = 9;
    public final static int GWP_N20_STATIONARY_DIESEL = 10;
    public final static int GWP_R22 = 11;
    public final static int GWP_R407C_W_PERCENT_CH2F2 = 12;
    public final static int GWP_R407C_W_PERCENT_CF3CHF2 = 13;
    public final static int GWP_R407C_W_PERCENT_CF3CH2F = 14;
    public final static int GWP_R407C_W_COFF_CF3CHF2 = 15;
    public final static int GWP_R407C_W_COFF_CH2F2 = 16;
    public final static int GWP_R407C_W_COFF_CF3CH2F = 17;

    public final static int GWP_R410A_W_PERCENT_CH2F2 = 18;
    public final static int GWP_R410A_W_PERCENT_CHF2CF3 = 19;
    public final static int GWP_R410A_W_COFF_CH2F2 = 20;
    public final static int GWP_R410A_W_COFF_CHF2CF3 = 21;

    public final static int EF_CO2_MOBILE_GASOLINE = 22;
    public final static int EF_CO2_MOBILE_DIESEL = 23;
    public final static int EF_CH4_MOBILE_GASOLINE = 24;
    public final static int EF_CH4_MOBILE_DIESEL = 25;
    public final static int EF_N20_MOBILE_GASOLINE = 26;
    public final static int EF_N20_MOBILE_DIESEL = 27;

    public final static int EF_CO2_OFF_ROAD_GASOLINE = 28;
    public final static int EF_CO2_OFF_ROAD_DIESEL = 29;
    public final static int EF_CH4_OFF_ROAD_GASOLINE = 30;
    public final static int EF_CH4_OFF_ROAD_DIESEL = 31;
    public final static int EF_N20_OFF_ROAD_GASOLINE = 32;
    public final static int EF_N20_OFF_ROAD_DIESEL = 33;

    public final static int NATIONAL_GRID_EMISSION_FACTOR = 34;
    public final static int T_AND_D_LOSS_PERCENT = 35;
    public final static int CF_MUNICIPAL_WATER = 36;

    public final static int AVG_FEED_RATE_PER_PIG = 37;

    public final static int PRICE_DIESEL_LITER = 38; //financial LAD
    public final static int PRICE_PETROL_LITER = 39; //financial LP95
    //new factors
    public final static int PRICE_LSD = 74; //financial LSD
    public final static int PRICE_LP92= 75; // financial LP92


    public final static int PRICE_DIESEL_LITER_CALENDER = 40;// cal LAD
    public final static int PRICE_PETROL_LITER_CALENDER = 41;//cal LP95
    public final static int PRICE_LSD_CALENDER = 76;//cal LSD
    public final static int PRICE_LP92_CALENDER = 77;//cal LP92

    //newnew
    public final static int PRICE_LP95_P = 78; //financial LP95_P
    public final static int PRICE_LP92_P = 79; //financial LP92_P
    public final static int PRICE_LAD_P = 80;// financial  LAD_P
    public final static int PRICE_LSD_P = 81;// financial  LSD_P

    public final static int PRICE_LP95_P_CALENDER = 82; //cal LP95_P
    public final static int PRICE_LP92_P_CALENDER = 83; //cal LP92_P
    public final static int PRICE_LAD_P_CALENDER = 84;//cal LAD_P
    public final static int PRICE_LSD_P_CALENDER = 85;//cal LSD_P

    public final static int PRICE_LP95_PP = 86; //financial LP95_PP
    public final static int PRICE_LP92_PP = 87; //financial LP92_PP
    public final static int PRICE_LAD_PP = 88;// financial LAD_PP
    public final static int PRICE_LSD_PP = 89;// financial LSD_PP

    public final static int PRICE_LP95_PP_CALENDER = 90; //cal LP95_PP
    public final static int PRICE_LP92_PP_CALENDER = 91; //cal LP92_PP
    public final static int PRICE_LAD_PP_CALENDER = 92;//cal LAD_PP
    public final static int PRICE_LSD_PP_CALENDER = 93;//cal LSD_PP  //2019/20










    //    'Finance', 'Telecommmunication', 'Apparel', 'Hospitality', 'Plantation',
//            'Transport', 'Food and Beverage', 'Manufacturing', 'Other'
    public final static int EM_INTENSITY_FINANCE = 42;
    public final static int EM_INTENSITY_TEL_COMM = 43;
    public final static int EM_INTENSITY_APPAREL = 44;
    public final static int EM_INTENSITY_HOSPITALITY = 45;
    public final static int EM_INTENSITY_PLANTATION = 46;
    public final static int EM_INTENSITY_TRANSPORT = 47;
    public final static int EM_INTENSITY_FOOD_AND_BEVERAGE = 48;
    public final static int EM_INTENSITY_MANUFACTURING = 49;
    public final static int EM_INTENSITY_OTHER = 50;

    public final static int CF_NAUTIC_KM = 51;
    public final static int CO2_EF_AIR_FREIGHT_RANGE1 = 52;
    public final static int CO2_EF_AIR_FREIGHT_RANGE2 = 53;
    public final static int CO2_EF_AIR_FREIGHT_RANGE3 = 54;

    public final static int CO2_EF_SEA_FREIGHT = 55;

    public final static int BIOMASS_NCV = 56;
    public final static int BIOMASS_EF_CH4 = 57;
    public final static int BIOMASS_EF_CO2 = 58;
    public final static int BIOMASS_EF_N20 = 59;

    public final static int LPGAS_NCV = 60;
    public final static int LPGAS_EF_CH4 = 61;
    public final static int LPGAS_EF_CO2 = 62;
    public final static int LPGAS_EF_N20 = 63;

    public final static int PAPER_WASTE_EF_CO2 = 64;

    public final static int FURNACE_OIL_DENSITY = 65;
    public final static int FURNACE_OIL_NCV = 66;
    public final static int FURNACE_OIL_EF_CO2 = 67;
    public final static int FURNACE_OIL_EF_CH4 = 68;
    public final static int FURNACE_OIL_EF_N20 = 69;

    public final static int OTHER_BIOMASS_NCV = 70;
    public final static int  OTHER_BIOMASS_EF_CH4 = 71;
    public final static int  OTHER_BIOMASS_EF_CO2 = 72;
    public final static int  OTHER_BIOMASS_EF_N20 = 73;










}
