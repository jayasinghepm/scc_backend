package com.climatesi.ghg.api.beans;

public interface EmissionFactors {


    public int getId();

    public void setId(int id);

    public String getDescription();

    public void setDescription(String description);

    public float getValue();

    public void setValue(float value);

    public String getReference();

    public void setReference(String reference);

    public int isDeleted();

    public void setDeleted(int deleted);

    public int getUnitId() ;

    public void setUnitId(int unitId);

    public String getUnitStr();

    public void setUnitStr(String unitStr);

}
