package com.climatesi.ghg.api.facade;

import com.climatesi.ghg.api.beans.WasteDisposalFactors;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface WasteDisposalFactorsManager extends DataEntryManager<WasteDisposalFactors, Integer> {
}
