package com.climatesi.ghg.api.constants;

public class PublicTransport_Keys {

    public static final int VAN_DIESEL = 1;
    public static final int MEDIUM_BUS_DIESEL = 2;
    public static final int BUS_DIESEL = 3;
    public static final int TRAIN = 4;
}
