package com.climatesi.ghg.api;

import com.climatesi.ghg.api.facade.EmissionFactorsManager;
import com.climatesi.ghg.api.facade.IncinerationFactorsManager;
import com.climatesi.ghg.api.facade.PublicTransportEmissionFactorsManager;
import com.climatesi.ghg.api.facade.WasteDisposalFactorsManager;
import com.climatesi.ghg.implGeneral.facade.EmissionFactorsFacade;
import com.climatesi.ghg.implGeneral.facade.IncinerationFactorsFacade;
import com.climatesi.ghg.implGeneral.facade.PublicTransportEmissionFactorsFacade;
import com.climatesi.ghg.implGeneral.facade.WasteDisposalFactorsFacade;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class EmissionCalcFacotory {

    public static EmissionCalcFacotory instance;
    private static Logger logger = Logger.getLogger(EmissionCalcFacotory.class);
    private static EmissionFactorsManager emissionFactorsManager;
    private static PublicTransportEmissionFactorsManager publicTransportEmissionFactorsManager;
    private static WasteDisposalFactorsManager wasteDisposalFactorsManager;
    private static IncinerationFactorsManager incinerationFactorsManager;

    private EmissionCalcFacotory() {

    }

    public static EmissionCalcFacotory getInstance() {
        if (instance == null) {
            instance = new EmissionCalcFacotory();
        }
        return instance;
    }


    public EmissionFactorsManager getEmissionFactorsManager(EntityManager em) {
        if (emissionFactorsManager == null) {
            emissionFactorsManager = new EmissionFactorsFacade();
            emissionFactorsManager.injectEntityManager(em);
        }
        return emissionFactorsManager;
    }

    public PublicTransportEmissionFactorsManager getPublicTransportEmissionFactorsManager(EntityManager em) {
        if (publicTransportEmissionFactorsManager == null) {
            publicTransportEmissionFactorsManager = new PublicTransportEmissionFactorsFacade();
            publicTransportEmissionFactorsManager.injectEntityManager(em);
        }
        return publicTransportEmissionFactorsManager;
    }

    public WasteDisposalFactorsManager getWasteDisposalFactorsManager(EntityManager em) {
        if (wasteDisposalFactorsManager == null) {
            wasteDisposalFactorsManager = new WasteDisposalFactorsFacade();
            wasteDisposalFactorsManager.injectEntityManager(em);
        }
        return wasteDisposalFactorsManager;
    }

    public IncinerationFactorsManager getIncinerationFactorsManager(EntityManager em) {
        if(incinerationFactorsManager == null) {
            incinerationFactorsManager = new IncinerationFactorsFacade();
            incinerationFactorsManager.injectEntityManager(em);
        }
        return incinerationFactorsManager;
    }

}

