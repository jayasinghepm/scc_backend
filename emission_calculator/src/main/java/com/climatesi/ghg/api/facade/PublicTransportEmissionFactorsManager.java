package com.climatesi.ghg.api.facade;

import com.climatesi.ghg.api.beans.PublicTransposrtEmisssionFactor;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface PublicTransportEmissionFactorsManager extends DataEntryManager<PublicTransposrtEmisssionFactor, Integer> {
}
