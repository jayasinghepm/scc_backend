package com.climatesi.ghg.api.beans;

public interface PublicTransposrtEmisssionFactor {

    public int getVehicleType();

    public void setVehicleType(int vehicleType);

    public String getVehicleName();

    public void setVehicleName(String vehicleName);

    public float getG_per_km();

    public void setG_per_km(float g_per_km);

    public float getCo2_per_km();

    public void setCo2_per_km(float co2_per_km);

    public String getAssumption();

    public void setAssumption(String assumption);

    public int isDeleted();

    public void setDeleted(int deleted);
}
