package com.climatesi.ghg.api.beans;

public interface IncinerationFactor {

    public int getId();

    public void setId(int id);

    public float getDry_matter_per() ;

    public void setDry_matter_per(float dry_matter_per);
    public float getCarbon_fraction();

    public void setCarbon_fraction(float carbon_fraction);

    public float getFossil_carbon_fraction();
    public void setFossil_carbon_fraction(float fossil_carbon_fraction);
}
