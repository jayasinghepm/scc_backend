package com.climatesi.ghg.api.facade;

import com.climatesi.ghg.api.beans.EmissionFactors;
import com.climatesi.ghg.api.beans.IncinerationFactor;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface IncinerationFactorsManager extends DataEntryManager<IncinerationFactor, Integer> {
}
