package com.climatesi.ghg.api.constants;

public class WasteType_Keys {

    public static final int AVG_CONSTRUCTION = 1;
    public static final int SOILS = 2;
    public static final int TYRES = 3;
    public static final int WOOD = 4;
    public static final int BOOKS = 5;
    public static final int GLASS = 6;
    public static final int CLOTHING = 7;
    public static final int MUNICIPAL_WASTE = 8;
    public static final int ORGANIC_FOOD_DRINK_WASTE = 9;
    public static final int ORGANIC_GARDEN_WASTE = 10;
    public static final int ORGANIC_MIXED_FOOD_GARDEN_WASTE = 11;
    public static final int COMMERCIAL_INDUSTRIAL_WASTE = 12;
    public static final int WEEE = 13;
    public static final int WEEE_FRIDGES_FREEZERS = 14;
    public static final int BATTERIES = 15;
    public static final int METAL = 16;
    public static final int PLASTICS = 17;
    public static final int PAPER_BOARD = 18;


}
