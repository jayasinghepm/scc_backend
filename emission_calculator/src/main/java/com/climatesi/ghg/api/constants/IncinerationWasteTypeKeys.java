package com.climatesi.ghg.api.constants;

public class IncinerationWasteTypeKeys {

    public static final int PAPER_CARDBOARD = 19;
    public static final int TEXTILES = 20;
    public static final int FOOD_WASTE = 21;
    public static final int WOOD = 22;
    public static final int GARDEN_AND_PARK_WASTE= 23;
    public static final int NAPPIES = 24;
    public static final int RUBBER_LEATHER = 25;
    public static final int PLASTICS = 26;
    public static final int OTHER_INERT_WASTE= 27;
    public static final int DOMESTIC = 28;
    public static final int INDUSTRIAL = 29;


}
