package com.climatesi.ghg.calc;

import com.climatesi.ghg.implGeneral.data.*;

public class Calculator {

    public static final float CO2_EF_SEA_FREIGHT = 0.013232f;

    public static final float CO2_EF_AIR_FRIEGHT_RANGE1 = 0.9f; //<1000

    public static final float CO2_EF_AIR_FRIEGHT_RANGE2 = 0.57f ;  // 1000<D<4000

    public static final float CO2_EF_AIR_FRIEGHT_RANGE3 = 0.32f; //>4000

    public static final float CF_NAUTICMILES_TO_KM =1.852f;


    public static ResultDTO emissionLPG(float amountKg, float efCo2, float efCh4, float efN2o, float ncv) {
        float totalEnergy = amountKg * ncv;
        float eCo2 = totalEnergy * efCo2;
        float eCh4 = totalEnergy * efCh4;
        float eN2o = totalEnergy * efN2o;
        return new ResultDTO(
                eCo2 + eCh4 + eN2o,
                eCo2,
                eCh4,
                eN2o
        );
    }
    public static ResultDTO emissionBGAS(float amountKg, float efCo2, float efCh4, float efN2o, float ncv,float gwp_co2,float gwp_ch4,float gwp_n20) {
        float totalEnergy = amountKg * ncv;
        float eCo2 = totalEnergy * efCo2 * gwp_co2 ;
        float eCh4 = totalEnergy * efCh4 * gwp_ch4 ;
        float eN2o = totalEnergy * efN2o * gwp_n20 ;
        return new ResultDTO(
                eCo2 + eCh4 + eN2o,
                eCo2,
                eCh4,
                eN2o
        );
    }


    public static ResultDTO emissionPaperWaste(float tons, float ef) {
        float emission = tons * ef;
        return new ResultDTO(emission, emission, 0, 0);
    }

    public static ResultDTO emissionVehicles(
            float consumptionLiters,
            float ncv,
            float density,
            float efCo2 ,
            float efCh4,
            float efN2o,
            float gwpCo2,
            float gwpCh4,
            float gwpN20


    ) {
        //m3 * t/m3 * TJ/t = TJ
        float totalEnergy = consumptionLiters * ncv * density * 0.001f;

        float eCo2 = totalEnergy * efCo2 * gwpCo2 * 0.001f;
        float eCh4 = totalEnergy * efCh4 * gwpCh4 * 0.001f;
        float eN2o = totalEnergy * efN2o * gwpN20 * 0.001f;
      ResultDTO result =  new ResultDTO(
                eCo2 + eCh4 + eN2o,
                eCo2,
                eCh4,
                eN2o
        );

      result.setTotalEnergy(totalEnergy);
      return result
              ;
    }




    public static ResultDTO emissionBioMass(
            float quantity,
            float ncv,
            float efCo2 ,
            float efCh4,
            float efN2o,
            float gwpCo2,
            float gwpCh4,
            float gwpN20) {
        float totalEnergy = quantity * ncv;
        float eCo2 = totalEnergy * efCo2 * gwpCo2 * 0.001f;
        float eCh4 = totalEnergy * efCh4 * gwpCh4 * 0.001f;
        float eN2o = totalEnergy * efN2o * gwpN20 * 0.001f;
        ResultDTO result = new ResultDTO(
                eCo2 + eCh4 + eN2o,
                eCo2,
                eCh4,
                eN2o
        );
        result.setTotalEnergy(totalEnergy);
        return result;
    }

    public static ResultDTO emissionAirFreight(float amount, float nauticDistance,
                                               float cfNauticMilesToKm,
                                               float efCO2Range1,
                                               float efCO2range2,
                                               float efCO2range3

    ){
        float distance = nauticDistance * cfNauticMilesToKm;
        if (distance < 1000) {
            ResultDTO resultDTO =  new ResultDTO(
                    (distance * amount * efCO2Range1)/1000,
                    (distance * amount * efCO2Range1)/1000, 0, 0);
            resultDTO.ef = efCO2Range1;
           return resultDTO;
        }else if (nauticDistance * cfNauticMilesToKm >= 1000 && nauticDistance *cfNauticMilesToKm <= 4000) {
           ResultDTO resultDTO =  new ResultDTO(
                    (distance * amount *  efCO2range2)/1000,
                    (distance * amount * efCO2range2)/1000, 0, 0);
            resultDTO.ef = efCO2range2;
            return resultDTO;
        }
        else {
            ResultDTO resultDTO = new ResultDTO(
                    (distance * amount * efCO2range3)/1000,
                    (distance * amount * efCO2range3)/1000, 0, 0);

            resultDTO.ef = efCO2range3;
            return resultDTO;
        }
    }

    public static ResultDTO emissionSeaFreight(float amount, float nauticDistance,  float cfNauticMilesToKm,
                                               float efCO2) {
        ResultDTO dto =  new ResultDTO(
                (amount * nauticDistance * cfNauticMilesToKm * efCO2)/1000
                , 0, 0, 0);
        dto.ef = efCO2;
        return dto;
    }


    public static ResultDTO emissionGenerators(DiselGeneratorCalcData data) {
        float te = (data.getConsumption() / 1000) * data.getDensity() * data.getNetCaloricValue();
        float e_1 = ((data.getEf_CO2_s_n() * data.getGwp_CO2()) / 1000) * te;
        float e_2 = ((data.getEf_CH4_s_n() * data.getGwp_CH4()) / 1000) * te;
        float e_3 = ((data.getEf_N20_s_n() * data.getGwp_N2O()) / 1000) * te;
        return new ResultDTO(e_1 + e_2 + e_3,  e_1, e_2, e_3);
    }

    public static float emissionRefrigernatLeakage(RefrigerantLeakageCalData data) {
        return data.getGfp() * data.getWeight();
    }

    public static ResultDTO emissionEmpCommFuelPaidByCompany(EmpCommutPaidByCompanyData data) {
        float te = (data.getConsumption_liters() / 1000) * data.getDensity() * data.getNet_caloric_value();
        float emc_1 = ((data.getEf_co2() * data.getGwp_co2()) / 1000) * te;
        float emc_2 = ((data.getEf_ch4() * data.getGwp_ch4()) / 1000) * te;
        float emc_3 = ((data.getEf_n2o() * data.getGwp_n2o()) / 1000) * te;
        return new ResultDTO(emc_1 + emc_2 + emc_3, emc_1, emc_2, emc_3);
    }

    public static ResultDTO emissionFireExtRefilling(FireExtCalData data) {
        return new ResultDTO(data.getWeight_per_tank() * data.getNumber_of_tanks(), 0f, 0f, 0f);
    }

    public static ResultDTO emissionCompanyOwnedVehicle(CompanyOwnedVehicleCalcData data) {
        float te = (data.getConsumption_liters() / 1000) * data.getDensity() * data.getNet_caloric_value();
        float emc_1 = ((data.getEf_co2() * data.getGwp_co2()) / 1000) * te;
        float emc_2 = ((data.getEf_ch4() * data.getGwp_ch4()) / 1000) * te;
        float emc_3 = ((data.getEf_n2o() * data.getGwp_n2o()) / 1000) * te;
        return new ResultDTO(emc_1 + emc_2 + emc_3, emc_1, emc_2, emc_3);////result  is OK - tco2e -
    }

    public static ResultDTO emissionOffroadVehicles(OffRoadVehiclesCalcData data) {
        float te = (data.getConsumption_liters() / 1000) * data.getDensity() * data.getNet_caloric_value();
        float emc_1 = ((data.getEf_co2() * data.getGwp_co2()) / 1000) * te;
        float emc_2 = ((data.getEf_ch4() * data.getGwp_ch4()) / 1000) * te;
        float emc_3 = ((data.getEf_n2o() * data.getGwp_n2o()) / 1000) * te;
        return new ResultDTO(emc_1 + emc_2 + emc_3, emc_1, emc_2, emc_3);
    }

    public static ResultDTO emissionGridElectricity(GridElectricityData data) {
        return new ResultDTO((data.getConsumption() / 1000) * data.getEf_grid_electricity(), 0f, 0f, 0f);
    }

    public static ResultDTO emissionTandDLoss(TandDLossCalcData data) {
        return new ResultDTO((((data.getT_d_percent()/100) * data.getConsumption()) / (1 - (data.getT_d_percent()/100))) * data.getEf_grid_electricity(), 0f, 0f, 0f);
    }

    public static ResultDTO emissionMunicipalWater(MunicipalWaterCalcData data) {
        return new ResultDTO(((data.getConsumption_m3() * data.getCf_municipal_water()) / 1000) * data.getEf_grid_electricty(), 0f, 0f, 0f);

    }

    public static float emissionBusinessAirtravelEcononmy() {
        return 0;
    }

    public static ResultDTO emissionWasteDisposal(WasteDisposalCalcData data) {
        return new ResultDTO((data.getCo2_waste_method() * data.getWeight_waste()) / 1000, 0f, 0f, 0f);
    }

    public static ResultDTO emissionPiggery(PiggeryCalcData data) {
        float e_p_1 = (((data.getWeight() / (data.getFeed_rate() * data.getNum_of_days())) * 1) *  data.getGwp_ch4()) /1000;
//        float e_p_2 = ((data.getWeight() / (data.getFeed_rate() * data.getNum_of_days())) * 6) / data.getGwp_ch4();
        return new ResultDTO(e_p_1 , 0f, e_p_1 , 0f);
    }

    public static ResultDTO emissionIncineration(IncinerationCalcData data) {
        float e1  = data.getWeightwaste() * (data.getDry_content_percen()/100) * (data.getCarbon_fraction()/100) * (data.getFossil_carbon_fraction()/100)
                * (data.getOxidation_factor()/100);
        float e2 = ((data.getWeight_solid_waste() * data.getEf_ch4()));
        float e3 = ((data.getWeight_solid_waste() * data.getEf_n20()));
        return new ResultDTO((e1 + e2 + e3), 0f, e2, e3);
    }

    public static ResultDTO emissionWasteTransport(WasteTransportCalcData data) {
        float fcn =data.getFuel_economy_km_l() == 0 ? 0 : (data.getWaste_tonnes() / data.getLoading_capacity_tonnes()) * data.getUp_down_distance_km() / data.getFuel_economy_km_l()*data.getNo_of_turns();
        float te_mc = (fcn / 1000) * data.getDensity_fuel_t_m3() * data.getNet_caloric_fuel();
        float e_mc_1 = ((data.getEf_co2() * data.getGwp_co2()) / 1000) * te_mc;
        float e_mc_2 = ((data.getEf_ch4() * data.getGwp_ch4()) / 1000) * te_mc;
        float e_mc_3 = ((data.getEf_n2o() * data.getGwp_n2o()) / 1000) * te_mc;
        return new ResultDTO(e_mc_1 + e_mc_2 + e_mc_3, e_mc_1, e_mc_2, e_mc_3);
    }

    public static ResultDTO emissionPrivateTransport(EmpCommuPrivateCalcData data) {
        float fcn =data.getFuel_economy_km_l() == 0 ? 0 : (data.getNumber_of_trips() * data.getUp_down_distance_km()) / data.getFuel_economy_km_l();
        float te_mc = (fcn / 1000) * data.getDensity_fuel_t_m3() * data.getNet_caloric_fuel();
        float e_mc_1 = ((data.getEf_co2() * data.getGwp_co2()) / 1000) * te_mc;
        float e_mc_2 = ((data.getEf_ch4() * data.getGwp_ch4()) / 1000) * te_mc;
        float e_mc_3 = ((data.getEf_n2o() * data.getGwp_n2o()) / 1000) * te_mc;
        ResultDTO dto =  new ResultDTO(e_mc_1 + e_mc_2 + e_mc_3, e_mc_1, e_mc_2, e_mc_3);
        dto.m3 = fcn/1000;
        return dto;

    }

    public static ResultDTO emissionPublicTransport(EmpCommuPublicCalcData data) {
        return new ResultDTO((data.getUp_down_distance() * data.getEf_public_vehicle_per_passenger_kilometer()) / 1000, 0f, 0f, 0f);
    }

    public static float gwp_R407C(GWP_R407C data) {
        float gwp_1 = data.getWeight() * (data.getCh2f2_percent() / 100.0f) * data.getCh2f2_coff();
        float gwp_2 = data.getWeight() * (data.getCf3chf2_percent() / 100.0f) * data.getCf3chf2_coff();
        float gwp_3 = data.getWeight() * (data.getCf3ch2f_percent() / 100.0f) * data.getCf3ch2f_coff();
        return gwp_1 + gwp_2 + gwp_3;
    }

    public static float gwp_R410A(GWP_R410A data) {
        float gwp_1 = data.getWeight() * (data.getCh2f2_percent() / 100.0f) * data.getCh2f2_coff();
        float gwp_2 = data.getWeight() * (data.getChf2cf3_percent() / 100.0f) * data.getChf2cf3_coff();
        return gwp_1 + gwp_2;
    }


}
