package com.climatesi.ghg.implGeneral.facades;

import com.climatesi.ghg.api.beans.Airport;
import com.climatesi.ghg.api.facades.AirportFacade;
import com.climatesi.ghg.implGeneral.persistence.AirportDAO;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class AirportFacadeManager implements AirportFacade {

    private static Logger logger = Logger.getLogger(AirportFacadeManager.class);
    private AirportDAO dao;

    @Override
    public Object addEntity(Airport entity) {
        return null;
    }

    @Override
    public Object addEntity(Airport entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Airport entity) {
        return null;
    }

    @Override
    public Object updateEntity(Airport entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Airport entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Airport entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Airport entity) {
        return null;
    }

    @Override
    public Airport getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Airport> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Airport> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Airport getEntityByKey(Integer id) {
        return null;
    }

    @Override
    public ListResult<Airport> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Airport> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new AirportDAO(entityManager);
    }
}
