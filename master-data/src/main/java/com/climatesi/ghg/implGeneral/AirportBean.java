package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.Airport;

import javax.persistence.*;

@Entity
@Table(name = "AIRPORT")
@NamedQueries({
        @NamedQuery(name = "AirportBean.findById",
                query = "SELECT i FROM AirportBean i WHERE i.id = :id"),
})
public class AirportBean implements Airport {

    @Id
    @Column(name = "AIRPORT_ID")
    private int id;
    @Column(name = "NAME")
    private String name;

    private double lon;
    private double lat;

    private String searchName;

    @Column(name = "IS_DELETED")
    private int isDeleted;




    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
