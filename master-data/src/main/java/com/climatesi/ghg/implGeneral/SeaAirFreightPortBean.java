package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.SeaAirFreightPort;

import javax.persistence.*;


@Entity
@Table(name = "freight_ports")
@NamedQueries({
        @NamedQuery(name = "SeaAirFreightPortBean.findById",
                query = "SELECT i FROM SeaAirFreightPortBean i WHERE i.id = :id"),
})
public class SeaAirFreightPortBean  implements SeaAirFreightPort {


    @Id
    @Column(name = "FREIGHT_PORT_ID")
    private int id;

    private String name;

    private float nauticAirMilesFromCMB;

    private float nauticSeaMilesFromCMB;

    private int isDeleted;




    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public float getNauticAirMilesFromCMB() {
        return this.nauticAirMilesFromCMB;
    }

    @Override
    public void setNauticAirMilesFromCMB(float nauticAirMilesFromCMB) {
        this.nauticAirMilesFromCMB = nauticAirMilesFromCMB;
    }

    @Override
    public float getNauticSeaMilesFromCMB() {
        return this.nauticSeaMilesFromCMB;
    }

    @Override
    public void setNauticSeaMilesFromCMB(float nauticSeaMilesFromCMB) {
        this.nauticSeaMilesFromCMB = nauticSeaMilesFromCMB;
    }

    @Override
    public int getIsDeleted() {
        return this.isDeleted;
    }

    @Override
    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }
}
