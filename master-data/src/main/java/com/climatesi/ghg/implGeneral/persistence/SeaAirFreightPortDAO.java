package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.Country;
import com.climatesi.ghg.api.beans.SeaAirFreightPort;
import com.climatesi.ghg.implGeneral.SeaAirFreightPortBean;
import com.climatesi.ghg.implGeneral.SeaAirFreightPortBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class SeaAirFreightPortDAO  extends AbstractDAO<SeaAirFreightPortBean, SeaAirFreightPort> {

    private static Logger logger = Logger.getLogger(SeaAirFreightPortDAO.class);
    private EntityManager em;


    public SeaAirFreightPortDAO(EntityManager em) {
        super(em, SeaAirFreightPortBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("freight_ports", "FREIGHT_PORT_ID", "SeaAirFreightPortBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(SeaAirFreightPortBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(SeaAirFreightPortBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(SeaAirFreightPortBean entityImpl) throws GHGException {
        return null;
    }
}
