package com.climatesi.ghg.implGeneral.facades;

import com.climatesi.ghg.api.beans.Country;
import com.climatesi.ghg.api.facades.CountryFacade;
import com.climatesi.ghg.implGeneral.persistence.CountryDAO;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class CountryFacadeManager implements CountryFacade {

    private static Logger logger = Logger.getLogger(CountryFacadeManager.class);
    private CountryDAO dao;


    @Override
    public Object addEntity(Country entity) {
        return null;
    }

    @Override
    public Object addEntity(Country entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(Country entity) {
        return null;
    }

    @Override
    public Object updateEntity(Country entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(Country entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(Country entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(Country entity) {
        return null;
    }

    @Override
    public Country getDummyEntity() {
        return null;
    }

    @Override
    public ListResult<Country> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<Country> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public Country getEntityByKey(Integer id) {
        return null;
    }

    @Override
    public ListResult<Country> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<Country> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);
    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new CountryDAO(entityManager);
    }
}
