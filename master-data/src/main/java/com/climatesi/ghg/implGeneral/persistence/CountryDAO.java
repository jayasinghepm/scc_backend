package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.Country;
import com.climatesi.ghg.implGeneral.CountryBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class CountryDAO extends AbstractDAO<CountryBean, Country> {

    private static Logger logger = Logger.getLogger(CountryDAO.class);
    private EntityManager em;


    public CountryDAO(EntityManager em) {
        super(em, CountryBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("countries", "COUNTRY_ID", "CountryBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(CountryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(CountryBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(CountryBean entityImpl) throws GHGException {
        return null;
    }
}
