package com.climatesi.ghg.implGeneral.persistence;

import com.climatesi.ghg.api.beans.Airport;
import com.climatesi.ghg.implGeneral.AirportBean;
import com.climatesi.ghg.utility.api.dao.AbstractDAO;
import com.climatesi.ghg.utility.api.dao.DataEntryInfo;
import com.climatesi.ghg.utility.api.enums.PersistMode;
import com.climatesi.ghg.utility.api.exceptions.GHGException;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import java.util.Map;

public class AirportDAO extends AbstractDAO<AirportBean, Airport> {

    private static Logger logger = Logger.getLogger(AirportDAO.class);
    private EntityManager em;


    public AirportDAO(EntityManager em) {
        super(em, AirportBean.class);
        this.em = em;
        DataEntryInfo meta = new DataEntryInfo("AIRPORT", "AIRPORT_ID", "AirportBean");
        meta.setKeyPropertyName("id");
        meta.setAddMode(PersistMode.JPA_PERSIST);
        super.setDaoMeta(meta);
    }

    @Override
    public Map<String, Object> getAddSpParams(AirportBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getEditSpParams(AirportBean entityImpl) throws GHGException {
        return null;
    }

    @Override
    public Map<String, Object> getChangeStatusSpParams(AirportBean entityImpl) throws GHGException {
        return null;
    }
}
