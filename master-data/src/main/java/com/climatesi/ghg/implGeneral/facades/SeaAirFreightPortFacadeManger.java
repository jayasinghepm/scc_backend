package com.climatesi.ghg.implGeneral.facades;

import com.climatesi.ghg.api.beans.SeaAirFreightPort;
import com.climatesi.ghg.api.facades.SeaAirFreightPortFacade;
import com.climatesi.ghg.implGeneral.persistence.CountryDAO;
import com.climatesi.ghg.implGeneral.persistence.SeaAirFreightPortDAO;
import com.climatesi.ghg.utility.api.data.ListResult;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class SeaAirFreightPortFacadeManger  implements SeaAirFreightPortFacade {
    private static Logger logger = Logger.getLogger(SeaAirFreightPortFacadeManger.class);
    private SeaAirFreightPortDAO dao;


    @Override
    public ListResult<SeaAirFreightPort> getAllEntityList() {
        return null;
    }

    @Override
    public ListResult<SeaAirFreightPort> getAllPaginatedEntityList(int pageNumber, Object sortingProperty) {
        return null;
    }

    @Override
    public SeaAirFreightPort getEntityByKey(Integer id) {
        return dao.findByKey(id);
    }

    @Override
    public ListResult<SeaAirFreightPort> getEntityListByFilter(Object filterCriteria) {
        return null;
    }

    @Override
    public ListResult<SeaAirFreightPort> getPaginatedEntityListByFilter(int pageNumber, Object sortingProperty, Object filterCriteria) {
        return dao.findPaginatedSearchResults(filterCriteria, sortingProperty, pageNumber);

    }

    @Override
    public void injectEntityManager(EntityManager entityManager) {
        dao = new SeaAirFreightPortDAO(entityManager);
    }

    @Override
    public Object addEntity(SeaAirFreightPort entity) {
        return null;
    }

    @Override
    public Object addEntity(SeaAirFreightPort entity, int userID) {
        return null;
    }

    @Override
    public Object updateEntity(SeaAirFreightPort entity) {
        return null;
    }

    @Override
    public Object updateEntity(SeaAirFreightPort entity, int userID) {
        return null;
    }

    @Override
    public boolean setStatusOfEntity(SeaAirFreightPort entity) {
        return false;
    }

    @Override
    public boolean setStatusOfEntity(SeaAirFreightPort entity, int userID) {
        return false;
    }

    @Override
    public Object setStatus(SeaAirFreightPort entity) {
        return null;
    }

    @Override
    public SeaAirFreightPort getDummyEntity() {
        return null;
    }
}
