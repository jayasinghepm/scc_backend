package com.climatesi.ghg.implGeneral;

import com.climatesi.ghg.api.beans.Country;

import javax.persistence.*;

@Entity
@Table(name = "countries")
@NamedQueries({
        @NamedQuery(name = "CountryBean.findById",
                query = "SELECT i FROM CountryBean i WHERE i.id = :id"),
})
public class CountryBean implements Country {

    @Id
    @Column(name = "COUNTRY_ID")
    private int id;

    @Column(name = "NAME", unique = true)
    private String name;

    @Column(name = "IS_DELETED")
    private int isDeleted;

    public int isDeleted() {
        return isDeleted;
    }

    public void setDeleted(int deleted) {
        isDeleted = deleted;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
