package com.climatesi.ghg.api.beans;

public interface SeaAirFreightPort {

    public int getId() ;

    public void setId(int id);

    public String getName() ;

    public void setName(String name) ;

    public float getNauticAirMilesFromCMB();

    public void setNauticAirMilesFromCMB(float nauticAirMilesFromCMB);
    public float getNauticSeaMilesFromCMB() ;

    public void setNauticSeaMilesFromCMB(float nauticSeaMilesFromCMB);

    public int getIsDeleted();

    public void setIsDeleted(int isDeleted) ;
}
