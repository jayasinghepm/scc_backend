package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.SeaAirFreightPort;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface SeaAirFreightPortFacade  extends DataEntryManager<SeaAirFreightPort, Integer> {
}
