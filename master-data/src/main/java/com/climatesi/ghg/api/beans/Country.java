package com.climatesi.ghg.api.beans;

public interface Country {

    public String getName();

    public void setName(String name);

    public int getId();

    public void setId(int id);

    public int isDeleted();

    public void setDeleted(int deleted);
}
