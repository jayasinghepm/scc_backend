package com.climatesi.ghg.api.facades;

import com.climatesi.ghg.api.beans.Country;
import com.climatesi.ghg.utility.api.facade.DataEntryManager;

public interface CountryFacade extends DataEntryManager<Country, Integer> {
}
