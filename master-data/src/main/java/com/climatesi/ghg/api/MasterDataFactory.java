package com.climatesi.ghg.api;

import com.climatesi.ghg.api.facades.AirportFacade;
import com.climatesi.ghg.api.facades.CountryFacade;
import com.climatesi.ghg.api.facades.SeaAirFreightPortFacade;
import com.climatesi.ghg.implGeneral.facades.AirportFacadeManager;
import com.climatesi.ghg.implGeneral.facades.CountryFacadeManager;
import com.climatesi.ghg.implGeneral.facades.SeaAirFreightPortFacadeManger;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;

public class MasterDataFactory {

    private static Logger logger = Logger.getLogger(MasterDataFactory.class);
    private static MasterDataFactory instance;
    private CountryFacade countryFacade;
    private AirportFacade airportFacade;
    private SeaAirFreightPortFacade seaAirFreightPortFacade;

    private MasterDataFactory() {

    }

    public SeaAirFreightPortFacade getSeaAirFreightPortFacade(EntityManager em) {
        if (seaAirFreightPortFacade == null) {
            seaAirFreightPortFacade = new SeaAirFreightPortFacadeManger();
            seaAirFreightPortFacade.injectEntityManager(em);
        }
        return seaAirFreightPortFacade;
    }



    public static MasterDataFactory getInstance() {
        if (instance == null) {
            instance = new MasterDataFactory();
        }
        return instance;
    }

    public CountryFacade getCountryFacade(EntityManager em) {
        if (countryFacade == null) {
            countryFacade = new CountryFacadeManager();
            countryFacade.injectEntityManager(em);
        }
        return countryFacade;
    }

    public AirportFacade getAirportFacade(EntityManager em) {
        if (airportFacade == null) {
            airportFacade = new AirportFacadeManager();
        }
        airportFacade.injectEntityManager(em);
        return airportFacade;
    }
}
